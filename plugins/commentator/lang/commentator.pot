#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Commentator\n"
"POT-Creation-Date: 2015-11-07 11:58+0100\n"
"PO-Revision-Date: 2015-11-07 11:56+0100\n"
"Last-Translator: \n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.8.3\n"
"X-Poedit-Basepath: ..\n"
"X-Poedit-WPHeader: commentator.php\n"
"X-Poedit-SourceCharset: UTF-8\n"
"X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;esc_attr__;"
"esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c;_n_noop:1,2;"
"_nx_noop:3c,1,2;__ngettext_noop:1,2\n"
"X-Poedit-SearchPath-0: .\n"
"X-Poedit-SearchPathExcluded-0: *.js\n"

#: commentator.php:15
msgid "This page cannot be called directly."
msgstr ""

#: commentator.php:44
msgid "Commentator Commenter"
msgstr ""

#: commentator.php:103
msgid "User name should not be empty."
msgstr ""

#: commentator.php:107
msgid "The email is empty or invalid."
msgstr ""

#: commentator.php:112
msgid "You need to chose a password."
msgstr ""

#: commentator.php:130 commentator.php:294
msgid "From"
msgstr ""

#: commentator.php:131 commentator.php:132
msgid "Registration successful"
msgstr ""

#: commentator.php:132
msgid "Your login details"
msgstr ""

#: commentator.php:132
msgid "Username"
msgstr ""

#: commentator.php:132 php/commentator-template.php:231
#: php/commentator-template.php:301
msgid "Password"
msgstr ""

#: commentator.php:137
msgid "Registration successful, check your email for your password"
msgstr ""

#: commentator.php:219
msgid "the post doesn't exist"
msgstr ""

#: commentator.php:223 commentator.php:273 commentator.php:343
#: commentator.php:391
msgid "you are not logged in !"
msgstr ""

#: commentator.php:277
msgid "Flagging is not allowed !"
msgstr ""

#: commentator.php:295
msgid "New Comment Flagging"
msgstr ""

#: commentator.php:297
msgid "A new comment has been flagged on "
msgstr ""

#: commentator.php:299
msgid "Number of flags : "
msgstr ""

#: commentator.php:303
msgid "Go to your wordpress admin to see it"
msgstr ""

#: commentator.php:308
msgid "The comment has been flagged"
msgstr ""

#: commentator.php:348
msgid "Sorry, this is not your comment"
msgstr ""

#: commentator.php:354
msgid "This comment has been deleted"
msgstr ""

#: commentator.php:396
msgid "Sorry, this is not your post"
msgstr ""

#: commentator.php:406
msgid "This comment has been unpicked"
msgstr ""

#: commentator.php:412
msgid "This comment has been picked"
msgstr ""

#: commentator.php:501
msgid "Commentator Social Signin"
msgstr ""

#: commentator.php:553
msgid "Connecting..."
msgstr ""

#: commentator.php:563
msgid "Error: please try again!"
msgstr ""

#: commentator.php:564
msgid "Original error message: "
msgstr ""

#: commentator.php:572
msgid "Congratulations !"
msgstr ""

#: commentator.php:573
msgid "You can now join the conversation !"
msgstr ""

#: commentator.php:609
msgid "This type of image is not allowed"
msgstr ""

#: commentator.php:616 commentator.php:620
msgid "There was an error uploading the image"
msgstr ""

#: commentator.php:625
msgid "Images upload is not allowed"
msgstr ""

#: commentator.php:677
msgid "Sorry, comments are closed for this item."
msgstr ""

#: commentator.php:742
msgid "Sorry, your user role doesn't allow you to comment."
msgstr ""

#: commentator.php:754
msgid "Sorry, you must be logged in to post a comment."
msgstr ""

#: commentator.php:761
msgid "<strong>ERROR</strong>: please fill the required fields (name, email)."
msgstr ""

#: commentator.php:763
msgid "<strong>ERROR</strong>: please enter a valid email address."
msgstr ""

#: commentator.php:767
msgid "<strong>ERROR</strong>: please type a comment."
msgstr ""

#: commentator.php:796
msgid "Your comment is awaiting moderation"
msgstr ""

#: commentator.php:841
msgid "you are not logged in"
msgstr ""

#: commentator.php:845
msgid "the comment you wan't to vote doesn't exist"
msgstr ""

#: commentator.php:1102
msgid "author"
msgstr ""

#: commentator.php:1121
#, php-format
msgid "%1$s ago"
msgstr ""

#: commentator.php:1124
#, php-format
msgid "%1$s at %2$s"
msgstr ""

#: commentator.php:1157 commentator.php:1159
msgid "Flag comment"
msgstr ""

#: commentator.php:1167 commentator.php:1169
msgid "Delete comment"
msgstr ""

#: commentator.php:1183
msgid "Your comment is awaiting moderation."
msgstr ""

#: commentator.php:1218
msgid "Pick comment"
msgstr ""

#: commentator.php:1226
msgid "Vote up"
msgstr ""

#: commentator.php:1230
msgid "Vote down"
msgstr ""

#: commentator.php:1243
msgid "Reply"
msgstr ""

#: commentator.php:1309
msgid "Upvotes"
msgstr ""

#: commentator.php:1310
msgid "Downvotes"
msgstr ""

#: commentator.php:1311
msgid "Flags"
msgstr ""

#: commentator.php:1442
msgid "Overview for the Commentator Plugin"
msgstr ""

#. Plugin Name of the plugin/theme
#: commentator.php:1442
msgid "Commentator"
msgstr ""

#: commentator.php:1521
msgid "Oldest"
msgstr ""

#: commentator.php:1522
msgid "Newest"
msgstr ""

#: commentator.php:1523
msgid "Best"
msgstr ""

#: commentator.php:1524
msgid "Picked"
msgstr ""

#: commentator.php:1537
msgid "General"
msgstr ""

#: commentator.php:1540
msgid "Icons"
msgstr ""

#: commentator.php:1543
msgid "Style"
msgstr ""

#: commentator.php:1546
msgid "Social"
msgstr ""

#: commentator.php:1549
msgid "Advanced"
msgstr ""

#: commentator.php:1561
msgid "Max Threaded Comments Depth (default : 3)"
msgstr ""

#: commentator.php:1565
msgid "Anybody can register ?"
msgstr ""

#: commentator.php:1569
msgid "Human readable time (ago) ?"
msgstr ""

#: commentator.php:1573
msgid "Chose your password when registering ?"
msgstr ""

#: commentator.php:1577
msgid "Disable login tab ?"
msgstr ""

#: commentator.php:1581
msgid "Enable comment flagging ?"
msgstr ""

#: commentator.php:1585
msgid "Enable comment deletion by their author ?"
msgstr ""

#: commentator.php:1589
msgid "Enable comment picking ?"
msgstr ""

#: commentator.php:1593
msgid ""
"From how many flags untill a comment gets unapproved (leave blank for no "
"automation)"
msgstr ""

#: commentator.php:1597
msgid "Disable thread votes ?"
msgstr ""

#: commentator.php:1601
msgid "Section of content before the commenting section"
msgstr ""

#: commentator.php:1606
msgid "Custom menu html"
msgstr ""

#: commentator.php:1610
msgid "Disable avatars ?"
msgstr ""

#: commentator.php:1614
msgid "Enable images uploads ?"
msgstr ""

#: commentator.php:1618
msgid "Enable oEmbed ?"
msgstr ""

#: commentator.php:1622
msgid "Enable clickable links ?"
msgstr ""

#: commentator.php:1632
msgid "Order and Disable Tabs ?"
msgstr ""

#: commentator.php:1634
msgid "Enabled Tabs"
msgstr ""

#: commentator.php:1651
msgid "Disabled Tabs"
msgstr ""

#: commentator.php:1668
msgid "Disable commenting abilities to some user roles ?"
msgstr ""

#: commentator.php:1695
msgid "Base Theme"
msgstr ""

#: commentator.php:1719
msgid "Icon and color for thread voting"
msgstr ""

#: commentator.php:1720
msgid "Icon for thread voted"
msgstr ""

#: commentator.php:1721
msgid "Color when the thread is voted"
msgstr ""

#: commentator.php:1722
msgid "Icon for comment button"
msgstr ""

#: commentator.php:1723
msgid "Icon for comment upvote"
msgstr ""

#: commentator.php:1724
msgid "Icon for comment downvote"
msgstr ""

#: commentator.php:1725
msgid "Icon for comment flagging"
msgstr ""

#: commentator.php:1726
msgid "Icon to pick a comment"
msgstr ""

#: commentator.php:1727
msgid "Icon to delete a comment"
msgstr ""

#: commentator.php:1728
msgid "Icon for comment reply"
msgstr ""

#: commentator.php:1729
msgid "Icon for comment register"
msgstr ""

#: commentator.php:1730
msgid "Icon for comment login"
msgstr ""

#: commentator.php:1731
msgid "Color when a comment is down voted"
msgstr ""

#: commentator.php:1732
msgid "Color when a comment is up voted"
msgstr ""

#: commentator.php:1733
msgid "Color when a comment is picked"
msgstr ""

#: commentator.php:1739
msgid "Enable Social Signin ?"
msgstr ""

#: commentator.php:1752
msgid "I load my scripts myself from my CDN"
msgstr ""

#: commentator.php:1767
msgid "Choose an icon"
msgstr ""

#: php/commentator-template.php:79
msgid "guest"
msgstr ""

#: php/commentator-template.php:98
#, php-format
msgid "%1$s comments"
msgstr ""

#: php/commentator-template.php:100
msgid "One comment"
msgstr ""

#: php/commentator-template.php:102
msgid "There are no comments yet"
msgstr ""

#: php/commentator-template.php:109
msgid "Star this discussion"
msgstr ""

#: php/commentator-template.php:127
msgid "Comments are closed."
msgstr ""

#: php/commentator-template.php:137
msgid "You need to log in to enter the discussion"
msgstr ""

#: php/commentator-template.php:158
msgid "Join the discussion..."
msgstr ""

#: php/commentator-template.php:164 php/commentator-template.php:230
#: php/commentator-template.php:296
msgid "Name"
msgstr ""

#: php/commentator-template.php:167 php/commentator-template.php:297
msgid "Email"
msgstr ""

#: php/commentator-template.php:170
msgid "Website"
msgstr ""

#: php/commentator-template.php:213
msgid "Hello,"
msgstr ""

#: php/commentator-template.php:221
msgid "Logout"
msgstr ""

#: php/commentator-template.php:226
msgid "Login"
msgstr ""

#: php/commentator-template.php:234
msgid "Remember Me"
msgstr ""

#: php/commentator-template.php:244
msgid "Or use one of these social networks"
msgstr ""

#: php/commentator-template.php:292
msgid "Register"
msgstr ""

#. Plugin URI of the plugin/theme
msgid "http://codecanyon.net/item/commentator-wordpress-plugin/6425752"
msgstr ""

#. Description of the plugin/theme
msgid ""
"Commentator Wordpress Plugin is a fully featured ajaxified comments system "
"for your wordpress website that let you easily supercharge your discussions."
msgstr ""

#. Author of the plugin/theme
msgid "Yukulelix"
msgstr ""

#. Author URI of the plugin/theme
msgid "http://codecanyon.net/user/yukulelix"
msgstr ""
