<?php get_header(); ?>

   <?php
   // get menu item id
   global $wpdb;
   $post_id = $wpdb->get_var( 'SELECT ID FROM '.$wpdb->posts.' WHERE post_name="'.$jozoor_gallery_page_current_page.'" AND post_type="page" AND post_status="publish"' );
   $post_title = $wpdb->get_var( 'SELECT post_id FROM '.$wpdb->postmeta.' WHERE meta_key="_menu_item_object_id" AND meta_value="'.$post_id.'"' );
   
   // add active class for nav item
   if (!is_admin() && $jozoor_gallery_page_current_page != 'Select Page Name') { ?>
   <script>
    jQuery(document).ready(function($) {
     $("#menu-item-<?php echo $post_title;  ?>").addClass("current-menu-item");
    });
    </script>
   <?php } ?>

   <!-- Start main content -->
   <div class="container main-content clearfix">
       
   <div id="post-<?php the_ID(); ?>" <?php post_class('gallery single-gallery'); ?>>
       
   <?php 

   while ( have_posts() ) : the_post();

   // check post protected 
   if ( !post_password_required() ) {
    
   // get metaboxs
   $j_gallery_layout = get_post_meta( $post->ID, '_jozoor_gallery_layout', true ); // gallery layout
   $j_gallery_layout_masonry = get_post_meta( $post->ID, '_jozoor_gallery_layout_masonry', true ); // gallery layout masonry
   $j_gallery_images = get_post_meta( $post->ID, '_jozoor_gallery_images', true ); // gallery images 
       
   // check column
   if( $j_gallery_layout == 'col2' ) { 
       $j_gallery_layout_class = 'eight columns col2'; 
       if( $j_gallery_layout_masonry == 'on' ) { $j_gallery_images_size = 'full'; } else { $j_gallery_images_size = 'gallery-thumb';  } 
   }
   elseif( $j_gallery_layout == 'col1' ) { 
       $j_gallery_layout_class = 'sixteen columns col1'; $j_gallery_images_size = 'full'; 
   }
   elseif ( $j_gallery_layout == 'col4' ) { 
       $j_gallery_layout_class = 'four columns col4'; 
       if( $j_gallery_layout_masonry == 'on' ) { $j_gallery_images_size = 'full'; } else { $j_gallery_images_size = 'gallery-thumb';  }
   }
   else { 
       $j_gallery_layout_class = 'one-third column col3'; 
       if( $j_gallery_layout_masonry == 'on' ) { $j_gallery_images_size = 'full'; } else { $j_gallery_images_size = 'gallery-thumb';  }
   }
       
       
   if( !empty( $post->post_content) ) {
   echo '<div class="sixteen columns full-width">';
   echo '<div class="entry-content post-content bottom-3">';
     the_content();
   echo '</div><!-- End page-content -->';
   echo '</div><div class="clearfix"></div>';
   }
       
   if( $j_gallery_layout_masonry == 'on' ) {
   echo '<div id="contain" class="clearfix">';
   }
   
   ?>
    
   <!-- Gallery Images -->
   <?php if ( $j_gallery_images ) { 
    
   foreach ( $j_gallery_images as $attachment_id => $img_full_url ) {
        echo '<div class="'.$j_gallery_layout_class.' item gallery-item">';
        $j_full_image_url = wp_get_attachment_image_src( $attachment_id, 'full');
        // get attachment data
        $attachment = get_post( $attachment_id );
        // $j_attachment_title = $attachment->post_title; // title
        $j_attachment_caption = $attachment->post_excerpt;  // caption
        // description
        if(!empty($attachment->post_content) ) {
        $j_attachment_description = '<br>'.$attachment->post_content; 
        } else { $j_attachment_description = ''; }
       
        echo '<a class="fancybox-gallery" href="'. $j_full_image_url[0]  .'" rel="'. get_the_title() .'" caption="'.$j_attachment_caption.$j_attachment_description.'">';
        echo wp_get_attachment_image( $attachment_id, $j_gallery_images_size );
        echo '<div class="img-caption-gallery">
         <div class="desc"><h5>'.$j_attachment_caption.'</h5></div>
        </div>';
        echo '</a></div>';
   }
       
   } ?>
   <!-- End -->  
       
   <?php 
   if( $j_gallery_layout_masonry == 'on' ) {
   echo '</div>';
   }
   ?>
  
   <?php
       
   } else { 
   echo '<div class="sixteen columns entry-content post-content">';    
   the_content(); 
   echo '</div>';
   }

   // comments 
   if( $jozoor_gallery_page_comments == 1 ) {
   if ( comments_open() || get_comments_number() ) {
       echo '<div class="sixteen columns bottom-1">';
       comments_template();
       echo '</div>';
   }
   }

   endwhile; 

   ?>

   </div><!-- End gallery page -->
       
<?php get_footer(); ?>