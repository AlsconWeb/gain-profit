/**
 * SMOF js
 *
 * contains the core functionalities to be used
 * inside SMOF
 */

jQuery.noConflict();

/** Fire up jQuery - let's dance! 
 */
jQuery(document).ready(function($){
	
	//(un)fold options in a checkbox-group
  	jQuery('.fld').click(function() {
    	var $fold='.f_'+this.id;
    	$($fold).slideToggle('normal', "swing");
  	});

  	//Color picker
  	$('.of-color').wpColorPicker();
	
	//hides warning if js is enabled			
	$('#js-warning').hide();
	
	//Tabify Options			
	$('.group').hide();
	
	// Get the URL parameter for tab
	function getURLParameter(name) {
	    return decodeURI(
	        (RegExp(name + '=' + '(.+?)(&|$)').exec(location.search)||[,''])[1]
	    );
	}
	
	// If the $_GET param of tab is set, use that for the tab that should be open
	if (getURLParameter('tab') != "") {
		$.cookie('of_current_opt', '#'+getURLParameter('tab'), { expires: 7, path: '/' });
	}

	// Display last current tab	
	if ($.cookie("of_current_opt") === null) {
		$('.group:first').fadeIn('fast');	
		$('#of-nav li:first').addClass('current');
	} else {
	
		var hooks = $('#hooks').html();
		hooks = jQuery.parseJSON(hooks);
		
		$.each(hooks, function(key, value) { 
		
			if ($.cookie("of_current_opt") == '#of-option-'+ value) {
				$('.group#of-option-' + value).fadeIn();
				$('#of-nav li.' + value).addClass('current');
			}
			
		});
	
	}
				
	//Current Menu Class
	$('#of-nav li a').click(function(evt){
	// event.preventDefault();
				
		$('#of-nav li').removeClass('current');
		$(this).parent().addClass('current');
							
		var clicked_group = $(this).attr('href');
		
		$.cookie('of_current_opt', clicked_group, { expires: 7, path: '/' });
			
		$('.group').hide();
							
		$(clicked_group).fadeIn('fast');
		return false;
						
	});

	//Expand Options 
	var flip = 0;
				
	$('#expand_options').click(function(){
		if(flip == 0){
			flip = 1;
			$('#of_container #of-nav').hide();
			$('#of_container #content').width(910);
            $('#of_container .controls').width(650);
            $('#of_container .explain').width(235);
            $('#of_container .of-info').width(887);
			$('#of_container .group').add('#of_container .group h2').show();
	
			$(this).removeClass('expand');
			$(this).addClass('close');
			$(this).text('Close');
					
		} else {
			flip = 0;
			$('#of_container #of-nav').show();
			$('#of_container #content').width(700);
            $('#of_container .controls').width(480);
            $('#of_container .explain').width(195);
            $('#of_container .of-info').width(677);
			$('#of_container .group').add('#of_container .group h2').hide();
			$('#of_container .group:first').show();
			$('#of_container #of-nav li').removeClass('current');
			$('#of_container #of-nav li:first').addClass('current');
					
			$(this).removeClass('close');
			$(this).addClass('expand');
			$(this).text('Expand');
				
		}
			
	});
	
	//Update Message popup
	$.fn.center = function () {
		this.animate({"top":( $(window).height() - this.height() - 200 ) / 2+$(window).scrollTop() + "px"},100);
		this.css("left", 385 );
		return this;
	}
		
			
	$('#of-popup-save').center();
	$('#of-popup-reset').center();
	$('#of-popup-fail').center();
			
	$(window).scroll(function() { 
		$('#of-popup-save').center();
		$('#of-popup-reset').center();
		$('#of-popup-fail').center();
	});
			

	//Masked Inputs (images as radio buttons)
	$('.of-radio-img-img').click(function(){
		$(this).parent().parent().find('.of-radio-img-img').removeClass('of-radio-img-selected');
		$(this).addClass('of-radio-img-selected');
	});
	$('.of-radio-img-label').hide();
	$('.of-radio-img-img').show();
	$('.of-radio-img-radio').hide();
	
	//Masked Inputs (background images as radio buttons)
	$('.of-radio-tile-img').click(function(){
		$(this).parent().parent().find('.of-radio-tile-img').removeClass('of-radio-tile-selected');
		$(this).addClass('of-radio-tile-selected');
	});
	$('.of-radio-tile-label').hide();
	$('.of-radio-tile-img').show();
	$('.of-radio-tile-radio').hide();

	// Style Select
	(function ($) {
	styleSelect = {
		init: function () {
		$('.select_wrapper').each(function () {
			$(this).prepend('<span>' + $(this).find('.select option:selected').text() + '</span>');
		});
		$('.select').live('change', function () {
			$(this).prev('span').replaceWith('<span>' + $(this).find('option:selected').text() + '</span>');
		});
		$('.select').bind($.browser.msie ? 'click' : 'change', function(event) {
			$(this).prev('span').replaceWith('<span>' + $(this).find('option:selected').text() + '</span>');
		}); 
		}
	};
	$(document).ready(function () {
		styleSelect.init()
	})
	})(jQuery);
	
	
	/** Aquagraphite Slider MOD */
	
	//Hide (Collapse) the toggle containers on load
	$(".slide_body").hide(); 

	//Switch the "Open" and "Close" state per click then slide up/down (depending on open/close state)
	$(".slide_edit_button").live( 'click', function(){		
		/*
		//display as an accordion
		$(".slide_header").removeClass("active");	
		$(".slide_body").slideUp("fast");
		*/
		//toggle for each
		$(this).parent().toggleClass("active").next().slideToggle("fast");
		return false; //Prevent the browser jump to the link anchor
	});	
	
	// Update slide title upon typing		
	function update_slider_title(e) {
		var element = e;
		if ( this.timer ) {
			clearTimeout( element.timer );
		}
		this.timer = setTimeout( function() {
			$(element).parent().prev().find('strong').text( element.value );
		}, 100);
		return true;
	}
	
	$('.of-slider-title').live('keyup', function(){
		update_slider_title(this);
	});
		
	
	//Remove individual slide
	$('.slide_delete_button').live('click', function(){
	// event.preventDefault();
	var agree = confirm("Are you sure you wish to delete this slide?");
		if (agree) {
			var $trash = $(this).parents('li');
			//$trash.slideUp('slow', function(){ $trash.remove(); }); //chrome + confirm bug made slideUp not working...
			$trash.animate({
					opacity: 0.25,
					height: 0,
				}, 500, function() {
					$(this).remove();
			});
			return false; //Prevent the browser jump to the link anchor
		} else {
		return false;
		}	
	});
	
	//Add new slide
	$(".slide_add_button").live('click', function(){		
		var slidesContainer = $(this).prev();
		var sliderId = slidesContainer.attr('id');
		
		var numArr = $('#'+sliderId +' li').find('.order').map(function() { 
			var str = this.id; 
			str = str.replace(/\D/g,'');
			str = parseFloat(str);
			return str;			
		}).get();
		
		var maxNum = Math.max.apply(Math, numArr);
		if (maxNum < 1 ) { maxNum = 0};
		var newNum = maxNum + 1;
		
		var newSlide = '<li class="temphide"><div class="slide_header"><strong>Slide ' + newNum + '</strong><input type="hidden" class="slide of-input order" name="' + sliderId + '[' + newNum + '][order]" id="' + sliderId + '_slide_order-' + newNum + '" value="' + newNum + '"><a class="slide_edit_button" href="#">Edit</a><a class="slide_delete_button" href="#" title="delete">Delete</a></div><div class="slide_body" style="display: none; "><label>Title</label><input class="slide of-input of-slider-title" name="' + sliderId + '[' + newNum + '][title]" id="' + sliderId + '_' + newNum + '_slide_title" value=""><label>Image URL</label><input class="upload slide of-input" name="' + sliderId + '[' + newNum + '][url]" id="' + sliderId + '_' + newNum + '_slide_url" value=""><div class="upload_button_div"><span class="button media_upload_button" id="' + sliderId + '_' + newNum + '">Upload</span><span class="button remove-image hide" id="reset_' + sliderId + '_' + newNum + '" title="' + sliderId + '_' + newNum + '">Remove</span></div><div class="screenshot"></div><label>Link URL (optional)</label><input class="slide of-input" name="' + sliderId + '[' + newNum + '][link]" id="' + sliderId + '_' + newNum + '_slide_link" value=""><label>Description (optional)</label><textarea class="slide of-input" name="' + sliderId + '[' + newNum + '][description]" id="' + sliderId + '_' + newNum + '_slide_description" cols="8" rows="8"></textarea><div class="clear"></div></div></li>';
		
		slidesContainer.append(newSlide);
		var nSlide = slidesContainer.find('.temphide');
		nSlide.fadeIn('fast', function() {
			$(this).removeClass('temphide');
		});
				
		optionsframework_file_bindings(); // re-initialise upload image..
		
		return false; //prevent jumps, as always..
	});	
	
	//Sort slides
	jQuery('.slider').find('ul').each( function() {
		var id = jQuery(this).attr('id');
		$('#'+ id).sortable({
			placeholder: "placeholder",
			opacity: 0.6,
			handle: ".slide_header",
			cancel: "a"
		});	
	});
    
    /*======================================== new code =================================*/
    
    /** social Slider MOD */
	
	//Hide (Collapse) the toggle containers on load
	$(".slide_body_social").hide(); 

	//Switch the "Open" and "Close" state per click then slide up/down (depending on open/close state)
	$(".slide_edit_button_social").live( 'click', function(){		
		/*
		//display as an accordion
		$(".slide_header").removeClass("active");	
		$(".slide_body").slideUp("fast");
		*/
		//toggle for each
		$(this).parent().toggleClass("active").next().slideToggle("fast");
		return false; //Prevent the browser jump to the link anchor
	});	
	
	// Update slide title upon typing		
	function update_slider_title(e) {
		var element = e;
		if ( this.timer ) {
			clearTimeout( element.timer );
		}
		this.timer = setTimeout( function() {
			$(element).parent().prev().find('strong').text( element.value );
		}, 100);
		return true;
	}
	
	/*$('.of-slider-name').live('keyup', function(){
		update_slider_title(this);
	});*/
    
    /*$('.select_wrapper_1 .select').live('change', function(){
        $('#social_slider_header_1_slide_name').val(this.value);
        $('.select_wrapper_1').parent().prev().find('strong').text( this.value );
	});
    
    $('.select_wrapper_1 .select').val($('#social_slider_header_1_slide_name').val());
    */
	
	//Remove individual slide
	$('.slide_delete_button_social').live('click', function(){
	// event.preventDefault();
	var agree = confirm("Are you sure you wish to delete this slide?");
		if (agree) {
			var $trash = $(this).parents('li');
			//$trash.slideUp('slow', function(){ $trash.remove(); }); //chrome + confirm bug made slideUp not working...
			$trash.animate({
					opacity: 0.25,
					height: 0,
				}, 500, function() {
					$(this).remove();
			});
			return false; //Prevent the browser jump to the link anchor
		} else {
		return false;
		}	
	});
    
    // icons list array
    var socialIconslistContent = '';
    var socialIconslist = ['twitter', 'facebook', 'forrst', 'reddit', 'rss', 'tumblr', 'delicious', 'paypal', 'gplus', 'android', 'pinterest', 'foursquare', 'behance', 'deviantart', 'digg', 'dribbble', 
          'blogger', 'flickr', 'google', 'skype', 'youtube', 'vimeo', 'yahoo', 'dropbox', 'soundcloud', 'instagram', 'wordpress', 'duckduckgo', 'aim', 'flattr', 'eventful', 'smashmag', 
          'wikipedia', 'lanyrd', 'stumbleupon', 'fivehundredpx', 'bitcoin', 'w3c', 'html5', 'ie', 'grooveshark', 'ninetyninedesigns', 'spotify', 'gowalla', 'appstore', 'cc', 'evernote', 
          'viadeo', 'instapaper', 'weibo', 'klout', 'linkedin', 'meetup', 'vk', 'plancast', 'disqus', 'windows', 'xing', 'chrome', 'myspace', 'podcast', 'amazon', 'steam', 
          'cloudapp', 'ebay', 'github', 'googleplay', 'itunes', 'plurk', 'songkick', 'lastfm', 'gmail', 'openid', 'quora', 'eventasaurus', 'yelp', 'intensedebate', 'eventbrite', 
          'scribd', 'posterous', 'stripe', 'opentable', 'angellist', 'dwolla', 'appnet', 'statusnet', 'drupal', 'buffer', 'pocket', 'github-circled', 'bitbucket', 'lego', 'stackoverflow', 
          'hackernews', 'lkdto'] ;
    
    $.each( socialIconslist, function( intIndex, objValue ){
      socialIconslistContent += '<option id="'+objValue+'" value="'+objValue+'">'+objValue+'</option>';
    });
	
	//Add new slide
	$(".slide_add_button_social").live('click', function(){		
		var slidesContainer = $(this).prev();
		var sliderId = slidesContainer.attr('id');
		
		var numArr = $('#'+sliderId +' li').find('.order').map(function() { 
			var str = this.id; 
			str = str.replace(/\D/g,'');
			str = parseFloat(str);
			return str;			
		}).get();
		
		var maxNum = Math.max.apply(Math, numArr);
		if (maxNum < 1 ) { maxNum = 0};
		var newNum = maxNum + 1;
		
		var newSlide = '<li class="temphide"><div class="slide_header_social"><strong>Network ' + newNum + '</strong><input type="hidden" class="slide of-input order" name="' + sliderId + '[' + newNum + '][order]" id="' + sliderId + '_slide_order-' + newNum + '" value="' + newNum + '"><a class="slide_edit_button_social" href="#">Edit</a><a class="slide_delete_button_social" href="#" title="delete">Delete</a></div><div class="slide_body_social" style="display: none; "><label>Select Social Network</label><input class="slide of-input of-slider-name" name="' + sliderId + '[' + newNum + '][name]" id="' + sliderId + '_' + newNum + '_slide_name" value=""><div class="select_wrapper select_wrapper_'+ newNum +'"><select class="select">'+socialIconslistContent+'</select></div><div class="clear"></div><label>Social Network Link <br> [ <small>EX : http://twitter.com/username</small> ]</label><input class="slide of-input" name="' + sliderId + '[' + newNum + '][link]" id="' + sliderId + '_' + newNum + '_slide_link" value=""><div class="clear"></div></div></li>';
		
		slidesContainer.append(newSlide);
		var nSlide = slidesContainer.find('.temphide');
		nSlide.fadeIn('fast', function() {
			$(this).removeClass('temphide');
		});
				
		optionsframework_file_bindings(); // re-initialise upload image..
        
        $('#'+sliderId+' .select_wrapper_'+ newNum +' select').live('change', function(){
        var nowNum = newNum;
        var nowsliderId = sliderId;
        $('#'+nowsliderId+' #social_slider_header_'+nowNum+'_slide_name').val(this.value);
        $('#'+nowsliderId+' .select_wrapper_'+ nowNum +'').parent().prev().find('strong').text( this.value );
	    });
    
       $('#'+sliderId+' .select_wrapper_'+ newNum +' select').val($('#'+sliderId+' #social_slider_header_'+newNum+'_slide_name').val());
        
        if($('#social_slider_header_'+ newNum +'_slide_name').val() == '' ){  
            $('#social_slider_header .select_wrapper_'+ newNum +' span').remove(); 
            $('#social_slider_header .select_wrapper_'+ newNum +'').prepend('<span>twitter</span>'); 
        } 
        
        if($('#social_slider_footer_'+ newNum +'_slide_name').val() == '' ){  
            $('#social_slider_footer .select_wrapper_'+ newNum +' span').remove(); 
            $('#social_slider_footer .select_wrapper_'+ newNum +'').prepend('<span>twitter</span>'); 
        } 
        
        if($('#under_cons_social_slider_'+ newNum +'_slide_name').val() == '' ){  
            $('#under_cons_social_slider .select_wrapper_'+ newNum +' span').remove(); 
            $('#under_cons_social_slider .select_wrapper_'+ newNum +'').prepend('<span>twitter</span>'); 
        } 
		
		return false; //prevent jumps, as always..
	});	
	
	//Sort slides
	jQuery('.social').find('ul').each( function() {
		var id = jQuery(this).attr('id');
		$('#'+ id).sortable({
			placeholder: "placeholder",
			opacity: 0.6,
			handle: ".slide_header_social",
			cancel: "a"
		});	
	});
    
    if($('#social_slider_header_1_slide_name').val() == '' ){  $('#social_slider_header .select_wrapper_1 span').remove(); $('#social_slider_header .select_wrapper_1').prepend('<span>twitter</span>'); }
    if($('#social_slider_header_2_slide_name').val() == '' ){  $('#social_slider_header .select_wrapper_2 span').remove(); $('#social_slider_header .select_wrapper_2').prepend('<span>twitter</span>'); }
    if($('#social_slider_header_3_slide_name').val() == '' ){  $('#social_slider_header .select_wrapper_3 span').remove(); $('#social_slider_header .select_wrapper_3').prepend('<span>twitter</span>'); }
    if($('#social_slider_header_4_slide_name').val() == '' ){  $('#social_slider_header .select_wrapper_4 span').remove(); $('#social_slider_header .select_wrapper_4').prepend('<span>twitter</span>'); }
    if($('#social_slider_header_5_slide_name').val() == '' ){  $('#social_slider_header .select_wrapper_5 span').remove(); $('#social_slider_header .select_wrapper_5').prepend('<span>twitter</span>'); }
    if($('#social_slider_header_6_slide_name').val() == '' ){  $('#social_slider_header .select_wrapper_6 span').remove(); $('#social_slider_header .select_wrapper_6').prepend('<span>twitter</span>'); }
    if($('#social_slider_header_7_slide_name').val() == '' ){  $('#social_slider_header .select_wrapper_7 span').remove(); $('#social_slider_header .select_wrapper_7').prepend('<span>twitter</span>'); }
    if($('#social_slider_header_8_slide_name').val() == '' ){  $('#social_slider_header .select_wrapper_8 span').remove(); $('#social_slider_header .select_wrapper_8').prepend('<span>twitter</span>'); }
    if($('#social_slider_header_9_slide_name').val() == '' ){  $('#social_slider_header .select_wrapper_9 span').remove(); $('#social_slider_header .select_wrapper_9').prepend('<span>twitter</span>'); }
    if($('#social_slider_header_10_slide_name').val() == '' ){  $('#social_slider_header .select_wrapper_10 span').remove(); $('#social_slider_header .select_wrapper_10').prepend('<span>twitter</span>'); }
    if($('#social_slider_header_11_slide_name').val() == '' ){  $('#social_slider_header .select_wrapper_11 span').remove(); $('#social_slider_header .select_wrapper_11').prepend('<span>twitter</span>'); }
    if($('#social_slider_header_12_slide_name').val() == '' ){  $('#social_slider_header .select_wrapper_12 span').remove(); $('#social_slider_header .select_wrapper_12').prepend('<span>twitter</span>'); }
    if($('#social_slider_header_13_slide_name').val() == '' ){  $('#social_slider_header .select_wrapper_13 span').remove(); $('#social_slider_header .select_wrapper_13').prepend('<span>twitter</span>'); }
    if($('#social_slider_header_14_slide_name').val() == '' ){  $('#social_slider_header .select_wrapper_14 span').remove(); $('#social_slider_header .select_wrapper_14').prepend('<span>twitter</span>'); }
    if($('#social_slider_header_15_slide_name').val() == '' ){  $('#social_slider_header .select_wrapper_15 span').remove(); $('#social_slider_header .select_wrapper_15').prepend('<span>twitter</span>'); }
    if($('#social_slider_header_16_slide_name').val() == '' ){  $('#social_slider_header .select_wrapper_16 span').remove(); $('#social_slider_header .select_wrapper_16').prepend('<span>twitter</span>'); }
    if($('#social_slider_header_17_slide_name').val() == '' ){  $('#social_slider_header .select_wrapper_17 span').remove(); $('#social_slider_header .select_wrapper_17').prepend('<span>twitter</span>'); }
    if($('#social_slider_header_18_slide_name').val() == '' ){  $('#social_slider_header .select_wrapper_18 span').remove(); $('#social_slider_header .select_wrapper_18').prepend('<span>twitter</span>'); }
    if($('#social_slider_header_19_slide_name').val() == '' ){  $('#social_slider_header .select_wrapper_19 span').remove(); $('#social_slider_header .select_wrapper_19').prepend('<span>twitter</span>'); }
    if($('#social_slider_header_20_slide_name').val() == '' ){  $('#social_slider_header .select_wrapper_20 span').remove(); $('#social_slider_header .select_wrapper_20').prepend('<span>twitter</span>'); }
    if($('#social_slider_header_21_slide_name').val() == '' ){  $('#social_slider_header .select_wrapper_21 span').remove(); $('#social_slider_header .select_wrapper_21').prepend('<span>twitter</span>'); }
    if($('#social_slider_header_22_slide_name').val() == '' ){  $('#social_slider_header .select_wrapper_22 span').remove(); $('#social_slider_header .select_wrapper_22').prepend('<span>twitter</span>'); }
    if($('#social_slider_header_23_slide_name').val() == '' ){  $('#social_slider_header .select_wrapper_23 span').remove(); $('#social_slider_header .select_wrapper_23').prepend('<span>twitter</span>'); }
    if($('#social_slider_header_24_slide_name').val() == '' ){  $('#social_slider_header .select_wrapper_24 span').remove(); $('#social_slider_header .select_wrapper_24').prepend('<span>twitter</span>'); }
    if($('#social_slider_header_25_slide_name').val() == '' ){  $('#social_slider_header .select_wrapper_25 span').remove(); $('#social_slider_header .select_wrapper_25').prepend('<span>twitter</span>'); }
    if($('#social_slider_header_26_slide_name').val() == '' ){  $('#social_slider_header .select_wrapper_26 span').remove(); $('#social_slider_header .select_wrapper_26').prepend('<span>twitter</span>'); }
    if($('#social_slider_header_27_slide_name').val() == '' ){  $('#social_slider_header .select_wrapper_27 span').remove(); $('#social_slider_header .select_wrapper_27').prepend('<span>twitter</span>'); }
    if($('#social_slider_header_28_slide_name').val() == '' ){  $('#social_slider_header .select_wrapper_28 span').remove(); $('#social_slider_header .select_wrapper_28').prepend('<span>twitter</span>'); }
    if($('#social_slider_header_29_slide_name').val() == '' ){  $('#social_slider_header .select_wrapper_29 span').remove(); $('#social_slider_header .select_wrapper_29').prepend('<span>twitter</span>'); }
    if($('#social_slider_header_30_slide_name').val() == '' ){  $('#social_slider_header .select_wrapper_30 span').remove(); $('#social_slider_header .select_wrapper_30').prepend('<span>twitter</span>'); }
    
    
    if($('#social_slider_footer_1_slide_name').val() == '' ){  $('#social_slider_footer .select_wrapper_1 span').remove(); $('#social_slider_footer .select_wrapper_1').prepend('<span>twitter</span>'); }
    if($('#social_slider_footer_2_slide_name').val() == '' ){  $('#social_slider_footer .select_wrapper_2 span').remove(); $('#social_slider_footer .select_wrapper_2').prepend('<span>twitter</span>'); }
    if($('#social_slider_footer_3_slide_name').val() == '' ){  $('#social_slider_footer .select_wrapper_3 span').remove(); $('#social_slider_footer .select_wrapper_3').prepend('<span>twitter</span>'); }
    if($('#social_slider_footer_4_slide_name').val() == '' ){  $('#social_slider_footer .select_wrapper_4 span').remove(); $('#social_slider_footer .select_wrapper_4').prepend('<span>twitter</span>'); }
    if($('#social_slider_footer_5_slide_name').val() == '' ){  $('#social_slider_footer .select_wrapper_5 span').remove(); $('#social_slider_footer .select_wrapper_5').prepend('<span>twitter</span>'); }
    if($('#social_slider_footer_6_slide_name').val() == '' ){  $('#social_slider_footer .select_wrapper_6 span').remove(); $('#social_slider_footer .select_wrapper_6').prepend('<span>twitter</span>'); }
    if($('#social_slider_footer_7_slide_name').val() == '' ){  $('#social_slider_footer .select_wrapper_7 span').remove(); $('#social_slider_footer .select_wrapper_7').prepend('<span>twitter</span>'); }
    if($('#social_slider_footer_8_slide_name').val() == '' ){  $('#social_slider_footer .select_wrapper_8 span').remove(); $('#social_slider_footer .select_wrapper_8').prepend('<span>twitter</span>'); }
    if($('#social_slider_footer_9_slide_name').val() == '' ){  $('#social_slider_footer .select_wrapper_9 span').remove(); $('#social_slider_footer .select_wrapper_9').prepend('<span>twitter</span>'); }
    if($('#social_slider_footer_10_slide_name').val() == '' ){  $('#social_slider_footer .select_wrapper_10 span').remove(); $('#social_slider_footer .select_wrapper_10').prepend('<span>twitter</span>'); }
    if($('#social_slider_footer_11_slide_name').val() == '' ){  $('#social_slider_footer .select_wrapper_11 span').remove(); $('#social_slider_footer .select_wrapper_11').prepend('<span>twitter</span>'); }
    if($('#social_slider_footer_12_slide_name').val() == '' ){  $('#social_slider_footer .select_wrapper_12 span').remove(); $('#social_slider_footer .select_wrapper_12').prepend('<span>twitter</span>'); }
    if($('#social_slider_footer_13_slide_name').val() == '' ){  $('#social_slider_footer .select_wrapper_13 span').remove(); $('#social_slider_footer .select_wrapper_13').prepend('<span>twitter</span>'); }
    if($('#social_slider_footer_14_slide_name').val() == '' ){  $('#social_slider_footer .select_wrapper_14 span').remove(); $('#social_slider_footer .select_wrapper_14').prepend('<span>twitter</span>'); }
    if($('#social_slider_footer_15_slide_name').val() == '' ){  $('#social_slider_footer .select_wrapper_15 span').remove(); $('#social_slider_footer .select_wrapper_15').prepend('<span>twitter</span>'); }
    if($('#social_slider_footer_16_slide_name').val() == '' ){  $('#social_slider_footer .select_wrapper_16 span').remove(); $('#social_slider_footer .select_wrapper_16').prepend('<span>twitter</span>'); }
    if($('#social_slider_footer_17_slide_name').val() == '' ){  $('#social_slider_footer .select_wrapper_17 span').remove(); $('#social_slider_footer .select_wrapper_17').prepend('<span>twitter</span>'); }
    if($('#social_slider_footer_18_slide_name').val() == '' ){  $('#social_slider_footer .select_wrapper_18 span').remove(); $('#social_slider_footer .select_wrapper_18').prepend('<span>twitter</span>'); }
    if($('#social_slider_footer_19_slide_name').val() == '' ){  $('#social_slider_footer .select_wrapper_19 span').remove(); $('#social_slider_footer .select_wrapper_19').prepend('<span>twitter</span>'); }
    if($('#social_slider_footer_20_slide_name').val() == '' ){  $('#social_slider_footer .select_wrapper_20 span').remove(); $('#social_slider_footer .select_wrapper_20').prepend('<span>twitter</span>'); }
    if($('#social_slider_footer_21_slide_name').val() == '' ){  $('#social_slider_footer .select_wrapper_21 span').remove(); $('#social_slider_footer .select_wrapper_21').prepend('<span>twitter</span>'); }
    if($('#social_slider_footer_22_slide_name').val() == '' ){  $('#social_slider_footer .select_wrapper_22 span').remove(); $('#social_slider_footer .select_wrapper_22').prepend('<span>twitter</span>'); }
    if($('#social_slider_footer_23_slide_name').val() == '' ){  $('#social_slider_footer .select_wrapper_23 span').remove(); $('#social_slider_footer .select_wrapper_23').prepend('<span>twitter</span>'); }
    if($('#social_slider_footer_24_slide_name').val() == '' ){  $('#social_slider_footer .select_wrapper_24 span').remove(); $('#social_slider_footer .select_wrapper_24').prepend('<span>twitter</span>'); }
    if($('#social_slider_footer_25_slide_name').val() == '' ){  $('#social_slider_footer .select_wrapper_25 span').remove(); $('#social_slider_footer .select_wrapper_25').prepend('<span>twitter</span>'); }
    if($('#social_slider_footer_26_slide_name').val() == '' ){  $('#social_slider_footer .select_wrapper_26 span').remove(); $('#social_slider_footer .select_wrapper_26').prepend('<span>twitter</span>'); }
    if($('#social_slider_footer_27_slide_name').val() == '' ){  $('#social_slider_footer .select_wrapper_27 span').remove(); $('#social_slider_footer .select_wrapper_27').prepend('<span>twitter</span>'); }
    if($('#social_slider_footer_28_slide_name').val() == '' ){  $('#social_slider_footer .select_wrapper_28 span').remove(); $('#social_slider_footer .select_wrapper_28').prepend('<span>twitter</span>'); }
    if($('#social_slider_footer_29_slide_name').val() == '' ){  $('#social_slider_footer .select_wrapper_29 span').remove(); $('#social_slider_footer .select_wrapper_29').prepend('<span>twitter</span>'); }
    if($('#social_slider_footer_30_slide_name').val() == '' ){  $('#social_slider_footer .select_wrapper_30 span').remove(); $('#social_slider_footer .select_wrapper_30').prepend('<span>twitter</span>'); }
    
    
    if($('#under_cons_social_slider_1_slide_name').val() == '' ){  $('#under_cons_social_slider .select_wrapper_1 span').remove(); $('#under_cons_social_slider .select_wrapper_1').prepend('<span>twitter</span>'); }
    if($('#under_cons_social_slider_2_slide_name').val() == '' ){  $('#under_cons_social_slider .select_wrapper_2 span').remove(); $('#under_cons_social_slider .select_wrapper_2').prepend('<span>twitter</span>'); }
    if($('#under_cons_social_slider_3_slide_name').val() == '' ){  $('#under_cons_social_slider .select_wrapper_3 span').remove(); $('#under_cons_social_slider .select_wrapper_3').prepend('<span>twitter</span>'); }
    if($('#under_cons_social_slider_4_slide_name').val() == '' ){  $('#under_cons_social_slider .select_wrapper_4 span').remove(); $('#under_cons_social_slider .select_wrapper_4').prepend('<span>twitter</span>'); }
    if($('#under_cons_social_slider_5_slide_name').val() == '' ){  $('#under_cons_social_slider .select_wrapper_5 span').remove(); $('#under_cons_social_slider .select_wrapper_5').prepend('<span>twitter</span>'); }
    if($('#under_cons_social_slider_6_slide_name').val() == '' ){  $('#under_cons_social_slider .select_wrapper_6 span').remove(); $('#under_cons_social_slider .select_wrapper_6').prepend('<span>twitter</span>'); }
    if($('#under_cons_social_slider_7_slide_name').val() == '' ){  $('#under_cons_social_slider .select_wrapper_7 span').remove(); $('#under_cons_social_slider .select_wrapper_7').prepend('<span>twitter</span>'); }
    if($('#under_cons_social_slider_8_slide_name').val() == '' ){  $('#under_cons_social_slider .select_wrapper_8 span').remove(); $('#under_cons_social_slider .select_wrapper_8').prepend('<span>twitter</span>'); }
    if($('#under_cons_social_slider_9_slide_name').val() == '' ){  $('#under_cons_social_slider .select_wrapper_9 span').remove(); $('#under_cons_social_slider .select_wrapper_9').prepend('<span>twitter</span>'); }
    if($('#under_cons_social_slider_10_slide_name').val() == '' ){  $('#under_cons_social_slider .select_wrapper_10 span').remove(); $('#under_cons_social_slider .select_wrapper_10').prepend('<span>twitter</span>'); }
    if($('#under_cons_social_slider_11_slide_name').val() == '' ){  $('#under_cons_social_slider .select_wrapper_11 span').remove(); $('#under_cons_social_slider .select_wrapper_11').prepend('<span>twitter</span>'); }
    if($('#under_cons_social_slider_12_slide_name').val() == '' ){  $('#under_cons_social_slider .select_wrapper_12 span').remove(); $('#under_cons_social_slider .select_wrapper_12').prepend('<span>twitter</span>'); }
    if($('#under_cons_social_slider_13_slide_name').val() == '' ){  $('#under_cons_social_slider .select_wrapper_13 span').remove(); $('#under_cons_social_slider .select_wrapper_13').prepend('<span>twitter</span>'); }
    if($('#under_cons_social_slider_14_slide_name').val() == '' ){  $('#under_cons_social_slider .select_wrapper_14 span').remove(); $('#under_cons_social_slider .select_wrapper_14').prepend('<span>twitter</span>'); }
    if($('#under_cons_social_slider_15_slide_name').val() == '' ){  $('#under_cons_social_slider .select_wrapper_15 span').remove(); $('#under_cons_social_slider .select_wrapper_15').prepend('<span>twitter</span>'); }
    if($('#under_cons_social_slider_16_slide_name').val() == '' ){  $('#under_cons_social_slider .select_wrapper_16 span').remove(); $('#under_cons_social_slider .select_wrapper_16').prepend('<span>twitter</span>'); }
    if($('#under_cons_social_slider_17_slide_name').val() == '' ){  $('#under_cons_social_slider .select_wrapper_17 span').remove(); $('#under_cons_social_slider .select_wrapper_17').prepend('<span>twitter</span>'); }
    if($('#under_cons_social_slider_18_slide_name').val() == '' ){  $('#under_cons_social_slider .select_wrapper_18 span').remove(); $('#under_cons_social_slider .select_wrapper_18').prepend('<span>twitter</span>'); }
    if($('#under_cons_social_slider_19_slide_name').val() == '' ){  $('#under_cons_social_slider .select_wrapper_19 span').remove(); $('#under_cons_social_slider .select_wrapper_19').prepend('<span>twitter</span>'); }
    if($('#under_cons_social_slider_20_slide_name').val() == '' ){  $('#under_cons_social_slider .select_wrapper_20 span').remove(); $('#under_cons_social_slider .select_wrapper_20').prepend('<span>twitter</span>'); }
    if($('#under_cons_social_slider_21_slide_name').val() == '' ){  $('#under_cons_social_slider .select_wrapper_21 span').remove(); $('#under_cons_social_slider .select_wrapper_21').prepend('<span>twitter</span>'); }
    if($('#under_cons_social_slider_22_slide_name').val() == '' ){  $('#under_cons_social_slider .select_wrapper_22 span').remove(); $('#under_cons_social_slider .select_wrapper_22').prepend('<span>twitter</span>'); }
    if($('#under_cons_social_slider_23_slide_name').val() == '' ){  $('#under_cons_social_slider .select_wrapper_23 span').remove(); $('#under_cons_social_slider .select_wrapper_23').prepend('<span>twitter</span>'); }
    if($('#under_cons_social_slider_24_slide_name').val() == '' ){  $('#under_cons_social_slider .select_wrapper_24 span').remove(); $('#under_cons_social_slider .select_wrapper_24').prepend('<span>twitter</span>'); }
    if($('#under_cons_social_slider_25_slide_name').val() == '' ){  $('#under_cons_social_slider .select_wrapper_25 span').remove(); $('#under_cons_social_slider .select_wrapper_25').prepend('<span>twitter</span>'); }
    if($('#under_cons_social_slider_26_slide_name').val() == '' ){  $('#under_cons_social_slider .select_wrapper_26 span').remove(); $('#under_cons_social_slider .select_wrapper_26').prepend('<span>twitter</span>'); }
    if($('#under_cons_social_slider_27_slide_name').val() == '' ){  $('#under_cons_social_slider .select_wrapper_27 span').remove(); $('#under_cons_social_slider .select_wrapper_27').prepend('<span>twitter</span>'); }
    if($('#under_cons_social_slider_28_slide_name').val() == '' ){  $('#under_cons_social_slider .select_wrapper_28 span').remove(); $('#under_cons_social_slider .select_wrapper_28').prepend('<span>twitter</span>'); }
    if($('#under_cons_social_slider_29_slide_name').val() == '' ){  $('#under_cons_social_slider .select_wrapper_29 span').remove(); $('#under_cons_social_slider .select_wrapper_29').prepend('<span>twitter</span>'); }
    if($('#under_cons_social_slider_30_slide_name').val() == '' ){  $('#under_cons_social_slider .select_wrapper_30 span').remove(); $('#under_cons_social_slider .select_wrapper_30').prepend('<span>twitter</span>'); }
    
    
    
    $('#social_slider_header .select_wrapper_1 select').live('change', function(){ $('#social_slider_header_1_slide_name').val(this.value); $('#social_slider_header .select_wrapper_1').parent().prev().find('strong').text( this.value ); });
    $('#social_slider_header .select_wrapper_1 select').val($('#social_slider_header_1_slide_name').val());
    
    $('#social_slider_header .select_wrapper_2 select').live('change', function(){ $('#social_slider_header_2_slide_name').val(this.value); $('#social_slider_header .select_wrapper_2').parent().prev().find('strong').text( this.value ); });
    $('#social_slider_header .select_wrapper_2 select').val($('#social_slider_header_2_slide_name').val());
    
    $('#social_slider_header .select_wrapper_3 select').live('change', function(){ $('#social_slider_header_3_slide_name').val(this.value); $('#social_slider_header .select_wrapper_3').parent().prev().find('strong').text( this.value ); });
    $('#social_slider_header .select_wrapper_3 select').val($('#social_slider_header_3_slide_name').val());
    
    $('#social_slider_header .select_wrapper_4 select').live('change', function(){ $('#social_slider_header_4_slide_name').val(this.value); $('#social_slider_header .select_wrapper_4').parent().prev().find('strong').text( this.value ); });
    $('#social_slider_header .select_wrapper_4 select').val($('#social_slider_header_4_slide_name').val());
    
    $('#social_slider_header .select_wrapper_5 select').live('change', function(){ $('#social_slider_header_5_slide_name').val(this.value); $('#social_slider_header .select_wrapper_5').parent().prev().find('strong').text( this.value ); });
    $('#social_slider_header .select_wrapper_5 select').val($('#social_slider_header_5_slide_name').val());
    
    $('#social_slider_header .select_wrapper_6 select').live('change', function(){ $('#social_slider_header_6_slide_name').val(this.value); $('#social_slider_header .select_wrapper_6').parent().prev().find('strong').text( this.value ); });
    $('#social_slider_header .select_wrapper_6 select').val($('#social_slider_header_6_slide_name').val());
    
    $('#social_slider_header .select_wrapper_7 select').live('change', function(){ $('#social_slider_header_7_slide_name').val(this.value); $('#social_slider_header .select_wrapper_7').parent().prev().find('strong').text( this.value ); });
    $('#social_slider_header .select_wrapper_7 select').val($('#social_slider_header_7_slide_name').val());
    
    $('#social_slider_header .select_wrapper_8 select').live('change', function(){ $('#social_slider_header_8_slide_name').val(this.value); $('#social_slider_header .select_wrapper_8').parent().prev().find('strong').text( this.value ); });
    $('#social_slider_header .select_wrapper_8 select').val($('#social_slider_header_8_slide_name').val());
    
    $('#social_slider_header .select_wrapper_9 select').live('change', function(){ $('#social_slider_header_9_slide_name').val(this.value); $('#social_slider_header .select_wrapper_9').parent().prev().find('strong').text( this.value ); });
    $('#social_slider_header .select_wrapper_9 select').val($('#social_slider_header_9_slide_name').val());
    
    $('#social_slider_header .select_wrapper_10 select').live('change', function(){ $('#social_slider_header_10_slide_name').val(this.value); $('#social_slider_header .select_wrapper_10').parent().prev().find('strong').text( this.value ); });
    $('#social_slider_header .select_wrapper_10 select').val($('#social_slider_header_10_slide_name').val());
    
    $('#social_slider_header .select_wrapper_11 select').live('change', function(){ $('#social_slider_header_11_slide_name').val(this.value); $('#social_slider_header .select_wrapper_11').parent().prev().find('strong').text( this.value ); });
    $('#social_slider_header .select_wrapper_11 select').val($('#social_slider_header_11_slide_name').val());
    
    $('#social_slider_header .select_wrapper_12 select').live('change', function(){ $('#social_slider_header_12_slide_name').val(this.value); $('#social_slider_header .select_wrapper_12').parent().prev().find('strong').text( this.value ); });
    $('#social_slider_header .select_wrapper_12 select').val($('#social_slider_header_12_slide_name').val());
    
    $('#social_slider_header .select_wrapper_13 select').live('change', function(){ $('#social_slider_header_13_slide_name').val(this.value); $('#social_slider_header .select_wrapper_13').parent().prev().find('strong').text( this.value ); });
    $('#social_slider_header .select_wrapper_13 select').val($('#social_slider_header_13_slide_name').val());
    
    $('#social_slider_header .select_wrapper_14 select').live('change', function(){ $('#social_slider_header_14_slide_name').val(this.value); $('#social_slider_header .select_wrapper_14').parent().prev().find('strong').text( this.value ); });
    $('#social_slider_header .select_wrapper_14 select').val($('#social_slider_header_14_slide_name').val());
    
    $('#social_slider_header .select_wrapper_15 select').live('change', function(){ $('#social_slider_header_15_slide_name').val(this.value); $('#social_slider_header .select_wrapper_15').parent().prev().find('strong').text( this.value ); });
    $('#social_slider_header .select_wrapper_15 select').val($('#social_slider_header_15_slide_name').val());
    
    $('#social_slider_header .select_wrapper_16 select').live('change', function(){ $('#social_slider_header_16_slide_name').val(this.value); $('#social_slider_header .select_wrapper_16').parent().prev().find('strong').text( this.value ); });
    $('#social_slider_header .select_wrapper_16 select').val($('#social_slider_header_16_slide_name').val());
    
    $('#social_slider_header .select_wrapper_17 select').live('change', function(){ $('#social_slider_header_17_slide_name').val(this.value); $('#social_slider_header .select_wrapper_17').parent().prev().find('strong').text( this.value ); });
    $('#social_slider_header .select_wrapper_17 select').val($('#social_slider_header_17_slide_name').val());
    
    $('#social_slider_header .select_wrapper_18 select').live('change', function(){ $('#social_slider_header_18_slide_name').val(this.value); $('#social_slider_header .select_wrapper_18').parent().prev().find('strong').text( this.value ); });
    $('#social_slider_header .select_wrapper_18 select').val($('#social_slider_header_18_slide_name').val());
    
    $('#social_slider_header .select_wrapper_19 select').live('change', function(){ $('#social_slider_header_19_slide_name').val(this.value); $('#social_slider_header .select_wrapper_19').parent().prev().find('strong').text( this.value ); });
    $('#social_slider_header .select_wrapper_19 select').val($('#social_slider_header_19_slide_name').val());
    
    $('#social_slider_header .select_wrapper_20 select').live('change', function(){ $('#social_slider_header_20_slide_name').val(this.value); $('#social_slider_header .select_wrapper_20').parent().prev().find('strong').text( this.value ); });
    $('#social_slider_header .select_wrapper_20 select').val($('#social_slider_header_20_slide_name').val());
    
    $('#social_slider_header .select_wrapper_21 select').live('change', function(){ $('#social_slider_header_21_slide_name').val(this.value); $('#social_slider_header .select_wrapper_21').parent().prev().find('strong').text( this.value ); });
    $('#social_slider_header .select_wrapper_21 select').val($('#social_slider_header_21_slide_name').val());
    
    $('#social_slider_header .select_wrapper_22 select').live('change', function(){ $('#social_slider_header_22_slide_name').val(this.value); $('#social_slider_header .select_wrapper_22').parent().prev().find('strong').text( this.value ); });
    $('#social_slider_header .select_wrapper_22 select').val($('#social_slider_header_22_slide_name').val());
    
    $('#social_slider_header .select_wrapper_23 select').live('change', function(){ $('#social_slider_header_23_slide_name').val(this.value); $('#social_slider_header .select_wrapper_23').parent().prev().find('strong').text( this.value ); });
    $('#social_slider_header .select_wrapper_23 select').val($('#social_slider_header_23_slide_name').val());
    
    $('#social_slider_header .select_wrapper_24 select').live('change', function(){ $('#social_slider_header_24_slide_name').val(this.value); $('#social_slider_header .select_wrapper_24').parent().prev().find('strong').text( this.value ); });
    $('#social_slider_header .select_wrapper_24 select').val($('#social_slider_header_24_slide_name').val());
    
    $('#social_slider_header .select_wrapper_25 select').live('change', function(){ $('#social_slider_header_25_slide_name').val(this.value); $('#social_slider_header .select_wrapper_25').parent().prev().find('strong').text( this.value ); });
    $('#social_slider_header .select_wrapper_25 select').val($('#social_slider_header_25_slide_name').val());
    
    $('#social_slider_header .select_wrapper_26 select').live('change', function(){ $('#social_slider_header_26_slide_name').val(this.value); $('#social_slider_header .select_wrapper_26').parent().prev().find('strong').text( this.value ); });
    $('#social_slider_header .select_wrapper_26 select').val($('#social_slider_header_26_slide_name').val());
    
    $('#social_slider_header .select_wrapper_26 select').live('change', function(){ $('#social_slider_header_26_slide_name').val(this.value); $('#social_slider_header .select_wrapper_26').parent().prev().find('strong').text( this.value ); });
    $('#social_slider_header .select_wrapper_26 select').val($('#social_slider_header_26_slide_name').val());
    
    $('#social_slider_header .select_wrapper_27 select').live('change', function(){ $('#social_slider_header_27_slide_name').val(this.value); $('#social_slider_header .select_wrapper_27').parent().prev().find('strong').text( this.value ); });
    $('#social_slider_header .select_wrapper_27 select').val($('#social_slider_header_27_slide_name').val());
    
    $('#social_slider_header .select_wrapper_28 select').live('change', function(){ $('#social_slider_header_28_slide_name').val(this.value); $('#social_slider_header .select_wrapper_28').parent().prev().find('strong').text( this.value ); });
    $('#social_slider_header .select_wrapper_28 select').val($('#social_slider_header_28_slide_name').val());
    
    $('#social_slider_header .select_wrapper_29 select').live('change', function(){ $('#social_slider_header_29_slide_name').val(this.value); $('#social_slider_header .select_wrapper_29').parent().prev().find('strong').text( this.value ); });
    $('#social_slider_header .select_wrapper_29 select').val($('#social_slider_header_29_slide_name').val());
    
    $('#social_slider_header .select_wrapper_30 select').live('change', function(){ $('#social_slider_header_30_slide_name').val(this.value); $('#social_slider_header .select_wrapper_30').parent().prev().find('strong').text( this.value ); });
    $('#social_slider_header .select_wrapper_30 select').val($('#social_slider_header_30_slide_name').val());
    
    
    
    
    $('#social_slider_footer .select_wrapper_1 select').live('change', function(){ $('#social_slider_footer_1_slide_name').val(this.value); $('#social_slider_footer .select_wrapper_1').parent().prev().find('strong').text( this.value ); });
    $('#social_slider_footer .select_wrapper_1 select').val($('#social_slider_footer_1_slide_name').val());
    
    $('#social_slider_footer .select_wrapper_2 select').live('change', function(){ $('#social_slider_footer_2_slide_name').val(this.value); $('#social_slider_footer .select_wrapper_2').parent().prev().find('strong').text( this.value ); });
    $('#social_slider_footer .select_wrapper_2 select').val($('#social_slider_footer_2_slide_name').val());
    
    $('#social_slider_footer .select_wrapper_3 select').live('change', function(){ $('#social_slider_footer_3_slide_name').val(this.value); $('#social_slider_footer .select_wrapper_3').parent().prev().find('strong').text( this.value ); });
    $('#social_slider_footer .select_wrapper_3 select').val($('#social_slider_footer_3_slide_name').val());
    
    $('#social_slider_footer .select_wrapper_4 select').live('change', function(){ $('#social_slider_footer_4_slide_name').val(this.value); $('#social_slider_footer .select_wrapper_4').parent().prev().find('strong').text( this.value ); });
    $('#social_slider_footer .select_wrapper_4 select').val($('#social_slider_footer_4_slide_name').val());
    
    $('#social_slider_footer .select_wrapper_5 select').live('change', function(){ $('#social_slider_footer_5_slide_name').val(this.value); $('#social_slider_footer .select_wrapper_5').parent().prev().find('strong').text( this.value ); });
    $('#social_slider_footer .select_wrapper_5 select').val($('#social_slider_footer_5_slide_name').val());
    
    $('#social_slider_footer .select_wrapper_6 select').live('change', function(){ $('#social_slider_footer_6_slide_name').val(this.value); $('#social_slider_footer .select_wrapper_6').parent().prev().find('strong').text( this.value ); });
    $('#social_slider_footer .select_wrapper_6 select').val($('#social_slider_footer_6_slide_name').val());
    
    $('#social_slider_footer .select_wrapper_7 select').live('change', function(){ $('#social_slider_footer_7_slide_name').val(this.value); $('#social_slider_footer .select_wrapper_7').parent().prev().find('strong').text( this.value ); });
    $('#social_slider_footer .select_wrapper_7 select').val($('#social_slider_footer_7_slide_name').val());
    
    $('#social_slider_footer .select_wrapper_8 select').live('change', function(){ $('#social_slider_footer_8_slide_name').val(this.value); $('#social_slider_footer .select_wrapper_8').parent().prev().find('strong').text( this.value ); });
    $('#social_slider_footer .select_wrapper_8 select').val($('#social_slider_footer_8_slide_name').val());
    
    $('#social_slider_footer .select_wrapper_9 select').live('change', function(){ $('#social_slider_footer_9_slide_name').val(this.value); $('#social_slider_footer .select_wrapper_9').parent().prev().find('strong').text( this.value ); });
    $('#social_slider_footer .select_wrapper_9 select').val($('#social_slider_footer_9_slide_name').val());
    
    $('#social_slider_footer .select_wrapper_10 select').live('change', function(){ $('#social_slider_footer_10_slide_name').val(this.value); $('#social_slider_footer .select_wrapper_10').parent().prev().find('strong').text( this.value ); });
    $('#social_slider_footer .select_wrapper_10 select').val($('#social_slider_footer_10_slide_name').val());
    
    $('#social_slider_footer .select_wrapper_11 select').live('change', function(){ $('#social_slider_footer_11_slide_name').val(this.value); $('#social_slider_footer .select_wrapper_11').parent().prev().find('strong').text( this.value ); });
    $('#social_slider_footer .select_wrapper_11 select').val($('#social_slider_footer_11_slide_name').val());
    
    $('#social_slider_footer .select_wrapper_12 select').live('change', function(){ $('#social_slider_footer_12_slide_name').val(this.value); $('#social_slider_footer .select_wrapper_12').parent().prev().find('strong').text( this.value ); });
    $('#social_slider_footer .select_wrapper_12 select').val($('#social_slider_footer_12_slide_name').val());
    
    $('#social_slider_footer .select_wrapper_13 select').live('change', function(){ $('#social_slider_footer_13_slide_name').val(this.value); $('#social_slider_footer .select_wrapper_13').parent().prev().find('strong').text( this.value ); });
    $('#social_slider_footer .select_wrapper_13 select').val($('#social_slider_footer_13_slide_name').val());
    
    $('#social_slider_footer .select_wrapper_14 select').live('change', function(){ $('#social_slider_footer_14_slide_name').val(this.value); $('#social_slider_footer .select_wrapper_14').parent().prev().find('strong').text( this.value ); });
    $('#social_slider_footer .select_wrapper_14 select').val($('#social_slider_footer_14_slide_name').val());
    
    $('#social_slider_footer .select_wrapper_15 select').live('change', function(){ $('#social_slider_footer_15_slide_name').val(this.value); $('#social_slider_footer .select_wrapper_15').parent().prev().find('strong').text( this.value ); });
    $('#social_slider_footer .select_wrapper_15 select').val($('#social_slider_footer_15_slide_name').val());
    
    $('#social_slider_footer .select_wrapper_16 select').live('change', function(){ $('#social_slider_footer_16_slide_name').val(this.value); $('#social_slider_footer .select_wrapper_16').parent().prev().find('strong').text( this.value ); });
    $('#social_slider_footer .select_wrapper_16 select').val($('#social_slider_footer_16_slide_name').val());
    
    $('#social_slider_footer .select_wrapper_17 select').live('change', function(){ $('#social_slider_footer_17_slide_name').val(this.value); $('#social_slider_footer .select_wrapper_17').parent().prev().find('strong').text( this.value ); });
    $('#social_slider_footer .select_wrapper_17 select').val($('#social_slider_footer_17_slide_name').val());
    
    $('#social_slider_footer .select_wrapper_18 select').live('change', function(){ $('#social_slider_footer_18_slide_name').val(this.value); $('#social_slider_footer .select_wrapper_18').parent().prev().find('strong').text( this.value ); });
    $('#social_slider_footer .select_wrapper_18 select').val($('#social_slider_footer_18_slide_name').val());
    
    $('#social_slider_footer .select_wrapper_19 select').live('change', function(){ $('#social_slider_footer_19_slide_name').val(this.value); $('#social_slider_footer .select_wrapper_19').parent().prev().find('strong').text( this.value ); });
    $('#social_slider_footer .select_wrapper_19 select').val($('#social_slider_footer_19_slide_name').val());
    
    $('#social_slider_footer .select_wrapper_20 select').live('change', function(){ $('#social_slider_footer_20_slide_name').val(this.value); $('#social_slider_footer .select_wrapper_20').parent().prev().find('strong').text( this.value ); });
    $('#social_slider_footer .select_wrapper_20 select').val($('#social_slider_footer_20_slide_name').val());
    
    $('#social_slider_footer .select_wrapper_21 select').live('change', function(){ $('#social_slider_footer_21_slide_name').val(this.value); $('#social_slider_footer .select_wrapper_21').parent().prev().find('strong').text( this.value ); });
    $('#social_slider_footer .select_wrapper_21 select').val($('#social_slider_footer_21_slide_name').val());
    
    $('#social_slider_footer .select_wrapper_22 select').live('change', function(){ $('#social_slider_footer_22_slide_name').val(this.value); $('#social_slider_footer .select_wrapper_22').parent().prev().find('strong').text( this.value ); });
    $('#social_slider_footer .select_wrapper_22 select').val($('#social_slider_footer_22_slide_name').val());
    
    $('#social_slider_footer .select_wrapper_23 select').live('change', function(){ $('#social_slider_footer_23_slide_name').val(this.value); $('#social_slider_footer .select_wrapper_23').parent().prev().find('strong').text( this.value ); });
    $('#social_slider_footer .select_wrapper_23 select').val($('#social_slider_footer_23_slide_name').val());
    
    $('#social_slider_footer .select_wrapper_24 select').live('change', function(){ $('#social_slider_footer_24_slide_name').val(this.value); $('#social_slider_footer .select_wrapper_24').parent().prev().find('strong').text( this.value ); });
    $('#social_slider_footer .select_wrapper_24 select').val($('#social_slider_footer_24_slide_name').val());
    
    $('#social_slider_footer .select_wrapper_25 select').live('change', function(){ $('#social_slider_footer_25_slide_name').val(this.value); $('#social_slider_footer .select_wrapper_25').parent().prev().find('strong').text( this.value ); });
    $('#social_slider_footer .select_wrapper_25 select').val($('#social_slider_footer_25_slide_name').val());
    
    $('#social_slider_footer .select_wrapper_26 select').live('change', function(){ $('#social_slider_footer_26_slide_name').val(this.value); $('#social_slider_footer .select_wrapper_26').parent().prev().find('strong').text( this.value ); });
    $('#social_slider_footer .select_wrapper_26 select').val($('#social_slider_footer_26_slide_name').val());
    
    $('#social_slider_footer .select_wrapper_26 select').live('change', function(){ $('#social_slider_footer_26_slide_name').val(this.value); $('#social_slider_footer .select_wrapper_26').parent().prev().find('strong').text( this.value ); });
    $('#social_slider_footer .select_wrapper_26 select').val($('#social_slider_footer_26_slide_name').val());
    
    $('#social_slider_footer .select_wrapper_27 select').live('change', function(){ $('#social_slider_footer_27_slide_name').val(this.value); $('#social_slider_footer .select_wrapper_27').parent().prev().find('strong').text( this.value ); });
    $('#social_slider_footer .select_wrapper_27 select').val($('#social_slider_footer_27_slide_name').val());
    
    $('#social_slider_footer .select_wrapper_28 select').live('change', function(){ $('#social_slider_footer_28_slide_name').val(this.value); $('#social_slider_footer .select_wrapper_28').parent().prev().find('strong').text( this.value ); });
    $('#social_slider_footer .select_wrapper_28 select').val($('#social_slider_footer_28_slide_name').val());
    
    $('#social_slider_footer .select_wrapper_29 select').live('change', function(){ $('#social_slider_footer_29_slide_name').val(this.value); $('#social_slider_footer .select_wrapper_29').parent().prev().find('strong').text( this.value ); });
    $('#social_slider_footer .select_wrapper_29 select').val($('#social_slider_footer_29_slide_name').val());
    
    $('#social_slider_footer .select_wrapper_30 select').live('change', function(){ $('#social_slider_footer_30_slide_name').val(this.value); $('#social_slider_footer .select_wrapper_30').parent().prev().find('strong').text( this.value ); });
    $('#social_slider_footer .select_wrapper_30 select').val($('#social_slider_footer_30_slide_name').val());
    
    
    
    
    $('#under_cons_social_slider .select_wrapper_1 select').live('change', function(){ $('#under_cons_social_slider_1_slide_name').val(this.value); $('#under_cons_social_slider .select_wrapper_1').parent().prev().find('strong').text( this.value ); });
    $('#under_cons_social_slider .select_wrapper_1 select').val($('#under_cons_social_slider_1_slide_name').val());
    
    $('#under_cons_social_slider .select_wrapper_2 select').live('change', function(){ $('#under_cons_social_slider_2_slide_name').val(this.value); $('#under_cons_social_slider .select_wrapper_2').parent().prev().find('strong').text( this.value ); });
    $('#under_cons_social_slider .select_wrapper_2 select').val($('#under_cons_social_slider_2_slide_name').val());
    
    $('#under_cons_social_slider .select_wrapper_3 select').live('change', function(){ $('#under_cons_social_slider_3_slide_name').val(this.value); $('#under_cons_social_slider .select_wrapper_3').parent().prev().find('strong').text( this.value ); });
    $('#under_cons_social_slider .select_wrapper_3 select').val($('#under_cons_social_slider_3_slide_name').val());
    
    $('#under_cons_social_slider .select_wrapper_4 select').live('change', function(){ $('#under_cons_social_slider_4_slide_name').val(this.value); $('#under_cons_social_slider .select_wrapper_4').parent().prev().find('strong').text( this.value ); });
    $('#under_cons_social_slider .select_wrapper_4 select').val($('#under_cons_social_slider_4_slide_name').val());
    
    $('#under_cons_social_slider .select_wrapper_5 select').live('change', function(){ $('#under_cons_social_slider_5_slide_name').val(this.value); $('#under_cons_social_slider .select_wrapper_5').parent().prev().find('strong').text( this.value ); });
    $('#under_cons_social_slider .select_wrapper_5 select').val($('#under_cons_social_slider_5_slide_name').val());
    
    $('#under_cons_social_slider .select_wrapper_6 select').live('change', function(){ $('#under_cons_social_slider_6_slide_name').val(this.value); $('#under_cons_social_slider .select_wrapper_6').parent().prev().find('strong').text( this.value ); });
    $('#under_cons_social_slider .select_wrapper_6 select').val($('#under_cons_social_slider_6_slide_name').val());
    
    $('#under_cons_social_slider .select_wrapper_7 select').live('change', function(){ $('#under_cons_social_slider_7_slide_name').val(this.value); $('#under_cons_social_slider .select_wrapper_7').parent().prev().find('strong').text( this.value ); });
    $('#under_cons_social_slider .select_wrapper_7 select').val($('#under_cons_social_slider_7_slide_name').val());
    
    $('#under_cons_social_slider .select_wrapper_8 select').live('change', function(){ $('#under_cons_social_slider_8_slide_name').val(this.value); $('#under_cons_social_slider .select_wrapper_8').parent().prev().find('strong').text( this.value ); });
    $('#under_cons_social_slider .select_wrapper_8 select').val($('#under_cons_social_slider_8_slide_name').val());
    
    $('#under_cons_social_slider .select_wrapper_9 select').live('change', function(){ $('#under_cons_social_slider_9_slide_name').val(this.value); $('#under_cons_social_slider .select_wrapper_9').parent().prev().find('strong').text( this.value ); });
    $('#under_cons_social_slider .select_wrapper_9 select').val($('#under_cons_social_slider_9_slide_name').val());
    
    $('#under_cons_social_slider .select_wrapper_10 select').live('change', function(){ $('#under_cons_social_slider_10_slide_name').val(this.value); $('#under_cons_social_slider .select_wrapper_10').parent().prev().find('strong').text( this.value ); });
    $('#under_cons_social_slider .select_wrapper_10 select').val($('#under_cons_social_slider_10_slide_name').val());
    
    $('#under_cons_social_slider .select_wrapper_11 select').live('change', function(){ $('#under_cons_social_slider_11_slide_name').val(this.value); $('#under_cons_social_slider .select_wrapper_11').parent().prev().find('strong').text( this.value ); });
    $('#under_cons_social_slider .select_wrapper_11 select').val($('#under_cons_social_slider_11_slide_name').val());
    
    $('#under_cons_social_slider .select_wrapper_12 select').live('change', function(){ $('#under_cons_social_slider_12_slide_name').val(this.value); $('#under_cons_social_slider .select_wrapper_12').parent().prev().find('strong').text( this.value ); });
    $('#under_cons_social_slider .select_wrapper_12 select').val($('#under_cons_social_slider_12_slide_name').val());
    
    $('#under_cons_social_slider .select_wrapper_13 select').live('change', function(){ $('#under_cons_social_slider_13_slide_name').val(this.value); $('#under_cons_social_slider .select_wrapper_13').parent().prev().find('strong').text( this.value ); });
    $('#under_cons_social_slider .select_wrapper_13 select').val($('#under_cons_social_slider_13_slide_name').val());
    
    $('#under_cons_social_slider .select_wrapper_14 select').live('change', function(){ $('#under_cons_social_slider_14_slide_name').val(this.value); $('#under_cons_social_slider .select_wrapper_14').parent().prev().find('strong').text( this.value ); });
    $('#under_cons_social_slider .select_wrapper_14 select').val($('#under_cons_social_slider_14_slide_name').val());
    
    $('#under_cons_social_slider .select_wrapper_15 select').live('change', function(){ $('#under_cons_social_slider_15_slide_name').val(this.value); $('#under_cons_social_slider .select_wrapper_15').parent().prev().find('strong').text( this.value ); });
    $('#under_cons_social_slider .select_wrapper_15 select').val($('#under_cons_social_slider_15_slide_name').val());
    
    $('#under_cons_social_slider .select_wrapper_16 select').live('change', function(){ $('#under_cons_social_slider_16_slide_name').val(this.value); $('#under_cons_social_slider .select_wrapper_16').parent().prev().find('strong').text( this.value ); });
    $('#under_cons_social_slider .select_wrapper_16 select').val($('#under_cons_social_slider_16_slide_name').val());
    
    $('#under_cons_social_slider .select_wrapper_17 select').live('change', function(){ $('#under_cons_social_slider_17_slide_name').val(this.value); $('#under_cons_social_slider .select_wrapper_17').parent().prev().find('strong').text( this.value ); });
    $('#under_cons_social_slider .select_wrapper_17 select').val($('#under_cons_social_slider_17_slide_name').val());
    
    $('#under_cons_social_slider .select_wrapper_18 select').live('change', function(){ $('#under_cons_social_slider_18_slide_name').val(this.value); $('#under_cons_social_slider .select_wrapper_18').parent().prev().find('strong').text( this.value ); });
    $('#under_cons_social_slider .select_wrapper_18 select').val($('#under_cons_social_slider_18_slide_name').val());
    
    $('#under_cons_social_slider .select_wrapper_19 select').live('change', function(){ $('#under_cons_social_slider_19_slide_name').val(this.value); $('#under_cons_social_slider .select_wrapper_19').parent().prev().find('strong').text( this.value ); });
    $('#under_cons_social_slider .select_wrapper_19 select').val($('#under_cons_social_slider_19_slide_name').val());
    
    $('#under_cons_social_slider .select_wrapper_20 select').live('change', function(){ $('#under_cons_social_slider_20_slide_name').val(this.value); $('#under_cons_social_slider .select_wrapper_20').parent().prev().find('strong').text( this.value ); });
    $('#under_cons_social_slider .select_wrapper_20 select').val($('#under_cons_social_slider_20_slide_name').val());
    
    $('#under_cons_social_slider .select_wrapper_21 select').live('change', function(){ $('#under_cons_social_slider_21_slide_name').val(this.value); $('#under_cons_social_slider .select_wrapper_21').parent().prev().find('strong').text( this.value ); });
    $('#under_cons_social_slider .select_wrapper_21 select').val($('#under_cons_social_slider_21_slide_name').val());
    
    $('#under_cons_social_slider .select_wrapper_22 select').live('change', function(){ $('#under_cons_social_slider_22_slide_name').val(this.value); $('#under_cons_social_slider .select_wrapper_22').parent().prev().find('strong').text( this.value ); });
    $('#under_cons_social_slider .select_wrapper_22 select').val($('#under_cons_social_slider_22_slide_name').val());
    
    $('#under_cons_social_slider .select_wrapper_23 select').live('change', function(){ $('#under_cons_social_slider_23_slide_name').val(this.value); $('#under_cons_social_slider .select_wrapper_23').parent().prev().find('strong').text( this.value ); });
    $('#under_cons_social_slider .select_wrapper_23 select').val($('#under_cons_social_slider_23_slide_name').val());
    
    $('#under_cons_social_slider .select_wrapper_24 select').live('change', function(){ $('#under_cons_social_slider_24_slide_name').val(this.value); $('#under_cons_social_slider .select_wrapper_24').parent().prev().find('strong').text( this.value ); });
    $('#under_cons_social_slider .select_wrapper_24 select').val($('#under_cons_social_slider_24_slide_name').val());
    
    $('#under_cons_social_slider .select_wrapper_25 select').live('change', function(){ $('#under_cons_social_slider_25_slide_name').val(this.value); $('#under_cons_social_slider .select_wrapper_25').parent().prev().find('strong').text( this.value ); });
    $('#under_cons_social_slider .select_wrapper_25 select').val($('#under_cons_social_slider_25_slide_name').val());
    
    $('#under_cons_social_slider .select_wrapper_26 select').live('change', function(){ $('#under_cons_social_slider_26_slide_name').val(this.value); $('#under_cons_social_slider .select_wrapper_26').parent().prev().find('strong').text( this.value ); });
    $('#under_cons_social_slider .select_wrapper_26 select').val($('#under_cons_social_slider_26_slide_name').val());
    
    $('#under_cons_social_slider .select_wrapper_26 select').live('change', function(){ $('#under_cons_social_slider_26_slide_name').val(this.value); $('#under_cons_social_slider .select_wrapper_26').parent().prev().find('strong').text( this.value ); });
    $('#under_cons_social_slider .select_wrapper_26 select').val($('#under_cons_social_slider_26_slide_name').val());
    
    $('#under_cons_social_slider .select_wrapper_27 select').live('change', function(){ $('#under_cons_social_slider_27_slide_name').val(this.value); $('#under_cons_social_slider .select_wrapper_27').parent().prev().find('strong').text( this.value ); });
    $('#under_cons_social_slider .select_wrapper_27 select').val($('#under_cons_social_slider_27_slide_name').val());
    
    $('#under_cons_social_slider .select_wrapper_28 select').live('change', function(){ $('#under_cons_social_slider_28_slide_name').val(this.value); $('#under_cons_social_slider .select_wrapper_28').parent().prev().find('strong').text( this.value ); });
    $('#under_cons_social_slider .select_wrapper_28 select').val($('#under_cons_social_slider_28_slide_name').val());
    
    $('#under_cons_social_slider .select_wrapper_29 select').live('change', function(){ $('#under_cons_social_slider_29_slide_name').val(this.value); $('#under_cons_social_slider .select_wrapper_29').parent().prev().find('strong').text( this.value ); });
    $('#under_cons_social_slider .select_wrapper_29 select').val($('#under_cons_social_slider_29_slide_name').val());
    
    $('#under_cons_social_slider .select_wrapper_30 select').live('change', function(){ $('#under_cons_social_slider_30_slide_name').val(this.value); $('#under_cons_social_slider .select_wrapper_30').parent().prev().find('strong').text( this.value ); });
    $('#under_cons_social_slider .select_wrapper_30 select').val($('#under_cons_social_slider_30_slide_name').val());
    
    
    /* ======================================= End ======================================*/
	
	
	/**	Sorter (Layout Manager) */
	jQuery('.sorter').each( function() {
		var id = jQuery(this).attr('id');
		$('#'+ id).find('ul').sortable({
			items: 'li',
			placeholder: "placeholder",
			connectWith: '.sortlist_' + id,
			opacity: 0.6,
			update: function() {
				$(this).find('.position').each( function() {
				
					var listID = $(this).parent().attr('id');
					var parentID = $(this).parent().parent().attr('id');
					parentID = parentID.replace(id + '_', '')
					var optionID = $(this).parent().parent().parent().attr('id');
					$(this).prop("name", optionID + '[' + parentID + '][' + listID + ']');
					
				});
			}
		});	
	});
	
	
	/**	Ajax Backup & Restore MOD */
	//backup button
	$('#of_backup_button').live('click', function(){
	
		var answer = confirm("Click OK to backup your current saved options.")
		
		if (answer){
	
			var clickedObject = $(this);
			var clickedID = $(this).attr('id');
					
			var nonce = $('#security').val();
		
			var data = {
				action: 'of_ajax_post_action',
				type: 'backup_options',
				security: nonce
			};
						
			$.post(ajaxurl, data, function(response) {
							
				//check nonce
				if(response==-1){ //failed
								
					var fail_popup = $('#of-popup-fail');
					fail_popup.fadeIn();
					window.setTimeout(function(){
						fail_popup.fadeOut();                        
					}, 2000);
				}
							
				else {
							
					var success_popup = $('#of-popup-save');
					success_popup.fadeIn();
					window.setTimeout(function(){
						location.reload();                        
					}, 1000);
				}
							
			});
			
		}
		
	return false;
					
	}); 
	
	//restore button
	$('#of_restore_button').live('click', function(){
	
		var answer = confirm("'Warning: All of your current options will be replaced with the data from your last backup! Proceed?")
		
		if (answer){
	
			var clickedObject = $(this);
			var clickedID = $(this).attr('id');
					
			var nonce = $('#security').val();
		
			var data = {
				action: 'of_ajax_post_action',
				type: 'restore_options',
				security: nonce
			};
						
			$.post(ajaxurl, data, function(response) {
			
				//check nonce
				if(response==-1){ //failed
								
					var fail_popup = $('#of-popup-fail');
					fail_popup.fadeIn();
					window.setTimeout(function(){
						fail_popup.fadeOut();                        
					}, 2000);
				}
							
				else {
							
					var success_popup = $('#of-popup-save');
					success_popup.fadeIn();
					window.setTimeout(function(){
						location.reload();                        
					}, 1000);
				}	
						
			});
	
		}
	
	return false;
					
	});
	
	/**	Ajax Transfer (Import/Export) Option */
	$('#of_import_button').live('click', function(){
	
		var answer = confirm("Click OK to import options.")
		
		if (answer){
	
			var clickedObject = $(this);
			var clickedID = $(this).attr('id');
					
			var nonce = $('#security').val();
			
			var import_data = $('#export_data').val();
		
			var data = {
				action: 'of_ajax_post_action',
				type: 'import_options',
				security: nonce,
				data: import_data
			};
						
			$.post(ajaxurl, data, function(response) {
				var fail_popup = $('#of-popup-fail');
				var success_popup = $('#of-popup-save');
				
				//check nonce
				if(response==-1){ //failed
					fail_popup.fadeIn();
					window.setTimeout(function(){
						fail_popup.fadeOut();                        
					}, 2000);
				}		
				else 
				{
					success_popup.fadeIn();
					window.setTimeout(function(){
						location.reload();                        
					}, 1000);
				}
							
			});
			
		}
		
	return false;
					
	});
	
	/** AJAX Save Options */
	$('#of_save').live('click',function() {
			
		var nonce = $('#security').val();
					
		$('.ajax-loading-img').fadeIn();
		
		//get serialized data from all our option fields			
		var serializedReturn = $('#of_form :input[name][name!="security"][name!="of_reset"]').serialize();

		$('#of_form :input[type=checkbox]').each(function() {     
		    if (!this.checked) {
		        serializedReturn += '&'+this.name+'=0';
		    }
		});
						
		var data = {
			type: 'save',
			action: 'of_ajax_post_action',
			security: nonce,
			data: serializedReturn
		};
					
		$.post(ajaxurl, data, function(response) {
			var success = $('#of-popup-save');
			var fail = $('#of-popup-fail');
			var loading = $('.ajax-loading-img');
			loading.fadeOut();  
						
			if (response==1) {
				success.fadeIn();
			} else { 
				fail.fadeIn();
			}
						
			window.setTimeout(function(){
				success.fadeOut(); 
				fail.fadeOut();				
			}, 2000);
		});
			
	return false; 
					
	});   
	
	
	/* AJAX Options Reset */	
	$('#of_reset').click(function() {
		
		//confirm reset
		var answer = confirm("Click OK to reset. All settings will be lost and replaced with default settings!");
		
		//ajax reset
		if (answer){
			
			var nonce = $('#security').val();
						
			$('.ajax-reset-loading-img').fadeIn();
							
			var data = {
			
				type: 'reset',
				action: 'of_ajax_post_action',
				security: nonce,
			};
						
			$.post(ajaxurl, data, function(response) {
				var success = $('#of-popup-reset');
				var fail = $('#of-popup-fail');
				var loading = $('.ajax-reset-loading-img');
				loading.fadeOut();  
							
				if (response==1)
				{
					success.fadeIn();
					window.setTimeout(function(){
						location.reload();                        
					}, 2000);
				} 
				else 
				{ 
					fail.fadeIn();
					window.setTimeout(function(){
						fail.fadeOut();				
					}, 2000);
				}
							

			});
			
		}
			
	return false;
		
	});


	/**	Tipsy @since v1.3 */
	if (jQuery().tipsy) {
		$('.tooltip, .typography-size, .typography-height, .typography-face, .typography-style, .of-typography-color').tipsy({
			fade: true,
			gravity: 's',
			opacity: 0.7,
		});
	}
	
	
	/**
	  * JQuery UI Slider function
	  * Dependencies 	 : jquery, jquery-ui-slider
	  * Feature added by : Smartik - http://smartik.ws/
	  * Date 			 : 03.17.2013
	  */
	jQuery('.smof_sliderui').each(function() {
		
		var obj   = jQuery(this);
		var sId   = "#" + obj.data('id');
		var val   = parseInt(obj.data('val'));
		var min   = parseInt(obj.data('min'));
		var max   = parseInt(obj.data('max'));
		var step  = parseInt(obj.data('step'));
		
		//slider init
		obj.slider({
			value: val,
			min: min,
			max: max,
			step: step,
			range: "min",
			slide: function( event, ui ) {
				jQuery(sId).val( ui.value );
			}
		});
		
	});


	/**
	  * Switch
	  * Dependencies 	 : jquery
	  * Feature added by : Smartik - http://smartik.ws/
	  * Date 			 : 03.17.2013
	  */
	jQuery(".cb-enable").click(function(){
		var parent = $(this).parents('.switch-options');
		jQuery('.cb-disable',parent).removeClass('selected');
		jQuery(this).addClass('selected');
		jQuery('.main_checkbox',parent).attr('checked', true);
		
		//fold/unfold related options
		var obj = jQuery(this);
		var $fold='.f_'+obj.data('id');
		jQuery($fold).slideDown('normal', "swing");
	});
	jQuery(".cb-disable").click(function(){
		var parent = $(this).parents('.switch-options');
		jQuery('.cb-enable',parent).removeClass('selected');
		jQuery(this).addClass('selected');
		jQuery('.main_checkbox',parent).attr('checked', false);
		
		//fold/unfold related options
		var obj = jQuery(this);
		var $fold='.f_'+obj.data('id');
		jQuery($fold).slideUp('normal', "swing");
	});
	//disable text select(for modern chrome, safari and firefox is done via CSS)
	if (($.browser.msie && $.browser.version < 10) || $.browser.opera) { 
		$('.cb-enable span, .cb-disable span').find().attr('unselectable', 'on');
	}
	
	
	/**
	  * Google Fonts
	  * Dependencies 	 : google.com, jquery
	  * Feature added by : Smartik - http://smartik.ws/
	  * Date 			 : 03.17.2013
	  */
	function GoogleFontSelect( slctr, mainID ){
		
		var _selected = $(slctr).val(); 						//get current value - selected and saved
		var _linkclass = 'style_link_'+ mainID;
		var _previewer = mainID +'_ggf_previewer';
		
		if( _selected ){ //if var exists and isset

			$('.'+ _previewer ).fadeIn();
			
			//Check if selected is not equal with "Select a font" and execute the script.
			if ( _selected !== 'none' && _selected !== 'Select a font' ) {
				
				//remove other elements crested in <head>
				$( '.'+ _linkclass ).remove();
				
				//replace spaces with "+" sign
				var the_font = _selected.replace(/\s+/g, '+');
				
				//add reference to google font family
				$('head').append('<link href="http://fonts.googleapis.com/css?family='+ the_font +'" rel="stylesheet" type="text/css" class="'+ _linkclass +'">');
				
				//show in the preview box the font
				$('.'+ _previewer ).css('font-family', _selected +', sans-serif' );
				
			}else{
				
				//if selected is not a font remove style "font-family" at preview box
				$('.'+ _previewer ).css('font-family', '' );
				$('.'+ _previewer ).fadeOut();
				
			}
		
		}
	
	}
	
	//init for each element
	jQuery( '.google_font_select' ).each(function(){ 
		var mainID = jQuery(this).attr('id');
		GoogleFontSelect( this, mainID );
	});
	
	//init when value is changed
	jQuery( '.google_font_select' ).change(function(){ 
		var mainID = jQuery(this).attr('id');
		GoogleFontSelect( this, mainID );
	});


	/**
	  * Media Uploader
	  * Dependencies 	 : jquery, wp media uploader
	  * Feature added by : Smartik - http://smartik.ws/
	  * Date 			 : 05.28.2013
	  */
	function optionsframework_add_file(event, selector) {
	
		var upload = $(".uploaded-file"), frame;
		var $el = $(this);

		event.preventDefault();

		// If the media frame already exists, reopen it.
		if ( frame ) {
			frame.open();
			return;
		}

		// Create the media frame.
		frame = wp.media({
			// Set the title of the modal.
			title: $el.data('choose'),

			// Customize the submit button.
			button: {
				// Set the text of the button.
				text: $el.data('update'),
				// Tell the button not to close the modal, since we're
				// going to refresh the page when the image is selected.
				close: false
			}
		});

		// When an image is selected, run a callback.
		frame.on( 'select', function() {
			// Grab the selected attachment.
			var attachment = frame.state().get('selection').first();
			frame.close();
			selector.find('.upload').val(attachment.attributes.url);
			if ( attachment.attributes.type == 'image' ) {
				selector.find('.screenshot').empty().hide().append('<img class="of-option-image" src="' + attachment.attributes.url + '">').slideDown('fast');
			}
			selector.find('.media_upload_button').unbind();
			selector.find('.remove-image').show().removeClass('hide');//show "Remove" button
			selector.find('.of-background-properties').slideDown();
			optionsframework_file_bindings();
		});

		// Finally, open the modal.
		frame.open();
	}
    
	function optionsframework_remove_file(selector) {
		selector.find('.remove-image').hide().addClass('hide');//hide "Remove" button
		selector.find('.upload').val('');
		selector.find('.of-background-properties').hide();
		selector.find('.screenshot').slideUp();
		selector.find('.remove-file').unbind();
		// We don't display the upload button if .upload-notice is present
		// This means the user doesn't have the WordPress 3.5 Media Library Support
		if ( $('.section-upload .upload-notice').length > 0 ) {
			$('.media_upload_button').remove();
		}
		optionsframework_file_bindings();
	}
	
	function optionsframework_file_bindings() {
		$('.remove-image, .remove-file').on('click', function() {
			optionsframework_remove_file( $(this).parents('.section-upload, .section-media, .slide_body') );
        });
        
        $('.media_upload_button').unbind('click').click( function( event ) {
        	optionsframework_add_file(event, $(this).parents('.section-upload, .section-media, .slide_body'));
        });
    }
    
    optionsframework_file_bindings();

	
	

}); //end doc ready
