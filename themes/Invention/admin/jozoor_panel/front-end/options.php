<div class="wrap" id="of_container">

	<div id="of-popup-save" class="of-save-popup">
		<div class="of-save-save">Options Updated</div>
	</div>
	
	<div id="of-popup-reset" class="of-save-popup">
		<div class="of-save-reset">Options Reset</div>
	</div>
	
	<div id="of-popup-fail" class="of-save-popup">
		<div class="of-save-fail">Error!</div>
	</div>
	
	<span style="display: none;" id="hooks"><?php echo json_encode(of_get_header_classes_array()); ?></span>
	<input type="hidden" id="reset" value="<?php if(isset($_REQUEST['reset'])) echo $_REQUEST['reset']; ?>" />
	<input type="hidden" id="security" name="security" value="<?php echo wp_create_nonce('of_ajax_nonce'); ?>" />

	<form id="of_form" method="post" action="<?php echo esc_attr( $_SERVER['REQUEST_URI'] ) ?>" enctype="multipart/form-data" >
	
		<div id="header">
		
			<div class="logo">
		      <a href="http://jozoor.com" target="_blank" >
              <img src="<?php echo ADMIN_DIR; ?>assets/images/jozoor.png" />
              </a>
			</div>
            
            <ul class="links" id="nav">
              <li><a href="http://themes.jozoor.com/wp/invention" target="_blank" ><span class="home"></span> <?php _e("View Demo", "jozoorthemes"); ?></a></li>
              <li><a href="http://themes.jozoor.com/wp/invention/docs" target="_blank" ><span class="docs"></span> <?php _e("Documentation", "jozoorthemes"); ?></a></li>
              <li><a href="http://support.jozoor.com/categories/invention-wordpress" target="_blank" class="active"><span class="support"></span><?php _e("Support", "jozoorthemes"); ?></a></li>
            </ul>
		
			<div id="js-warning"><?php _e("Warning- This options panel will not work properly without javascript", "jozoorthemes"); ?>!</div>
			
			<div class="clear"></div>
		
    	</div>

		<div id="info_bar">
		
			
				<div id="expand_options" class="expand" title="<?php _e("Expand Options", "jozoorthemes"); ?>"><?php _e("Expand", "jozoorthemes"); ?></div>
			
            
            <h2><img src="<?php echo ADMIN_DIR; ?>assets/images/theme-icon.png" />
            <?php echo THEMENAME; ?> - <?php _e("Theme", "jozoorthemes"); ?> <span><?php echo ('v'. THEMEVERSION); ?></span> <?php if (function_exists('new_updates_url')) { new_updates_url(); } ?></h2>
				
						
			<img style="display:none" src="<?php echo ADMIN_DIR; ?>assets/images/loading-bottom.gif" class="ajax-loading-img ajax-loading-img-bottom" alt="Working..." />

			<button id="of_save" type="button">
				<?php _e("Save Changes", "jozoorthemes"); ?>
			</button>
			
		</div><!--.info_bar--> 		
		
		<div id="main">
		
			<div id="of-nav">
				<ul>
				  <?php echo $options_machine->Menu ?>
				</ul>
			</div>

			<div id="content">
		  		<?php echo $options_machine->Inputs /* Settings */ ?>
		  	</div>
		  	
			<div class="clear"></div>
			
		</div>
		
		<div class="save_bar"> 
		
			<img style="display:none" src="<?php echo ADMIN_DIR; ?>assets/images/loading-bottom.gif" class="ajax-loading-img ajax-loading-img-bottom" alt="Working..." />
			<button id ="of_save" type="button"><?php _e("Save Changes", "jozoorthemes"); ?></button>			
			<button id ="of_reset" type="button" class="button submit-button reset-button" ><?php _e("Reset Options", "jozoorthemes"); ?></button>
			<img style="display:none" src="<?php echo ADMIN_DIR; ?>assets/images/loading-bottom.gif" class="ajax-reset-loading-img ajax-loading-img-bottom" alt="Working..." />
			
		</div><!--.save_bar-->  
 
	</form>
	
	<div style="clear:both;"></div>

</div><!--wrap-->

<script>

// add js class to html tag
jQuery('html').addClass('js');

// Responsive Navigation Menu by SelectNav
jQuery(document).ready(function () {
  selectnav('nav', {
  label: '- Navigation Menu - ',
  nested: true,
  indent: '-'
});
});

</script>