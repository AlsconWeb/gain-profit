<?php

add_action('init','of_options');

if (!function_exists('of_options'))
{
	function of_options()
	{
		//Access the WordPress Categories via an Array
		$of_categories 		= array();  
		$of_categories_obj 	= get_categories('hide_empty=0');
		foreach ($of_categories_obj as $of_cat) {
		    $of_categories[$of_cat->cat_ID] = $of_cat->cat_name;}
		$categories_tmp 	= array_unshift($of_categories, __("Select category", "jozoorthemes") );    
	       
		//Access the WordPress Pages via an Array
		$of_pages 			= array();
		$of_pages_obj 		= get_pages('sort_column=post_parent,menu_order');    
		foreach ($of_pages_obj as $of_page) {
		    $of_pages[$of_page->ID] = $of_page->post_name; }
		$of_pages_tmp 		= array_unshift($of_pages, __("Select Page Name", "jozoorthemes") );       
	
		//Testing 
		$of_options_select 	= array("one","two","three","four","five"); 
		$of_options_radio 	= array("one" => "One","two" => "Two","three" => "Three","four" => "Four","five" => "Five");
		
		//Sample Homepage blocks for the layout manager (sorter)
		$of_options_homepage_blocks = array
		(   
            "enabled" => array (
				"placebo" 		=> "placebo", //REQUIRED!
                "block_one"		=> "Block One",
				"block_two"		=> "Block Two",
				"block_three"	=> "Block Three",
			),
			"disabled" => array (
				"placebo" 		=> "placebo", //REQUIRED!
				"block_four"	=> "Block Four",
                "block_five"	=> "Block Five",
                "block_six"	    => "Block Six",
			), 
		);


		//Stylesheets Reader
		$alt_stylesheet_path = LAYOUT_PATH;
		$alt_stylesheets = array();
		
		if ( is_dir($alt_stylesheet_path) ) 
		{
		    if ($alt_stylesheet_dir = opendir($alt_stylesheet_path) ) 
		    { 
		        while ( ($alt_stylesheet_file = readdir($alt_stylesheet_dir)) !== false ) 
		        {
		            if(stristr($alt_stylesheet_file, ".css") !== false)
		            {
		                $alt_stylesheets[] = $alt_stylesheet_file;
		            }
		        }    
		    }
		}
        
        //select options 
		$of_options_select_slider1_animation = array("fade","slide"); 
        $of_options_select_slider2_animation = array("center","sides"); 
		$of_options_layout_page_style = array("Right Sidebar","Left Sidebar","Full Width"); 
        $of_options_blog_style = array("Blog1","Blog2","Blog3");
        $of_options_sidebar_type = array("Default Sidebar","Sidebar2","Sidebar3","Sidebar4","Sidebar5");
        $of_options_bg_repeat = array("repeat","repeat-x","repeat-y","fixed","no-repeat");
        $of_options_slider_type_default_home = array("default_slider","slider2");
        $of_options_header_layout3_right_content = array("search","custom content");
        $of_options_projects_category_layout_page_style = array("4 Columns","3 Columns","2 Columns");


		//Background Images Reader
		$bg_images_path = get_stylesheet_directory(). '/css/bg/'; // change this to where you store your bg images
		$bg_images_url = get_template_directory_uri().'/css/bg/'; // change this to where you store your bg images
		$bg_images = array();
		
		if ( is_dir($bg_images_path) ) {
		    if ($bg_images_dir = opendir($bg_images_path) ) { 
		        while ( ($bg_images_file = readdir($bg_images_dir)) !== false ) {
		            if(stristr($bg_images_file, ".png") !== false || stristr($bg_images_file, ".jpg") !== false) {
		            	natsort($bg_images); //Sorts the array into a natural order
		                $bg_images[] = $bg_images_url . $bg_images_file;
		            }
		        }    
		    }
		}
		

		/*-----------------------------------------------------------------------------------*/
		/* TO DO: Add options/functions that use these */
		/*-----------------------------------------------------------------------------------*/
		
		//More Options
		$uploads_arr 		= wp_upload_dir();
		$all_uploads_path 	= $uploads_arr['path'];
		$all_uploads 		= get_option('of_uploads');
		$other_entries 		= array("Select a number:","1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19");
		$body_repeat 		= array("no-repeat","repeat-x","repeat-y","repeat");
		$body_pos 			= array("top left","top center","top right","center left","center center","center right","bottom left","bottom center","bottom right");
		
		// Image Alignment radio box
		$of_options_thumb_align = array("alignleft" => "Left","alignright" => "Right","aligncenter" => "Center"); 
		
		// Image Links to Options
		$of_options_image_link_to = array("image" => "The Image","post" => "The Post"); 
        
        // for under construction timer
        $year2 = date("Y");
        $year = date("Y")+1; 
        $month = 2/*date("n")*/; 
        $day = date("j"); 
        $hour = date("G"); 
        $minute = date("i"); 
        
        // for rss link
        $rss_link = get_bloginfo('rss2_url') ;
        
        // home link for 404 page
        $home_link = home_url() ;

        // Fetch the Google Web Fonts API list
        $google_font_list_jozoor = array("none" => "Select a font",//please, always use this key: "none"
                          "ABeeZee" => "ABeeZee","Abel" => "Abel","Abril Fatface" => "Abril Fatface","Aclonica" => "Aclonica","Acme" => "Acme","Actor" => "Actor","Adamina" => "Adamina","Advent Pro" => "Advent Pro","Aguafina Script" => "Aguafina Script","Akronim" => "Akronim","Aladin" => "Aladin","Aldrich" => "Aldrich","Alef" => "Alef","Alegreya" => "Alegreya","Alegreya SC" => "Alegreya SC","Alex Brush" => "Alex Brush","Alfa Slab One" => "Alfa Slab One","Alice" => "Alice","Alike" => "Alike","Alike Angular" => "Alike Angular","Allan" => "Allan","Allerta" => "Allerta","Allerta Stencil" => "Allerta Stencil","Allura" => "Allura","Almendra" => "Almendra","Almendra Display" => "Almendra Display","Almendra SC" => "Almendra SC","Amarante" => "Amarante","Amaranth" => "Amaranth","Amatic SC" => "Amatic SC","Amethysta" => "Amethysta","Anaheim" => "Anaheim","Andada" => "Andada","Andika" => "Andika","Angkor" => "Angkor","Annie Use Your Telescope" => "Annie Use Your Telescope","Anonymous Pro" => "Anonymous Pro","Antic" => "Antic","Antic Didone" => "Antic Didone","Antic Slab" => "Antic Slab","Anton" => "Anton","Arapey" => "Arapey","Arbutus" => "Arbutus","Arbutus Slab" => "Arbutus Slab","Architects Daughter" => "Architects Daughter","Archivo Black" => "Archivo Black","Archivo Narrow" => "Archivo Narrow","Arimo" => "Arimo","Arizonia" => "Arizonia","Armata" => "Armata","Artifika" => "Artifika","Arvo" => "Arvo","Asap" => "Asap","Asset" => "Asset","Astloch" => "Astloch","Asul" => "Asul","Atomic Age" => "Atomic Age","Aubrey" => "Aubrey","Audiowide" => "Audiowide","Autour One" => "Autour One","Average" => "Average","Average Sans" => "Average Sans","Averia Gruesa Libre" => "Averia Gruesa Libre","Averia Libre" => "Averia Libre","Averia Sans Libre" => "Averia Sans Libre","Averia Serif Libre" => "Averia Serif Libre","Bad Script" => "Bad Script","Balthazar" => "Balthazar","Bangers" => "Bangers","Basic" => "Basic","Battambang" => "Battambang","Baumans" => "Baumans","Bayon" => "Bayon","Belgrano" => "Belgrano","Belleza" => "Belleza","BenchNine" => "BenchNine","Bentham" => "Bentham","Berkshire Swash" => "Berkshire Swash","Bevan" => "Bevan","Bigelow Rules" => "Bigelow Rules","Bigshot One" => "Bigshot One","Bilbo" => "Bilbo","Bilbo Swash Caps" => "Bilbo Swash Caps","Bitter" => "Bitter","Black Ops One" => "Black Ops One","Bokor" => "Bokor","Bonbon" => "Bonbon","Boogaloo" => "Boogaloo","Bowlby One" => "Bowlby One","Bowlby One SC" => "Bowlby One SC","Brawler" => "Brawler","Bree Serif" => "Bree Serif","Bubblegum Sans" => "Bubblegum Sans","Bubbler One" => "Bubbler One","Buda" => "Buda","Buenard" => "Buenard","Butcherman" => "Butcherman","Butterfly Kids" => "Butterfly Kids","Cabin" => "Cabin","Cabin Condensed" => "Cabin Condensed","Cabin Sketch" => "Cabin Sketch","Caesar Dressing" => "Caesar Dressing","Cagliostro" => "Cagliostro","Calligraffitti" => "Calligraffitti","Cambo" => "Cambo","Candal" => "Candal","Cantarell" => "Cantarell","Cantata One" => "Cantata One","Cantora One" => "Cantora One","Capriola" => "Capriola","Cardo" => "Cardo","Carme" => "Carme","Carrois Gothic" => "Carrois Gothic","Carrois Gothic SC" => "Carrois Gothic SC","Carter One" => "Carter One","Caudex" => "Caudex","Cedarville Cursive" => "Cedarville Cursive","Ceviche One" => "Ceviche One","Changa One" => "Changa One","Chango" => "Chango","Chau Philomene One" => "Chau Philomene One","Chela One" => "Chela One","Chelsea Market" => "Chelsea Market","Chenla" => "Chenla","Cherry Cream Soda" => "Cherry Cream Soda","Cherry Swash" => "Cherry Swash","Chewy" => "Chewy","Chicle" => "Chicle","Chivo" => "Chivo","Cinzel" => "Cinzel","Cinzel Decorative" => "Cinzel Decorative","Clicker Script" => "Clicker Script","Coda" => "Coda","Coda Caption" => "Coda Caption","Codystar" => "Codystar","Combo" => "Combo","Comfortaa" => "Comfortaa","Coming Soon" => "Coming Soon","Concert One" => "Concert One","Condiment" => "Condiment","Content" => "Content","Contrail One" => "Contrail One","Convergence" => "Convergence","Cookie" => "Cookie","Copse" => "Copse","Corben" => "Corben","Courgette" => "Courgette","Cousine" => "Cousine","Coustard" => "Coustard","Covered By Your Grace" => "Covered By Your Grace","Crafty Girls" => "Crafty Girls","Creepster" => "Creepster","Crete Round" => "Crete Round","Crimson Text" => "Crimson Text","Croissant One" => "Croissant One","Crushed" => "Crushed","Cuprum" => "Cuprum","Cutive" => "Cutive","Cutive Mono" => "Cutive Mono","Damion" => "Damion","Dancing Script" => "Dancing Script","Dangrek" => "Dangrek","Dawning of a New Day" => "Dawning of a New Day","Days One" => "Days One","Delius" => "Delius","Delius Swash Caps" => "Delius Swash Caps","Delius Unicase" => "Delius Unicase","Della Respira" => "Della Respira","Denk One" => "Denk One","Devonshire" => "Devonshire","Didact Gothic" => "Didact Gothic","Diplomata" => "Diplomata","Diplomata SC" => "Diplomata SC","Domine" => "Domine","Donegal One" => "Donegal One","Doppio One" => "Doppio One","Dorsa" => "Dorsa","Dosis" => "Dosis","Dr Sugiyama" => "Dr Sugiyama","Droid Sans" => "Droid Sans","Droid Sans Mono" => "Droid Sans Mono","Droid Serif" => "Droid Serif","Duru Sans" => "Duru Sans","Dynalight" => "Dynalight","EB Garamond" => "EB Garamond","Eagle Lake" => "Eagle Lake","Eater" => "Eater","Economica" => "Economica","Electrolize" => "Electrolize","Elsie" => "Elsie","Elsie Swash Caps" => "Elsie Swash Caps","Emblema One" => "Emblema One","Emilys Candy" => "Emilys Candy","Engagement" => "Engagement","Englebert" => "Englebert","Enriqueta" => "Enriqueta","Erica One" => "Erica One","Esteban" => "Esteban","Euphoria Script" => "Euphoria Script","Ewert" => "Ewert","Exo" => "Exo","Expletus Sans" => "Expletus Sans","Fanwood Text" => "Fanwood Text","Fascinate" => "Fascinate","Fascinate Inline" => "Fascinate Inline","Faster One" => "Faster One","Fasthand" => "Fasthand","Fauna One" => "Fauna One","Federant" => "Federant","Federo" => "Federo","Felipa" => "Felipa","Fenix" => "Fenix","Finger Paint" => "Finger Paint","Fjalla One" => "Fjalla One","Fjord One" => "Fjord One","Flamenco" => "Flamenco","Flavors" => "Flavors","Fondamento" => "Fondamento","Fontdiner Swanky" => "Fontdiner Swanky","Forum" => "Forum","Francois One" => "Francois One","Freckle Face" => "Freckle Face","Fredericka the Great" => "Fredericka the Great","Fredoka One" => "Fredoka One","Freehand" => "Freehand","Fresca" => "Fresca","Frijole" => "Frijole","Fruktur" => "Fruktur","Fugaz One" => "Fugaz One","GFS Didot" => "GFS Didot","GFS Neohellenic" => "GFS Neohellenic","Gabriela" => "Gabriela","Gafata" => "Gafata","Galdeano" => "Galdeano","Galindo" => "Galindo","Gentium Basic" => "Gentium Basic","Gentium Book Basic" => "Gentium Book Basic","Geo" => "Geo","Geostar" => "Geostar","Geostar Fill" => "Geostar Fill","Germania One" => "Germania One","Gilda Display" => "Gilda Display","Give You Glory" => "Give You Glory","Glass Antiqua" => "Glass Antiqua","Glegoo" => "Glegoo","Gloria Hallelujah" => "Gloria Hallelujah","Goblin One" => "Goblin One","Gochi Hand" => "Gochi Hand","Gorditas" => "Gorditas","Goudy Bookletter 1911" => "Goudy Bookletter 1911","Graduate" => "Graduate","Grand Hotel" => "Grand Hotel","Gravitas One" => "Gravitas One","Great Vibes" => "Great Vibes","Griffy" => "Griffy","Gruppo" => "Gruppo","Gudea" => "Gudea","Habibi" => "Habibi","Hammersmith One" => "Hammersmith One","Hanalei" => "Hanalei","Hanalei Fill" => "Hanalei Fill","Handlee" => "Handlee","Hanuman" => "Hanuman","Happy Monkey" => "Happy Monkey","Headland One" => "Headland One","Henny Penny" => "Henny Penny","Herr Von Muellerhoff" => "Herr Von Muellerhoff","Holtwood One SC" => "Holtwood One SC","Homemade Apple" => "Homemade Apple","Homenaje" => "Homenaje","IM Fell DW Pica" => "IM Fell DW Pica","IM Fell DW Pica SC" => "IM Fell DW Pica SC","IM Fell Double Pica" => "IM Fell Double Pica","IM Fell Double Pica SC" => "IM Fell Double Pica SC","IM Fell English" => "IM Fell English","IM Fell English SC" => "IM Fell English SC","IM Fell French Canon" => "IM Fell French Canon","IM Fell French Canon SC" => "IM Fell French Canon SC","IM Fell Great Primer" => "IM Fell Great Primer","IM Fell Great Primer SC" => "IM Fell Great Primer SC","Iceberg" => "Iceberg","Iceland" => "Iceland","Imprima" => "Imprima","Inconsolata" => "Inconsolata","Inder" => "Inder","Indie Flower" => "Indie Flower","Inika" => "Inika","Irish Grover" => "Irish Grover","Istok Web" => "Istok Web","Italiana" => "Italiana","Italianno" => "Italianno","Jacques Francois" => "Jacques Francois","Jacques Francois Shadow" => "Jacques Francois Shadow","Jim Nightshade" => "Jim Nightshade","Jockey One" => "Jockey One","Jolly Lodger" => "Jolly Lodger","Josefin Sans" => "Josefin Sans","Josefin Slab" => "Josefin Slab","Joti One" => "Joti One","Judson" => "Judson","Julee" => "Julee","Julius Sans One" => "Julius Sans One","Junge" => "Junge","Jura" => "Jura","Just Another Hand" => "Just Another Hand","Just Me Again Down Here" => "Just Me Again Down Here","Kameron" => "Kameron","Karla" => "Karla","Kaushan Script" => "Kaushan Script","Kavoon" => "Kavoon","Keania One" => "Keania One","Kelly Slab" => "Kelly Slab","Kenia" => "Kenia","Khmer" => "Khmer","Kite One" => "Kite One","Knewave" => "Knewave","Kotta One" => "Kotta One","Koulen" => "Koulen","Kranky" => "Kranky","Kreon" => "Kreon","Kristi" => "Kristi","Krona One" => "Krona One","La Belle Aurore" => "La Belle Aurore","Lancelot" => "Lancelot","Lato" => "Lato","League Script" => "League Script","Leckerli One" => "Leckerli One","Ledger" => "Ledger","Lekton" => "Lekton","Lemon" => "Lemon","Libre Baskerville" => "Libre Baskerville","Life Savers" => "Life Savers","Lilita One" => "Lilita One","Lily Script One" => "Lily Script One","Limelight" => "Limelight","Linden Hill" => "Linden Hill","Lobster" => "Lobster","Lobster Two" => "Lobster Two","Londrina Outline" => "Londrina Outline","Londrina Shadow" => "Londrina Shadow","Londrina Sketch" => "Londrina Sketch","Londrina Solid" => "Londrina Solid","Lora" => "Lora","Love Ya Like A Sister" => "Love Ya Like A Sister","Loved by the King" => "Loved by the King","Lovers Quarrel" => "Lovers Quarrel","Luckiest Guy" => "Luckiest Guy","Lusitana" => "Lusitana","Lustria" => "Lustria","Macondo" => "Macondo","Macondo Swash Caps" => "Macondo Swash Caps","Magra" => "Magra","Maiden Orange" => "Maiden Orange","Mako" => "Mako","Marcellus" => "Marcellus","Marcellus SC" => "Marcellus SC","Marck Script" => "Marck Script","Margarine" => "Margarine","Marko One" => "Marko One","Marmelad" => "Marmelad","Marvel" => "Marvel","Mate" => "Mate","Mate SC" => "Mate SC","Maven Pro" => "Maven Pro","McLaren" => "McLaren","Meddon" => "Meddon","MedievalSharp" => "MedievalSharp","Medula One" => "Medula One","Megrim" => "Megrim","Meie Script" => "Meie Script","Merienda" => "Merienda","Merienda One" => "Merienda One","Merriweather" => "Merriweather","Merriweather Sans" => "Merriweather Sans","Metal" => "Metal","Metal Mania" => "Metal Mania","Metamorphous" => "Metamorphous","Metrophobic" => "Metrophobic","Michroma" => "Michroma","Milonga" => "Milonga","Miltonian" => "Miltonian","Miltonian Tattoo" => "Miltonian Tattoo","Miniver" => "Miniver","Miss Fajardose" => "Miss Fajardose","Modern Antiqua" => "Modern Antiqua","Molengo" => "Molengo","Molle" => "Molle","Monda" => "Monda","Monofett" => "Monofett","Monoton" => "Monoton","Monsieur La Doulaise" => "Monsieur La Doulaise","Montaga" => "Montaga","Montez" => "Montez","Montserrat" => "Montserrat","Montserrat Alternates" => "Montserrat Alternates","Montserrat Subrayada" => "Montserrat Subrayada","Moul" => "Moul","Moulpali" => "Moulpali","Mountains of Christmas" => "Mountains of Christmas","Mouse Memoirs" => "Mouse Memoirs","Mr Bedfort" => "Mr Bedfort","Mr Dafoe" => "Mr Dafoe","Mr De Haviland" => "Mr De Haviland","Mrs Saint Delafield" => "Mrs Saint Delafield","Mrs Sheppards" => "Mrs Sheppards","Muli" => "Muli","Mystery Quest" => "Mystery Quest","Neucha" => "Neucha","Neuton" => "Neuton","New Rocker" => "New Rocker","News Cycle" => "News Cycle","Niconne" => "Niconne","Nixie One" => "Nixie One","Nobile" => "Nobile","Nokora" => "Nokora","Norican" => "Norican","Nosifer" => "Nosifer","Nothing You Could Do" => "Nothing You Could Do","Noticia Text" => "Noticia Text","Noto Sans" => "Noto Sans","Noto Serif" => "Noto Serif","Nova Cut" => "Nova Cut","Nova Flat" => "Nova Flat","Nova Mono" => "Nova Mono","Nova Oval" => "Nova Oval","Nova Round" => "Nova Round","Nova Script" => "Nova Script","Nova Slim" => "Nova Slim","Nova Square" => "Nova Square","Numans" => "Numans","Nunito" => "Nunito","Odor Mean Chey" => "Odor Mean Chey","Offside" => "Offside","Old Standard TT" => "Old Standard TT","Oldenburg" => "Oldenburg","Oleo Script" => "Oleo Script","Oleo Script Swash Caps" => "Oleo Script Swash Caps","Open Sans" => "Open Sans","Open Sans Condensed" => "Open Sans Condensed","Oranienbaum" => "Oranienbaum","Orbitron" => "Orbitron","Oregano" => "Oregano","Orienta" => "Orienta","Original Surfer" => "Original Surfer","Oswald" => "Oswald","Over the Rainbow" => "Over the Rainbow","Overlock" => "Overlock","Overlock SC" => "Overlock SC","Ovo" => "Ovo","Oxygen" => "Oxygen","Oxygen Mono" => "Oxygen Mono","PT Mono" => "PT Mono","PT Sans" => "PT Sans","PT Sans Caption" => "PT Sans Caption","PT Sans Narrow" => "PT Sans Narrow","PT Serif" => "PT Serif","PT Serif Caption" => "PT Serif Caption","Pacifico" => "Pacifico","Paprika" => "Paprika","Parisienne" => "Parisienne","Passero One" => "Passero One","Passion One" => "Passion One","Pathway Gothic One" => "Pathway Gothic One","Patrick Hand" => "Patrick Hand","Patrick Hand SC" => "Patrick Hand SC","Patua One" => "Patua One","Paytone One" => "Paytone One","Peralta" => "Peralta","Permanent Marker" => "Permanent Marker","Petit Formal Script" => "Petit Formal Script","Petrona" => "Petrona","Philosopher" => "Philosopher","Piedra" => "Piedra","Pinyon Script" => "Pinyon Script","Pirata One" => "Pirata One","Plaster" => "Plaster","Play" => "Play","Playball" => "Playball","Playfair Display" => "Playfair Display","Playfair Display SC" => "Playfair Display SC","Podkova" => "Podkova","Poiret One" => "Poiret One","Poller One" => "Poller One","Poly" => "Poly","Pompiere" => "Pompiere","Pontano Sans" => "Pontano Sans","Port Lligat Sans" => "Port Lligat Sans","Port Lligat Slab" => "Port Lligat Slab","Prata" => "Prata","Preahvihear" => "Preahvihear","Press Start 2P" => "Press Start 2P","Princess Sofia" => "Princess Sofia","Prociono" => "Prociono","Prosto One" => "Prosto One","Puritan" => "Puritan","Purple Purse" => "Purple Purse","Quando" => "Quando","Quantico" => "Quantico","Quattrocento" => "Quattrocento","Quattrocento Sans" => "Quattrocento Sans","Questrial" => "Questrial","Quicksand" => "Quicksand","Quintessential" => "Quintessential","Qwigley" => "Qwigley","Racing Sans One" => "Racing Sans One","Radley" => "Radley","Raleway" => "Raleway","Raleway Dots" => "Raleway Dots","Rambla" => "Rambla","Rammetto One" => "Rammetto One","Ranchers" => "Ranchers","Rancho" => "Rancho","Rationale" => "Rationale","Redressed" => "Redressed","Reenie Beanie" => "Reenie Beanie","Revalia" => "Revalia","Ribeye" => "Ribeye","Ribeye Marrow" => "Ribeye Marrow","Righteous" => "Righteous","Risque" => "Risque","Roboto" => "Roboto","Roboto Condensed" => "Roboto Condensed","Roboto Slab" => "Roboto Slab","Rochester" => "Rochester","Rock Salt" => "Rock Salt","Rokkitt" => "Rokkitt","Romanesco" => "Romanesco","Ropa Sans" => "Ropa Sans","Rosario" => "Rosario","Rosarivo" => "Rosarivo","Rouge Script" => "Rouge Script","Ruda" => "Ruda","Rufina" => "Rufina","Ruge Boogie" => "Ruge Boogie","Ruluko" => "Ruluko","Rum Raisin" => "Rum Raisin","Ruslan Display" => "Ruslan Display","Russo One" => "Russo One","Ruthie" => "Ruthie","Rye" => "Rye","Sacramento" => "Sacramento","Sail" => "Sail","Salsa" => "Salsa","Sanchez" => "Sanchez","Sancreek" => "Sancreek","Sansita One" => "Sansita One","Sarina" => "Sarina","Satisfy" => "Satisfy","Scada" => "Scada","Schoolbell" => "Schoolbell","Seaweed Script" => "Seaweed Script","Sevillana" => "Sevillana","Seymour One" => "Seymour One","Shadows Into Light" => "Shadows Into Light","Shadows Into Light Two" => "Shadows Into Light Two","Shanti" => "Shanti","Share" => "Share","Share Tech" => "Share Tech","Share Tech Mono" => "Share Tech Mono","Shojumaru" => "Shojumaru","Short Stack" => "Short Stack","Siemreap" => "Siemreap","Sigmar One" => "Sigmar One","Signika" => "Signika","Signika Negative" => "Signika Negative","Simonetta" => "Simonetta","Sintony" => "Sintony","Sirin Stencil" => "Sirin Stencil","Six Caps" => "Six Caps","Skranji" => "Skranji","Slackey" => "Slackey","Smokum" => "Smokum","Smythe" => "Smythe","Sniglet" => "Sniglet","Snippet" => "Snippet","Snowburst One" => "Snowburst One","Sofadi One" => "Sofadi One","Sofia" => "Sofia","Sonsie One" => "Sonsie One","Sorts Mill Goudy" => "Sorts Mill Goudy","Source Code Pro" => "Source Code Pro","Source Sans Pro" => "Source Sans Pro","Special Elite" => "Special Elite","Spicy Rice" => "Spicy Rice","Spinnaker" => "Spinnaker","Spirax" => "Spirax","Squada One" => "Squada One","Stalemate" => "Stalemate","Stalinist One" => "Stalinist One","Stardos Stencil" => "Stardos Stencil","Stint Ultra Condensed" => "Stint Ultra Condensed","Stint Ultra Expanded" => "Stint Ultra Expanded","Stoke" => "Stoke","Strait" => "Strait","Sue Ellen Francisco" => "Sue Ellen Francisco","Sunshiney" => "Sunshiney","Supermercado One" => "Supermercado One","Suwannaphum" => "Suwannaphum","Swanky and Moo Moo" => "Swanky and Moo Moo","Syncopate" => "Syncopate","Tangerine" => "Tangerine","Taprom" => "Taprom","Tauri" => "Tauri","Telex" => "Telex","Tenor Sans" => "Tenor Sans","Text Me One" => "Text Me One","The Girl Next Door" => "The Girl Next Door","Tienne" => "Tienne","Tinos" => "Tinos","Titan One" => "Titan One","Titillium Web" => "Titillium Web","Trade Winds" => "Trade Winds","Trocchi" => "Trocchi","Trochut" => "Trochut","Trykker" => "Trykker","Tulpen One" => "Tulpen One","Ubuntu" => "Ubuntu","Ubuntu Condensed" => "Ubuntu Condensed","Ubuntu Mono" => "Ubuntu Mono","Ultra" => "Ultra","Uncial Antiqua" => "Uncial Antiqua","Underdog" => "Underdog","Unica One" => "Unica One","UnifrakturCook" => "UnifrakturCook","UnifrakturMaguntia" => "UnifrakturMaguntia","Unkempt" => "Unkempt","Unlock" => "Unlock","Unna" => "Unna","VT323" => "VT323","Vampiro One" => "Vampiro One","Varela" => "Varela","Varela Round" => "Varela Round","Vast Shadow" => "Vast Shadow","Vibur" => "Vibur","Vidaloka" => "Vidaloka","Viga" => "Viga","Voces" => "Voces","Volkhov" => "Volkhov","Vollkorn" => "Vollkorn","Voltaire" => "Voltaire","Waiting for the Sunrise" => "Waiting for the Sunrise","Wallpoet" => "Wallpoet","Walter Turncoat" => "Walter Turncoat","Warnes" => "Warnes","Wellfleet" => "Wellfleet","Wendy One" => "Wendy One","Wire One" => "Wire One","Yanone Kaffeesatz" => "Yanone Kaffeesatz","Yellowtail" => "Yellowtail","Yeseva One" => "Yeseva One","Yesteryear" => "Yesteryear","Zeyada" => "Zeyada", "ar" => "-- خطوط عربية --", "JF-Flat" => "JF-Flat", "AraJozoor" => "AraJozoor", "DroidKufi" => "DroidKufi", "DroidNaskh" => "DroidNaskh", "amiri" => "amiri", "lateef" => "lateef", "scheherazade" => "scheherazade", "thabit" => "thabit"
                          ); 
                          

        
// Fetch the Standard Fonts list
$standard_font_list_jozoor = array("none" => "Select a font", 
                                   "Arial, Helvetica, sans-serif" => "Arial, Helvetica, sans-serif",
                                   "'Arial Black', Gadget, sans-serif" => "'Arial Black', Gadget, sans-serif",
                                   "'Comic Sans MS', cursive, sans-serif" => "'Comic Sans MS', cursive, sans-serif",
                                   "Impact, Charcoal, sans-serif" => "Impact, Charcoal, sans-serif",
                                   "'Lucida Sans Unicode', 'Lucida Grande', sans-serif" => "'Lucida Sans Unicode', 'Lucida Grande', sans-serif",
                                   "Tahoma, Geneva, sans-serif" => "Tahoma, Geneva, sans-serif",
                                   "'Trebuchet MS', Helvetica, sans-serif" => "'Trebuchet MS', Helvetica, sans-serif",
                                   "Verdana, Geneva, sans-serif" => "Verdana, Geneva, sans-serif",
                                   "Georgia, serif" => "Georgia, serif",
                                   "'Palatino Linotype', 'Book Antiqua', Palatino, serif" => "'Palatino Linotype', 'Book Antiqua', Palatino, serif",
                                   "'Times New Roman', Times, serif" => "'Times New Roman', Times, serif",
                                   "'Courier New', Courier, monospace" => "'Courier New', Courier, monospace",
                                   "'Lucida Console', Monaco, monospace" => "'Lucida Console', Monaco, monospace"
                                  );

// Font line heights
$fonts_line_heights = array("15px" => "15px","16px" => "16px","17px" => "17px","18px" => "18px","19px" => "19px","20px" => "20px","21px" => "21px","22px" => "22px","23px" => "23px",
                            "24px" => "24px","25px" => "25px","26px" => "26px","27px" => "27px","28px" => "28px","29px" => "29px","30px" => "30px","31px" => "31px","32px" => "32px",
                            "33px" => "33px","34px" => "34px","35px" => "35px","36px" => "36px","37px" => "37px","38px" => "38px","39px" => "39px","40px" => "40px","41px" => "41px",
                            "42px" => "42px","43px" => "43px","44px" => "44px","45px" => "45px","46px" => "46px","47px" => "47px","48px" => "48px","49px" => "49px","50px" => "50px",
                            );

/*-----------------------------------------------------------------------------------*/
/* The Options Array */
/*-----------------------------------------------------------------------------------*/

// Set the Options Array
global $of_options;
$of_options = array();

/* ======================================== General Settings ============================================*/
$of_options[] = array( "name" => __("General Settings", "jozoorthemes"),
                        "type" 		=> "heading",
                        "icon"		=> ADMIN_IMAGES . "icon-settings.png"
				);
					
$of_options[] = array( 	"name" 		=> "Hello there!",
                        "desc" 		=> "",
						"id" 		=> "introduction",
						"std" => "<h3 style=\"margin: 0 0 10px;\">".__("Welcome to Jozoor Theme Options.", "jozoorthemes")."</h3>
					".__("Here you can control with anything in your theme, like general settings, styling your theme, view or hidden elements, configuration any option and more.. if you have any trouble with using theme, please go here", "jozoorthemes")." <a href=\"http://support.jozoor.com/categories/invention-wordpress\" target=\"_blank\" class=\"color\">".__("Support Center", "jozoorthemes")."</a> .",
						"icon" 		=> true,
						"type" 		=> "info" );

/* folds */                
$of_options[] = array( "name" => __("Close Site", "jozoorthemes"),
						"desc" => __("Here you can close your site by check [ ON ] for any visitors but enough you can see normal site, after that you will found [ Under Construction ] settings and you can change what you want in this page", "jozoorthemes").".",
						"id" 		=> "close_site",
						"std" 		=> 0,
						"folds"		=> 0,
                        "on" 		=> __("ON", "jozoorthemes"),
						"off" 		=> __("OFF", "jozoorthemes"),
						"type" 		=> "switch" );
                        
$of_options[] = array( "name" => __("Start Under Construction Settings", "jozoorthemes"),
					   "desc" => "",
				       "id" => "under_cons_start",
					   "std" => "<h3 style=\"margin: 0;\">".__("Start Under Construction Settings", "jozoorthemes")."</h3>",
                       "fold" => "close_site", /* the switch hook */
					   "icon" => true,
					   "type" => "info" );
                       
$of_options[] = array( "name" => __("Under Construction Title", "jozoorthemes"),
					"desc" => __("Write your title", "jozoorthemes").".",
					"id" => "under_cons_title",
					"std" => __("Our Website is Under Construction", "jozoorthemes"),
          			"fold" => "close_site", /* the switch hook */
					"type" => "text");
                    
$of_options[] = array( "name" => __("Under Construction Message", "jozoorthemes"),
					"desc" => __("Write your message under title", "jozoorthemes").".",
					"id" => "under_cons_msg",
					"std" => __("We'll be here soon with a new website. Estimated Time Remaining", "jozoorthemes"),
          			"fold" => "close_site", /* the switch hook */
					"type" => "textarea" );
                    
$of_options[] = array( "name" => __("Ended Date & Time", "jozoorthemes"),
					"desc" => __("Add your date and time like that : <br> year | month | day | hour | minute | time zone <br>
                    found time zone from here : <a href='http://wwp.greenwichmeantime.com/index.htm' target='_blank'>timezone offset for your location</a> .
                    ", "jozoorthemes"),
					"id" => "under_cons_timer",
					"std" => "$year | $month | $day | $hour | $minute | +2",
                    "fold" => "close_site", /* the switch hook */
					"type" => "text");  
                    
$of_options[] = array( "name" => __("Subscribe Form Code", "jozoorthemes"),
					"desc" => __("Add your subscribe form, like feedburner or mailchimp.com or others, you should add this classes to", "jozoorthemes")." <br>
                    ".__("input text : add", "jozoorthemes")." [ class=\"subscribe-text\" ] <br>
                    ".__("subscribe button : add", "jozoorthemes")." [ class=\"subscribe-button\" ]
                     ",
					"id" => "under_cons_subscribe",
					"std" => "<form action=\"#\" class=\"subscribe\">
<input type=\"text\" class=\"subscribe-text\" value=\"Enter Your Email Address\" onBlur=\"if(this.value == '') { this.value = 'Enter Your Email Address'; }\" onFocus=\"if(this.value == 'Enter Your Email Address') { this.value = ''; }\" />
<input type=\"submit\" value=\"Subscribe\" required  class=\"subscribe-button\" />
</form>",
          			"fold" => "close_site", /* the switch hook */
					"type" => "textarea");
                    
$of_options[] = array( "name" => __("Footer Copyrights", "jozoorthemes"),
					"desc" => __("Here you can add Footer Copyrights in under construction page", "jozoorthemes"),
					"id" => "under_cons_footer_text",
					"std" => "&#169; Copyright $year2 <a href=\"http://themes.jozoor.com/wp/invention\" target=\"_blank\" data=\"Invention Wordpress Theme\">Invention</a>. All Rights Reserved. by <a href=\"http://jozoor.com/\" target=\"_blank\" data=\"Jozoor Team\">jozoor</a>",
                    "fold" => "close_site", /* the switch hook */
                    "type" => "textarea");
                    
$of_options[] = array( 	"name" 		=> __("Social Networks links", "jozoorthemes"),
						"desc" => "Here you can add your social networks icons , also sorting and unlimited adding and deleteing icons easy",
						"id" => "under_cons_social_slider",
						"std" => "",
                        "fold" => "close_site", /* the switch hook */
						"type" => "social" );  
                    
                    $of_options[] = array( "name" => __("Add Background Image for Main Content", "jozoorthemes"),
					"desc" => __("You can upload normal image and pattern also , if normal image the image size should be [ 2000 x 695 ]  ", "jozoorthemes"),
					"id" => "bg_image_under_cons",
					"std" => "",
                    "fold" => "close_site", /* the switch hook */
					"type" => "upload");
                    
                    $of_options[] = array( "name" => __("Background Image Repeat Style", "jozoorthemes"),
					"desc" => __("select repeat style for background image", "jozoorthemes"),
					"id" => "bg_image_repeat_under_cons",
					"std" => "no-repeat",
                    "fold" => "close_site", /* the switch hook */
					"type" => "select",
					"options" => $of_options_bg_repeat);

$of_options[] = array( "name" => __("End Under Construction Settings", "jozoorthemes"),
					"desc" => "",
					"id" => "under_cons_end",
					"std" => "<h3 style=\"margin: 0;\">".__("End Under Construction Settings", "jozoorthemes")."</h3>",
                    "fold" => "close_site", /* the switch hook */
					"icon" => true,
					"type" => "info");
/* End folds */

$of_options[] = array( "name" => __("Favicon Image", "jozoorthemes"),
					"desc" => __("Upload your favicon image in size <br>( 16 x 16 ) PX", "jozoorthemes"),
					"id" => "favicon_image",
					"std" => "",
					"type" => "upload");
                    
$of_options[] = array( "name" => __("Apple iPhone Icon", "jozoorthemes"),
					"desc" => __("Upload your iphone icon in size <br>( 57 x 57 ) PX", "jozoorthemes"),
					"id" => "iphone_icon",
					"std" => "",
					"type" => "upload");
                    
$of_options[] = array( "name" => __("Apple iPhone Retina Icon", "jozoorthemes"),
					"desc" => __("Upload your iphone retina icon in size <br>( 114 x 114 ) PX", "jozoorthemes"),
					"id" => "iphone_retina_icon",
					"std" => "",
					"type" => "upload");
                    
$of_options[] = array( "name" => __("Apple iPad Icon", "jozoorthemes"),
					"desc" => __("Upload your ipad icon in size <br>( 72 x 72 ) PX", "jozoorthemes"),
					"id" => "ipad_icon",
					"std" => "",
					"type" => "upload");
                    
$of_options[] = array( "name" => __("Apple iPad Retina Icon", "jozoorthemes"),
					"desc" => __("Upload your ipad retina icon in size <br>( 144 x 144 ) PX", "jozoorthemes"),
					"id" => "ipad_retina_icon",
					"std" => "",
					"type" => "upload");
                    
$of_options[] = array( "name" => __("Style Switcher Box", "jozoorthemes"),
					"desc" => __("Here you can active style switcher box or not", "jozoorthemes").".",
					"id" => "style_switcher",
					"std" => 0,
                    "on" 		=> __("ON", "jozoorthemes"),
					"off" 		=> __("OFF", "jozoorthemes"),
					"type" => "switch"); 
                    
$of_options[] = array( "name" => __("TopTop Icon", "jozoorthemes"),
					"desc" => __("Here you can active totop icon or not", "jozoorthemes").".",
					"id" => "totop_icon",
					"std" => 1,
                    "on" 		=> __("ON", "jozoorthemes"),
					"off" 		=> __("OFF", "jozoorthemes"),
					"type" => "switch");
                    
$of_options[] = array( "name" => __("Responsive Design", "jozoorthemes"),
					"desc" => __("Here you can active responssive design or not", "jozoorthemes").".",
					"id" => "responsive_design",
					"std" => 1,
                    "on" 		=> __("ON", "jozoorthemes"),
					"off" 		=> __("OFF", "jozoorthemes"),
					"type" => "switch");  
        
$of_options[] = array( "name" => __("Loading Image Before Show Content", "jozoorthemes"),
					"desc" => __("Here you can active loading image or not", "jozoorthemes").".",
					"id" => "loading_image",
					"std" => 0,
                    "on" 		=> __("ON", "jozoorthemes"),
					"off" 		=> __("OFF", "jozoorthemes"),
					"type" => "switch");
        
$of_options[] = array( "name" => __("Default Pagination", "jozoorthemes"),
					"desc" => __("Here you can active default wordpress pagination or not", "jozoorthemes").".",
					"id" => "default_wp_pagination",
					"std" => 0,
                    "on" 		=> __("ON", "jozoorthemes"),
					"off" 		=> __("OFF", "jozoorthemes"),
					"type" => "switch");
        
        
// Search Page
$of_options[] = array( "name" => __("Search Page options", "jozoorthemes"),
					"desc" => "",
					"id" => "search_page_info",
					"std" => "<h3 style=\"margin: 0;\">".__("Search Page options", "jozoorthemes")."</h3>",
					"icon" => true,
					"type" => "info");

$of_options[] = array( "name" => __("Select Current Page in Nav Menu", "jozoorthemes"),
					"desc" => __("Here you can select page name , which define current page when go to search page", "jozoorthemes").".",
					"id" => "search_page_current_page",
					"std" => "Select Page Name :",
					"type" => "select",
					"options" => $of_pages);

$of_options[] = array( "name" => __("Select Layout Page Style", "jozoorthemes"),
					"desc" => __("Here you can select layout page style from [ Right Sidebar, Left Sidebar, Full Width ]", "jozoorthemes"),
					"id" => "search_page_layout_style",
					"std" => "Right Sidebar",
					"type" => "select",
					"options" => $of_options_layout_page_style);

$of_options[] = array( "name" => __("Select Sidebar Type", "jozoorthemes"),
					"desc" => __("Here you can select sidebar type from [ Default Sidebar, Sidebar2, Sidebar3, Sidebar4, Sidebar5 ]", "jozoorthemes"),
					"id" => "search_page_sidebar_type",
					"std" => "Default Sidebar",
					"type" => "select",
					"options" => $of_options_sidebar_type);

// 404 Page
$of_options[] = array( "name" => __("404 Page", "jozoorthemes"),
					"desc" => "",
					"id" => "page404_info",
					"std" => "<h3 style=\"margin: 0;\">".__("404 Page", "jozoorthemes")."</h3>",
					"icon" => true,
					"type" => "info");
                    
$of_options[] = array( "name" => __("404 Page Message code & text", "jozoorthemes"),
					"desc" => __("Here you can add 404 page message", "jozoorthemes"),
					"id" => "page404_message",
					"std" => "<div class=\"oops\">oops!</div>
<div class=\"text\">The page you are looking for, doesn't seem to exist.</div>
<div class=\"text\">take a coffee break with me <i class=\"icon-smile\"></i>,  or back to <a href=\"$home_link\">
<span class=\"color\">Homepage</span></a></div>
<div class=\"coffee\">
<i class=\"icon-coffee\"></i>
<i class=\"icon-fire\"></i>
</div>",
					"type" => "textarea");
        

// Static Words
$of_options[] = array( "name" => __("Static Words", "jozoorthemes"),
					"desc" => "",
					"id" => "static_words_info",
					"std" => "<h3 style=\"margin: 0;\">".__("Static Words", "jozoorthemes")."</h3>",
					"icon" => true,
					"type" => "info");
        
                    $of_options[] = array( "name" => "",
					"desc" => "",
					"id" => "static_words_recent_work",
					"std" => __("Recent Work", "jozoorthemes"),
					"type" => "text"
                    );
        
                    $of_options[] = array( "name" => "",
					"desc" => "",
					"id" => "static_words_latest_news",
					"std" => __("Latest News", "jozoorthemes"),
					"type" => "text"
                    );
          
                    $of_options[] = array( "name" => "",
					"desc" => "",
					"id" => "static_words_featured_clients",
					"std" => __("Featured Clients", "jozoorthemes"),
					"type" => "text"
                    );
        
                    $of_options[] = array( "name" => "",
					"desc" => "",
					"id" => "static_words_testimonials",
					"std" => __("Testimonials", "jozoorthemes"),
					"type" => "text"
                    );
        
                    $of_options[] = array( "name" => "",
					"desc" => "",
					"id" => "static_words_description",
					"std" => __("Description", "jozoorthemes"),
					"type" => "text"
                    );
        
                    $of_options[] = array( "name" => "",
					"desc" => "",
					"id" => "static_words_details",
					"std" => __("Details", "jozoorthemes"),
					"type" => "text"
                    );
        
                    $of_options[] = array( "name" => "",
					"desc" => "",
					"id" => "static_words_share",
					"std" => __("Share", "jozoorthemes"),
					"type" => "text"
                    );
        
                    $of_options[] = array( "name" => "",
					"desc" => "",
					"id" => "static_words_related_projects",
					"std" => __("Related Projects", "jozoorthemes"),
					"type" => "text"
                    );
        
                    $of_options[] = array( "name" => "",
					"desc" => "",
					"id" => "static_words_view_project",
					"std" => __("View Project", "jozoorthemes"),
					"type" => "text"
                    );


// Custom Options
$of_options[] = array( "name" => __("Custom Options", "jozoorthemes"),
					"desc" => "",
					"id" => "custom_options_info",
					"std" => "<h3 style=\"margin: 0;\">".__("Custom Options", "jozoorthemes")."</h3>",
					"icon" => true,
					"type" => "info");
                    
$of_options[] = array( "name" => __("Custom Codes OR Javascript Codes in Header before < / head >", "jozoorthemes"),
					"desc" => __("Here you can add Custom Codes OR Javascript Codes in Header", "jozoorthemes"),
					"id" => "custom_codes_header",
					"std" => "",
					"type" => "textarea");
                    
$of_options[] = array( "name" => __("Custom Codes OR Javascript Codes in Footer before < / body >", "jozoorthemes"),
					"desc" => __("Here you can add Custom Codes OR Javascript Codes in Footer", "jozoorthemes"),
					"id" => "custom_codes_footer",
					"std" => "",
					"type" => "textarea");


/* ======================================== Header Options ============================================*/
$of_options[] = array( "name" => __("Header Options", "jozoorthemes"),
						"type" 		=> "heading",
						"icon"		=> ADMIN_IMAGES . "icon-header.png"
				);

// Logo Options
$of_options[] = array( "name" => __("Logo Options", "jozoorthemes"),
					"desc" => "",
					"id" => "logo_options_info",
					"std" => "<h3 style=\"margin: 0;\">".__("Logo Options", "jozoorthemes")."</h3>",
					"icon" => true,
					"type" => "info");
                    
$of_options[] = array( "name" => __("Logo Image (@2x)", "jozoorthemes"),
					"desc" => __("You should upload your logo image with (@2x) size , <br> example if logo size ( 100 x 100 ) px should upload logo with ( 200 x 200 ) px , to appear fine in normal devices and retina devices ", "jozoorthemes"),
					"id" => "logo_image",
					"std" => "",
					"type" => "upload");
                    
$of_options[] = array( "name" => __("Logo Width for (@1x)", "jozoorthemes"),
					"desc" => __("Here you can define width for normal devices (@1x) , this should half width the big logo for (@2x) size,  in PX", "jozoorthemes"),
					"id" => "logo_width",
				    "std" => "171",
                    "mod" => "mini",
					"type" => "text"
					); 
                    
$of_options[] = array( "name" => __("Logo Height for (@1x)", "jozoorthemes"),
					"desc" => __("Here you can define height for normal devices (@1x) , this should half height the big logo for (@2x) size,  in PX", "jozoorthemes"),
					"id" => "logo_height",
				    "std" => "33",
                    "mod" => "mini",
					"type" => "text"
					); 
                    
$of_options[] = array( "name" => __("Logo Top Margin", "jozoorthemes"),
					"desc" => __("Here you can add margin top for logo in PX", "jozoorthemes"),
					"id" => "logo_top_margin",
				    "std" => "35",
                    "mod" => "mini",
					"type" => "text"
					); 
                    
$of_options[] = array( "name" => __("Logo Bottom Margin", "jozoorthemes"),
					"desc" => __("Here you can add margin bottom for logo in PX", "jozoorthemes"),
					"id" => "logo_bottom_margin",
				    "std" => "35",
                    "mod" => "mini",
					"type" => "text"
					); 
        
/* folds */                          
$of_options[] = array( "name" => __("Using Logo Text instead of image", "jozoorthemes"),
					"desc" => __("Here you can add logo text instead of image", "jozoorthemes").".",
					"id" => "logo_text_instead_image",
					"std" => 0,
                    "folds" => 0,
                    "on" 		=> __("ON", "jozoorthemes"),
					"off" 		=> __("OFF", "jozoorthemes"),
					"type" => "switch");
        
                    $of_options[] = array( "name" => __("Logo Name Text", "jozoorthemes"),
					"desc" => __("add your logo name text", "jozoorthemes"),
					"id" => "logo_name_text",
					"std" => "",
                    "fold" => "logo_text_instead_image", /* the checkbox hook */
					"type" => "text"
                    );
        
                   $of_options[] = array( 	"name" 		=> __("Logo Name Font [Type]", "jozoorthemes"),
						"desc" 		=> __("select font for Logo Name", "jozoorthemes"),
						"id" 		=> "logo_name_font_type",
						"std" 		=> "Select a font",
                        "fold"      => "logo_text_instead_image", /* the checkbox hook */
						"type" 		=> "select_google_font",
						"preview" 	=> array(
										"text" => __("This is the preview text!", "jozoorthemes"), //this is the text from preview box
										"size" => "25px" //this is the text size from preview box
						),
						"options" 	=> $google_font_list_jozoor // get google font list
						);
        
                    $of_options[] = array( 	"name" 		=> __("Logo Name Font [size&style]", "jozoorthemes"),
						"desc" 		=> __("select font [size&style] for logo name", "jozoorthemes"),
						"id" 		=> "logo_name_font_sizes_styles",
						"std"       => array('size' => '25px','style' => 'normal'),
                        "fold"      => "logo_text_instead_image", /* the checkbox hook */
						"type" 		=> "typography"
						);
        
                   $of_options[] = array( "name" =>  __("Logo Name Color", "jozoorthemes"),
					"desc" => __("Pick a Custom Color for logo name", "jozoorthemes"),
					"id" => "logo_name_custom_color",
					"std" => "",
                    "fold" => "logo_text_instead_image", /* the checkbox hook */
					"type" => "color");
                                    
// Header Layout Options
$of_options[] = array( "name" => __("Header Layout Options", "jozoorthemes"),
					"desc" => "",
					"id" => "header_layout_options_info",
					"std" => "<h3 style=\"margin: 0;\">".__("Header Layout Options", "jozoorthemes")."</h3>",
					"icon" => true,
					"type" => "info");
                
$url =  ADMIN_DIR . 'assets/images/layouts/';
$of_options[] = array( "name" => __("Select Header Layout", "jozoorthemes"),
					"desc" => __("Here you can define header layout style , you can see all header styles in : ", "jozoorthemes")."<a href=\"http://themes.jozoor.com/wp/invention\" target=\"_blank\" class=\"color\">".__("Theme Demo", "jozoorthemes")."</a> .",
					"id" => "header_layout",
					"std" => "style-default",
					"type" => "images",
					"options" => array(
						'style-default' => $url . 'header-1.jpg',
						'style-3' => $url . 'header-3.jpg',
                        'style-4' => $url . 'header-4.jpg',
                        'style-3 style-5' => $url . 'header-5.jpg' )
					);
                    
$of_options[] = array( "name" => __("Fixed header", "jozoorthemes"),
					"desc" => __("Here you can define fixed header or not", "jozoorthemes").".",
					"id" => "fixed_header",
					"std" => 0,
                    "on" 		=> __("ON", "jozoorthemes"),
					"off" 		=> __("OFF", "jozoorthemes"),
					"type" => "switch");
        
$of_options[] = array( "name" => __("Remove Menu Item bg & top border for hover and current page", "jozoorthemes"),
					"desc" => __("if you don't like bg and border in menu links, just check [ ON ] to remove it", "jozoorthemes").".",
					"id" => "menu_item_bg_border_none",
					"std" => 0,
                    "on" 		=> __("ON", "jozoorthemes"),
					"off" 		=> __("OFF", "jozoorthemes"),
					"type" => "switch");  
        
$of_options[] = array( "name" => __("Show Search Icon in Nav Menu", "jozoorthemes"),
					"desc" => __("Here you can show search icon in navigation menu or not", "jozoorthemes").".",
					"id" => "show_search_icon",
					"std" => 0,
                    "on" 		=> __("ON", "jozoorthemes"),
					"off" 		=> __("OFF", "jozoorthemes"),
					"type" => "switch");
                    
/* folds */                          
$of_options[] = array( "name" => __("Add Background Image for Header Main Content", "jozoorthemes"),
					"desc" => __("Here you can background image for header main content", "jozoorthemes").".",
					"id" => "add_bg_image_main_header",
					"std" => 0,
                    "folds" => 0,
                    "on" 		=> __("ON", "jozoorthemes"),
					"off" 		=> __("OFF", "jozoorthemes"),
					"type" => "switch");
                    
                    $of_options[] = array( "name" => __("Upload Background Image", "jozoorthemes"),
					"desc" => __("You can upload normal image and pattern also , if normal image the size will be 100% ", "jozoorthemes"),
					"id" => "bg_image_main_header",
					"std" => "",
                    "fold" => "add_bg_image_main_header", /* the switch hook */
					"type" => "upload");
                    
                    $of_options[] = array( "name" => __("Background Image Repeat Style", "jozoorthemes"),
					"desc" => __("select repeat style for background image", "jozoorthemes"),
					"id" => "bg_image_repeat_main_header",
					"std" => "repeat",
                    "fold" => "add_bg_image_main_header", /* the checkbox hook */
					"type" => "select",
					"options" => $of_options_bg_repeat);

// Top Bar Options
$of_options[] = array( "name" => __("Top Bar Options", "jozoorthemes"),
					"desc" => "",
					"id" => "topbar_options_info",
					"std" => "<h3 style=\"margin: 0;\">".__("Top Bar Options", "jozoorthemes")."</h3>",
					"icon" => true,
					"type" => "info");

/* folds */                    
$of_options[] = array( "name" => __("Show Top Bar in header", "jozoorthemes"),
					"desc" => __("Here you can show top bar in header or not", "jozoorthemes").".",
					"id" => "show_top_bar_header",
					"std" => 1,
          			"folds" => 1,
                    "on" 		=> __("ON", "jozoorthemes"),
					"off" 		=> __("OFF", "jozoorthemes"),
					"type" => "switch");  
                    
                    $of_options[] = array( "name" => __("Phone Number", "jozoorthemes"),
					"desc" => __("Here you can add your Phone Number, [ if you want to hidden any item , just let input empty ]", "jozoorthemes"),
					"id" => "phone_number_top_bar",
					"std" => "Phone : +22 665 450 210",
                    "fold" => "show_top_bar_header", /* the switch hook */
					"type" => "text"); 
                    
                    $of_options[] = array( "name" => __("E-Mail Address", "jozoorthemes"),
					"desc" => __("Here you can add your E-Mail Address, [ if you want to hidden any item , just let input empty ]", "jozoorthemes"),
					"id" => "email_address_top_bar",
					"std" => "Mail : info@website.com",
                    "fold" => "show_top_bar_header", /* the switch hook */
					"type" => "text"); 
                    
                    $of_options[] = array( 	"name" 		=> __("Social Networks links", "jozoorthemes"),
						"desc" => "Here you can add your social networks icons , also sorting and unlimited adding and deleteing icons easy, and this is rss link form your site if you want to add rss link [ $rss_link ]",
						"id" => "social_slider_header",
						"std" => "",
                        "fold" => "show_top_bar_header", /* the switch hook */
						"type" => "social" );
        
                     $of_options[] = array( "name" => __("Show Top Header Menu instead of social links", "jozoorthemes"),
					"desc" => __("Here you can show top menu instead of social links, just go to : appearance > menus > add menu and select [ Top Header Menu ] from Theme locations", "jozoorthemes").".",
					"id" => "show_top_header_menu",
					"std" => 0,
                    "on" 		=> __("ON", "jozoorthemes"),
					"off" 		=> __("OFF", "jozoorthemes"),
                    "fold" => "show_top_bar_header", /* the switch hook */
					"type" => "switch");

// Menu Options
$of_options[] = array( "name" => __("Menu Options", "jozoorthemes"),
					"desc" => "",
					"id" => "menu_options_info",
					"std" => "<h3 style=\"margin: 0;\">".__("Menu Options", "jozoorthemes")."</h3>",
					"icon" => true,
					"type" => "info");                    
                    
$of_options[] = array( "name" => __("Menu Links Top Padding for header layout ( #1 & #3 )", "jozoorthemes"),
					"desc" => __("Here you can add top padding for menu links in PX", "jozoorthemes"),
					"id" => "menu_links_top_padding_one",
				    "std" => "48",
                    "mod" => "mini",
					"type" => "text"
					); 
                    
$of_options[] = array( "name" => __("Menu Links Bottom Padding for header layout ( #1 & #3 )", "jozoorthemes"),
					"desc" => __("Here you can add bottom padding for menu links in PX", "jozoorthemes"),
					"id" => "menu_links_bottom_padding_one",
				    "std" => "42",
                    "mod" => "mini",
					"type" => "text"
					); 
                    
$of_options[] = array( "name" => __("Menu Links left & right Padding for header layout ( #1 & #3 )", "jozoorthemes"),
					"desc" => __("Here you can add left & right padding for menu links in PX", "jozoorthemes"),
					"id" => "menu_links_leftright_padding_one",
				    "std" => "15",
                    "mod" => "mini",
					"type" => "text"
					); 
                    
$of_options[] = array( "name" => __("Dropdown SubMenu Top Position in Main Menu for header layout ( #1 & #3 )", "jozoorthemes"),
					"desc" => __("Here you can add top position for submenu links in PX", "jozoorthemes"),
					"id" => "submenu_links_top_position_one",
				    "std" => "110",
                    "mod" => "mini",
					"type" => "text"
					); 
                    
$of_options[] = array( "name" => __("Menu Links Top Padding for header layout ( #2 & #4 )", "jozoorthemes"),
					"desc" => __("Here you can add top padding for menu links in PX", "jozoorthemes"),
					"id" => "menu_links_top_padding_two",
				    "std" => "28",
                    "mod" => "mini",
					"type" => "text"
					); 
                    
$of_options[] = array( "name" => __("Menu Links Bottom Padding for header layout ( #2 & #4 )", "jozoorthemes"),
					"desc" => __("Here you can add bottom padding for menu links in PX", "jozoorthemes"),
					"id" => "menu_links_bottom_padding_two",
				    "std" => "22",
                    "mod" => "mini",
					"type" => "text"
					);   
                    
$of_options[] = array( "name" => __("Menu Links left & right Padding for header layout ( #2 & #4 )", "jozoorthemes"),
					"desc" => __("Here you can add left & right padding for menu links in PX", "jozoorthemes"),
					"id" => "menu_links_leftright_padding_two",
				    "std" => "20",
                    "mod" => "mini",
					"type" => "text"
					);           
                    
$of_options[] = array( "name" => __("Dropdown SubMenu Top Position in Main Menu for header layout ( #2 & #4 )", "jozoorthemes"),
					"desc" => __("Here you can add top position for submenu links in PX", "jozoorthemes"),
					"id" => "submenu_links_top_position_two",
				    "std" => "67",
                    "mod" => "mini",
					"type" => "text"
					);     
                    
// Header Layout #3 Options
$of_options[] = array( "name" => __("Header Layout #2 Options", "jozoorthemes"),
					"desc" => "",
					"id" => "header_layout3_options_info",
					"std" => "<h3 style=\"margin: 0;\">".__("Header Layout #2 Options", "jozoorthemes")."</h3>",
					"icon" => true,
					"type" => "info");      
                    
                    $of_options[] = array( "name" => __("Header Right Content", "jozoorthemes"),
					"desc" => __("here you can select right content type", "jozoorthemes"),
					"id" => "header_layout3_right_content",
					"std" => "search",
					"type" => "select",
					"options" => $of_options_header_layout3_right_content);    
                    
                    $of_options[] = array( "name" => __("Header Right Custom Content", "jozoorthemes"),
					"desc" => __("you can add text or shortcodes or html codes like , banners or google ads or other thing , should select [ custom content ] in [ Header Right Content ] option  ", "jozoorthemes").".",
					"id" => "header_layout3_right_custom_content",
					"std" => "",
					"type" => "textarea" );  
        
                    $of_options[] = array( "name" => __("Margin Top & Bottom for Header Right Custom Content", "jozoorthemes"),
					"desc" => __("Here you can add margin top & bottom for header right cutom content in PX", "jozoorthemes"),
					"id" => "header_layout3_right_custom_content_margin",
				    "std" => "25",
                    "mod" => "mini",
					"type" => "text"
					);    
                    
// Header Custom Options
$of_options[] = array( "name" => __("Header Custom Options", "jozoorthemes"),
					"desc" => "",
					"id" => "header_custom_options_info",
					"std" => "<h3 style=\"margin: 0;\">".__("Header Custom Options", "jozoorthemes")."</h3>",
					"icon" => true,
					"type" => "info");  
                    
/* folds */                    
$of_options[] = array( "name" => __("Add Custom Content in Top Header", "jozoorthemes"),
					"desc" => __("Here you can add custom content in top header", "jozoorthemes").".",
					"id" => "add_custom_content_top_header",
					"std" => 0,
          			"folds" => 0,
                    "on" 		=> __("ON", "jozoorthemes"),
					"off" 		=> __("OFF", "jozoorthemes"),
					"type" => "switch"); 
                    
                    $of_options[] = array( "name" => __("Top Header Custom Content", "jozoorthemes"),
					"desc" => __("you can add text or html codes like , banners or google ads or other thing ", "jozoorthemes").".",
					"id" => "custom_content_top_header",
					"std" => "",
                    "fold" => "add_custom_content_top_header", /* the switch hook */
					"type" => "textarea" ); 

/* folds */                    
$of_options[] = array( "name" => __("Add Custom Content in Bottom Header", "jozoorthemes"),
					"desc" => __("Here you can add custom content in bottom header", "jozoorthemes").".",
					"id" => "add_custom_content_bottom_header",
					"std" => 0,
          			"folds" => 0,
                    "on" 		=> __("ON", "jozoorthemes"),
					"off" 		=> __("OFF", "jozoorthemes"),
					"type" => "switch"); 
                    
                    $of_options[] = array( "name" => __("Bottom Header Custom Content", "jozoorthemes"),
					"desc" => __("you can add text or html codes like , banners or google ads or other thing ", "jozoorthemes").".",
					"id" => "custom_content_bottom_header",
					"std" => "",
                    "fold" => "add_custom_content_bottom_header", /* the switch hook */
					"type" => "textarea" );      
                    
/* ======================================== Footer Options ============================================*/
$of_options[] = array( "name" => __("Footer Options", "jozoorthemes"),
						"type" 		=> "heading",
						"icon"		=> ADMIN_IMAGES . "icon-footer.png"
				);
                
// Footer Widgets Options
$of_options[] = array( "name" => __("Footer Widgets Options", "jozoorthemes"),
					"desc" => "",
					"id" => "footer_widgets_options_info",
					"std" => "<h3 style=\"margin: 0;\">".__("Footer Widgets Options", "jozoorthemes")."</h3>",
					"icon" => true,
					"type" => "info");

/* folds */                    
$of_options[] = array( "name" => __("Show Footer Widgets", "jozoorthemes"),
					"desc" => __("Here you can show footer widgets or not", "jozoorthemes").".",
					"id" => "show_footer_widgets",
					"std" => 1,
          			"folds" => 1,
                    "on" 		=> __("ON", "jozoorthemes"),
					"off" 		=> __("OFF", "jozoorthemes"),
					"type" => "switch");                      
                    
                    $url =  ADMIN_DIR . 'assets/images/layouts/';
                    $of_options[] = array( "name" => __("Footer Widgets Layout Style", "jozoorthemes"),
					"desc" => __("Here you can define footer layout style", "jozoorthemes"),
					"id" => "footer_layout",
					"std" => "1/3 Column",
                    "fold" => "show_footer_widgets", /* the switch hook */
					"type" => "images",
					"options" => array(
                        '1/4 Column' => $url . '1-4-col.jpg',
                        '1/3 Column' => $url . '1-3-col.jpg',
                        '12/4 Column' => $url . '12-4-col.jpg',
                        '11/5 Column' => $url . '11-5-col.jpg',
                        '1/2 Column' => $url . '1-2-col.jpg',
						'16 Column' => $url . '16-col.jpg' )
					);

// Footer Down Bar Options
$of_options[] = array( "name" => __("Footer Down Bar Options", "jozoorthemes"),
					"desc" => "",
					"id" => "footer_downbar_options_info",
					"std" => "<h3 style=\"margin: 0;\">".__("Footer Down Bar Options", "jozoorthemes")."</h3>",
					"icon" => true,
					"type" => "info");
                    
/* folds */                    
$of_options[] = array( "name" => __("Show Footer Down Bar", "jozoorthemes"),
					"desc" => __("Here you can show footer down bar or not", "jozoorthemes").".",
					"id" => "show_footer_downbar",
					"std" => 1,
          			"folds" => 1,
                    "on" 		=> __("ON", "jozoorthemes"),
					"off" 		=> __("OFF", "jozoorthemes"),
					"type" => "switch");    
                    
                    $of_options[] = array( "name" => __("Footer Copyrights", "jozoorthemes"),
					"desc" => __("Here you can add Footer Copyrights", "jozoorthemes"),
					"id" => "footer_text",
					"std" => "&#169; Copyright $year2 <a href='http://themes.jozoor.com/wp/invention' target='_blank' data='Invention Wordpress Theme'>Invention</a>. All Rights Reserved. by <a href='http://jozoor.com/' target='_blank' data='Jozoor Team'>jozoor</a>",
                    "fold" => "show_footer_downbar", /* the switch hook */
                    "type" => "textarea");
                    
                    $of_options[] = array( 	"name" 		=> __("Social Networks links", "jozoorthemes"),
						"desc" => "Here you can add your social networks icons , also sorting and unlimited adding and deleteing icons easy, and this is rss link form your site if you want to add rss link [ $rss_link ]",
						"id" => "social_slider_footer",
						"std" => "",
                        "fold" => "show_footer_downbar", /* the switch hook */
						"type" => "social" );
        
                    $of_options[] = array( "name" => __("Show Down Footer Menu instead of social links", "jozoorthemes"),
					"desc" => __("Here you can show down menu instead of social links, just go to : appearance > menus > add menu and select [ Down Footer Menu ] from Theme locations", "jozoorthemes").".",
					"id" => "show_down_footer_menu",
					"std" => 0,
                    "on" 		=> __("ON", "jozoorthemes"),
					"off" 		=> __("OFF", "jozoorthemes"),
                    "fold" => "show_footer_downbar", /* the switch hook */
					"type" => "switch");

// Footer Custom Options
$of_options[] = array( "name" => __("Footer Custom Options", "jozoorthemes"),
					"desc" => "",
					"id" => "footer_custom_options_info",
					"std" => "<h3 style=\"margin: 0;\">".__("Footer Custom Options", "jozoorthemes")."</h3>",
					"icon" => true,
					"type" => "info");  
                    
/* folds */                    
$of_options[] = array( "name" => __("Add Custom Content in Top Footer", "jozoorthemes"),
					"desc" => __("Here you can add custom content in top footer", "jozoorthemes").".",
					"id" => "add_custom_content_top_footer",
					"std" => 0,
          			"folds" => 0,
                    "on" 		=> __("ON", "jozoorthemes"),
					"off" 		=> __("OFF", "jozoorthemes"),
					"type" => "switch"); 
                    
                    $of_options[] = array( "name" => __("Top Footer Custom Content", "jozoorthemes"),
					"desc" => __("you can add text or html codes like , banners or google ads or other thing ", "jozoorthemes").".",
					"id" => "custom_content_top_footer",
					"std" => "",
                    "fold" => "add_custom_content_top_footer", /* the switch hook */
					"type" => "textarea" ); 

/* folds */                    
$of_options[] = array( "name" => __("Add Custom Content in Bottom Footer", "jozoorthemes"),
					"desc" => __("Here you can add custom content in bottom footer", "jozoorthemes").".",
					"id" => "add_custom_content_bottom_footer",
					"std" => 0,
          			"folds" => 0,
                    "on" 		=> __("ON", "jozoorthemes"),
					"off" 		=> __("OFF", "jozoorthemes"),
					"type" => "switch"); 
                    
                    $of_options[] = array( "name" => __("Bottom Footer Custom Content", "jozoorthemes"),
					"desc" => __("you can add text or html codes like , banners or google ads or other thing ", "jozoorthemes").".",
					"id" => "custom_content_bottom_footer",
					"std" => "",
                    "fold" => "add_custom_content_bottom_footer", /* the switch hook */
					"type" => "textarea" );
                
/* ======================================== Styles Options ============================================*/
$of_options[] = array( "name" => __("Styles Options", "jozoorthemes"),
						"type" 		=> "heading",
						"icon"		=> ADMIN_IMAGES . "icon-paint.png"
				);

// Layout & Skin Options
$of_options[] = array( "name" => __("Layout & Skin Options", "jozoorthemes"),
					"desc" => "",
					"id" => "styles_options_info",
					"std" => "<h3 style=\"margin: 0;\">".__("Layout & Skin Options", "jozoorthemes")."</h3>",
					"icon" => true,
					"type" => "info");
                    
$url =  ADMIN_DIR . 'assets/images/layouts/';
$of_options[] = array( "name" => __("Layout Style", "jozoorthemes"),
					"desc" => __("Here you can define layout style", "jozoorthemes"),
					"id" => "layout_style",
					"std" => "wide",
					"type" => "images",
					"options" => array(
						'wide' => $url . 'wide.jpg',
                        'boxed' => $url . 'boxed.jpg')
					);

$url =  ADMIN_DIR . 'assets/images/layouts/';
$of_options[] = array( "name" => __("Theme Skin", "jozoorthemes"),
					"desc" => __("Here you can define theme skin", "jozoorthemes"),
					"id" => "theme_skin",
					"std" => "white",
					"type" => "images",
					"options" => array(
						'white' => $url . 'white.jpg',
                        'dark' => $url . 'dark.jpg')
					);
  

// Colors Options
$of_options[] = array( "name" => __("Colors Options", "jozoorthemes"),
					"desc" => "",
					"id" => "colors_styles_info",
					"std" => "<h3 style=\"margin: 0;\">".__("Colors Options", "jozoorthemes")."</h3>",
					"icon" => true,
					"type" => "info");

$url =  ADMIN_DIR . 'assets/images/layouts/';
$of_options[] = array( "name" => __("Default Color Scheme", "jozoorthemes"),
					"desc" => __("Here you can define default color scheme", "jozoorthemes"),
					"id" => "default_color_scheme",
					"std" => "red",
					"type" => "images",
					"options" => array(
                        'red' => $url . 'red.jpg',
                        'orange' => $url . 'orange.jpg',
                        'turquoise' => $url . 'turquoise.jpg',
                        'green' => $url . 'green.jpg',
						'blue' => $url . 'blue.jpg' )
					);

/* folds */
$of_options[] = array( "name" => __("Add Custom Colors [ backgrounds & fonts colors ]", "jozoorthemes"),
					"desc" => __("Here you can add custom colors, if you check [ ON ] you can do that", "jozoorthemes").".",
					"id" => "custom_design_colors",
					"std" => 0,
          			"folds" => 1,
                    "on" 		=> __("ON", "jozoorthemes"),
					"off" 		=> __("OFF", "jozoorthemes"),
					"type" => "switch"); 
                    
                    // Main Colors
                    $of_options[] = array( "name" => __("Main Colors", "jozoorthemes"),
					"desc" => "",
					"id" => "main_colors_info",
					"std" => "<h3 style=\"margin: 0;\">".__("Main Colors", "jozoorthemes")."</h3>",
					"icon" => true,
                    "fold" => "custom_design_colors", /* the checkbox hook */
					"type" => "info");
                    
                    $of_options[] = array( "name" =>  __("Custom Color Scheme", "jozoorthemes"),
					"desc" => __("Pick a Custom scheme Color for theme", "jozoorthemes"),
					"id" => "custom_color_scheme",
					"std" => "",
                    "fold" => "custom_design_colors", /* the checkbox hook */
					"type" => "color");
                    
                    $of_options[] = array( "name" =>  __("Body Background Color", "jozoorthemes"),
					"desc" => __("Pick a Body Background Color, [ this will work in boxed style ]", "jozoorthemes"),
					"id" => "body_background_color",
					"std" => "",
                    "fold" => "custom_design_colors", /* the checkbox hook */
					"type" => "color");                                        
                    
                    $of_options[] = array( "name" =>  __("Header Main content Background Color", "jozoorthemes"),
					"desc" => __("Pick a Header main content Background Color", "jozoorthemes"),
					"id" => "header_background_color",
					"std" => "",
                    "fold" => "custom_design_colors", /* the checkbox hook */
					"type" => "color");
                    
                    $of_options[] = array( "name" =>  __("Header Top Bar Background Color", "jozoorthemes"),
					"desc" => __("Pick a Header Top Bar Background Color", "jozoorthemes"),
					"id" => "top_bar_header_background_color",
					"std" => "",
                    "fold" => "custom_design_colors", /* the checkbox hook */
					"type" => "color");
                    
                    $of_options[] = array( "name" =>  __("Wrap Background Color", "jozoorthemes"),
					"desc" => __("Pick a Wrap Background Color, [ this will work in boxed style ]", "jozoorthemes"),
					"id" => "wrap_background_color",
					"std" => "",
                    "fold" => "custom_design_colors", /* the checkbox hook */
					"type" => "color");    
                    
                    $of_options[] = array( "name" =>  __("Separate Line Color in Body", "jozoorthemes"),
					"desc" => __("Pick a separate line color in Body", "jozoorthemes"),
					"id" => "separate_line_color_in_body",
					"std" => "",
                    "fold" => "custom_design_colors", /* the checkbox hook */
					"type" => "color");                                    
                    
                    $of_options[] = array( "name" =>  __("Footer Background Color", "jozoorthemes"),
					"desc" => __("Pick a Footer Background Color", "jozoorthemes"),
					"id" => "footer_background_color",
					"std" => "",
                    "fold" => "custom_design_colors", /* the checkbox hook */
					"type" => "color");
                    
                    $of_options[] = array( "name" =>  __("Footer Down Bar Background Color", "jozoorthemes"),
					"desc" => __("Pick a Footer down bar Background Color", "jozoorthemes"),
					"id" => "footer_downbar_background_color",
					"std" => "",
                    "fold" => "custom_design_colors", /* the checkbox hook */
					"type" => "color");
                    
                    // Header Menu Colors
                    $of_options[] = array( "name" => __("Header Menu Colors", "jozoorthemes"),
					"desc" => "",
					"id" => "menu_colors_info",
					"std" => "<h3 style=\"margin: 0;\">".__("Header Menu Colors", "jozoorthemes")."</h3>",
					"icon" => true,
                    "fold" => "custom_design_colors", /* the checkbox hook */
					"type" => "info");
                    
                    $of_options[] = array( "name" =>  __("Header Menu Links ( active & hover ) Background Color", "jozoorthemes"),
					"desc" => __("Pick a Menu Links ( active & hover ) Background Color", "jozoorthemes"),
					"id" => "menu_links_header_background_color",
					"std" => "",
                    "fold" => "custom_design_colors", /* the checkbox hook */
					"type" => "color");
                    
                    $of_options[] = array( "name" =>  __("Header Menu style ( #2 & #4 ) Background Color", "jozoorthemes"),
					"desc" => __("Pick a Header Menu style ( #2 & #4 ) Background Color", "jozoorthemes"),
					"id" => "menu_style35_header_background_color",
					"std" => "",
                    "fold" => "custom_design_colors", /* the checkbox hook */
					"type" => "color");
                    
                    $of_options[] = array( "name" =>  __("Top Border for Header Menu style ( #2 & #4 ) Color", "jozoorthemes"),
					"desc" => __("Pick a top border for Header Menu style ( #2 & #4 ) Color", "jozoorthemes"),
					"id" => "topborder_menu_style35_header_color",
					"std" => "",
                    "fold" => "custom_design_colors", /* the checkbox hook */
					"type" => "color");
                    
                    $of_options[] = array( "name" =>  __("Sublevels Menu links Background Color", "jozoorthemes"),
					"desc" => __("Pick a Sublevels Menu links Background Color", "jozoorthemes"),
					"id" => "sublevels_menu_links_background_color",
					"std" => "",
                    "fold" => "custom_design_colors", /* the checkbox hook */
					"type" => "color");
                    
                    $of_options[] = array( "name" =>  __("Sublevels Menu links ( active & hover ) Background Color", "jozoorthemes"),
					"desc" => __("Pick a Sublevels Menu links ( active & hover ) Background Color", "jozoorthemes"),
					"id" => "sublevels_menu_links_hover_background_color",
					"std" => "",
                    "fold" => "custom_design_colors", /* the checkbox hook */
					"type" => "color");
                    
                    $of_options[] = array( "name" =>  __("Separate line in Sublevels Menu Color", "jozoorthemes"),
					"desc" => __("Pick a Separate line in Sublevels Menu Color", "jozoorthemes"),
					"id" => "separate_sublevels_menu_color",
					"std" => "",
                    "fold" => "custom_design_colors", /* the checkbox hook */
					"type" => "color");
                    
                    $of_options[] = array( "name" =>  __("Menu Links Font Color", "jozoorthemes"),
					"desc" => __("Pick a Menu Links font Color", "jozoorthemes"),
					"id" => "menu_links_header_font_color",
					"std" => "",
                    "fold" => "custom_design_colors", /* the checkbox hook */
					"type" => "color");
                    
                    $of_options[] = array( "name" =>  __("Menu Links ( active & hover ) Font Color", "jozoorthemes"),
					"desc" => __("Pick a Menu Links ( active & hover ) font Color", "jozoorthemes"),
					"id" => "menu_links_active_header_font_color",
					"std" => "",
                    "fold" => "custom_design_colors", /* the checkbox hook */
					"type" => "color");
                    
                    $of_options[] = array( "name" =>  __("Sublevels Menu Links Font Color", "jozoorthemes"),
					"desc" => __("Pick a Sublevels Menu Links font Color", "jozoorthemes"),
					"id" => "sublevels_menu_links_header_font_color",
					"std" => "",
                    "fold" => "custom_design_colors", /* the checkbox hook */
					"type" => "color");
                    
                    $of_options[] = array( "name" =>  __("Sublevels Menu Links ( active & hover ) Font Color", "jozoorthemes"),
					"desc" => __("Pick a Sublevels Menu Links ( active & hover ) font Color", "jozoorthemes"),
					"id" => "sublevels_menu_links_active_header_font_color",
					"std" => "",
                    "fold" => "custom_design_colors", /* the checkbox hook */
					"type" => "color");
                    
                    // Font Colors
                    $of_options[] = array( "name" => __("Font Colors", "jozoorthemes"),
					"desc" => "",
					"id" => "font_colors_info",
					"std" => "<h3 style=\"margin: 0;\">".__("Font Colors", "jozoorthemes")."</h3>",
					"icon" => true,
                    "fold" => "custom_design_colors", /* the checkbox hook */
					"type" => "info");
                    
                    /* ===== */
                    $of_options[] = array( "name" =>  __("Body Text Color", "jozoorthemes"),
					"desc" => __("Pick a body text Color", "jozoorthemes"),
					"id" => "body_text_color",
					"std" => "",
                    "fold" => "custom_design_colors", /* the checkbox hook */
					"type" => "color");
                    
                    $of_options[] = array( "name" =>  __("Footer Text Color", "jozoorthemes"),
					"desc" => __("Pick a footer text Color", "jozoorthemes"),
					"id" => "footer_text_color",
					"std" => "",
                    "fold" => "custom_design_colors", /* the checkbox hook */
					"type" => "color");
                    
                    $of_options[] = array( "name" =>  __("Top Bar Header Text Color", "jozoorthemes"),
					"desc" => __("Pick a top bar header text Color", "jozoorthemes"),
					"id" => "topbar_header_text_color",
					"std" => "",
                    "fold" => "custom_design_colors", /* the checkbox hook */
					"type" => "color");
                    
                    $of_options[] = array( "name" =>  __("Down Bar Footer Text Color", "jozoorthemes"),
					"desc" => __("Pick a down bar footer text Color", "jozoorthemes"),
					"id" => "downbar_footer_text_color",
					"std" => "",
                    "fold" => "custom_design_colors", /* the checkbox hook */
					"type" => "color");
                    
                    $of_options[] = array( "name" =>  __("Body Links Color", "jozoorthemes"),
					"desc" => __("Pick a body links Color", "jozoorthemes"),
					"id" => "body_links_color",
					"std" => "",
                    "fold" => "custom_design_colors", /* the checkbox hook */
					"type" => "color");
                    
                    $of_options[] = array( "name" =>  __("Body Links ( hover ) Color", "jozoorthemes"),
					"desc" => __("Pick a body links ( hover ) Color", "jozoorthemes"),
					"id" => "body_links_hover_color",
					"std" => "",
                    "fold" => "custom_design_colors", /* the checkbox hook */
					"type" => "color");
                    
                    $of_options[] = array( "name" =>  __("Footer Links Color", "jozoorthemes"),
					"desc" => __("Pick a footer links Color", "jozoorthemes"),
					"id" => "footer_links_color",
					"std" => "",
                    "fold" => "custom_design_colors", /* the checkbox hook */
					"type" => "color");
                    
                    $of_options[] = array( "name" =>  __("Footer Links ( hover ) Color", "jozoorthemes"),
					"desc" => __("Pick a footer links ( hover ) Color", "jozoorthemes"),
					"id" => "footer_links_hover_color",
					"std" => "",
                    "fold" => "custom_design_colors", /* the checkbox hook */
					"type" => "color");
                    
                    $of_options[] = array( "name" =>  __("Heading Title Color in ( body )", "jozoorthemes"),
					"desc" => __("Pick a heading title color in body", "jozoorthemes"),
					"id" => "heading_title_color_body",
					"std" => "",
                    "fold" => "custom_design_colors", /* the checkbox hook */
					"type" => "color");
                    
                    $of_options[] = array( "name" =>  __("Heading Title Color in ( footer )", "jozoorthemes"),
					"desc" => __("Pick a heading title color in footer", "jozoorthemes"),
					"id" => "heading_title_color_footer",
					"std" => "",
                    "fold" => "custom_design_colors", /* the checkbox hook */
					"type" => "color");
                    
                    $of_options[] = array( "name" =>  __("Page Title Color", "jozoorthemes"),
					"desc" => __("Pick a page title color", "jozoorthemes"),
					"id" => "page_title_color",
					"std" => "",
                    "fold" => "custom_design_colors", /* the checkbox hook */
					"type" => "color");
                    
/* End folds */

// Body Background images Options
$of_options[] = array( "name" => __("Body Background images Options", "jozoorthemes"),
					"desc" => "",
					"id" => "backgrounds_styles_info",
					"std" => "<h3 style=\"margin: 0;\">".__("Body Background images Options", "jozoorthemes")."</h3>",
					"icon" => true,
					"type" => "info");

$of_options[] = array( "name" => __("Body Background Image", "jozoorthemes"),
					"desc" => __("Select a background pattern for body, [ this will work in boxed style ]", "jozoorthemes"),
					"id" => "body_bg_image",
					"std" => $bg_images_url."wood.jpg",
					"type" => "tiles",
					"options" => $bg_images,
					);

/* folds */                    
$of_options[] = array( "name" => __("Add Custom Body Background Image", "jozoorthemes"),
					"desc" => __("Here you can add custom Body Background Image, if you check [ ON ] you can do that", "jozoorthemes").".",
					"id" => "custom_body_bg_image",
					"std" => 0,
          			"folds" => 0,
                    "on" 		=> __("ON", "jozoorthemes"),
					"off" 		=> __("OFF", "jozoorthemes"),
					"type" => "switch"); 
                    
                    $of_options[] = array( "name" => __("Custom Body Background Image", "jozoorthemes"),
					"desc" => __("upload background image for body you ,can use pattern or big image , [ this will work in boxed style ]", "jozoorthemes"),
					"id" => "custom_body_background",
					"std" => "",
                    "fold" => "custom_body_bg_image", /* the checkbox hook */
					"type" => "upload");   
                    
                    $of_options[] = array( "name" => __("Body Background Image Repeat Style", "jozoorthemes"),
					"desc" => __("select repeat style for body background image wich uploaded", "jozoorthemes"),
					"id" => "body_bg_repeat",
					"std" => "repeat",
                    "fold" => "custom_body_bg_image", /* the checkbox hook */
					"type" => "select",
					"options" => $of_options_bg_repeat);                       
/* End folds */


/* ======================================== Typography Options ============================================*/
$of_options[] = array( "name" => __("Typography Options", "jozoorthemes"),
						"type" 		=> "heading",
						"icon"		=> ADMIN_IMAGES . "icon-typography.png"
				);
        
// Google Fonts
$of_options[] = array( "name" => __("Google Fonts", "jozoorthemes"),
					"desc" => "",
					"id" => "google_fonts_info",
					"std" => "<h3 style=\"margin: 0;\">".__("Google Fonts", "jozoorthemes")."</h3>",
					"icon" => true,
					"type" => "info");
        
/* folds */                    
$of_options[] = array( "name" => __("Using Google Fonts", "jozoorthemes"),
					"desc" => __("Here you can use google fonts, if you check [ ON ] you can do that", "jozoorthemes").".",
					"id" => "using_google_fonts",
					"std" => 0,
          			"folds" => 0,
                    "on" 		=> __("ON", "jozoorthemes"),
					"off" 		=> __("OFF", "jozoorthemes"),
					"type" => "switch"); 
                
                    $of_options[] = array( 	"name" 		=> __("Body Font [Type]", "jozoorthemes"),
						"desc" 		=> __("select font for body text", "jozoorthemes"),
						"id" 		=> "body_font_type",
						"std" 		=> "Select a font",
                        "fold"      => "using_google_fonts", /* the checkbox hook */
						"type" 		=> "select_google_font",
						"preview" 	=> array(
										"text" => __("This is the preview text!", "jozoorthemes"), //this is the text from preview box
										"size" => "30px" //this is the text size from preview box
						),
						"options" 	=> $google_font_list_jozoor // get google font list
						);
        
                       $of_options[] = array( 	"name" 		=> __("Menu Font [Type]", "jozoorthemes"),
						"desc" 		=> __("select font for menu text", "jozoorthemes"),
						"id" 		=> "menu_font_type",
						"std" 		=> "Select a font",
                        "fold"      => "using_google_fonts", /* the checkbox hook */
						"type" 		=> "select_google_font",
						"preview" 	=> array(
										"text" => __("This is the preview text!", "jozoorthemes"), //this is the text from preview box
										"size" => "30px" //this is the text size from preview box
						),
						"options" 	=> $google_font_list_jozoor // get google font list
						);
        
                       $of_options[] = array( 	"name" 		=> __("Top Bar header Font [Type]", "jozoorthemes"),
						"desc" 		=> __("select font for top bar header text", "jozoorthemes"),
						"id" 		=> "topbar_font_type",
						"std" 		=> "Select a font",
                        "fold"      => "using_google_fonts", /* the checkbox hook */
						"type" 		=> "select_google_font",
						"preview" 	=> array(
										"text" => __("This is the preview text!", "jozoorthemes"), //this is the text from preview box
										"size" => "30px" //this is the text size from preview box
						),
						"options" 	=> $google_font_list_jozoor // get google font list
						);
        
                        $of_options[] = array( 	"name" 		=> __("Heading Font [Type]", "jozoorthemes"),
						"desc" 		=> __("select font for heading text", "jozoorthemes"),
						"id" 		=> "heading_font_type",
						"std" 		=> "Select a font",
                        "fold"      => "using_google_fonts", /* the checkbox hook */
						"type" 		=> "select_google_font",
						"preview" 	=> array(
										"text" => __("This is the preview text!", "jozoorthemes"), //this is the text from preview box
										"size" => "30px" //this is the text size from preview box
						),
						"options" 	=> $google_font_list_jozoor // get google font list
						);
        
                        $of_options[] = array( 	"name" 		=> __("Heading Footer Font [Type]", "jozoorthemes"),
						"desc" 		=> __("select font for heading footer text", "jozoorthemes"),
						"id" 		=> "heading_footer_font_type",
						"std" 		=> "Select a font",
                        "fold"      => "using_google_fonts", /* the checkbox hook */
						"type" 		=> "select_google_font",
						"preview" 	=> array(
										"text" => __("This is the preview text!", "jozoorthemes"), //this is the text from preview box
										"size" => "30px" //this is the text size from preview box
						),
						"options" 	=> $google_font_list_jozoor // get google font list
						);
        
                        $of_options[] = array( 	"name" 		=> __("Down Bar Font [Type]", "jozoorthemes"),
						"desc" 		=> __("select font for down bar text", "jozoorthemes"),
						"id" 		=> "downbar_font_type",
						"std" 		=> "Select a font",
                        "fold"      => "using_google_fonts", /* the checkbox hook */
						"type" 		=> "select_google_font",
						"preview" 	=> array(
										"text" => __("This is the preview text!", "jozoorthemes"), //this is the text from preview box
										"size" => "30px" //this is the text size from preview box
						),
						"options" 	=> $google_font_list_jozoor // get google font list
						);
        
                        $of_options[] = array( 	"name" 		=> __("Sliders Title Font [Type]", "jozoorthemes"),
						"desc" 		=> __("select font for sliders title text", "jozoorthemes"),
						"id" 		=> "title_sliders_font_type",
						"std" 		=> "Select a font",
                        "fold"      => "using_google_fonts", /* the checkbox hook */
						"type" 		=> "select_google_font",
						"preview" 	=> array(
										"text" => __("This is the preview text!", "jozoorthemes"), //this is the text from preview box
										"size" => "30px" //this is the text size from preview box
						),
						"options" 	=> $google_font_list_jozoor // get google font list
						);
        
                        $of_options[] = array( 	"name" 		=> __("Sliders Description Font [Type]", "jozoorthemes"),
						"desc" 		=> __("select font for sliders description text", "jozoorthemes"),
						"id" 		=> "desc_sliders_font_type",
						"std" 		=> "Select a font",
                        "fold"      => "using_google_fonts", /* the checkbox hook */
						"type" 		=> "select_google_font",
						"preview" 	=> array(
										"text" => __("This is the preview text!", "jozoorthemes"), //this is the text from preview box
										"size" => "30px" //this is the text size from preview box
						),
						"options" 	=> $google_font_list_jozoor // get google font list
						);
        
// Standard Fonts
$of_options[] = array( "name" => __("Standard Fonts", "jozoorthemes"),
					"desc" => "",
					"id" => "standard_fonts_info",
					"std" => "<h3 style=\"margin: 0;\">".__("Standard Fonts : <small>( you should check [OFF] using google fonts, to activate this standard fonts )</small>", "jozoorthemes")."</h3>",
					"icon" => true,
					"type" => "info");
        
/* folds */                    
$of_options[] = array( "name" => __("Using Standard Fonts", "jozoorthemes"),
					"desc" => __("Here you can use standard fonts, if you check [ ON ] you can do that", "jozoorthemes").".",
					"id" => "using_standard_fonts",
					"std" => 0,
          			"folds" => 0,
                    "on" 		=> __("ON", "jozoorthemes"),
					"off" 		=> __("OFF", "jozoorthemes"),
					"type" => "switch");  
    
                    $of_options[] = array( 	"name" 		=> __("Body Font [Type]", "jozoorthemes"),
						"desc" 		=> __("select font for body text", "jozoorthemes"),
						"id" 		=> "body_font_type_standard",
						"std" 		=> "Select a font",
                        "fold"      => "using_standard_fonts", /* the checkbox hook */
						"type" 		=> "select",
						"options" 	=> $standard_font_list_jozoor // get standard font list
						);
        
                      $of_options[] = array( 	"name" 		=> __("Menu Font [Type]", "jozoorthemes"),
						"desc" 		=> __("select font for menu text", "jozoorthemes"),
						"id" 		=> "menu_font_type_standard",
						"std" 		=> "Select a font",
                        "fold"      => "using_standard_fonts", /* the checkbox hook */
						"type" 		=> "select",
						"options" 	=> $standard_font_list_jozoor // get standard font list
						);
                       
                       $of_options[] = array( 	"name" 		=> __("Top Bar header Font [Type]", "jozoorthemes"),
						"desc" 		=> __("select font for top bar header text", "jozoorthemes"),
						"id" 		=> "topbar_font_type_standard",
						"std" 		=> "Select a font",
                        "fold"      => "using_standard_fonts", /* the checkbox hook */
						"type" 		=> "select",
						"options" 	=> $standard_font_list_jozoor // get standard font list
						);
        
                        $of_options[] = array( 	"name" 		=> __("Heading Font [Type]", "jozoorthemes"),
						"desc" 		=> __("select font for heading text", "jozoorthemes"),
						"id" 		=> "heading_font_type_standard",
						"std" 		=> "Select a font",
                        "fold"      => "using_standard_fonts", /* the checkbox hook */
						"type" 		=> "select",
						"options" 	=> $standard_font_list_jozoor // get standard font list
						);
                        
				        $of_options[] = array( 	"name" 		=> __("Heading Footer Font [Type]", "jozoorthemes"),
						"desc" 		=> __("select font for heading footer text", "jozoorthemes"),
						"id" 		=> "heading_footer_font_type_standard",
						"std" 		=> "Select a font",
                        "fold"      => "using_standard_fonts", /* the checkbox hook */
						"type" 		=> "select",
						"options" 	=> $standard_font_list_jozoor // get standard font list
						);
                
                        $of_options[] = array( 	"name" 		=> __("Down Bar Font [Type]", "jozoorthemes"),
						"desc" 		=> __("select font for down bar footer text", "jozoorthemes"),
						"id" 		=> "downbar_font_type_standard",
						"std" 		=> "Select a font",
                        "fold"      => "using_standard_fonts", /* the checkbox hook */
						"type" 		=> "select",
						"options" 	=> $standard_font_list_jozoor // get standard font list
						);
        
                        $of_options[] = array( 	"name" 		=> __("Sliders Title Font [Type]", "jozoorthemes"),
						"desc" 		=> __("select font for sliders title text", "jozoorthemes"),
						"id" 		=> "title_sliders_font_type_standard",
						"std" 		=> "Select a font",
                        "fold"      => "using_standard_fonts", /* the checkbox hook */
						"type" 		=> "select",
						"options" 	=> $standard_font_list_jozoor // get standard font list
						);
        
                        $of_options[] = array( 	"name" 		=> __("Sliders Description Font [Type]", "jozoorthemes"),
						"desc" 		=> __("select font for sliders description text", "jozoorthemes"),
						"id" 		=> "desc_sliders_font_type_standard",
						"std" 		=> "Select a font",
                        "fold"      => "using_standard_fonts", /* the checkbox hook */
						"type" 		=> "select",
						"options" 	=> $standard_font_list_jozoor // get standard font list
						);
        
// Font Sizes & Styles
$of_options[] = array( "name" => __("Font Sizes & Styles", "jozoorthemes"),
					"desc" => "",
					"id" => "font_sizes_info",
					"std" => "<h3 style=\"margin: 0;\">".__("Font Sizes & Styles", "jozoorthemes")."</h3>",
					"icon" => true,
					"type" => "info");
        
/* folds */                    
$of_options[] = array( "name" => __("Update Font Sizes & Styles", "jozoorthemes"),
					"desc" => __("Here you can change font sizes & styles in your site, if you check [ ON ] you can do that", "jozoorthemes").".",
					"id" => "update_font_sizes_styles",
					"std" => 0,
          			"folds" => 0,
                    "on" 		=> __("ON", "jozoorthemes"),
					"off" 		=> __("OFF", "jozoorthemes"),
					"type" => "switch");  
        
                    $of_options[] = array( 	"name" 		=> __("Body Font", "jozoorthemes"),
						"desc" 		=> __("select font [size&style] for body text", "jozoorthemes"),
						"id" 		=> "body_font_sizes_styles",
						"std"       => array('size' => '14px','style' => 'normal'),
                        "fold"      => "update_font_sizes_styles", /* the checkbox hook */
						"type" 		=> "typography"
						);
        
                     $of_options[] = array( 	"name" 		=> __("Header Menu Font", "jozoorthemes"),
						"desc" 		=> __("select font [size&style] for header menu", "jozoorthemes"),
						"id" 		=> "menu_font_sizes_styles",
						"std"       => array('size' => '16px','style' => 'normal'),
                        "fold"      => "update_font_sizes_styles", /* the checkbox hook */
						"type" 		=> "typography"
						);
        
                      $of_options[] = array( 	"name" 		=> __("Sublevels Menu Font", "jozoorthemes"),
						"desc" 		=> __("select font [size&style] for sublevels menu", "jozoorthemes"),
						"id" 		=> "sublevels_menu_font_sizes_styles",
						"std"       => array('size' => '14px','style' => 'normal'),
                        "fold"      => "update_font_sizes_styles", /* the checkbox hook */
						"type" 		=> "typography"
						);
        
                       $of_options[] = array( 	"name" 		=> __("Top Bar Header Font", "jozoorthemes"),
						"desc" 		=> __("select font [size&style] for top bar header", "jozoorthemes"),
						"id" 		=> "topbar_header_font_sizes_styles",
						"std"       => array('size' => '12px','style' => 'normal'),
                        "fold"      => "update_font_sizes_styles", /* the checkbox hook */
						"type" 		=> "typography"
						);
        
                        $of_options[] = array( 	"name" 		=> __("Slider [1] Font", "jozoorthemes"),
						"desc" 		=> __("select font [size&style] for slider 1", "jozoorthemes"),
						"id" 		=> "slider1_font_sizes_styles",
						"std"       => array('size' => '27px','style' => 'normal'),
                        "fold"      => "update_font_sizes_styles", /* the checkbox hook */
						"type" 		=> "typography"
						);
        
                        $of_options[] = array( 	"name" 		=> __("Slider [2] Font", "jozoorthemes"),
						"desc" 		=> __("select font [size&style] for slider 2", "jozoorthemes"),
						"id" 		=> "slider2_font_sizes_styles",
						"std"       => array('size' => '42px','style' => 'normal'),
                        "fold"      => "update_font_sizes_styles", /* the checkbox hook */
						"type" 		=> "typography"
						);
        
                        $of_options[] = array( 	"name" 		=> __("Sliders Description Font", "jozoorthemes"),
						"desc" 		=> __("select font [size&style] for sliders description", "jozoorthemes"),
						"id" 		=> "desc_sliders_font_sizes_styles",
						"std"       => array('size' => '18px','style' => 'normal'),
                        "fold"      => "update_font_sizes_styles", /* the checkbox hook */
						"type" 		=> "typography"
						);
        
                        $of_options[] = array( 	"name" 		=> __("Page Title Font", "jozoorthemes"),
						"desc" 		=> __("select font [size&style] for page title", "jozoorthemes"),
						"id" 		=> "page_title_font_sizes_styles",
						"std"       => array('size' => '46px','style' => 'normal'),
                        "fold"      => "update_font_sizes_styles", /* the checkbox hook */
						"type" 		=> "typography"
						);
        
                        $of_options[] = array( 	"name" 		=> __("Heading Body Font", "jozoorthemes"),
						"desc" 		=> __("select font [size&style] for heading body", "jozoorthemes"),
						"id" 		=> "heading_body_font_sizes_styles",
						"std"       => array('size' => '26px','style' => 'normal'),
                        "fold"      => "update_font_sizes_styles", /* the checkbox hook */
						"type" 		=> "typography"
						);
        
                        $of_options[] = array( 	"name" 		=> __("Footer Font", "jozoorthemes"),
						"desc" 		=> __("select font [size&style] for footer", "jozoorthemes"),
						"id" 		=> "footer_font_sizes_styles",
						"std"       => array('size' => '15px','style' => 'normal'),
                        "fold"      => "update_font_sizes_styles", /* the checkbox hook */
						"type" 		=> "typography"
						);
        
                        $of_options[] = array( 	"name" 		=> __("Heading Footer Font", "jozoorthemes"),
						"desc" 		=> __("select font [size&style] for heading footer", "jozoorthemes"),
						"id" 		=> "heading_footer_font_sizes_styles",
						"std"       => array('size' => '23px','style' => 'normal'),
                        "fold"      => "update_font_sizes_styles", /* the checkbox hook */
						"type" 		=> "typography"
						);
        
                        $of_options[] = array( 	"name" 		=> __("Down Bar footer Font", "jozoorthemes"),
						"desc" 		=> __("select font [size&style] for down bar footer", "jozoorthemes"),
						"id" 		=> "downbar_footer_font_sizes_styles",
						"std"       => array('size' => '12px','style' => 'normal'),
                        "fold"      => "update_font_sizes_styles", /* the checkbox hook */
						"type" 		=> "typography"
						);
        
                        $of_options[] = array( 	"name" 		=> __("H1 Heading Font", "jozoorthemes"),
						"desc" 		=> __("select font [size&style] for H1", "jozoorthemes"),
						"id" 		=> "h1_font_sizes_styles",
						"std"       => array('size' => '46px','style' => 'normal'),
                        "fold"      => "update_font_sizes_styles", /* the checkbox hook */
						"type" 		=> "typography"
						);
        
                        $of_options[] = array( 	"name" 		=> __("H2 Heading Font", "jozoorthemes"),
						"desc" 		=> __("select font [size&style] for H2", "jozoorthemes"),
						"id" 		=> "h2_font_sizes_styles",
						"std"       => array('size' => '35px','style' => 'normal'),
                        "fold"      => "update_font_sizes_styles", /* the checkbox hook */
						"type" 		=> "typography"
						);
        
                        $of_options[] = array( 	"name" 		=> __("H3 Heading Font", "jozoorthemes"),
						"desc" 		=> __("select font [size&style] for H3", "jozoorthemes"),
						"id" 		=> "h3_font_sizes_styles",
						"std"       => array('size' => '28px','style' => 'normal'),
                        "fold"      => "update_font_sizes_styles", /* the checkbox hook */
						"type" 		=> "typography"
						);
        
                        $of_options[] = array( 	"name" 		=> __("H4 Heading Font", "jozoorthemes"),
						"desc" 		=> __("select font [size&style] for H4", "jozoorthemes"),
						"id" 		=> "h4_font_sizes_styles",
						"std"       => array('size' => '21px','style' => 'normal'),
                        "fold"      => "update_font_sizes_styles", /* the checkbox hook */
						"type" 		=> "typography"
						);
        
                        $of_options[] = array( 	"name" 		=> __("H5 Heading Font", "jozoorthemes"),
						"desc" 		=> __("select font [size&style] for H5", "jozoorthemes"),
						"id" 		=> "h5_font_sizes_styles",
						"std"       => array('size' => '17px','style' => 'normal'),
                        "fold"      => "update_font_sizes_styles", /* the checkbox hook */
						"type" 		=> "typography"
						);
        
                        $of_options[] = array( 	"name" 		=> __("H6 Heading Font", "jozoorthemes"),
						"desc" 		=> __("select font [size&style] for H6", "jozoorthemes"),
						"id" 		=> "h6_font_sizes_styles",
						"std"       => array('size' => '14px','style' => 'normal'),
                        "fold"      => "update_font_sizes_styles", /* the checkbox hook */
						"type" 		=> "typography"
						);
        
// Font Line Heights
$of_options[] = array( "name" => __("Font Line Heights", "jozoorthemes"),
					"desc" => "",
					"id" => "font_lineheights_info",
					"std" => "<h3 style=\"margin: 0;\">".__("Font Line Heights", "jozoorthemes")."</h3>",
					"icon" => true,
					"type" => "info");
        
/* folds */                    
$of_options[] = array( "name" => __("Update Font Line Heights", "jozoorthemes"),
					"desc" => __("Here you can change font line heights in your site, if you check [ ON ] you can do that", "jozoorthemes").".",
					"id" => "update_font_lineheights",
					"std" => 0,
          			"folds" => 0,
                    "on" 		=> __("ON", "jozoorthemes"),
					"off" 		=> __("OFF", "jozoorthemes"),
					"type" => "switch");  
        
                    $of_options[] = array( 	"name" 		=> __("Body Font [ Line Height ]", "jozoorthemes"),
						"desc" 		=> __("select font [line height] for body text", "jozoorthemes"),
						"id" 		=> "body_font_lineheights",
						"std"       => "23px",
                        "fold"      => "update_font_lineheights", /* the checkbox hook */
						"type"      => "select",
					    "options"   => $fonts_line_heights
						);
        
                    $of_options[] = array( 	"name" 		=> __("H1 - Heading Font [ Line Height ]", "jozoorthemes"),
						"desc" 		=> __("select font [line height] for h1", "jozoorthemes"),
						"id" 		=> "h1_font_lineheights",
						"std"       => "50px",
                        "fold"      => "update_font_lineheights", /* the checkbox hook */
						"type"      => "select",
					    "options"   => $fonts_line_heights
						);
        
                    $of_options[] = array( 	"name" 		=> __("H2 - Heading Font [ Line Height ]", "jozoorthemes"),
						"desc" 		=> __("select font [line height] for h2", "jozoorthemes"),
						"id" 		=> "h2_font_lineheights",
						"std"       => "40px",
                        "fold"      => "update_font_lineheights", /* the checkbox hook */
						"type"      => "select",
					    "options"   => $fonts_line_heights
						);
        
                    $of_options[] = array( 	"name" 		=> __("H3 - Heading Font [ Line Height ]", "jozoorthemes"),
						"desc" 		=> __("select font [line height] for h3", "jozoorthemes"),
						"id" 		=> "h3_font_lineheights",
						"std"       => "34px",
                        "fold"      => "update_font_lineheights", /* the checkbox hook */
						"type"      => "select",
					    "options"   => $fonts_line_heights
						);
        
                    $of_options[] = array( 	"name" 		=> __("H4 - Heading Font [ Line Height ]", "jozoorthemes"),
						"desc" 		=> __("select font [line height] for h4", "jozoorthemes"),
						"id" 		=> "h4_font_lineheights",
						"std"       => "30px",
                        "fold"      => "update_font_lineheights", /* the checkbox hook */
						"type"      => "select",
					    "options"   => $fonts_line_heights
						);
        
                     $of_options[] = array( 	"name" 		=> __("H5 - Heading Font [ Line Height ]", "jozoorthemes"),
						"desc" 		=> __("select font [line height] for h5", "jozoorthemes"),
						"id" 		=> "h5_font_lineheights",
						"std"       => "24px",
                        "fold"      => "update_font_lineheights", /* the checkbox hook */
						"type"      => "select",
					    "options"   => $fonts_line_heights
						);
        
                     $of_options[] = array( 	"name" 		=> __("H6 - Heading Font [ Line Height ]", "jozoorthemes"),
						"desc" 		=> __("select font [line height] for h6", "jozoorthemes"),
						"id" 		=> "h6_font_lineheights",
						"std"       => "21px",
                        "fold"      => "update_font_lineheights", /* the checkbox hook */
						"type"      => "select",
					    "options"   => $fonts_line_heights
						);
        
        
/* ======================================== Custom CSS ============================================*/
$of_options[] = array( "name" => __("Custom CSS", "jozoorthemes"),
						"type" 		=> "heading",
						"icon"		=> ADMIN_IMAGES . "icon-css.png"
				);
                
// Custom CSS Styles
$of_options[] = array( "name" => __("Custom CSS Styles", "jozoorthemes"),
					"desc" => "",
					"id" => "custom_css_styles_info",
					"std" => "<h3 style=\"margin: 0;\">".__("Custom CSS Styles", "jozoorthemes")."</h3>",
					"icon" => true,
					"type" => "info");
                    
                    /* folds */
                    $of_options[] = array( "name" => __("Add Custom CSS Code", "jozoorthemes"),
					"desc" => __("Here you can add Custom CSS Code, if you check [ ON ] you can do that", "jozoorthemes").".",
					"id" => "custom_css",
					"std" => 0,
          			"folds" => 0,
                    "on" 		=> __("ON", "jozoorthemes"),
					"off" 		=> __("OFF", "jozoorthemes"),
					"type" => "switch");  

                    $of_options[] = array( "name" => __("Main CSS Code", "jozoorthemes"),
                    "desc" => __("add some CSS codes to your site by adding it to this block", "jozoorthemes").".",
                    "id" => "custom_css_code",
                    "std" => "",
                    "fold" => "custom_css", /* the checkbox hook */
                    "type" => "textarea");
        
                    $of_options[] = array( "name" => __("Tablets Landscape [ 1024x768 ] CSS Code", "jozoorthemes"),
                    "desc" => __("add some CSS codes for tablets landscape to your site by adding it to this block", "jozoorthemes").".",
                    "id" => "tablets_landscape_custom_css_code",
                    "std" => "",
                    "fold" => "custom_css", /* the checkbox hook */
                    "type" => "textarea");
        
                    $of_options[] = array( "name" => __("Tablets Portrait [ 768x1024 ] CSS Code", "jozoorthemes"),
                    "desc" => __("add some CSS codes for tablets portrait to your site by adding it to this block", "jozoorthemes").".",
                    "id" => "tablets_portrait_custom_css_code",
                    "std" => "",
                    "fold" => "custom_css", /* the checkbox hook */
                    "type" => "textarea");
        
                    $of_options[] = array( "name" => __("Mobiles Landscape [ 480x320 ] CSS Code", "jozoorthemes"),
                    "desc" => __("add some CSS codes for mobiles landscape to your site by adding it to this block", "jozoorthemes").".",
                    "id" => "mobiles_landscape_custom_css_code",
                    "std" => "",
                    "fold" => "custom_css", /* the checkbox hook */
                    "type" => "textarea");
        
                    $of_options[] = array( "name" => __("Mobiles Portrait [ 320x480 ] CSS Code", "jozoorthemes"),
                    "desc" => __("add some CSS codes for mobiles portrait to your site by adding it to this block", "jozoorthemes").".",
                    "id" => "mobiles_portrait_custom_css_code",
                    "std" => "",
                    "fold" => "custom_css", /* the checkbox hook */
                    "type" => "textarea");
                    
                    
/* ======================================== Home 1 Options ============================================*/
$of_options[] = array( "name" => __("Home Options", "jozoorthemes"),
						"type" 		=> "heading",
						"icon"		=> ADMIN_IMAGES . "icon-home.png"
				);
        
// home 1 options
$of_options[] = array( "name" => __("Home [ 1 ] options", "jozoorthemes"),
					"desc" => "",
					"id" => "home1_options",
					"std" => "<h3 style=\"margin: 0;\">".__("Home [ 1 ] options", "jozoorthemes")."</h3>",
					"icon" => true,
					"type" => "info");
        
$of_options[] = array( "name" => __("Services Number", "jozoorthemes"),
                            "desc" => __("define number of services", "jozoorthemes").".",
                            "id" => "home1_services_num",
                            "std" => "3",
                            "mod" => "mini",
                            "type" => "text");
        
$of_options[] = array( "name" => __("Projects Number", "jozoorthemes"),
                            "desc" => __("define number of projects", "jozoorthemes").".",
                            "id" => "home1_projects_num",
                            "std" => "12",
                            "mod" => "mini",
                            "type" => "text");
        
$of_options[] = array( "name" => __("Projects Slides Number in Slider", "jozoorthemes"),
                            "desc" => __("define number of slides in projects slider", "jozoorthemes").".",
                            "id" => "home1_projects_slides_num",
                            "std" => "2",
                            "mod" => "mini",
                            "type" => "text");
        
$of_options[] = array( "name" => __("News Number", "jozoorthemes"),
                            "desc" => __("define number of news", "jozoorthemes").".",
                            "id" => "home1_news_num",
                            "std" => "3",
                            "mod" => "mini",
                            "type" => "text");
        
$of_options[] = array( "name" => __("Select Blog Page for [+] more link in latest news section", "jozoorthemes"),
					"desc" => __("Here you can select page name , which define page to go when click on + link in latest news section", "jozoorthemes").".",
					"id" => "home1_latest_news_blog_page_link",
					"std" => "Select Page Name :",
					"type" => "select",
					"options" => $of_pages);
        
$of_options[] = array( "name" => __("Clients Number", "jozoorthemes"),
                            "desc" => __("define number of clients", "jozoorthemes").".",
                            "id" => "home1_clients_num",
                            "std" => "8",
                            "mod" => "mini",
                            "type" => "text");
        
$of_options[] = array( "name" => __("Clients Slides Number in Slider", "jozoorthemes"),
                            "desc" => __("define number of slides in clients slider", "jozoorthemes").".",
                            "id" => "home1_clients_slides_num",
                            "std" => "2",
                            "mod" => "mini",
                            "type" => "text");
        
// home 2 options
$of_options[] = array( "name" => __("Home [ 2 ] options", "jozoorthemes"),
					"desc" => "",
					"id" => "home2_options",
					"std" => "<h3 style=\"margin: 0;\">".__("Home [ 2 ] options", "jozoorthemes")."</h3>",
					"icon" => true,
					"type" => "info");
        
$of_options[] = array( "name" => __("Services Number", "jozoorthemes"),
                            "desc" => __("define number of services", "jozoorthemes").".",
                            "id" => "home2_services_num",
                            "std" => "4",
                            "mod" => "mini",
                            "type" => "text");
        
$of_options[] = array( "name" => __("Projects Number", "jozoorthemes"),
                            "desc" => __("define number of projects", "jozoorthemes").".",
                            "id" => "home2_projects_num",
                            "std" => "12",
                            "mod" => "mini",
                            "type" => "text");
        
$of_options[] = array( "name" => __("Projects Slides Number in Slider", "jozoorthemes"),
                            "desc" => __("define number of slides in projects slider", "jozoorthemes").".",
                            "id" => "home2_projects_slides_num",
                            "std" => "2",
                            "mod" => "mini",
                            "type" => "text");
        
$of_options[] = array( "name" => __("News Number", "jozoorthemes"),
                            "desc" => __("define number of news", "jozoorthemes").".",
                            "id" => "home2_news_num",
                            "std" => "2",
                            "mod" => "mini",
                            "type" => "text");
        
$of_options[] = array( "name" => __("Testimonials Number", "jozoorthemes"),
                            "desc" => __("define number of testimonials, let field empty to show all testimonials", "jozoorthemes").".",
                            "id" => "home2_testimonials_num",
                            "std" => "",
                            "mod" => "mini",
                            "type" => "text");
        
$of_options[] = array( "name" => __("Clients Number", "jozoorthemes"),
                            "desc" => __("define number of clients", "jozoorthemes").".",
                            "id" => "home2_clients_num",
                            "std" => "8",
                            "mod" => "mini",
                            "type" => "text");
        
$of_options[] = array( "name" => __("Clients Slides Number in Slider", "jozoorthemes"),
                            "desc" => __("define number of slides in clients slider", "jozoorthemes").".",
                            "id" => "home2_clients_slides_num",
                            "std" => "2",
                            "mod" => "mini",
                            "type" => "text");
        
// home 3 options
$of_options[] = array( "name" => __("Home [ 3 ] options", "jozoorthemes"),
					"desc" => "",
					"id" => "home3_options",
					"std" => "<h3 style=\"margin: 0;\">".__("Home [ 3 ] options", "jozoorthemes")."</h3>",
					"icon" => true,
					"type" => "info");
        
$of_options[] = array( "name" => __("Services Number", "jozoorthemes"),
                            "desc" => __("define number of services", "jozoorthemes").".",
                            "id" => "home3_services_num",
                            "std" => "6",
                            "mod" => "mini",
                            "type" => "text");
        
$of_options[] = array( "name" => __("Projects Number", "jozoorthemes"),
                            "desc" => __("define number of projects", "jozoorthemes").".",
                            "id" => "home3_projects_num",
                            "std" => "12",
                            "mod" => "mini",
                            "type" => "text");
        
$of_options[] = array( "name" => __("Projects Slides Number in Slider", "jozoorthemes"),
                            "desc" => __("define number of slides in projects slider", "jozoorthemes").".",
                            "id" => "home3_projects_slides_num",
                            "std" => "2",
                            "mod" => "mini",
                            "type" => "text");
        
$of_options[] = array( "name" => __("News Number", "jozoorthemes"),
                            "desc" => __("define number of news", "jozoorthemes").".",
                            "id" => "home3_news_num",
                            "std" => "3",
                            "mod" => "mini",
                            "type" => "text");
        
$of_options[] = array( "name" => __("Select Blog Page for [+] more link in latest news section", "jozoorthemes"),
					"desc" => __("Here you can select page name , which define page to go when click on + link in latest news section", "jozoorthemes").".",
					"id" => "home3_latest_news_blog_page_link",
					"std" => "Select Page Name :",
					"type" => "select",
					"options" => $of_pages);
        
$of_options[] = array( "name" => __("Clients Number", "jozoorthemes"),
                            "desc" => __("define number of clients", "jozoorthemes").".",
                            "id" => "home3_clients_num",
                            "std" => "8",
                            "mod" => "mini",
                            "type" => "text");
        
$of_options[] = array( "name" => __("Clients Slides Number in Slider", "jozoorthemes"),
                            "desc" => __("define number of slides in clients slider", "jozoorthemes").".",
                            "id" => "home3_clients_slides_num",
                            "std" => "2",
                            "mod" => "mini",
                            "type" => "text");
        
// home 4 options
$of_options[] = array( "name" => __("Home [ 4 ] options", "jozoorthemes"),
					"desc" => "",
					"id" => "home4_options",
					"std" => "<h3 style=\"margin: 0;\">".__("Home [ 4 ] options", "jozoorthemes")."</h3>",
					"icon" => true,
					"type" => "info");
        
$of_options[] = array( "name" => __("Projects Number", "jozoorthemes"),
                            "desc" => __("define number of projects", "jozoorthemes").".",
                            "id" => "home4_projects_num",
                            "std" => "12",
                            "mod" => "mini",
                            "type" => "text");
        
$of_options[] = array( "name" => __("Projects Slides Number in Slider", "jozoorthemes"),
                            "desc" => __("define number of slides in projects slider", "jozoorthemes").".",
                            "id" => "home4_projects_slides_num",
                            "std" => "2",
                            "mod" => "mini",
                            "type" => "text");
        
$of_options[] = array( "name" => __("News Number", "jozoorthemes"),
                            "desc" => __("define number of news", "jozoorthemes").".",
                            "id" => "home4_news_num",
                            "std" => "3",
                            "mod" => "mini",
                            "type" => "text");
        
$of_options[] = array( "name" => __("Select Blog Page for [+] more link in latest news section", "jozoorthemes"),
					"desc" => __("Here you can select page name , which define page to go when click on + link in latest news section", "jozoorthemes").".",
					"id" => "home4_latest_news_blog_page_link",
					"std" => "Select Page Name :",
					"type" => "select",
					"options" => $of_pages);
        
$of_options[] = array( "name" => __("Clients Number", "jozoorthemes"),
                            "desc" => __("define number of clients", "jozoorthemes").".",
                            "id" => "home4_clients_num",
                            "std" => "8",
                            "mod" => "mini",
                            "type" => "text");
        
$of_options[] = array( "name" => __("Clients Slides Number in Slider", "jozoorthemes"),
                            "desc" => __("define number of slides in clients slider", "jozoorthemes").".",
                            "id" => "home4_clients_slides_num",
                            "std" => "2",
                            "mod" => "mini",
                            "type" => "text");
					
                
/* ======================================== Sliders Options ==========================================*/
$of_options[] = array( "name" => __("Sliders Options", "jozoorthemes"),
						"type" 		=> "heading",
						"icon"		=> ADMIN_IMAGES . "icon-slider.png"
				);

// Slider 1 Settings
$of_options[] = array( "name" => __("Slider 1 Settings", "jozoorthemes"),
					"desc" => "",
					"id" => "slider1_info",
					"std" => "<h3 style=\"margin: 0;\">".__("Slider 1 Settings", "jozoorthemes")."</h3>",
					"icon" => true,
					"type" => "info");
        
$of_options[] = array( "name" => __("Animation Loop", "jozoorthemes"),
					"desc" => __("if you checked off, directionNav will received 'disable' classes at either end", "jozoorthemes"),
					"id" => "slider1_animationloop",
					"std" => 1,
					"on" 		=> __("ON", "jozoorthemes"),
					"off" 		=> __("OFF", "jozoorthemes"),
					"type" => "switch");

$of_options[] = array( "name" => __("Slideshow", "jozoorthemes"),
					"desc" => __("Animate slider automatically", "jozoorthemes"),
					"id" => "slider1_slideshow",
					"std" => 1,
					"on" 		=> __("ON", "jozoorthemes"),
					"off" 		=> __("OFF", "jozoorthemes"),
					"type" => "switch");

$of_options[] = array( "name" => __("Slideshow Speed", "jozoorthemes"),
					"desc" => __("Set the speed of the slideshow cycling, in milliseconds : 6000 = 6 seconds", "jozoorthemes"),
					"id" => "slider1_slideshowSpeed",
					"std" => "6000",
                    "mod" => "mini",
					"type" => "text");

$of_options[] = array( "name" => __("Animation Speed", "jozoorthemes"),
					"desc" => __("Pause the slideshow when interacting with control elements, highly recommended : 800 = .8 second", "jozoorthemes"),
					"id" => "slider1_animationSpeed",
					"std" => "800",
                    "mod" => "mini",
					"type" => "text");

$of_options[] = array( "name" => __("Pause On Hover", "jozoorthemes"),
					"desc" => __("make slider Pause On Hover or not", "jozoorthemes"),
					"id" => "slider1_pauseOnHover",
					"std" => 1,
					"on" 		=> __("ON", "jozoorthemes"),
					"off" 		=> __("OFF", "jozoorthemes"),
					"type" => "switch");
                    
$of_options[] = array( "name" => __("Pause On Action", "jozoorthemes"),
					"desc" => __("make slider Pause On Action or not", "jozoorthemes"),
					"id" => "slider1_pauseOnAction",
					"std" => 0,
					"on" 		=> __("ON", "jozoorthemes"),
					"off" 		=> __("OFF", "jozoorthemes"),
					"type" => "switch");
                    
$of_options[] = array( "name" => __("Direction Nav", "jozoorthemes"),
					"desc" => __("show slider Direction Nav or not", "jozoorthemes"),
					"id" => "slider1_directionNav",
					"std" => 1,
                    "on" 		=> __("ON", "jozoorthemes"),
					"off" 		=> __("OFF", "jozoorthemes"),
					"type" => "switch"); 
        
// Slider 2 Settings
$of_options[] = array( "name" => __("Slider 2 Settings", "jozoorthemes"),
					"desc" => "",
					"id" => "slider2_info",
					"std" => "<h3 style=\"margin: 0;\">".__("Slider 2 Settings", "jozoorthemes")."</h3>",
					"icon" => true,
					"type" => "info");
        
$of_options[] = array( "name" => __("Animation Loop", "jozoorthemes"),
					"desc" => __("if you checked off, directionNav will received 'disable' classes at either end", "jozoorthemes"),
					"id" => "slider2_animationloop",
					"std" => 1,
					"on" 		=> __("ON", "jozoorthemes"),
					"off" 		=> __("OFF", "jozoorthemes"),
					"type" => "switch");

$of_options[] = array( "name" => __("Slideshow", "jozoorthemes"),
					"desc" => __("Animate slider automatically", "jozoorthemes"),
					"id" => "slider2_slideshow",
					"std" => 1,
					"on" 		=> __("ON", "jozoorthemes"),
					"off" 		=> __("OFF", "jozoorthemes"),
					"type" => "switch");

$of_options[] = array( "name" => __("Slideshow Speed", "jozoorthemes"),
					"desc" => __("Set the speed of the slideshow cycling, in milliseconds : 6000 = 6 seconds", "jozoorthemes"),
					"id" => "slider2_slideshowSpeed",
					"std" => "6000",
                    "mod" => "mini",
					"type" => "text");

$of_options[] = array( "name" => __("Animation Speed", "jozoorthemes"),
					"desc" => __("Pause the slideshow when interacting with control elements, highly recommended : 800 = .8 second", "jozoorthemes"),
					"id" => "slider2_animationSpeed",
					"std" => "800",
                    "mod" => "mini",
					"type" => "text");

$of_options[] = array( "name" => __("Pause On Hover", "jozoorthemes"),
					"desc" => __("make slider Pause On Hover or not", "jozoorthemes"),
					"id" => "slider2_pauseOnHover",
					"std" => 1,
					"on" 		=> __("ON", "jozoorthemes"),
					"off" 		=> __("OFF", "jozoorthemes"),
					"type" => "switch");
                    
$of_options[] = array( "name" => __("Pause On Action", "jozoorthemes"),
					"desc" => __("make slider Pause On Action or not", "jozoorthemes"),
					"id" => "slider2_pauseOnAction",
					"std" => 0,
					"on" 		=> __("ON", "jozoorthemes"),
					"off" 		=> __("OFF", "jozoorthemes"),
					"type" => "switch");
                    
$of_options[] = array( "name" => __("Direction Nav", "jozoorthemes"),
					"desc" => __("show slider Direction Nav or not", "jozoorthemes"),
					"id" => "slider2_directionNav",
					"std" => 1,
                    "on" 		=> __("ON", "jozoorthemes"),
					"off" 		=> __("OFF", "jozoorthemes"),
					"type" => "switch"); 

/* ======================================== Portfolio Options ============================================*/
$of_options[] = array( "name" => __("Portfolio Options", "jozoorthemes"),
						"type" 		=> "heading",
						"icon"		=> ADMIN_IMAGES . "icon-portfolio.png"
				);
        
// General Options
$of_options[] = array( "name" => __("General Options", "jozoorthemes"),
					"desc" => "",
					"id" => "general_portfolio_info",
					"std" => "<h3 style=\"margin: 0;\">".__("General Options", "jozoorthemes")."</h3>",
					"icon" => true,
					"type" => "info");
        
                    $of_options[] = array( "name" => __("Custom Projects per Page", "jozoorthemes"),
					"desc" => __("Here you can active custom projects per page instead of default reading setting or not", "jozoorthemes").".",
					"id" => "custom_projects_per_page",
					"std" => 0,
                    "on" 		=> __("ON", "jozoorthemes"),
					"off" 		=> __("OFF", "jozoorthemes"),
					"type" => "switch");
        
                            $of_options[] = array( "name" => __("Portfolio [ 4 ] columns / projects per page", "jozoorthemes"),
                            "desc" => __("define number of projects per page for Portfolio 4 columns", "jozoorthemes").".",
                            "id" => "portfolio4_projects_per_page",
                            "std" => "16",
                            "fold" => "custom_projects_per_page", /* the checkbox hook */
                            "mod" => "mini",
                            "type" => "text");
        
                            $of_options[] = array( "name" => __("Portfolio [ 3 ] columns / projects per page", "jozoorthemes"),
                            "desc" => __("define number of projects per page for Portfolio 3 columns", "jozoorthemes").".",
                            "id" => "portfolio3_projects_per_page",
                            "std" => "12",
                            "fold" => "custom_projects_per_page", /* the checkbox hook */
                            "mod" => "mini",
                            "type" => "text");
        
                            $of_options[] = array( "name" => __("Portfolio [ 2 ] columns / projects per page", "jozoorthemes"),
                            "desc" => __("define number of projects per page for Portfolio 2 columns", "jozoorthemes").".",
                            "id" => "portfolio2_projects_per_page",
                            "std" => "8",
                            "fold" => "custom_projects_per_page", /* the checkbox hook */
                            "mod" => "mini",
                            "type" => "text");
        
// Single Project Options
$of_options[] = array( "name" => __("Single Project Options", "jozoorthemes"),
					"desc" => "",
					"id" => "single_project_info",
					"std" => "<h3 style=\"margin: 0;\">".__("Single Project Options", "jozoorthemes")."</h3>",
					"icon" => true,
					"type" => "info");
        
                    $of_options[] = array( "name" => __("Show previous & next project links", "jozoorthemes"),
					"desc" => __("Here you can active previous & next project links or not", "jozoorthemes").".",
					"id" => "single_prev_next_links_project",
					"std" => 1,
                    "on" 		=> __("ON", "jozoorthemes"),
					"off" 		=> __("OFF", "jozoorthemes"),
					"type" => "switch");
        
                    $of_options[] = array( "name" => __("Select Current Page in Nav Menu", "jozoorthemes"),
					"desc" => __("Here you can select page name , which define current page when go to single project page", "jozoorthemes").".",
					"id" => "single_project_current_page",
					"std" => "Select Page Name :",
					"type" => "select",
					"options" => $of_pages);
        
                    $of_options[] = array( "name" => __("Show details section", "jozoorthemes"),
					"desc" => __("Here you can active details section or not", "jozoorthemes").".",
					"id" => "single_details_project",
					"std" => 1,
                    "on" 		=> __("ON", "jozoorthemes"),
					"off" 		=> __("OFF", "jozoorthemes"),
					"type" => "switch");
        
                    $of_options[] = array( "name" => __("Show share buttons", "jozoorthemes"),
					"desc" => __("Here you can active share buttons or not", "jozoorthemes").".",
					"id" => "single_share_buttons_project",
					"std" => 1,
                    "on" 		=> __("ON", "jozoorthemes"),
					"off" 		=> __("OFF", "jozoorthemes"),
					"type" => "switch");
        
                    $of_options[] = array( "name" => __("Show related projects", "jozoorthemes"),
					"desc" => __("Here you can active related projects or not", "jozoorthemes").".",
					"id" => "single_related_projects_project",
					"std" => 1,
                    "on" 		=> __("ON", "jozoorthemes"),
					"off" 		=> __("OFF", "jozoorthemes"),
					"type" => "switch");
        
                    $of_options[] = array( "name" => __("Show comments", "jozoorthemes"),
					"desc" => __("Here you can active default WordPress comments, just make sure you allow comments on project when you add it", "jozoorthemes").".",
					"id" => "single_comments_project",
					"std" => 0,
                    "on" 		=> __("ON", "jozoorthemes"),
					"off" 		=> __("OFF", "jozoorthemes"),
					"type" => "switch");
        
// Category Page
$of_options[] = array( "name" => __("Category Page options", "jozoorthemes"),
					"desc" => "",
					"id" => "projects_category_page_info",
					"std" => "<h3 style=\"margin: 0;\">".__("Category Page options", "jozoorthemes")."</h3>",
					"icon" => true,
					"type" => "info");

$of_options[] = array( "name" => __("Select Current Page in Nav Menu", "jozoorthemes"),
					"desc" => __("Here you can select page name , which define current page when go to [ category ] page", "jozoorthemes").".",
					"id" => "projects_category_current_page",
					"std" => "Select Page Name :",
					"type" => "select",
					"options" => $of_pages);

$of_options[] = array( "name" => __("Select Layout Page Style", "jozoorthemes"),
					"desc" => __("Here you can select layout page style from [ 4 Columns, 3 Columns, 2 Columns ]", "jozoorthemes"),
					"id" => "projects_category_layout_style",
					"std" => "3 Columns",
					"type" => "select",
					"options" => $of_options_projects_category_layout_page_style);
                
/* ======================================== Blog Options ============================================*/
$of_options[] = array( "name" => __("Blog Options", "jozoorthemes"),
						"type" 		=> "heading",
						"icon"		=> ADMIN_IMAGES . "icon-blog.png"
				);
        
// General Options
$of_options[] = array( "name" => __("General Options", "jozoorthemes"),
					"desc" => "",
					"id" => "general_blog_info",
					"std" => "<h3 style=\"margin: 0;\">".__("General Options", "jozoorthemes")."</h3>",
					"icon" => true,
					"type" => "info");
        
                    $of_options[] = array( "name" => __("Custom Posts per Page", "jozoorthemes"),
					"desc" => __("Here you can active custom posts per page instead of default reading setting or not", "jozoorthemes").".",
					"id" => "custom_posts_per_page",
					"std" => 0,
                    "on" 		=> __("ON", "jozoorthemes"),
					"off" 		=> __("OFF", "jozoorthemes"),
					"type" => "switch");
        
                            $of_options[] = array( "name" => __("Blog 1 / posts per page", "jozoorthemes"),
                            "desc" => __("define number of posts per page for blog 1", "jozoorthemes").".",
                            "id" => "blog1_posts_per_page",
                            "std" => "4",
                            "fold" => "custom_posts_per_page", /* the checkbox hook */
                            "mod" => "mini",
                            "type" => "text");
        
                            $of_options[] = array( "name" => __("Blog 2 / posts per page", "jozoorthemes"),
                            "desc" => __("define number of posts per page for blog 2", "jozoorthemes").".",
                            "id" => "blog2_posts_per_page",
                            "std" => "5",
                            "fold" => "custom_posts_per_page", /* the checkbox hook */
                            "mod" => "mini",
                            "type" => "text");
        
                            $of_options[] = array( "name" => __("Blog 3 / posts per page", "jozoorthemes"),
                            "desc" => __("define number of posts per page for blog 3", "jozoorthemes").".",
                            "id" => "blog3_posts_per_page",
                            "std" => "9",
                            "fold" => "custom_posts_per_page", /* the checkbox hook */
                            "mod" => "mini",
                            "type" => "text");
                    
                    $of_options[] = array( "name" => __("Show Views Counter", "jozoorthemes"),
					"desc" => __("Here you can active views counter in blog page and single post or not", "jozoorthemes").".",
					"id" => "blog_views_counter",
					"std" => 1,
                    "on" 		=> __("ON", "jozoorthemes"),
					"off" 		=> __("OFF", "jozoorthemes"),
					"type" => "switch");
        
                    $of_options[] = array( "name" => __("Using Excerpt function", "jozoorthemes"),
					"desc" => __("Here you can active excerpt function instead of add <!--more--> from content editor for blog 1 & blog 2 style or not", "jozoorthemes").".",
					"id" => "blog_using_excerpt",
					"std" => 0,
                    "on" 		=> __("ON", "jozoorthemes"),
					"off" 		=> __("OFF", "jozoorthemes"),
					"type" => "switch");
        
// Single Page Options
$of_options[] = array( "name" => __("Single Page Options", "jozoorthemes"),
					"desc" => "",
					"id" => "single_post_info",
					"std" => "<h3 style=\"margin: 0;\">".__("Single Page Options", "jozoorthemes")."</h3>",
					"icon" => true,
					"type" => "info");
        
                   $of_options[] = array( "name" => __("Show Tags", "jozoorthemes"),
					"desc" => __("Here you can active tags or not", "jozoorthemes").".",
					"id" => "single_post_tags",
					"std" => 1,
                    "on" 		=> __("ON", "jozoorthemes"),
					"off" 		=> __("OFF", "jozoorthemes"),
					"type" => "switch");
        
                    $of_options[] = array( "name" => __("Show previous & next post links", "jozoorthemes"),
					"desc" => __("Here you can active previous & next post links or not", "jozoorthemes").".",
					"id" => "single_prev_next_links",
					"std" => 1,
                    "on" 		=> __("ON", "jozoorthemes"),
					"off" 		=> __("OFF", "jozoorthemes"),
					"type" => "switch");
        
                    $of_options[] = array( "name" => __("Show about author box", "jozoorthemes"),
					"desc" => __("Here you can active about author box or not", "jozoorthemes").".",
					"id" => "single_about_author",
					"std" => 1,
                    "on" 		=> __("ON", "jozoorthemes"),
					"off" 		=> __("OFF", "jozoorthemes"),
					"type" => "switch");
        
                    $of_options[] = array( "name" => __("Show related posts", "jozoorthemes"),
					"desc" => __("Here you can active related posts or not", "jozoorthemes").".",
					"id" => "single_related_posts",
					"std" => 1,
                    "folds" => 1,
                    "on" 		=> __("ON", "jozoorthemes"),
					"off" 		=> __("OFF", "jozoorthemes"),
					"type" => "switch");
                            
                            $of_options[] = array( "name" => __("Number of related posts", "jozoorthemes"),
                            "desc" => __("add number of related posts", "jozoorthemes").".",
                            "id" => "single_related_posts_num",
                            "std" => "3",
                            "fold" => "single_related_posts", /* the checkbox hook */
                            "mod" => "mini",
                            "type" => "text");
        
        
                    /* folds */
                    $of_options[] = array( "name" => __("Add custom content top or down post entry", "jozoorthemes"),
					"desc" => __("Here you can add custom content top or down post entry, if you check [ ON ] you can do that", "jozoorthemes").".",
					"id" => "single_custom_entry",
					"std" => 0,
          			"folds" => 0,
                    "on" 		=> __("ON", "jozoorthemes"),
					"off" 		=> __("OFF", "jozoorthemes"),
					"type" => "switch");  

                    $of_options[] = array( "name" => __("Top post entry", "jozoorthemes"),
                    "desc" => __("add custom content in top post entry", "jozoorthemes").".",
                    "id" => "custom_content_top_entry",
                    "std" => "",
                    "fold" => "single_custom_entry", /* the checkbox hook */
                    "type" => "textarea");
        
                    $of_options[] = array( "name" => __("Down post entry", "jozoorthemes"),
                    "desc" => __("add custom content in down post entry", "jozoorthemes").".",
                    "id" => "custom_content_down_entry",
                    "std" => "",
                    "fold" => "single_custom_entry", /* the checkbox hook */
                    "type" => "textarea");
        
                    $of_options[] = array( "name" => __("Select Current Page in Nav Menu", "jozoorthemes"),
					"desc" => __("Here you can select page name , which define current page when go to single post page", "jozoorthemes").".",
					"id" => "single_post_current_page",
					"std" => "Select Page Name :",
					"type" => "select",
					"options" => $of_pages);

                    $of_options[] = array( "name" => __("Select Layout Page Style", "jozoorthemes"),
					"desc" => __("Here you can select layout page style from [ Right Sidebar, Left Sidebar, Full Width ]", "jozoorthemes"),
					"id" => "single_layout_page_style",
					"std" => "Right Sidebar",
					"type" => "select",
					"options" => $of_options_layout_page_style);
                    
                    $of_options[] = array( "name" => __("Select Sidebar Type", "jozoorthemes"),
					"desc" => __("Here you can select sidebar type from [ Default Sidebar, Sidebar2, Sidebar3, Sidebar4, Sidebar5 ]", "jozoorthemes"),
					"id" => "single_sidebar_type",
					"std" => "Default Sidebar",
					"type" => "select",
					"options" => $of_options_sidebar_type);
        
// Other Pages in Blog
$of_options[] = array( "name" => __("Other Pages in Blog section options", "jozoorthemes"),
					"desc" => "",
					"id" => "other_blog_pages_info",
					"std" => "<h3 style=\"margin: 0;\">".__("Other Pages in Blog section options, like [ archive, categories, author, tags ] page", "jozoorthemes")."</h3>",
					"icon" => true,
					"type" => "info");

$of_options[] = array( "name" => __("Select Current Page in Nav Menu", "jozoorthemes"),
					"desc" => __("Here you can select page name , which define current page when go to [ archive, categories, author, tags ] page", "jozoorthemes").".",
					"id" => "other_blog_pages_current_page",
					"std" => "Select Page Name :",
					"type" => "select",
					"options" => $of_pages);

$of_options[] = array( "name" => __("Select Layout Page Style", "jozoorthemes"),
					"desc" => __("Here you can select layout page style from [ Right Sidebar, Left Sidebar, Full Width ]", "jozoorthemes"),
					"id" => "other_blog_pages_layout_style",
					"std" => "Right Sidebar",
					"type" => "select",
					"options" => $of_options_layout_page_style);

$of_options[] = array( "name" => __("Select Sidebar Type", "jozoorthemes"),
					"desc" => __("Here you can select sidebar type from [ Default Sidebar, Sidebar2, Sidebar3, Sidebar4, Sidebar5 ]", "jozoorthemes"),
					"id" => "other_blog_pages_sidebar_type",
					"std" => "Default Sidebar",
					"type" => "select",
					"options" => $of_options_sidebar_type);
        
$of_options[] = array( "name" => __("Select Blog Style", "jozoorthemes"),
					"desc" => __("Here you can select blog style from [ Blog1, Blog2, Blog3 ]", "jozoorthemes"),
					"id" => "other_blog_pages_blog_style",
					"std" => "Blog1",
					"type" => "select",
					"options" => $of_options_blog_style);
        
                   
/* ======================================== Services Options ============================================*/
$of_options[] = array( "name" => __("Services Options", "jozoorthemes"),
						"type" 		=> "heading",
						"icon"		=> ADMIN_IMAGES . "icon-services.png"
				);      
        
// Single Page Options
$of_options[] = array( "name" => __("Single Page Options", "jozoorthemes"),
					"desc" => "",
					"id" => "single_service_post_info",
					"std" => "<h3 style=\"margin: 0;\">".__("Single Page Options", "jozoorthemes")."</h3>",
					"icon" => true,
					"type" => "info");
        
                    $of_options[] = array( "name" => __("Select Current Page in Nav Menu", "jozoorthemes"),
					"desc" => __("Here you can select page name , which define current page when go to single service page", "jozoorthemes").".",
					"id" => "single_service_post_current_page",
					"std" => "Select Page Name :",
					"type" => "select",
					"options" => $of_pages);

                    $of_options[] = array( "name" => __("Select Layout Page Style", "jozoorthemes"),
					"desc" => __("Here you can select layout page style from [ Right Sidebar, Left Sidebar, Full Width ]", "jozoorthemes"),
					"id" => "single_service_layout_page_style",
					"std" => "Right Sidebar",
					"type" => "select",
					"options" => $of_options_layout_page_style);
                    
                    $of_options[] = array( "name" => __("Select Sidebar Type", "jozoorthemes"),
					"desc" => __("Here you can select sidebar type from [ Default Sidebar, Sidebar2, Sidebar3, Sidebar4, Sidebar5 ]", "jozoorthemes"),
					"id" => "single_service_sidebar_type",
					"std" => "Default Sidebar",
					"type" => "select",
					"options" => $of_options_sidebar_type);
        
                    $of_options[] = array( "name" => __("Show comments", "jozoorthemes"),
					"desc" => __("Here you can active default WordPress comments, just make sure you allow comments on service when you add it", "jozoorthemes").".",
					"id" => "single_comments_service",
					"std" => 0,
                    "on" 		=> __("ON", "jozoorthemes"),
					"off" 		=> __("OFF", "jozoorthemes"),
					"type" => "switch");
        
        
/* ======================================== Galleries Options ============================================*/
$of_options[] = array( "name" => __("Galleries Options", "jozoorthemes"),
						"type" 		=> "heading",
						"icon"		=> ADMIN_IMAGES . "icon-galleries.png"
				);      
        
// General Options
$of_options[] = array( "name" => __("General Options", "jozoorthemes"),
					"desc" => "",
					"id" => "general_galleries_info",
					"std" => "<h3 style=\"margin: 0;\">".__("General Options", "jozoorthemes")."</h3>",
					"icon" => true,
					"type" => "info");
        
                    $of_options[] = array( "name" => __("Custom Galleries per Page", "jozoorthemes"),
					"desc" => __("Here you can active custom galleries per page instead of default reading setting or not", "jozoorthemes").".",
					"id" => "custom_galleries_per_page",
					"std" => 0,
                    "on" 		=> __("ON", "jozoorthemes"),
					"off" 		=> __("OFF", "jozoorthemes"),
					"type" => "switch");
        
                            $of_options[] = array( "name" => __("Layout [ 4 ] columns / Galleries per page", "jozoorthemes"),
                            "desc" => __("define number of galleries per page for layout 4 columns", "jozoorthemes").".",
                            "id" => "col4_galleries_per_page",
                            "std" => "16",
                            "fold" => "custom_galleries_per_page", /* the checkbox hook */
                            "mod" => "mini",
                            "type" => "text");
        
                            $of_options[] = array( "name" => __("Layout [ 3 ] columns / Galleries per page", "jozoorthemes"),
                            "desc" => __("define number of galleries per page for layout 3 columns", "jozoorthemes").".",
                            "id" => "col3_galleries_per_page",
                            "std" => "12",
                            "fold" => "custom_galleries_per_page", /* the checkbox hook */
                            "mod" => "mini",
                            "type" => "text");
        
                            $of_options[] = array( "name" => __("Layout [ 2 ] columns / Galleries per page", "jozoorthemes"),
                            "desc" => __("define number of galleries per page for layout 2 columns", "jozoorthemes").".",
                            "id" => "col2_galleries_per_page",
                            "std" => "8",
                            "fold" => "custom_galleries_per_page", /* the checkbox hook */
                            "mod" => "mini",
                            "type" => "text");
        
// Single Gallery Options
$of_options[] = array( "name" => __("Gallery Page Options", "jozoorthemes"),
					"desc" => "",
					"id" => "gallery_page_info",
					"std" => "<h3 style=\"margin: 0;\">".__("Gallery Page Options", "jozoorthemes")."</h3>",
					"icon" => true,
					"type" => "info");
        
                    $of_options[] = array( "name" => __("Show previous & next gallery links", "jozoorthemes"),
					"desc" => __("Here you can active previous & next gallery links or not", "jozoorthemes").".",
					"id" => "gallery_page_prev_next_links",
					"std" => 1,
                    "on" 		=> __("ON", "jozoorthemes"),
					"off" 		=> __("OFF", "jozoorthemes"),
					"type" => "switch");
        
                    $of_options[] = array( "name" => __("Select Current Page in Nav Menu", "jozoorthemes"),
					"desc" => __("Here you can select page name , which define current page when go to gallery page", "jozoorthemes").".",
					"id" => "gallery_page_current_page",
					"std" => "Select Page Name :",
					"type" => "select",
					"options" => $of_pages);
        
                    $of_options[] = array( "name" => __("Show comments", "jozoorthemes"),
					"desc" => __("Here you can active default WordPress comments, just make sure you allow comments on gallery when you add it", "jozoorthemes").".",
					"id" => "gallery_page_comments",
					"std" => 0,
                    "on" 		=> __("ON", "jozoorthemes"),
					"off" 		=> __("OFF", "jozoorthemes"),
					"type" => "switch");
        
                                                    				
/* ======================================== Backup Options ============================================*/
$of_options[] = array( "name" => __("Backup Options", "jozoorthemes"),
						"type" 		=> "heading",
						"icon"		=> ADMIN_IMAGES . "icon-backup.png"
				);
                
$of_options[] = array( "name" => __("Backup Options", "jozoorthemes"),
					"desc" => "",
					"id" => "backup_info",
					"std" => "<h3 style=\"margin: 0;\">".__("Backup Options", "jozoorthemes")."</h3>",
					"icon" => true,
					"type" => "info");
				
$of_options[] = array( "name" => __("Backup and Restore Options", "jozoorthemes"),
                    "id" => "of_backup",
                    "std" => "",
                    "type" => "backup",
					"desc" => __("You can use the two buttons below to backup your current options, and then restore it back at a later time. This is useful if you want to experiment on the options but would like to keep the old settings in case you need it back", "jozoorthemes").'.',
					);
				
$of_options[] = array( "name" => __("Transfer Theme Options Data", "jozoorthemes"),
                    "id" => "of_transfer",
                    "std" => "",
                    "type" => "transfer",
					"desc" => __("You can tranfer the saved options data between different installs by copying the text inside the text box. To import data from another install, replace the data in the text box with the one from another install and click 'Import Options' " , "jozoorthemes")
					);	
				
	}//End function: of_options()
}//End chack if function exists: of_options()
?>
