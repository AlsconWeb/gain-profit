<div class="search">
<form action="<?php echo home_url(); ?>" id="searchform" method="get">
    <input type="text" id="s" name="s" class="text-search" value="<?php _e("Search...", "jozoorthemes"); ?>" onBlur="if(this.value == '') { this.value = '<?php _e("Search...", "jozoorthemes"); ?>'; }" 
    onfocus="if(this.value == '<?php _e("Search...", "jozoorthemes"); ?>') { this.value = ''; }" />
    <input type="submit" value="" id="searchsubmit" class="submit-search" />
</form>
</div>
