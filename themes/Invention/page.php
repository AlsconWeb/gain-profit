<?php get_header(); ?>

   <?php 
   // get metaboxes
   global $post;
   $j_page_layout = get_post_meta( $post->ID, '_jozoor_page_layout', true );
   $j_page_sidebar = get_post_meta( $post->ID, '_jozoor_page_sidebar', true );
   // check using builder
   $j_using_page_builder = get_post_meta( $post->ID, '_jozoor_using_page_builder', true ); 
   ?>


   <?php 
   if($j_using_page_builder == 'on') { 

   // get sections
   $j_page_section_data = vp_metabox('jozoor_metabox_page_builder._jozoor_page_section');
       
   // start loop sections
   foreach ( $j_page_section_data as $key => $entry ) {
       
   $j_section_name = vp_metabox('jozoor_metabox_page_builder._jozoor_page_section.'.$key.'.section_name'); // section name
   $j_section_bg_type = vp_metabox('jozoor_metabox_page_builder._jozoor_page_section.'.$key.'.section_bg_type'); // section bg type
   $j_section_bg_type_color = vp_metabox('jozoor_metabox_page_builder._jozoor_page_section.'.$key.'.section_bg_type_color'); // section bg color type
   $j_section_bg_type_color_custom = vp_metabox('jozoor_metabox_page_builder._jozoor_page_section.'.$key.'.section_bg_type_color_custom'); // section bg custom color
   $j_section_bg_type_image = vp_metabox('jozoor_metabox_page_builder._jozoor_page_section.'.$key.'.section_bg_type_image'); // section bg image
   $j_section_bg_type_image_repeat = vp_metabox('jozoor_metabox_page_builder._jozoor_page_section.'.$key.'.section_bg_type_image_repeat'); // section bg image repeat
   $j_section_bg_type_image_parallax = vp_metabox('jozoor_metabox_page_builder._jozoor_page_section.'.$key.'.section_bg_type_image_parallax'); // section bg image parallax
   $j_section_content_text_color = vp_metabox('jozoor_metabox_page_builder._jozoor_page_section.'.$key.'.section_content_text_color'); // section content text color
   $j_section_remove_padding_top = vp_metabox('jozoor_metabox_page_builder._jozoor_page_section.'.$key.'.section_remove_padding_top'); // section remove padding top
   $j_section_remove_padding_bottom = vp_metabox('jozoor_metabox_page_builder._jozoor_page_section.'.$key.'.section_remove_padding_bottom'); // section remove padding bottom
       
   // check bg type
   if($j_section_bg_type == 'image') {
       
   // check bg image parallax to fixed bg
   if($j_section_bg_type_image_parallax == 1) { $j_page_section_parallax_fixed = ' fixed'; } else { $j_page_section_parallax_fixed = ''; }
       
   if(!empty($j_section_bg_type_image_repeat)) { $j_section_bg_image_repeat = $j_section_bg_type_image_repeat; } else { $j_section_bg_image_repeat = 'no-repeat'; }
   if($j_section_bg_image_repeat == 'repeat') {
     $j_section_bg_type_image_style = 'background:url('.$j_section_bg_type_image.') '.$j_section_bg_image_repeat.$j_page_section_parallax_fixed.';';
   } else {
     $j_section_bg_type_image_style = 'background:url('.$j_section_bg_type_image.') '.$j_section_bg_image_repeat.$j_page_section_parallax_fixed.';background-position:center;background-size: cover;-moz-background-size: cover;-webkit-background-size: cover;-o-background-size: cover;';  
   } 
       
   $j_page_section_class_color = $j_page_section_custom_color = ''; 
       
   } else {
       
   if($j_section_bg_type_color == 'defaultcolor') { $j_page_section_class_color = ' default-color'; $j_page_section_custom_color = ''; }
   elseif($j_section_bg_type_color == 'themeskincolor') { $j_page_section_class_color = ' theme-skin-color'; $j_page_section_custom_color = ''; }
   elseif($j_section_bg_type_color == 'customcolor') { $j_page_section_class_color = ''; $j_page_section_custom_color = 'background-color:'.$j_section_bg_type_color_custom.'; '; }
   else { $j_page_section_class_color = ''; $j_page_section_custom_color = ''; }
       
   $j_section_bg_type_image_style = '';
       
   }
       
   // check section padding top
   if($j_section_remove_padding_top == 1) { $j_page_section_padding_top = ' no-padding-top'; } else { $j_page_section_padding_top = ''; }
   // check section padding bottom
   if($j_section_remove_padding_bottom == 1) { $j_page_section_padding_bottom = ' no-padding-bottom'; } else { $j_page_section_padding_bottom = ''; }
       
   // check section bg image parallax
   if($j_section_bg_type_image_parallax == 1) { $j_page_section_parallax_class = ' parallax'; } else { $j_page_section_parallax_class = ''; }
       
   ?>
   
   <div id="<?php echo jozoor_clean_string($j_section_name); ?>" class="page-section<?php echo $j_page_section_parallax_class.$j_page_section_class_color.$j_page_section_padding_top.$j_page_section_padding_bottom; ?>" style="<?php echo $j_page_section_custom_color.$j_section_bg_type_image_style; ?>">
     <div class="container clearfix">
         
     <?php if(!empty($j_section_content_text_color)) { ?>   
     <style>
      #<?php echo jozoor_clean_string($j_section_name); ?> * {
       color:<?php echo $j_section_content_text_color;?>;   
       }
     </style>
     <?php } ?>
     
     <?php
     // get columns
     $j_section_column_data = vp_metabox('jozoor_metabox_page_builder._jozoor_page_section.'.$key.'.section_column');
       
     // start loop columns
     foreach ( $j_section_column_data as $key2 => $entry ) {
       
     $j_section_column_width = vp_metabox('jozoor_metabox_page_builder._jozoor_page_section.'.$key.'.section_column.'.$key2.'.section_column_width'); // column width
     $j_section_column_content = vp_metabox('jozoor_metabox_page_builder._jozoor_page_section.'.$key.'.section_column.'.$key2.'.section_column_content'); // column content
         
     
     if(!empty($j_section_column_width)) {
          echo '<div class="'.$j_section_column_width.'">';    
     } else {
          echo '<div class="sixteen columns">';   
     }
     
     echo '<div class="entry-content post-content">'.apply_filters('the_content', $j_section_column_content ).'</div>';   
         
     echo '</div>';
         
     } // end loop columns ?>
       
     </div><!-- End Container -->
   </div><!-- End section -->
   
   <?php } // end loop sections ?>
   
   <?php } // end page builder

   else { // start normal page
   ?>
   <!-- Start main content -->
   <div class="container main-content clearfix">
       
   <?php if( $j_page_layout == 'left' ) { 
       if( $j_page_sidebar == 'default' ) { get_sidebar(); } else {
       echo '<!-- Start Sidebar Widgets --><aside class="five columns sidebar bottom-3">';
       if (function_exists('dynamic_sidebar') && dynamic_sidebar($j_page_sidebar)) : endif;
       echo '</aside><!-- End Sidebar Widgets -->';
       }
    } ?>
   
   <!-- Start Page Content -->
   <?php if( $j_page_layout == 'right' || $j_page_layout == 'left' ) { ?>
   <div class="eleven columns bottom-3 single-post">
   <?php } else { ?>
   <div class="sixteen columns full-width bottom-3 single-post">    
   <?php } ?>
       
   <?php 

   while ( have_posts() ) : the_post();

   // check post protected 
   if ( !post_password_required() ) { 
      
      if ( has_post_thumbnail() ) {
        if( $j_page_layout == 'full' || !$j_page_layout ) { echo '<div class="sixteen columns">'; }
        echo '<div class="image-post">';
        echo get_the_post_thumbnail($post->ID, 'full', array('title' => ''));
        echo '</div><!-- End image-post -->';
        if( $j_page_layout == 'full' || !$j_page_layout ) { echo '</div><div class="clearfix"></div>'; } 
      } 
   }

   if( !empty( $post->post_content) ) {
   echo '<div class="entry-content post-content bottom-3">';
     the_content();
   echo '</div><!-- End page-content -->';
   }
   
   // comments
   if ( comments_open() || get_comments_number() ) { 
       comments_template(); 
   }

   endwhile; 

   ?>
       
   </div><!-- End Posts -->
       
<?php if( $j_page_layout == 'right' ) { 
   if( $j_page_sidebar == 'default' ) { get_sidebar(); } else {
   echo '<!-- Start Sidebar Widgets --><aside class="five columns sidebar bottom-3">';
   if (function_exists('dynamic_sidebar') && dynamic_sidebar($j_page_sidebar)) : endif;
   echo '</aside><!-- End Sidebar Widgets -->';
   }  
} ?>
       
<?php } // end normal page ?>
    
<?php get_footer(); ?>