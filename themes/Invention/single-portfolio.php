<?php get_header(); ?>

   <?php
   global $smof_data;
   // get menu item id
   global $wpdb;
   $post_id = $wpdb->get_var( 'SELECT ID FROM '.$wpdb->posts.' WHERE post_name="'.$jozoor_single_project_current_page.'" AND post_type="page" AND post_status="publish"' );
   $post_title = $wpdb->get_var( 'SELECT post_id FROM '.$wpdb->postmeta.' WHERE meta_key="_menu_item_object_id" AND meta_value="'.$post_id.'"' );
   
   // add active class for nav item
   if (!is_admin() && $jozoor_single_project_current_page != 'Select Page Name') { ?>
   <script>
    jQuery(document).ready(function($) {
     $("#menu-item-<?php echo $post_title;  ?>").addClass("current-menu-item");
    });
    </script>
   <?php } ?>

   <!-- Start main content -->
   <div class="container main-content clearfix">
       
   <div id="post-<?php the_ID(); ?>" <?php post_class('single-project'); ?>>
       
   <?php 

   while ( have_posts() ) : the_post();

   // check post protected 
   if ( !post_password_required() ) {
    
   // get metaboxs
   $j_project_format = get_post_meta( $post->ID, '_jozoor_project_format', true ); // project format
   $j_project_images = get_post_meta( $post->ID, '_jozoor_project_images', true ); // project images 
   $j_project_images_without_slider = get_post_meta( $post->ID, '_jozoor_project_images_without_slider', true ); // check slider 
   $j_project_images_using_lightbox = get_post_meta( $post->ID, '_jozoor_project_images_using_lightbox', true ); // check lightbox
   $j_project_video_embed_content = get_post_meta( $post->ID, '_jozoor_project_video_embed_content', true ); // video embed
   $j_project_audio_mp3_url = get_post_meta( $post->ID, '_jozoor_project_audio_mp3_url', true ); // audio file
   $j_project_audio_mp3_bg = get_post_meta( $post->ID, '_jozoor_project_audio_mp3_bg', true ); // audio bg
   $j_project_using_soundcloud = get_post_meta( $post->ID, '_jozoor_project_using_soundcloud', true ); // check soundcloud
   $j_project_soundcloud_embed = get_post_meta( $post->ID, '_jozoor_project_soundcloud_embed', true ); // soundcloud embed
   $j_project_page_layout = get_post_meta( $post->ID, '_jozoor_project_page_layout', true ); // page layout
   $j_project_skills = get_post_meta( $post->ID, '_jozoor_project_skills', true ); // project skills
   $j_project_online_url = get_post_meta( $post->ID, '_jozoor_project_online_url', true ); // project url   
   ?>
    
   <!-- Start Project -->
   <?php if( $j_project_page_layout == 'full' ) { ?>
   <div class="sixteen columns top-1 bottom-1">    
   <?php } else { ?>
   <div class="twelve columns top-1 bottom-2">
   <?php } ?>
   
     <?php 
     if( $j_project_format == 'audio' ) {
         
     if( $j_project_using_soundcloud == 'on' ) {
       if ( $j_project_soundcloud_embed ) {   
       echo '<div class="audio entry">';
       $j_htmlcode = wp_oembed_get($j_project_soundcloud_embed);
       echo $j_htmlcode;    
       echo '</div><!-- End audio -->';    
       }  
     } else {
       if ( $j_project_audio_mp3_url ) {   
       echo '<div class="audio entry">';
       if( $j_project_audio_mp3_bg ) {
       echo '<div class="image-post">';
       echo '<img src="'. $j_project_audio_mp3_bg .'" alt="'. get_the_title() .'">'; 
       echo '</div>';
       }
       ?>
       <audio src="<?php echo $j_project_audio_mp3_url; ?>" width="100%"></audio> 
       <?php
       echo '</div><!-- End audio -->';    
       }  
     }
         
     } elseif( $j_project_format == 'video' ) {
     
     if( $j_project_video_embed_content ) {
       echo '<div class="video entry">';
       $j_htmlcode = wp_oembed_get($j_project_video_embed_content);
       echo $j_htmlcode;
       echo '</div><!-- End video -->';
     }
     
     } else {
         
     if ( $j_project_images ) { 
     ?>
       
     <?php if( $j_project_images_without_slider == 'on' ) { ?>
     <ul class="project-full">
     <?php } else { ?>
     <div class="slider-project">
      <div class="flex-container">
	  <div class="flexslider2">
		<ul class="slides">
     <?php } ?>
       
          <?php foreach ( $j_project_images as $attachment_id => $img_full_url ) {
               echo '<li class="item">';
               $j_full_image_url = wp_get_attachment_image_src( $attachment_id, 'full');
               if( $j_project_images_using_lightbox == 'on' ) {
               echo '<a class="fancybox" href="'. $j_full_image_url[0]  .'" rel="'. get_the_title() .'">';
               }
               echo wp_get_attachment_image( $attachment_id, 'full' ); 
               if( $j_project_images_using_lightbox == 'on' ) {
               echo '<i class="icon-search"></i></a>';
               }
               echo '</li>';
          } ?> 
            
        </ul>
     <?php if( $j_project_images_without_slider == 'on' ) { } else { ?>
	  </div>
      </div>
     </div><!-- End --> 
     <?php } ?>
       
     <?php } // end check images 
         
     } ?>
   
   </div>
   <!-- End column -->
       
   <!-- Start Project Details -->
   <?php if( $j_project_page_layout == 'full' ) { ?>
   <div class="clearfix"></div>
   <div class="twelve columns bottom-1">
   <?php } else { ?>
   <div class="four columns top-0 bottom-3">
   <?php } ?>
   
   <?php if( !empty( $post->post_content) ) { ?>
   <h3 class="title"><?php if(!empty($smof_data['static_words_description'])) { echo $smof_data['static_words_description']; } else { _e("Description", "jozoorthemes"); } ?></h3>
   <div class="about-project bottom-2 entry-content post-content">
   <?php the_content(); ?>
   </div>
   <div class="clearfix"></div>
   <?php } ?>
   <?php if( $j_project_online_url ) { 
   if( $j_project_page_layout == 'full' ) { ?>
   <a href="<?php echo $j_project_online_url; ?>" class="button medium color bottom-3" target="_blank"><?php if(!empty($smof_data['static_words_view_project'])) { echo $smof_data['static_words_view_project']; } else { _e("View Project", "jozoorthemes"); } ?></a>
   <?php } } ?>
       
   <?php if( $j_project_page_layout == 'full' ) { ?>
   </div>
   <div class="four columns bottom-3">
   <?php } ?>
       
   <?php if( $jozoor_single_details_project == 1 ) { ?>
   <h3 class="title"><?php if(!empty($smof_data['static_words_details'])) { echo $smof_data['static_words_details']; } else { _e("Details", "jozoorthemes"); } ?></h3>
   <ul class="project-details bottom-2">
   <?php if( $j_project_skills ) { ?>
   <li class="skills"><?php echo $j_project_skills; ?></li>    
   <?php } ?>
   <?php
   $j_terms = get_the_terms( $post->ID, 'portfolio-category' );						
   if ( $j_terms && ! is_wp_error( $j_terms ) ) : 
   $j_draught_links = array();
   foreach ( $j_terms as $term ) {
    $term_link = get_term_link( $term, 'portfolio-category' );
    $j_draught_links[] = '<a href="'.esc_url( $term_link ).'">'.$term->name.'</a>';
   }	
   echo '<li class="cats">';
   echo $j_on_draught = join( ", ", $j_draught_links );
   echo '</li>';
   endif;
   ?>
   <li class="date"><?php the_time('d M, Y'); ?></li>
   </ul>
   <?php } ?>
   
   <?php if( $j_project_online_url ) { 
   if( $j_project_page_layout == 'default' ) { ?>
   <a href="<?php echo $j_project_online_url; ?>" class="button medium color bottom-3" target="_blank"><?php if(!empty($smof_data['static_words_view_project'])) { echo $smof_data['static_words_view_project']; } else { _e("View Project", "jozoorthemes"); } ?></a>
   <?php } } ?>
    
   <?php if( $jozoor_single_share_buttons_project == 1 ) { ?>
   <div class="clearfix"></div>
       
   <h3 class="title bottom-1"><?php if(!empty($smof_data['static_words_share'])) { echo $smof_data['static_words_share']; } else { _e("Share", "jozoorthemes"); } ?></h3>
   <div class="share-social">
   <a href="http://twitter.com/home?status=<?php echo get_the_title(); ?> :+<?php echo get_permalink(); ?>" target="_blank">
   <i class="social_icon-twitter s-17 white"></i></a>
   <a href="http://www.facebook.com/share.php?u=<?php echo get_permalink(); ?>" target="_blank">
   <i class="social_icon-facebook s-18 white"></i></a>
   <a href="https://plus.google.com/share?url=<?php echo get_permalink(); ?>" target="_blank">
   <i class="social_icon-gplus s-18 white"></i></a>
   <a href="http://pinterest.com/pin/create/button/?url=<?php echo get_permalink(); ?>&media=<?php 
   if (has_post_thumbnail( $post->ID ) ) {
   $jozoor_image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' );
   echo $jozoor_image[0];
   }?>&description=<?php echo get_the_title(); ?>" target="_blank">
   <i class="social_icon-pinterest s-18 white"></i></a>
   </div>
       
   <?php } ?>
  
   </div>
   <!-- End -->
       
   <?php // related projects
   if( $jozoor_single_related_projects_project == 1 ) { 
    
   $terms = get_the_terms( $post->ID , 'portfolio-category', 'string');
   if($terms) {
   
   $term_ids = array_values( wp_list_pluck($terms,'term_id') );
   $second_query = new WP_Query( array(
    'post_type' => 'portfolio',
    'tax_query' => array(
        array(
        'taxonomy' => 'portfolio-category',
        'field' => 'id',
        'terms' => $term_ids,
        'operator'=> 'IN'
        )),
    'posts_per_page' => 3,
    'ignore_sticky_posts' => 1,
    'orderby' => 'rand',
    'post__not_in'=>array($post->ID)
   ) );
       
   if($second_query->have_posts()) {
       
   ?>
       
   <div class="sixteen columns clearfix"><hr class="line bottom-3"></div><!-- End line -->
   
   <div class="sixteen columns bottom-1">
   <h3 class="title"><?php if(!empty($smof_data['static_words_related_projects'])) { echo $smof_data['static_words_related_projects']; } else { _e("Related Projects", "jozoorthemes"); } ?></h3>
   </div>
   
   <div class="clearfix"></div>  
       
   <div class="recent-work bottom-2">
   <?php while ($second_query->have_posts() ) : $second_query->the_post(); ?>
     
   <div id="post-<?php the_ID(); ?>" <?php post_class('one-third column item col3'); ?>>
   <a href="<?php echo get_permalink(); ?>">
    <?php echo get_the_post_thumbnail($post->ID, 'portfolio-thumb', array('title' => '')); ?>
    <div class="img-caption">
    <div class="desc">
      <h3><?php the_title(); ?></h3>
      <p><?php
      $j_terms = get_the_terms( $post->ID, 'portfolio-category' );						
      if ( $j_terms && ! is_wp_error( $j_terms ) ) : 
      $j_draught_links = array();
      foreach ( $j_terms as $term ) {
      $j_draught_links[] = $term->name;
      }		
      echo $j_on_draught = join( ", ", $j_draught_links );
      endif;
      ?></p>
      <span>+</span></div>
    </div><!-- hover effect -->
    </a>
   </div>
   <?php endwhile; wp_reset_query(); ?>  
   </div><!-- End related projects -->
       
   <?php } }
       
   } // end related projects ?>
       
   <?php
   } else { 
   echo '<div class="sixteen columns entry-content post-content">';    
   the_content(); 
   echo '</div>';
   }

   // comments 
   if( $jozoor_single_comments_project == 1 ) {
   if ( comments_open() || get_comments_number() ) {
       echo '<div class="sixteen columns bottom-1">';
       comments_template();
       echo '</div>';
   }
   }

   endwhile; 

   ?>

   </div><!-- End project page -->
       
<?php get_footer(); ?>