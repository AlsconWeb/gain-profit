<?php
/*
Template Name: Home
*/
?>

<?php get_header(); ?>

   <?php // check home layout
   global $post;
   $j_home_layout = get_post_meta( $post->ID, '_jozoor_home_layout', true ); 
   
   ?>

   <!-- Start main content -->
   <div class="container clearfix">
       
   <!-- Start Page Content -->       
   <div class="sixteen columns full-width"> 
   <?php 

   // page loop
   while ( have_posts() ) : the_post();

   // check post protected 
   if ( !post_password_required() ) { 
      
      if ( has_post_thumbnail() ) {
        echo '<div class="image-post">';
        echo get_the_post_thumbnail($post->ID, 'full', array('title' => ''));
        echo '</div><!-- End image-post -->';
      } 
   }
   
   if( !empty( $post->post_content) ) {
   echo '<div class="entry-content post-content bottom-3">';
     the_content();
   echo '</div><!-- End page-content -->';
   }

   endwhile; 

   // Reset Query
   wp_reset_query();

   ?>  
   </div><!-- End -->
       
   </div><!-- <<< End Container >>> -->


   <?php // check home layout
   if($j_home_layout == 'home2') {
       
     $j_folder_template_part = 'home-2/';
     
     // home 2 sections
     $j_home_show_services_section = get_post_meta( $post->ID, '_jozoor_home_2_show_services_section', true );
        $j_home_services_section_instead_content = get_post_meta( $post->ID, '_jozoor_home_2_services_section_instead_content', true );
     $j_home_show_recentwork_section = get_post_meta( $post->ID, '_jozoor_home_2_show_recentwork_section', true );
        $j_home_recentwork_section_instead_content = get_post_meta( $post->ID, '_jozoor_home_2_recentwork_section_instead_content', true );
     $j_home_show_latestnews_section = get_post_meta( $post->ID, '_jozoor_home_2_show_latestnews_section', true );
        $j_home_show_latestnews_section_instead_content = get_post_meta( $post->ID, '_jozoor_home_2_show_latestnews_section_instead_content', true );
     $j_home_show_testimonials_section = get_post_meta( $post->ID, '_jozoor_home_2_show_testimonials_section', true );
        $j_home_show_testimonials_section_instead_content = get_post_meta( $post->ID, '_jozoor_home_2_show_testimonials_section_instead_content', true );
     $j_home_show_clients_section = get_post_meta( $post->ID, '_jozoor_home_2_show_clients_section', true );
        $j_home_clients_section_instead_content = get_post_meta( $post->ID, '_jozoor_home_2_clients_section_instead_content', true );
       
     $j_news_column_section = 'two-thirds column';
       
   } 
   elseif($j_home_layout == 'home3') {
       
     $j_folder_template_part = 'home-3/';
     
     // home 3 sections
     $j_home_show_services_section = get_post_meta( $post->ID, '_jozoor_home_3_show_services_section', true );
        $j_home_services_section_instead_content = get_post_meta( $post->ID, '_jozoor_home_3_services_section_instead_content', true );
     $j_home_show_recentwork_section = get_post_meta( $post->ID, '_jozoor_home_3_show_recentwork_section', true );
        $j_home_recentwork_section_instead_content = get_post_meta( $post->ID, '_jozoor_home_3_recentwork_section_instead_content', true );
     $j_home_show_latestnews_section = get_post_meta( $post->ID, '_jozoor_home_3_show_latestnews_section', true );
        $j_home_show_latestnews_section_instead_content = get_post_meta( $post->ID, '_jozoor_home_3_show_latestnews_section_instead_content', true );
     $j_home_show_clients_section = get_post_meta( $post->ID, '_jozoor_home_3_show_clients_section', true );
        $j_home_clients_section_instead_content = get_post_meta( $post->ID, '_jozoor_home_3_clients_section_instead_content', true );
       
     $j_news_column_section = 'sixteen columns full-width';
       
   }
   elseif($j_home_layout == 'home4') {
       
     $j_folder_template_part = 'home-4/';
     
     // home 4 sections
     $j_home_show_welcome_section = get_post_meta( $post->ID, '_jozoor_home_4_show_welcome_section', true );
        $j_home_show_welcome_text = get_post_meta( $post->ID, '_jozoor_home_4_show_welcome_text', true );
        $j_home_welcome_section_instead_content = get_post_meta( $post->ID, '_jozoor_home_4_welcome_section_instead_content', true );
     $j_home_show_recentwork_section = get_post_meta( $post->ID, '_jozoor_home_4_show_recentwork_section', true );
        $j_home_recentwork_section_instead_content = get_post_meta( $post->ID, '_jozoor_home_4_recentwork_section_instead_content', true );
     $j_home_show_latestnews_section = get_post_meta( $post->ID, '_jozoor_home_4_show_latestnews_section', true );
        $j_home_show_latestnews_section_instead_content = get_post_meta( $post->ID, '_jozoor_home_4_show_latestnews_section_instead_content', true );
     $j_home_show_clients_section = get_post_meta( $post->ID, '_jozoor_home_4_show_clients_section', true );
        $j_home_clients_section_instead_content = get_post_meta( $post->ID, '_jozoor_home_4_clients_section_instead_content', true );
       
     $j_news_column_section = 'sixteen columns full-width';
       
    $j_home_show_services_section = '';
    $j_home_services_section_instead_content = '';
       
   }
   else {
       
     $j_folder_template_part = '';
     
     // home 1 sections
     $j_home_show_services_section = get_post_meta( $post->ID, '_jozoor_home_1_show_services_section', true );
        $j_home_services_section_instead_content = get_post_meta( $post->ID, '_jozoor_home_1_services_section_instead_content', true );
     $j_home_show_recentwork_section = get_post_meta( $post->ID, '_jozoor_home_1_show_recentwork_section', true );
        $j_home_recentwork_section_instead_content = get_post_meta( $post->ID, '_jozoor_home_1_recentwork_section_instead_content', true );
     $j_home_show_latestnews_section = get_post_meta( $post->ID, '_jozoor_home_1_show_latestnews_section', true );
        $j_home_show_latestnews_section_instead_content = get_post_meta( $post->ID, '_jozoor_home_1_show_latestnews_section_instead_content', true );
     $j_home_show_clients_section = get_post_meta( $post->ID, '_jozoor_home_1_show_clients_section', true );
        $j_home_clients_section_instead_content = get_post_meta( $post->ID, '_jozoor_home_1_clients_section_instead_content', true );
       
    $j_news_column_section = 'sixteen columns full-width';
       
   }
   ?>
   

   <?php // services
   if($j_home_show_services_section == 'yes') {
   get_template_part( 'inc/home_sections/'.$j_folder_template_part.'content', 'services' );  
   } else {
       if(!empty($j_home_services_section_instead_content)) {
          echo '<div class="container clearfix">
               <div class="sixteen columns full-width">
               <div class="entry-content post-content">'. apply_filters('the_content', $j_home_services_section_instead_content ) .'</div></div></div>';
       }
   }
   ?>

   <?php // welcome box
   if($j_home_layout == 'home4') {
       
   if($j_home_show_welcome_section == 'yes') {
   
       echo '
       <div class="one-page home-bg welcome bottom-3">
        <div class="container clearfix">
         <div class="sixteen columns"><h1 class="welcome-text">'.$j_home_show_welcome_text.'</h1></div><!-- End welcome -->
       </div><!-- End Container -->
       </div><!-- End services -->
       ';
       
   } else {
       if(!empty($j_home_welcome_section_instead_content)) {
          echo '<div class="sixteen columns full-width"><div class="entry-content post-content">'. apply_filters('the_content', $j_home_welcome_section_instead_content ) .'</div></div>';
       }
   }
       
   }
   ?>

   <!-- Start main content -->
   <div class="container main-content clearfix">
       
   <?php // recent work
   if($j_home_show_recentwork_section == 'yes') {
   get_template_part( 'inc/home_sections/'.$j_folder_template_part.'content', 'works' ); 
   echo '<div class="sixteen columns"><hr class="line bottom-3"></div><!-- End line -->';
   } else {
       if(!empty($j_home_recentwork_section_instead_content)) {
          echo '<div class="sixteen columns full-width"><div class="entry-content post-content">'. apply_filters('the_content', $j_home_recentwork_section_instead_content ) .'</div></div>';
       }
   }
   ?>
       
   <?php // latest news
   if($j_home_show_latestnews_section == 'yes') {
   get_template_part( 'inc/home_sections/'.$j_folder_template_part.'content', 'blog' );  
   echo '<div class="sixteen columns"><hr class="line bottom-3"></div><!-- End line -->';
   } else {
       if(!empty($j_home_show_latestnews_section_instead_content)) {
          echo '<div class="'.$j_news_column_section.'"><div class="entry-content post-content">'. apply_filters('the_content', $j_home_show_latestnews_section_instead_content ) .'</div></div>';
       }
   }
   ?>
       
   <?php // testimonials
   if($j_home_layout == 'home2') {
       
   if($j_home_show_testimonials_section == 'yes') {
   get_template_part( 'inc/home_sections/'.$j_folder_template_part.'content', 'testimonials' );  
   } else {
       if(!empty($j_home_show_testimonials_section_instead_content)) {
          echo '<div class="one-third column"><div class="entry-content post-content">'. apply_filters('the_content', $j_home_show_testimonials_section_instead_content ) .'</div></div>';
       }
   }
       
   }
   ?>
       
   <?php // clients
   if($j_home_show_clients_section == 'yes') {
//   echo '<div class="sixteen columns"><hr class="line bottom-3"></div><!-- End line -->';
   get_template_part( 'inc/home_sections/'.$j_folder_template_part.'content', 'clients' );  
   } else {
       if(!empty($j_home_clients_section_instead_content)) {
          echo '<div class="sixteen columns full-width"><div class="entry-content post-content">'. apply_filters('the_content', $j_home_clients_section_instead_content ) .'</div></div>';
       }
   }
   ?>
       
       
<?php get_footer(); ?>