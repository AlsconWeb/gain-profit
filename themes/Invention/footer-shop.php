<?php include (get_template_directory() . '/inc/theme-options.php'); // get all theme options ?>

<?php if($jozoor_close_site == 1 && !current_user_can('administrator')) { ?>
    
  <!--[if lt IE 9]>
  <script src="<?php echo get_template_directory_uri(); ?>/js/html5.js"></script>
  <![endif]-->
  
  <!-- Start Style Switcher -->
  <?php include (get_template_directory() . '/inc/switcher.php'); ?>
  <div class="switcher"></div><!-- Div Contain Options -->
  <!-- End Style Switcher -->

  <?php include (get_template_directory() . '/inc/under-construction-js.php'); ?>
    
<?php } else { ?>

<?php 
global $post;

if ( is_404() || !have_posts() ) { 
    $j_using_page_builder = $j_page_custom_options = $j_page_hide_footer_widgets = '';
} else {
$j_using_page_builder = get_post_meta( wc_get_page_id('shop'), '_jozoor_using_page_builder', true );
// get page custom options metaboxs
$j_page_custom_options = get_post_meta( wc_get_page_id('shop'), '_jozoor_page_custom_options', true ); // check custom options
$j_page_hide_footer_widgets = get_post_meta( wc_get_page_id('shop'), '_jozoor_page_hide_footer_widgets', true ); // hide footer widgets
$j_page_show_down_footer_menu = get_post_meta( wc_get_page_id('shop'), '_jozoor_page_show_down_footer_menu', true ); // show down footer menu
$j_page_header_layout = get_post_meta( wc_get_page_id('shop'), '_jozoor_page_header_layout', true ); // header layout
$j_page_fixed_header = get_post_meta( wc_get_page_id('shop'), '_jozoor_page_fixed_header', true ); // fixed header
$j_page_show_search_icon = get_post_meta( wc_get_page_id('shop'), '_jozoor_page_show_search_icon', true ); // search icon
}

if($j_using_page_builder == 'on') { } else { 
?>
</div><!-- <<< End Container >>> -->
<?php } ?>
<?php if(is_404()) { ?>
</div><!-- End 404 -->
<?php } ?>
   
   <div class="clearfix"></div>

   <?php if($jozoor_add_custom_content_top_footer == 1) { ?>
    <div class="top-footer-custom-content post-content">
      <div class="container clearfix">
          <div class="sixteen columns"> <?php echo do_shortcode($jozoor_custom_content_top_footer); ?> </div>
      </div><!-- End Container -->
    </div>  
   <?php } ?>
   
   <footer>
     
    <?php if($j_page_custom_options == 'on' && $j_page_hide_footer_widgets == 'on') { } else { ?>
       
     <?php if($jozoor_show_footer_widgets == 1) { 
       
     if( is_active_sidebar('Footer1') || is_active_sidebar('Footer2') || is_active_sidebar('Footer3') || is_active_sidebar('Footer4') ) { 
         
     ?>
       
     <div class="footer-top">
      <div class="container clearfix">
         
      <?php 
      // Start [ 1/4 Column ] Layouts Footer Widgets
      if($jozoor_footer_layout == '1/4 Column') { ?>
      
      <div class="four columns">
      <?php if (function_exists('dynamic_sidebar') && dynamic_sidebar('Footer1')) { } else { } ?>
      </div>
      
      <div class="four columns">
      <?php if (function_exists('dynamic_sidebar') && dynamic_sidebar('Footer2')) { } else { } ?>
      </div>
      
      <div class="four columns">
      <?php if (function_exists('dynamic_sidebar') && dynamic_sidebar('Footer3')) { } else { } ?>
      </div>
      
      <div class="four columns">
      <?php if (function_exists('dynamic_sidebar') && dynamic_sidebar('Footer4')) { } else { } ?>
      </div>
      
      <?php } 
    
      // Start [ 1/3 Column ] Layouts Footer Widgets
      if($jozoor_footer_layout == '1/3 Column') { ?>
      
      <div class="one-third column">
      <?php if (function_exists('dynamic_sidebar') && dynamic_sidebar('Footer1')) { } else { } ?>
      </div>
      
      <div class="one-third column">
      <?php if (function_exists('dynamic_sidebar') && dynamic_sidebar('Footer2')) { } else { } ?>
      </div>
      
      <div class="one-third column">
      <?php if (function_exists('dynamic_sidebar') && dynamic_sidebar('Footer3')) { } else { } ?>
      </div>
      
      <?php } 
    
      // Start [ 1/2 Column ] Layouts Footer Widgets
      if($jozoor_footer_layout == '1/2 Column') { ?>
      
      <div class="eight columns">
      <?php if (function_exists('dynamic_sidebar') && dynamic_sidebar('Footer1')) { } else { } ?>
      </div>
      
      <div class="eight columns">
      <?php if (function_exists('dynamic_sidebar') && dynamic_sidebar('Footer2')) { } else { } ?>
      </div>
      
      <?php } 
    
      // Start [ 11/5 Column ] Layouts Footer Widgets
      if($jozoor_footer_layout == '11/5 Column') { ?>
      
      <div class="eleven columns">
      <?php if (function_exists('dynamic_sidebar') && dynamic_sidebar('Footer1')) { } else { } ?>
      </div>
      
      <div class="five columns">
      <?php if (function_exists('dynamic_sidebar') && dynamic_sidebar('Footer2')) { } else { } ?>
      </div>
      
      <?php } 
    
      // Start [ 12/4 Column ] Layouts Footer Widgets
      if($jozoor_footer_layout == '12/4 Column') { ?>
      
      <div class="twelve columns">
      <?php if (function_exists('dynamic_sidebar') && dynamic_sidebar('Footer1')) { } else { } ?>
      </div>
      
      <div class="four columns">
      <?php if (function_exists('dynamic_sidebar') && dynamic_sidebar('Footer2')) { } else { } ?>
      </div>
      
      <?php } 
    
      // Start [ 16 Column ] Layouts Footer Widgets
      if($jozoor_footer_layout == '16 Column') { ?>
      
      <div class="sixteen columns">
      <?php if (function_exists('dynamic_sidebar') && dynamic_sidebar('Footer1')) { } else { } ?>
      </div>
      
      <?php } ?>
      
        
      </div><!-- End container -->
     </div><!-- End footer-top -->
       
     <?php }
     } // end footer widgets 
       
     } // end check page hide footer widgets ?>
     
     <?php if($jozoor_show_footer_downbar == 1) { ?>   
     <div class="footer-down">
      <div class="container clearfix">
        
        <div class="eight columns left">
         <span class="copyright"><?php echo $jozoor_footer_text; ?></span>
        </div><!-- End copyright -->
        
        <div class="eight columns right">
           <?php if($j_page_custom_options == 'on' && $j_page_show_down_footer_menu == 'on') { 
       
           if ( has_nav_menu( 'down_footer' ) ) {
			  wp_nav_menu ( array( 'menu_id' => 'top-down-nav', 'menu_class' => 'top-down-menu', 'container'=> 'ul', 'theme_location' => 'down_footer', 'depth' => -1 ));
           }
       
           } else { ?>
           <?php if($jozoor_show_down_footer_menu == 1) { ?>
                
           <?php 
           if ( has_nav_menu( 'down_footer' ) ) {
			  wp_nav_menu ( array( 'menu_id' => 'top-down-nav', 'menu_class' => 'top-down-menu', 'container'=> 'ul', 'theme_location' => 'down_footer', 'depth' => -1 ));
           } 
           ?>
       
           <?php } else { ?> 
          <div class="social">
           <?php if ($jozoor_social_slider_footer != '') { // get all social networks 
           foreach ($jozoor_social_slider_footer as $slide) {  
           if(!empty($slide['name'])) {
           ?>
           <a href="<?php echo $slide['link']; ?>" target="_blank"><i class="social_icon-<?php echo $slide['name']; ?>"></i></a>
           <?php } 
           } } else { echo '<span class="none">'.__("you need to add social links from footer options!", "jozoorthemes").'</span>'; } ?>
          </div>
          <?php } ?>
          <?php } ?>
        </div><!-- End social icons -->
        
      </div><!-- End container -->
     </div><!-- End footer-down -->
     <?php } else { ?> 
       <div class="footer-down none"></div><!-- End footer-down -->
     <?php } ?>
     
   </footer><!-- <<< End Footer >>> -->

   <?php if($jozoor_add_custom_content_bottom_footer == 1) { ?>
    <div class="bottom-footer-custom-content post-content">
      <div class="container clearfix">
          <div class="sixteen columns"> <?php echo do_shortcode($jozoor_custom_content_bottom_footer); ?> </div>
      </div><!-- End Container -->
    </div>  
   <?php } ?>
  
  </div><!-- End wrap -->
  
  <!--[if lt IE 9]>
    <script src="<?php echo get_template_directory_uri(); ?>/js/html5.js"></script>
  <![endif]-->

  <!-- Start JavaScript from options -->
  <?php include (get_template_directory() . '/inc/custom-js.php'); ?>
  
  <!-- Start Style Switcher -->
  <?php include (get_template_directory() . '/inc/switcher.php'); ?>
  <div class="switcher"></div><!-- Div Contain Options -->
  <!-- End Style Switcher -->

  <?php echo $jozoor_custom_codes_footer; // Custom Codes OR Javascript Codes in Footer ?>

<?php } ?>

  <?php wp_footer(); ?>
    
</body>
</html>