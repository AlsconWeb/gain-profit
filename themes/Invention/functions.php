<?php

#=============================================================#
# After setup theme
#=============================================================#
add_action( 'after_setup_theme', 'jozoor_theme_setup' );

if ( !function_exists( 'jozoor_theme_setup' ) ) {
function jozoor_theme_setup() {

	// Add RSS links to <head> section
	add_theme_support( 'automatic-feed-links' );

	// Languages translatable for any language
    load_theme_textdomain( 'jozoorthemes', get_template_directory() . '/languages' );
    
	// Enable support for Post Formats
    add_theme_support( 'post-formats', array( 'aside', 'image', 'gallery', 'video', 'audio', 'link', 'quote', 'status' ) );
    
    // Enable support for Post Thumbnails
    add_theme_support( 'post-thumbnails' );
    add_image_size( 'latest-news-home', 420, 268, true ); 
    add_image_size( 'portfolio-thumb', 460, 368, true ); 
    add_image_size( 'gallery-post-thumb', 465, 332, true );
    add_image_size( 'related-posts-thumb', 260, 165, true );
    add_image_size( 'team-members-thumb', 420, 324, true );
    add_image_size( 'gallery-thumb', 460, 460, true ); 
    
    // Declare WooCommerce support in third party theme
    add_theme_support( 'woocommerce' );
    
}
}


#=============================================================#
# Jozoor Panel Options
#=============================================================#
require_once (get_template_directory() . '/admin/jozoor_panel/index.php');
	

#=============================================================#
# Clean up the <head>
#=============================================================#
function jozoor_removeHeadLinks() {
   remove_action('wp_head', 'rsd_link');
   remove_action('wp_head', 'wlwmanifest_link');
}
add_action('init', 'jozoor_removeHeadLinks');
remove_action('wp_head', 'wp_generator');


#=============================================================#
# Register/Enqueue JS
#=============================================================#
function jozoor_register_js() {
    
    if (!is_admin()) {
         
      global $smof_data;
        
      // Register 
      wp_register_script('jquery-easing', get_template_directory_uri() . '/js/jquery-easing.js', array('jquery'), '1.3', true); 
      wp_register_script('jquery-cookie', get_template_directory_uri() . '/js/jquery-cookie.js', array('jquery'), '1.3', true); 
      wp_register_script('ddsmoothmenu', get_template_directory_uri() . '/js/ddsmoothmenu.js', array('jquery'), '1.5.1', true);
      wp_register_script('flexslider', get_template_directory_uri() . '/js/jquery.flexslider.js', array('jquery'), '2.1', true);
      wp_register_script('colortip', get_template_directory_uri() . '/js/colortip.js', array('jquery'), '1.0', true);
      wp_register_script('tytabs', get_template_directory_uri() . '/js/tytabs.js', array('jquery'), '1.0', true);
      wp_register_script('carousel', get_template_directory_uri() . '/js/carousel.js', array('jquery'), '1.0', true);
      wp_register_script('isotope', get_template_directory_uri() . '/js/jquery.isotope.min.js', array('jquery'), '1.5.25', true);
      wp_register_script('jflickrfeed', get_template_directory_uri() . '/js/jflickrfeed.min.js', array('jquery'), '1.0', true);
      wp_register_script('doubletaptogo', get_template_directory_uri() . '/js/doubletaptogo.js', array('jquery'), '1.0', true);
      wp_register_script('fancybox', get_template_directory_uri() . '/js/fancybox/jquery.fancybox.js', array('jquery'), '3.0.0', true);
      wp_register_script('fancybox-media', get_template_directory_uri() . '/js/fancybox/jquery.fancybox-media.js', array('jquery'), '1.0.5', true);
      wp_register_script('sticky', get_template_directory_uri() . '/js/jquery.sticky.js', array('jquery'), '1.0', true);
      wp_register_script('fitvids', get_template_directory_uri() . '/js/fitvids.js', array('jquery'), '1.0.3', true);
      wp_register_script('parallax', get_template_directory_uri() . '/js/jquery.parallax.js', array('jquery'), '1.1.3', true);
      wp_register_script('nicescroll', get_template_directory_uri() . '/js/jquery.nicescroll.js', array('jquery'), '3.5.4', true);
      wp_register_script('scrollTo', get_template_directory_uri() . '/js/jquery.scrollTo.js', array('jquery'), '1.4.12', true);
      wp_register_script('localscroll', get_template_directory_uri() . '/js/jquery.localscroll.js', array('jquery'), '1.2.7', true);
      wp_register_script('wow-animation', get_template_directory_uri() . '/js/wow.min.js', array('jquery'), '0.1.6', true);
      wp_register_script('jozoor-custom-js', get_template_directory_uri() . '/js/custom.js', array('jquery'), '1.1', true);
        
      
      // Enqueue
      wp_enqueue_script('jquery');
      
      if($smof_data['close_site'] == 1 && !current_user_can('administrator')) {
      wp_enqueue_script('jquery-easing');  
      wp_enqueue_script('jquery-cookie');
      wp_enqueue_script('colortip');
      } else {
      wp_enqueue_script('jquery-ui-core');
      wp_enqueue_script('jquery-ui-widget');
      wp_enqueue_script('jquery-ui-accordion');
      wp_enqueue_script('wp-mediaelement');
      wp_enqueue_script('jquery-easing');  
      wp_enqueue_script('jquery-cookie');
      wp_enqueue_script('ddsmoothmenu');
      wp_enqueue_script('flexslider');
      wp_enqueue_script('colortip');        
      wp_enqueue_script('tytabs');
      wp_enqueue_script('carousel');
      wp_enqueue_script('isotope');
      wp_enqueue_script('jflickrfeed');
      wp_enqueue_script('doubletaptogo');
      wp_enqueue_script('fancybox');
      wp_enqueue_script('fancybox-media');
      wp_enqueue_script('sticky');
      wp_enqueue_script('fitvids');
      wp_enqueue_script('parallax');
      wp_enqueue_script('nicescroll');
      wp_enqueue_script('scrollTo');
      wp_enqueue_script('localscroll');
      wp_enqueue_script('wow-animation');
      wp_enqueue_script('jozoor-custom-js');
      }
      
        
    }
}

add_action('wp_enqueue_scripts', 'jozoor_register_js');


// Enqueue custom options & page specific js  
function jozoor_register_custom_options_js() {
    
      if (!is_admin()) {
          
      // totop icon option 
      global $smof_data;
      if($smof_data['close_site'] == 1 && !current_user_can('administrator')) { } else {
       if($smof_data['totop_icon'] == 1) {
         wp_register_script('totop', get_template_directory_uri() . '/js/jquery.ui.totop.js', array('jquery'), '1.1', true);  
         wp_enqueue_script('totop');    
       }
      }
          
      } // end admin check
	
	  // comments
	  if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
         wp_enqueue_script( 'comment-reply' );
      }

}

add_action('wp_enqueue_scripts', 'jozoor_register_custom_options_js');


#=============================================================#
# Register/Enqueue CSS
#=============================================================#
// Theme Styles
function jozoor_register_css() {	
    
      if (!is_admin()) {
      
      global $smof_data, $post;
          
      // get page custom options metaboxs
      if ( is_404() || !have_posts() ) { $j_page_custom_options = $j_page_layout_style = ''; } else {
          
      if ( function_exists('is_woocommerce') && is_shop() ) {
      $j_page_custom_options = get_post_meta( wc_get_page_id('shop'), '_jozoor_page_custom_options', true ); // check custom options
      $j_page_layout_style = get_post_meta( wc_get_page_id('shop'), '_jozoor_page_layout_style', true ); // page layout    
      } else {
      $j_page_custom_options = get_post_meta( $post->ID, '_jozoor_page_custom_options', true ); // check custom options
      $j_page_layout_style = get_post_meta( $post->ID, '_jozoor_page_layout_style', true ); // page layout
      }
          
      }
    
      // load google fonts
      if(isset($smof_data['using_google_fonts']) && $smof_data['using_google_fonts'] == 1) {
          
        if($smof_data['body_font_type'] != 'none' && $smof_data['body_font_type'] != 'ar' && $smof_data['body_font_type'] != 'JF-Flat' && $smof_data['body_font_type'] != 'AraJozoor' && $smof_data['body_font_type'] != 'DroidKufi' && $smof_data['body_font_type'] != 'DroidNaskh' && $smof_data['body_font_type'] != 'amiri' && $smof_data['body_font_type'] != 'lateef' && $smof_data['body_font_type'] != 'scheherazade' && $smof_data['body_font_type'] != 'thabit') {
            
        wp_enqueue_style('body-gf', 'http://fonts.googleapis.com/css?family='.$smof_data['body_font_type'].'&subset=latin,cyrillic-ext,greek-ext,greek,vietnamese,latin-ext,cyrillic'); 
            
        } else {
            if( $smof_data['body_font_type'] != 'none' && $smof_data['body_font_type'] != 'ar' ) {
               wp_enqueue_style('body-gf', get_stylesheet_directory_uri() . '/css/font-ar/'.$smof_data['body_font_type'].'.css');
           }  
        }
          
          
        if($smof_data['menu_font_type'] != 'none' && $smof_data['menu_font_type'] != 'ar' && $smof_data['menu_font_type'] != 'JF-Flat' && $smof_data['menu_font_type'] != 'AraJozoor' && $smof_data['menu_font_type'] != 'DroidKufi' && $smof_data['menu_font_type'] != 'DroidNaskh' && $smof_data['menu_font_type'] != 'amiri' && $smof_data['menu_font_type'] != 'lateef' && $smof_data['menu_font_type'] != 'scheherazade' && $smof_data['menu_font_type'] != 'thabit') {
            
        wp_enqueue_style('menu-gf', 'http://fonts.googleapis.com/css?family='.$smof_data['menu_font_type'].'&subset=latin,cyrillic-ext,greek-ext,greek,vietnamese,latin-ext,cyrillic'); 
            
        } else {
            if( $smof_data['menu_font_type'] != 'none' && $smof_data['menu_font_type'] != 'ar' ) {
               wp_enqueue_style('menu-gf', get_stylesheet_directory_uri() . '/css/font-ar/'.$smof_data['menu_font_type'].'.css');
           }  
        }
          
          
        if($smof_data['topbar_font_type'] != 'none' && $smof_data['topbar_font_type'] != 'ar' && $smof_data['topbar_font_type'] != 'JF-Flat' && $smof_data['topbar_font_type'] != 'AraJozoor' && $smof_data['topbar_font_type'] != 'DroidKufi' && $smof_data['topbar_font_type'] != 'DroidNaskh' && $smof_data['topbar_font_type'] != 'amiri' && $smof_data['topbar_font_type'] != 'lateef' && $smof_data['topbar_font_type'] != 'scheherazade' && $smof_data['topbar_font_type'] != 'thabit') {
            
        wp_enqueue_style('topbar-gf', 'http://fonts.googleapis.com/css?family='.$smof_data['topbar_font_type'].'&subset=latin,cyrillic-ext,greek-ext,greek,vietnamese,latin-ext,cyrillic');
            
        } else {
            if( $smof_data['topbar_font_type'] != 'none' && $smof_data['topbar_font_type'] != 'ar' ) {
               wp_enqueue_style('topbar-gf', get_stylesheet_directory_uri() . '/css/font-ar/'.$smof_data['topbar_font_type'].'.css');
           }  
        }
          
          
        if($smof_data['heading_font_type'] != 'none' && $smof_data['heading_font_type'] != 'ar' && $smof_data['heading_font_type'] != 'JF-Flat' && $smof_data['heading_font_type'] != 'AraJozoor' && $smof_data['heading_font_type'] != 'DroidKufi' && $smof_data['heading_font_type'] != 'DroidNaskh' && $smof_data['heading_font_type'] != 'amiri' && $smof_data['heading_font_type'] != 'lateef' && $smof_data['heading_font_type'] != 'scheherazade' && $smof_data['heading_font_type'] != 'thabit') {
            
        wp_enqueue_style('heading-gf', 'http://fonts.googleapis.com/css?family='.$smof_data['heading_font_type'].'&subset=latin,cyrillic-ext,greek-ext,greek,vietnamese,latin-ext,cyrillic'); 
            
        } else {
            if( $smof_data['heading_font_type'] != 'none' && $smof_data['heading_font_type'] != 'ar' ) {
               wp_enqueue_style('heading-gf', get_stylesheet_directory_uri() . '/css/font-ar/'.$smof_data['heading_font_type'].'.css');
           }  
        }
          
          
        if($smof_data['heading_footer_font_type'] != 'none' && $smof_data['heading_footer_font_type'] != 'ar' && $smof_data['heading_footer_font_type'] != 'JF-Flat' && $smof_data['heading_footer_font_type'] != 'AraJozoor' && $smof_data['heading_footer_font_type'] != 'DroidKufi' && $smof_data['heading_footer_font_type'] != 'DroidNaskh' && $smof_data['heading_footer_font_type'] != 'amiri' && $smof_data['heading_footer_font_type'] != 'lateef' && $smof_data['heading_footer_font_type'] != 'scheherazade' && $smof_data['heading_footer_font_type'] != 'thabit') {
            
        wp_enqueue_style('heading-foot-gf', 'http://fonts.googleapis.com/css?family='.$smof_data['heading_footer_font_type'].'&subset=latin,cyrillic-ext,greek-ext,greek,vietnamese,latin-ext,cyrillic'); 
        
        } else {
            if( $smof_data['heading_footer_font_type'] != 'none' && $smof_data['heading_footer_font_type'] != 'ar' ) {
               wp_enqueue_style('heading-foot-gf', get_stylesheet_directory_uri() . '/css/font-ar/'.$smof_data['heading_footer_font_type'].'.css');
           }  
        }
          
          
        if($smof_data['downbar_font_type'] != 'none' && $smof_data['downbar_font_type'] != 'ar' && $smof_data['downbar_font_type'] != 'JF-Flat' && $smof_data['downbar_font_type'] != 'AraJozoor' && $smof_data['downbar_font_type'] != 'DroidKufi' && $smof_data['downbar_font_type'] != 'DroidNaskh' && $smof_data['downbar_font_type'] != 'amiri' && $smof_data['downbar_font_type'] != 'lateef' && $smof_data['downbar_font_type'] != 'scheherazade' && $smof_data['downbar_font_type'] != 'thabit') {
            
        wp_enqueue_style('downbar-gf', 'http://fonts.googleapis.com/css?family='.$smof_data['downbar_font_type'].'&subset=latin,cyrillic-ext,greek-ext,greek,vietnamese,latin-ext,cyrillic'); 
            
        } else {
            if( $smof_data['downbar_font_type'] != 'none' && $smof_data['downbar_font_type'] != 'ar' ) {
               wp_enqueue_style('downbar-gf', get_stylesheet_directory_uri() . '/css/font-ar/'.$smof_data['downbar_font_type'].'.css');
           }  
        }
          
          
        if($smof_data['title_sliders_font_type'] != 'none' && $smof_data['title_sliders_font_type'] != 'ar' && $smof_data['title_sliders_font_type'] != 'JF-Flat' && $smof_data['title_sliders_font_type'] != 'AraJozoor' && $smof_data['title_sliders_font_type'] != 'DroidKufi' && $smof_data['title_sliders_font_type'] != 'DroidNaskh' && $smof_data['title_sliders_font_type'] != 'amiri' && $smof_data['title_sliders_font_type'] != 'lateef' && $smof_data['title_sliders_font_type'] != 'scheherazade' && $smof_data['title_sliders_font_type'] != 'thabit') {
            
        wp_enqueue_style('title-sliders-gf', 'http://fonts.googleapis.com/css?family='.$smof_data['title_sliders_font_type'].'&subset=latin,cyrillic-ext,greek-ext,greek,vietnamese,latin-ext,cyrillic'); 
            
        } else {
            if( $smof_data['title_sliders_font_type'] != 'none' && $smof_data['title_sliders_font_type'] != 'ar' ) {
               wp_enqueue_style('title-sliders-gf', get_stylesheet_directory_uri() . '/css/font-ar/'.$smof_data['title_sliders_font_type'].'.css');
           }  
        }
          
          
        if($smof_data['desc_sliders_font_type'] != 'none' && $smof_data['desc_sliders_font_type'] != 'ar' && $smof_data['desc_sliders_font_type'] != 'JF-Flat' && $smof_data['desc_sliders_font_type'] != 'AraJozoor' && $smof_data['desc_sliders_font_type'] != 'DroidKufi' && $smof_data['desc_sliders_font_type'] != 'DroidNaskh' && $smof_data['desc_sliders_font_type'] != 'amiri' && $smof_data['desc_sliders_font_type'] != 'lateef' && $smof_data['desc_sliders_font_type'] != 'scheherazade' && $smof_data['desc_sliders_font_type'] != 'thabit') {
            
        wp_enqueue_style('desc-sliders-gf', 'http://fonts.googleapis.com/css?family='.$smof_data['desc_sliders_font_type'].'&subset=latin,cyrillic-ext,greek-ext,greek,vietnamese,latin-ext,cyrillic'); 
            
        } else {
            if( $smof_data['desc_sliders_font_type'] != 'none' && $smof_data['desc_sliders_font_type'] != 'ar' ) {
               wp_enqueue_style('desc-sliders-gf', get_stylesheet_directory_uri() . '/css/font-ar/'.$smof_data['desc_sliders_font_type'].'.css');
           }  
        }
          
           
      }
    
      // google font for logo text
      
      if($smof_data['logo_text_instead_image'] == 1) {
          
      if( $smof_data['logo_name_font_type'] != 'none' && $smof_data['logo_name_font_type'] != 'ar' && $smof_data['logo_name_font_type'] != 'JF-Flat' && $smof_data['logo_name_font_type'] != 'AraJozoor' && $smof_data['logo_name_font_type'] != 'DroidKufi' && $smof_data['logo_name_font_type'] != 'DroidNaskh' && $smof_data['logo_name_font_type'] != 'amiri' && $smof_data['logo_name_font_type'] != 'lateef' && $smof_data['logo_name_font_type'] != 'scheherazade' && $smof_data['logo_name_font_type'] != 'thabit') {
          
        wp_enqueue_style('logo-name-gf', 'http://fonts.googleapis.com/css?family='.$smof_data['logo_name_font_type'].'&subset=latin,cyrillic-ext,greek-ext,greek,vietnamese,latin-ext,cyrillic'); 
          
      } else {
        if( $smof_data['logo_name_font_type'] != 'none' && $smof_data['logo_name_font_type'] != 'ar' ) {
               wp_enqueue_style('logo-name-gf', get_stylesheet_directory_uri() . '/css/font-ar/'.$smof_data['logo_name_font_type'].'.css');
        }  
      }
          
      }
         
      // main styles 
      wp_enqueue_style('main-style', get_stylesheet_uri());
      
      // mediaelement player
      wp_enqueue_style('wp-mediaelement');
    
      // theme skins
      if($smof_data['theme_skin'] == 'dark') {
        wp_enqueue_style('dark-skin', get_stylesheet_directory_uri() . '/css/skins/dark.css');
      } else {
        wp_enqueue_style('white-skin', get_stylesheet_directory_uri() . '/css/skins/white.css');
      }
      
      // color style
      wp_enqueue_style('color-style', get_stylesheet_directory_uri() . '/css/skins/colors/'.$smof_data['default_color_scheme'].'.css');
    
      // layout style
      if($j_page_custom_options == 'on' && $j_page_layout_style != 'none') {
      wp_enqueue_style('layout-style', get_stylesheet_directory_uri() . '/css/layout/'.$j_page_layout_style.'.css');    
      } else {
      wp_enqueue_style('layout-style', get_stylesheet_directory_uri() . '/css/layout/'.$smof_data['layout_style'].'.css');
      }
    
      // responsive layouts
      if($smof_data['responsive_design'] == 1) {
          
        wp_enqueue_style('responsive-skeleton', get_stylesheet_directory_uri() . '/css/responsive/skeleton.css');  
        wp_enqueue_style('responsive-style', get_stylesheet_directory_uri() . '/css/responsive/style.css');  
        wp_enqueue_style('responsive-boxed', get_stylesheet_directory_uri() . '/css/responsive/boxed.css'); 
        if($smof_data['theme_skin'] == 'dark') {
          wp_enqueue_style('responsive-dark', get_stylesheet_directory_uri() . '/css/responsive/dark.css');  
        } else {
          wp_enqueue_style('responsive-white', get_stylesheet_directory_uri() . '/css/responsive/white.css');     
        }  
          
      }
          
      // animate.css
      wp_enqueue_style('animate-css', get_stylesheet_directory_uri() . '/css/animate.min.css');
    
      // custom styles
      wp_enqueue_style('custom-styles', get_stylesheet_directory_uri() . '/css/custom.css.php');
   
   }
    
}

add_action('wp_enqueue_scripts', 'jozoor_register_css');


#=============================================================#
# New Widget areas
#=============================================================#
include_once (get_template_directory() . '/inc/widgets.php'); 


#=============================================================#
# Custom Widgets
#=============================================================#
include_once (get_template_directory() . '/inc/custom_widgets/latest-tweets.php'); // Latest Tweets
include_once (get_template_directory() . '/inc/custom_widgets/flickr-stream.php'); // Flickr Stream
include_once (get_template_directory() . '/inc/custom_widgets/tabs.php'); // Blog Tabs
include_once (get_template_directory() . '/inc/custom_widgets/subscribe-form.php'); // Subscribe Form
include_once (get_template_directory() . '/inc/custom_widgets/custom-content.php'); // Custom Content
include_once (get_template_directory() . '/inc/custom_widgets/recent-projects.php'); // Recent Projects
include_once (get_template_directory() . '/inc/custom_widgets/latest-galleries.php'); // Latest Galleries


#=============================================================#
# Shortcodes
#=============================================================#
require_once (get_template_directory() . '/inc/shortcodes/tinymce-plugin.php'); // TinyMCE plugin
require_once (get_template_directory() . '/inc/shortcodes/shortcodes-list.php'); // Shortcodes list



#=============================================================#
# Register Custom Menu Header
#=============================================================#
if ( function_exists( 'register_nav_menu' ) ) {
  register_nav_menus(
    array( 'headermenu'   =>  'Main Header Menu', 'top_header'   =>  'Top Header Menu', 'down_footer'   =>  'Down Footer Menu' )
  );
 }


#=============================================================#
# Pagination Function 
#=============================================================#
if ( !function_exists( 'jozoor_pagination' ) ) {
function jozoor_pagination() {    
     global $smof_data; // get options
     
     if($smof_data['default_wp_pagination'] == 1) {
         
      if( get_next_posts_link() || get_previous_posts_link() ) { 
       echo '<div class="pagination default">'.get_previous_posts_link('<i class="icon-angle-left"></i>').''.get_next_posts_link('<i class="icon-angle-right"></i>').'</div>';
			
      }   
         
     } else {
         
     global $wp_query;  
      
	 $total_pages = $wp_query->max_num_pages;  
			      
	 if ($total_pages > 1){  
			      
		$current_page = max(1, get_query_var('paged'));  
		echo '<div class="pagination">';
		echo paginate_links(array(  
			 'base' => str_replace( $total_pages, '%#%', esc_url( get_pagenum_link( $total_pages ) ) ),
             'format' => '/page/%#%', 
			 'current' => $current_page,  
			 'total' => $total_pages,  
             'prev_text' => '<i class="icon-angle-left"></i>',
	         'next_text' => '<i class="icon-angle-right"></i>',
         )); 
		 echo  '</div>'; 
       }  
         
    }
    
} 
}

// Default page links
wp_link_pages();


#=============================================================#
# Comments List Function callback 
#=============================================================#
include_once (get_template_directory() . '/inc/comments-list.php'); 


#=============================================================#
# Posts Views get & set
#=============================================================#
// Get Views
function jozoor_getPostViews($postID){
    $count_key = 'post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '1');
        return "1";
    }
    return $count.'';
}

// Set Views
function jozoor_setPostViews($postID) {
    $count_key = 'post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        $count = 0;
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '1');
    }else{
        $count++;
        update_post_meta($postID, $count_key, $count);
    }
}


#=============================================================#
# Get & Show Featured Image in posts columns dashboard
#=============================================================#
// get featured image
function jozoor_get_featured_image_dash($post_ID) {
   $post_thumbnail_id = get_post_thumbnail_id($post_ID);
   if ($post_thumbnail_id) {
	  $post_thumbnail_img = wp_get_attachment_image_src($post_thumbnail_id, 'featured_preview');
	  return $post_thumbnail_img[0];
   }
}

// add new column
function jozoor_columns_head_dash($defaults) {
  $defaults['featured_image'] = 'Featured Image';
  return $defaults;
}

// show featured image
function jozoor_columns_content_dash($column_name, $post_ID) {
if ($column_name == 'featured_image') {
  $post_featured_image = jozoor_get_featured_image_dash($post_ID);
  if ($post_featured_image) {
	  // has featured image
	  echo '<img src="' . $post_featured_image . '" width="110" />';
  }

}
}

// only default posts
add_filter('manage_post_posts_columns', 'jozoor_columns_head_dash', 10);
add_action('manage_post_posts_custom_column', 'jozoor_columns_content_dash', 10, 2); 


#=============================================================#
# Custom Post Types & MetaBoxs 
#=============================================================#
include_once (get_template_directory() . '/inc/custom_types_metaboxs/post.php');          // Posts
include_once (get_template_directory() . '/inc/custom_types_metaboxs/user.php');          // Users
include_once (get_template_directory() . '/inc/custom_types_metaboxs/page.php');          // Pages
include_once (get_template_directory() . '/inc/custom_types_metaboxs/sliders.php');       // sliders
include_once (get_template_directory() . '/inc/custom_types_metaboxs/portfolio.php');     // portfolio
include_once (get_template_directory() . '/inc/custom_types_metaboxs/services.php');      // services
include_once (get_template_directory() . '/inc/custom_types_metaboxs/clients.php');       // clients
include_once (get_template_directory() . '/inc/custom_types_metaboxs/testimonials.php');  // testimonials
include_once (get_template_directory() . '/inc/custom_types_metaboxs/team.php');          // team
include_once (get_template_directory() . '/inc/custom_types_metaboxs/gallery.php');       // gallery

// Initialize the metabox class
add_action( 'init', 'jozoor_initialize_cmb_meta_boxes', 9999 );
function jozoor_initialize_cmb_meta_boxes() {
    if ( !class_exists( 'cmb_Meta_Box' ) ) {
        require_once( 'inc/cmb_metaboxs/init.php' );
    }
}

/**
 * Gets a number of posts and displays them as options
 * @param  array $query_args Optional. Overrides defaults.
 * @return array An array of options that matches the CMB options array
 */
function jozoor_cmb_get_post_options( $query_args ) {

    $args = wp_parse_args( $query_args, array(
        'post_type' => 'post',
        'numberposts' => 10,
    ) );

    $posts = get_posts( $args );

    $post_options = array();
    if ( $posts ) {
        foreach ( $posts as $post ) {
                   $post_options[] = array(
                       'name' => $post->post_title,
                       'value' => $post->ID
                   );
        }
    } else {
        $post_options[] = array(
            'name' => __( 'You don\'t create sliders yet' ),
            'value' => 'none'
        );
    }

    return $post_options;
}


#=============================================================#
# Modify More Link [ add new class + disable jump + add p tag ]
#=============================================================#
// add new class
function jozoor_add_morelink_class( $link, $text )
{
    return str_replace(
         'more-link'
        ,'more-link button small color'
        ,$link
    );
}
add_action( 'the_content_more_link', 'jozoor_add_morelink_class', 10, 2 );

// disable jump
function jozoor_remove_more_jump_link($link) { 
	$offset = strpos($link, '#more-');
	if ($offset) {
		$end = strpos($link, '"',$offset);
	}
	if ($end) {
		$link = substr_replace($link, '', $offset, $end-$offset);
	}
	return $link;
}
add_filter('the_content_more_link', 'jozoor_remove_more_jump_link');

// add p tag
function jozoor_add_p_tag_more($link){
    return "<p class='last-tag'>$link</p>";
}
add_filter('the_content_more_link', 'jozoor_add_p_tag_more');


#=============================================================#
# Customize the Protected post form content
#=============================================================#
function jozoor_my_password_form() {
    global $post;
    $label = 'pwbox-'.( empty( $post->ID ) ? rand() : $post->ID );
    $o = '<form action="' . esc_url( site_url( 'wp-login.php?action=postpass', 'login_post' ) ) . '" method="post" class="form-elements post-protected">
    <p>' . __( 'This post is password protected. To view it please enter your password below :', 'jozoorthemes' ) . '</p>
    <fieldset>
    <span for="' . $label . '">' . __( 'Password :', 'jozoorthemes' ) . ' </span><input name="post_password" id="' . $label . '" type="password" size="20" maxlength="20" /><input type="submit" name="Submit" value="' . esc_attr__( "Submit" ) . '" class="button small color" />
    </fieldset>
    </form>
    ';
    return $o;
}
add_filter( 'the_password_form', 'jozoor_my_password_form' );


#=============================================================#
# Adding the infinity symbol fo aside post
#=============================================================#
add_filter( 'the_content', 'jozoor_aside_to_infinity_and_beyond', 9 ); // run before wpautop

function jozoor_aside_to_infinity_and_beyond( $content ) {

	if ( has_post_format( 'aside' ) && !is_singular() && !post_password_required() )
		$content .= ' <a href="' . get_permalink() . '" class="infinity-symbol"> &#8734;</a>';

	return $content;
}


#=============================================================#
# Set up the content width value
#=============================================================#
if ( ! isset( $content_width ) ) {
	$content_width = 940;
}

#=============================================================#
# Enqueue Switch Post Formats Script & font icons files
#=============================================================#
function jozoor_metabox_scripts() {
	wp_register_script('switch-formats', get_template_directory_uri() .'/inc/cmb_metaboxs/js/switch-formats.js', array('jquery'));
	wp_enqueue_script('switch-formats');
    
    wp_enqueue_style('icons-font-awesome', get_template_directory_uri() . '/css/font-awesome/css/font-awesome.min.css');
    wp_enqueue_style('icons-fontello', get_template_directory_uri() . '/css/fontello/css/fontello.css');
}

add_action('admin_enqueue_scripts', 'jozoor_metabox_scripts');


#=============================================================#
# Page & single title
#=============================================================#
if ( !function_exists( 'jozoor_page_title' ) ) {
function jozoor_page_title ($postid) {
 
   if( is_single() || is_category() || is_tag() || is_day() || is_month() || is_year() || is_author() || is_search() ) { ?>
   <div class="page-title">
     <div class="container clearfix">
       
       <div class="sixteen columns"> 
         <?php if(is_search()) { ?>
          <h1><?php echo __("Results for", "jozoorthemes"); ?> : <?php the_search_query(); ?></h1>
         <?php } else { ?>
          <h1><?= get_the_title() ?></h1>
         <?php } ?>
         <?php 
         global $post;
         global $smof_data;
         if ($post->post_type == 'portfolio') { 
          
         if( $smof_data['single_prev_next_links_project'] == 1) {
             
         $jozoor_prev_post = get_previous_post();
         $jozoor_next_post = get_next_post();
             
         if( !empty($jozoor_prev_post) || !empty($jozoor_next_post) ) { echo '<div class="portfolio-control">'; }
         
         if( !empty($jozoor_next_post) ) { ?>
         <a href="<?php echo get_permalink( $jozoor_next_post->ID ); ?>" data="<?php echo $jozoor_next_post->post_title; ?>" class="black"><i class="icon-angle-right"></i></a>
         <?php } 
         if( !empty($jozoor_prev_post) ) { ?>
         <a href="<?php echo get_permalink( $jozoor_prev_post->ID ); ?>" data="<?php echo $jozoor_prev_post->post_title; ?>" class="black"><i class="icon-angle-left"></i></a>
         <?php }
         
         if( !empty($jozoor_prev_post) || !empty($jozoor_next_post) ) { echo '</div><!-- End control -->'; }
             
         } 
           
         } 
         
         if ($post->post_type == 'gallery') { 
          
         if( $smof_data['gallery_page_prev_next_links'] == 1) {
             
         $jozoor_prev_post = get_previous_post();
         $jozoor_next_post = get_next_post();
             
         if( !empty($jozoor_prev_post) || !empty($jozoor_next_post) ) { echo '<div class="portfolio-control">'; }
         
         if( !empty($jozoor_next_post) ) { ?>
         <a href="<?php echo get_permalink( $jozoor_next_post->ID ); ?>" data="<?php echo $jozoor_next_post->post_title; ?>" class="black"><i class="icon-angle-right"></i></a>
         <?php } 
         if( !empty($jozoor_prev_post) ) { ?>
         <a href="<?php echo get_permalink( $jozoor_prev_post->ID ); ?>" data="<?php echo $jozoor_prev_post->post_title; ?>" class="black"><i class="icon-angle-left"></i></a>
         <?php }
         
         if( !empty($jozoor_prev_post) || !empty($jozoor_next_post) ) { echo '</div><!-- End control -->'; }
             
         }
             
         // get number of images
         $j_gallery_images = get_post_meta( $post->ID, '_jozoor_gallery_images', true );
         if(!empty($j_gallery_images)) {
          $j_gallery_images_num = ' &nbsp;&nbsp; <i class="icon-picture"></i> ' . count($j_gallery_images) . ' '.__("Photos", "jozoorthemes");
         } else {
          $j_gallery_images_num = '';
         }
             
         echo '<span class="single-gallery-meta"> <i class="icon-time"></i> '.get_the_time('d M, Y').$j_gallery_images_num.'</span>';
           
         }
                                                                                                                        
         ?>
       </div>
       
     </div><!-- End Container -->
   </div><!-- End Page title -->
   <?php }
    
   if( is_taxonomy_hierarchical('portfolio-category') ) { 
   $taxonomy = 'portfolio-category';
   $queried_term = get_query_var($taxonomy);
   $term = get_term_by( 'slug', $queried_term, $taxonomy );
   if( $term ) {
   echo '<div class="page-title">
   <div class="container clearfix">
   <div class="sixteen columns"><h1>';
   echo $term->name; 
   echo '</h1></div>
   </div><!-- End Container -->
   </div><!-- End Page title -->'; 
   }
   }
    
   if( is_page() && !is_front_page() ) {
       
   $j_show_page_title = get_post_meta( $postid, '_jozoor_show_page_title', true );
   $j_page_title_bg = get_post_meta( $postid, '_jozoor_page_title_bg', true );
   $j_page_title_bg_height = get_post_meta( $postid, '_jozoor_page_title_bg_height', true );
   $j_page_title_margin_top = get_post_meta( $postid, '_jozoor_page_title_margin_top', true );
       
   if( $j_show_page_title == 'yes' || $j_show_page_title !='no' ) {  ?>
   <div class="page-title"<?php if( !empty($j_page_title_bg) ) { ?> style="background:url('<?php echo $j_page_title_bg; ?>') no-repeat; height:<?php echo $j_page_title_bg_height; ?>px;background-position: center; background-size: cover;"<?php } ?>>
     <div class="container clearfix">
       
       <div class="sixteen columns"> 
         <h1<?php if( !empty($j_page_title_margin_top) && !empty($j_page_title_bg) ) { ?> style="margin-top:<?php echo $j_page_title_margin_top; ?>px;"<?php } ?>><?php wp_title('', true, 'right'); ?></h1>
       </div>
       
     </div><!-- End Container -->
   </div><!-- End Page title -->   

   <?php } 
   
   }
    
   if (function_exists('is_woocommerce')) {
       
   if( is_shop() || is_product_category() || is_product_tag() && !is_front_page() ) {
       
   if(is_search()) { } else {
       
   $j_show_page_title = get_post_meta( $postid, '_jozoor_show_page_title', true );
   $j_page_title_bg = get_post_meta( $postid, '_jozoor_page_title_bg', true );
   $j_page_title_bg_height = get_post_meta( $postid, '_jozoor_page_title_bg_height', true );
   $j_page_title_margin_top = get_post_meta( $postid, '_jozoor_page_title_margin_top', true );
       
   if( $j_show_page_title == 'yes' || $j_show_page_title !='no' ) {  ?>
   <div class="page-title"<?php if( !empty($j_page_title_bg) ) { ?> style="background:url('<?php echo $j_page_title_bg; ?>') no-repeat; height:<?php echo $j_page_title_bg_height; ?>px;background-position: center; background-size: cover;"<?php } ?>>
     <div class="container clearfix">
       
       <div class="sixteen columns"> 
         <h1<?php if( !empty($j_page_title_margin_top) && !empty($j_page_title_bg) ) { ?> style="margin-top:<?php echo $j_page_title_margin_top; ?>px;"<?php } ?>><?php woocommerce_page_title(); ?></h1>
       </div>
       
     </div><!-- End Container -->
   </div><!-- End Page title -->   

   <?php } 
       
   }
   
   }
       
  }
    
    
}
}


#=============================================================#
# Remove the dots [...] at the end of Excerpts
#=============================================================#
function jozoor_new_excerpt_more( $more ) {
    global $post;
    if ($post->post_type == 'services') {
	return '...';
    } else {
    return '... <a class="read-more" href="'. get_permalink( get_the_ID() ) . '">'.__("more", "jozoorthemes").'</a>';
    }
}
add_filter('excerpt_more', 'jozoor_new_excerpt_more');


#=============================================================#
# Define Excerpt Length for custom post types
#=============================================================#
function jozoor_custom_excerpt_length( $length ) {
    global $post;
    if ($post->post_type == 'services') {
	return 19;
    } elseif (is_page_template('template-home.php') && $post->post_type == 'post') {
    return 20;
    } elseif (is_page() && $post->post_type == 'post') {
    return 20;
    } else {
    return 57;
    }
}
add_filter( 'excerpt_length', 'jozoor_custom_excerpt_length', 999 );


#=============================================================#
# Using Custom Single Template for custom types
#=============================================================#
function jozoor_custom_single_templates($single_template) {
    
   global $post;
    
   if ($post->post_type == 'portfolio') {
   $single_template = get_template_directory() . '/single-portfolio.php';
   }
   if ($post->post_type == 'services') {
   $single_template = get_template_directory() . '/single-services.php';
   }
   if ($post->post_type == 'gallery') {
   $single_template = get_template_directory() . '/single-gallery.php';
   }
   if ($post->post_type == 'sliders') {
   $single_template = get_template_directory() . '/single-slider.php';
   }
   return $single_template;
    
}
add_filter( 'single_template', 'jozoor_custom_single_templates' );


#=============================================================#
# Get Sliders [ slider 1 & 2 ]
#=============================================================#
if ( !function_exists( 'jozoor_sliders_show' ) ) {
function jozoor_sliders_show ($postid) {
    
    if ( is_page() || function_exists('is_woocommerce') ) {
    
    if( is_page() || is_shop() ) {
        
    // page slider option
    $j_show_page_slider = get_post_meta( $postid, '_jozoor_show_page_slider', true );
    $j_page_slider_name = get_post_meta( $postid, '_jozoor_page_slider_name', true );
        
    if( $j_show_page_slider == 'yes') { 
        
    global $post;

    // get slider
    $sliders_args = array( 'post_type' => 'sliders', 'p' => $j_page_slider_name );
    $sliders_query = new WP_Query( $sliders_args );
    while ( $sliders_query->have_posts() ) : $sliders_query->the_post();
        
    // get slider metaboxs
    $j_slider_type = get_post_meta( $post->ID, '_jozoor_slider_type', true ); // slider type
    $j_slider_height = get_post_meta( $post->ID, '_jozoor_slider_min_height', true ); // slider height
    // slider 1 slides
    $j_slider_one_slides_data = vp_metabox('jozoor_metabox_slider_one._jozoor_slider_one_slides');
    // slider 2 slides
    $j_slider_two_slides_data = vp_metabox('jozoor_metabox_slider_two._jozoor_slider_two_slides');
        
        
    // check slider height
    if(!empty($j_slider_height)) {
      $j_slider_height_style = ' style="min-height:'.$j_slider_height.'px"';  
    } else {
      $j_slider_height_style = '';
    }
        
    
    if($j_slider_type == 'slider2') {
        
    echo '<div class="slider-2 clearfix"><div class="flex-container"><div class="flexslider4 loading">
          <ul class="slides">';
            
   foreach ( $j_slider_two_slides_data as $key => $entry ) {
       
      $j_two_slide_bg_image = vp_metabox('jozoor_metabox_slider_two._jozoor_slider_two_slides.'.$key.'.slide_bg_image'); // bg image
      $j_two_slide_bg_image_repeat = vp_metabox('jozoor_metabox_slider_two._jozoor_slider_two_slides.'.$key.'.slide_bg_image_repeat'); // bg repeat
      $j_two_slide_bg_color = vp_metabox('jozoor_metabox_slider_two._jozoor_slider_two_slides.'.$key.'.slide_bg_color'); // bg color
      $j_two_slide_title = vp_metabox('jozoor_metabox_slider_two._jozoor_slider_two_slides.'.$key.'.slide_title'); // slide title
      $j_two_slide_title_bottom = vp_metabox('jozoor_metabox_slider_two._jozoor_slider_two_slides.'.$key.'.slide_title_bottom'); // title bottom 
      $j_two_slide_title_color = vp_metabox('jozoor_metabox_slider_two._jozoor_slider_two_slides.'.$key.'.slide_title_color'); // title color
      $j_two_slide_title_bg_color = vp_metabox('jozoor_metabox_slider_two._jozoor_slider_two_slides.'.$key.'.slide_title_bg_color'); // title bg color
      $j_two_slide_desc = vp_metabox('jozoor_metabox_slider_two._jozoor_slider_two_slides.'.$key.'.slide_desc');  // slide description
      $j_two_slide_desc_bottom = vp_metabox('jozoor_metabox_slider_two._jozoor_slider_two_slides.'.$key.'.slide_desc_bottom'); // description bottom
      $j_two_slide_desc_color = vp_metabox('jozoor_metabox_slider_two._jozoor_slider_two_slides.'.$key.'.slide_desc_color'); // description color
      $j_two_slide_buttons = vp_metabox('jozoor_metabox_slider_two._jozoor_slider_two_slides.'.$key.'.slide_buttons'); // slide buttons   
      $j_two_slide_buttons_bottom = vp_metabox('jozoor_metabox_slider_two._jozoor_slider_two_slides.'.$key.'.slide_buttons_bottom'); // buttons bottom
      $j_two_slide_image = vp_metabox('jozoor_metabox_slider_two._jozoor_slider_two_slides.'.$key.'.slide_image');  // slide image
      $j_two_slide_image_bottom = vp_metabox('jozoor_metabox_slider_two._jozoor_slider_two_slides.'.$key.'.slide_image_bottom'); // image bottom
      $j_two_slide_global_link = vp_metabox('jozoor_metabox_slider_two._jozoor_slider_two_slides.'.$key.'.slide_global_link'); // global link
        
      // check bg image
      if(!empty($j_two_slide_bg_image)) {
          
        if(!empty($j_two_slide_bg_image_repeat)) { $j_bg_image_repeat = $j_two_slide_bg_image_repeat; } else { $j_bg_image_repeat = 'no-repeat'; }
        if($j_bg_image_repeat == 'repeat') {
        $j_two_slide_bg_image_content = 'background:url('.$j_two_slide_bg_image.') '.$j_bg_image_repeat.';';
        } else {
        $j_two_slide_bg_image_content = 'background:url('.$j_two_slide_bg_image.') '.$j_bg_image_repeat.';background-position:center;background-size: cover;-moz-background-size: cover;
        -webkit-background-size: cover;-o-background-size: cover;';  
        }
          
      } else { $j_two_slide_bg_image_content = ''; }
        
      // check bg color
      if(!empty($j_two_slide_bg_color)) {
          
        $j_two_slide_bg_color_content = 'background-color:'.$j_two_slide_bg_color.';';  $j_two_slide_bg_color_class = '';
          
      } else { $j_two_slide_bg_color_content = ''; $j_two_slide_bg_color_class = 'class="slide-bg-color"'; }
        
      // check title
      if(!empty($j_two_slide_title)) {
          
        if(!empty($j_two_slide_title_bottom)) { $j_title_bottom = $j_two_slide_title_bottom; } else { $j_title_bottom = '45'; }
        if(!empty($j_two_slide_title_color)) { $j_title_color = 'color:'.$j_two_slide_title_color.';'; } else { $j_title_color = ''; }
        if(!empty($j_two_slide_title_bg_color)) { $j_title_bg_color = 'background:'.$j_two_slide_title_bg_color.';'; } else { $j_title_bg_color = ''; }
        if(!empty($j_two_slide_global_link)) { 
            $j_global_link_start = '<a href="'.$j_two_slide_global_link.'"'.$j_title_color.'>'; 
            $j_global_link_end = '</a>'; 
        } else { $j_global_link_start = ''; $j_global_link_end = ''; }
        $j_two_slide_title_content = '<h2 data-bottomtitle="'.$j_title_bottom.'%"><span style="'.$j_title_color.$j_title_bg_color.'">'.$j_global_link_start.''.$j_two_slide_title.''.$j_global_link_end.'</span></h2>';
          
      } else { $j_two_slide_title_content = ''; } 
       
      // check desc
      if(!empty($j_two_slide_desc)) {
          
        if(!empty($j_two_slide_desc_bottom)) { $j_desc_bottom = $j_two_slide_desc_bottom; } else { $j_desc_bottom = '33'; }
        if(!empty($j_two_slide_desc_color)) { $j_desc_color = ' style="color:'.$j_two_slide_desc_color.'"'; } else { $j_desc_color = ''; }
        if(!empty($j_two_slide_global_link)) { 
            $j_global_link_start = '<a href="'.$j_two_slide_global_link.'"'.$j_desc_color.'>'; 
            $j_global_link_end = '</a>'; 
        } else { $j_global_link_start = ''; $j_global_link_end = ''; }
        $j_two_slide_desc_content = '<div class="slide-desc" data-bottomtext="'.$j_desc_bottom.'%"><span'.$j_desc_color.'>'.$j_global_link_start.''.$j_two_slide_desc.''.$j_global_link_end.'</span></div>';
          
      } else { $j_two_slide_desc_content = ''; }
       
      // check buttons textarea
      if(!empty($j_two_slide_buttons)) {
          
        if(!empty($j_two_slide_buttons_bottom)) { $j_buttons_bottom = $j_two_slide_buttons_bottom; } else { $j_buttons_bottom = '18'; }
        $j_two_slide_buttons_content = '<div class="links" data-bottomlinks="'.$j_buttons_bottom.'%"><span>'.do_shortcode(wpautop($j_two_slide_buttons)).'</span></div>';
          
      } else { $j_two_slide_buttons_content = ''; }
       
      // check image
      if(!empty($j_two_slide_image)) {
        if(!empty($j_two_slide_image_bottom)) { $j_image_bottom = $j_two_slide_image_bottom; } else { $j_image_bottom = '0'; }
        if(!empty($j_two_slide_global_link)) { 
            $j_global_link_start = '<a href="'.$j_two_slide_global_link.'">'; 
            $j_global_link_end = '</a>'; 
        } else { $j_global_link_start = ''; $j_global_link_end = ''; }
        $j_two_slide_image_content = '<div class="item" data-bottomimage="'.$j_image_bottom.'%"><center>'.$j_global_link_start.'<img src="'.$j_two_slide_image.'" alt="'.$j_two_slide_title.'">'.$j_global_link_end.'</center></div>';  
      }
      else { $j_two_slide_image_content = ''; }
       
      // check if all fields empty
      if( empty($j_two_slide_title) && empty($j_two_slide_desc) && empty($j_two_slide_buttons) ) {
         $j_item_class = ' image-video-item';   
      } else {
          $j_item_class = '';
      }
       
    ?>

    <li <?php echo $j_two_slide_bg_color_class; ?> style="<?php echo $j_two_slide_bg_color_content.$j_two_slide_bg_image_content; ?>">
          
     <div class="container">
     <div class="sixteen columns contain<?php echo $j_item_class; ?>"<?php echo $j_slider_height_style; ?>>
          
     <?php
     echo $j_two_slide_title_content.$j_two_slide_desc_content.$j_two_slide_buttons_content.$j_two_slide_image_content;
     ?>
           
     </div>
     </div>
          
   </li>

    <?php
    } // end foreach 
        
    echo '</ul>
          </div></div></div><!-- End slider -->';
        
    } // end slider 2
        
    else {
        
    echo '<div class="slider-1 clearfix"><div class="flex-container"><div class="flexslider loading">
          <ul class="slides">';
    
   
    foreach ( $j_slider_one_slides_data as $key => $entry ) {
       
      $j_one_slide_bg_image = vp_metabox('jozoor_metabox_slider_one._jozoor_slider_one_slides.'.$key.'.slide_bg_image'); // bg image
      $j_one_slide_bg_image_repeat = vp_metabox('jozoor_metabox_slider_one._jozoor_slider_one_slides.'.$key.'.slide_bg_image_repeat'); // bg repeat
      $j_one_slide_bg_color = vp_metabox('jozoor_metabox_slider_one._jozoor_slider_one_slides.'.$key.'.slide_bg_color'); // bg color
      $j_one_slide_title = vp_metabox('jozoor_metabox_slider_one._jozoor_slider_one_slides.'.$key.'.slide_title'); // slide title
      $j_one_slide_title_top = vp_metabox('jozoor_metabox_slider_one._jozoor_slider_one_slides.'.$key.'.slide_title_top'); // title top 
      $j_one_slide_desc = vp_metabox('jozoor_metabox_slider_one._jozoor_slider_one_slides.'.$key.'.slide_desc');  // slide description
      $j_one_slide_desc_bottom = vp_metabox('jozoor_metabox_slider_one._jozoor_slider_one_slides.'.$key.'.slide_desc_bottom'); // description bottom
      $j_one_slide_title_desc_color = vp_metabox('jozoor_metabox_slider_one._jozoor_slider_one_slides.'.$key.'.slide_title_desc_color'); // title/desc color
      $j_one_slide_buttons = vp_metabox('jozoor_metabox_slider_one._jozoor_slider_one_slides.'.$key.'.slide_buttons'); // slide buttons   
      $j_one_slide_buttons_bottom = vp_metabox('jozoor_metabox_slider_one._jozoor_slider_one_slides.'.$key.'.slide_buttons_bottom'); // buttons bottom
      $j_one_slide_image = vp_metabox('jozoor_metabox_slider_one._jozoor_slider_one_slides.'.$key.'.slide_image');  // slide image
      $j_one_slide_image_top = vp_metabox('jozoor_metabox_slider_one._jozoor_slider_one_slides.'.$key.'.slide_image_top'); // image top
      $j_one_slide_global_link = vp_metabox('jozoor_metabox_slider_one._jozoor_slider_one_slides.'.$key.'.slide_global_link'); // global link
        
      // check bg image
      if(!empty($j_one_slide_bg_image)) {
          
        if(!empty($j_one_slide_bg_image_repeat)) { $j_bg_image_repeat = $j_one_slide_bg_image_repeat; } else { $j_bg_image_repeat = 'no-repeat'; }
        if($j_bg_image_repeat == 'repeat') {
        $j_one_slide_bg_image_content = 'background:url('.$j_one_slide_bg_image.') '.$j_bg_image_repeat.';';
        } else {
        $j_one_slide_bg_image_content = 'background:url('.$j_one_slide_bg_image.') '.$j_bg_image_repeat.';background-position:center;background-size: cover;-moz-background-size: cover;
        -webkit-background-size: cover;-o-background-size: cover;';  
        }
          
      } else { $j_one_slide_bg_image_content = ''; }
        
      // check bg color
      if(!empty($j_one_slide_bg_color)) {
          
        $j_one_slide_bg_color_content = 'background-color:'.$j_one_slide_bg_color.';';  $j_one_slide_bg_color_class = '';
          
      } else { $j_one_slide_bg_color_content = ''; $j_one_slide_bg_color_class = 'class="slide-bg-color"'; }
        
      // check title
      if(!empty($j_one_slide_title)) {
          
        if(!empty($j_one_slide_title_top)) { $j_title_top = $j_one_slide_title_top; } else { $j_title_top = '20'; }
        if(!empty($j_one_slide_title_desc_color)) { $j_title_desc_color = ' style="color:'.$j_one_slide_title_desc_color.'"'; } else { $j_title_desc_color = ''; }
        if(!empty($j_one_slide_global_link)) { 
            $j_global_link_start = '<a href="'.$j_one_slide_global_link.'"'.$j_title_desc_color.'>'; 
            $j_global_link_end = '</a>'; 
        } else { $j_global_link_start = ''; $j_global_link_end = ''; }
        $j_one_slide_title_content = '<h2 data-toptitle="'.$j_title_top.'%"'.$j_title_desc_color.'>'.$j_global_link_start.''.$j_one_slide_title.''.$j_global_link_end.'</h2>';
          
      } else { $j_one_slide_title_content = ''; }
        
      // check desc
      if(!empty($j_one_slide_desc)) {
          
        if(!empty($j_one_slide_desc_bottom)) { $j_desc_bottom = $j_one_slide_desc_bottom; } else { $j_desc_bottom = '39'; }
        if(!empty($j_one_slide_title_desc_color)) { $j_title_desc_color = ' style="color:'.$j_one_slide_title_desc_color.'"'; } else { $j_title_desc_color = ''; }
        if(!empty($j_one_slide_global_link)) { 
            $j_global_link_start = '<a href="'.$j_one_slide_global_link.'"'.$j_title_desc_color.'>'; 
            $j_global_link_end = '</a>'; 
        } else { $j_global_link_start = ''; $j_global_link_end = ''; }
        $j_one_slide_desc_content = '<div class="slide-desc" data-bottomtext="'.$j_desc_bottom.'%"'.$j_title_desc_color.'>'.$j_global_link_start.''.$j_one_slide_desc.''.$j_global_link_end.'</div>';
          
      } else { $j_one_slide_desc_content = ''; }
        
      // check buttons textarea
      if(!empty($j_one_slide_buttons)) {
          
        if(!empty($j_one_slide_buttons_bottom)) { $j_buttons_bottom = $j_one_slide_buttons_bottom; } else { $j_buttons_bottom = '20'; }
        $j_one_slide_buttons_content = '<div class="links" data-bottomlinks="'.$j_buttons_bottom.'%">'.do_shortcode(wpautop($j_one_slide_buttons)).'</div>';
          
      } else { $j_one_slide_buttons_content = ''; }
        
      // check image
      if(!empty($j_one_slide_image)) {
        if(!empty($j_one_slide_image_top)) { $j_image_top = $j_one_slide_image_top; } else { $j_image_top = '16'; }
        if(!empty($j_one_slide_global_link)) { 
            $j_global_link_start = '<a href="'.$j_one_slide_global_link.'">'; 
            $j_global_link_end = '</a>'; 
        } else { $j_global_link_start = ''; $j_global_link_end = ''; }
        $j_one_slide_image_content = '<div class="item" data-topimage="'.$j_image_top.'%">'.$j_global_link_start.'<img src="'.$j_one_slide_image.'" alt="'.$j_one_slide_title.'">'.$j_global_link_end.'</div>';  
      }
      else { $j_one_slide_image_content = ''; }
       
    ?>

    <li <?php echo $j_one_slide_bg_color_class; ?> style="<?php echo $j_one_slide_bg_color_content.$j_one_slide_bg_image_content; ?>">
          
     <div class="container">
     <div class="sixteen columns contain"<?php echo $j_slider_height_style; ?>>
          
     <?php
     echo $j_one_slide_title_content.$j_one_slide_desc_content.$j_one_slide_buttons_content.$j_one_slide_image_content;
     ?>
           
     </div>
     </div>
          
   </li>

    <?php
    } // end foreach
    

    echo '</ul>
          </div></div></div><!-- End slider -->';

    } // end slider 1
    
    
    endwhile;
    wp_reset_postdata();
        
    }
        
    }
    
}
}
}


#=============================================================#
# Builder Framework
#=============================================================#
require_once( 'vaf_framework/bootstrap.php' );

// create content builder
function jozoor_theme_init_builder() {
    
    // get slider 1
    $jozoor_mb_path_slider_1  = get_template_directory() . '/inc/content_builder/slider-1.php';
    // get slider 2
    $jozoor_mb_path_slider_2  = get_template_directory() . '/inc/content_builder/slider-2.php';
    // get page builder
    $jozoor_mb_path_page_builder  = get_template_directory() . '/inc/content_builder/page-builder.php';
    
    // activate metaboxs for slider 1
    $mb = new VP_Metabox(array(
        'id'          => 'jozoor_metabox_slider_one',
        'types'       => array('sliders'),
        'title'       => __('Create Slider', 'jozoorthemes').' [ 1 ]',
        'priority'    => 'high',
        'is_dev_mode' => false,
        'template'    => $jozoor_mb_path_slider_1
    ));
    
    // activate metaboxs for slider 2
    $mb = new VP_Metabox(array(
        'id'          => 'jozoor_metabox_slider_two',
        'types'       => array('sliders'),
        'title'       => __('Create Slider', 'jozoorthemes').' [ 2 ]',
        'priority'    => 'high',
        'is_dev_mode' => false,
        'template'    => $jozoor_mb_path_slider_2
    ));
    
    // activate metaboxs for page builder
    $mb = new VP_Metabox(array(
        'id'          => 'jozoor_metabox_page_builder',
        'types'       => array('page'),
        'title'       => __('Build Your Page Content', 'jozoorthemes'),
        'priority'    => 'high',
        'is_dev_mode' => false,
        'template'    => $jozoor_mb_path_page_builder
    ));
    
    // activate check radiobutton section bg color
    VP_Security::instance()->whitelist_function('vp_dep_is_color');
    function vp_dep_is_color($value)
    {
	if($value === 'color')
		return true;
	return false;
    }

    // activate check radiobutton section bg image
    VP_Security::instance()->whitelist_function('vp_dep_is_image');
    function vp_dep_is_image($value)
    {
	if($value === 'image')
		return true;
	return false;
    }
    
    // activate select option for custom bg color
    VP_Security::instance()->whitelist_function('vp_dep_is_customcolor');
    function vp_dep_is_customcolor($value)
    {
	if($value === 'customcolor')
		return true;
	return false;
    }
    
}
add_action( 'after_setup_theme', 'jozoor_theme_init_builder' );


#=============================================================#
# clean section & menu item name for onepage
#=============================================================#
if ( !function_exists( 'jozoor_clean_string' ) ) {
function jozoor_clean_string($string) {
    
   // replaces all spaces with hyphens
   $string = str_replace(' ', '-', $string);
   // removes special chars.
   $string = preg_replace('/[^A-Za-z0-9أ-يא-ת\-]/ui', '', $string);
   // replaces multiple hyphens with single one
   return preg_replace('/-+/', '-', $string);
}
}


#=============================================================#
# TGM_Plugin_Activation
#=============================================================#
// Include the TGM_Plugin_Activation class.
require_once dirname( __FILE__ ) . '/inc/class-tgm-plugin-activation.php';

// Register the required plugins for this theme.
add_action( 'tgmpa_register', 'jozoor_register_required_plugins' );

function jozoor_register_required_plugins() {

    // Array of plugins.
    $plugins = array(

        // Contact Form 7
        array(
            'name'      => 'Contact Form 7',
            'slug'      => 'contact-form-7',
            'required'  => true,
        ),
        // WooCommerce 
        array(
            'name'      => 'Widget Importer & Exporter',
            'slug'      => 'widget-importer-exporter',
            'required'  => true,
        ),
        // WooCommerce 
        array(
            'name'      => 'WooCommerce',
            'slug'      => 'woocommerce',
            'required'  => false,
        ),
        // WooCommerce Cart Tab 
        array(
            'name'      => 'WooCommerce Cart Tab',
            'slug'      => 'woocommerce-cart-tab',
            'required'  => false,
        ),

    );

    // Array of configuration settings.
    $config = array(
        'id'           => 'tgmpa',                 // Unique ID for hashing notices for multiple instances of TGMPA.
        'default_path' => '',                      // Default absolute path to pre-packaged plugins.
        'menu'         => 'tgmpa-install-plugins', // Menu slug.
        'has_notices'  => true,                    // Show admin notices or not.
        'dismissable'  => true,                    // If false, a user cannot dismiss the nag message.
        'dismiss_msg'  => '',                      // If 'dismissable' is false, this message will be output at top of nag.
        'is_automatic' => false,                   // Automatically activate plugins after installation or not.
        'message'      => '',                      // Message to output right before the plugins table.
        'strings'      => array(
            'page_title'                      => __( 'Install Required Plugins', 'tgmpa' ),
            'menu_title'                      => __( 'Install Plugins', 'tgmpa' ),
            'installing'                      => __( 'Installing Plugin: %s', 'tgmpa' ), // %s = plugin name.
            'oops'                            => __( 'Something went wrong with the plugin API.', 'tgmpa' ),
            'notice_can_install_required'     => _n_noop( 'This theme requires the following plugin: %1$s.', 'This theme requires the following plugins: %1$s.', 'tgmpa' ), // %1$s = plugin name(s).
            'notice_can_install_recommended'  => _n_noop( 'This theme recommends the following plugin: %1$s.', 'This theme recommends the following plugins: %1$s.', 'tgmpa' ), // %1$s = plugin name(s).
            'notice_cannot_install'           => _n_noop( 'Sorry, but you do not have the correct permissions to install the %s plugin. Contact the administrator of this site for help on getting the plugin installed.', 'Sorry, but you do not have the correct permissions to install the %s plugins. Contact the administrator of this site for help on getting the plugins installed.', 'tgmpa' ), // %1$s = plugin name(s).
            'notice_can_activate_required'    => _n_noop( 'The following required plugin is currently inactive: %1$s.', 'The following required plugins are currently inactive: %1$s.', 'tgmpa' ), // %1$s = plugin name(s).
            'notice_can_activate_recommended' => _n_noop( 'The following recommended plugin is currently inactive: %1$s.', 'The following recommended plugins are currently inactive: %1$s.', 'tgmpa' ), // %1$s = plugin name(s).
            'notice_cannot_activate'          => _n_noop( 'Sorry, but you do not have the correct permissions to activate the %s plugin. Contact the administrator of this site for help on getting the plugin activated.', 'Sorry, but you do not have the correct permissions to activate the %s plugins. Contact the administrator of this site for help on getting the plugins activated.', 'tgmpa' ), // %1$s = plugin name(s).
            'notice_ask_to_update'            => _n_noop( 'The following plugin needs to be updated to its latest version to ensure maximum compatibility with this theme: %1$s.', 'The following plugins need to be updated to their latest version to ensure maximum compatibility with this theme: %1$s.', 'tgmpa' ), // %1$s = plugin name(s).
            'notice_cannot_update'            => _n_noop( 'Sorry, but you do not have the correct permissions to update the %s plugin. Contact the administrator of this site for help on getting the plugin updated.', 'Sorry, but you do not have the correct permissions to update the %s plugins. Contact the administrator of this site for help on getting the plugins updated.', 'tgmpa' ), // %1$s = plugin name(s).
            'install_link'                    => _n_noop( 'Begin installing plugin', 'Begin installing plugins', 'tgmpa' ),
            'activate_link'                   => _n_noop( 'Begin activating plugin', 'Begin activating plugins', 'tgmpa' ),
            'return'                          => __( 'Return to Required Plugins Installer', 'tgmpa' ),
            'plugin_activated'                => __( 'Plugin activated successfully.', 'tgmpa' ),
            'complete'                        => __( 'All plugins installed and activated successfully. %s', 'tgmpa' ), // %s = dashboard link.
            'nag_type'                        => 'updated' // Determines admin notice type - can only be 'updated', 'update-nag' or 'error'.
        )
    );

    tgmpa( $plugins, $config );

}


#=============================================================#
# woocommerce custom configuration
#=============================================================#
include_once (get_template_directory() . '/inc/woocommerce-config.php');




 