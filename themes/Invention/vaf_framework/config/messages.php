<?php

return array(

	////////////////////////////////////////
	// Localized JS Message Configuration //
	////////////////////////////////////////

	/**
	 * Validation Messages
	 */
	'validation' => array(
		'alphabet'     => __('Value needs to be Alphabet', 'jozoorthemes'),
		'alphanumeric' => __('Value needs to be Alphanumeric', 'jozoorthemes'),
		'numeric'      => __('Value needs to be Numeric', 'jozoorthemes'),
		'email'        => __('Value needs to be Valid Email', 'jozoorthemes'),
		'url'          => __('Value needs to be Valid URL', 'jozoorthemes'),
		'maxlength'    => __('Length needs to be less than {0} characters', 'jozoorthemes'),
		'minlength'    => __('Length needs to be more than {0} characters', 'jozoorthemes'),
		'maxselected'  => __('Select no more than {0} items', 'jozoorthemes'),
		'minselected'  => __('Select at least {0} items', 'jozoorthemes'),
		'required'     => __('This is required', 'jozoorthemes'),
	),

	/**
	 * Import / Export Messages
	 */
	'util' => array(
		'import_success'    => __('Import succeed, option page will be refreshed..', 'jozoorthemes'),
		'import_failed'     => __('Import failed', 'jozoorthemes'),
		'export_success'    => __('Export succeed, copy the serialized options', 'jozoorthemes'),
		'export_failed'     => __('Export failed', 'jozoorthemes'),
		'restore_success'   => __('Restoration succeed, option page will be refreshed..', 'jozoorthemes'),
		'restore_nochanges' => __('Options identical to default', 'jozoorthemes'),
		'restore_failed'    => __('Restoration failed', 'jozoorthemes'),
	),

	/**
	 * Control Fields String
	 */
	'control' => array(
		// select2 select box
		'select2_placeholder' => __('Select option(s)', 'jozoorthemes'),
		// fontawesome chooser
		'fac_placeholder'     => __('Select an Icon', 'jozoorthemes'),
	),

);

/**
 * EOF
 */