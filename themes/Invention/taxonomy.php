<?php get_header(); ?>

   <?php
   // get menu item id
   global $wpdb;
   $post_id = $wpdb->get_var( 'SELECT ID FROM '.$wpdb->posts.' WHERE post_name="'.$jozoor_projects_category_current_page.'" AND post_type="page" AND post_status="publish"' );
   $post_title = $wpdb->get_var( 'SELECT post_id FROM '.$wpdb->postmeta.' WHERE meta_key="_menu_item_object_id" AND meta_value="'.$post_id.'"' );
   
   // add active class for nav item
   if (!is_admin() && $jozoor_projects_category_current_page != 'Select Page Name') { ?>
   <script>
    jQuery(document).ready(function($) {
     $("#menu-item-<?php echo $post_title;  ?>").addClass("current-menu-item");
    });
    </script>
   <?php } ?>

   <?php
   // check column
   if( $jozoor_projects_category_layout_style == '2 Columns' ) { $j_portfolio_layout_class = 'eight columns col2'; }
   elseif ( $jozoor_projects_category_layout_style == '3 Columns' ) { $j_portfolio_layout_class = 'one-third column col3'; }
   else { $j_portfolio_layout_class = 'four columns col4'; }
   ?>

   <!-- Start main content -->
   <div class="container main-content clearfix">
       
   <div class="portfolio bottom-3">
   
   <div id="contain" class="clearfix"> 
       
   <?php
   if(have_posts()) : while(have_posts()) : the_post(); 
   ?>
       
   <div id="post-<?php the_ID(); ?>" class="<?php $allClasses = get_post_class(); foreach ($allClasses as $class) { echo $class . ' '; } ?><?php echo $j_portfolio_layout_class; ?> item">
    <a href="<?php echo get_permalink(); ?>">
    <?php echo get_the_post_thumbnail($post->ID, 'portfolio-thumb', array('title' => '')); ?>
    <div class="img-caption">
    <div class="desc">
      <h3><?php the_title(); ?></h3>
      <p><?php
      $j_terms = get_the_terms( $post->ID, 'portfolio-category' );						
      if ( $j_terms && ! is_wp_error( $j_terms ) ) : 
      $j_draught_links = array();
      foreach ( $j_terms as $term ) {
      $j_draught_links[] = $term->name;
      }		
      echo $j_on_draught = join( ", ", $j_draught_links );
      endif;
      ?></p>
      <span>+</span></div>
    </div><!-- hover effect -->
    </a>
   </div>
   
   <?php
   endwhile; 

   echo '</div><!-- End contain -->';

   echo '<div class="sixteen columns">';
   jozoor_pagination(); // Pagination
   echo '</div>';

   endif;
   ?>
   </div><!-- End portfolio -->
       
<?php get_footer(); ?> 