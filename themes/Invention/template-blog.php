<?php
/*
Template Name: Blog
*/
?>

<?php get_header(); ?>

<?php // get metaboxes
global $post;
$j_page_layout = get_post_meta($post->ID, '_jozoor_page_layout', true);
$j_page_sidebar = get_post_meta($post->ID, '_jozoor_page_sidebar', true);
// blog options
$j_blog_style = get_post_meta($post->ID, '_jozoor_blog_style', true);
$j_using_cats_filter = get_post_meta($post->ID, '_jozoor_using_cats_filter', true);
?>

    <!-- Start main content -->
    <div class="container main-content clearfix">

<?php if ($j_blog_style == 'blog3') { ?>
    <div class="blog-3 bottom-2 top-0">
    <?php } else { ?>

    <?php if ($j_page_layout == 'left') {
        if ($j_page_sidebar == 'default') {
            get_sidebar();
        } else {
            echo '<!-- Start Sidebar Widgets --><aside class="five columns sidebar bottom-3">';
            if (function_exists('dynamic_sidebar') && dynamic_sidebar($j_page_sidebar)) : endif;
            echo '</aside><!-- End Sidebar Widgets -->';
        }
    } ?>

    <!-- Start Page Content -->
    <?php if ($j_page_layout == 'right' || $j_page_layout == 'left') { ?>
    <div class="eleven columns bottom-3">
    <?php } else { ?>
    <div class="sixteen columns full-width bottom-3">
    <?php } ?>

<?php } ?>

<?php

// page loop
while (have_posts()) : the_post();

    // check post protected
    if (!post_password_required()) {

        if (has_post_thumbnail()) {
            echo '<div class="image-post">';
            echo get_the_post_thumbnail($post->ID, 'full', array('title' => ''));
            echo '</div><!-- End image-post -->';
        }
    }

    if (!empty($post->post_content)) {
        echo '<div class="entry-content post-content bottom-3">';
        the_content();
        echo '</div><!-- End page-content -->';
    }

endwhile;

// Reset Query
wp_reset_query();


// blog posts loop
$query_string;
parse_str($query_string, $my_query_array);
$paged = (isset($my_query_array['paged']) && !empty($my_query_array['paged'])) ? $my_query_array['paged'] : 1;

// check blog style
if ($j_blog_style == 'blog3') {
    $j_folder_template_part = 'blog-3/';
} elseif ($j_blog_style == 'blog2') {
    $j_folder_template_part = 'blog-2/';
} else {
    $j_folder_template_part = '';
}

// get posts per page
if ($jozoor_custom_posts_per_page == 1) {
    if ($j_blog_style == 'blog3') {
        $j_posts_per_page = $jozoor_blog3_posts_per_page;
    } elseif ($j_blog_style == 'blog2') {
        $j_posts_per_page = $jozoor_blog2_posts_per_page;
    } elseif ($j_blog_style == 'blog1') {
        $j_posts_per_page = $jozoor_blog1_posts_per_page;
    } else {
        $j_posts_per_page = '';
    }
} else {
    $j_posts_per_page = '';
}

// turn on Read More in Pages
global $more;
$more = 0;
if ($j_blog_style == 'blog3') {

    if ($j_using_cats_filter == 'on') {
        fragment_cache('categoriesInner-' . $_SERVER['REQUEST_URI'], DAY_IN_SECONDS, function () use ($post) {
            ?>

            <div class="sixteen columns">
                <div class="title clearfix" id="options">
                    <ul id="filters" class="option-set clearfix" data-option-key="filter">
                        <li class="first"><a href="#*" data-option-value="*" class="selected"><?php _e("All",
                                    "jozoorthemes"); ?></a></li>
                        <?php

                        $args = array(
                            'type' => 'post',
                            'orderby' => 'name',
                            'order' => 'ASC'
                        );
                        $categories = get_categories($args);
                        foreach ($categories as $category) {
                            echo '<li><a href="#.category-' . $category->slug . '" data-option-value=".category-' . $category->slug . '">' . $category->name . '</a></li>';
                        }
                        ?>
                    </ul>
                </div>
            </div> <!-- End options -->

            <div class="clearfix"></div>

            <?php
        });
    }

    echo '<div id="contain" class="clearfix">';

}
//fragment_cache('postCard-' . $_SERVER['REQUEST_URI'], DAY_IN_SECONDS, function () use ($post, $j_posts_per_page, $paged, $j_folder_template_part, $j_blog_style) {
    query_posts('post_type=post&paged=' . $paged . '&posts_per_page=' . $j_posts_per_page . '');
    if (have_posts()) : while (have_posts()) : the_post();

        get_template_part('inc/post_formats/' . $j_folder_template_part . 'content', get_post_format());

    endwhile;

        if ($j_blog_style == 'blog3') {
            echo '</div><!-- End contain -->';
        }

        if ($j_blog_style == 'blog3') {
            echo '<div class="pagination-contain top-2">';
        }

        jozoor_pagination(); // Pagination

        if ($j_blog_style == 'blog3') {
            echo '</div>';
        }

    endif;

//});
// Reset Query
wp_reset_query();

?>

    </div><!-- End Posts -->

<?php if ($j_blog_style == 'blog3') {
} else { ?>

    <?php if ($j_page_layout == 'right') {
        if ($j_page_sidebar == 'default') {
            get_sidebar();
        } else {
            echo '<!-- Start Sidebar Widgets --><aside class="five columns sidebar bottom-3">';
            if (function_exists('dynamic_sidebar') && dynamic_sidebar($j_page_sidebar)) : endif;
            echo '</aside><!-- End Sidebar Widgets -->';
        }
    }

}
?>

<?php get_footer(); ?>