<?php include (get_template_directory() . '/inc/theme-options.php'); // get all theme options ?>
<!DOCTYPE html>
<!--[if IE 8 ]><html class="ie ie8" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 9 ]><html class="ie ie9" <?php language_attributes(); ?>> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html <?php language_attributes(); ?>> <!--<![endif]-->
<head>

  <meta charset="<?php bloginfo('charset'); ?>">
  <title><?php if($jozoor_close_site == 1) {
    
    // closing site title check
    $year = date("Y"); $month = date("n"); $day = date("j"); $hour = date("G"); $minute = date("i");
    $cons_timer = explode(" | ", $jozoor_under_cons_timer);
    
    if($cons_timer[0] < $year ) { wp_title('|', true, 'right'); } 
    elseif($cons_timer[0] == $year && $cons_timer[1] < $month) { wp_title('|', true, 'right'); }
    elseif($cons_timer[0] == $year && $cons_timer[1] == $month && $cons_timer[2] < $day) { wp_title('|', true, 'right'); }
    elseif($cons_timer[0] == $year && $cons_timer[1] == $month && $cons_timer[2] == $day && $cons_timer[3]-$cons_timer[5] < $hour) { wp_title('|', true, 'right'); }
    elseif($cons_timer[0] == $year && $cons_timer[1] == $month && $cons_timer[2] == $day && $cons_timer[3]-$cons_timer[5] == $hour && $cons_timer[4] <= $minute) { wp_title('|', true, 'right'); }
    else { if (current_user_can('administrator')){ wp_title('|', true, 'right'); } else { echo __("Under Construction", "jozoorthemes").' | '; } }
    
    } else { wp_title('|', true, 'right'); } ?><?php bloginfo('name'); ?></title>
  
  <?php if($jozoor_responsive_design == 1) { ?>
  <!-- Mobile Specific Metas -->
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
  <?php } ?>
  
  <?php echo $jozoor_custom_codes_header; // Custom Codes OR Javascript Codes in Header ?>
  
  <!-- Favicons -->
  <?php if(!empty($jozoor_favicon_image)) { ?> <link rel="shortcut icon" href="<?php echo $jozoor_favicon_image;?>"> <?php } ?>
  <?php if(!empty($jozoor_iphone_icon)) { ?> <link rel="apple-touch-icon-precomposed" href="<?php echo $jozoor_iphone_icon;?>" /> <?php } ?>
  <?php if(!empty($jozoor_iphone_retina_icon)) { ?> <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo $jozoor_iphone_retina_icon;?>" /> <?php } ?>
  <?php if(!empty($jozoor_ipad_icon)) { ?> <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo $jozoor_ipad_icon;?>" /> <?php } ?>
  <?php if(!empty($jozoor_ipad_retina_icon)) { ?> <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo $jozoor_ipad_retina_icon;?>" /> <?php } ?>
    

  <?php wp_head(); ?>
  
</head>

<?php 

  // closing site check
  if($jozoor_close_site == 1) {
    
    $year = date("Y"); $month = date("n"); $day = date("j"); $hour = date("G"); $minute = date("i");
    $cons_timer = explode(" | ", $jozoor_under_cons_timer);
    
    if($cons_timer[0] < $year ) { echo ''; } 
    elseif($cons_timer[0] == $year && $cons_timer[1] < $month) { echo ''; }
    elseif($cons_timer[0] == $year && $cons_timer[1] == $month && $cons_timer[2] < $day) { echo ''; }
    elseif($cons_timer[0] == $year && $cons_timer[1] == $month && $cons_timer[2] == $day && $cons_timer[3]-$cons_timer[5] < $hour) { echo ''; }
    elseif($cons_timer[0] == $year && $cons_timer[1] == $month && $cons_timer[2] == $day && $cons_timer[3]-$cons_timer[5] == $hour && $cons_timer[4] <= $minute) { echo ''; }
    else {  if (current_user_can('administrator')){ } else { include (get_template_directory() . '/inc/under_construction.php'); die(); } }
  
  }

 // get default body boxed class
 if($jozoor_layout_style == 'boxed') { 
    $path = explode('/', $jozoor_body_bg_image);
    $filename = $path[count($path)-1];
    $class_name = str_replace('.jpg', '', $filename); 
 } else { $class_name = ''; }

?>

<?php
global $post;

// get page custom options metaboxs
if ( is_404() || !have_posts() ) { 
    $j_page_custom_options = $j_page_layout_style = $j_page_body_bg = $j_page_body_bg_repeat = $j_page_body_bg_color = $j_page_header_layout = $j_page_fixed_header = $j_page_menu_item_bg_border_none = $j_page_header_bg = $j_page_header_bg_repeat = $j_page_hide_top_bar_header = ''; 
} else {
$j_page_custom_options = get_post_meta( wc_get_page_id('shop'), '_jozoor_page_custom_options', true ); // check custom options
$j_page_layout_style = get_post_meta( wc_get_page_id('shop'), '_jozoor_page_layout_style', true ); // page layout
$j_page_body_bg = get_post_meta( wc_get_page_id('shop'), '_jozoor_page_body_bg', true ); // body bg
$j_page_body_bg_repeat = get_post_meta( wc_get_page_id('shop'), '_jozoor_page_body_bg_repeat', true ); // body bg repeat
$j_page_body_bg_color = get_post_meta( wc_get_page_id('shop'), '_jozoor_page_body_bg_color', true ); // body bg color
$j_page_header_layout = get_post_meta( wc_get_page_id('shop'), '_jozoor_page_header_layout', true ); // header layout
$j_page_fixed_header = get_post_meta( wc_get_page_id('shop'), '_jozoor_page_fixed_header', true ); // fixed header
$j_page_menu_item_bg_border_none = get_post_meta( wc_get_page_id('shop'), '_jozoor_page_menu_item_bg_border_none', true ); // no bg border for menu item
$j_page_header_bg = get_post_meta( wc_get_page_id('shop'), '_jozoor_page_header_bg', true ); // header bg
$j_page_header_bg_repeat = get_post_meta( wc_get_page_id('shop'), '_jozoor_page_header_bg_repeat', true ); // header bg repeat
$j_page_hide_top_bar_header = get_post_meta( wc_get_page_id('shop'), '_jozoor_page_hide_top_bar_header', true ); // hide top bar in header
$j_page_show_top_header_menu = get_post_meta( wc_get_page_id('shop'), '_jozoor_page_show_top_header_menu', true ); // show top header menu
}

// custom body bg for page
if($j_page_custom_options == 'on' && $j_page_layout_style == 'boxed' && !empty($j_page_body_bg)) {
    
    if($j_page_body_bg_repeat == 'fixed') {
        $j_body_bg_custom_page_style = ' style="background:url('.$j_page_body_bg.') '.$j_page_body_bg_repeat.' !important; background-position:center !important; background-size: cover !important; -moz-background-size: cover !important; -webkit-background-size: cover !important; -o-background-size: cover !important;"';
    } else {
        $j_body_bg_custom_page_style = ' style="background:url('.$j_page_body_bg.') '.$j_page_body_bg_repeat.' !important;"';
    }
    
} else {
  $path = explode('/', $jozoor_body_bg_image);
  $filename = $path[count($path)-1];
  $class_name = str_replace('.jpg', '', $filename); 
  $j_body_bg_custom_page_style = '';  
}

// custom body bg color for page
if($j_page_custom_options == 'on' && $j_page_layout_style == 'boxed' && !empty($j_page_body_bg_color)) {
    $j_body_bg_color_custom_page_style = ' style="background:'.$j_page_body_bg_color.' !important;"';
} else {
  $j_body_bg_color_custom_page_style = '';  
}

// custom header bg for page
if($j_page_custom_options == 'on' && !empty($j_page_header_bg)) {
    
    if($j_page_header_bg_repeat == 'no-repeat') {
        $j_header_bg_custom_page_style = ' style="background:url('.$j_page_header_bg.') '.$j_page_header_bg_repeat.' !important; background-position:center !important; background-size: cover !important; -moz-background-size: cover !important; -webkit-background-size: cover !important; -o-background-size: cover !important;"';
    } else {
        $j_header_bg_custom_page_style = ' style="background:url('.$j_page_header_bg.') '.$j_page_header_bg_repeat.' !important;"';
    }
    
} else {
  $j_header_bg_custom_page_style = '';  
}
?>
  
<body <?php body_class($class_name); ?><?php echo $j_body_bg_custom_page_style.$j_body_bg_color_custom_page_style; ?>>
  
  <?php if($jozoor_loading_image == 1) { ?>  
  <div class="loader"></div>   
  <?php } ?>

  <?php 
  global $post;

  // get page builder options
  if ( is_404() || !have_posts() ) { 
      $j_using_page_builder = $j_page_builder_page_type = $j_menu_terms = '';
  } else {
  $j_using_page_builder = get_post_meta( wc_get_page_id('shop'), '_jozoor_using_page_builder', true );
  $j_page_builder_page_type = get_post_meta( wc_get_page_id('shop'), '_jozoor_page_builder_page_type', true );
  // get selected menu
  $j_page_builder_page_one_menu = '';
  $j_menu_terms = get_the_terms( wc_get_page_id('shop'), 'nav_menu' );
  if ( $j_menu_terms && ! is_wp_error( $j_menu_terms ) ) {
    $j_page_builder_page_one_menu = $j_menu_terms[0]->slug;
  }
  }

  if($j_using_page_builder == 'on') {
  if($j_page_builder_page_type == 'onepage') {
      $j_page_builder_page_type_class = ' onepage';
  } else {
      $j_page_builder_page_type_class = '';
  }
  ?>
  <div id="wrap" class="boxed page-sections<?php echo $j_page_builder_page_type_class; ?>">
  <?php
  } else { 
  ?>
  <div id="wrap" class="boxed">
  <?php } ?>
      
   <?php if($jozoor_add_custom_content_top_header == 1) { ?>
    <div class="top-header-custom-content post-content">
      <div class="container clearfix">
          <div class="sixteen columns"> <?php echo do_shortcode($jozoor_custom_content_top_header); ?> </div>
      </div><!-- End Container -->
    </div>  
   <?php } ?>
      
   <?php 
   // header layout
   if($j_page_custom_options == 'on' && $j_page_header_layout != 'none') {
      if($j_page_header_layout == 'style-default')  { $jozoor_header_style_class = ''; } else { $jozoor_header_style_class = $j_page_header_layout; }  
   } else {
      if($jozoor_header_layout == 'style-default')  { $jozoor_header_style_class = ''; } else { $jozoor_header_style_class = $jozoor_header_layout; } 
   }
   // fixed header
   if($j_page_custom_options == 'on' && $j_page_fixed_header == 'on') {
      $jozoor_header_fixed_class = 'fixed'; 
   } else {
      if($jozoor_fixed_header == 1) { $jozoor_header_fixed_class = 'fixed'; } else { $jozoor_header_fixed_class = ''; }
   }
   // no bg border for menu item
   if($j_page_custom_options == 'on' && $j_page_menu_item_bg_border_none == 'on') {
      $jozoor_header_bg_border_none_class = ' bg-border-none'; 
   } else {
      if($jozoor_menu_item_bg_border_none == 1) { $jozoor_header_bg_border_none_class = ' bg-border-none'; } else { $jozoor_header_bg_border_none_class = ''; } 
   }
   
   ?>
   <header class="<?php echo $jozoor_header_style_class.' '.$jozoor_header_fixed_class.$jozoor_header_bg_border_none_class; ?> clearfix">
       
    <?php if($j_page_custom_options == 'on' && $j_page_hide_top_bar_header == 'on') { ?>
     <div class="top-bar none"></div><!-- End top-bar -->
    <?php } else { ?>
       
    <?php if($jozoor_show_top_bar_header == 1) { ?>    
     <div class="top-bar">
           
       <div class="container clearfix">
        <div class="slidedown">
        
         <div class="eight columns left">
           <div class="phone-mail">
             <?php if(!empty($jozoor_phone_number_top_bar)) { ?>
               <a><i class="icon-phone"></i> <?php echo $jozoor_phone_number_top_bar; ?></a>
             <?php } ?>
             <?php if(!empty($jozoor_email_address_top_bar)) { 
               $pattern_matches_email_address_top_bar = '/[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b/i';
               preg_match ($pattern_matches_email_address_top_bar, $jozoor_email_address_top_bar, $matches_email_address_top_bar);
               ?>
               <a href="mailto:<?php echo $matches_email_address_top_bar[0]; ?>"><i class="icon-envelope-alt"></i> <?php echo $jozoor_email_address_top_bar; ?></a>
             <?php } ?>
           </div><!-- End phone-mail -->
         </div>
         <div class="eight columns right">
           <?php if($j_page_custom_options == 'on' && $j_page_show_top_header_menu == 'on') { 
       
           if ( has_nav_menu( 'top_header' ) ) {
			  wp_nav_menu ( array( 'menu_id' => 'top-down-nav', 'menu_class' => 'top-down-menu', 'container'=> 'ul', 'theme_location' => 'top_header', 'depth' => -1 ));
           } 
       
           } else { ?>
           <?php if($jozoor_show_top_header_menu == 1) { ?>
                
           <?php 
           if ( has_nav_menu( 'top_header' ) ) {
			  wp_nav_menu ( array( 'menu_id' => 'top-down-nav', 'menu_class' => 'top-down-menu', 'container'=> 'ul', 'theme_location' => 'top_header', 'depth' => -1 ));
           } 
           ?>
       
           <?php } else { ?> 
           <div class="social">
           <?php if ($jozoor_social_slider_header != '') { // get all social networks 
           foreach ($jozoor_social_slider_header as $slide) {  
           if(!empty($slide['name'])) {
           ?>
           <a href="<?php echo $slide['link']; ?>" target="_blank"><i class="social_icon-<?php echo $slide['name']; ?>"></i></a>
           <?php } 
           } } else { echo '<span class="none">'.__("you need to add social links from header options!", "jozoorthemes").'</span>'; } ?>
           </div>
           <?php } ?>
           <?php } ?>
          </div><!-- End social-icons -->
          
         </div><!-- End slidedown -->
       </div><!-- End Container -->
       
      </div><!-- End top-bar -->
      <?php } else { ?> 
       <div class="top-bar none"></div><!-- End top-bar -->
      <?php } ?>
       
      <?php } ?>
       
      <?php 
      // start check header layout sections
      if($j_page_custom_options == 'on' && $j_page_header_layout != 'none') { ?>
       
       
      <?php 
      // Default Header & Style 3
      if($j_page_header_layout == 'style-default' || $j_page_header_layout == 'style-4') { ?>  
       
       <div class="main-header"<?php echo $j_header_bg_custom_page_style; ?>>
       <div class="container clearfix">
         <?php if($jozoor_show_top_bar_header == 1) { ?>
         <a href="#" class="down-button"><i class="icon-angle-down"></i></a><!-- this appear on small devices -->
         <?php } ?> 
           
         <div class="one-third column">
          <div class="logo"> 
            <?php if($jozoor_logo_text_instead_image == 1 && !empty($jozoor_logo_name_text)) { ?>
              <h1 class="site-title-logo"><a href="<?php echo home_url(); ?>"><?php echo $jozoor_logo_name_text; ?></a></h1>  
            <?php } else { ?>
            <a href="<?php echo home_url(); ?>"> 
            <?php if(!empty($jozoor_logo_image)) { ?>
            <img src="<?php echo $jozoor_logo_image;?>" alt="<?php bloginfo('name'); ?>">
            <?php } else { ?>
            <img src="<?php echo get_template_directory_uri(); ?>/images/logo.png" alt="Invention - Responsive WordPress Theme" />
            <?php } ?>
            </a>
            <?php } ?>
          </div>
         </div><!-- End Logo -->
         
         <div class="two-thirds column">
          <nav id="menu" class="navigation" role="navigation">
          <a href="#"><?php echo __("Show navigation", "jozoorthemes"); ?></a><!-- this appear on small devices -->
              
          <?php  
             if($j_using_page_builder == 'on' && $j_page_builder_page_type == 'onepage') { 
              wp_nav_menu ( array( 'menu_id' => 'nav','container'=> 'ul', 'menu_class' => 'onepage-menu', 'menu' => $j_page_builder_page_one_menu ));    
             } else {
               
              if ( has_nav_menu( 'headermenu' ) ) {
			  wp_nav_menu ( array( 'menu_id' => 'nav','container'=> 'ul', 'theme_location' => 'headermenu' ));
              } 
              else { ?>
			  <ul id="nav"> <?php wp_list_pages(array( 'title_li' => false, 'sort_column'  => 'menu_order')); ?> </ul> 
              <?php }
               
             } ?> 
              
         </nav>
        </div><!-- End Menu -->
           
        </div><!-- End Container -->
        </div><!-- End main-header -->
           
        <?php } 
        // Header Style 2
        elseif($j_page_header_layout == 'style-3') { ?>
       
        <div class="main-header"<?php echo $j_header_bg_custom_page_style; ?>>
        <div class="container clearfix">
         <?php if($jozoor_show_top_bar_header == 1) { ?>
         <a href="#" class="down-button"><i class="icon-angle-down"></i></a><!-- this appear on small devices -->
         <?php } ?>
         <div class="eleven columns">
          <div class="logo"> 
            <?php if($jozoor_logo_text_instead_image == 1 && !empty($jozoor_logo_name_text)) { ?>
              <h1 class="site-title-logo"><a href="<?php echo home_url(); ?>"><?php echo $jozoor_logo_name_text; ?></a></h1>  
            <?php } else { ?>
            <a href="<?php echo home_url(); ?>"> 
            <?php if(!empty($jozoor_logo_image)) { ?>
            <img src="<?php echo $jozoor_logo_image;?>" alt="<?php bloginfo('name'); ?>">
            <?php } else { ?>
            <img src="<?php echo get_template_directory_uri(); ?>/images/logo.png" alt="Invention - Responsive WordPress Theme" />
            <?php } ?>
            </a>
            <?php } ?>
          </div>
         </div><!-- End Logo -->
         
         <div class="five columns">
          
          <?php if($jozoor_header_layout3_right_content == 'custom content') { ?>
          <!-- Custom Content -->
          <div class="header-custom-content post-content">
            <?php echo do_shortcode($jozoor_header_layout3_right_custom_content); ?>  
          </div>
          <!-- End -->
          <?php } else { ?>
          <!-- Search Widget -->
          <div class="widget search">
           <form action="<?php echo home_url(); ?>" id="searchform" method="get">
           <input type="text" id="s" name="s" class="text-search" value="<?php _e("Search...", "jozoorthemes"); ?>" onBlur="if(this.value == '') { this.value = '<?php _e("Search...", "jozoorthemes"); ?>'; }" onfocus="if(this.value == '<?php _e("Search...", "jozoorthemes"); ?>') { this.value = ''; }" />
           <input type="submit" value="" id="searchsubmit" class="submit-search" />
           </form>
          </div>
          <!-- End -->
          <?php } ?>     
             
         </div>
       
        </div><!-- End Container -->
        </div><!-- End main-header --> 
       
        <div class="down-header">
         <div class="container clearfix">
             
           <div class="sixteen columns">
             <nav id="menu" class="navigation" role="navigation">
             <a href="#"><?php echo __("Show navigation", "jozoorthemes"); ?></a><!-- this appear on small devices -->
                 
             <?php  
             if($j_using_page_builder == 'on' && $j_page_builder_page_type == 'onepage') { 
              wp_nav_menu ( array( 'menu_id' => 'nav','container'=> 'ul', 'menu_class' => 'onepage-menu', 'menu' => $j_page_builder_page_one_menu ));
             } else {
               
              if ( has_nav_menu( 'headermenu' ) ) {
			  wp_nav_menu ( array( 'menu_id' => 'nav','container'=> 'ul', 'theme_location' => 'headermenu' ));
              } 
              else { ?>
			  <ul id="nav"> <?php wp_list_pages(array( 'title_li' => false, 'sort_column'  => 'menu_order')); ?> </ul> 
              <?php }
               
             } ?> 
            
            </nav>
          </div><!-- End Menu --> 
       
        </div><!-- End Container -->
        </div><!-- End down-header -->
           
        <?php } 
        // Header Style 4
        elseif($j_page_header_layout == 'style-3 style-5') { ?>
       
         <div class="main-header"<?php echo $j_header_bg_custom_page_style; ?>>
          <div class="container clearfix">
           <?php if($jozoor_show_top_bar_header == 1) { ?>
            <a href="#" class="down-button"><i class="icon-angle-down"></i></a><!-- this appear on small devices -->
           <?php } ?>
              
           <div class="sixteen columns">
            <div class="logo">
            <?php if($jozoor_logo_text_instead_image == 1 && !empty($jozoor_logo_name_text)) { ?>
            <h1 class="site-title-logo"><a href="<?php echo home_url(); ?>"><?php echo $jozoor_logo_name_text; ?></a></h1>  
            <?php } else { ?>
            <a href="<?php echo home_url(); ?>"> 
            <?php if(!empty($jozoor_logo_image)) { ?>
            <img src="<?php echo $jozoor_logo_image;?>" alt="<?php bloginfo('name'); ?>">
            <?php } else { ?>
            <img src="<?php echo get_template_directory_uri(); ?>/images/logo.png" alt="Invention - Responsive WordPress Theme" />
            <?php } ?>
            </a>
            <?php } ?>
            </div>
           </div><!-- End Logo -->
       
          </div><!-- End Container -->
         </div><!-- End main-header -->
       
         <div class="down-header">
         <div class="container clearfix">
             
           <div class="sixteen columns">
             <nav id="menu" class="navigation" role="navigation">
             <a href="#"><?php echo __("Show navigation", "jozoorthemes"); ?></a><!-- this appear on small devices -->
                 
             <?php  
             if($j_using_page_builder == 'on' && $j_page_builder_page_type == 'onepage') { 
              wp_nav_menu ( array( 'menu_id' => 'nav','container'=> 'ul', 'menu_class' => 'onepage-menu', 'menu' => $j_page_builder_page_one_menu ));    
             } else {
               
              if ( has_nav_menu( 'headermenu' ) ) {
			  wp_nav_menu ( array( 'menu_id' => 'nav','container'=> 'ul', 'theme_location' => 'headermenu' ));
              } 
              else { ?>
			  <ul id="nav"> <?php wp_list_pages(array( 'title_li' => false, 'sort_column'  => 'menu_order')); ?> </ul> 
              <?php }
               
             } ?> 
               
            </nav>
          </div><!-- End Menu --> 
       
        </div><!-- End Container -->
        </div><!-- End down-header -->
           
        <?php } ?>
       
       
      <?php } // end if custom options on 

      else { // start theme option header layout sections
      ?>
           
      <?php 
      // Default Header & Style 3
      if($jozoor_header_layout == 'style-default' || $jozoor_header_layout == 'style-4') { ?>  
       
       <div class="main-header"<?php echo $j_header_bg_custom_page_style; ?>>
       <div class="container clearfix">
         <?php if($jozoor_show_top_bar_header == 1) { ?>
         <a href="#" class="down-button"><i class="icon-angle-down"></i></a><!-- this appear on small devices -->
         <?php } ?> 
           
         <div class="one-third column">
          <div class="logo"> 
            <?php if($jozoor_logo_text_instead_image == 1 && !empty($jozoor_logo_name_text)) { ?>
              <h1 class="site-title-logo"><a href="<?php echo home_url(); ?>"><?php echo $jozoor_logo_name_text; ?></a></h1>  
            <?php } else { ?>
            <a href="<?php echo home_url(); ?>"> 
            <?php if(!empty($jozoor_logo_image)) { ?>
            <img src="<?php echo $jozoor_logo_image;?>" alt="<?php bloginfo('name'); ?>">
            <?php } else { ?>
            <img src="<?php echo get_template_directory_uri(); ?>/images/logo.png" alt="Invention - Responsive WordPress Theme" />
            <?php } ?>
            </a>
            <?php } ?>
          </div>
         </div><!-- End Logo -->
         
         <div class="two-thirds column">
          <nav id="menu" class="navigation" role="navigation">
          <a href="#"><?php echo __("Show navigation", "jozoorthemes"); ?></a><!-- this appear on small devices -->
              
          <?php  
             if($j_using_page_builder == 'on' && $j_page_builder_page_type == 'onepage') { 
              wp_nav_menu ( array( 'menu_id' => 'nav','container'=> 'ul', 'menu_class' => 'onepage-menu', 'menu' => $j_page_builder_page_one_menu ));    
             } else {
               
              if ( has_nav_menu( 'headermenu' ) ) {
			  wp_nav_menu ( array( 'menu_id' => 'nav','container'=> 'ul', 'theme_location' => 'headermenu' ));
              } 
              else { ?>
			  <ul id="nav"> <?php wp_list_pages(array( 'title_li' => false, 'sort_column'  => 'menu_order')); ?> </ul> 
              <?php }
               
             } ?> 
              
         </nav>
        </div><!-- End Menu -->
           
        </div><!-- End Container -->
        </div><!-- End main-header -->
           
        <?php } 
        // Header Style 2
        elseif($jozoor_header_layout == 'style-3') { ?>
       
        <div class="main-header"<?php echo $j_header_bg_custom_page_style; ?>>
        <div class="container clearfix">
         <?php if($jozoor_show_top_bar_header == 1) { ?>
         <a href="#" class="down-button"><i class="icon-angle-down"></i></a><!-- this appear on small devices -->
         <?php } ?>
         <div class="eleven columns">
          <div class="logo"> 
            <?php if($jozoor_logo_text_instead_image == 1 && !empty($jozoor_logo_name_text)) { ?>
              <h1 class="site-title-logo"><a href="<?php echo home_url(); ?>"><?php echo $jozoor_logo_name_text; ?></a></h1>  
            <?php } else { ?>
            <a href="<?php echo home_url(); ?>"> 
            <?php if(!empty($jozoor_logo_image)) { ?>
            <img src="<?php echo $jozoor_logo_image;?>" alt="<?php bloginfo('name'); ?>">
            <?php } else { ?>
            <img src="<?php echo get_template_directory_uri(); ?>/images/logo.png" alt="Invention - Responsive WordPress Theme" />
            <?php } ?>
            </a>
            <?php } ?>
          </div>
         </div><!-- End Logo -->
         
         <div class="five columns">
          
          <?php if($jozoor_header_layout3_right_content == 'custom content') { ?>
          <!-- Custom Content -->
          <div class="header-custom-content post-content">
            <?php echo do_shortcode($jozoor_header_layout3_right_custom_content); ?>  
          </div>
          <!-- End -->
          <?php } else { ?>
          <!-- Search Widget -->
          <div class="widget search">
           <form action="<?php echo home_url(); ?>" id="searchform" method="get">
           <input type="text" id="s" name="s" class="text-search" value="<?php _e("Search...", "jozoorthemes"); ?>" onBlur="if(this.value == '') { this.value = '<?php _e("Search...", "jozoorthemes"); ?>'; }" onfocus="if(this.value == '<?php _e("Search...", "jozoorthemes"); ?>') { this.value = ''; }" />
           <input type="submit" value="" id="searchsubmit" class="submit-search" />
           </form>
          </div>
          <!-- End -->
          <?php } ?>     
             
         </div>
       
        </div><!-- End Container -->
        </div><!-- End main-header --> 
       
        <div class="down-header">
         <div class="container clearfix">
             
           <div class="sixteen columns">
             <nav id="menu" class="navigation" role="navigation">
             <a href="#"><?php echo __("Show navigation", "jozoorthemes"); ?></a><!-- this appear on small devices -->
                 
             <?php  
             if($j_using_page_builder == 'on' && $j_page_builder_page_type == 'onepage') { 
              wp_nav_menu ( array( 'menu_id' => 'nav','container'=> 'ul', 'menu_class' => 'onepage-menu', 'menu' => $j_page_builder_page_one_menu ));
             } else {
               
              if ( has_nav_menu( 'headermenu' ) ) {
			  wp_nav_menu ( array( 'menu_id' => 'nav','container'=> 'ul', 'theme_location' => 'headermenu' ));
              } 
              else { ?>
			  <ul id="nav"> <?php wp_list_pages(array( 'title_li' => false, 'sort_column'  => 'menu_order')); ?> </ul> 
              <?php }
               
             } ?> 
            
            </nav>
          </div><!-- End Menu --> 
       
        </div><!-- End Container -->
        </div><!-- End down-header -->
           
        <?php } 
        // Header Style 4
        elseif($jozoor_header_layout == 'style-3 style-5') { ?>
       
         <div class="main-header"<?php echo $j_header_bg_custom_page_style; ?>>
          <div class="container clearfix">
           <?php if($jozoor_show_top_bar_header == 1) { ?>
            <a href="#" class="down-button"><i class="icon-angle-down"></i></a><!-- this appear on small devices -->
           <?php } ?>
              
           <div class="sixteen columns">
            <div class="logo">
            <?php if($jozoor_logo_text_instead_image == 1 && !empty($jozoor_logo_name_text)) { ?>
            <h1 class="site-title-logo"><a href="<?php echo home_url(); ?>"><?php echo $jozoor_logo_name_text; ?></a></h1>  
            <?php } else { ?>
            <a href="<?php echo home_url(); ?>"> 
            <?php if(!empty($jozoor_logo_image)) { ?>
            <img src="<?php echo $jozoor_logo_image;?>" alt="<?php bloginfo('name'); ?>">
            <?php } else { ?>
            <img src="<?php echo get_template_directory_uri(); ?>/images/logo.png" alt="Invention - Responsive WordPress Theme" />
            <?php } ?>
            </a>
            <?php } ?>
            </div>
           </div><!-- End Logo -->
       
          </div><!-- End Container -->
         </div><!-- End main-header -->
       
         <div class="down-header">
         <div class="container clearfix">
             
           <div class="sixteen columns">
             <nav id="menu" class="navigation" role="navigation">
             <a href="#"><?php echo __("Show navigation", "jozoorthemes"); ?></a><!-- this appear on small devices -->
                 
             <?php  
             if($j_using_page_builder == 'on' && $j_page_builder_page_type == 'onepage') { 
              wp_nav_menu ( array( 'menu_id' => 'nav','container'=> 'ul', 'menu_class' => 'onepage-menu', 'menu' => $j_page_builder_page_one_menu ));    
             } else {
               
              if ( has_nav_menu( 'headermenu' ) ) {
			  wp_nav_menu ( array( 'menu_id' => 'nav','container'=> 'ul', 'theme_location' => 'headermenu' ));
              } 
              else { ?>
			  <ul id="nav"> <?php wp_list_pages(array( 'title_li' => false, 'sort_column'  => 'menu_order')); ?> </ul> 
              <?php }
               
             } ?> 
               
            </nav>
          </div><!-- End Menu --> 
       
        </div><!-- End Container -->
        </div><!-- End down-header -->
           
        <?php } ?>
       
       
       <?php } // end theme option header layout ?>
       
     
   </header><!-- <<< End Header >>> -->
      
   <?php if($jozoor_add_custom_content_bottom_header == 1) { ?>
    <div class="bottom-header-custom-content post-content">
      <div class="container clearfix">
          <div class="sixteen columns"> <?php echo do_shortcode($jozoor_custom_content_bottom_header); ?> </div>
      </div><!-- End Container -->
    </div>  
   <?php } ?>
   
   <?php 
   if ( is_404() || !have_posts() || $post->post_type == 'sliders' ) { } else {
   // get slider
   jozoor_sliders_show(wc_get_page_id('shop'));  
   // get page title
   jozoor_page_title(wc_get_page_id('shop')); 
   }
   ?>