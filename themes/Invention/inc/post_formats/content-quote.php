<article id="post-<?php the_ID(); ?>" <?php post_class('style-1 bottom-2'); ?>>

  <div class="post-wrap">
      
      <?php // check post protected 
      if ( !post_password_required() ) { ?>
      
      <?php 
        $j_quote_content = get_post_meta( $post->ID, '_jozoor_quote_content', true ); // get quote
        $j_author_name = get_post_meta( $post->ID, '_jozoor_quote_author', true ); // get author
      ?>
      
      <?php  if ( !empty($j_quote_content) ) { ?>
      <div class="entry-quote top-0">
          
        <blockquote> 
        <p>
        <?php if( !is_single() ) { ?><a href="<?php echo get_permalink(); ?>"><?php } ?>
         <?php echo $j_quote_content; ?>   
        <?php if( !is_single() ) { ?></a><?php } ?>
        </p>
        <?php  if ( !empty($j_author_name) ) { ?>
        <span class="author">ــ <?php echo $j_author_name; ?> ــ</span> 
        <?php } ?> 
        </blockquote>
          
       </div><!-- End-->
       <?php } ?>
          
       <div class="post-meta<?php if( !empty( $post->post_content) ) { echo ' bottom-1'; } ?>">
           
            <div class="meta"><?php if( !is_single() ) { ?> <a href="<?php echo get_permalink() ; ?>"> <?php } ?>
            <i class="icon-time"></i> <?php the_time('d M, Y'); ?> 
            <?php if( !is_single() ) { ?></a><?php } ?>
            </div><!-- Date -->
            <div class="meta"><i class="icon-user"></i> <?php the_author_posts_link(); ?> </div><!-- Author -->
            <div class="meta"><i class="icon-list-alt"></i> <?php the_category(', '); ?> </div><!-- Category -->
            <?php if ( comments_open() || get_comments_number() ) : ?>
            <div class="meta"><i class="icon-comments"></i> <a href="<?php comments_link(); ?>">
            <?php comments_popup_link( __("Leave a comment", "jozoorthemes") , __("1 Comment", "jozoorthemes"), __("% Comments", "jozoorthemes") ); ?>    
            </a> </div><!-- Comments -->
            <?php endif; ?>
            <?php 
            global $smof_data; // get options
            if(isset($smof_data['blog_views_counter']) && $smof_data['blog_views_counter'] != 0) { ?>
            <div class="meta"><i class="icon-eye-open"></i> <?php echo jozoor_getPostViews(get_the_ID()); ?> <?php _e("Views", "jozoorthemes"); ?> </div><!-- Views -->
            <?php } ?>
           
        </div><!-- End post-meta -->
          
      <?php } ?> 
      
      <div class="entry-content post-content">
        <?php 
        global $smof_data; // get options
        if(isset($smof_data['blog_using_excerpt']) && $smof_data['blog_using_excerpt'] == 1 && !is_single()) {
        the_excerpt();     
        } else {
        the_content(__("Read More", "jozoorthemes")); 
        }
        ?>
      </div><!-- End post-content -->
      
  </div> 
    
</article><!-- End Post -->