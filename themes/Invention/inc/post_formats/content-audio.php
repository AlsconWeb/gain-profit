<article id="post-<?php the_ID(); ?>" <?php post_class('style-1 bottom-2'); ?>>

  <div class="post-wrap">
      
      <?php if( !is_single() ) { 
        the_title( '<h3 class="entry-title title bottom-1"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h3><!-- Title Post -->' );
      } ?>
      
      <?php // check post protected 
      if ( !post_password_required() ) { ?>
      
      <?php 
        $j_audio_mp3_url = get_post_meta( $post->ID, '_jozoor_audio_mp3_url', true ); // get mp3 file url
        $j_using_soundcloud = get_post_meta( $post->ID, '_jozoor_using_soundcloud', true ); // get using soundcloud
        $j_soundcloud_embed = get_post_meta( $post->ID, '_jozoor_soundcloud_embed', true ); // get soundcloud url
      ?>
      
      <div class="audio entry">
          
      <?php if ( $j_using_soundcloud == 'on' ) { 
            
        $j_htmlcode = wp_oembed_get($j_soundcloud_embed);
        echo $j_htmlcode;
          
      } else { 
          
        if ( has_post_thumbnail() ) {
        echo '<div class="image-post">';
          
        if( !is_single() ) {  echo '<a href="'. get_permalink() .'">'; }
    
        echo get_the_post_thumbnail($post->ID, 'full', array('title' => ''));
    
        if( !is_single() ) {  echo '</a>'; }
            
        echo '</div><!-- End image-post -->';
      } ?>
      
      <?php if( !empty($j_audio_mp3_url) ) { ?>
      <audio src="<?php echo $j_audio_mp3_url; ?>" width="100%"></audio>
      <?php } ?>
          
      <?php } ?>      
      
      </div><!-- End audio -->    
      
          
      <div class="post-meta<?php if( !empty( $post->post_content) ) { echo ' bottom-1'; } ?>">
           
            <div class="meta"><?php if( !is_single() ) { ?> <a href="<?php echo get_permalink() ; ?>"> <?php } ?>
            <i class="icon-time"></i> <?php the_time('d M, Y'); ?> 
            <?php if( !is_single() ) { ?></a><?php } ?>
            </div><!-- Date -->
            <div class="meta"><i class="icon-user"></i> <?php the_author_posts_link(); ?> </div><!-- Author -->
            <div class="meta"><i class="icon-list-alt"></i> <?php the_category(', '); ?> </div><!-- Category -->
            <?php if ( comments_open() || get_comments_number() ) : ?>
            <div class="meta"><i class="icon-comments"></i> <a href="<?php comments_link(); ?>">
            <?php comments_popup_link( __("Leave a comment", "jozoorthemes") , __("1 Comment", "jozoorthemes"), __("% Comments", "jozoorthemes") ); ?>    
            </a> </div><!-- Comments -->
            <?php endif; ?>
            <?php 
            global $smof_data; // get options
            if(isset($smof_data['blog_views_counter']) && $smof_data['blog_views_counter'] != 0) { ?>
            <div class="meta"><i class="icon-eye-open"></i> <?php echo jozoor_getPostViews(get_the_ID()); ?> <?php _e("Views", "jozoorthemes"); ?> </div><!-- Views -->
            <?php } ?>
           
      </div><!-- End post-meta -->
          
      <?php } ?>  
      
      <div class="entry-content post-content">
        <?php 
        global $smof_data; // get options
        if(isset($smof_data['blog_using_excerpt']) && $smof_data['blog_using_excerpt'] == 1 && !is_single()) {
        the_excerpt();     
        } else {
        the_content(__("Read More", "jozoorthemes")); 
        }
        ?>
      </div><!-- End post-content -->
      
  </div> 
    
</article><!-- End Post -->