<article id="post-<?php the_ID(); ?>" class="<?php $allClasses = get_post_class(); foreach ($allClasses as $class) { echo $class . ' '; } ?>one-third column item <?php
foreach((get_the_category()) as $category) {
echo jozoor_clean_string($category->name).' ';   
}
?>">

  <div class="contain">
      
      <?php // check post protected 
      if ( !post_password_required() ) { ?>
      
      <?php 
        $j_using_lightbox = get_post_meta( $post->ID, '_jozoor_using_lightbox', true ); // get using lightbox
      ?>
      
      <?php if ( has_post_thumbnail() ) {
        echo '<div class="gallery entry"> <div class="image-post item">';
          
        if( $j_using_lightbox == 'on' ) {
        $j_full_image_url = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full');
        echo '<a class="fancybox" href="'. $j_full_image_url[0]  .'" rel="'. get_the_title() .'">'; 
        } else { if( !is_single() ) {  echo '<a href="'. get_permalink() .'">'; } }
    
        echo get_the_post_thumbnail($post->ID, 'full', array('title' => ''));
        
        if( $j_using_lightbox == 'on' ) {
            echo '<i class="icon-search"></i></a>';
        } else { if( !is_single() ) {  echo '</a>'; } }
            
        echo '</div> </div><!-- End image-post -->';
      } ?>
      
      <?php } ?>  
      
      <div class="data">
      
      <?php if( !is_single() ) { 
        the_title( '<a href="' . esc_url( get_permalink() ) . '" class="title" rel="bookmark">', '</a><!-- Title Post -->' );
      } ?>
      
      <?php // check post protected 
      if ( !post_password_required() ) { ?> 
          
      <div class="post-meta">
        <div class="meta"><?php if( !is_single() ) { ?> <a href="<?php echo get_permalink() ; ?>"> <?php } ?>
        <i class="icon-time"></i> <?php the_time('d M, Y'); ?> 
        <?php if( !is_single() ) { ?></a><?php } ?>
        </div><!-- Date -->
        <div class="meta"><i class="icon-list-alt"></i> <?php the_category(', '); ?> </div><!-- Category -->
      </div><!-- End post-meta -->
      
      <?php } ?>
      
      <?php if( !empty( $post->post_content) ) { ?>
      <div class="entry-content post-content">
        <?php the_excerpt(); ?>
      </div><!-- End post-content -->
      <?php } ?>
          
    </div><!-- End data -->
      
  </div> 
    
</article><!-- End Post -->