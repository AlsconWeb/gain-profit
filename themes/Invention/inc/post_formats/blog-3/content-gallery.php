<article id="post-<?php the_ID(); ?>" class="<?php $allClasses = get_post_class(); foreach ($allClasses as $class) { echo $class . ' '; } ?>one-third column item <?php
foreach((get_the_category()) as $category) {
echo jozoor_clean_string($category->name).' ';   
}
?>">
    
  <script type="text/javascript">
  jQuery(document).ready(function($) {

  $(window).ready(function() {  
    $('.flexslider<?php the_ID(); ?>').flexslider({
	animation: 'slide', animationLoop: true, slideshow: true, slideshowSpeed: 4500, 
    animationSpeed: 700, pauseOnHover: true, pauseOnAction:true, controlNav: false, 
    directionNav: true, controlsContainer: '.flex-container.post-<?php the_ID(); ?>',
    });
    
  });
    
  });
  </script>

  <div class="contain">
      
      <?php // check post protected 
      if ( !post_password_required() ) { ?>
      
      <?php 
        $j_gallery_images = get_post_meta( $post->ID, '_jozoor_gallery_images', true ); // get gallery images
        $j_gallery_images_col3 = get_post_meta( $post->ID, '_jozoor_gallery_images_col3', true ); // get 3 columns
        $j_gallery_images_slider = get_post_meta( $post->ID, '_jozoor_gallery_images_slider', true ); // get slider
      ?>
      
      <?php if ( $j_gallery_images ) { ?>
      
      <div class="gallery slider image-post"> 
      
        <div class="flex-container">
         <div class="flexslider<?php the_ID(); ?>">
          <ul class="slides">
            <?php foreach ( $j_gallery_images as $attachment_id => $img_full_url ) {
               echo '<li class="item">';
               $j_full_image_url = wp_get_attachment_image_src( $attachment_id, 'full');
               echo '<a class="fancybox" href="'. $j_full_image_url[0]  .'" rel="'. get_the_title() .'">';
               echo wp_get_attachment_image( $attachment_id, 'latest-news-home' ); 
               echo '<i class="icon-search"></i></a>';
               echo '</li>';
            } ?> 
          </ul>
         </div>
        </div>
        
      </div><!-- End slider -->
      
      <?php 
      
      } // end check images ?>
      
      <?php } ?>  
      
      <div class="data">
      
      <?php if( !is_single() ) { 
        the_title( '<a href="' . esc_url( get_permalink() ) . '" class="title" rel="bookmark">', '</a><!-- Title Post -->' );
      } ?>
      
      <?php // check post protected 
      if ( !post_password_required() ) { ?>  
          
      <div class="post-meta">
            <div class="meta"><?php if( !is_single() ) { ?> <a href="<?php echo get_permalink() ; ?>"> <?php } ?>
            <i class="icon-time"></i> <?php the_time('d M, Y'); ?> 
            <?php if( !is_single() ) { ?></a><?php } ?>
            </div><!-- Date -->
            <div class="meta"><i class="icon-list-alt"></i> <?php the_category(', '); ?> </div><!-- Category -->
           
      </div><!-- End post-meta -->
          
      <?php } ?>
      
      <?php if( !empty( $post->post_content) ) { ?>
      <div class="entry-content post-content">
        <?php the_excerpt(); ?>
      </div><!-- End post-content -->
      <?php } ?>
          
    </div><!-- End data -->
      
  </div> 
    
</article><!-- End Post -->