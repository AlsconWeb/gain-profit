<article id="post-<?php the_ID(); ?>" class="<?php $allClasses = get_post_class(); foreach ($allClasses as $class) { echo $class . ' '; } ?>one-third column item <?php
foreach((get_the_category()) as $category) {
echo jozoor_clean_string($category->name).' ';   
}
?>">

  <div class="contain">
      
      <?php // check post protected 
      if ( !post_password_required() ) { ?>
      
      <?php 
        $j_link_url = get_post_meta( $post->ID, '_jozoor_link_url', true ); // get link url
      ?>
      
      <div class="entry-link">
        <p><a href="<?php echo $j_link_url; ?>" target="_blank"> <?php the_title(); ?> </a></p>    
      </div><!-- End-->
      
      <div class="data">
          
      <div class="post-meta">
        <div class="meta"><?php if( !is_single() ) { ?> <a href="<?php echo get_permalink() ; ?>"> <?php } ?>
        <i class="icon-time"></i> <?php the_time('d M, Y'); ?> 
        <?php if( !is_single() ) { ?></a><?php } ?>
        </div><!-- Date -->
        <div class="meta"><i class="icon-list-alt"></i> <?php the_category(', '); ?> </div><!-- Category -->
      </div><!-- End post-meta -->
          
      <?php } ?>  
      
      <?php if( !empty( $post->post_content) ) { ?>
      <div class="entry-content post-content">
        <?php the_excerpt(); ?>
      </div><!-- End post-content -->
      <?php } ?>
          
    </div><!-- End data -->
      
  </div> 
    
</article><!-- End Post -->