<article id="post-<?php the_ID(); ?>" class="<?php $allClasses = get_post_class(); foreach ($allClasses as $class) { echo $class . ' '; } ?>one-third column item <?php
foreach((get_the_category()) as $category) {
echo jozoor_clean_string($category->name).' ';   
}
?>">

  <div class="contain">
      
      <?php // check post protected 
      if ( !post_password_required() ) { ?>
      
      <?php 
        $j_audio_mp3_url = get_post_meta( $post->ID, '_jozoor_audio_mp3_url', true ); // get mp3 file url
        $j_using_soundcloud = get_post_meta( $post->ID, '_jozoor_using_soundcloud', true ); // get using soundcloud
        $j_soundcloud_embed = get_post_meta( $post->ID, '_jozoor_soundcloud_embed', true ); // get soundcloud url
      ?>
      
      
      <div class="audio entry">
          
      <?php if ( $j_using_soundcloud == 'on' ) { 
            
        $j_htmlcode = wp_oembed_get($j_soundcloud_embed);
        echo $j_htmlcode;
          
      } else { 
          
        if ( has_post_thumbnail() ) {
        echo '<div class="image-post">';
          
        if( !is_single() ) {  echo '<a href="'. get_permalink() .'">'; }
    
        echo get_the_post_thumbnail($post->ID, 'latest-news-home', array('title' => ''));
    
        if( !is_single() ) {  echo '</a>'; }
            
        echo '</div><!-- End image-post -->';
      } ?>
      
      <?php if( !empty($j_audio_mp3_url) ) { ?>
      <audio src="<?php echo $j_audio_mp3_url; ?>" width="100%"></audio>
      <?php } ?>
          
      <?php } ?>      
      
      </div><!-- End audio -->  
      
      <?php } ?>
          
      <div class="data">
          
      <?php if( !is_single() ) { 
        the_title( '<a href="' . esc_url( get_permalink() ) . '" class="title" rel="bookmark">', '</a><!-- Title Post -->' );
      } ?>
      
      <?php // check post protected 
      if ( !post_password_required() ) { ?>
          
      <div class="post-meta">
        <div class="meta"><?php if( !is_single() ) { ?> <a href="<?php echo get_permalink() ; ?>"> <?php } ?>
        <i class="icon-time"></i> <?php the_time('d M, Y'); ?> 
        <?php if( !is_single() ) { ?></a><?php } ?>
        </div><!-- Date -->
        <div class="meta"><i class="icon-list-alt"></i> <?php the_category(', '); ?> </div><!-- Category -->
      </div><!-- End post-meta -->
          
      <?php } ?>  
      
      <?php if( !empty( $post->post_content) ) { ?>
      <div class="entry-content post-content">
        <?php the_excerpt(); ?>
      </div><!-- End post-content -->
      <?php } ?>
          
   </div><!-- End data -->
      
  </div> 
    
</article><!-- End Post -->