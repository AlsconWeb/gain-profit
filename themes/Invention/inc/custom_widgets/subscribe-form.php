<?php

// Subscribe Form Widget

class jozoor_Subscribe_Form extends WP_Widget {

  function __construct() {
     /* Widget settings. */
     $widget_ops = array(
      'classname' => 'subscribe',
      'description' => __("Add you Subscribe Form ", "jozoorthemes"));

     /* Widget control settings. */
     $control_ops = array(
       'width' => 250,
       'height' => 350,
       'id_base' => 'subscribeform-widget');

    /* Create the widget. */
    parent::__construct('subscribeform-widget', __("[ Jozoor - Subscribe Form ]", "jozoorthemes"), $widget_ops, $control_ops );
  }
  
  
// Display Outputs form widget
function widget ($args,$instance) {
   extract($args);

  $title = apply_filters('widget_title', $instance['title']);
  if(isset($instance['description'])) { $description = $instance['description']; } // description
  if(isset($instance['form_code'])) { $form_code = $instance['form_code']; } // form_code
  

  // Get Outputs
  echo $before_widget;
  if(!empty($title)){ echo $before_title.$title.$after_title; }
  ?>
  
  <p><?php if(!empty($description)){echo esc_attr($description); } else { echo '';}?></p>
  <?php if(!empty($form_code)){echo $form_code; } else { echo '';}?>
  
  <?php
  echo $after_widget;
 }
 
 
// Update Widget
function update ($new_instance, $old_instance) {
  $instance = $old_instance;

  $instance['title'] = strip_tags( $new_instance['title']);
  $instance['description'] = strip_tags( $new_instance['description']);
  $instance['form_code'] = $new_instance['form_code'];
  
  return $instance;
}

// Form Fields in widget
function form ($instance) {
    /* Set up some default widget settings. */
    $defaults = array('title'=>'','description'=>'','form_code'=>'');
    $instance = wp_parse_args( (array) $instance, $defaults ); ?>

  <p>
    <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title :', 'jozoorthemes'); ?></label>
    <input class="widefat" type="text" name="<?php echo $this->get_field_name('title') ?>" id="<?php echo $this->get_field_id('title') ?> " value="<?php echo esc_attr($instance['title']) ?>">
  </p>
  
  <p>
    <label for="<?php echo $this->get_field_id('description'); ?>"><?php _e('Subscribe Description :', 'jozoorthemes'); ?></label>
     <textarea class="widefat" id="<?php echo $this->get_field_id('description'); ?>" name="<?php echo $this->get_field_name('description'); ?>" style="min-height: 100px;"><?php echo esc_attr($instance['description']) ?></textarea>
  </p>

  
  <p>
    <label for="<?php echo $this->get_field_id('form_code'); ?>"><?php _e('Subscribe Form Code :', 'jozoorthemes'); ?></label>
     <br /><small><?php _e("you can add your ", "jozoorthemes"); ?><a href="http://mailchimp.com" target="_blank"><?php _e("mailchimp", "jozoorthemes"); ?></a> <?php _e("subscribe form code or another mail list form, after get your form code you should edit some html codes to be stylish with theme standard style for theme", "jozoorthemes"); ?> 
     <a href="http://snippi.com/s/62m3cig" target="_blank"><?php _e("Get Standard Code Form Here", "jozoorthemes"); ?></a> .</small>
     <textarea class="widefat" id="<?php echo $this->get_field_id('form_code'); ?>" name="<?php echo $this->get_field_name('form_code'); ?>" style="min-height: 250px; margin-top: 5px;"><?php echo esc_attr($instance['form_code']) ?></textarea>
  </p>


  <?php
}



}

function jozoor_subscribe_load_widgets() {
  register_widget('jozoor_Subscribe_Form');
}

add_action('widgets_init', 'jozoor_subscribe_load_widgets');

?>
