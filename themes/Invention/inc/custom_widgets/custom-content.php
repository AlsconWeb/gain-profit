<?php

// Custom Content Widget

class jozoor_Custom_Content extends WP_Widget {

  function __construct() {
     /* Widget settings. */
     $widget_ops = array(
      'classname' => 'customcontent',
      'description' => __("You can add any custom content ", "jozoorthemes"));

     /* Widget control settings. */
     $control_ops = array(
       'width' => 250,
       'height' => 350,
       'id_base' => 'customcontent-widget');

    /* Create the widget. */
    parent::__construct('customcontent-widget', __("[ Jozoor - Custom Content ]", "jozoorthemes"), $widget_ops, $control_ops );
  }
  
  
// Display Outputs form widget
function widget ($args,$instance) {
   extract($args);
  
  $title = apply_filters('widget_title', $instance['title']);
  if(isset($instance['content'])) { $content = $instance['content']; } // content
  
  if(!empty($content)) {
  // Get Outputs
  echo $before_widget;
  if(!empty($title)){ echo $before_title.$title.$after_title; }
  ?>
  
  <?php echo '<div class="post-content"><div class="arrow-list" >'.$content .'</div></div>'; ?>
 
  <?php
  echo $after_widget;
  
  }
  
 }
 
 
 
// Update Widget
function update ($new_instance, $old_instance) {
  $instance = $old_instance;
  
  $instance['title'] = strip_tags( $new_instance['title']);
  $instance['content'] = $new_instance['content'];


  return $instance;
}

// Form Fields in widget
function form ($instance) {
    /* Set up some default widget settings. */
    $defaults = array('title'=>'','content'=>'');
    $instance = wp_parse_args( (array) $instance, $defaults ); ?>
    
   <p>
    <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title :', 'jozoorthemes'); ?></label>
    <input class="widefat" type="text" name="<?php echo $this->get_field_name('title') ?>" id="<?php echo $this->get_field_id('title') ?> " value="<?php echo esc_attr($instance['title']) ?>">
  </p>

  <p>
    <label for="<?php echo $this->get_field_id('content'); ?>"><?php _e('Custom Content :', 'jozoorthemes'); ?></label>
     <br /><small><?php _e("You can add any custom content like, networks share code - google adsense - html content - shortcodes and what you want.", "jozoorthemes"); ?></small>
     <textarea class="widefat" id="<?php echo $this->get_field_id('content'); ?>" name="<?php echo $this->get_field_name('content'); ?>" style="min-height: 250px; margin-top: 5px;"><?php echo esc_attr($instance['content']) ?></textarea>
  </p>


  <?php
}



}

function jozoor_customcontent_load_widgets() {
  register_widget('jozoor_Custom_Content');
}

add_action('widgets_init', 'jozoor_customcontent_load_widgets');

?>
