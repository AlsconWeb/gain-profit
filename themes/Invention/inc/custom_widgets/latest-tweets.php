<?php

// Latest Tweets Widget

class jozoor_Latest_Tweets extends WP_Widget {

  function __construct() {
     /* Widget settings. */
     $widget_ops = array(
      'classname' => 'latesttweets',
      'description' => __("Get your Latest Tweets ", "jozoorthemes"));

     /* Widget control settings. */
     $control_ops = array(
       'width' => 250,
       'height' => 350,
       'id_base' => 'latesttweets-widget');

    /* Create the widget. */
    parent::__construct('latesttweets-widget', __("[ Jozoor - Latest Tweets ]", "jozoorthemes"), $widget_ops, $control_ops );
  }
  
  
// Display Outputs form widget
function widget ($args,$instance) {
   extract($args);

  $title = apply_filters('widget_title', $instance['title']);
  $username = $instance['username']; // username
  $num_tweets = $instance['num_tweets']; // number of tweets
  $position_id = $instance['position_id']; // position id
  
  if(isset($instance['consumer_key'])) { $consumer_key = $instance['consumer_key']; } // consumer_key
  if(isset($instance['consumer_secret'])) { $consumer_secret = $instance['consumer_secret']; } // consumer_secret
  if(isset($instance['access_token'])) { $access_token = $instance['access_token']; } // access_token
  if(isset($instance['access_token_secret'])) { $access_token_secret = $instance['access_token_secret']; } // access_token_secret
  if(isset($instance['cache_time_updates'])) { $cache_time_updates = $instance['cache_time_updates']; } // Time in minutes between updates 
  
  if(empty($cache_time_updates)) { $cache_time_updates = 5; } else { $cache_time_updates = $cache_time_updates; }

  // Get Outputs
  echo $before_widget;
  if(!empty($title)){ echo $before_title.$title.$after_title; }
  ?>
  
  <?php
  
  $numTweets      = $num_tweets;                // Number of tweets to display.
  $name           = $username;  // Username to display tweets from.
  $excludeReplies = true;             // Leave out @replies
  $transName      = $position_id.'list-tweets';    // Name of value in database.
  $cacheTime      = $cache_time_updates;                // Time in minutes between updates default (5).

$backupName = $transName . '-backup';

// Do we already have saved tweet data? If not, lets get it.
if(false === ($tweets = get_transient($transName) ) ) :	

  // Get the tweets from Twitter.
  include (get_template_directory() . '/inc/twitteroauth/twitteroauth.php'); 
  
  if(isset($consumer_key) && isset($consumer_secret) && isset($access_token) && isset($access_token_secret)) { }
  
  $connection = new TwitterOAuth(
    ''.@$consumer_key.'',   // Consumer key
    ''.@$consumer_secret.'',   // Consumer secret
    ''.@$access_token.'',   // Access token
    ''.@$access_token_secret.''    // Access token secret
  );
  

  
  // If excluding replies, we need to fetch more than requested as the
  // total is fetched first, and then replies removed.
  $totalToFetch = ($excludeReplies) ? max(50, $numTweets * 3) : $numTweets;
  
  $fetchedTweets = $connection->get(
    'statuses/user_timeline',
    array(
      'screen_name'     => $name,
      'count'           => $totalToFetch,
      'exclude_replies' => $excludeReplies
    )
  );
  
  // Did the fetch fail?
  if($connection->http_code != 200) :
    $tweets = get_option($backupName); // False if there has never been data saved.
    
  else :
    // Fetch succeeded.
    // Now update the array to store just what we need.
    // (Done here instead of PHP doing this for every page load)
    $limitToDisplay = min($numTweets, count($fetchedTweets));
    
    for($i = 0; $i < $limitToDisplay; $i++) :
      $tweet = $fetchedTweets[$i];
    
      // Core info.
      $name = $tweet->user->screen_name; //name
      $permalink = 'http://twitter.com/'. $name .'/status/'. $tweet->id_str;

      /* Alternative image sizes method: http://dev.twitter.com/doc/get/users/profile_image/:screen_name */
      $image = $tweet->user->profile_image_url;

      // Message. Convert links to real links.
      $pattern = '/http:(\S)+/';
      $replace = '<a href="${0}" target="_blank" rel="nofollow">${0}</a>';
      $text_1 = preg_replace($pattern, $replace, $tweet->text);
      // new code for @mentions links
      $text_2 = preg_replace('/@([a-z0-9_]+)/i','<a href="https://twitter.com/$1" target="_blank" rel="nofollow">@$1</a>',$text_1);
      // new code for #hashtags links
      $text = preg_replace('/#([a-z0-9_]+)/i','<a href="https://twitter.com/search?q=%23$1&src=typd" target="_blank" rel="nofollow">#$1</a>',$text_2);

      // Need to get time in Unix format.
      $time = $tweet->created_at;
      $time = date_parse($time);
      $uTime = mktime($time['hour'], $time['minute'], $time['second'], $time['month'], $time['day'], $time['year']);

      // Now make the new array.
      $tweets[] = array(
              'text' => $text,
              'name' => $name,
              'permalink' => $permalink,
              'image' => $image,
              'time' => $uTime
              );
    endfor;

    // Save our new transient, and update the backup.
    set_transient($transName, $tweets, 1 * $cacheTime); // default ( 60 )
    update_option($backupName, $tweets);
  endif;
endif; 

// Now display the tweets.
?>
  
  <?php if($position_id == 'sidebar') { $footer_class = '';} elseif($position_id == 'footer') { $footer_class = 'footer';} ?>
  
  <div id="tweets-<?php echo $position_id; ?>" class='tweet query <?php echo $footer_class; ?> '>
  
  <ul class="tweet_list">
  <?php foreach($tweets as $t) : ?>
    <li>
     <i class="icon-twitter"></i>
     <span class="tweet_time"> 
     <a href="<?php echo $t['permalink']; ?>" target="_blank">
     <?php echo human_time_diff($t['time'], current_time('timestamp')); ?> ago
     </a>
     </span>
     <span class="tweet_text"><?php echo $t['text']; ?></span>
    </li>
  <?php endforeach; ?>
  </ul>
  
  </div><!-- Tweets Code -->
  
  <?php
  echo $after_widget;
 }
 
 
// Update Widget
function update ($new_instance, $old_instance) {
  $instance = $old_instance;

  $instance['title'] = strip_tags( $new_instance['title']);
  $instance['username'] = strip_tags( $new_instance['username']);
  $instance['num_tweets'] = strip_tags( $new_instance['num_tweets']);
  $instance['position_id'] = strip_tags( $new_instance['position_id']);
  
  $instance['consumer_key'] = strip_tags( $new_instance['consumer_key']);
  $instance['consumer_secret'] = strip_tags( $new_instance['consumer_secret']);
  $instance['access_token'] = strip_tags( $new_instance['access_token']);
  $instance['access_token_secret'] = strip_tags( $new_instance['access_token_secret']);
  $instance['cache_time_updates'] = strip_tags( $new_instance['cache_time_updates']);

  return $instance;
}

// Form Fields in widget
function form ($instance) {
    /* Set up some default widget settings. */
    $defaults = array('title'=>'','username'=>'','num_tweets'=>'2','position_id'=>'sidebar','consumer_key'=>'',
    'consumer_secret'=>'','access_token'=>'','access_token_secret'=>'','cache_time_updates'=>'5');
    $instance = wp_parse_args( (array) $instance, $defaults ); ?>

  <p>
    <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title :', 'jozoorthemes'); ?></label>
    <input class="widefat" type="text" name="<?php echo $this->get_field_name('title') ?>" id="<?php echo $this->get_field_id('title') ?> " value="<?php echo $instance['title'] ?>">
  </p>
  
  <p>
    <label for="<?php echo $this->get_field_id('username'); ?>"><?php _e('Your Twitter Username :', 'jozoorthemes'); ?></label>
    <input class="widefat" type="text" name="<?php echo $this->get_field_name('username') ?>" id="<?php echo $this->get_field_id('username') ?> " value="<?php echo $instance['username'] ?>">
  </p>

  
  <p>
   <label for="<?php echo $this->get_field_id('num_tweets'); ?>"><?php _e('Number of Tweets :', 'jozoorthemes'); ?></label>
   <input type="text" name="<?php echo $this->get_field_name('num_tweets') ?>" id="<?php echo $this->get_field_id('num_tweets') ?> " value="<?php echo $instance['num_tweets'] ?>" size="10">
  </p>
  
  <p>
   <label for="<?php echo $this->get_field_id('position_id'); ?>"><?php _e('Css Position id :', 'jozoorthemes'); ?></label>
   <select class="widefat" id="<?php echo $this->get_field_id('position_id'); ?>" name="<?php echo $this->get_field_name('position_id'); ?>">
   <?php 
         if($instance['position_id'] == 'sidebar'){
            $sidebar = 'selected="selected"';
            $footer= '';
         }
         elseif($instance['position_id'] == 'footer'){
            $footer = 'selected="selected"';
            $sidebar= '';
         }
         echo '
         <option value="sidebar" '.$sidebar.'>'.__("sidebar", "jozoorthemes").'</option>
         <option value="footer" '.$footer.'>'.__("footer", "jozoorthemes").'</option>
         ';
         
         ?>
       </select>
  </p>
  
  <p>
    <label for="twitter-app-info"><?php _e('Create An Application On Twitter :', 'jozoorthemes'); ?></label>
    <br />
    <small>
    <?php _e("you need to create application to can show latest tweets , which you can create new APP from here :", "jozoorthemes"); ?>
    <a href="https://dev.twitter.com/apps/new" target="_blank">https://dev.twitter.com/apps/new</a>, 
    <?php _e("after create APP you will found your data which you can use it in fileds below, also you can see this video to know how to create your app :", "jozoorthemes"); ?>
    <a href="http://www.youtube.com/watch?v=noB3P-K-wb4" target="_blank">http://www.youtube.com/watch?v=noB3P-K-wb4</a>
    </small>
  </p>
  
  <p>
    <label for="<?php echo $this->get_field_id('consumer_key'); ?>"><?php _e('Consumer Key :', 'jozoorthemes'); ?></label>
    <input class="widefat" type="text" name="<?php echo $this->get_field_name('consumer_key') ?>" id="<?php echo $this->get_field_id('consumer_key') ?> " value="<?php echo $instance['consumer_key'] ?>">
  </p>
  
  <p>
    <label for="<?php echo $this->get_field_id('consumer_secret'); ?>"><?php _e('Consumer Secret :', 'jozoorthemes'); ?></label>
    <input class="widefat" type="text" name="<?php echo $this->get_field_name('consumer_secret') ?>" id="<?php echo $this->get_field_id('consumer_secret') ?> " value="<?php echo $instance['consumer_secret'] ?>">
  </p>
  
  <p>
    <label for="<?php echo $this->get_field_id('access_token'); ?>"><?php _e('Access Token :', 'jozoorthemes'); ?></label>
    <input class="widefat" type="text" name="<?php echo $this->get_field_name('access_token') ?>" id="<?php echo $this->get_field_id('access_token') ?> " value="<?php echo $instance['access_token'] ?>">
  </p>
  
  <p>
    <label for="<?php echo $this->get_field_id('access_token_secret'); ?>"><?php _e('Access Token Secret :', 'jozoorthemes'); ?></label>
    <input class="widefat" type="text" name="<?php echo $this->get_field_name('access_token_secret') ?>" id="<?php echo $this->get_field_id('access_token_secret') ?> " value="<?php echo $instance['access_token_secret'] ?>">
  </p>
  
  <p>
    <label for="<?php echo $this->get_field_id('cache_time_updates'); ?>"><?php _e('Cache Time Between Updates :', 'jozoorthemes'); ?></label>
    <input class="widefat" type="text" name="<?php echo $this->get_field_name('cache_time_updates') ?>" id="<?php echo $this->get_field_id('cache_time_updates') ?> " value="<?php echo $instance['cache_time_updates'] ?>">
  </p>


  <?php
}



}

function jozoor_tweets_load_widgets() {
  register_widget('jozoor_Latest_Tweets');
}

add_action('widgets_init', 'jozoor_tweets_load_widgets');

?>
