<?php

// Recent Projects Widget

class jozoor_Recent_projects extends WP_Widget {

  function __construct() {
     /* Widget settings. */
     $widget_ops = array(
      'classname' => 'flickrstream recent-projects',
      'description' => __("Get your recent projects ", "jozoorthemes"));

     /* Widget control settings. */
     $control_ops = array(
       'width' => 250,
       'height' => 350,
       'id_base' => 'recent-projects-widget');

    /* Create the widget. */
    parent::__construct('recent-projects-widget', __("[ Jozoor - Recent Projects ]", "jozoorthemes"), $widget_ops, $control_ops );
  }
  
  
// Display Outputs form widget
function widget ($args,$instance) {
   extract($args);

  $title = apply_filters('widget_title', $instance['title']);
  if(isset($instance['num_projects'])) { $num_projects = $instance['num_projects']; } // number of images
  $position_id = $instance['position_id']; // position id
  

  // Get Outputs
  echo $before_widget;
  if(!empty($title)){ echo $before_title.$title.$after_title; }
  ?>
  
  <ul id="<?php echo esc_attr($position_id); ?>" class="thumbs">
  <?php
  query_posts('post_type=portfolio&posts_per_page='.$num_projects.'orderby=date&order=DESC');
  while (have_posts()) : the_post();
  global $post;
  ?>
  <li><a href="<?php echo esc_url(get_permalink()); ?>" title="<?php the_title(); ?>">
  <?php echo get_the_post_thumbnail($post->ID, array(126,126), array('title' => '')); ?>
  </a></li>
  <?php
  endwhile;
  wp_reset_query();
  ?> 
  </ul>
  
  <?php
  echo $after_widget;
 }
 
 
// Update Widget
function update ($new_instance, $old_instance) {
  $instance = $old_instance;

  $instance['title'] = strip_tags( $new_instance['title']);
  $instance['num_projects'] = strip_tags( $new_instance['num_projects']);
  $instance['position_id'] = strip_tags( $new_instance['position_id']);

  return $instance;
}

// Form Fields in widget
function form ($instance) {
    /* Set up some default widget settings. */
    $defaults = array('title'=>'','num_projects'=>'6','position_id'=>'sidebar');
    $instance = wp_parse_args( (array) $instance, $defaults ); ?>

  <p>
    <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title :', 'jozoorthemes'); ?></label>
    <input class="widefat" type="text" name="<?php echo $this->get_field_name('title') ?>" id="<?php echo $this->get_field_id('title') ?> " value="<?php echo esc_attr($instance['title']) ?>">
  </p>
  
  <p>
   <label for="<?php echo $this->get_field_id('num_projects'); ?>"><?php _e('Number of Projects :', 'jozoorthemes'); ?></label>
   <input type="text" name="<?php echo $this->get_field_name('num_projects') ?>" id="<?php echo $this->get_field_id('num_projects') ?> " value="<?php echo esc_attr($instance['num_projects']) ?>" size="10">
  </p>
  
  <p>
   <label for="<?php echo $this->get_field_id('position_id'); ?>"><?php _e('Css Position id :', 'jozoorthemes'); ?></label>
   <select class="widefat" id="<?php echo $this->get_field_id('position_id'); ?>" name="<?php echo $this->get_field_name('position_id'); ?>">
   <?php 
         if($instance['position_id'] == 'sidebar'){
            $sidebar = 'selected="selected"';
            $footer= '';
         }
         elseif($instance['position_id'] == 'footer'){
            $footer = 'selected="selected"';
            $sidebar= '';
         }
         echo '
         <option value="sidebar" '.$sidebar.'>sidebar</option>
         <option value="footer" '.$footer.'>footer</option>
         ';
         
         ?>
       </select>
  </p>


  <?php
}

}

function jozoor_recentprojects_load_widgets() {
  register_widget('jozoor_Recent_projects');
}

add_action('widgets_init', 'jozoor_recentprojects_load_widgets');

?>
