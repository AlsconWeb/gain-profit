<?php

// Flickr Stream Widget

class jozoor_Flickr_Stream extends WP_Widget {

  function __construct() {
     /* Widget settings. */
     $widget_ops = array(
      'classname' => 'flickrstream',
      'description' => __("Get your Flickr Stream ", "jozoorthemes"));

     /* Widget control settings. */
     $control_ops = array(
       'width' => 250,
       'height' => 350,
       'id_base' => 'flickrstream-widget');

    /* Create the widget. */
    parent::__construct('flickrstream-widget', __("[ Jozoor - Flickr Stream ]", "jozoorthemes"), $widget_ops, $control_ops );
  }
  
  
// Display Outputs form widget
function widget ($args,$instance) {
   extract($args);

  $title = apply_filters('widget_title', $instance['title']);
  if(isset($instance['flickrid'])) { $flickrid = $instance['flickrid']; } // flickr id
  if(isset($instance['num_images'])) { $num_images = $instance['num_images']; } // number of images
  $position_id = $instance['position_id']; // position id
  

  // Get Outputs
  echo $before_widget;
  if(!empty($title)){ echo $before_title.$title.$after_title; }
  ?>
  
  <script>
  
  // flickr options
   jQuery(document).ready(function($) {

	$('#<?php echo $position_id; ?>.flickr').jflickrfeed({
		limit: <?php if(!empty($num_images)){echo $num_images; } else { echo '6';} ?>,
		qstrings: {
			id: '<?php if(!empty($flickrid)){echo $flickrid; } else { echo '87696564@N03';} ?>' // Flickr Id form feed Rss in your photostream in flickr profile
		},
		itemTemplate: '<li><a href="{{link}}" title="{{title}}" target="_blank"><img src="{{image_m}}" alt="{{title}}" /></a></li>'
	});

   });
  
  </script>
  
  <ul id="<?php echo esc_attr($position_id); ?>" class="flickr thumbs"></ul> <!-- Flickr Code -->
  
  <?php
  echo $after_widget;
 }
 
 
// Update Widget
function update ($new_instance, $old_instance) {
  $instance = $old_instance;

  $instance['title'] = strip_tags( $new_instance['title']);
  $instance['flickrid'] = strip_tags( $new_instance['flickrid']);
  $instance['num_images'] = strip_tags( $new_instance['num_images']);
  $instance['position_id'] = strip_tags( $new_instance['position_id']);

  return $instance;
}

// Form Fields in widget
function form ($instance) {
    /* Set up some default widget settings. */
    $defaults = array('title'=>'','flickrid'=>'','num_images'=>'6','position_id'=>'sidebar');
    $instance = wp_parse_args( (array) $instance, $defaults ); ?>

  <p>
    <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title :', 'jozoorthemes'); ?></label>
    <input class="widefat" type="text" name="<?php echo $this->get_field_name('title') ?>" id="<?php echo $this->get_field_id('title') ?> " value="<?php echo esc_attr($instance['title']) ?>">
  </p>
  
  <p>
    <label for="<?php echo $this->get_field_id('flickrid'); ?>"><?php _e('Your Flickr User ID :', 'jozoorthemes'); ?></label>
    <input class="widefat" type="text" name="<?php echo $this->get_field_name('flickrid') ?>" id="<?php echo $this->get_field_id('flickrid') ?> " value="<?php echo esc_attr($instance['flickrid']) ?>">
    <small><?php _e("Don't know your ID? Go to ", "jozoorthemes"); ?><a href="http://idgettr.com" target="_blank"><?php _e("idgettr", "jozoorthemes"); ?></a> <?php _e("to find it.", "jozoorthemes"); ?></small>
  </p>

  
  <p>
   <label for="<?php echo $this->get_field_id('num_images'); ?>"><?php _e('Number of Images :', 'jozoorthemes'); ?></label>
   <input type="text" name="<?php echo $this->get_field_name('num_images') ?>" id="<?php echo $this->get_field_id('num_images') ?> " value="<?php echo esc_attr($instance['num_images']) ?>" size="10">
  </p>
  
  <p>
   <label for="<?php echo $this->get_field_id('position_id'); ?>"><?php _e('Css Position id :', 'jozoorthemes'); ?></label>
   <select class="widefat" id="<?php echo $this->get_field_id('position_id'); ?>" name="<?php echo $this->get_field_name('position_id'); ?>">
   <?php 
         if($instance['position_id'] == 'sidebar'){
            $sidebar = 'selected="selected"';
            $footer= '';
         }
         elseif($instance['position_id'] == 'footer'){
            $footer = 'selected="selected"';
            $sidebar= '';
         }
         echo '
         <option value="sidebar" '.$sidebar.'>sidebar</option>
         <option value="footer" '.$footer.'>footer</option>
         ';
         
         ?>
       </select>
  </p>


  <?php
}



}

function jozoor_load_widgets() {
  register_widget('jozoor_Flickr_Stream');
}

add_action('widgets_init', 'jozoor_load_widgets');

?>
