<?php

// Tabs Widget

class jozoor_Blog_Tabs extends WP_Widget {

  function __construct() {
     /* Widget settings. */
     $widget_ops = array(
      'classname' => 'most-posts',
      'description' => __("show tabs ( popular, recent, comments ). ", "jozoorthemes"));

     /* Widget control settings. */
     $control_ops = array(
       'width' => 250,
       'height' => 350,
       'id_base' => 'most-posts-widget');

    /* Create the widget. */
    parent::__construct('most-posts-widget', __("[ Jozoor - Tabs ]", "jozoorthemes"), $widget_ops, $control_ops );
  }
  
  
// Display Outputs form widget
function widget ($args,$instance) {
   extract($args);

  if(isset($instance['popular_tab_name'])) {$popular_tab_name = $instance['popular_tab_name'];} // popular_tab_name
  if(isset($instance['popular_num'])) {$popular_num = $instance['popular_num'];} // popular_num
  
  if(isset($instance['recent_tab_name'])) {$recent_tab_name = $instance['recent_tab_name'];} // recent_tab_name
  if(isset($instance['recent_num'])) {$recent_num = $instance['recent_num'];} // recent_num
  $recent_show = $instance['recent_show']; // recent_show
  
  if(isset($instance['comments_tab_name'])) {$comments_tab_name = $instance['comments_tab_name'];} // comments_tab_name
  if(isset($instance['comments_num'])) {$comments_num = $instance['comments_num'];} // comments_num
  $comments_show = $instance['comments_show']; // comments_show
  

  // Get Outputs
  echo $before_widget;
  ?>
  
  
  <div id="horizontal-tabs" class="two style2">
      
        <ul class="tabs">
          <li id="tab_two1"><?php if(!empty($popular_tab_name)){echo esc_attr($popular_tab_name); } else { echo __("Popular", "jozoorthemes");}?></li>
          <?php if($recent_show == 'yes2'){ ?>
          <li id="tab_two2"><?php if(!empty($recent_tab_name)){echo esc_attr($recent_tab_name); } else { echo __("Recent", "jozoorthemes");}?></li>
          <?php } ?>
          <?php if($comments_show == 'yes3'){ ?>
          <li id="tab_two3"><?php if(!empty($comments_tab_name)){echo esc_attr($comments_tab_name); } else { echo __("Comments", "jozoorthemes");}?></li>
          <?php } ?>
        </ul>
        <div class="contents">
        
        <div id="content_two1" class="tabscontent">
          <ul class="posts">
          <?php
           query_posts('post_type=post&posts_per_page='.$popular_num.'&meta_key=post_views_count&orderby=meta_value_num&order=DESC');
           while (have_posts()) : the_post();
           global $post;
      
	      ?>
            <li>
              <?php if (has_post_thumbnail( $post->ID ) ) {?>
              <a href="<?php echo esc_url(get_permalink()); ?>"><?php echo get_the_post_thumbnail($post->ID, array(68,68), array('title' => '')); ?></a>
              <?php } ?>
              <p><a href="<?php echo esc_url(get_permalink()); ?>"><?php the_title(); ?></a></p>
              <span class="color"><?php echo jozoor_getPostViews(get_the_ID()); ?> <?php _e("views", "jozoorthemes"); ?></span>
            </li>
            
           <?php
           endwhile;
           wp_reset_query();
           ?> 
            
          </ul>
        </div> 
        
        <?php if($recent_show == 'yes2'){ ?>
        <div id="content_two2" class="tabscontent">
         <ul class="posts">
          <?php
           query_posts('post_type=post&posts_per_page='.$recent_num.'orderby=date&order=DESC');
           while (have_posts()) : the_post();

           global $post;
          
	      ?>
            <li>
            <?php if (has_post_thumbnail( $post->ID ) ) {?>
              <a href="<?php echo esc_url(get_permalink()); ?>"><?php echo get_the_post_thumbnail($post->ID, array(68,68), array('title' => '')); ?></a>
              <?php } ?>
              <p><a href="<?php echo esc_url(get_permalink()); ?>"><?php the_title(); ?></a></p>
              <span class="color"><?php the_time('d M, Y'); ?></span>
            </li>
            
           <?php
           endwhile;
           wp_reset_query();
           ?> 
           </ul>
          </div>  
          <?php } ?>
          
          <?php if($comments_show == 'yes3'){ ?>
           <div id="content_two3" class="tabscontent">
         <ul class="posts">
         
          <?php
  global $wpdb;
  $sql = "SELECT DISTINCT ID, post_title, post_password, comment_ID, comment_post_ID, comment_author, comment_author_email, comment_date_gmt, comment_approved, comment_type,comment_author_url,  SUBSTRING(comment_content,1,30) AS com_excerpt FROM $wpdb->comments LEFT OUTER JOIN $wpdb->posts ON ($wpdb->comments.comment_post_ID = $wpdb->posts.ID) WHERE comment_approved = '1' AND comment_type = '' AND post_password = '' ORDER BY comment_date_gmt DESC LIMIT $comments_num";

  $comments = $wpdb->get_results($sql);
  $output = isset($pre_HTML);
  foreach ($comments as $comment) {
    $output .= '<li><a href="' . htmlspecialchars( get_comment_link( $comment->comment_ID ) ) . '" title="'.__("on", "jozoorthemes").' '.$comment->post_title . '">'.get_avatar( $comment->comment_author_email, $size='68',$default='' ).'</a>
    <p><a href="' . htmlspecialchars( get_comment_link( $comment->comment_ID ) ) . '" title="'.__("on", "jozoorthemes").' '.$comment->post_title . '">'.strip_tags($comment->com_excerpt).'...</a></p>
    '.__("by :", "jozoorthemes").' <span class="color">'.strip_tags($comment->comment_author) .'</span></li>';
  }
  $output .= isset($pre_HTML);
  echo $output;
?>
          
           </ul>
          </div>  
          <?php } ?>
          
        
        </div><!-- End contents -->
        
      </div><!-- End horizontal-tabs -->
      
      
  <?php
  echo $after_widget;
 }
 
 
// Update Widget
function update ($new_instance, $old_instance) {
  $instance = $old_instance;

  $instance['popular_tab_name'] = strip_tags( $new_instance['popular_tab_name']);
  $instance['popular_num'] = strip_tags( $new_instance['popular_num']);
  
  $instance['recent_tab_name'] = strip_tags( $new_instance['recent_tab_name']);
  $instance['recent_num'] = strip_tags( $new_instance['recent_num']);
  $instance['recent_show'] = strip_tags( $new_instance['recent_show']);
  
  $instance['comments_tab_name'] = strip_tags( $new_instance['comments_tab_name']);
  $instance['comments_num'] = strip_tags( $new_instance['comments_num']);
  $instance['comments_show'] = strip_tags( $new_instance['comments_show']);

  return $instance;
}

// Form Fields in widget
function form ($instance) {
    /* Set up some default widget settings. */
    $defaults = array('popular_tab_name'=>'Popular','popular_num'=>'4',
                      'recent_tab_name'=>'Recent','recent_num'=>'4','recent_show'=>'yes',
                      'comments_tab_name'=>'Comments','comments_num'=>'4','comments_show'=>'yes'
                     );
    $instance = wp_parse_args( (array) $instance, $defaults ); ?>
    
    <!-- =========== start poular tab ================ -->
  
  <p>
    <label for="<?php echo $this->get_field_id('popular_tab_name'); ?>"><?php _e('Popular Tab Name :', 'jozoorthemes'); ?></label>
    <input class="widefat" type="text" name="<?php echo $this->get_field_name('popular_tab_name') ?>" id="<?php echo $this->get_field_id('popular_tab_name') ?> " value="<?php echo esc_attr($instance['popular_tab_name']) ?>">
  </p>

  
  <p>
   <label for="<?php echo $this->get_field_id('popular_num'); ?>"><?php _e('Number of Posts :', 'jozoorthemes'); ?></label>
   <input type="text" name="<?php echo $this->get_field_name('popular_num') ?>" id="<?php echo $this->get_field_id('popular_num') ?> " value="<?php echo esc_attr($instance['popular_num']) ?>" size="10">
  </p>
  
  <!-- =========== End ================ -->
  <p><hr style="margin: 16px 0 3px 0; border: 1px solid #ccc;"/></p>
  <!-- =========== start recent tab ================ -->
  
  <p>
    <label for="<?php echo $this->get_field_id('recent_tab_name'); ?>"><?php _e('Recent Tab Name :', 'jozoorthemes'); ?></label>
    <input class="widefat" type="text" name="<?php echo $this->get_field_name('recent_tab_name') ?>" id="<?php echo $this->get_field_id('recent_tab_name') ?> " value="<?php echo esc_attr($instance['recent_tab_name']) ?>">
  </p>

  
  <p>
   <label for="<?php echo $this->get_field_id('recent_num'); ?>"><?php _e('Number of Posts :', 'jozoorthemes'); ?></label>
   <input type="text" name="<?php echo $this->get_field_name('recent_num') ?>" id="<?php echo $this->get_field_id('recent_num') ?> " value="<?php echo esc_attr($instance['recent_num']) ?>" size="10">
  </p>
  
  <p>
   <label for="<?php echo $this->get_field_id('recent_show'); ?>"><?php _e('Show Recent Tab :', 'jozoorthemes'); ?></label>
   <select class="widefat" id="<?php echo $this->get_field_id('recent_show'); ?>" name="<?php echo $this->get_field_name('recent_show'); ?>">
   <?php 
         if($instance['recent_show'] == 'yes2'){
            $yes2 = 'selected="selected"';
            $no2  = '';
         }
         elseif($instance['recent_show'] == 'no2'){
            $no2 = 'selected="selected"';
            $yes2= '';
         }
         echo '
         <option value="yes2" '.$yes2.'>'.__("Yes", "jozoorthemes").'</option>
         <option value="no2" '.$no2.'>'.__("No", "jozoorthemes").'</option>
         ';
         
         ?>
       </select>
  </p>
  
  <!-- =========== End ================ -->
  <p><hr style="margin: 16px 0 3px 0; border: 1px solid #ccc;"/></p>
  <!-- =========== start comments tab ================ -->
  
  <p>
    <label for="<?php echo $this->get_field_id('comments_tab_name'); ?>"><?php _e('Comments Tab Name :', 'jozoorthemes'); ?></label>
    <input class="widefat" type="text" name="<?php echo $this->get_field_name('comments_tab_name') ?>" id="<?php echo $this->get_field_id('comments_tab_name') ?> " value="<?php echo esc_attr($instance['comments_tab_name']) ?>">
  </p>

  
  <p>
   <label for="<?php echo $this->get_field_id('comments_num'); ?>"><?php _e('Number of Comments :', 'jozoorthemes'); ?></label>
   <input type="text" name="<?php echo $this->get_field_name('comments_num') ?>" id="<?php echo $this->get_field_id('comments_num') ?> " value="<?php echo esc_attr($instance['comments_num']) ?>" size="10">
  </p>
  
  <p>
   <label for="<?php echo $this->get_field_id('comments_show'); ?>"><?php _e('Show Comments Tab :', 'jozoorthemes'); ?></label>
   <select class="widefat" id="<?php echo $this->get_field_id('comments_show'); ?>" name="<?php echo $this->get_field_name('comments_show'); ?>">
   <?php 
         if($instance['comments_show'] == 'yes3'){
            $yes3 = 'selected="selected"';
            $no3  = '';
         }
         elseif($instance['comments_show'] == 'no3'){
            $no3 = 'selected="selected"';
            $yes3= '';
         }
         echo '
         <option value="yes3" '.$yes3.'>'.__("Yes", "jozoorthemes").'</option>
         <option value="no3" '.$no3.'>'.__("No", "jozoorthemes").'</option>
         ';
         
         ?>
       </select>
  </p>
  
  <!-- =========== End ================ -->


  <?php
}



}

function jozoor_tabs_load_widgets() {
  register_widget('jozoor_Blog_Tabs');
}

add_action('widgets_init', 'jozoor_tabs_load_widgets');

?>
