<div class="featured-clients clearfix bottom-2">
     
       <div class="slidewrap4" >
    
        <div class="sixteen columns"> 
         <h3 class="title bottom-2"><?php global $smof_data; if(!empty($smof_data['static_words_featured_clients'])) { echo $smof_data['static_words_featured_clients']; } else { _e("Featured Clients", "jozoorthemes"); } ?></h3> 
        </div>
        
        <ul class="slider" id="sliderName4">
            
        <?php
        global $smof_data; // get options

        if( !empty($smof_data['home1_clients_num']) ) {
          $j_clients_num = $smof_data['home1_clients_num'];
        } else {
          $j_clients_num = 8;
        }

        if( !empty($smof_data['home1_clients_slides_num']) ) {
          $j_clients_slides_num = $smof_data['home1_clients_slides_num'];
        } else {
          $j_clients_slides_num = 2;
        }

        $clients_args = array( 'post_type' => 'clients', 'posts_per_page' => $j_clients_num );
        $clients_query = get_posts($clients_args);

        $clients_per_page = $j_clients_num; // num of clients
        $clients_num_cols = $j_clients_slides_num; // num of columns
        $clients_num_per_cols = ceil($clients_per_page / $clients_num_cols);
        $clients_group = array_chunk($clients_query, $clients_num_per_cols);
 
        // Start Counter
        $counter = 0;

        // Start loop
        foreach ($clients_group as $posts) {
        echo '<li class="slide">';
            
        foreach($posts as $post) { // while(have_posts()) 
        
        // get metaboxs
        $j_client_online_url = get_post_meta( $post->ID, '_jozoor_client_online_url', true ); // client url
        ?>
        
            <!-- item -->
             <div id="post-<?php the_ID(); ?>" <?php post_class('four columns item'); ?>>
             <?php if($j_client_online_url) { ?>
             <a href="<?php echo $j_client_online_url; ?>" target="_blank">
             <?php } ?>
             <?php echo get_the_post_thumbnail($post->ID, 'full', array('title' => '')); ?>
             <?php if($j_client_online_url) { ?>
             </a>
             <?php } ?>
             </div>
             <!-- End -->
          
        <?php
        } 
            
        echo '</li><!-- End slide -->';
        $counter ++;   
        }
        wp_reset_postdata();
        ?>
          
        </ul>
      
       </div><!-- End slidewrap -->
     
     </div><!-- End features-clients -->