<div class="recent-work portfolio clearfix bottom-2">
     
     <div class="slidewrap1" >
    
      <div class="sixteen columns"> 
       <h3 class="title bottom-2"><?php global $smof_data; if(!empty($smof_data['static_words_recent_work'])) { echo $smof_data['static_words_recent_work']; } else { _e("Recent Work", "jozoorthemes"); } ?></h3> 
      </div>
      
      <ul class="slider" id="sliderName1">
          
        <?php
        global $smof_data; // get options

        if( !empty($smof_data['home1_projects_num']) ) {
          $j_projects_num = $smof_data['home1_projects_num'];
        } else {
          $j_projects_num = 12;
        }

        if( !empty($smof_data['home1_projects_slides_num']) ) {
          $j_projects_slides_num = $smof_data['home1_projects_slides_num'];
        } else {
          $j_projects_slides_num = 2;
        }

        $works_args = array( 'post_type' => 'portfolio', 'posts_per_page' => $j_projects_num );
        $works_query = get_posts($works_args);

        $works_per_page = $j_projects_num; // num of works
        $works_num_cols = $j_projects_slides_num; // num of columns
        $works_num_per_cols = ceil($works_per_page / $works_num_cols);
        $works_group = array_chunk($works_query, $works_num_per_cols);
 
        // Start Counter
        $counter = 0;

        // Start loop
        foreach ($works_group as $posts) {
        echo '<li class="slide">';
            
        foreach($posts as $post) { // while(have_posts()) ?>
        
            <!-- item -->
             <div id="post-<?php the_ID(); ?>" <?php post_class('one-third column item col3'); ?>>
             <a href="<?php echo get_permalink(); ?>">
             <?php echo get_the_post_thumbnail($post->ID, 'portfolio-thumb', array('title' => '')); ?>
             <div class="img-caption">
             <div class="desc">
             <h3><?php the_title(); ?></h3>
             <p><?php
             $j_terms = get_the_terms( $post->ID, 'portfolio-category' );						
             if ( $j_terms && ! is_wp_error( $j_terms ) ) : 
             $j_draught_links = array();
             foreach ( $j_terms as $term ) {
             $j_draught_links[] = $term->name;
             }		
             echo $j_on_draught = join( ", ", $j_draught_links );
             endif;
             ?></p>
             <span>+</span></div>
             </div><!-- hover effect -->
             </a>
             </div>
             <!-- End -->
          
        <?php
        } 
            
        echo '</li><!-- End slide -->';
        $counter ++;   
        }
        wp_reset_postdata();
        ?>
        
        </ul>
      
      </div><!-- End slidewrap -->
    
     </div><!-- End recent-work -->