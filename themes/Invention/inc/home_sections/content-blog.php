<div class="latest-blog clearfix bottom-1">
     
       <div class="slidewrap2" >
    
      <div class="sixteen columns"> 
       <h3 class="title bottom-2"><?php global $smof_data; if(!empty($smof_data['static_words_latest_news'])) { echo $smof_data['static_words_latest_news']; } else { _e("Latest News", "jozoorthemes"); } ?> 
       <?php 
       global $smof_data; // get options

       if( !empty($smof_data['home1_news_num']) ) {
          $j_news_num = $smof_data['home1_news_num'];
       } else {
          $j_news_num = 3;
       }

       $j_count_posts = wp_count_posts('post');
       $j_published_posts = $j_count_posts->publish;
       if( $j_published_posts > $j_news_num ) {
       
       if ($smof_data['home1_latest_news_blog_page_link'] != 'Select Page Name') {
       global $wpdb;
       $j_page_id = $wpdb->get_var( 'SELECT ID FROM '.$wpdb->posts.' WHERE post_name="'.$smof_data['home1_latest_news_blog_page_link'].'" AND post_type="page" AND post_status="publish"' );
       $j_blog_page_link = get_page_link($j_page_id);
       } else { $j_blog_page_link = '#'; }
       ?>
       <a href="<?php echo $j_blog_page_link; ?>" class="more black" data="<?php _e("More News", "jozoorthemes"); ?>">+</a>
       <?php } ?>
       </h3> 
      </div>
      
      <ul class="slider" id="sliderName2">
        
      <li class="slide">
          
      <?php
      
      $posts_args = array( 'post_type' => 'post', 'posts_per_page' => $j_news_num );
      $posts_query = new WP_Query( $posts_args );
      while ( $posts_query->have_posts() ) : $posts_query->the_post();

      ?>
       
      <!-- item -->
      <div id="post-<?php the_ID(); ?>" <?php post_class('one-third column item'); ?>>
       <a href="<?php echo get_permalink(); ?>">
       <?php 
       if ( has_post_thumbnail() ) {
          echo get_the_post_thumbnail($post->ID, 'latest-news-home', array('title' => ''));
       }
       ?>
       </a>
       <h3><a href="<?php echo get_permalink(); ?>"><?php the_title(); ?></a></h3>
       <div class="meta"><?php the_time('d M, Y'); ?> <?php if ( comments_open() || get_comments_number() ) : ?> / <a href="<?php comments_link(); ?>">
       <?php comments_popup_link( __("Leave a comment", "jozoorthemes") , __("1 Comment", "jozoorthemes"), __("% Comments", "jozoorthemes") ); ?></a><?php endif; ?></div>
       <?php the_excerpt(); ?>
      </div>
      <!-- End -->
       
      <?php  
      endwhile;
      wp_reset_postdata();
      ?>
            
      </li><!-- End slide -->
        
      </ul>
      
      </div><!-- End slidewrap -->
     
     </div><!-- End latest-blog -->