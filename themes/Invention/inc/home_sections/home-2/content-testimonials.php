<div class="one-third column bottom-3">
    
     <div class="slidewrap3" data-autorotate="4000">
      
      <h3 class="title bottom-2"><?php global $smof_data; if(!empty($smof_data['static_words_testimonials'])) { echo $smof_data['static_words_testimonials']; } else { _e("Testimonials", "jozoorthemes"); } ?></h3> 
         
      <?php 
       global $smof_data; // get options

       if( !empty($smof_data['home2_testimonials_num']) ) {
          $j_testimonials_num = $smof_data['home2_testimonials_num'];
       } else {
          $j_testimonials_num = -1;
       }

       ?>
      
       <ul class="slider" id="sliderName3">
           
      <?php
      
      $testimonials_args = array( 'post_type' => 'testimonials', 'posts_per_page' => $j_testimonials_num );
      $testimonials_query = new WP_Query( $testimonials_args );
      while ( $testimonials_query->have_posts() ) : $testimonials_query->the_post();

      // get metaboxs
      $j_testimonials_client_online_url = get_post_meta( $post->ID, '_jozoor_testimonial_client_online_url', true ); // client url
      $j_testimonials_client_job = get_post_meta( $post->ID, '_jozoor_testimonial_client_job', true ); // client job
    
      if(!empty($j_testimonials_client_online_url)) {
       $j_test_client_url_start = '<a href="'.$j_testimonials_client_online_url.'" target="_blank">';
       $j_test_client_url_end = '</a>';
      } else {
       $j_test_client_url_start = '';
       $j_test_client_url_end = '';
      }

      ?>
       
      <li class="slide">
      <div id="post-<?php the_ID(); ?>" <?php post_class('testimonials'); ?>>
       
        <div class="testimonial-item">
        <?php the_content(); ?>
        <div class="testimonials-arrow"></div>
        <div class="author"><?php echo $j_test_client_url_start; ?><span class="color"><?php the_title(); ?></span><?php echo $j_test_client_url_end; ?>, <?php echo $j_testimonials_client_job; ?></div>
        </div>
          
      </div>
      </li><!-- End slide -->
       
      <?php  
      endwhile;
      wp_reset_postdata();
      ?>
        
        </ul>
      
      </div><!-- End slidewrap -->
      
    </div><!-- End column -->