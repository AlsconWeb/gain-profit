<div class="two-thirds column">
     
     <div class="latest-blog home-2 clearfix bottom-1">
     
       <div class="slidewrap2" >
    
       <h3 class="title bottom-2"><?php global $smof_data; if(!empty($smof_data['static_words_latest_news'])) { echo $smof_data['static_words_latest_news']; } else { _e("Latest News", "jozoorthemes"); } ?> 
       <?php 
       global $smof_data; // get options

       if( !empty($smof_data['home2_news_num']) ) {
          $j_news_num = $smof_data['home2_news_num'];
       } else {
          $j_news_num = 2;
       }

       ?>
       </h3> 
      
      <ul class="slider" id="sliderName2">
        
      <li class="slide">
          
      <?php
      
      $posts_args = array( 'post_type' => 'post', 'posts_per_page' => $j_news_num );
      $posts_query = new WP_Query( $posts_args );
      while ( $posts_query->have_posts() ) : $posts_query->the_post();

      ?>
       
      <!-- item -->
      <div id="post-<?php the_ID(); ?>" <?php post_class('one-third column item'); ?>>
       <a href="<?php echo get_permalink(); ?>">
       <?php 
       if ( has_post_thumbnail() ) {
          echo get_the_post_thumbnail($post->ID, 'latest-news-home', array('title' => ''));
       }
       ?>
       </a>
       <h3><a href="<?php echo get_permalink(); ?>"><?php the_title(); ?></a></h3>
       <div class="meta"><?php the_time('d M, Y'); ?> <?php if ( comments_open() || get_comments_number() ) : ?> / <a href="<?php comments_link(); ?>">
       <?php comments_popup_link( __("Leave a comment", "jozoorthemes") , __("1 Comment", "jozoorthemes"), __("% Comments", "jozoorthemes") ); ?></a><?php endif; ?></div>
      </div>
      <!-- End -->
       
      <?php  
      endwhile;
      wp_reset_postdata();
      ?>
            
      </li><!-- End slide -->
        
      </ul>
      
      </div><!-- End slidewrap -->
     
     </div><!-- End latest-blog -->
     
</div><!-- End column -->
