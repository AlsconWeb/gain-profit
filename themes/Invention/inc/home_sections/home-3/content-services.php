<div class="services style-2 home s-2 bottom-3">
     <div class="container clearfix">
     
      <?php
      global $smof_data; // get options
      if( !empty($smof_data['home3_services_num']) ) {
          $j_services_num = $smof_data['home3_services_num'];
      } else {
          $j_services_num = 6;
      }
      
      $services_args = array( 'post_type' => 'services', 'order' => 'ASC', 'posts_per_page' => $j_services_num );
      $services_query = new WP_Query( $services_args );
      while ( $services_query->have_posts() ) : $services_query->the_post();

      // get metaboxs
      $j_service_icon_type = get_post_meta( $post->ID, '_jozoor_service_icon_type', true ); // icon type
      $j_service_icon_font_name = get_post_meta( $post->ID, '_jozoor_service_icon_font_name', true ); // icon font name
      $j_service_icon_image_name = get_post_meta( $post->ID, '_jozoor_service_icon_image_name', true ); // icon image
      $j_service_icon_font_spin = get_post_meta( $post->ID, '_jozoor_service_icon_font_spin', true ); // icon spin
      $j_service_icon_font_flip = get_post_meta( $post->ID, '_jozoor_service_icon_font_flip', true ); // icon flip
      $j_service_icon_font_rotate = get_post_meta( $post->ID, '_jozoor_service_icon_font_rotate', true ); // icon rotate

      // check icon font options
      // spin
      if($j_service_icon_font_spin == 'on') { $j_spin_icon_class = ' icon-spin'; } else { $j_spin_icon_class = ''; }
      // flip
      if ($j_service_icon_font_flip == 'horizontal') { $j_icon_flip_class = ' icon-flip-horizontal'; } 
      elseif ($j_service_icon_font_flip == 'vertical') { $j_icon_flip_class = ' icon-flip-vertical'; }
      else { $j_icon_flip_class = ''; }
      // rotate
      if ($j_service_icon_font_rotate == 'ro90') { $j_icon_rotate_class = ' icon-rotate-90'; } 
      elseif ($j_service_icon_font_rotate == 'ro180') { $j_icon_rotate_class = ' icon-rotate-180'; }
      elseif ($j_service_icon_font_rotate == 'ro270') { $j_icon_rotate_class = ' icon-rotate-270'; }
      else { $j_icon_rotate_class = ''; }

      ?>
         
      <div id="post-<?php the_ID(); ?>" <?php post_class('one-third column'); ?>>
         <div class="item bottom-4">
           <div class="circle float-left"><a href="<?php echo get_permalink(); ?>">
           <?php if($j_service_icon_type == 'iconimage') { ?>
           <img src="<?php echo $j_service_icon_image_name; ?>" alt="<?php the_title(); ?>">  
           <?php } else { ?>
           <i class="<?php echo $j_service_icon_font_name.$j_spin_icon_class.$j_icon_flip_class.$j_icon_rotate_class; ?>"></i>
           <?php } ?>
           </a></div>
           <div class="data float-right">
           <h4><a href="<?php echo get_permalink(); ?>"><?php the_title(); ?></a></h4>
           <?php the_excerpt(); ?>
           </div>
         </div>
      </div><!-- End item -->

      <?php  
      endwhile;
      wp_reset_postdata();
      ?>
       
     </div><!-- End Container -->
   </div><!-- End services -->