(function() {

    tinymce.create('tinymce.plugins.jozoor_plugin', {
        init : function(ed, url) {
                // Register command for when button is clicked
                ed.addCommand('jozoor_insert_shortcode', function() {
                    tb_show("Jozoor Shortcodes", url + '/inc/shortcode-generator.php?height=540&width=640');
                });

            // Register button
            ed.addButton('jozoor_button', {title : 'Jozoor Shortcodes', cmd : 'jozoor_insert_shortcode', image: url + '/inc/icons/shortcodes.png' });
        },   
        
        createControl : function(n, cm) {
			  return null;
        },
		  getInfo : function() {
			return {
				longname : 'Jozoor TinyMCE Shortcodes',
				author : 'Jozoor Team',
				authorurl : 'http://jozoor.com',
				infourl : 'http://jozoor.com',
				version : tinymce.majorVersion + "." + tinymce.minorVersion
			};
		}
        
    });

    // Register TinyMCE plugin
    tinymce.PluginManager.add('jozoor_button', tinymce.plugins.jozoor_plugin);
    
})();