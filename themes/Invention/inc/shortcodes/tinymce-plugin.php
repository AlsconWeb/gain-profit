<?php

// init process for registering our button
add_action('init', 'jozoor_shortcode_button_init');
function jozoor_shortcode_button_init() {

      // Abort early if the user will never see TinyMCE
      if ( ! current_user_can('edit_posts') && ! current_user_can('edit_pages') ) {
           return;
      }
    
      if ( get_user_option('rich_editing') == 'true' ) {
         add_filter("mce_external_plugins", "jozoor_register_tinymce_plugin"); // regiser tinymce plugin
         add_filter('mce_buttons', 'jozoor_add_tinymce_button'); // add button to the TinyMCE toolbar
      }
    
}

// This callback registers our plugin
function jozoor_register_tinymce_plugin($plugin_array) {
    $plugin_array['jozoor_button'] = get_template_directory_uri() . '/inc/shortcodes/tinymce-js.js';
    return $plugin_array;
}

// This callback adds our button to the toolbar
function jozoor_add_tinymce_button($buttons) {
    //Add the button ID to the $button array
    array_push( $buttons, "jozoor_button" );
	return $buttons;
}


?>