<?php 
// Access WordPress 
$absolute_path = __FILE__;
$path_to_file = explode( 'wp-content', $absolute_path );
$path_to_wp = $path_to_file[0];

require_once( $path_to_wp . '/wp-load.php' );
?>

<!DOCTYPE html>
<html>
<head>
<title></title>

<!-- Start Styles -->
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/inc/shortcodes/inc/js/minimalect/minimalect.css" />
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/inc/shortcodes/inc/css/shortcode-generator.css" />
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/font-awesome/css/font-awesome.min.css" />
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/fontello/css/fontello.css" />

<!-- Start JavaScript -->
<script src="<?php echo get_template_directory_uri(); ?>/inc/shortcodes/inc/js/minimalect/minimalect.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/inc/shortcodes/inc/js/init.js"></script>
    
</head>

<body>	
    
<?php
      // icons list
      $jozoor_shortcode_icons_list = array(
          'glass', 'music', 'search', 'envelope', 'heart', 'star', 'star-empty', 'user', 'film', 'th-large', 'th', 'th-list', 'ok', 'zoom-in', 'zoom-out', 'off', 'signal', 'cog', 'trash', 'home',
          'file', 'time', 'road', 'download-alt', 'download', 'upload', 'inbox', 'play-circle', 'repeat', 'refresh', 'list-alt', 'lock', 'flag', 'headphones', 'volume-off', 'volume-down', 'volume-up',           'qrcode', 'barcode', 'tag', 'tags', 'book', 'bookmark', 'print', 'camera', 'font', 'bold', 'italic', 'text-height', 'text-width', 'align-left', 'align-center', 'align-right', 'align-justify',           'list', 'indent-left', 'indent-right', 'facetime-video', 'pencil', 'map-marker', 'adjust', 'tint', 'edit', 'share', 'check', 'move', 'step-backward', 'fast-backward', 'backward', 'play',               'pause', 'stop', 'forward', 'fast-forward', 'step-forward', 'eject', 'chevron-left', 'chevron-right', 'plus-sign', 'minus-sign', 'remove-sign', 'ok-sign', 'question-sign',
          'info-sign', 'screenshot', 'remove-circle', 'ok-circle', 'ban-circle', 'arrow-left', 'arrow-right', 'arrow-up', 'arrow-down', 'share-alt', 'resize-full', 'resize-small', 'plus', 'minus',               'asterisk', 'exclamation-sign', 'gift', 'leaf', 'fire', 'eye-open', 'eye-close',
          'warning-sign', 'plane', 'calendar', 'random', 'comment', 'magnet', 'chevron-up', 'chevron-down', 'retweet', 'shopping-cart', 'folder-close', 'folder-open', 'resize-vertical', 
          'resize-horizontal', 'bar-chart', 'twitter-sign', 'facebook-sign', 'camera-retro', 'key', 'cogs', 'comments',
          'thumbs-up', 'thumbs-down', 'star-half', 'heart-empty', 'signout', 'linkedin-sign', 'pushpin', 'external-link', 'signin', 'trophy', 'github-sign', 'upload-alt', 'lemon', 'phone', 
          'check-empty', 'bookmark-empty', 'phone-sign', 'twitter', 'facebook', 'github', 'unlock',
          'credit', 'rss', 'hdd', 'bullhorn', 'bell', 'certificate', 'hand-right', 'hand-left', 'hand-up', 'hand-down', 'circle-arrow-left', 'circle-arrow-right', 'circle-arrow-up', 
          'circle-arrow-down', 'globe', 'wrench', 'tasks', 'filter', 'briefcase', 'fullscreen', 'group',
          'link', 'cloud', 'beaker', 'cut', 'copy', 'paper-clip', 'save', 'sign-blank', 'reorder', 'list-ul', 'list-ol', 'strikethrough', 'underline', 'table', 'magic', 'truck', 'pinterest',                     'pinterest-sign', 'google-plus-sign', 'google-plus', 'money',
          'caret-down', 'caret-up', 'caret-left', 'caret-right', 'columns', 'sort', 'sort-down', 'sort-up', 'envelope-alt', 'linkedin', 'undo', 'legal', 'dashboard', 'comment-alt', 'comments-alt',               'bolt', 'sitemap', 'umbrella', 'paste', 'lightbulb', 'exchange',
          'cloud-download', 'cloud-upload', 'user-md', 'stethoscope', 'suitcase', 'bell-alt', 'coffee', 'food', 'file-alt', 'building', 'hospital', 'ambulance', 'medkit', 'fighter-jet', 'beer', 
          'h-sign', 'plus-sign2', 'double-angle-left', 'double-angle-right', 'double-angle-up', 'double-angle-down',
          'angle-left', 'angle-right', 'angle-up', 'angle-down', 'desktop', 'laptop', 'tablet', 'mobile', 'circle-blank', 'quote-left', 'quote-right', 'spinner', 'circle', 'reply', 'github-alt',                 'folder-close-alt', 'folder-open-alt', 'expand-alt', 'collapse-alt', 'smile', 'frown',
          'meh', 'gamepad', 'keyboard', 'flag-alt', 'flag-checkered', 'terminal', 'code', 'reply-all', 'star-half-full', 'location-arrow', 'crop', 'code-fork', 'unlink', 'question', 'info',                       'exclamation', 'superscript', 'subscript', 'eraser', 'puzzle', 'microphone',
          'microphone-off', 'shield', 'calendar-empty', 'fire-extinguisher', 'rocket', 'maxcdn', 'chevron-sign-left', 'chevron-sign-right', 'chevron-sign-up', 'chevron-sign-down', 'html5', 'css3',               'anchor', 'unlock-alt', 'bullseye', 'ellipsis-horizontal', 'ellipsis-vertical', 'rss-sign', 'play-sign', 'ticket', 'minus-sign-alt',
          'check-minus', 'level-up', 'file-text', 'sort-by-alphabet', 'sort-by-alphabet-alt', 'tumblr', 'tumblr-sign', 'long-arrow-down', 'vk', 'weibo', 'renren', 'level-down', 'check-sign', 
          'edit-sign', 'external-link-sign', 'share-sign', 'compass', 'collapse', 'collapse-top', 'expand', 'euro', 'gbp', 'dollar', 'rupee', 'yen', 'renminbi', 'won', 'bitcoin', 'file2', 
          'sort-by-attributes', 'sort-by-attributes-alt', 'sort-by-order', 'sort-by-order-alt', 'thumbs-up2', 'thumbs-down2', 'youtube-sign', 'youtube', 'xing', 'xing-sign', 'youtube-play', 'dropbox', 
          'stackexchange', 'instagram', 'flickr', 'adn', 'bitbucket', 'bitbucket-sign', 'long-arrow-up', 'long-arrow-left', 'long-arrow-right', 'apple', 'windows', 'android', 'linux', 'dribbble',                 'skype', 'foursquare', 'trello', 'female', 'male', 'gittip', 'sun', 'moon', 'archive', 'bug', 'picture', 'remove'
      );

      // social icons list
      $jozoor_shortcode_social_icons_list = array(
          'twitter', 'facebook', 'forrst', 'reddit', 'rss', 'tumblr', 'delicious', 'paypal', 'gplus', 'android', 'pinterest', 'foursquare', 'behance', 'deviantart', 'digg', 'dribbble', 
          'blogger', 'flickr', 'google', 'skype', 'youtube', 'vimeo', 'yahoo', 'dropbox', 'soundcloud', 'instagram', 'wordpress', 'duckduckgo', 'aim', 'flattr', 'eventful', 'smashmag', 
          'wikipedia', 'lanyrd', 'stumbleupon', 'fivehundredpx', 'bitcoin', 'w3c', 'html5', 'ie', 'grooveshark', 'ninetyninedesigns', 'spotify', 'gowalla', 'appstore', 'cc', 'evernote', 
          'viadeo', 'instapaper', 'weibo', 'klout', 'linkedin', 'meetup', 'vk', 'plancast', 'disqus', 'windows', 'xing', 'chrome', 'myspace', 'podcast', 'amazon', 'steam', 
          'cloudapp', 'ebay', 'github', 'googleplay', 'itunes', 'plurk', 'songkick', 'lastfm', 'gmail', 'openid', 'quora', 'eventasaurus', 'yelp', 'intensedebate', 'eventbrite', 
          'scribd', 'posterous', 'stripe', 'opentable', 'angellist', 'dwolla', 'appnet', 'statusnet', 'drupal', 'buffer', 'pocket', 'github-circled', 'bitbucket', 'lego', 'stackoverflow', 
          'hackernews', 'lkdto'
      );

     // icons list
      $jozoor_shortcode_animation_type_list = array(
          'bounce', 'flash', 'pulse', 'rubberBand', 'shake', 'swing', 'tada', 'wobble', 'bounceIn', 'bounceInDown', 'bounceInLeft', 'bounceInRight', 'bounceInUp', 'bounceOut',
          'bounceOutDown', 'bounceOutLeft', 'bounceOutRight', 'bounceOutUp', 'fadeIn', 'fadeInDown', 'fadeInDownBig', 'fadeInLeft', 'fadeInLeftBig', 'fadeInRight', 'fadeInRightBig',
          'fadeInUp', 'fadeInUpBig', 'fadeOut', 'fadeOutDown', 'fadeOutDownBig', 'fadeOutLeft', 'fadeOutLeftBig', 'fadeOutRight', 'fadeOutRightBig', 'fadeOutUp', 'fadeOutUpBig', 
          'flip', 'flipInX', 'flipInY', 'flipOutX', 'flipOutY', 'lightSpeedIn', 'lightSpeedOut', 'rotateIn', 'rotateInDownLeft', 'rotateInDownRight', 'rotateInUpLeft', 'rotateInUpRight',
          'rotateOut', 'rotateOutDownLeft', 'rotateOutDownRight', 'rotateOutUpLeft', 'rotateOutUpRight', 'slideInDown', 'slideInLeft', 'slideInRight', 'slideOutLeft', 'slideOutRight',
          'slideOutUp', 'hinge', 'rollIn', 'rollOut'
      );
?>
    
<div id="shortcode-generator">
    
  <select class="dropdown">
      
    <optgroup label="<?php _e("Columns", "jozoorthemes"); ?>">
      <?php
      // columns list
      $jozoor_shortcode_columns = array(
          "one_half" => '1/2', "one_third" => '1/3', "two_thirds" => '2/3', "one_fourth" => '1/4', "one_eleven" => '11', "one_five" => '5', "one_twelve" => '12'
      );

      foreach ($jozoor_shortcode_columns as $id => $column) {
      echo '<option value="'.$id.'">'. $column .' '.__("Column", "jozoorthemes").'</option>';
      }
      ?>
    </optgroup>
      
    <optgroup label="<?php _e("Elements", "jozoorthemes"); ?>">
       <option value="heading_title"><?php _e("Heading Title", "jozoorthemes"); ?></option>
       <option value="button"><?php _e("Button", "jozoorthemes"); ?></option>
       <option value="highlight"><?php _e("Highlight", "jozoorthemes"); ?></option>
       <option value="dropcaps"><?php _e("Dropcaps", "jozoorthemes"); ?></option>
       <option value="tooltip"><?php _e("Tooltip", "jozoorthemes"); ?></option>
       <option value="info_box"><?php _e("Info Box", "jozoorthemes"); ?></option>
       <option value="info_box_big"><?php _e("Big Info Box", "jozoorthemes"); ?></option>
       <option value="divider"><?php _e("Divider", "jozoorthemes"); ?></option>
       <option value="list"><?php _e("List Styles", "jozoorthemes"); ?></option>
       <option value="alert_box"><?php _e("Alert Box", "jozoorthemes"); ?></option>
       <option value="progress_bar"><?php _e("Progress Bar", "jozoorthemes"); ?></option>
       <option value="accordion"><?php _e("Accordion", "jozoorthemes"); ?></option>
       <option value="toggle"><?php _e("Toggle", "jozoorthemes"); ?></option>
       <option value="tabs"><?php _e("Tabs", "jozoorthemes"); ?></option>
       <option value="pricing_table"><?php _e("Pricing Table", "jozoorthemes"); ?></option>
       <option value="icon"><?php _e("Icon", "jozoorthemes"); ?></option>
       <option value="social_icon"><?php _e("Social Icon", "jozoorthemes"); ?></option>
       <option value="icon_boxes"><?php _e("Icon Boxes", "jozoorthemes"); ?></option>
       <option value="carousel_slider"><?php _e("Carousel Slider", "jozoorthemes"); ?></option>
       <option value="images_slider"><?php _e("Images Slider", "jozoorthemes"); ?></option>
       <option value="animate_element"><?php _e("Animate Element", "jozoorthemes"); ?></option>
       <option value="gmaps"><?php _e("Google Map", "jozoorthemes"); ?></option>
    </optgroup>
      
    <optgroup label="<?php _e("Query Posts", "jozoorthemes"); ?>">
       <option value="latest_posts"><?php _e("Latest News (default posts)", "jozoorthemes"); ?></option>
       <option value="recent_projects"><?php _e("Recent Projects", "jozoorthemes"); ?></option>
       <option value="services"><?php _e("Services", "jozoorthemes"); ?></option>
       <option value="clients"><?php _e("Clients", "jozoorthemes"); ?></option>
       <option value="testimonials"><?php _e("Testimonials", "jozoorthemes"); ?></option>
       <option value="team"><?php _e("Team", "jozoorthemes"); ?></option>
    </optgroup>
      
  </select>
    
  <div class="clear"></div>
      
    <?php
    // columns list
    $jozoor_shortcode_columns_content = array(
          'one_half', 'one_third', 'two_thirds', 'one_fourth', 'one_eleven', 'one_five', 'one_twelve'
    );

    foreach ($jozoor_shortcode_columns_content as $column_id) {
      echo '
      <div id="'.$column_id.'" class="content">
      
      <div class="option">
      <div class="label"><label for="main_content">'.__("Content", "jozoorthemes").' : </label></div>
      <div class="option-content">
        <textarea id="main_content" name="main_content_'.$column_id.'"></textarea>
      </div>
      </div><!-- /option -->
      
      <div class="option">
      <div class="label"><label for="text_center_'.$column_id.'">'.__("Text Align Center", "jozoorthemes").' : </label></div>
      <div class="option-content">
        <input type="checkbox" id="text_center_'.$column_id.'" name="text_center_'.$column_id.'">
        <small>'.__("this option make text centered", "jozoorthemes").'</small>
      </div>
      </div><!-- /option -->
      
      <div class="option">
      <div class="label"><label for="last_column_'.$column_id.'">'.__("Last Column", "jozoorthemes").' : </label></div>
      <div class="option-content">
        <input type="checkbox" id="last_column_'.$column_id.'" name="last_column_'.$column_id.'">
        <small>'.__("this option use of last column in row", "jozoorthemes").'</small>
      </div>
      </div><!-- /option -->
      
      <div class="option">
      <div class="label"><label for="remove_margin_bottom_'.$column_id.'">'.__("Remove Margin Bottom", "jozoorthemes").' : </label></div>
      <div class="option-content">
        <input type="checkbox" id="remove_margin_bottom_'.$column_id.'" name="remove_margin_bottom_'.$column_id.'">
        <small>'.__("this option make marrgin bottom = 0", "jozoorthemes").'</small>
      </div>
      </div><!-- /option -->
      
      <div class="option">
      <div class="label"><label for="animate_'.$column_id.'">'.__("Animate Column", "jozoorthemes").' : </label></div>
      <div class="option-content">
        <input type="checkbox" id="animate_'.$column_id.'" name="animate_'.$column_id.'">
        <small>'.__("yes add CSS animation", "jozoorthemes").'</small>
      </div>
      </div><!-- /option -->
      
      <div class="option" id="animate_type_content_'.$column_id.'">
      <div class="label padding-top"><label for="animate_type_'.$column_id.'">'.__("Animate type", "jozoorthemes").' : </label></div>
      <div class="option-content">
        <select class="animation" id="animate_type_'.$column_id.'" name="animate_type_'.$column_id.'">';
        foreach ($jozoor_shortcode_animation_type_list as $animation_name) {
        echo '<option value="'.$animation_name.'">'. $animation_name .'</option>';  
        }
        echo'
        </select>
        <small><a href="http://daneden.github.io/animate.css/" target="_blank">'.__("See all animation types in active", "jozoorthemes").'</a></small>
      </div>
      </div><!-- /option -->
      
      <div class="option" id="animate_duration_content_'.$column_id.'">
      <div class="label padding-top"><label for="animate_duration_'.$column_id.'">'.__("Animate Duration", "jozoorthemes").' : </label></div>
      <div class="option-content">
        <input type="text" id="animate_duration_'.$column_id.'" name="animate_duration_'.$column_id.'" value="" placeholder="EX : 2">
        <small class="margin-top">'.__("Change the animation duration, default is 2 (seconds)", "jozoorthemes").'</small>
      </div>
      </div><!-- /option -->
      
      <div class="option" id="animate_delay_content_'.$column_id.'">
      <div class="label padding-top"><label for="animate_delay_'.$column_id.'">'.__("Animate Delay", "jozoorthemes").' : </label></div>
      <div class="option-content">
        <input type="text" id="animate_delay_'.$column_id.'" name="animate_delay_'.$column_id.'" value="" placeholder="EX : 0">
        <small class="margin-top">'.__("Delay before the animation starts, default is 0 (seconds)", "jozoorthemes").'</small>
      </div>
      </div><!-- /option -->
      
      <div class="option" id="animate_iteration_content_'.$column_id.'">
      <div class="label padding-top"><label for="animate_iteration_'.$column_id.'">'.__("Animate Iteration", "jozoorthemes").' : </label></div>
      <div class="option-content">
        <input type="text" id="animate_iteration_'.$column_id.'" name="animate_iteration_'.$column_id.'" value="" placeholder="EX : 0">
        <small class="margin-top">'.__("Number of times the animation is repeated, default is 0 (times)", "jozoorthemes").'</small>
      </div>
      </div><!-- /option -->
      
      </div><!-- /column content-->
      ';
    }
    ?>
    
  <div id="heading_title" class="content">
      
    <div class="option">
      <div class="label padding-top"><label for="main_content"><?php _e("Title Text", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <input type="text" id="main_content" name="main_content_heading_title" value="">
      </div>
    </div><!-- /option -->
      
    <div class="option">
      <div class="label"><label for="text_center_heading_title"><?php _e("Text Align Center", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <input type="checkbox" id="text_center_heading_title" name="text_center_heading_title">
        <small><?php _e("this option make title text centered", "jozoorthemes"); ?></small>
      </div>
    </div><!-- /option -->
      
    <div class="option">
      <div class="label"><label for="animate_heading_title"><?php _e("Animate Title", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <input type="checkbox" id="animate_heading_title" name="animate_heading_title">
        <small><?php _e("yes add CSS animation", "jozoorthemes"); ?></small>
      </div>
      </div><!-- /option -->
      
      <div class="option" id="animate_type_content_heading_title">
      <div class="label padding-top"><label for="animate_type_heading_title"><?php _e("Animate type", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <select class="animation" id="animate_type_heading_title" name="animate_type_heading_title">
        <?php
        foreach ($jozoor_shortcode_animation_type_list as $animation_name) {
        echo '<option value="'.$animation_name.'">'. $animation_name .'</option>';  
        }
        ?>
        </select>
        <small><a href="http://daneden.github.io/animate.css/" target="_blank"><?php _e("See all animation types in active", "jozoorthemes"); ?></a></small>
      </div>
      </div><!-- /option -->
      
      <div class="option" id="animate_duration_content_heading_title">
      <div class="label padding-top"><label for="animate_duration_heading_title"><?php _e("Animate Duration", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <input type="text" id="animate_duration_heading_title" name="animate_duration_heading_title" value="" placeholder="EX : 2">
        <small class="margin-top"><?php _e("Change the animation duration, default is 2 (seconds)", "jozoorthemes"); ?></small>
      </div>
      </div><!-- /option -->
      
      <div class="option" id="animate_delay_content_heading_title">
      <div class="label padding-top"><label for="animate_delay_heading_title"><?php _e("Animate Delay", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <input type="text" id="animate_delay_heading_title" name="animate_delay_heading_title" value="" placeholder="EX : 0">
        <small class="margin-top"><?php _e("Delay before the animation starts, default is 0 (seconds)", "jozoorthemes"); ?></small>
      </div>
      </div><!-- /option -->
      
      <div class="option" id="animate_iteration_content_heading_title">
      <div class="label padding-top"><label for="animate_iteration_heading_title"><?php _e("Animate Iteration", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <input type="text" id="animate_iteration_heading_title" name="animate_iteration_heading_title" value="" placeholder="EX : 0">
        <small class="margin-top"><?php _e("Number of times the animation is repeated, default is 0 (times)", "jozoorthemes"); ?></small>
      </div>
      </div><!-- /option -->
      
  </div><!-- /heading title -->
    
  <div id="button" class="content">
      
    <div class="option">
      <div class="label padding-top"><label for="main_content"><?php _e("Text", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <input type="text" id="main_content" name="main_content_button_text" value="">
        <small class="margin-top"><?php _e("if you want to show icon without text, let field empty", "jozoorthemes"); ?></small>
      </div>
    </div><!-- /option -->
      
    <div id="button_icons_list" class="option">
      <div class="label"><label><?php _e("Choose Icon", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <small class="margin-bottom"><?php _e("don't choose any icon if you want text enough", "jozoorthemes"); ?></small>
        <ul id="button_icon">
      <?php
      foreach ($jozoor_shortcode_icons_list as $icon_name) {
        echo '<li><i class="icon-'.$icon_name.'"></i></li>';  
      }
      foreach ($jozoor_shortcode_social_icons_list as $icon_name) {
        echo '<li><i class="social_icon-'.$icon_name.'"></i></li>';  
      }
      ?>
      </ul>
      </div>
    </div><!-- /option -->
      
    <div class="option">
      <div class="label"><label for="button_spin_icon"><?php _e("Spin Icon", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <input type="checkbox" id="button_spin_icon" name="button_spin_icon">
        <small><?php _e("yes make icon spin", "jozoorthemes"); ?></small>
      </div>
    </div><!-- /option -->
      
    <div id="button_icon_flip" class="option">
      <div class="label"><label><?php _e("Flip Icon", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <input type="radio" id="button_icon_flip_no" name="button_icon_flip" value="no" checked="checked">
        <label for="button_icon_flip_no"><?php _e("No", "jozoorthemes"); ?></label>
        <input type="radio" id="button_icon_flip_horizontal" name="button_icon_flip" value="horizontal">
        <label for="button_icon_flip_horizontal"><?php _e("Horizontal", "jozoorthemes"); ?></label>
        <input type="radio" id="button_icon_flip_vertical" name="button_icon_flip" value="vertical">
        <label for="button_icon_flip_vertical"><?php _e("Vertical", "jozoorthemes"); ?></label>
      </div>
    </div><!-- /option -->
      
    <div id="button_icon_rotate" class="option">
      <div class="label"><label><?php _e("Rotate Icon", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <input type="radio" id="button_icon_rotate_no" name="button_icon_rotate" value="no" checked="checked">
        <label for="button_icon_rotate_no"><?php _e("No", "jozoorthemes"); ?></label>
        <input type="radio" id="button_icon_rotate_90" name="button_icon_rotate" value="90">
        <label for="button_icon_rotate_90">90</label>
        <input type="radio" id="button_icon_rotate_180" name="button_icon_rotate" value="180">
        <label for="button_icon_rotate_180">180</label>
        <input type="radio" id="button_icon_rotate_270" name="button_icon_rotate" value="270">
        <label for="button_icon_rotate_270">270</label>
      </div>
    </div><!-- /option -->
      
    <div class="option">
      <div class="label padding-top"><label for="button_link_url"><?php _e("Link URL", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <input type="text" id="button_link_url" name="button_link_url" value="" placeholder="EX : http://www.website.com">
      </div>
    </div><!-- /option -->
      
    <div id="button_link_target" class="option">
      <div class="label"><label><?php _e("Link Target", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <input type="radio" id="button_link_target_out" name="button_link_target" value="out" checked="checked">
        <label for="button_link_target_out"><?php _e("New Tab", "jozoorthemes"); ?></label>
        <input type="radio" id="button_link_target_in" name="button_link_target" value="in">
        <label for="button_link_target_in"><?php _e("Self Tab", "jozoorthemes"); ?></label>
      </div>
    </div><!-- /option -->
      
    <div id="button_size" class="option">
      <div class="label"><label><?php _e("Size", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <input type="radio" id="button_size_small" name="button_size" value="small" checked="checked">
        <label for="button_size_small"><?php _e("Small", "jozoorthemes"); ?></label>
        <input type="radio" id="button_size_medium" name="button_size" value="medium">
        <label for="button_size_medium"><?php _e("Medium", "jozoorthemes"); ?></label>
        <input type="radio" id="button_size_large" name="button_size" value="large">
        <label for="button_size_large"><?php _e("Large", "jozoorthemes"); ?></label>
      </div>
    </div><!-- /option -->
      
    <div id="button_color" class="option">
      <div class="label"><label><?php _e("BG Color", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <input type="radio" id="button_color_color" name="button_color" value="color" checked="checked">
        <label for="button_color_color"><?php _e("Color", "jozoorthemes"); ?></label>
        <input type="radio" id="button_color_black" name="button_color" value="black">
        <label for="button_color_black"><?php _e("Black", "jozoorthemes"); ?></label>
        <input type="radio" id="button_color_normal" name="button_color" value="normal">
        <label for="button_color_normal"><?php _e("Normal", "jozoorthemes"); ?></label>
      </div>
    </div><!-- /option -->
      
    <div class="option">
      <div class="label"><label for="animate_button"><?php _e("Animate Button", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <input type="checkbox" id="animate_button" name="animate_button">
        <small><?php _e("yes add CSS animation", "jozoorthemes"); ?></small>
      </div>
      </div><!-- /option -->
      
      <div class="option" id="animate_type_content_button">
      <div class="label padding-top"><label for="animate_type_button"><?php _e("Animate type", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <select class="animation" id="animate_type_button" name="animate_type_button">
        <?php
        foreach ($jozoor_shortcode_animation_type_list as $animation_name) {
        echo '<option value="'.$animation_name.'">'. $animation_name .'</option>';  
        }
        ?>
        </select>
        <small><a href="http://daneden.github.io/animate.css/" target="_blank"><?php _e("See all animation types in active", "jozoorthemes"); ?></a></small>
      </div>
      </div><!-- /option -->
      
      <div class="option" id="animate_duration_content_button">
      <div class="label padding-top"><label for="animate_duration_button"><?php _e("Animate Duration", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <input type="text" id="animate_duration_button" name="animate_duration_button" value="" placeholder="EX : 2">
        <small class="margin-top"><?php _e("Change the animation duration, default is 2 (seconds)", "jozoorthemes"); ?></small>
      </div>
      </div><!-- /option -->
      
      <div class="option" id="animate_delay_content_button">
      <div class="label padding-top"><label for="animate_delay_button"><?php _e("Animate Delay", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <input type="text" id="animate_delay_button" name="animate_delay_button" value="" placeholder="EX : 0">
        <small class="margin-top"><?php _e("Delay before the animation starts, default is 0 (seconds)", "jozoorthemes"); ?></small>
      </div>
      </div><!-- /option -->
      
      <div class="option" id="animate_iteration_content_button">
      <div class="label padding-top"><label for="animate_iteration_button"><?php _e("Animate Iteration", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <input type="text" id="animate_iteration_button" name="animate_iteration_button" value="" placeholder="EX : 0">
        <small class="margin-top"><?php _e("Number of times the animation is repeated, default is 0 (times)", "jozoorthemes"); ?></small>
      </div>
      </div><!-- /option -->
      
  </div><!-- /button -->
    
  <div id="highlight" class="content">
      
    <div class="option">
      <div class="label"><label for="main_content"><?php _e("Content", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <textarea id="main_content" name="main_content_highlight"></textarea>
      </div>
    </div><!-- /option -->
      
    <div id="highlight_color" class="option">
      <div class="label"><label><?php _e("BG Color", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <input type="radio" id="highlight_color_color" name="highlight_color" value="color" checked="checked">
        <label for="highlight_color_color"><?php _e("Color", "jozoorthemes"); ?></label>
        <input type="radio" id="highlight_color_black" name="highlight_color" value="black">
        <label for="highlight_color_black"><?php _e("Black", "jozoorthemes"); ?></label>
      </div>
    </div><!-- /option -->
      
  </div><!-- /highlight -->
    
  <div id="dropcaps" class="content">
      
    <div class="option">
      <div class="label"><label for="main_content"><?php _e("Content", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <textarea id="main_content" name="main_content_dropcaps"></textarea>
      </div>
    </div><!-- /option -->
      
    <div id="dropcaps_color" class="option">
      <div class="label"><label><?php _e("BG Color", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <input type="radio" id="dropcaps_color_color" name="dropcaps_color" value="color" checked="checked">
        <label for="dropcaps_color_color"><?php _e("Color", "jozoorthemes"); ?></label>
        <input type="radio" id="dropcaps_color_black" name="dropcaps_color" value="black">
        <label for="dropcaps_color_black"><?php _e("Black", "jozoorthemes"); ?></label>
      </div>
    </div><!-- /option -->
      
  </div><!-- /dropcaps -->
    
  <div id="tooltip" class="content">
      
    <div class="option">
      <div class="label"><label for="tooltip_content"><?php _e("Tooltip Content", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <textarea id="tooltip_content" name="tooltip_content" class="small-height"></textarea>
      </div>
    </div><!-- /option -->
      
    <div id="tooltip_color" class="option">
      <div class="label"><label><?php _e("Tooltip Color", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <input type="radio" id="tooltip_color_black" name="tooltip_color" value="black" checked="checked">
        <label for="tooltip_color_black"><?php _e("Black", "jozoorthemes"); ?></label>
        <input type="radio" id="tooltip_color_yellow" name="tooltip_color" value="yellow">
        <label for="tooltip_color_yellow"><?php _e("Yellow", "jozoorthemes"); ?></label>
        <input type="radio" id="tooltip_color_white" name="tooltip_color" value="white">
        <label for="tooltip_color_white"><?php _e("White", "jozoorthemes"); ?></label>
      </div>
    </div><!-- /option -->
      
    <div class="option">
      <div class="label"><label for="main_content"><?php _e("Main Content", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <textarea id="main_content" name="main_content_tooltip"></textarea>
      </div>
    </div><!-- /option -->
      
  </div><!-- /tooltip -->
    
  <div id="info_box" class="content">
      
    <div class="option">
      <div class="label"><label for="main_content"><?php _e("Content", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <textarea id="main_content" name="main_content_info_box"></textarea>
      </div>
    </div><!-- /option -->
      
    <div class="option">
      <div class="label"><label for="animate_info_box"><?php _e("Animate Box", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <input type="checkbox" id="animate_info_box" name="animate_info_box">
        <small><?php _e("yes add CSS animation", "jozoorthemes"); ?></small>
      </div>
      </div><!-- /option -->
      
      <div class="option" id="animate_type_content_info_box">
      <div class="label padding-top"><label for="animate_type_info_box"><?php _e("Animate type", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <select class="animation" id="animate_type_info_box" name="animate_type_info_box">
        <?php
        foreach ($jozoor_shortcode_animation_type_list as $animation_name) {
        echo '<option value="'.$animation_name.'">'. $animation_name .'</option>';  
        }
        ?>
        </select>
        <small><a href="http://daneden.github.io/animate.css/" target="_blank"><?php _e("See all animation types in active", "jozoorthemes"); ?></a></small>
      </div>
      </div><!-- /option -->
      
      <div class="option" id="animate_duration_content_info_box">
      <div class="label padding-top"><label for="animate_duration_info_box"><?php _e("Animate Duration", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <input type="text" id="animate_duration_info_box" name="animate_duration_info_box" value="" placeholder="EX : 2">
        <small class="margin-top"><?php _e("Change the animation duration, default is 2 (seconds)", "jozoorthemes"); ?></small>
      </div>
      </div><!-- /option -->
      
      <div class="option" id="animate_delay_content_info_box">
      <div class="label padding-top"><label for="animate_delay_info_box"><?php _e("Animate Delay", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <input type="text" id="animate_delay_info_box" name="animate_delay_info_box" value="" placeholder="EX : 0">
        <small class="margin-top"><?php _e("Delay before the animation starts, default is 0 (seconds)", "jozoorthemes"); ?></small>
      </div>
      </div><!-- /option -->
      
      <div class="option" id="animate_iteration_content_info_box">
      <div class="label padding-top"><label for="animate_iteration_info_box"><?php _e("Animate Iteration", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <input type="text" id="animate_iteration_info_box" name="animate_iteration_info_box" value="" placeholder="EX : 0">
        <small class="margin-top"><?php _e("Number of times the animation is repeated, default is 0 (times)", "jozoorthemes"); ?></small>
      </div>
      </div><!-- /option -->
      
  </div><!-- /info box -->
    
  <div id="info_box_big" class="content">
      
    <div class="option">
      <div class="label padding-top"><label for="info_box_big_large_text"><?php _e("Large Text", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <input type="text" id="info_box_big_large_text" name="info_box_big_large_text" value="">
      </div>
    </div><!-- /option -->
      
    <div class="option">
      <div class="label padding-top"><label for="info_box_big_small_text"><?php _e("Small Text", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <input type="text" id="info_box_big_small_text" name="info_box_big_small_text" value="">
      </div>
    </div><!-- /option -->
      
    <div class="option">
      <div class="label padding-top"><label for="info_box_big_button_text"><?php _e("Button Text", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <input type="text" id="info_box_big_button_text" name="info_box_big_button_text" value="">
      </div>
    </div><!-- /option -->
      
    <div class="option">
      <div class="label padding-top"><label for="info_box_big_button_link"><?php _e("Button Link URL", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <input type="text" id="info_box_big_button_link" name="info_box_big_button_link" value="" placeholder="EX : http://www.website.com">
      </div>
    </div><!-- /option -->
      
    <div id="info_box_big_button_link_target" class="option">
      <div class="label"><label><?php _e("Link Target", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <input type="radio" id="info_box_big_button_link_target_out" name="info_box_big_button_link_target" value="out" checked="checked">
        <label for="info_box_big_button_link_target_out"><?php _e("New Tab", "jozoorthemes"); ?></label>
        <input type="radio" id="info_box_big_button_link_target_in" name="info_box_big_button_link_target" value="in">
        <label for="info_box_big_button_link_target_in"><?php _e("Self Tab", "jozoorthemes"); ?></label>
      </div>
    </div><!-- /option -->
      
    <div class="option">
      <div class="label"><label for="animate_info_box_big"><?php _e("Animate Box", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <input type="checkbox" id="animate_info_box_big" name="animate_info_box_big">
        <small><?php _e("yes add CSS animation", "jozoorthemes"); ?></small>
      </div>
      </div><!-- /option -->
      
      <div class="option" id="animate_type_content_info_box_big">
      <div class="label padding-top"><label for="animate_type_info_box_big"><?php _e("Animate type", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <select class="animation" id="animate_type_info_box_big" name="animate_type_info_box_big">
        <?php
        foreach ($jozoor_shortcode_animation_type_list as $animation_name) {
        echo '<option value="'.$animation_name.'">'. $animation_name .'</option>';  
        }
        ?>
        </select>
        <small><a href="http://daneden.github.io/animate.css/" target="_blank"><?php _e("See all animation types in active", "jozoorthemes"); ?></a></small>
      </div>
      </div><!-- /option -->
      
      <div class="option" id="animate_duration_content_info_box_big">
      <div class="label padding-top"><label for="animate_duration_info_box_big"><?php _e("Animate Duration", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <input type="text" id="animate_duration_info_box_big" name="animate_duration_info_box_big" value="" placeholder="EX : 2">
        <small class="margin-top"><?php _e("Change the animation duration, default is 2 (seconds)", "jozoorthemes"); ?></small>
      </div>
      </div><!-- /option -->
      
      <div class="option" id="animate_delay_content_info_box_big">
      <div class="label padding-top"><label for="animate_delay_info_box_big"><?php _e("Animate Delay", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <input type="text" id="animate_delay_info_box_big" name="animate_delay_info_box_big" value="" placeholder="EX : 0">
        <small class="margin-top"><?php _e("Delay before the animation starts, default is 0 (seconds)", "jozoorthemes"); ?></small>
      </div>
      </div><!-- /option -->
      
      <div class="option" id="animate_iteration_content_info_box_big">
      <div class="label padding-top"><label for="animate_iteration_info_box_big"><?php _e("Animate Iteration", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <input type="text" id="animate_iteration_info_box_big" name="animate_iteration_info_box_big" value="" placeholder="EX : 0">
        <small class="margin-top"><?php _e("Number of times the animation is repeated, default is 0 (times)", "jozoorthemes"); ?></small>
      </div>
      </div><!-- /option -->
      
  </div><!-- /info box big -->
    
  <div id="divider" class="content">
      
    <div class="option">
      <div class="label"><label for="divider_hidden_line"><?php _e("Hidden Line", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <input type="checkbox" id="divider_hidden_line" name="divider_hidden_line">
        <small><?php _e("check here to hidden divider border line", "jozoorthemes"); ?></small>
      </div>
    </div><!-- /option -->
      
  </div><!-- /divider -->
    
  <div id="list" class="content">
      
    <div id="choose_list_style" class="option">
      <div class="label"><label><?php _e("Choose Style", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <div class="label-group">
          <input type="radio" id="choose_list_style_square" name="choose_list_style" value="square" checked="checked">
          <label for="choose_list_style_square"><i class="icon-stop s-7"></i> <?php _e("List item", "jozoorthemes"); ?></label>
        </div>
        <div class="label-group">
          <input type="radio" id="choose_list_style_check" name="choose_list_style" value="check">
          <label for="choose_list_style_check"><i class="icon-ok s-11"></i><?php _e("List item", "jozoorthemes"); ?></label>
        </div>
        <div class="label-group">
          <input type="radio" id="choose_list_style_plus" name="choose_list_style" value="plus">
          <label for="choose_list_style_plus"><i class="icon-plus s-10"></i> <?php _e("List item", "jozoorthemes"); ?></label>
        </div>
        <div class="label-group">
          <input type="radio" id="choose_list_style_arrow" name="choose_list_style" value="arrow">
          <label for="choose_list_style_arrow"><i class="icon-chevron-right s-10"></i> <?php _e("List item", "jozoorthemes"); ?></label>
        </div>
        <div class="label-group">
          <input type="radio" id="choose_list_style_cross" name="choose_list_style" value="cross">
          <label for="choose_list_style_cross"><i class="icon-remove s-9"></i> <?php _e("List item", "jozoorthemes"); ?></label>
        </div>
        <div class="label-group">
          <input type="radio" id="choose_list_style_star" name="choose_list_style" value="star">
          <label for="choose_list_style_star"><i class="icon-star s-9"></i> <?php _e("List item", "jozoorthemes"); ?></label>
        </div>
        <div class="label-group">
          <input type="radio" id="choose_list_style_minus" name="choose_list_style" value="minus">
          <label for="choose_list_style_minus"><i class="icon-minus s-9"></i> <?php _e("List item", "jozoorthemes"); ?></label>
        </div>
        <div class="label-group">
          <input type="radio" id="choose_list_style_heart" name="choose_list_style" value="heart">
          <label for="choose_list_style_heart"><i class="icon-heart s-8"></i> <?php _e("List item", "jozoorthemes"); ?></label>
        </div>
        <div class="label-group">
          <input type="radio" id="choose_list_style_circle" name="choose_list_style" value="circle">
          <label for="choose_list_style_circle"><i class="icon-circle s-8"></i> <?php _e("List item", "jozoorthemes"); ?></label>
        </div>
        <div class="label-group">
          <input type="radio" id="choose_list_style_asterisk" name="choose_list_style" value="asterisk">
          <label for="choose_list_style_asterisk"><i class="icon-asterisk s-9"></i> <?php _e("List item", "jozoorthemes"); ?></label>
        </div>
      </div>
    </div><!-- /option -->
      
    <div id="list_style_color" class="option">
      <div class="label"><label><?php _e("List Icon Color", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <input type="radio" id="list_style_color_normal" name="list_style_color" value="normal" checked="checked">
        <label for="list_style_color_normal"><?php _e("Normal", "jozoorthemes"); ?></label>
        <input type="radio" id="list_style_color_color" name="list_style_color" value="color">
        <label for="list_style_color_color"><?php _e("Color", "jozoorthemes"); ?></label>
      </div>
    </div><!-- /option -->
      
    <div class="option">
      <div class="label"><label for="animate_list"><?php _e("Animate List", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <input type="checkbox" id="animate_list" name="animate_list">
        <small><?php _e("yes add CSS animation", "jozoorthemes"); ?></small>
      </div>
      </div><!-- /option -->
      
      <div class="option" id="animate_type_content_list">
      <div class="label padding-top"><label for="animate_type_list"><?php _e("Animate type", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <select class="animation" id="animate_type_list" name="animate_type_list">
        <?php
        foreach ($jozoor_shortcode_animation_type_list as $animation_name) {
        echo '<option value="'.$animation_name.'">'. $animation_name .'</option>';  
        }
        ?>
        </select>
        <small><a href="http://daneden.github.io/animate.css/" target="_blank"><?php _e("See all animation types in active", "jozoorthemes"); ?></a></small>
      </div>
      </div><!-- /option -->
      
      <div class="option" id="animate_duration_content_list">
      <div class="label padding-top"><label for="animate_duration_list"><?php _e("Animate Duration", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <input type="text" id="animate_duration_list" name="animate_duration_list" value="" placeholder="EX : 2">
        <small class="margin-top"><?php _e("Change the animation duration, default is 2 (seconds)", "jozoorthemes"); ?></small>
      </div>
      </div><!-- /option -->
      
      <div class="option" id="animate_delay_content_list">
      <div class="label padding-top"><label for="animate_delay_list"><?php _e("Animate Delay", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <input type="text" id="animate_delay_list" name="animate_delay_list" value="" placeholder="EX : 0">
        <small class="margin-top"><?php _e("Delay before the animation starts, default is 0 (seconds)", "jozoorthemes"); ?></small>
      </div>
      </div><!-- /option -->
      
      <div class="option" id="animate_iteration_content_list">
      <div class="label padding-top"><label for="animate_iteration_list"><?php _e("Animate Iteration", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <input type="text" id="animate_iteration_list" name="animate_iteration_list" value="" placeholder="EX : 0">
        <small class="margin-top"><?php _e("Number of times the animation is repeated, default is 0 (times)", "jozoorthemes"); ?></small>
      </div>
      </div><!-- /option -->
      
  </div><!-- /list -->
    
  <div id="alert_box" class="content">
      
    <div class="option">
      <div class="label"><label for="main_content"><?php _e("Content", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <textarea id="main_content" name="main_content_alert_box" class="small-height"></textarea>
      </div>
    </div><!-- /option -->
      
    <div id="choose_alert_type" class="option">
      <div class="label"><label><?php _e("Alert Type", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <div class="label-group">
          <input type="radio" id="choose_alert_type_warning" name="choose_alert_type" value="warning" checked="checked">
          <label for="choose_alert_type_warning"><i class="icon-warning-sign s-15"></i> <?php _e("Warning Message", "jozoorthemes"); ?></label>
        </div>
        <div class="label-group">
          <input type="radio" id="choose_alert_type_success" name="choose_alert_type" value="success">
          <label for="choose_alert_type_success"><i class="icon-check s-15"></i> <?php _e("Success Message", "jozoorthemes"); ?></label>
        </div>
        <div class="label-group">
          <input type="radio" id="choose_alert_type_info" name="choose_alert_type" value="info">
          <label for="choose_alert_type_info"><i class="icon-info-sign s-15"></i> <?php _e("Info Message", "jozoorthemes"); ?></label>
        </div>
        <div class="label-group">
          <input type="radio" id="choose_alert_type_notice" name="choose_alert_type" value="notice">
          <label for="choose_alert_type_notice"><i class="icon-exclamation-sign s-15"></i> <?php _e("Notice Message", "jozoorthemes"); ?></label>
        </div>
      </div>
    </div><!-- /option -->
      
    <div class="option">
      <div class="label"><label for="alert_box_close"><?php _e("Allow Close it", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <input type="checkbox" id="alert_box_close" name="alert_box_close">
        <small><?php _e("Yes user can close it", "jozoorthemes"); ?></small>
      </div>
    </div><!-- /option -->
      
    <div class="option">
      <div class="label"><label for="animate_alert_box"><?php _e("Animate Box", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <input type="checkbox" id="animate_alert_box" name="animate_alert_box">
        <small><?php _e("yes add CSS animation", "jozoorthemes"); ?></small>
      </div>
      </div><!-- /option -->
      
      <div class="option" id="animate_type_content_alert_box">
      <div class="label padding-top"><label for="animate_type_alert_box"><?php _e("Animate type", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <select class="animation" id="animate_type_alert_box" name="animate_type_alert_box">
        <?php
        foreach ($jozoor_shortcode_animation_type_list as $animation_name) {
        echo '<option value="'.$animation_name.'">'. $animation_name .'</option>';  
        }
        ?>
        </select>
        <small><a href="http://daneden.github.io/animate.css/" target="_blank"><?php _e("See all animation types in active", "jozoorthemes"); ?></a></small>
      </div>
      </div><!-- /option -->
      
      <div class="option" id="animate_duration_content_alert_box">
      <div class="label padding-top"><label for="animate_duration_alert_box"><?php _e("Animate Duration", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <input type="text" id="animate_duration_alert_box" name="animate_duration_alert_box" value="" placeholder="EX : 2">
        <small class="margin-top"><?php _e("Change the animation duration, default is 2 (seconds)", "jozoorthemes"); ?></small>
      </div>
      </div><!-- /option -->
      
      <div class="option" id="animate_delay_content_alert_box">
      <div class="label padding-top"><label for="animate_delay_alert_box"><?php _e("Animate Delay", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <input type="text" id="animate_delay_alert_box" name="animate_delay_alert_box" value="" placeholder="EX : 0">
        <small class="margin-top"><?php _e("Delay before the animation starts, default is 0 (seconds)", "jozoorthemes"); ?></small>
      </div>
      </div><!-- /option -->
      
      <div class="option" id="animate_iteration_content_alert_box">
      <div class="label padding-top"><label for="animate_iteration_alert_box"><?php _e("Animate Iteration", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <input type="text" id="animate_iteration_alert_box" name="animate_iteration_alert_box" value="" placeholder="EX : 0">
        <small class="margin-top"><?php _e("Number of times the animation is repeated, default is 0 (times)", "jozoorthemes"); ?></small>
      </div>
      </div><!-- /option -->
      
  </div><!-- /alert box -->
    
  <div id="progress_bar" class="content">
      
    <div class="group_container">
      
    <div class="item-group">
    <div class="option">
      <div class="label padding-top"><label for="bar_title_1"><?php _e("Title", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <input type="text" id="bar_title_1" name="bar_title_1" value="">
      </div>
    </div><!-- /option -->
      
    <div class="option last">
      <div class="label padding-top"><label for="bar_percent_1"><?php _e("Bar Percent", "jozoorthemes"); ?> % : </label></div>
      <div class="option-content">
        <input type="text" id="bar_percent_1" name="bar_percent_1" value="" placeholder="EX: 50">
      </div>
    </div><!-- /option -->
    </div><!--/item-group -->
        
    </div><!-- /group container -->
      
    <a id="add-new-group-progress_bar" class="add-new-group"><?php _e("Add New", "jozoorthemes"); ?></a>
      
    <div class="option">
      <div class="label"><label for="animate_progress_bar"><?php _e("Animate Bars", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <input type="checkbox" id="animate_progress_bar" name="animate_progress_bar">
        <small><?php _e("yes add CSS animation", "jozoorthemes"); ?></small>
      </div>
      </div><!-- /option -->
      
      <div class="option" id="animate_type_content_progress_bar">
      <div class="label padding-top"><label for="animate_type_progress_bar"><?php _e("Animate type", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <select class="animation" id="animate_type_progress_bar" name="animate_type_progress_bar">
        <?php
        foreach ($jozoor_shortcode_animation_type_list as $animation_name) {
        echo '<option value="'.$animation_name.'">'. $animation_name .'</option>';  
        }
        ?>
        </select>
        <small><a href="http://daneden.github.io/animate.css/" target="_blank"><?php _e("See all animation types in active", "jozoorthemes"); ?></a></small>
      </div>
      </div><!-- /option -->
      
      <div class="option" id="animate_duration_content_progress_bar">
      <div class="label padding-top"><label for="animate_duration_progress_bar"><?php _e("Animate Duration", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <input type="text" id="animate_duration_progress_bar" name="animate_duration_progress_bar" value="" placeholder="EX : 2">
        <small class="margin-top"><?php _e("Change the animation duration, default is 2 (seconds)", "jozoorthemes"); ?></small>
      </div>
      </div><!-- /option -->
      
      <div class="option" id="animate_delay_content_progress_bar">
      <div class="label padding-top"><label for="animate_delay_progress_bar"><?php _e("Animate Delay", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <input type="text" id="animate_delay_progress_bar" name="animate_delay_progress_bar" value="" placeholder="EX : 0">
        <small class="margin-top"><?php _e("Delay before the animation starts, default is 0 (seconds)", "jozoorthemes"); ?></small>
      </div>
      </div><!-- /option -->
      
      <div class="option" id="animate_iteration_content_progress_bar">
      <div class="label padding-top"><label for="animate_iteration_progress_bar"><?php _e("Animate Iteration", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <input type="text" id="animate_iteration_progress_bar" name="animate_iteration_progress_bar" value="" placeholder="EX : 0">
        <small class="margin-top"><?php _e("Number of times the animation is repeated, default is 0 (times)", "jozoorthemes"); ?></small>
      </div>
      </div><!-- /option -->
      
  </div><!-- /progress bar -->
    
  <div id="accordion" class="content">
      
    <div id="accordion_style" class="option">
      <div class="label"><label><?php _e("Choose Style", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <input type="radio" id="accordion_style_1" name="accordion_style" value="style1" checked="checked">
        <label for="accordion_style_1"><?php _e("Accordion 1", "jozoorthemes"); ?></label>
        <input type="radio" id="accordion_style_2" name="accordion_style" value="style2">
        <label for="accordion_style_2"><?php _e("Accordion 2", "jozoorthemes"); ?></label>
      </div>
    </div><!-- /option -->
      
    <div class="group_container">
      
    <div class="item-group">
    <div class="option">
      <div class="label padding-top"><label for="accordion_title_1"><?php _e("Title", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <input type="text" id="accordion_title_1" name="accordion_title_1" value="">
      </div>
    </div><!-- /option -->
      
    <div class="option last">
      <div class="label padding-top"><label for="accordion_content_1"><?php _e("Content", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <textarea id="accordion_content_1" name="accordion_content_1" class="small-height"></textarea>
      </div>
    </div><!-- /option -->
    </div><!--/item-group -->
        
    </div><!-- /group container -->
      
    <a id="add-new-group-accordion" class="add-new-group"><?php _e("Add New", "jozoorthemes"); ?></a>
      
    <div class="option">
      <div class="label"><label for="animate_accordion"><?php _e("Animate Items", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <input type="checkbox" id="animate_accordion" name="animate_accordion">
        <small><?php _e("yes add CSS animation", "jozoorthemes"); ?></small>
      </div>
      </div><!-- /option -->
      
      <div class="option" id="animate_type_content_accordion">
      <div class="label padding-top"><label for="animate_type_accordion"><?php _e("Animate type", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <select class="animation" id="animate_type_accordion" name="animate_type_accordion">
        <?php
        foreach ($jozoor_shortcode_animation_type_list as $animation_name) {
        echo '<option value="'.$animation_name.'">'. $animation_name .'</option>';  
        }
        ?>
        </select>
        <small><a href="http://daneden.github.io/animate.css/" target="_blank"><?php _e("See all animation types in active", "jozoorthemes"); ?></a></small>
      </div>
      </div><!-- /option -->
      
      <div class="option" id="animate_duration_content_accordion">
      <div class="label padding-top"><label for="animate_duration_accordion"><?php _e("Animate Duration", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <input type="text" id="animate_duration_accordion" name="animate_duration_accordion" value="" placeholder="EX : 2">
        <small class="margin-top"><?php _e("Change the animation duration, default is 2 (seconds)", "jozoorthemes"); ?></small>
      </div>
      </div><!-- /option -->
      
      <div class="option" id="animate_delay_content_accordion">
      <div class="label padding-top"><label for="animate_delay_accordion"><?php _e("Animate Delay", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <input type="text" id="animate_delay_accordion" name="animate_delay_accordion" value="" placeholder="EX : 0">
        <small class="margin-top"><?php _e("Delay before the animation starts, default is 0 (seconds)", "jozoorthemes"); ?></small>
      </div>
      </div><!-- /option -->
      
      <div class="option" id="animate_iteration_content_accordion">
      <div class="label padding-top"><label for="animate_iteration_accordion"><?php _e("Animate Iteration", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <input type="text" id="animate_iteration_accordion" name="animate_iteration_accordion" value="" placeholder="EX : 0">
        <small class="margin-top"><?php _e("Number of times the animation is repeated, default is 0 (times)", "jozoorthemes"); ?></small>
      </div>
      </div><!-- /option -->
      
  </div><!-- /accordion -->
    
  <div id="toggle" class="content">
      
    <div class="group_container">
      
    <div class="item-group">
    <div class="option">
      <div class="label padding-top"><label for="toggle_title_1"><?php _e("Title", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <input type="text" id="toggle_title_1" name="toggle_title_1" value="">
      </div>
    </div><!-- /option -->
        
    <div id="toggle_color_1" class="option">
      <div class="label"><label><?php _e("BG Color", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <input type="radio" id="toggle_color_color_1" name="toggle_color_1" value="color" checked="checked">
        <label for="toggle_color_color_1"><?php _e("Color", "jozoorthemes"); ?></label>
        <input type="radio" id="toggle_color_black_1" name="toggle_color_1" value="black">
        <label for="toggle_color_black_1"><?php _e("Black", "jozoorthemes"); ?></label>
        <input type="radio" id="toggle_color_gray_1" name="toggle_color_1" value="gray">
        <label for="toggle_color_gray_1"><?php _e("Gray", "jozoorthemes"); ?></label>
        <input type="radio" id="toggle_color_transparent_1" name="toggle_color_1" value="transparent">
        <label for="toggle_color_transparent_1"><?php _e("Transparent", "jozoorthemes"); ?></label>
      </div>
    </div><!-- /option -->
      
    <div class="option last">
      <div class="label padding-top"><label for="toggle_content_1"><?php _e("Content", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <textarea id="toggle_content_1" name="toggle_content_1" class="small-height"></textarea>
      </div>
    </div><!-- /option -->
    </div><!--/item-group -->
        
    </div><!-- /group container -->
      
    <a id="add-new-group-toggle" class="add-new-group"><?php _e("Add New", "jozoorthemes"); ?></a>
      
    <div class="option">
      <div class="label"><label for="animate_toggle"><?php _e("Animate Items", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <input type="checkbox" id="animate_toggle" name="animate_toggle">
        <small><?php _e("yes add CSS animation", "jozoorthemes"); ?></small>
      </div>
      </div><!-- /option -->
      
      <div class="option" id="animate_type_content_toggle">
      <div class="label padding-top"><label for="animate_type_toggle"><?php _e("Animate type", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <select class="animation" id="animate_type_toggle" name="animate_type_toggle">
        <?php
        foreach ($jozoor_shortcode_animation_type_list as $animation_name) {
        echo '<option value="'.$animation_name.'">'. $animation_name .'</option>';  
        }
        ?>
        </select>
        <small><a href="http://daneden.github.io/animate.css/" target="_blank"><?php _e("See all animation types in active", "jozoorthemes"); ?></a></small>
      </div>
      </div><!-- /option -->
      
      <div class="option" id="animate_duration_content_toggle">
      <div class="label padding-top"><label for="animate_duration_toggle"><?php _e("Animate Duration", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <input type="text" id="animate_duration_toggle" name="animate_duration_toggle" value="" placeholder="EX : 2">
        <small class="margin-top"><?php _e("Change the animation duration, default is 2 (seconds)", "jozoorthemes"); ?></small>
      </div>
      </div><!-- /option -->
      
      <div class="option" id="animate_delay_content_toggle">
      <div class="label padding-top"><label for="animate_delay_toggle"><?php _e("Animate Delay", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <input type="text" id="animate_delay_toggle" name="animate_delay_toggle" value="" placeholder="EX : 0">
        <small class="margin-top"><?php _e("Delay before the animation starts, default is 0 (seconds)", "jozoorthemes"); ?></small>
      </div>
      </div><!-- /option -->
      
      <div class="option" id="animate_iteration_content_toggle">
      <div class="label padding-top"><label for="animate_iteration_toggle"><?php _e("Animate Iteration", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <input type="text" id="animate_iteration_toggle" name="animate_iteration_toggle" value="" placeholder="EX : 0">
        <small class="margin-top"><?php _e("Number of times the animation is repeated, default is 0 (times)", "jozoorthemes"); ?></small>
      </div>
      </div><!-- /option -->
      
  </div><!-- /toggle -->
    
  <div id="tabs" class="content">
      
    <div id="tabs_style" class="option">
      <div class="label"><label><?php _e("Choose Style", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <input type="radio" id="tabs_style_h1" name="tabs_style" value="horizontal1" checked="checked">
        <label for="tabs_style_h1"><?php _e("Horizontal 1", "jozoorthemes"); ?></label>
        <input type="radio" id="tabs_style_h2" name="tabs_style" value="horizontal2">
        <label for="tabs_style_h2"><?php _e("Horizontal 2", "jozoorthemes"); ?></label>
        <input type="radio" id="tabs_style_v1" name="tabs_style" value="vertical1">
        <label for="tabs_style_v1"><?php _e("Vertical 1", "jozoorthemes"); ?></label>
        <input type="radio" id="tabs_style_v2" name="tabs_style" value="vertical2">
        <label for="tabs_style_v2"><?php _e("Vertical 2", "jozoorthemes"); ?></label>
      </div>
    </div><!-- /option -->
      
    <div class="group_container">
      
    <div class="item-group">
    <div class="option">
      <div class="label padding-top"><label for="tabs_title_1"><?php _e("Title", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <input type="text" id="tabs_title_1" name="tabs_title_1" value="">
      </div>
    </div><!-- /option -->
      
    <div class="option last">
      <div class="label padding-top"><label for="tabs_content_1"><?php _e("Content", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <textarea id="tabs_content_1" name="tabs_content_1" class="small-height"></textarea>
      </div>
    </div><!-- /option -->
    </div><!--/item-group -->
        
    </div><!-- /group container -->
      
    <a id="add-new-group-tabs" class="add-new-group"><?php _e("Add New", "jozoorthemes"); ?></a>
      
    <div class="option">
      <div class="label"><label for="animate_tabs"><?php _e("Animate Tabs", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <input type="checkbox" id="animate_tabs" name="animate_tabs">
        <small><?php _e("yes add CSS animation", "jozoorthemes"); ?></small>
      </div>
      </div><!-- /option -->
      
      <div class="option" id="animate_type_content_tabs">
      <div class="label padding-top"><label for="animate_type_tabs"><?php _e("Animate type", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <select class="animation" id="animate_type_tabs" name="animate_type_tabs">
        <?php
        foreach ($jozoor_shortcode_animation_type_list as $animation_name) {
        echo '<option value="'.$animation_name.'">'. $animation_name .'</option>';  
        }
        ?>
        </select>
        <small><a href="http://daneden.github.io/animate.css/" target="_blank"><?php _e("See all animation types in active", "jozoorthemes"); ?></a></small>
      </div>
      </div><!-- /option -->
      
      <div class="option" id="animate_duration_content_tabs">
      <div class="label padding-top"><label for="animate_duration_tabs"><?php _e("Animate Duration", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <input type="text" id="animate_duration_tabs" name="animate_duration_tabs" value="" placeholder="EX : 2">
        <small class="margin-top"><?php _e("Change the animation duration, default is 2 (seconds)", "jozoorthemes"); ?></small>
      </div>
      </div><!-- /option -->
      
      <div class="option" id="animate_delay_content_tabs">
      <div class="label padding-top"><label for="animate_delay_tabs"><?php _e("Animate Delay", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <input type="text" id="animate_delay_tabs" name="animate_delay_tabs" value="" placeholder="EX : 0">
        <small class="margin-top"><?php _e("Delay before the animation starts, default is 0 (seconds)", "jozoorthemes"); ?></small>
      </div>
      </div><!-- /option -->
      
      <div class="option" id="animate_iteration_content_tabs">
      <div class="label padding-top"><label for="animate_iteration_tabs"><?php _e("Animate Iteration", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <input type="text" id="animate_iteration_tabs" name="animate_iteration_tabs" value="" placeholder="EX : 0">
        <small class="margin-top"><?php _e("Number of times the animation is repeated, default is 0 (times)", "jozoorthemes"); ?></small>
      </div>
      </div><!-- /option -->
      
  </div><!-- /tabs -->
    
  <div id="pricing_table" class="content">
      
    <div id="pricing_table_columns" class="option">
      <div class="label"><label><?php _e("Choose Columns", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <input type="radio" id="pricing_table_columns2" name="pricing_table_columns" value="cols2">
        <label for="pricing_table_columns2"><?php _e("Two 2", "jozoorthemes"); ?></label>
        <input type="radio" id="pricing_table_columns3" name="pricing_table_columns" value="cols3">
        <label for="pricing_table_columns3"><?php _e("Three 3", "jozoorthemes"); ?></label>
        <input type="radio" id="pricing_table_columns4" name="pricing_table_columns" value="cols4">
        <label for="pricing_table_columns4"><?php _e("Four 4", "jozoorthemes"); ?></label>
      </div>
    </div><!-- /option -->
      
    <div class="group_container"></div><!-- /group container -->
      
    <div class="option">
      <div class="label"><label for="animate_pricing_table"><?php _e("Animate Tables", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <input type="checkbox" id="animate_pricing_table" name="animate_pricing_table">
        <small><?php _e("yes add CSS animation", "jozoorthemes"); ?></small>
      </div>
      </div><!-- /option -->
      
      <div class="option" id="animate_type_content_pricing_table">
      <div class="label padding-top"><label for="animate_type_pricing_table"><?php _e("Animate type", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <select class="animation" id="animate_type_pricing_table" name="animate_type_pricing_table">
        <?php
        foreach ($jozoor_shortcode_animation_type_list as $animation_name) {
        echo '<option value="'.$animation_name.'">'. $animation_name .'</option>';  
        }
        ?>
        </select>
        <small><a href="http://daneden.github.io/animate.css/" target="_blank"><?php _e("See all animation types in active", "jozoorthemes"); ?></a></small>
      </div>
      </div><!-- /option -->
      
      <div class="option" id="animate_duration_content_pricing_table">
      <div class="label padding-top"><label for="animate_duration_pricing_table"><?php _e("Animate Duration", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <input type="text" id="animate_duration_pricing_table" name="animate_duration_pricing_table" value="" placeholder="EX : 2">
        <small class="margin-top"><?php _e("Change the animation duration, default is 2 (seconds)", "jozoorthemes"); ?></small>
      </div>
      </div><!-- /option -->
      
      <div class="option" id="animate_delay_content_pricing_table">
      <div class="label padding-top"><label for="animate_delay_pricing_table"><?php _e("Animate Delay", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <input type="text" id="animate_delay_pricing_table" name="animate_delay_pricing_table" value="" placeholder="EX : 0">
        <small class="margin-top"><?php _e("Delay before the animation starts, default is 0 (seconds)", "jozoorthemes"); ?></small>
      </div>
      </div><!-- /option -->
      
      <div class="option" id="animate_iteration_content_pricing_table">
      <div class="label padding-top"><label for="animate_iteration_pricing_table"><?php _e("Animate Iteration", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <input type="text" id="animate_iteration_pricing_table" name="animate_iteration_pricing_table" value="" placeholder="EX : 0">
        <small class="margin-top"><?php _e("Number of times the animation is repeated, default is 0 (times)", "jozoorthemes"); ?></small>
      </div>
      </div><!-- /option -->
      
  </div><!-- /pricing table -->
    
  <div id="icon" class="content">
         
    <div class="option">
      <div class="label padding-top"><label for="icon_size"><?php _e("Size", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <input type="text" id="icon_size" name="icon_size" value="" placeholder="EX: 20">
        <small class="margin-top"><?php _e("default size (20) and you have size range from 10 to 100", "jozoorthemes"); ?></small>
      </div>
    </div><!-- /option -->
      
    <div id="icon_color" class="option">
      <div class="label"><label><?php _e("Color", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <input type="radio" id="icon_color_normal" name="icon_color" value="normal" checked="checked">
        <label for="icon_color_normal"><?php _e("Normal", "jozoorthemes"); ?></label>
        <input type="radio" id="icon_color_color" name="icon_color" value="color">
        <label for="icon_color_color"><?php _e("Color", "jozoorthemes"); ?></label>
        <input type="radio" id="icon_color_white" name="icon_color" value="white">
        <label for="icon_color_white"><?php _e("White", "jozoorthemes"); ?></label>
      </div>
    </div><!-- /option -->
      
    <div id="icons_list" class="option">
      <ul>
      <?php
      foreach ($jozoor_shortcode_icons_list as $icon_name) {
        echo '<li><i class="icon-'.$icon_name.'"></i></li>';  
      }
      ?>
      </ul>
    </div><!-- /option -->
      
    <div class="option">
      <div class="label"><label for="spin_icon"><?php _e("Spin Icon", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <input type="checkbox" id="spin_icon" name="spin_icon">
        <small><?php _e("yes make icon spin", "jozoorthemes"); ?></small>
      </div>
    </div><!-- /option -->
      
    <div id="icon_flip" class="option">
      <div class="label"><label><?php _e("Flip Icon", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <input type="radio" id="icon_flip_no" name="icon_flip" value="no" checked="checked">
        <label for="icon_flip_no"><?php _e("No", "jozoorthemes"); ?></label>
        <input type="radio" id="icon_flip_horizontal" name="icon_flip" value="horizontal">
        <label for="icon_flip_horizontal"><?php _e("Horizontal", "jozoorthemes"); ?></label>
        <input type="radio" id="icon_flip_vertical" name="icon_flip" value="vertical">
        <label for="icon_flip_vertical"><?php _e("Vertical", "jozoorthemes"); ?></label>
      </div>
    </div><!-- /option -->
      
    <div id="icon_rotate" class="option">
      <div class="label"><label><?php _e("Rotate Icon", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <input type="radio" id="icon_rotate_no" name="icon_rotate" value="no" checked="checked">
        <label for="icon_rotate_no"><?php _e("No", "jozoorthemes"); ?></label>
        <input type="radio" id="icon_rotate_90" name="icon_rotate" value="90">
        <label for="icon_rotate_90">90</label>
        <input type="radio" id="icon_rotate_180" name="icon_rotate" value="180">
        <label for="icon_rotate_180">180</label>
        <input type="radio" id="icon_rotate_270" name="icon_rotate" value="270">
        <label for="icon_rotate_270">270</label>
      </div>
    </div><!-- /option -->
      
  </div><!-- /icon -->
    
  <div id="social_icon" class="content">
         
    <div class="option">
      <div class="label padding-top"><label for="social_icon_size"><?php _e("Size", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <input type="text" id="social_icon_size" name="social_icon_size" value="" placeholder="EX: 20">
        <small class="margin-top"><?php _e("default size (20) and you have size range from 10 to 100", "jozoorthemes"); ?></small>
      </div>
    </div><!-- /option -->
      
    <div id="social_icon_color" class="option">
      <div class="label"><label><?php _e("Color", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <input type="radio" id="social_icon_color_normal" name="social_icon_color" value="normal" checked="checked">
        <label for="social_icon_color_normal"><?php _e("Normal", "jozoorthemes"); ?></label>
        <input type="radio" id="social_icon_color_color" name="social_icon_color" value="color">
        <label for="social_icon_color_color"><?php _e("Color", "jozoorthemes"); ?></label>
        <input type="radio" id="social_icon_color_white" name="social_icon_color" value="white">
        <label for="social_icon_color_white"><?php _e("White", "jozoorthemes"); ?></label>
      </div>
    </div><!-- /option -->
      
    <div id="social_icons_list" class="option">
      <ul>
      <?php
      foreach ($jozoor_shortcode_social_icons_list as $social_icon_name) {
        echo '<li><i class="social_icon-'.$social_icon_name.'"></i></li>';  
      }
      ?>
      </ul>
    </div><!-- /option -->
      
  </div><!-- /social icon -->
    
  <div id="icon_boxes" class="content">
      
    <div id="icon_boxes_type" class="option">
      <div class="label"><label><?php _e("Boxes Type", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <input type="radio" id="icon_boxes_type_style1" name="icon_boxes_type" value="style1" checked="checked">
        <label for="icon_boxes_type_style1"><?php _e("Style 1", "jozoorthemes"); ?></label>
        <input type="radio" id="icon_boxes_type_style2" name="icon_boxes_type" value="style2">
        <label for="icon_boxes_type_style2"><?php _e("Style 2", "jozoorthemes"); ?></label>
        <input type="radio" id="icon_boxes_type_style3" name="icon_boxes_type" value="style3">
        <label for="icon_boxes_type_style3"><?php _e("Style 3", "jozoorthemes"); ?></label>
      </div>
    </div><!-- /option -->
      
    <div id="icon_boxes_columns" class="option">
      <div class="label"><label><?php _e("Boxes Columns", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <input type="radio" id="icon_boxes_type_cols1" name="icon_boxes_columns" value="cols1">
        <label for="icon_boxes_type_cols1"><?php _e("One 1", "jozoorthemes"); ?></label>
        <input type="radio" id="icon_boxes_type_cols2" name="icon_boxes_columns" value="cols2">
        <label for="icon_boxes_type_cols2"><?php _e("Two 2", "jozoorthemes"); ?></label>
        <input type="radio" id="icon_boxes_type_cols3" name="icon_boxes_columns" value="cols3" checked="checked">
        <label for="icon_boxes_type_cols3"><?php _e("Three 3", "jozoorthemes"); ?></label>
        <input type="radio" id="icon_boxes_type_cols4" name="icon_boxes_columns" value="cols4">
        <label for="icon_boxes_type_cols4"><?php _e("Four 4", "jozoorthemes"); ?></label>
      </div>
    </div><!-- /option -->
      
    <div class="group_container">
      
    <div class="item-group">
    <div id="icon_boxes_icons_list" class="option">
      <div class="label"><label><?php _e("Choose Icon", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <ul id="icon_boxes_icon_1">
      <?php
      foreach ($jozoor_shortcode_icons_list as $icon_name) {
        echo '<li><i class="icon-'.$icon_name.'"></i></li>';  
      }
      ?>
      </ul>
      </div>
    </div><!-- /option -->
        
    <div id="icon_boxes_color_1" class="option">
      <div class="label"><label><?php _e("Icon BG", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <input type="radio" id="icon_boxes_color_normal_1" name="icon_boxes_color_1" value="normal" checked="checked">
        <label for="icon_boxes_color_normal_1"><?php _e("Normal", "jozoorthemes"); ?></label>
        <input type="radio" id="icon_boxes_color_color_1" name="icon_boxes_color_1" value="color">
        <label for="icon_boxes_color_color_1"><?php _e("Color", "jozoorthemes"); ?></label>
      </div>
    </div><!-- /option -->
    <div class="option">
      <div class="label"><label for="icon_boxes_spin_icon_1"><?php _e("Spin Icon", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <input type="checkbox" id="icon_boxes_spin_icon_1" name="icon_boxes_spin_icon_1">
        <small><?php _e("yes make icon spin", "jozoorthemes"); ?></small>
      </div>
    </div><!-- /option -->
    <div id="icon_boxes_icon_flip_1" class="option">
      <div class="label"><label><?php _e("Flip Icon", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <input type="radio" id="icon_boxes_icon_flip_no_1" name="icon_boxes_icon_flip_1" value="no" checked="checked">
        <label for="icon_boxes_icon_flip_no_1"><?php _e("No", "jozoorthemes"); ?></label>
        <input type="radio" id="icon_boxes_icon_flip_horizontal_1" name="icon_boxes_icon_flip_1" value="horizontal">
        <label for="icon_boxes_icon_flip_horizontal_1"><?php _e("Horizontal", "jozoorthemes"); ?></label>
        <input type="radio" id="icon_boxes_icon_flip_vertical_1" name="icon_boxes_icon_flip_1" value="vertical">
        <label for="icon_boxes_icon_flip_vertical_1"><?php _e("Vertical", "jozoorthemes"); ?></label>
      </div>
    </div><!-- /option -->
    <div id="icon_boxes_icon_rotate_1" class="option">
      <div class="label"><label><?php _e("Rotate Icon", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <input type="radio" id="icon_boxes_icon_rotate_no_1" name="icon_boxes_icon_rotate_1" value="no" checked="checked">
        <label for="icon_boxes_icon_rotate_no_1"><?php _e("No", "jozoorthemes"); ?></label>
        <input type="radio" id="icon_boxes_icon_rotate_90_1" name="icon_boxes_icon_rotate_1" value="90">
        <label for="icon_boxes_icon_rotate_90_1">90</label>
        <input type="radio" id="icon_boxes_icon_rotate_180_1" name="icon_boxes_icon_rotate_1" value="180">
        <label for="icon_boxes_icon_rotate_180_1">180</label>
        <input type="radio" id="icon_boxes_icon_rotate_270_1" name="icon_boxes_icon_rotate_1" value="270">
        <label for="icon_boxes_icon_rotate_270_1">270</label>
      </div>
    </div><!-- /option -->
    <div class="option">
      <div class="label padding-top"><label for="icon_boxes_title_1"><?php _e("Title", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <input type="text" id="icon_boxes_title_1" name="icon_boxes_title_1" value="">
      </div>
    </div><!-- /option -->
      
    <div class="option">
      <div class="label padding-top"><label for="icon_boxes_content_1"><?php _e("Content", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <textarea id="icon_boxes_content_1" name="icon_boxes_content_1" class="small-height"></textarea>
      </div>
    </div><!-- /option -->
        
    <div class="option last">
      <div class="label padding-top"><label for="icon_boxes_url_1"><?php _e("URL", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <input type="text" id="icon_boxes_url_1" name="icon_boxes_url_1" value="" placeholder="EX: http://www.yourURL.com">
        <small class="margin-top"><?php _e("if you want box without link, let field empty", "jozoorthemes"); ?></small>
      </div>
    </div><!-- /option -->
    </div><!--/item-group -->
        
    </div><!-- /group container -->
      
    <a id="add-new-group-icon_boxes" class="add-new-group"><?php _e("Add New", "jozoorthemes"); ?></a>
      
    <div class="option">
      <div class="label"><label for="animate_icon_boxes"><?php _e("Animate Boxes", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <input type="checkbox" id="animate_icon_boxes" name="animate_icon_boxes">
        <small><?php _e("yes add CSS animation", "jozoorthemes"); ?></small>
      </div>
      </div><!-- /option -->
      
      <div class="option" id="animate_type_content_icon_boxes">
      <div class="label padding-top"><label for="animate_type_icon_boxes"><?php _e("Animate type", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <select class="animation" id="animate_type_icon_boxes" name="animate_type_icon_boxes">
        <?php
        foreach ($jozoor_shortcode_animation_type_list as $animation_name) {
        echo '<option value="'.$animation_name.'">'. $animation_name .'</option>';  
        }
        ?>
        </select>
        <small><a href="http://daneden.github.io/animate.css/" target="_blank"><?php _e("See all animation types in active", "jozoorthemes"); ?></a></small>
      </div>
      </div><!-- /option -->
      
      <div class="option" id="animate_duration_content_icon_boxes">
      <div class="label padding-top"><label for="animate_duration_icon_boxes"><?php _e("Animate Duration", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <input type="text" id="animate_duration_icon_boxes" name="animate_duration_icon_boxes" value="" placeholder="EX : 2">
        <small class="margin-top"><?php _e("Change the animation duration, default is 2 (seconds)", "jozoorthemes"); ?></small>
      </div>
      </div><!-- /option -->
      
      <div class="option" id="animate_delay_content_icon_boxes">
      <div class="label padding-top"><label for="animate_delay_icon_boxes"><?php _e("Animate Delay", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <input type="text" id="animate_delay_icon_boxes" name="animate_delay_icon_boxes" value="" placeholder="EX : 0">
        <small class="margin-top"><?php _e("Delay before the animation starts, default is 0 (seconds)", "jozoorthemes"); ?></small>
      </div>
      </div><!-- /option -->
      
      <div class="option" id="animate_iteration_content_icon_boxes">
      <div class="label padding-top"><label for="animate_iteration_icon_boxes"><?php _e("Animate Iteration", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <input type="text" id="animate_iteration_icon_boxes" name="animate_iteration_icon_boxes" value="" placeholder="EX : 0">
        <small class="margin-top"><?php _e("Number of times the animation is repeated, default is 0 (times)", "jozoorthemes"); ?></small>
      </div>
      </div><!-- /option -->
      
  </div><!-- /icon boxes -->
    
  <div id="carousel_slider" class="content">
      
    <div class="option">
      <div class="label padding-top"><label for="carousel_slider_title"><?php _e("Heading Title", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <input type="text" id="carousel_slider_title" name="carousel_slider_title" value="">
      </div>
    </div><!-- /option -->
      
    <div class="option">
      <div class="label padding-top"><label for="carousel_slider_slides_num"><?php _e("Slides Number", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <input type="text" id="carousel_slider_slides_num" name="carousel_slider_slides_num" value="" placeholder="EX : 2">
      </div>
    </div><!-- /option -->
      
    <div class="option">
      <div class="label"><label for="carousel_slider_auto"><?php _e("Auto Rotate", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <input type="checkbox" id="carousel_slider_auto" name="carousel_slider_auto">
        <small><?php _e("yes make slider working automatic", "jozoorthemes"); ?></small>
      </div>
    </div><!-- /option -->
      
    <div class="option">
      <div class="label"><label for="animate_carousel_slider"><?php _e("Animate Slider", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <input type="checkbox" id="animate_carousel_slider" name="animate_carousel_slider">
        <small><?php _e("yes add CSS animation", "jozoorthemes"); ?></small>
      </div>
      </div><!-- /option -->
      
      <div class="option" id="animate_type_content_carousel_slider">
      <div class="label padding-top"><label for="animate_type_carousel_slider"><?php _e("Animate type", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <select class="animation" id="animate_type_carousel_slider" name="animate_type_carousel_slider">
        <?php
        foreach ($jozoor_shortcode_animation_type_list as $animation_name) {
        echo '<option value="'.$animation_name.'">'. $animation_name .'</option>';  
        }
        ?>
        </select>
        <small><a href="http://daneden.github.io/animate.css/" target="_blank"><?php _e("See all animation types in active", "jozoorthemes"); ?></a></small>
      </div>
      </div><!-- /option -->
      
      <div class="option" id="animate_duration_content_carousel_slider">
      <div class="label padding-top"><label for="animate_duration_carousel_slider"><?php _e("Animate Duration", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <input type="text" id="animate_duration_carousel_slider" name="animate_duration_carousel_slider" value="" placeholder="EX : 2">
        <small class="margin-top"><?php _e("Change the animation duration, default is 2 (seconds)", "jozoorthemes"); ?></small>
      </div>
      </div><!-- /option -->
      
      <div class="option" id="animate_delay_content_carousel_slider">
      <div class="label padding-top"><label for="animate_delay_carousel_slider"><?php _e("Animate Delay", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <input type="text" id="animate_delay_carousel_slider" name="animate_delay_carousel_slider" value="" placeholder="EX : 0">
        <small class="margin-top"><?php _e("Delay before the animation starts, default is 0 (seconds)", "jozoorthemes"); ?></small>
      </div>
      </div><!-- /option -->
      
      <div class="option" id="animate_iteration_content_carousel_slider">
      <div class="label padding-top"><label for="animate_iteration_carousel_slider"><?php _e("Animate Iteration", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <input type="text" id="animate_iteration_carousel_slider" name="animate_iteration_carousel_slider" value="" placeholder="EX : 0">
        <small class="margin-top"><?php _e("Number of times the animation is repeated, default is 0 (times)", "jozoorthemes"); ?></small>
      </div>
      </div><!-- /option -->
      
  </div><!-- /carousel slider -->
    
  <div id="images_slider" class="content">
      
    <div id="images_slider_animation" class="option">
      <div class="label"><label><?php _e("Animation", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <input type="radio" id="images_slider_animation_slide" name="images_slider_animation" value="slide" checked="checked">
        <label for="images_slider_animation_slide"><?php _e("Slide", "jozoorthemes"); ?></label>
        <input type="radio" id="images_slider_animation_fade" name="images_slider_animation" value="fade">
        <label for="images_slider_animation_fade"><?php _e("Fade", "jozoorthemes"); ?></label>
      </div>
    </div><!-- /option -->
      
    <div class="option">
      <div class="label"><label for="images_slider_hidden_nav"><?php _e("Hidden Arrows", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <input type="checkbox" id="images_slider_hidden_nav" name="images_slider_hidden_nav">
        <small><?php _e("yes hidden slider navigation arrows", "jozoorthemes"); ?></small>
      </div>
    </div><!-- /option -->
      
    <div class="group_container">
      
    <div class="item-group">
        
    <div class="option">
      <div class="label padding-top"><label for="images_slider_image_1"><?php _e("Upload Image", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <input type="text" id="images_slider_image_1" name="images_slider_image_1" value="" placeholder="upload image or add url direct">
        <a id="upload-image-images_slider_1" class="upload-image">Upload</a>
        <input type="text" id="images_slider_image_alt_1" class="upload-image-alt" name="images_slider_image_alt_1" value="">
      </div>
      <div id="image_preview_1" class="image_preview">
          <img src="">
          <a id="remove-image-images_slider_1" class="remove-image"><i class="icon-remove"></i></a>
      </div>
    </div><!-- /option -->
        
    <div class="option">
      <div class="label padding-top"><label for="images_slider_link_url_1"><?php _e("Image Link", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <input type="text" id="images_slider_link_url_1" name="images_slider_link_url_1" value="" placeholder="EX : http://www.website.com">
        <small class="margin-top">- <?php _e("if you don't need link, let field empty", "jozoorthemes"); ?><br>- <?php _e("if you want to show image in popup lightbox, just copy image url from upload field and past here in this field", "jozoorthemes"); ?></small>
      </div>
    </div><!-- /option -->
      
    <div id="images_slider_link_target_1" class="option last">
      <div class="label"><label><?php _e("Link Target", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <input type="radio" id="images_slider_link_target_out_1" name="images_slider_link_target_1" value="out" checked="checked">
        <label for="images_slider_link_target_out_1"><?php _e("New Tab", "jozoorthemes"); ?></label>
        <input type="radio" id="images_slider_link_target_in_1" name="images_slider_link_target_1" value="in">
        <label for="images_slider_link_target_in_1"><?php _e("Self Tab", "jozoorthemes"); ?></label>
      </div>
    </div><!-- /option -->
        
    </div><!--/item-group -->
        
    </div><!-- /group container -->
      
    <a id="add-new-group-images_slider" class="add-new-group"><?php _e("Add New", "jozoorthemes"); ?></a>
      
    <div class="option">
      <div class="label"><label for="animate_images_slider"><?php _e("Animate Slider", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <input type="checkbox" id="animate_images_slider" name="animate_images_slider">
        <small><?php _e("yes add CSS animation", "jozoorthemes"); ?></small>
      </div>
      </div><!-- /option -->
      
      <div class="option" id="animate_type_content_images_slider">
      <div class="label padding-top"><label for="animate_type_images_slider"><?php _e("Animate type", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <select class="animation" id="animate_type_images_slider" name="animate_type_images_slider">
        <?php
        foreach ($jozoor_shortcode_animation_type_list as $animation_name) {
        echo '<option value="'.$animation_name.'">'. $animation_name .'</option>';  
        }
        ?>
        </select>
        <small><a href="http://daneden.github.io/animate.css/" target="_blank"><?php _e("See all animation types in active", "jozoorthemes"); ?></a></small>
      </div>
      </div><!-- /option -->
      
      <div class="option" id="animate_duration_content_images_slider">
      <div class="label padding-top"><label for="animate_duration_images_slider"><?php _e("Animate Duration", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <input type="text" id="animate_duration_images_slider" name="animate_duration_images_slider" value="" placeholder="EX : 2">
        <small class="margin-top"><?php _e("Change the animation duration, default is 2 (seconds)", "jozoorthemes"); ?></small>
      </div>
      </div><!-- /option -->
      
      <div class="option" id="animate_delay_content_images_slider">
      <div class="label padding-top"><label for="animate_delay_images_slider"><?php _e("Animate Delay", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <input type="text" id="animate_delay_images_slider" name="animate_delay_images_slider" value="" placeholder="EX : 0">
        <small class="margin-top"><?php _e("Delay before the animation starts, default is 0 (seconds)", "jozoorthemes"); ?></small>
      </div>
      </div><!-- /option -->
      
      <div class="option" id="animate_iteration_content_images_slider">
      <div class="label padding-top"><label for="animate_iteration_images_slider"><?php _e("Animate Iteration", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <input type="text" id="animate_iteration_images_slider" name="animate_iteration_images_slider" value="" placeholder="EX : 0">
        <small class="margin-top"><?php _e("Number of times the animation is repeated, default is 0 (times)", "jozoorthemes"); ?></small>
      </div>
      </div><!-- /option -->
      
  </div><!-- /images slider -->
    
  <div id="animate_element" class="content">
      
    <div class="option">
      <div class="label"><label for="main_content"><?php _e("Content", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <textarea id="main_content" name="main_content_animate_element"></textarea>
      </div>
    </div><!-- /option -->
      
      <div class="option">
      <div class="label padding-top"><label for="animate_type_animate_element"><?php _e("Animate type", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <select class="animation" id="animate_type_animate_element" name="animate_type_animate_element">
        <?php
        foreach ($jozoor_shortcode_animation_type_list as $animation_name) {
        echo '<option value="'.$animation_name.'">'. $animation_name .'</option>';  
        }
        ?>
        </select>
        <small><a href="http://daneden.github.io/animate.css/" target="_blank"><?php _e("See all animation types in active", "jozoorthemes"); ?></a></small>
      </div>
      </div><!-- /option -->
      
      <div class="option">
      <div class="label padding-top"><label for="animate_duration_animate_element"><?php _e("Animate Duration", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <input type="text" id="animate_duration_animate_element" name="animate_duration_animate_element" value="" placeholder="EX : 2">
        <small class="margin-top"><?php _e("Change the animation duration, default is 2 (seconds)", "jozoorthemes"); ?></small>
      </div>
      </div><!-- /option -->
      
      <div class="option">
      <div class="label padding-top"><label for="animate_delay_animate_element"><?php _e("Animate Delay", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <input type="text" id="animate_delay_animate_element" name="animate_delay_animate_element" value="" placeholder="EX : 0">
        <small class="margin-top"><?php _e("Delay before the animation starts, default is 0 (seconds)", "jozoorthemes"); ?></small>
      </div>
      </div><!-- /option -->
      
      <div class="option">
      <div class="label padding-top"><label for="animate_iteration_animate_element"><?php _e("Animate Iteration", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <input type="text" id="animate_iteration_animate_element" name="animate_iteration_animate_element" value="" placeholder="EX : 0">
        <small class="margin-top"><?php _e("Number of times the animation is repeated, default is 0 (times)", "jozoorthemes"); ?></small>
      </div>
      </div><!-- /option -->
      
  </div><!-- /animate element -->
    
  <div id="gmaps" class="content">
      
    <div class="option">
      <div class="label"><label><?php _e("Main Directions", "jozoorthemes"); ?> </label></div>
      <div class="option-content">
        <small><?php _e("define main area directions, get it by address from here : ", "jozoorthemes"); ?><a href="http://www.mapcoordinates.net" target="_blank">mapcoordinates.net</a></small>
      </div>
    </div><!-- /option -->
      
    <div class="option">
      <div class="label padding-top"><label for="gmaps_main_lat"><?php _e("Latitude (lat)", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <input type="text" id="gmaps_main_lat" name="gmaps_main_lat" value="" placeholder="EX: 51.5073346">
      </div>
    </div><!-- /option -->
      
    <div class="option">
      <div class="label padding-top"><label for="gmaps_main_lng"><?php _e("Longitude (lng)", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <input type="text" id="gmaps_main_lng" name="gmaps_main_lng" value="" placeholder="EX: -0.1276831">
      </div>
    </div><!-- /option -->
      
    <div class="option">
      <div class="label"><label for="gmaps_main_full_width_top"><?php _e("Make it in top and full width", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <input type="checkbox" id="gmaps_main_full_width_top" name="gmaps_main_full_width_top">
        <small><?php _e("yes make it, this will move map outside main content in top & down header", "jozoorthemes"); ?></small>
      </div>
      </div><!-- /option -->
      
    <div class="option">
      <div class="label"><label><?php _e("Address Markers", "jozoorthemes"); ?> </label></div>
      <div class="option-content">
        <small><?php _e("add address markers directions, get it by address from here : ", "jozoorthemes"); ?><a href="http://www.mapcoordinates.net" target="_blank">mapcoordinates.net</a></small>
      </div>
    </div><!-- /option -->
      
    <div class="group_container">
      
    <div class="item-group">
        
    <div class="option">
      <div class="label padding-top"><label for="gmaps_marker_lat_1"><?php _e("Latitude (lat)", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <input type="text" id="gmaps_marker_lat_1" name="gmaps_marker_lat_1" value="" placeholder="EX: 51.503324">
      </div>
    </div><!-- /option -->
        
    <div class="option">
      <div class="label padding-top"><label for="gmaps_marker_lng_1"><?php _e("Longitude (lng)", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <input type="text" id="gmaps_marker_lng_1" name="gmaps_marker_lng_1" value="" placeholder="EX: -0.119543">
      </div>
    </div><!-- /option -->
        
    <div class="option">
      <div class="label padding-top"><label for="gmaps_marker_title_1"><?php _e("Marker Title", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <input type="text" id="gmaps_marker_title_1" name="gmaps_marker_title_1" value="" placeholder="EX: Big Ben">
      </div>
    </div><!-- /option -->
        
    <div class="option">
      <div class="label padding-top"><label for="gmaps_marker_content_1"><?php _e("Popup Content", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <textarea id="gmaps_marker_content_1" name="gmaps_marker_content_1" class="small-height"></textarea>
      </div>
    </div><!-- /option -->
      
    </div><!--/item-group -->
        
    </div><!-- /group container -->
      
    <a id="add-new-group-gmaps" class="add-new-group"><?php _e("Add New", "jozoorthemes"); ?></a>
      
  </div><!-- /gmaps -->
    
  <div id="latest_posts" class="content">
      
    <div class="option">
      <div class="label padding-top"><label for="latest_posts_num"><?php _e("News Number", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <input type="text" id="latest_posts_num" name="latest_posts_num" value="" placeholder="EX : 3">
        <small class="margin-top"><?php _e("default number is", "jozoorthemes"); ?> : 3</small>
      </div>
    </div><!-- /option -->
      
    <div class="option">
      <div class="label"><label for="animate_latest_posts"><?php _e("Animate Section", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <input type="checkbox" id="animate_latest_posts" name="animate_latest_posts">
        <small><?php _e("yes add CSS animation", "jozoorthemes"); ?></small>
      </div>
      </div><!-- /option -->
      
      <div class="option" id="animate_type_content_latest_posts">
      <div class="label padding-top"><label for="animate_type_latest_posts"><?php _e("Animate type", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <select class="animation" id="animate_type_latest_posts" name="animate_type_latest_posts">
        <?php
        foreach ($jozoor_shortcode_animation_type_list as $animation_name) {
        echo '<option value="'.$animation_name.'">'. $animation_name .'</option>';  
        }
        ?>
        </select>
        <small><a href="http://daneden.github.io/animate.css/" target="_blank"><?php _e("See all animation types in active", "jozoorthemes"); ?></a></small>
      </div>
      </div><!-- /option -->
      
      <div class="option" id="animate_duration_content_latest_posts">
      <div class="label padding-top"><label for="animate_duration_latest_posts"><?php _e("Animate Duration", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <input type="text" id="animate_duration_latest_posts" name="animate_duration_latest_posts" value="" placeholder="EX : 2">
        <small class="margin-top"><?php _e("Change the animation duration, default is 2 (seconds)", "jozoorthemes"); ?></small>
      </div>
      </div><!-- /option -->
      
      <div class="option" id="animate_delay_content_latest_posts">
      <div class="label padding-top"><label for="animate_delay_latest_posts"><?php _e("Animate Delay", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <input type="text" id="animate_delay_latest_posts" name="animate_delay_latest_posts" value="" placeholder="EX : 0">
        <small class="margin-top"><?php _e("Delay before the animation starts, default is 0 (seconds)", "jozoorthemes"); ?></small>
      </div>
      </div><!-- /option -->
      
      <div class="option" id="animate_iteration_content_latest_posts">
      <div class="label padding-top"><label for="animate_iteration_latest_posts"><?php _e("Animate Iteration", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <input type="text" id="animate_iteration_latest_posts" name="animate_iteration_latest_posts" value="" placeholder="EX : 0">
        <small class="margin-top"><?php _e("Number of times the animation is repeated, default is 0 (times)", "jozoorthemes"); ?></small>
      </div>
      </div><!-- /option -->
      
  </div><!-- /latest posts -->
    
  <div id="recent_projects" class="content">
      
    <div class="option">
      <div class="label padding-top"><label for="recent_projects_num"><?php _e("Projects Number", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <input type="text" id="recent_projects_num" name="recent_projects_num" value="" placeholder="EX : 3">
        <small class="margin-top"><?php _e("default number is", "jozoorthemes"); ?> : 3</small>
      </div>
    </div><!-- /option -->
      
    <div id="recent_projects_cols" class="option last">
      <div class="label"><label><?php _e("Choose Columns", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <input type="radio" id="recent_projects_cols2" name="recent_projects_cols" value="cols2">
        <label for="recent_projects_cols2"><?php _e("Two 2", "jozoorthemes"); ?></label>
        <input type="radio" id="recent_projects_cols3" name="recent_projects_cols" value="cols3" checked="checked">
        <label for="recent_projects_cols3"><?php _e("Three 3", "jozoorthemes"); ?></label>
        <input type="radio" id="recent_projects_cols4" name="recent_projects_cols" value="cols4">
        <label for="recent_projects_cols4"><?php _e("Four 4", "jozoorthemes"); ?></label>
      </div>
    </div><!-- /option -->
      
    <div class="option">
      <div class="label"><label for="animate_recent_projects"><?php _e("Animate Section", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <input type="checkbox" id="animate_recent_projects" name="animate_recent_projects">
        <small><?php _e("yes add CSS animation", "jozoorthemes"); ?></small>
      </div>
      </div><!-- /option -->
      
      <div class="option" id="animate_type_content_recent_projects">
      <div class="label padding-top"><label for="animate_type_recent_projects"><?php _e("Animate type", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <select class="animation" id="animate_type_recent_projects" name="animate_type_recent_projects">
        <?php
        foreach ($jozoor_shortcode_animation_type_list as $animation_name) {
        echo '<option value="'.$animation_name.'">'. $animation_name .'</option>';  
        }
        ?>
        </select>
        <small><a href="http://daneden.github.io/animate.css/" target="_blank"><?php _e("See all animation types in active", "jozoorthemes"); ?></a></small>
      </div>
      </div><!-- /option -->
      
      <div class="option" id="animate_duration_content_recent_projects">
      <div class="label padding-top"><label for="animate_duration_recent_projects"><?php _e("Animate Duration", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <input type="text" id="animate_duration_recent_projects" name="animate_duration_recent_projects" value="" placeholder="EX : 2">
        <small class="margin-top"><?php _e("Change the animation duration, default is 2 (seconds)", "jozoorthemes"); ?></small>
      </div>
      </div><!-- /option -->
      
      <div class="option" id="animate_delay_content_recent_projects">
      <div class="label padding-top"><label for="animate_delay_recent_projects"><?php _e("Animate Delay", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <input type="text" id="animate_delay_recent_projects" name="animate_delay_recent_projects" value="" placeholder="EX : 0">
        <small class="margin-top"><?php _e("Delay before the animation starts, default is 0 (seconds)", "jozoorthemes"); ?></small>
      </div>
      </div><!-- /option -->
      
      <div class="option" id="animate_iteration_content_recent_projects">
      <div class="label padding-top"><label for="animate_iteration_recent_projects"><?php _e("Animate Iteration", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <input type="text" id="animate_iteration_recent_projects" name="animate_iteration_recent_projects" value="" placeholder="EX : 0">
        <small class="margin-top"><?php _e("Number of times the animation is repeated, default is 0 (times)", "jozoorthemes"); ?></small>
      </div>
      </div><!-- /option -->
      
  </div><!-- /recent projects -->
    
  <div id="services" class="content">
      
    <div id="services_style" class="option">
      <div class="label"><label><?php _e("Choose style", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <input type="radio" id="services_style1" name="services_style" value="style1" checked="checked">
        <label for="services_style1"><?php _e("Style 1", "jozoorthemes"); ?></label>
        <input type="radio" id="services_style2" name="services_style" value="style2">
        <label for="services_style2"><?php _e("Style 2", "jozoorthemes"); ?></label>
        <input type="radio" id="services_style3" name="services_style" value="style3">
        <label for="services_style3"><?php _e("Style 3", "jozoorthemes"); ?></label>
      </div>
    </div><!-- /option -->
      
    <div id="services_icon_bg" class="option">
      <div class="label"><label><?php _e("Icon BG", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <input type="radio" id="services_icon_bg_normal" name="services_icon_bg" value="normal" checked="checked">
        <label for="services_icon_bg_normal"><?php _e("Normal", "jozoorthemes"); ?></label>
        <input type="radio" id="services_icon_bg_color" name="services_icon_bg" value="color">
        <label for="services_icon_bg_color"><?php _e("Color", "jozoorthemes"); ?></label>
      </div>
    </div><!-- /option -->
      
    <div class="option">
      <div class="label"><label for="animate_services"><?php _e("Animate Section", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <input type="checkbox" id="animate_services" name="animate_services">
        <small><?php _e("yes add CSS animation", "jozoorthemes"); ?></small>
      </div>
      </div><!-- /option -->
      
      <div class="option" id="animate_type_content_services">
      <div class="label padding-top"><label for="animate_type_services"><?php _e("Animate type", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <select class="animation" id="animate_type_services" name="animate_type_services">
        <?php
        foreach ($jozoor_shortcode_animation_type_list as $animation_name) {
        echo '<option value="'.$animation_name.'">'. $animation_name .'</option>';  
        }
        ?>
        </select>
        <small><a href="http://daneden.github.io/animate.css/" target="_blank"><?php _e("See all animation types in active", "jozoorthemes"); ?></a></small>
      </div>
      </div><!-- /option -->
      
      <div class="option" id="animate_duration_content_services">
      <div class="label padding-top"><label for="animate_duration_services"><?php _e("Animate Duration", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <input type="text" id="animate_duration_services" name="animate_duration_services" value="" placeholder="EX : 2">
        <small class="margin-top"><?php _e("Change the animation duration, default is 2 (seconds)", "jozoorthemes"); ?></small>
      </div>
      </div><!-- /option -->
      
      <div class="option" id="animate_delay_content_services">
      <div class="label padding-top"><label for="animate_delay_services"><?php _e("Animate Delay", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <input type="text" id="animate_delay_services" name="animate_delay_services" value="" placeholder="EX : 0">
        <small class="margin-top"><?php _e("Delay before the animation starts, default is 0 (seconds)", "jozoorthemes"); ?></small>
      </div>
      </div><!-- /option -->
      
      <div class="option" id="animate_iteration_content_services">
      <div class="label padding-top"><label for="animate_iteration_services"><?php _e("Animate Iteration", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <input type="text" id="animate_iteration_services" name="animate_iteration_services" value="" placeholder="EX : 0">
        <small class="margin-top"><?php _e("Number of times the animation is repeated, default is 0 (times)", "jozoorthemes"); ?></small>
      </div>
      </div><!-- /option -->
      
  </div><!-- /services -->
    
  <div id="testimonials" class="content">
      
    <div id="testimonials_columns" class="option">
      <div class="label"><label><?php _e("Columns Number", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <input type="radio" id="testimonials_columns1" name="testimonials_columns" value="cols1" checked="checked">
        <label for="testimonials_columns1"><?php _e("One 1", "jozoorthemes"); ?></label>
        <input type="radio" id="testimonials_columns2" name="testimonials_columns" value="cols2">
        <label for="testimonials_columns2"><?php _e("Two 2", "jozoorthemes"); ?></label>
        <input type="radio" id="testimonials_columns3" name="testimonials_columns" value="cols3">
        <label for="testimonials_columns3"><?php _e("Three 3", "jozoorthemes"); ?></label>
      </div>
    </div><!-- /option -->
      
    <div class="option">
      <div class="label"><label for="testimonials_carousel"><?php _e("Show in Carousel", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <input type="checkbox" id="testimonials_carousel" name="testimonials_carousel">
        <small><?php _e("yes", "jozoorthemes"); ?></small>
      </div>
    </div><!-- /option -->
      
    <div class="option">
      <div class="label"><label for="animate_testimonials"><?php _e("Animate Section", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <input type="checkbox" id="animate_testimonials" name="animate_testimonials">
        <small><?php _e("yes add CSS animation", "jozoorthemes"); ?></small>
      </div>
      </div><!-- /option -->
      
      <div class="option" id="animate_type_content_testimonials">
      <div class="label padding-top"><label for="animate_type_testimonials"><?php _e("Animate type", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <select class="animation" id="animate_type_testimonials" name="animate_type_testimonials">
        <?php
        foreach ($jozoor_shortcode_animation_type_list as $animation_name) {
        echo '<option value="'.$animation_name.'">'. $animation_name .'</option>';  
        }
        ?>
        </select>
        <small><a href="http://daneden.github.io/animate.css/" target="_blank"><?php _e("See all animation types in active", "jozoorthemes"); ?></a></small>
      </div>
      </div><!-- /option -->
      
      <div class="option" id="animate_duration_content_testimonials">
      <div class="label padding-top"><label for="animate_duration_testimonials"><?php _e("Animate Duration", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <input type="text" id="animate_duration_testimonials" name="animate_duration_testimonials" value="" placeholder="EX : 2">
        <small class="margin-top"><?php _e("Change the animation duration, default is 2 (seconds)", "jozoorthemes"); ?></small>
      </div>
      </div><!-- /option -->
      
      <div class="option" id="animate_delay_content_testimonials">
      <div class="label padding-top"><label for="animate_delay_testimonials"><?php _e("Animate Delay", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <input type="text" id="animate_delay_testimonials" name="animate_delay_testimonials" value="" placeholder="EX : 0">
        <small class="margin-top"><?php _e("Delay before the animation starts, default is 0 (seconds)", "jozoorthemes"); ?></small>
      </div>
      </div><!-- /option -->
      
      <div class="option" id="animate_iteration_content_testimonials">
      <div class="label padding-top"><label for="animate_iteration_testimonials"><?php _e("Animate Iteration", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <input type="text" id="animate_iteration_testimonials" name="animate_iteration_testimonials" value="" placeholder="EX : 0">
        <small class="margin-top"><?php _e("Number of times the animation is repeated, default is 0 (times)", "jozoorthemes"); ?></small>
      </div>
      </div><!-- /option -->
      
  </div><!-- /testimonials -->
    
  <div id="clients" class="content">
      
    <div class="option">
      <div class="label"><label for="clients_carousel"><?php _e("Show in Carousel", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <input type="checkbox" id="clients_carousel" name="clients_carousel">
        <small><?php _e("yes", "jozoorthemes"); ?></small>
      </div>
    </div><!-- /option -->
      
    <div id="clients_carousel_title_content" class="option hidden-area">
      <div class="label padding-top"><label for="clients_carousel_title"><?php _e("Carousel Title", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <input type="text" id="clients_carousel_title" name="clients_carousel_title" value="">
      </div>
    </div><!-- /option -->
      
    <div class="option">
      <div class="label"><label for="animate_clients"><?php _e("Animate Section", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <input type="checkbox" id="animate_clients" name="animate_clients">
        <small><?php _e("yes add CSS animation", "jozoorthemes"); ?></small>
      </div>
      </div><!-- /option -->
      
      <div class="option" id="animate_type_content_clients">
      <div class="label padding-top"><label for="animate_type_clients"><?php _e("Animate type", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <select class="animation" id="animate_type_clients" name="animate_type_clients">
        <?php
        foreach ($jozoor_shortcode_animation_type_list as $animation_name) {
        echo '<option value="'.$animation_name.'">'. $animation_name .'</option>';  
        }
        ?>
        </select>
        <small><a href="http://daneden.github.io/animate.css/" target="_blank"><?php _e("See all animation types in active", "jozoorthemes"); ?></a></small>
      </div>
      </div><!-- /option -->
      
      <div class="option" id="animate_duration_content_clients">
      <div class="label padding-top"><label for="animate_duration_clients"><?php _e("Animate Duration", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <input type="text" id="animate_duration_clients" name="animate_duration_clients" value="" placeholder="EX : 2">
        <small class="margin-top"><?php _e("Change the animation duration, default is 2 (seconds)", "jozoorthemes"); ?></small>
      </div>
      </div><!-- /option -->
      
      <div class="option" id="animate_delay_content_clients">
      <div class="label padding-top"><label for="animate_delay_clients"><?php _e("Animate Delay", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <input type="text" id="animate_delay_clients" name="animate_delay_clients" value="" placeholder="EX : 0">
        <small class="margin-top"><?php _e("Delay before the animation starts, default is 0 (seconds)", "jozoorthemes"); ?></small>
      </div>
      </div><!-- /option -->
      
      <div class="option" id="animate_iteration_content_clients">
      <div class="label padding-top"><label for="animate_iteration_clients"><?php _e("Animate Iteration", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <input type="text" id="animate_iteration_clients" name="animate_iteration_clients" value="" placeholder="EX : 0">
        <small class="margin-top"><?php _e("Number of times the animation is repeated, default is 0 (times)", "jozoorthemes"); ?></small>
      </div>
      </div><!-- /option -->
      
  </div><!-- /clients -->
    
  <div id="team" class="content">
      
    <div id="team_columns" class="option">
      <div class="label"><label><?php _e("Columns Number", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <input type="radio" id="team_columns3" name="team_columns" value="cols3" checked="checked">
        <label for="team_columns3"><?php _e("Three 3", "jozoorthemes"); ?></label>
        <input type="radio" id="team_columns4" name="team_columns" value="cols4">
        <label for="team_columns4"><?php _e("Four 4", "jozoorthemes"); ?></label>
      </div>
    </div><!-- /option -->
      
    <div class="option">
      <div class="label"><label for="animate_team"><?php _e("Animate Section", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <input type="checkbox" id="animate_team" name="animate_team">
        <small><?php _e("yes add CSS animation", "jozoorthemes"); ?></small>
      </div>
      </div><!-- /option -->
      
      <div class="option" id="animate_type_content_team">
      <div class="label padding-top"><label for="animate_type_team"><?php _e("Animate type", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <select class="animation" id="animate_type_team" name="animate_type_team">
        <?php
        foreach ($jozoor_shortcode_animation_type_list as $animation_name) {
        echo '<option value="'.$animation_name.'">'. $animation_name .'</option>';  
        }
        ?>
        </select>
        <small><a href="http://daneden.github.io/animate.css/" target="_blank"><?php _e("See all animation types in active", "jozoorthemes"); ?></a></small>
      </div>
      </div><!-- /option -->
      
      <div class="option" id="animate_duration_content_team">
      <div class="label padding-top"><label for="animate_duration_team"><?php _e("Animate Duration", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <input type="text" id="animate_duration_team" name="animate_duration_team" value="" placeholder="EX : 2">
        <small class="margin-top"><?php _e("Change the animation duration, default is 2 (seconds)", "jozoorthemes"); ?></small>
      </div>
      </div><!-- /option -->
      
      <div class="option" id="animate_delay_content_team">
      <div class="label padding-top"><label for="animate_delay_team"><?php _e("Animate Delay", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <input type="text" id="animate_delay_team" name="animate_delay_team" value="" placeholder="EX : 0">
        <small class="margin-top"><?php _e("Delay before the animation starts, default is 0 (seconds)", "jozoorthemes"); ?></small>
      </div>
      </div><!-- /option -->
      
      <div class="option" id="animate_iteration_content_team">
      <div class="label padding-top"><label for="animate_iteration_team"><?php _e("Animate Iteration", "jozoorthemes"); ?> : </label></div>
      <div class="option-content">
        <input type="text" id="animate_iteration_team" name="animate_iteration_team" value="" placeholder="EX : 0">
        <small class="margin-top"><?php _e("Number of times the animation is repeated, default is 0 (times)", "jozoorthemes"); ?></small>
      </div>
      </div><!-- /option -->
      
  </div><!-- /team -->
    
  <!-- store html content in this hidden div -->
  <div id="main_content" class="hidden-area"></div>
    
  <div class="clear"></div>
    
  <a id="insert-shortcode" class="insert-shortcode"><?php _e("Insert Shortcode", "jozoorthemes"); ?></a>
    
</div><!-- /generator -->
    
</body>
</html>