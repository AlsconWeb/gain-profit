<?php

#=============================================================#
# Columns
#=============================================================#

// 1/2 Column 
function jozoor_one_half_column($atts, $content = null) {
    
   extract(shortcode_atts(array( "text_center" => '', "last_column" => '', "remove_margin_bottom" => '', "animation" => '', "duration" => '', "delay" => '', "iteration" => '' ), $atts));
   if($text_center == 'true') { $j_center_class = ' text-center'; } else { $j_center_class = ''; }
   if($last_column == 'true') { $j_last_column_class = ' last'; } else { $j_last_column_class = ''; }
   if($remove_margin_bottom == 'true') { $j_remove_margin_bottom_class = ' no-margin-bottom'; } else { $j_remove_margin_bottom_class = ''; }
    
   if( $animation && !empty($animation) ) {
       $j_animation_class = ' wow '.$animation;
       if($duration != 0) { $j_duration_data = ' data-wow-duration="'.$duration.'s"'; } else { $j_duration_data = ''; }
       if($delay != 0) { $j_delay_data = ' data-wow-delay="'.$delay.'s"'; } else { $j_delay_data = ''; }
       if($iteration != 0) { $j_iteration_data = ' data-wow-iteration="'.$iteration.'"'; } else { $j_iteration_data = ''; }
   } else {
       $j_animation_class = $j_duration_data = $j_delay_data = $j_iteration_data = '';
   }
    
   return '<div class="eight columns shortcode-column'.$j_center_class.$j_last_column_class.$j_remove_margin_bottom_class.$j_animation_class.'"'.$j_duration_data.$j_delay_data.$j_iteration_data.'>'. do_shortcode($content) .'</div>';
}
add_shortcode("one_half", "jozoor_one_half_column");

// 1/3 Column 
function jozoor_one_third_column($atts, $content = null) {
    
   extract(shortcode_atts(array( "text_center" => '', "last_column" => '', "remove_margin_bottom" => '', "animation" => '', "duration" => '', "delay" => '', "iteration" => '' ), $atts));
   if($text_center == 'true') { $j_center_class = ' text-center'; } else { $j_center_class = ''; }
   if($last_column == 'true') { $j_last_column_class = ' last'; } else { $j_last_column_class = ''; }
   if($remove_margin_bottom == 'true') { $j_remove_margin_bottom_class = ' no-margin-bottom'; } else { $j_remove_margin_bottom_class = ''; }
    
   if( $animation && !empty($animation) ) {
       $j_animation_class = ' wow '.$animation;
       if($duration != 0) { $j_duration_data = ' data-wow-duration="'.$duration.'s"'; } else { $j_duration_data = ''; }
       if($delay != 0) { $j_delay_data = ' data-wow-delay="'.$delay.'s"'; } else { $j_delay_data = ''; }
       if($iteration != 0) { $j_iteration_data = ' data-wow-iteration="'.$iteration.'"'; } else { $j_iteration_data = ''; }
   } else {
       $j_animation_class = $j_duration_data = $j_delay_data = $j_iteration_data = '';
   }
    
   return '<div class="one-third column shortcode-column'.$j_center_class.$j_last_column_class.$j_remove_margin_bottom_class.$j_animation_class.'"'.$j_duration_data.$j_delay_data.$j_iteration_data.'>'. do_shortcode($content) .'</div>';
}
add_shortcode("one_third", "jozoor_one_third_column");

// 2/3 Column 
function jozoor_two_thirds_column($atts, $content = null) {
    
   extract(shortcode_atts(array( "text_center" => '', "last_column" => '', "remove_margin_bottom" => '', "animation" => '', "duration" => '', "delay" => '', "iteration" => '' ), $atts));
   if($text_center == 'true') { $j_center_class = ' text-center'; } else { $j_center_class = ''; }
   if($last_column == 'true') { $j_last_column_class = ' last'; } else { $j_last_column_class = ''; }
   if($remove_margin_bottom == 'true') { $j_remove_margin_bottom_class = ' no-margin-bottom'; } else { $j_remove_margin_bottom_class = ''; }
    
   if( $animation && !empty($animation) ) {
       $j_animation_class = ' wow '.$animation;
       if($duration != 0) { $j_duration_data = ' data-wow-duration="'.$duration.'s"'; } else { $j_duration_data = ''; }
       if($delay != 0) { $j_delay_data = ' data-wow-delay="'.$delay.'s"'; } else { $j_delay_data = ''; }
       if($iteration != 0) { $j_iteration_data = ' data-wow-iteration="'.$iteration.'"'; } else { $j_iteration_data = ''; }
   } else {
       $j_animation_class = $j_duration_data = $j_delay_data = $j_iteration_data = '';
   }
    
   return '<div class="two-thirds column shortcode-column'.$j_center_class.$j_last_column_class.$j_remove_margin_bottom_class.$j_animation_class.'"'.$j_duration_data.$j_delay_data.$j_iteration_data.'>'. do_shortcode($content) .'</div>';
}
add_shortcode("two_thirds", "jozoor_two_thirds_column");

// 1/4 Column 
function jozoor_one_fourth_column($atts, $content = null) {
    
   extract(shortcode_atts(array( "text_center" => '', "last_column" => '', "remove_margin_bottom" => '', "animation" => '', "duration" => '', "delay" => '', "iteration" => '' ), $atts));
   if($text_center == 'true') { $j_center_class = ' text-center'; } else { $j_center_class = ''; }
   if($last_column == 'true') { $j_last_column_class = ' last'; } else { $j_last_column_class = ''; }
   if($remove_margin_bottom == 'true') { $j_remove_margin_bottom_class = ' no-margin-bottom'; } else { $j_remove_margin_bottom_class = ''; }
    
   if( $animation && !empty($animation) ) {
       $j_animation_class = ' wow '.$animation;
       if($duration != 0) { $j_duration_data = ' data-wow-duration="'.$duration.'s"'; } else { $j_duration_data = ''; }
       if($delay != 0) { $j_delay_data = ' data-wow-delay="'.$delay.'s"'; } else { $j_delay_data = ''; }
       if($iteration != 0) { $j_iteration_data = ' data-wow-iteration="'.$iteration.'"'; } else { $j_iteration_data = ''; }
   } else {
       $j_animation_class = $j_duration_data = $j_delay_data = $j_iteration_data = '';
   }
    
   return '<div class="four columns shortcode-column'.$j_center_class.$j_last_column_class.$j_remove_margin_bottom_class.$j_animation_class.'"'.$j_duration_data.$j_delay_data.$j_iteration_data.'>'. do_shortcode($content) .'</div>';
}
add_shortcode("one_fourth", "jozoor_one_fourth_column");

// 11 Column 
function jozoor_one_eleven_column($atts, $content = null) {
    
   extract(shortcode_atts(array( "text_center" => '', "last_column" => '', "remove_margin_bottom" => '', "animation" => '', "duration" => '', "delay" => '', "iteration" => '' ), $atts));
   if($text_center == 'true') { $j_center_class = ' text-center'; } else { $j_center_class = ''; }
   if($last_column == 'true') { $j_last_column_class = ' last'; } else { $j_last_column_class = ''; }
   if($remove_margin_bottom == 'true') { $j_remove_margin_bottom_class = ' no-margin-bottom'; } else { $j_remove_margin_bottom_class = ''; }
    
   if( $animation && !empty($animation) ) {
       $j_animation_class = ' wow '.$animation;
       if($duration != 0) { $j_duration_data = ' data-wow-duration="'.$duration.'s"'; } else { $j_duration_data = ''; }
       if($delay != 0) { $j_delay_data = ' data-wow-delay="'.$delay.'s"'; } else { $j_delay_data = ''; }
       if($iteration != 0) { $j_iteration_data = ' data-wow-iteration="'.$iteration.'"'; } else { $j_iteration_data = ''; }
   } else {
       $j_animation_class = $j_duration_data = $j_delay_data = $j_iteration_data = '';
   }
    
   return '<div class="eleven columns shortcode-column'.$j_center_class.$j_last_column_class.$j_remove_margin_bottom_class.$j_animation_class.'"'.$j_duration_data.$j_delay_data.$j_iteration_data.'>'. do_shortcode($content) .'</div>';
}
add_shortcode("one_eleven", "jozoor_one_eleven_column");

// 5 Column 
function jozoor_one_five_column($atts, $content = null) {
    
   extract(shortcode_atts(array( "text_center" => '', "last_column" => '', "remove_margin_bottom" => '', "animation" => '', "duration" => '', "delay" => '', "iteration" => '' ), $atts));
   if($text_center == 'true') { $j_center_class = ' text-center'; } else { $j_center_class = ''; }
   if($last_column == 'true') { $j_last_column_class = ' last'; } else { $j_last_column_class = ''; }
   if($remove_margin_bottom == 'true') { $j_remove_margin_bottom_class = ' no-margin-bottom'; } else { $j_remove_margin_bottom_class = ''; }
    
   if( $animation && !empty($animation) ) {
       $j_animation_class = ' wow '.$animation;
       if($duration != 0) { $j_duration_data = ' data-wow-duration="'.$duration.'s"'; } else { $j_duration_data = ''; }
       if($delay != 0) { $j_delay_data = ' data-wow-delay="'.$delay.'s"'; } else { $j_delay_data = ''; }
       if($iteration != 0) { $j_iteration_data = ' data-wow-iteration="'.$iteration.'"'; } else { $j_iteration_data = ''; }
   } else {
       $j_animation_class = $j_duration_data = $j_delay_data = $j_iteration_data = '';
   }
    
   return '<div class="five columns shortcode-column'.$j_center_class.$j_last_column_class.$j_remove_margin_bottom_class.$j_animation_class.'"'.$j_duration_data.$j_delay_data.$j_iteration_data.'>'. do_shortcode($content) .'</div>';
}
add_shortcode("one_five", "jozoor_one_five_column");

// 12 Column 
function jozoor_one_twelve_column($atts, $content = null) {
    
   extract(shortcode_atts(array( "text_center" => '', "last_column" => '', "remove_margin_bottom" => '', "animation" => '', "duration" => '', "delay" => '', "iteration" => '' ), $atts));
   if($text_center == 'true') { $j_center_class = ' text-center'; } else { $j_center_class = ''; }
   if($last_column == 'true') { $j_last_column_class = ' last'; } else { $j_last_column_class = ''; }
   if($remove_margin_bottom == 'true') { $j_remove_margin_bottom_class = ' no-margin-bottom'; } else { $j_remove_margin_bottom_class = ''; }
    
   if( $animation && !empty($animation) ) {
       $j_animation_class = ' wow '.$animation;
       if($duration != 0) { $j_duration_data = ' data-wow-duration="'.$duration.'s"'; } else { $j_duration_data = ''; }
       if($delay != 0) { $j_delay_data = ' data-wow-delay="'.$delay.'s"'; } else { $j_delay_data = ''; }
       if($iteration != 0) { $j_iteration_data = ' data-wow-iteration="'.$iteration.'"'; } else { $j_iteration_data = ''; }
   } else {
       $j_animation_class = $j_duration_data = $j_delay_data = $j_iteration_data = '';
   }
    
   return '<div class="twelve columns shortcode-column'.$j_center_class.$j_last_column_class.$j_remove_margin_bottom_class.$j_animation_class.'"'.$j_duration_data.$j_delay_data.$j_iteration_data.'>'. do_shortcode($content) .'</div>';
}
add_shortcode("one_twelve", "jozoor_one_twelve_column");


#=============================================================#
# Elements
#=============================================================#

// Heading title
function jozoor_heading_title($atts, $content = null) {
    
   extract(shortcode_atts(array( "text_center" => '', "animation" => '', "duration" => '', "delay" => '', "iteration" => '' ), $atts));
   if($text_center == 'true') { $j_heading_center_class = ' text-center'; } else { $j_heading_center_class = ''; }
    
   if( $animation && !empty($animation) ) {
       $j_animation_class = ' wow '.$animation;
       if($duration != 0) { $j_duration_data = ' data-wow-duration="'.$duration.'s"'; } else { $j_duration_data = ''; }
       if($delay != 0) { $j_delay_data = ' data-wow-delay="'.$delay.'s"'; } else { $j_delay_data = ''; }
       if($iteration != 0) { $j_iteration_data = ' data-wow-iteration="'.$iteration.'"'; } else { $j_iteration_data = ''; }
   } else {
       $j_animation_class = $j_duration_data = $j_delay_data = $j_iteration_data = '';
   }
    
   return '<h3 class="title bottom-2'.$j_heading_center_class.$j_animation_class.'"'.$j_duration_data.$j_delay_data.$j_iteration_data.'>'.do_shortcode($content).'</h3>';
    
}
add_shortcode("heading_title", "jozoor_heading_title");

// button
function jozoor_button($atts, $content = null) {
    
   extract(shortcode_atts(array( "url" => '', "target" => '', "size" => '', "color" => '', "no_text" => '', "animation" => '', "duration" => '', "delay" => '', "iteration" => '' ), $atts));
    
   if($no_text == 'true') { $j_notext_class = ' no-text'; } else { $j_notext_class = ''; }
    
   if( $animation && !empty($animation) ) {
       $j_animation_class = ' wow '.$animation;
       if($duration != 0) { $j_duration_data = ' data-wow-duration="'.$duration.'s"'; } else { $j_duration_data = ''; }
       if($delay != 0) { $j_delay_data = ' data-wow-delay="'.$delay.'s"'; } else { $j_delay_data = ''; }
       if($iteration != 0) { $j_iteration_data = ' data-wow-iteration="'.$iteration.'"'; } else { $j_iteration_data = ''; }
   } else {
       $j_animation_class = $j_duration_data = $j_delay_data = $j_iteration_data = '';
   }
    
   return '<a href="'.$url.'" class="button '.$size.' '.$color.$j_notext_class.$j_animation_class.'"'.$j_duration_data.$j_delay_data.$j_iteration_data.' target="'.$target.'">'.do_shortcode($content).'</a>';
    
}
add_shortcode("button", "jozoor_button");

// highlight
function jozoor_highlight($atts, $content = null) {
    
   extract(shortcode_atts(array( "color" => '' ), $atts));
    
   return '<i class="highlight-'.$color.'">'.do_shortcode($content).'</i>';
    
}
add_shortcode("highlight", "jozoor_highlight");

// dropcaps
function jozoor_dropcaps($atts, $content = null) {
    
   extract(shortcode_atts(array( "color" => '' ), $atts));
    
   return '<div class="dropcaps '.$color.'">'.do_shortcode($content).'</div>';
    
}
add_shortcode("dropcaps", "jozoor_dropcaps");

// tooltip
function jozoor_tooltip($atts, $content = null) {
    
   extract(shortcode_atts(array( "text" => '', "color" => '' ), $atts));
    
   return '<i data="'.$text.'" class="'.$color.'">'.do_shortcode($content).'</i>';
    
}
add_shortcode("tooltip", "jozoor_tooltip");

// info box
function jozoor_info_box($atts, $content = null) {
    
   extract(shortcode_atts(array( "animation" => '', "duration" => '', "delay" => '', "iteration" => '' ), $atts));
    
   if( $animation && !empty($animation) ) {
       $j_animation_class = ' wow '.$animation;
       if($duration != 0) { $j_duration_data = ' data-wow-duration="'.$duration.'s"'; } else { $j_duration_data = ''; }
       if($delay != 0) { $j_delay_data = ' data-wow-delay="'.$delay.'s"'; } else { $j_delay_data = ''; }
       if($iteration != 0) { $j_iteration_data = ' data-wow-iteration="'.$iteration.'"'; } else { $j_iteration_data = ''; }
   } else {
       $j_animation_class = $j_duration_data = $j_delay_data = $j_iteration_data = '';
   }
    
   return '<div class="info-box'.$j_animation_class.'"'.$j_duration_data.$j_delay_data.$j_iteration_data.'>'.do_shortcode($content).'</div>';
    
}
add_shortcode("info_box", "jozoor_info_box");

// info box big
function jozoor_info_box_big($atts, $content = null) {
    
   extract(shortcode_atts(array( "large_text" => '', "small_text" => '', "button_text" => '', "url" => '', "target" => '', "animation" => '', "duration" => '', "delay" => '', "iteration" => '' ), $atts));
    
   if( $animation && !empty($animation) ) {
       $j_animation_class = ' wow '.$animation;
       if($duration != 0) { $j_duration_data = ' data-wow-duration="'.$duration.'s"'; } else { $j_duration_data = ''; }
       if($delay != 0) { $j_delay_data = ' data-wow-delay="'.$delay.'s"'; } else { $j_delay_data = ''; }
       if($iteration != 0) { $j_iteration_data = ' data-wow-iteration="'.$iteration.'"'; } else { $j_iteration_data = ''; }
   } else {
       $j_animation_class = $j_duration_data = $j_delay_data = $j_iteration_data = '';
   }
    
   return '<div class="qoute'.$j_animation_class.'"'.$j_duration_data.$j_delay_data.$j_iteration_data.'>
           <div class="eleven columns omega"><h4>'.$large_text.'</h4><p>'.$small_text.'</p></div>
           <div class="four columns alpha"><a href="'.$url.'" class="button medium color" target="'.$target.'">'.$button_text.'</a></div>
           </div>';
    
}
add_shortcode("info_box_big", "jozoor_info_box_big");

// divider
function jozoor_divider($atts, $content = null) {
    
   extract(shortcode_atts(array( "hidden_line" => '' ), $atts));
   if($hidden_line == 'true') { $j_hidden_line_class = ' hidden'; } else { $j_hidden_line_class = ''; }
    
   return '<hr class="line'.$j_hidden_line_class.' bottom-3 top-1"><!-- End line -->';
    
}
add_shortcode("divider", "jozoor_divider");

// list styles
function jozoor_list($atts, $content = null) {
    
   extract(shortcode_atts(array( "style" => '', "color" => '', "animation" => '', "duration" => '', "delay" => '', "iteration" => '' ), $atts));
   if($color == 'true') { $j_list_color = ' list-color'; } else { $j_list_color = ''; }
    
   if( $animation && !empty($animation) ) {
       $j_animation_class = ' wow '.$animation;
       if($duration != 0) { $j_duration_data = ' data-wow-duration="'.$duration.'s"'; } else { $j_duration_data = ''; }
       if($delay != 0) { $j_delay_data = ' data-wow-delay="'.$delay.'s"'; } else { $j_delay_data = ''; }
       if($iteration != 0) { $j_iteration_data = ' data-wow-iteration="'.$iteration.'"'; } else { $j_iteration_data = ''; }
   } else {
       $j_animation_class = $j_duration_data = $j_delay_data = $j_iteration_data = '';
   }
    
   return '<div class="'.$style.'-list'.$j_list_color.$j_animation_class.'"'.$j_duration_data.$j_delay_data.$j_iteration_data.'>'.do_shortcode($content).'</div>';
    
}
add_shortcode("list", "jozoor_list");

// alert box
function jozoor_alert_box($atts, $content = null) {
    
   extract(shortcode_atts(array( "type" => '', "close" => '', "animation" => '', "duration" => '', "delay" => '', "iteration" => '' ), $atts));
   
   if ($type == 'success') { $j_alert_icon = 'check'; }
   elseif ($type == 'info') { $j_alert_icon = 'info-sign'; }
   elseif ($type == 'notice') { $j_alert_icon = 'exclamation-sign'; }
   else { $j_alert_icon = 'warning-sign'; } 
    
   if($close == 'true') { $j_close_class = ' hideit'; $j_close_icon = '<i class="icon-remove close"></i>'; } else { $j_close_class = ''; $j_close_icon = ''; }
    
   if( $animation && !empty($animation) ) {
       $j_animation_class = ' wow '.$animation;
       if($duration != 0) { $j_duration_data = ' data-wow-duration="'.$duration.'s"'; } else { $j_duration_data = ''; }
       if($delay != 0) { $j_delay_data = ' data-wow-delay="'.$delay.'s"'; } else { $j_delay_data = ''; }
       if($iteration != 0) { $j_iteration_data = ' data-wow-iteration="'.$iteration.'"'; } else { $j_iteration_data = ''; }
   } else {
       $j_animation_class = $j_duration_data = $j_delay_data = $j_iteration_data = '';
   }
    
   return '<div class="alert '.$type.$j_close_class.$j_animation_class.'"'.$j_duration_data.$j_delay_data.$j_iteration_data.'><i class="icon-'.$j_alert_icon.'"></i><p>'.do_shortcode($content).'</p>'.$j_close_icon.'</div>';
    
}
add_shortcode("alert_box", "jozoor_alert_box");

// progress bar container
function jozoor_progress_bar($atts, $content = null) {
    
   extract(shortcode_atts(array( "animation" => '', "duration" => '', "delay" => '', "iteration" => '' ), $atts));
    
   if( $animation && !empty($animation) ) {
       $j_animation_class = ' wow '.$animation;
       if($duration != 0) { $j_duration_data = ' data-wow-duration="'.$duration.'s"'; } else { $j_duration_data = ''; }
       if($delay != 0) { $j_delay_data = ' data-wow-delay="'.$delay.'s"'; } else { $j_delay_data = ''; }
       if($iteration != 0) { $j_iteration_data = ' data-wow-iteration="'.$iteration.'"'; } else { $j_iteration_data = ''; }
   } else {
       $j_animation_class = $j_duration_data = $j_delay_data = $j_iteration_data = '';
   }
    
   return '<ul class="progress-bar'.$j_animation_class.'"'.$j_duration_data.$j_delay_data.$j_iteration_data.'>'.do_shortcode($content).'</ul>';
    
}
add_shortcode("progress_bar", "jozoor_progress_bar");

// progress bar item
function jozoor_bar($atts, $content = null) {
    
   extract(shortcode_atts(array( "title" => '', "percent" => '' ), $atts));
    
   return '<li class="meter">
          <div class="meter-content" data-percentage="'.$percent.'" style="width: '.$percent.'%"></div>
          <span class="meter-title">'.$title.' '.$percent.'%</span>
          </li>';
    
}
add_shortcode("bar", "jozoor_bar");

// accordion container
function jozoor_accordion($atts, $content = null) {
    
   extract(shortcode_atts(array( "style" => '', "id" => '', "animation" => '', "duration" => '', "delay" => '', "iteration" => '' ), $atts));
   if ($style == 2) { $j_accordion_style = ' style2'; } else { $j_accordion_style = ''; }
    
   if( $animation && !empty($animation) ) {
       $j_animation_class = ' wow '.$animation;
       if($duration != 0) { $j_duration_data = ' data-wow-duration="'.$duration.'s"'; } else { $j_duration_data = ''; }
       if($delay != 0) { $j_delay_data = ' data-wow-delay="'.$delay.'s"'; } else { $j_delay_data = ''; }
       if($iteration != 0) { $j_iteration_data = ' data-wow-iteration="'.$iteration.'"'; } else { $j_iteration_data = ''; }
   } else {
       $j_animation_class = $j_duration_data = $j_delay_data = $j_iteration_data = '';
   }
    
   return '<div id="'.$id.'" class="accordion'.$j_accordion_style.$j_animation_class.'"'.$j_duration_data.$j_delay_data.$j_iteration_data.'>'.do_shortcode($content).'</div>
          <script>
          jQuery(document).ready(function($){
            $("#'.$id.'").accordion({ 
              heightStyle: "content",
              icons: { "header": "icon-plus", "activeHeader": "icon-minus" }
            });
          });
          </script>
          ';
    
}
add_shortcode("accordion", "jozoor_accordion");

// accordion item
function jozoor_accordion_item($atts, $content = null) {
    
   extract(shortcode_atts(array( "title" => '' ), $atts));
    
   return '<h4><a href="#">'.$title.'</a></h4>
          <div><p>'.do_shortcode($content).'</p></div>';
    
}
add_shortcode("accordion_item", "jozoor_accordion_item");

// toggle container
function jozoor_toggle($atts, $content = null) {
    
   extract(shortcode_atts(array( "animation" => '', "duration" => '', "delay" => '', "iteration" => '' ), $atts));
    
   if( $animation && !empty($animation) ) {
       $j_animation_class = ' class="wow '.$animation.'"';
       if($duration != 0) { $j_duration_data = ' data-wow-duration="'.$duration.'s"'; } else { $j_duration_data = ''; }
       if($delay != 0) { $j_delay_data = ' data-wow-delay="'.$delay.'s"'; } else { $j_delay_data = ''; }
       if($iteration != 0) { $j_iteration_data = ' data-wow-iteration="'.$iteration.'"'; } else { $j_iteration_data = ''; }
   } else {
       $j_animation_class = $j_duration_data = $j_delay_data = $j_iteration_data = '';
   }
    
   return '<ul id="toggle-view"'.$j_animation_class.$j_duration_data.$j_delay_data.$j_iteration_data.'>'.do_shortcode($content).'</ul>';
    
}
add_shortcode("toggle", "jozoor_toggle");

// toggle item
function jozoor_toggle_item($atts, $content = null) {
    
   extract(shortcode_atts(array( "title" => '', "color" => '' ), $atts));
    
   if ($color == 'black') { $j_toggle_color = 'black'; $j_toggle_panel = ''; }
   elseif ($color == 'gray') { $j_toggle_color = 'normal'; $j_toggle_panel = ' normal'; }
   elseif ($color == 'transparent') { $j_toggle_color = 'normal border'; $j_toggle_panel = ' normal'; }
   else { $j_toggle_color = 'color'; $j_toggle_panel = ''; }
    
    
   return '<li><h4 class="'.$j_toggle_color.'">'.$title.'</h4><span class="link'.$j_toggle_panel.'">+</span><div class="panel'.$j_toggle_panel.'"><p>'.do_shortcode($content).'</p></div></li>';
    
}
add_shortcode("toggle_item", "jozoor_toggle_item");

// tabs container
function jozoor_tabs($atts, $content = null) {
    
   extract(shortcode_atts(array( "style" => '', "id" => '', "t_id" => '', "c_id" => '', "animation" => '', "duration" => '', "delay" => '', "iteration" => '' ), $atts));
   
   if ($style == 'h-2') { $j_tabs_style = ' style2'; $j_tabs_type = 'horizontal'; } 
   elseif ($style == 'v-1') { $j_tabs_style = ''; $j_tabs_type = 'vertical'; }
   elseif ($style == 'v-2') { $j_tabs_style = ' style2'; $j_tabs_type = 'vertical'; }
   else { $j_tabs_style = ''; $j_tabs_type = 'horizontal'; }
    
   if( $animation && !empty($animation) ) {
       $j_animation_class = ' wow '.$animation;
       if($duration != 0) { $j_duration_data = ' data-wow-duration="'.$duration.'s"'; } else { $j_duration_data = ''; }
       if($delay != 0) { $j_delay_data = ' data-wow-delay="'.$delay.'s"'; } else { $j_delay_data = ''; }
       if($iteration != 0) { $j_iteration_data = ' data-wow-iteration="'.$iteration.'"'; } else { $j_iteration_data = ''; }
   } else {
       $j_animation_class = $j_duration_data = $j_delay_data = $j_iteration_data = '';
   }
    
   return '<div id="'.$j_tabs_type.'-tabs" class="'.$id.''.$j_tabs_style.$j_animation_class.'"'.$j_duration_data.$j_delay_data.$j_iteration_data.'>'.do_shortcode($content).'</div>
          <script>
          jQuery(document).ready(function($){
            $("#'.$j_tabs_type.'-tabs.'.$id.'").tytabs({
              tabinit:"1",
              prefixtabs:"'.$t_id.'",
              prefixcontent:"'.$c_id.'",
              fadespeed:"fast"
            });
          });
          </script>
          ';
    
}
add_shortcode("tabs", "jozoor_tabs");

// tabs titles container
function jozoor_tabs_titles($atts, $content = null) {
    
   return '<ul class="tabs">'.do_shortcode($content).'</ul>';
    
}
add_shortcode("titles", "jozoor_tabs_titles");

// tabs titles
function jozoor_tab_title($atts, $content = null) {
    
   extract(shortcode_atts(array( "id" => '' ), $atts));
   
   return '<li id="'.$id.'">'.do_shortcode($content).'</li>';
    
}
add_shortcode("tab_title", "jozoor_tab_title");

// tabs contents container
function jozoor_tabs_contents($atts, $content = null) {
    
   return '<div class="contents">'.do_shortcode($content).'</div>';
    
}
add_shortcode("contents", "jozoor_tabs_contents");

// tabs contetns
function jozoor_tab_content($atts, $content = null) {
    
   extract(shortcode_atts(array( "id" => '' ), $atts));
   
   return '<div id="'.$id.'" class="tabscontent"><p>'.do_shortcode($content).'</p></div>';
    
}
add_shortcode("tab_content", "jozoor_tab_content");

// pricing table container
function jozoor_pricing_table($atts, $content = null) {
    
   extract(shortcode_atts(array( "animation" => '', "duration" => '', "delay" => '', "iteration" => '' ), $atts));
    
   if( $animation && !empty($animation) ) {
       $j_animation_class = ' wow '.$animation;
       if($duration != 0) { $j_duration_data = ' data-wow-duration="'.$duration.'s"'; } else { $j_duration_data = ''; }
       if($delay != 0) { $j_delay_data = ' data-wow-delay="'.$delay.'s"'; } else { $j_delay_data = ''; }
       if($iteration != 0) { $j_iteration_data = ' data-wow-iteration="'.$iteration.'"'; } else { $j_iteration_data = ''; }
   } else {
       $j_animation_class = $j_duration_data = $j_delay_data = $j_iteration_data = '';
   }
    
   return '<div class="pricing-tables'.$j_animation_class.'"'.$j_duration_data.$j_delay_data.$j_iteration_data.'>'.do_shortcode($content).'</div>';
    
}
add_shortcode("pricing_table", "jozoor_pricing_table");

// pricing table column
function jozoor_pricing_column($atts, $content = null) {
    
   extract(shortcode_atts(array( "title" => '', "price" => '', "interval" => '', "popular" => '', "class" => '' ), $atts));
    
   if ($class == '3') { $j_column_class = 'one-third column'; } 
   elseif ($class == '4') { $j_column_class = 'four columns'; }
   else { $j_column_class = 'eight columns'; }
    
   if ($popular == 'true') { $j_column_popular = ' color'; } else { $j_column_popular = ''; }
    
   return '<div class="'.$j_column_class.' tables-column'.$j_column_popular.'">
           <div class="header">
            <h3>'.$title.'</h3>
            <div class="price">
            <h2>'.$price.'</h2>
            <span>'.$interval.'</span>
            </div>
           </div>
           '.do_shortcode($content).'
          </div>';
    
}
add_shortcode("pricing_column", "jozoor_pricing_column");

// pricing table button
function jozoor_pricing_button($atts, $content = null) {
    
   extract(shortcode_atts(array( "text" => '', "url" => '' ), $atts));
    
   return '<div class="footer">
           <h4><a href="'.$url.'">'.$text.'</a></h4>
           </div>';
    
}
add_shortcode("pricing_button", "jozoor_pricing_button");

// icons
function jozoor_icon($atts, $content = null) {
    
   extract(shortcode_atts(array( "name" => '', "size" => '', "color" => '', "spin" => '', "flip" => '', "rotate" => '' ), $atts));
    
   if( !empty($size)) { $j_icon_size = $size; } else { $j_icon_size = '20'; }
    
   if ($color == 'color') { $j_icon_color = ' color'; } 
   elseif ($color == 'white') { $j_icon_color = ' white'; }
   else { $j_icon_color = ''; }
    
   if($spin == 'true') { $j_spin_icon_class = ' icon-spin'; } else { $j_spin_icon_class = ''; }
    
   if ($flip == 'horizontal') { $j_icon_flip_class = ' icon-flip-horizontal'; } 
   elseif ($flip == 'vertical') { $j_icon_flip_class = ' icon-flip-vertical'; }
   else { $j_icon_flip_class = ''; }
    
   if ($rotate == '90') { $j_icon_rotate_class = ' icon-rotate-90'; } 
   elseif ($rotate == '180') { $j_icon_rotate_class = ' icon-rotate-180'; }
   elseif ($rotate == '270') { $j_icon_rotate_class = ' icon-rotate-270'; }
   else { $j_icon_rotate_class = ''; }
    
   return '<i class="'.$name.' s-'.$j_icon_size.$j_icon_color.$j_spin_icon_class.$j_icon_flip_class.$j_icon_rotate_class.'"></i>';
    
}
add_shortcode("icon", "jozoor_icon");

// social icons
function jozoor_social_icon($atts, $content = null) {
    
   extract(shortcode_atts(array( "name" => '', "size" => '', "color" => '' ), $atts));
    
   if( !empty($size)) { $j_icon_size = $size; } else { $j_icon_size = '20'; }
    
   if ($color == 'color') { $j_icon_color = ' color'; } 
   elseif ($color == 'white') { $j_icon_color = ' white'; }
   else { $j_icon_color = ''; }
    
   return '<i class="'.$name.' s-'.$j_icon_size.$j_icon_color.'"></i>';
    
}
add_shortcode("social_icon", "jozoor_social_icon");

// icon boxes container
function jozoor_icon_boxes($atts, $content = null) {
    
   extract(shortcode_atts(array( "style" => '', "animation" => '', "duration" => '', "delay" => '', "iteration" => '' ), $atts));
    
   if ($style == 2) { $j_boxes_style = ' style-2'; } 
   elseif ($style == 3) { $j_boxes_style = ' style-3'; } 
   else { $j_boxes_style = ' style-1'; }
    
   if( $animation && !empty($animation) ) {
       $j_animation_class = ' wow '.$animation;
       if($duration != 0) { $j_duration_data = ' data-wow-duration="'.$duration.'s"'; } else { $j_duration_data = ''; }
       if($delay != 0) { $j_delay_data = ' data-wow-delay="'.$delay.'s"'; } else { $j_delay_data = ''; }
       if($iteration != 0) { $j_iteration_data = ' data-wow-iteration="'.$iteration.'"'; } else { $j_iteration_data = ''; }
   } else {
       $j_animation_class = $j_duration_data = $j_delay_data = $j_iteration_data = '';
   }
    
   return '<div class="services'.$j_boxes_style.$j_animation_class.' clearfix"'.$j_duration_data.$j_delay_data.$j_iteration_data.'>'.do_shortcode($content).'</div>';
    
}
add_shortcode("icon_boxes", "jozoor_icon_boxes");

// icon boxes box
function jozoor_box($atts, $content = null) {
    
   extract(shortcode_atts(array( "class" => '', "bg" => '' ), $atts));
    
   if ($class == 1) { $j_box_class = 'full-column'; } 
   elseif ($class == 2) { $j_box_class = 'eight columns'; } 
   elseif ($class == 4) { $j_box_class = 'four columns'; } 
   else { $j_box_class = 'one-third column'; }
    
   if ($bg == 'color') { $j_box_color = ' active'; } 
   else { $j_box_color = ''; }
    
   return '<div class="'.$j_box_class.' icon_box"><div class="item'.$j_box_color.'">'.do_shortcode($content).'</div></div>';
    
}
add_shortcode("box", "jozoor_box");

// icon boxes icon
function jozoor_box_icon($atts, $content = null) {
    
   extract(shortcode_atts(array( "name" => '', "url" => '', "spin" => '', "flip" => '', "rotate" => '' ), $atts));
    
   if($url) {
       $j_box_url_start = '<a href="'.$url.'">';
       $j_box_url_end = '</a>';
   } else {
      $j_box_url_start = '';
      $j_box_url_end = ''; 
   }
    
   if($spin == 'true') { $j_spin_icon_class = ' icon-spin'; } else { $j_spin_icon_class = ''; }
    
   if ($flip == 'horizontal') { $j_icon_flip_class = ' icon-flip-horizontal'; } 
   elseif ($flip == 'vertical') { $j_icon_flip_class = ' icon-flip-vertical'; }
   else { $j_icon_flip_class = ''; }
    
   if ($rotate == '90') { $j_icon_rotate_class = ' icon-rotate-90'; } 
   elseif ($rotate == '180') { $j_icon_rotate_class = ' icon-rotate-180'; }
   elseif ($rotate == '270') { $j_icon_rotate_class = ' icon-rotate-270'; }
   else { $j_icon_rotate_class = ''; }
    
   return '<div class="circle float-left">'.$j_box_url_start.'<i class="'.$name.$j_spin_icon_class.$j_icon_flip_class.$j_icon_rotate_class.'"></i>'.$j_box_url_end.'</div>';
    
}
add_shortcode("box_icon", "jozoor_box_icon");

// icon boxes box content
function jozoor_box_content($atts, $content = null) {
    
   return '<div class="data float-right">'.do_shortcode($content).'</div>';
    
}
add_shortcode("box_content", "jozoor_box_content");

// icon boxes box content title
function jozoor_box_title($atts, $content = null) {
    
   extract(shortcode_atts(array( "text" => '', "url" => '' ), $atts));
    
   if($url) {
       $j_box_title_url_start = '<a href="'.$url.'">';
       $j_box_title_url_end = '</a>';
   } else {
      $j_box_title_url_start = '';
      $j_box_title_url_end = ''; 
   }
    
   return '<h4>'.$j_box_title_url_start.''.$text.''.$j_box_title_url_end.'</h4>';
    
}
add_shortcode("box_title", "jozoor_box_title");

// carousel slider
function jozoor_carousel_slider($atts, $content = null) {
    
   extract(shortcode_atts(array( "title" => '', "auto" => '', "id" => '', "animation" => '', "duration" => '', "delay" => '', "iteration" => '' ), $atts));
    
   if($auto == 'true') { $j_carousel_auto = ' data-autorotate="6000"'; } else { $j_carousel_auto = ''; }
    
   if( $animation && !empty($animation) ) {
       $j_animation_class = ' wow '.$animation;
       if($duration != 0) { $j_duration_data = ' data-wow-duration="'.$duration.'s"'; } else { $j_duration_data = ''; }
       if($delay != 0) { $j_delay_data = ' data-wow-delay="'.$delay.'s"'; } else { $j_delay_data = ''; }
       if($iteration != 0) { $j_iteration_data = ' data-wow-iteration="'.$iteration.'"'; } else { $j_iteration_data = ''; }
   } else {
       $j_animation_class = $j_duration_data = $j_delay_data = $j_iteration_data = '';
   }
    
   return '<div class="'.$id.$j_animation_class.' carousel-shortcode"'.$j_carousel_auto.$j_duration_data.$j_delay_data.$j_iteration_data.'><h3 class="title bottom-2">'.$title.'</h3>
          <ul class="slider" id="'.$id.'">'.do_shortcode($content).'</ul></div>
          <script>
          jQuery(document).ready(function($){
            $(".'.$id.'").carousel({ 
             namespace: "'.$id.'",
             speed: 600
            });
          });
          </script>
          ';
    
}
add_shortcode("carousel_slider", "jozoor_carousel_slider");

// carousel slider item
function jozoor_carousel_item($atts, $content = null) {
    
   return '<li class="slide">'.do_shortcode($content).'</li>';
    
}
add_shortcode("carousel_item", "jozoor_carousel_item");

// images slider
function jozoor_images_slider($atts, $content = null) {
    
   extract(shortcode_atts(array( "animation" => '', "nav" => '', "id" => '', "animation_slider" => '', "duration" => '', "delay" => '', "iteration" => '' ), $atts));
    
   if($animation == 'fade') { $j_slider_animation = 'fade'; } else { $j_slider_animation = 'slide'; }
   if($nav == 'false') { $j_slider_nav = 'false'; } else { $j_slider_nav = 'true'; }
    
   if( $animation_slider && !empty($animation_slider) ) {
       $j_animation_class = 'wow '.$animation_slider;
       if($duration != 0) { $j_duration_data = ' data-wow-duration="'.$duration.'s"'; } else { $j_duration_data = ''; }
       if($delay != 0) { $j_delay_data = ' data-wow-delay="'.$delay.'s"'; } else { $j_delay_data = ''; }
       if($iteration != 0) { $j_iteration_data = ' data-wow-iteration="'.$iteration.'"'; } else { $j_iteration_data = ''; }
       $j_animation_div_start = '<div class="'.$j_animation_class.'"'.$j_duration_data.$j_delay_data.$j_iteration_data.'>';
       $j_animation_div_end = '</div>';
   } else {
       $j_animation_class = $j_duration_data = $j_delay_data = $j_iteration_data = $j_animation_div_start = $j_animation_div_end = '';
   }
    
   return $j_animation_div_start.'<div class="flex-container slider-shortcode"><div class="'.$id.'"><ul class="slides">'.do_shortcode($content).'</ul></div></div>'.$j_animation_div_end.'
          <script>
          jQuery(document).ready(function($){
            $(".'.$id.'").flexslider({
	         animation: "'.$j_slider_animation.'",
	         animationLoop: true,
	         slideshow: true,               
	         slideshowSpeed: 4500,         
	         animationSpeed: 700,      
	         pauseOnHover: true, 
	         pauseOnAction:false,
	         controlNav: false,
	         directionNav: '.$j_slider_nav.',
	         controlsContainer: ".flex-container"
		});
          });
          </script>
          ';
    
}
add_shortcode("images_slider", "jozoor_images_slider");

// images slider item
function jozoor_slider_item($atts, $content = null) {
    
   extract(shortcode_atts(array( "img" => '', "alt" => '', "link" => '', "target" => '' ), $atts));
    
   if($link) {
       $j_slider_image_link_start = '<a href="'.$link.'" target="'.$target.'">';
       $j_slider_image_link_end = '</a>';
   } else {
      $j_slider_image_link_start = '';
      $j_slider_image_link_end = ''; 
   }
    
   return '<li>'.$j_slider_image_link_start.'<img src="'.$img.'" alt="'.$alt.'">'.$j_slider_image_link_end.'</li>';
    
}
add_shortcode("slider_item", "jozoor_slider_item");

// animate element
function jozoor_animate_element($atts, $content = null) {
    
   extract(shortcode_atts(array( "animation" => '', "duration" => '', "delay" => '', "iteration" => '' ), $atts));
   
   if( $animation && !empty($animation) ) {
       $j_animation_class = ' wow '.$animation;
       if($duration != 0) { $j_duration_data = ' data-wow-duration="'.$duration.'s"'; } else { $j_duration_data = ''; }
       if($delay != 0) { $j_delay_data = ' data-wow-delay="'.$delay.'s"'; } else { $j_delay_data = ''; }
       if($iteration != 0) { $j_iteration_data = ' data-wow-iteration="'.$iteration.'"'; } else { $j_iteration_data = ''; }
   } else {
       $j_animation_class = $j_duration_data = $j_delay_data = $j_iteration_data = '';
   }
    
   return '<div class="animate-element'.$j_animation_class.'"'.$j_duration_data.$j_delay_data.$j_iteration_data.'>'. do_shortcode($content) .'</div>';
}
add_shortcode("animate_element", "jozoor_animate_element");

// google maps main div
function jozoor_gmaps($atts, $content = null) {
    
   extract(shortcode_atts(array( "lat" => '', "lng" => '', "id" => '', "full_top" => '' ), $atts));
    
   if($full_top == 'true') { $j_full_top_class = ' full-top'; } else { $j_full_top_class = ''; }
    
   return '<!-- Start Google Map --><div id="'.$id.'" class="gmaps'.$j_full_top_class.'"></div><!-- End -->
          <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=true"></script>
          <script type="text/javascript" src="'.get_template_directory_uri().'/js/gmaps.js"></script>
          <script type="text/javascript">
          var map;
          jQuery(document).ready(function($){
	  
	      // main directions
          map = new GMaps({
          el: "#'.$id.'", lat: '.$lat.', lng: '.$lng.', zoom: 13, zoomControl : true, 
          zoomControlOpt: { style : "SMALL", position: "TOP_LEFT" }, panControl : false, scrollwheel: false
          });
         
	      // add address markers
	      '. strip_tags(do_shortcode($content)) .'
	  
          });
          </script>
          ';
    
}
add_shortcode("gmaps", "jozoor_gmaps");

// gmaps marker
function jozoor_marker($atts, $content = null) {
    
   extract(shortcode_atts(array( "lat" => '', "lng" => '', "title" => '', "content" => '' ), $atts));
    
   return 'map.addMarker({ lat: '.$lat.', lng: '.$lng.', title: "'.$title.'",
          infoWindow: { content: "<p>'.$content.'</p>" } });';
    
}
add_shortcode("marker", "jozoor_marker");


#=============================================================#
# Query Posts
#=============================================================#

// latest news
function jozoor_latest_posts($atts, $content = null) {
    
   global $post;
    
   extract(shortcode_atts(array( "num" => '', "animation" => '', "duration" => '', "delay" => '', "iteration" => '' ), $atts));
    
   if($num) {
       $j_latest_news_num = $num;
   } else {
      $j_latest_news_num = 3;
   }
    
   if( $animation && !empty($animation) ) {
       $j_animation_class = ' wow '.$animation;
       if($duration != 0) { $j_duration_data = ' data-wow-duration="'.$duration.'s"'; } else { $j_duration_data = ''; }
       if($delay != 0) { $j_delay_data = ' data-wow-delay="'.$delay.'s"'; } else { $j_delay_data = ''; }
       if($iteration != 0) { $j_iteration_data = ' data-wow-iteration="'.$iteration.'"'; } else { $j_iteration_data = ''; }
   } else {
       $j_animation_class = $j_duration_data = $j_delay_data = $j_iteration_data = '';
   }
    
   $latest_news_content = '<div class="latest-blog latest-news-shortcode bottom-1'.$j_animation_class.'"'.$j_duration_data.$j_delay_data.$j_iteration_data.'>';
    
   $posts_args = array( 'post_type' => 'post', 'posts_per_page' => $j_latest_news_num );
   $posts_query = new WP_Query( $posts_args );
   while ( $posts_query->have_posts() ) : $posts_query->the_post();
       
      $latest_news_content .= '<div id="post-'.get_the_ID().'" class="'; 
    
      $allClasses = get_post_class(); 
      foreach ($allClasses as $class) { $latest_news_content .= $class . ' '; }
    
      $latest_news_content .= 'one-third column item">';
       
      $latest_news_content .= '<a href="'.get_permalink().'">';
        
       if ( has_post_thumbnail() ) {
          $latest_news_content .= get_the_post_thumbnail($post->ID, 'latest-news-home', array('title' => ''));
       }
       
       $latest_news_content .= '</a>
       <h3><a href="'.get_permalink().'">'.get_the_title().'</a></h3>';
    
       $latest_news_content .= '<div class="meta">'.get_the_time('d M, Y'); 
       
       $j_comments_num = get_comments_number(); 
       if ( comments_open() ) {
           
		 if ( $j_comments_num == 0 ) { $comments_text = __("Leave a comment", "jozoorthemes"); }
         elseif ( $j_comments_num == 1 ) { $comments_text = __("1 Comment", "jozoorthemes"); }
         else { $comments_text = $j_comments_num . __(' Comments', "jozoorthemes"); } 
    
		 $latest_news_content .= ' / <a href="'.get_comments_link().'">'.$comments_text.'</a>';
       } 

       $latest_news_content .= '</div>';

       $latest_news_content .= '<p>'.get_the_excerpt().'</p>'; 

      $latest_news_content .= '</div>';
       
   
   endwhile;
   wp_reset_postdata();
    
   $latest_news_content .= '</div>';
    
   return $latest_news_content;
    
}
add_shortcode("latest_posts", "jozoor_latest_posts");


// latest projects
function jozoor_recent_projects($atts, $content = null) {
    
   global $post;
    
   extract(shortcode_atts(array( "num" => '', "cols" => '', "animation" => '', "duration" => '', "delay" => '', "iteration" => '' ), $atts));
    
   if($num) {
       $j_recent_projects_num = $num;
   } else {
      $j_recent_projects_num = 3;
   }
    
   // check column
   if( $cols == '2' ) { $j_portfolio_layout_class = 'eight columns col2'; }
   elseif ( $cols == '4' ) { $j_portfolio_layout_class = 'four columns col4'; }
   else { $j_portfolio_layout_class = 'one-third column col3'; }
    
   if( $animation && !empty($animation) ) {
       $j_animation_class = ' wow '.$animation;
       if($duration != 0) { $j_duration_data = ' data-wow-duration="'.$duration.'s"'; } else { $j_duration_data = ''; }
       if($delay != 0) { $j_delay_data = ' data-wow-delay="'.$delay.'s"'; } else { $j_delay_data = ''; }
       if($iteration != 0) { $j_iteration_data = ' data-wow-iteration="'.$iteration.'"'; } else { $j_iteration_data = ''; }
   } else {
       $j_animation_class = $j_duration_data = $j_delay_data = $j_iteration_data = '';
   }
    
   $recent_projects_content = '<div class="portfolio recent-projects-shortcode bottom-1'.$j_animation_class.'"'.$j_duration_data.$j_delay_data.$j_iteration_data.'>';
    
   $projects_args = array( 'post_type' => 'portfolio', 'posts_per_page' => $j_recent_projects_num );
   $projects_query = new WP_Query( $projects_args );
   while ( $projects_query->have_posts() ) : $projects_query->the_post();
    
   $recent_projects_content .= '<div id="post-'.get_the_ID().'" class="'; 
    
   $allClasses = get_post_class(); 
   foreach ($allClasses as $class) { $recent_projects_content .= $class . ' '; }
    
   $recent_projects_content .= ''.$j_portfolio_layout_class.' item">';
    
   $recent_projects_content .= '<a href="'.get_permalink().'">';
   $recent_projects_content .= get_the_post_thumbnail($post->ID, 'portfolio-thumb', array('title' => ''));
   $recent_projects_content .= '<div class="img-caption"><div class="desc"><h3>'.get_the_title().'</h3><p>';
    
      $j_terms = get_the_terms( $post->ID, 'portfolio-category' );						
      if ( $j_terms && ! is_wp_error( $j_terms ) ) : 
      $j_draught_links = array();
      foreach ( $j_terms as $term ) {
      $j_draught_links[] = $term->name;
      }		
      $recent_projects_content .= $j_on_draught = join( ", ", $j_draught_links );
      endif;
      
      
   $recent_projects_content .= '</p><span>+</span></div></div></a>';
    
   $recent_projects_content .= '</div>';
   
   endwhile;
   wp_reset_postdata();
    
   $recent_projects_content .= '</div>';
    
   return $recent_projects_content;
    
}
add_shortcode("recent_projects", "jozoor_recent_projects");


// services
function jozoor_services($atts, $content = null) {
    
   global $post;
    
   extract(shortcode_atts(array( "style" => '', "bg" => '', "animation" => '', "duration" => '', "delay" => '', "iteration" => '' ), $atts));
    
   if ($style == 2) { $j_services_style = ' style-2'; $j_box_class = 'one-third column'; } 
   elseif ($style == 3) { $j_services_style = ' style-3'; $j_box_class = 'eight columns'; } 
   else { $j_services_style = ' style-1'; $j_box_class = 'one-third column'; }
    
   if ($bg == 'color') { $j_services_bg = ' active'; } 
   else { $j_services_bg = ''; }
    
   if( $animation && !empty($animation) ) {
       $j_animation_class = ' wow '.$animation;
       if($duration != 0) { $j_duration_data = ' data-wow-duration="'.$duration.'s"'; } else { $j_duration_data = ''; }
       if($delay != 0) { $j_delay_data = ' data-wow-delay="'.$delay.'s"'; } else { $j_delay_data = ''; }
       if($iteration != 0) { $j_iteration_data = ' data-wow-iteration="'.$iteration.'"'; } else { $j_iteration_data = ''; }
   } else {
       $j_animation_class = $j_duration_data = $j_delay_data = $j_iteration_data = '';
   }
    
   $services_content = '<div class="services'.$j_services_style.$j_animation_class.' clearfix"'.$j_duration_data.$j_delay_data.$j_iteration_data.'>';
    
   $services_args = array( 'post_type' => 'services', 'order' => 'ASC', 'posts_per_page' => -1 );
   $services_query = new WP_Query( $services_args );
   while ( $services_query->have_posts() ) : $services_query->the_post();
    
   // get metaboxs
   $j_service_icon_type = get_post_meta( $post->ID, '_jozoor_service_icon_type', true ); // icon type
   $j_service_icon_font_name = get_post_meta( $post->ID, '_jozoor_service_icon_font_name', true ); // icon font name
   $j_service_icon_image_name = get_post_meta( $post->ID, '_jozoor_service_icon_image_name', true ); // icon image
   $j_service_icon_font_spin = get_post_meta( $post->ID, '_jozoor_service_icon_font_spin', true ); // icon spin
   $j_service_icon_font_flip = get_post_meta( $post->ID, '_jozoor_service_icon_font_flip', true ); // icon flip
   $j_service_icon_font_rotate = get_post_meta( $post->ID, '_jozoor_service_icon_font_rotate', true ); // icon rotate

   // check icon font options
   // spin
   if($j_service_icon_font_spin == 'on') { $j_spin_icon_class = ' icon-spin'; } else { $j_spin_icon_class = ''; }
   // flip
   if ($j_service_icon_font_flip == 'horizontal') { $j_icon_flip_class = ' icon-flip-horizontal'; } 
   elseif ($j_service_icon_font_flip == 'vertical') { $j_icon_flip_class = ' icon-flip-vertical'; }
   else { $j_icon_flip_class = ''; }
   // rotate
   if ($j_service_icon_font_rotate == 'ro90') { $j_icon_rotate_class = ' icon-rotate-90'; } 
   elseif ($j_service_icon_font_rotate == 'ro180') { $j_icon_rotate_class = ' icon-rotate-180'; }
   elseif ($j_service_icon_font_rotate == 'ro270') { $j_icon_rotate_class = ' icon-rotate-270'; }
   else { $j_icon_rotate_class = ''; }
       
   $services_content .= '<div id="post-'.get_the_ID().'" class="'; 
    
   $allClasses = get_post_class(); 
   foreach ($allClasses as $class) { $services_content .= $class . ' '; }
    
   $services_content .= ''.$j_box_class.' icon_box">';
    
   $services_content .= '
        <div class="item'.$j_services_bg.'">
           <div class="circle float-left"><a href="'.get_permalink().'">';
   if($j_service_icon_type == 'iconimage') {
       $services_content .= '<img src="'.$j_service_icon_image_name.'" alt="'.get_the_title().'">';
   } else {
       $services_content .= '<i class="'.$j_service_icon_font_name.$j_spin_icon_class.$j_icon_flip_class.$j_icon_rotate_class.'"></i>';
   }
   $services_content .= '</a></div>
           <div class="data float-right">
           <h4><a href="'.get_permalink().'">'.get_the_title().'</a></h4>
           <p>'.get_the_excerpt().'</p>
           </div>
         </div> ';
    
   $services_content .= '</div>';
   
   endwhile;
   wp_reset_postdata();
    
   $services_content .= '</div>';
    
   return $services_content;
    
}
add_shortcode("services", "jozoor_services");


// testimonials
function jozoor_testimonials($atts, $content = null) {
    
   global $post;
    
   extract(shortcode_atts(array( "cols" => '', "carousel" => '', "id" => '', "animation" => '', "duration" => '', "delay" => '', "iteration" => '' ), $atts));
    
   if($carousel == 'true') { 
       $j_testimonials_carousel_container_start = '<div class="'.$id.' carousel-shortcode" data-autorotate="6000"><ul class="slider" id="'.$id.'">';
       $j_testimonials_carousel_container_end = '</ul></div>
          <script>
          jQuery(document).ready(function($){
            $(".'.$id.'").carousel({ 
             namespace: "'.$id.'",
             speed: 600
            });
          });
          </script>
       '; 
       $j_testimonials_carousel_item_start = '<li class="slide">';
       $j_testimonials_carousel_item_end = '</li>';
       
       $j_testimonials_class = 'full-column'; // define full width column
       
   } else { 
       $j_testimonials_carousel_container_start = ''; 
       $j_testimonials_carousel_container_end = '';
       $j_testimonials_carousel_item_start = '';
       $j_testimonials_carousel_item_end = '';
       
       // check column
       if ($cols == 2) { $j_testimonials_class = 'eight columns'; } 
       elseif ($cols == 3) { $j_testimonials_class = 'one-third column'; } 
       else { $j_testimonials_class = 'full-column'; }
       
   }
    
   if( $animation && !empty($animation) ) {
       $j_animation_class = ' wow '.$animation;
       if($duration != 0) { $j_duration_data = ' data-wow-duration="'.$duration.'s"'; } else { $j_duration_data = ''; }
       if($delay != 0) { $j_delay_data = ' data-wow-delay="'.$delay.'s"'; } else { $j_delay_data = ''; }
       if($iteration != 0) { $j_iteration_data = ' data-wow-iteration="'.$iteration.'"'; } else { $j_iteration_data = ''; }
   } else {
       $j_animation_class = $j_duration_data = $j_delay_data = $j_iteration_data = '';
   }
    
   $testimonials_content = '<div class="testimonials-shortcode'.$j_animation_class.'"'.$j_duration_data.$j_delay_data.$j_iteration_data.'>';
    
   $testimonials_content .= $j_testimonials_carousel_container_start; // start carousel container
    
   $testimonials_args = array( 'post_type' => 'testimonials', 'posts_per_page' => -1 );
   $testimonials_query = new WP_Query( $testimonials_args );
   while ( $testimonials_query->have_posts() ) : $testimonials_query->the_post();
    
   // get metaboxs
   $j_testimonials_client_online_url = get_post_meta( $post->ID, '_jozoor_testimonial_client_online_url', true ); // client url
   $j_testimonials_client_job = get_post_meta( $post->ID, '_jozoor_testimonial_client_job', true ); // client job
    
   if(!empty($j_testimonials_client_online_url)) {
       $j_test_client_url_start = '<a href="'.$j_testimonials_client_online_url.'" target="_blank">';
       $j_test_client_url_end = '</a>';
   } else {
       $j_test_client_url_start = '';
       $j_test_client_url_end = '';
   }
    
   $testimonials_content .= $j_testimonials_carousel_item_start; // start carousel item
       
   $testimonials_content .= '<div id="post-'.get_the_ID().'" class="'; 
    
   $allClasses = get_post_class(); 
   foreach ($allClasses as $class) { $testimonials_content .= $class . ' '; }
    
   $testimonials_content .= ''.$j_testimonials_class.' testimonials">';
    
   $testimonials_content .= '<div class="testimonial-item"><p>'.get_the_content().'</p>
            <div class="testimonials-arrow"></div>
            <div class="author">'.$j_test_client_url_start.'<span class="color">'.get_the_title().'</span>'.$j_test_client_url_end.', '.$j_testimonials_client_job.'</div>
          </div>
   ';
    
   $testimonials_content .= '</div>';
    
   $testimonials_content .= $j_testimonials_carousel_item_end; // end carousel item
   
   endwhile;
   wp_reset_postdata();
    
   $testimonials_content .= $j_testimonials_carousel_container_end; // end carousel container
    
   $testimonials_content .= '</div>';
    
   return $testimonials_content;
    
}
add_shortcode("testimonials", "jozoor_testimonials");


// clients
function jozoor_clients($atts, $content = null) {
    
   global $post;
    
   extract(shortcode_atts(array( "title" => '', "carousel" => '', "id" => '', "animation" => '', "duration" => '', "delay" => '', "iteration" => '' ), $atts));
    
   if($carousel == 'true') { 
       $slideclass = "class='slide'";
       $j_clients_carousel_container_start = '<div class="'.$id.' carousel-shortcode"><h3 class="title bottom-2">'.$title.'</h3><ul class="slider" id="'.$id.'">';
       $j_clients_carousel_container_end = '</ul></div>
          <script>
          jQuery(document).ready(function($){
            $(".'.$id.'").carousel({ 
             namespace: "'.$id.'",
             speed: 600
            });
          });
          </script>
       '; 
       $j_clients_carousel_item_start = '<li class="slide">';
       $j_clients_carousel_item_end = '</li>';
       
       
   } else { 
       $j_clients_carousel_container_start = ''; 
       $j_clients_carousel_container_end = '';
       $j_clients_carousel_item_start = '';
       $j_clients_carousel_item_end = '';
       
   }
    
   if( $animation && !empty($animation) ) {
       $j_animation_class = ' wow '.$animation;
       if($duration != 0) { $j_duration_data = ' data-wow-duration="'.$duration.'s"'; } else { $j_duration_data = ''; }
       if($delay != 0) { $j_delay_data = ' data-wow-delay="'.$delay.'s"'; } else { $j_delay_data = ''; }
       if($iteration != 0) { $j_iteration_data = ' data-wow-iteration="'.$iteration.'"'; } else { $j_iteration_data = ''; }
   } else {
       $j_animation_class = $j_duration_data = $j_delay_data = $j_iteration_data = '';
   }
    
   $clients_content = '<div class="featured-clients clients-shortcode clearfix'.$j_animation_class.'"'.$j_duration_data.$j_delay_data.$j_iteration_data.'>';
    
   $clients_content .= $j_clients_carousel_container_start; // start carousel container
    
   $clients_args = array( 'post_type' => 'clients', 'posts_per_page' => -1 );
   $clients_query = get_posts($clients_args);
    
   $count_clients_num = wp_count_posts( 'clients' )->publish;

   $clients_per_page = $count_clients_num; // num of clients
   $clients_num_cols = $clients_per_page / 4; // num of columns
   $clients_num_per_cols = ceil($clients_per_page / $clients_num_cols);
   $clients_group = array_chunk($clients_query, $clients_num_per_cols);
 
   // Start Counter
   $counter = 0;
    
   // Start loop
   foreach ($clients_group as $posts) {
   $clients_content .= $j_clients_carousel_item_start; // start carousel item
            
   foreach($posts as $post) { // while(have_posts()) 
    
   // get metaboxs
   $j_clients_online_url = get_post_meta( $post->ID, '_jozoor_client_online_url', true ); // client url
       
   $clients_content .= '<div id="post-'.get_the_ID().'" class="'; 
    
   $allClasses = get_post_class(); 
   foreach ($allClasses as $class) { $clients_content .= $class . ' '; }
    
   $clients_content .= 'four columns item">';
    
   if($j_clients_online_url) {
   $clients_content .= '<a href="'.$j_clients_online_url.'" target="_blank">';
   }
   $clients_content .= get_the_post_thumbnail($post->ID, 'full', array('title' => '')); 
   if($j_clients_online_url) { 
   $clients_content .= '</a>';
   } 
    
   $clients_content .= '</div>';
       
   } 
            
   $clients_content .= $j_clients_carousel_item_end; // end carousel item
   $counter ++;   
   }
    
   wp_reset_postdata();
    
   $clients_content .= $j_clients_carousel_container_end; // end carousel container
    
   $clients_content .= '</div>';
    
   return $clients_content;
    
}
add_shortcode("clients", "jozoor_clients");


// team
function jozoor_team($atts, $content = null) {
    
   global $post;
    
   extract(shortcode_atts(array( "cols" => '', "animation" => '', "duration" => '', "delay" => '', "iteration" => '' ), $atts));
    
   if ($cols == 4) { $j_team_item_class = 'four columns'; $j_team_container_class = ' columns-4'; } 
   else { $j_team_item_class = 'one-third column'; $j_team_container_class = ''; }
    
   if( $animation && !empty($animation) ) {
       $j_animation_class = ' wow '.$animation;
       if($duration != 0) { $j_duration_data = ' data-wow-duration="'.$duration.'s"'; } else { $j_duration_data = ''; }
       if($delay != 0) { $j_delay_data = ' data-wow-delay="'.$delay.'s"'; } else { $j_delay_data = ''; }
       if($iteration != 0) { $j_iteration_data = ' data-wow-iteration="'.$iteration.'"'; } else { $j_iteration_data = ''; }
   } else {
       $j_animation_class = $j_duration_data = $j_delay_data = $j_iteration_data = '';
   }
    
   $team_content = '<div class="team team-shortcode'.$j_team_container_class.$j_animation_class.'"'.$j_duration_data.$j_delay_data.$j_iteration_data.'>';
    
   $team_args = array( 'post_type' => 'team', 'order' => 'ASC', 'posts_per_page' => -1 );
   $team_query = new WP_Query( $team_args );
   while ( $team_query->have_posts() ) : $team_query->the_post();
    
   // get metaboxs
   $j_member_social_icon_url_data = get_post_meta( $post->ID, '_jozoor_member_social_icon_url', true ); // icon type
       
   $team_content .= '<div id="post-'.get_the_ID().'" class="'; 
    
   $allClasses = get_post_class(); 
   foreach ($allClasses as $class) { $team_content .= $class . ' '; }
    
   $team_content .= ''.$j_team_item_class.' item">';
    
   $team_content .= get_the_post_thumbnail($post->ID, 'team-members-thumb', array('title' => '')); 
   $team_content .= '<div class="clearfix"></div><h4>'.get_the_title().'</h4><div class="clearfix"></div><span>';

   $j_terms = get_the_terms( $post->ID, 'team-category' );						
   if ( $j_terms && ! is_wp_error( $j_terms ) ) : 
      $j_draught_links = array();
      foreach ( $j_terms as $term ) {
      $j_draught_links[] = $term->name;
      }		
   $team_content .= $j_on_draught = join( " / ", $j_draught_links );
   endif;
    
   $team_content .= '</span><div class="clearfix"></div><p>'.get_the_content().'</p>';
    
   $team_content .= '<div class="member-social">';
    
   foreach ( (array) $j_member_social_icon_url_data as $key => $entry ) {
       
      $j_member_social_icon_name = $j_member_social_url = '';
    
      if ( isset( $entry['member_social_icon_name'] ) ) { $j_member_social_icon_name = $entry['member_social_icon_name']; } // get icon name
      if ( isset( $entry['member_social_url'] ) ) { $j_member_social_url = $entry['member_social_url']; } // get url
       
      if(!empty($j_member_social_url)) {
      $team_content .= '<a href="'.$j_member_social_url.'" target="_blank"><i class="social_icon-'.$j_member_social_icon_name.' s-17"></i></a>';
      } else {
      $team_content .= ''; 
      }
   }
    
   $team_content .= '</div>';
    
   $team_content .= '</div>';
   
   endwhile;
   wp_reset_query();
    
   $team_content .= '</div>';
    
   return $team_content;
    
}
add_shortcode("team", "jozoor_team");

?>