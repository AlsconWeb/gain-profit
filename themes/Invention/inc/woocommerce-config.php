<?php

/*
start woocommerce custom configuration
*/

// Remove the breadcrumbs
add_action( 'init', 'jozoor_remove_wc_breadcrumbs' );
function jozoor_remove_wc_breadcrumbs() {
    remove_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb', 20, 0 );
}

// Change number or products per row to 3
if (!function_exists('jozoor_loop_columns')) {
	function jozoor_loop_columns() {
		return 3; // 3 products per row
	}
}

// Display 12 products per page
add_filter( 'loop_shop_per_page', create_function( '$cols', 'return 12;' ), 20 );

// Remove the Product Count
// remove_action( 'woocommerce_before_shop_loop', 'woocommerce_result_count', 20 );

// Define image sizes
global $pagenow;
if ( is_admin() && isset( $_GET['activated'] ) && $pagenow == 'themes.php' ) add_action( 'init', 'jozoor_woocommerce_image_dimensions', 1 );
 
function jozoor_woocommerce_image_dimensions() {
  	$catalog = array(
		'width' 	=> '451',	// px
		'height'	=> '451',	// px
		'crop'		=> 1 		// true
	);
 
	$single = array(
		'width' 	=> '600',	// px
		'height'	=> '600',	// px
		'crop'		=> 1 		// true
	);
 
	$thumbnail = array(
		'width' 	=> '120',	// px
		'height'	=> '120',	// px
		'crop'		=> 0 		// false
	);
 
	// Image sizes
	update_option( 'shop_catalog_image_size', $catalog ); 		// Product category thumbs
	update_option( 'shop_single_image_size', $single ); 		// Single product image
	update_option( 'shop_thumbnail_image_size', $thumbnail ); 	// Image gallery thumbs
}

// Ensure cart contents update when products are added to the cart via AJAX
add_filter('add_to_cart_fragments', 'jozoor_woocommerce_header_add_to_cart_fragment');
 
function jozoor_woocommerce_header_add_to_cart_fragment( $fragments ) {
	global $woocommerce;
	
	ob_start();
	
	?>
	<a class="cart-contents" href="<?php echo $woocommerce->cart->get_cart_url(); ?>" title="<?php _e('View your shopping cart', 'woothemes'); ?>"><?php echo sprintf(_n('%d', '%d', $woocommerce->cart->cart_contents_count, 'woothemes'), $woocommerce->cart->cart_contents_count);?> - <?php echo $woocommerce->cart->get_cart_total(); ?></a>
	<?php
	
	$fragments['a.cart-contents'] = ob_get_clean();
	
	return $fragments;
	
}


// Change number of thumbnails per row in product galleries
add_filter ( 'woocommerce_product_thumbnails_columns', 'jozoor_thumb_cols' );
 function jozoor_thumb_cols() {
     return 4; // .last class applied to every 4th thumbnail
}


// Change the Number of Related Products in Woocommerce
function jozoor_woo_related_products_limit() {
  
global $product, $woocommerce_loop;
$related = $product->get_related(4);

$args = array(
 'post_type' => 'product',
 'no_found_rows' => 1,
 'posts_per_page' => 4,
 'ignore_sticky_posts' => 1,
 'orderby' => 'rand',
 'post__in' => $related,
 'post__not_in' => array($product->id)
 );
    
 return $args;
    
}
add_filter( 'woocommerce_related_products_args', 'jozoor_woo_related_products_limit' );


?>