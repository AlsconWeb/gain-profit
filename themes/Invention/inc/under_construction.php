<?php

// get default body boxed class
 if($jozoor_layout_style == 'boxed') { 
    $path = explode('/', $jozoor_body_bg_image);
    $filename = $path[count($path)-1];
    $class_name = str_replace('.jpg', '', $filename);
    $class_name .= ' under-construction';
 } else { $class_name = ' under-construction'; }

?>
  
<body <?php body_class($class_name); ?>>
  
  <?php if($jozoor_loading_image == 1) { ?>  
  <div class="loader"></div>   
  <?php } ?>

  <div id="wrap" class="boxed">

   <header>
     <div class="container clearfix">
       <div class="sixteen columns">
         <div class="logo">
          <?php if($jozoor_logo_text_instead_image == 1 && !empty($jozoor_logo_name_text)) { ?>
              <h1 class="site-title-logo"><a href="<?php echo home_url(); ?>"><?php echo $jozoor_logo_name_text; ?></a></h1>  
            <?php } else { ?>
            <a href="<?php echo home_url(); ?>"> 
            <?php if(!empty($jozoor_logo_image)) { ?>
            <img src="<?php echo $jozoor_logo_image;?>" alt="<?php bloginfo('name'); ?>">
            <?php } else { ?>
            <img src="<?php echo get_template_directory_uri(); ?>/images/logo.png" alt="Invention - Responsive WordPress Theme" />
            <?php } ?>
            </a>
            <?php } ?>
          </div>
       </div>
     </div><!-- End Container -->
   </header><!-- <<< End Header >>> -->
      
   <?php
    
   // custom body bg for page
   if(!empty($jozoor_bg_image_under_cons)) {
    
    if($jozoor_bg_image_repeat_under_cons == 'fixed' || $jozoor_bg_image_repeat_under_cons == 'no-repeat') {
        $j_bg_image_under_cons_style = ' style="background:url('.$jozoor_bg_image_under_cons.') '.$jozoor_bg_image_repeat_under_cons.'; background-position:center; background-size: cover; -moz-background-size: cover; -webkit-background-size: cover; -o-background-size: cover;"';
    } else {
        $j_bg_image_under_cons_style = ' style="background:url('.$jozoor_bg_image_under_cons.') '.$jozoor_bg_image_repeat_under_cons.';"';
    }
    
   } else {
    $j_bg_image_under_cons_style = '';  
   }

   ?>
   
   <div class="content"<?php echo $j_bg_image_under_cons_style; ?>>
    <div class="container clearfix">
      <div class="sixteen columns">
      
       <?php if(!empty($jozoor_under_cons_title)) { ?>
       <h2 class="title"><?php echo $jozoor_under_cons_title; ?></h2>
       <?php } ?>
       
       <?php if(!empty($jozoor_under_cons_msg)) { ?>
       <p class="text"><?php echo $jozoor_under_cons_msg; ?></p>
       <?php } ?>
      
      <div class="clearfix top-3 bottom-3"></div>
    
    
    <div id="count2" class="count-end-message" ></div><!-- Message After Time End -->
    
    
    <div id="spacer1">
    <div id="clock-ticker" class="clearfix">
        <div class="block">
            <span class="flip-top" id="dday"></span>
            <span class="label" id="days"><?php _e("Days", "jozoorthemes"); ?></span>
        </div>
        
        <div class="block">
            <span class="flip-top" id="dhour"></span>
            <span class="label" id="hours"><?php _e("Hours", "jozoorthemes"); ?></span>
        </div>
        
        <div class="block">
            <span class="flip-top" id="dmin"></span>
            <span class="label" id="minutes"><?php _e("Mins", "jozoorthemes"); ?></span>
        </div>
        
        <div class="block last">
            <span class="flip-top" id="dsec"></span>
            <span class="label" id="seconds"><?php _e("Secs", "jozoorthemes"); ?></span><br>
        </div>
        
    </div><!-- End clock-ticker -->
    </div><!-- End spacer1 -->
    
    <div class="clearfix top-2 bottom-4"></div>
    
    
    <?php if(!empty($jozoor_under_cons_subscribe)) {
        echo $jozoor_under_cons_subscribe;
    } ?>
    
      
       </div><!-- End Column --> 
    </div><!-- End Container -->
  </div><!-- <<< End Content >>> -->
   
   
   <footer>
      <div class="container clearfix">
      <div class="sixteen columns">
      
        <span class="copyright">
         <?php if(!empty($jozoor_under_cons_footer_text)) {
         echo $jozoor_under_cons_footer_text;
         } ?>
        </span>
        
        <div class="social">
          <?php if ($jozoor_under_cons_social_slider != '') { // get all social networks 
           foreach ($jozoor_under_cons_social_slider as $slide) {  
           if(!empty($slide['name'])) {
           ?>
           <a href="<?php echo $slide['link']; ?>" target="_blank"><i class="social_icon-<?php echo $slide['name']; ?>"></i></a>
           <?php } 
           } } else { echo '<span class="none">'.__("you need to add social links from footer options!", "jozoorthemes").'</span>'; } ?>
          </div>
      
      </div><!-- End Column --> 
    </div><!-- End Container -->
   </footer><!-- <<< End Footer >>> -->
  
  </div><!-- End wrap -->
  
<?php get_footer(); ?>