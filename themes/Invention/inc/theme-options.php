<?php

global $smof_data; //fetch options stored in $smof_data 

#=============================================================#
# General Settings
#=============================================================#
global $jozoor_close_site, $jozoor_under_cons_title, $jozoor_under_cons_msg, $jozoor_under_cons_timer, $jozoor_under_cons_subscribe, $jozoor_under_cons_footer_text, 
$jozoor_under_cons_social_slider, $jozoor_bg_image_under_cons, $jozoor_bg_image_repeat_under_cons, $jozoor_favicon_image, $jozoor_iphone_icon, $jozoor_iphone_retina_icon, 
$jozoor_ipad_icon, $jozoor_ipad_retina_icon, $jozoor_style_switcher, $jozoor_totop_icon, $jozoor_responsive_design, $jozoor_loading_image, $jozoor_default_wp_pagination, 
$jozoor_page404_message, $jozoor_custom_codes_header, $jozoor_custom_codes_footer, $jozoor_search_page_current_page,
$jozoor_search_page_layout_style, $jozoor_search_page_sidebar_type ;

$jozoor_close_site = $smof_data['close_site']; 
$jozoor_favicon_image = $smof_data['favicon_image']; 
$jozoor_iphone_icon = $smof_data['iphone_icon'];
$jozoor_iphone_retina_icon = $smof_data['iphone_retina_icon'];
$jozoor_ipad_icon = $smof_data['ipad_icon'];
$jozoor_ipad_retina_icon = $smof_data['ipad_retina_icon'];
if(isset($smof_data['style_switcher'])) { $jozoor_style_switcher = $smof_data['style_switcher']; }
$jozoor_totop_icon = $smof_data['totop_icon'];
$jozoor_responsive_design = $smof_data['responsive_design'];
$jozoor_loading_image = $smof_data['loading_image'];
$jozoor_default_wp_pagination = $smof_data['default_wp_pagination'];
$jozoor_page404_message = $smof_data['page404_message'];
$jozoor_custom_codes_header = $smof_data['custom_codes_header'];
$jozoor_custom_codes_footer = $smof_data['custom_codes_footer'];
$jozoor_search_page_current_page = $smof_data['search_page_current_page'];
$jozoor_search_page_layout_style = $smof_data['search_page_layout_style'];
$jozoor_search_page_sidebar_type = $smof_data['search_page_sidebar_type'];
 

// under construction
$jozoor_under_cons_title = $smof_data['under_cons_title'];
$jozoor_under_cons_msg = $smof_data['under_cons_msg'];
$jozoor_under_cons_timer = $smof_data['under_cons_timer'];
$jozoor_under_cons_subscribe = $smof_data['under_cons_subscribe'];
$jozoor_under_cons_footer_text = $smof_data['under_cons_footer_text'];
$jozoor_under_cons_social_slider = $smof_data['under_cons_social_slider'];
$jozoor_bg_image_under_cons = $smof_data['bg_image_under_cons'];
$jozoor_bg_image_repeat_under_cons = $smof_data['bg_image_repeat_under_cons'];
// End


#=============================================================#
# Header Options
#=============================================================#
global $jozoor_logo_image, $jozoor_logo_width, $jozoor_logo_height, $jozoor_logo_top_margin, $jozoor_logo_bottom_margin, $jozoor_header_layout, $jozoor_fixed_header, $jozoor_menu_item_bg_border_none, $jozoor_add_bg_image_main_header, $jozoor_show_search_icon,
$jozoor_bg_image_main_header, $jozoor_bg_image_repeat_main_header, $jozoor_show_top_bar_header, $jozoor_phone_number_top_bar, $jozoor_email_address_top_bar, 
$jozoor_social_slider_header, $jozoor_show_top_header_menu, $jozoor_menu_links_top_padding_one, $jozoor_menu_links_bottom_padding_one, $jozoor_menu_links_leftright_padding_one, 
$jozoor_submenu_links_top_position_one, $jozoor_menu_links_top_padding_two, $jozoor_menu_links_bottom_padding_two, $jozoor_menu_links_leftright_padding_two, 
$jozoor_submenu_links_top_position_two, $jozoor_header_layout3_right_content, $jozoor_header_layout3_right_custom_content, $jozoor_header_layout3_right_custom_content_margin, $jozoor_add_custom_content_top_header, $jozoor_custom_content_top_header, $jozoor_add_custom_content_bottom_header, $jozoor_custom_content_bottom_header, $jozoor_logo_text_instead_image, $jozoor_logo_name_text, $jozoor_logo_name_font_type, $jozoor_logo_name_font_sizes_styles, $jozoor_logo_name_custom_color;

$jozoor_logo_image = $smof_data['logo_image']; 
$jozoor_logo_width = $smof_data['logo_width']; 
$jozoor_logo_height = $smof_data['logo_height']; 
$jozoor_logo_top_margin = $smof_data['logo_top_margin'];
$jozoor_logo_bottom_margin = $smof_data['logo_bottom_margin'];
$jozoor_header_layout = $smof_data['header_layout'];
$jozoor_fixed_header = $smof_data['fixed_header'];
$jozoor_menu_item_bg_border_none = $smof_data['menu_item_bg_border_none'];
$jozoor_show_search_icon = $smof_data['show_search_icon'];
$jozoor_add_bg_image_main_header = $smof_data['add_bg_image_main_header'];
$jozoor_bg_image_main_header = $smof_data['bg_image_main_header'];
$jozoor_bg_image_repeat_main_header = $smof_data['bg_image_repeat_main_header'];
$jozoor_show_top_bar_header = $smof_data['show_top_bar_header'];
$jozoor_phone_number_top_bar = $smof_data['phone_number_top_bar'];
$jozoor_email_address_top_bar = $smof_data['email_address_top_bar'];
$jozoor_social_slider_header = $smof_data['social_slider_header'];
$jozoor_show_top_header_menu = $smof_data['show_top_header_menu'];
$jozoor_menu_links_top_padding_one = $smof_data['menu_links_top_padding_one'];
$jozoor_menu_links_bottom_padding_one = $smof_data['menu_links_bottom_padding_one'];
$jozoor_menu_links_leftright_padding_one = $smof_data['menu_links_leftright_padding_one'];
$jozoor_submenu_links_top_position_one = $smof_data['submenu_links_top_position_one'];
$jozoor_menu_links_top_padding_two = $smof_data['menu_links_top_padding_two'];
$jozoor_menu_links_bottom_padding_two = $smof_data['menu_links_bottom_padding_two'];
$jozoor_menu_links_leftright_padding_two = $smof_data['menu_links_leftright_padding_two'];
$jozoor_submenu_links_top_position_two = $smof_data['submenu_links_top_position_two'];
$jozoor_header_layout3_right_content = $smof_data['header_layout3_right_content'];
$jozoor_header_layout3_right_custom_content = $smof_data['header_layout3_right_custom_content'];
$jozoor_header_layout3_right_custom_content_margin = $smof_data['header_layout3_right_custom_content_margin'];
$jozoor_add_custom_content_top_header = $smof_data['add_custom_content_top_header'];
$jozoor_custom_content_top_header = $smof_data['custom_content_top_header'];
$jozoor_add_custom_content_bottom_header = $smof_data['add_custom_content_bottom_header'];
$jozoor_custom_content_bottom_header = $smof_data['custom_content_bottom_header'];
$jozoor_logo_text_instead_image = $smof_data['logo_text_instead_image'];
$jozoor_logo_name_text = $smof_data['logo_name_text'];
$jozoor_logo_name_font_type = $smof_data['logo_name_font_type'];
$jozoor_logo_name_font_sizes_styles = $smof_data['logo_name_font_sizes_styles'];
$jozoor_logo_name_custom_color = $smof_data['logo_name_custom_color'];


#=============================================================#
# Footer Options
#=============================================================#
global $jozoor_show_footer_widgets, $jozoor_footer_layout, $jozoor_show_footer_downbar, $jozoor_footer_text, $jozoor_social_slider_footer, $jozoor_show_down_footer_menu, 
$jozoor_add_custom_content_top_footer, $jozoor_custom_content_top_footer, $jozoor_add_custom_content_bottom_footer, $jozoor_custom_content_bottom_footer ;

$jozoor_show_footer_widgets = $smof_data['show_footer_widgets'];
$jozoor_footer_layout = $smof_data['footer_layout'];
$jozoor_show_footer_downbar = $smof_data['show_footer_downbar'];
$jozoor_footer_text = $smof_data['footer_text'];
$jozoor_social_slider_footer = $smof_data['social_slider_footer'];
$jozoor_show_down_footer_menu = $smof_data['show_down_footer_menu'];
$jozoor_add_custom_content_top_footer = $smof_data['add_custom_content_top_footer'];
$jozoor_custom_content_top_footer = $smof_data['custom_content_top_footer'];
$jozoor_add_custom_content_bottom_footer = $smof_data['add_custom_content_bottom_footer'];
$jozoor_custom_content_bottom_footer = $smof_data['custom_content_bottom_footer'];


#=============================================================#
# Styles Options
#=============================================================#
global $jozoor_layout_style, $jozoor_theme_skin, $jozoor_default_color_scheme, $jozoor_custom_design_colors, 
 $jozoor_body_bg_image, $jozoor_custom_body_bg_image, $jozoor_custom_body_background, $jozoor_body_bg_repeat;

$jozoor_layout_style = $smof_data['layout_style']; 
$jozoor_theme_skin = $smof_data['theme_skin']; 
$jozoor_default_color_scheme = $smof_data['default_color_scheme']; 
$jozoor_custom_design_colors = $smof_data['custom_design_colors'];
$jozoor_body_bg_image = $smof_data['body_bg_image'];
$jozoor_custom_body_bg_image = $smof_data['custom_body_bg_image'];
$jozoor_custom_body_background = $smof_data['custom_body_background'];
if(isset($smof_data['body_bg_repeat'])) { $jozoor_body_bg_repeat = $smof_data['body_bg_repeat']; }


#=============================================================#
# Typography Options
#=============================================================#
global $jozoor_using_google_fonts, $jozoor_using_standard_fonts, $jozoor_update_font_sizes_styles, $jozoor_update_font_lineheights ;

if(isset($smof_data['using_google_fonts'])) { $jozoor_using_google_fonts = $smof_data['using_google_fonts']; }
if(isset($smof_data['using_standard_fonts'])) { $jozoor_using_standard_fonts = $smof_data['using_standard_fonts']; }
if(isset($smof_data['update_font_sizes_styles'])) { $jozoor_update_font_sizes_styles = $smof_data['update_font_sizes_styles']; }
if(isset($smof_data['update_font_lineheights'])) { $jozoor_update_font_lineheights = $smof_data['update_font_lineheights']; }


#=============================================================#
# Custom CSS
#=============================================================#
global $jozoor_custom_css, $jozoor_custom_css_code, $jozoor_tablets_landscape_custom_css_code, $jozoor_tablets_portrait_custom_css_code, $jozoor_mobiles_landscape_custom_css_code,
$jozoor_mobiles_portrait_custom_css_code ;

if(isset($smof_data['custom_css'])) { $jozoor_custom_css = $smof_data['custom_css']; }
if(isset($smof_data['custom_css_code'])) { $jozoor_custom_css_code = $smof_data['custom_css_code']; }
if(isset($smof_data['tablets_landscape_custom_css_code'])) { $jozoor_tablets_landscape_custom_css_code = $smof_data['tablets_landscape_custom_css_code']; }
if(isset($smof_data['tablets_portrait_custom_css_code'])) { $jozoor_tablets_portrait_custom_css_code = $smof_data['tablets_portrait_custom_css_code']; }
if(isset($smof_data['mobiles_landscape_custom_css_code'])) { $jozoor_mobiles_landscape_custom_css_code = $smof_data['mobiles_landscape_custom_css_code']; }
if(isset($smof_data['mobiles_portrait_custom_css_code'])) { $jozoor_mobiles_portrait_custom_css_code = $smof_data['mobiles_portrait_custom_css_code']; }


#=============================================================#
# Sliders Options
#=============================================================#
global $jozoor_slider1_animationloop, $jozoor_slider1_slideshow, $jozoor_slider1_slideshowSpeed, $jozoor_slider1_animationSpeed, $jozoor_slider1_pauseOnHover, $jozoor_slider1_pauseOnAction, $jozoor_slider1_directionNav, $jozoor_slider2_animationloop, $jozoor_slider2_slideshow, $jozoor_slider2_slideshowSpeed, $jozoor_slider2_animationSpeed, $jozoor_slider2_pauseOnHover, $jozoor_slider2_pauseOnAction, $jozoor_slider2_directionNav ;

// Slider 1 Settings
if(isset($smof_data['slider1_animationloop'])) { $jozoor_slider1_animationloop = $smof_data['slider1_animationloop']; }
if(isset($smof_data['slider1_slideshow'])) { $jozoor_slider1_slideshow = $smof_data['slider1_slideshow']; }
if(isset($smof_data['slider1_slideshowSpeed'])) { $jozoor_slider1_slideshowSpeed = $smof_data['slider1_slideshowSpeed']; }
if(isset($smof_data['slider1_animationSpeed'])) { $jozoor_slider1_animationSpeed = $smof_data['slider1_animationSpeed']; }
if(isset($smof_data['slider1_pauseOnHover'])) { $jozoor_slider1_pauseOnHover = $smof_data['slider1_pauseOnHover']; }
if(isset($smof_data['slider1_pauseOnAction'])) { $jozoor_slider1_pauseOnAction = $smof_data['slider1_pauseOnAction']; }
if(isset($smof_data['slider1_directionNav'])) { $jozoor_slider1_directionNav = $smof_data['slider1_directionNav']; }
// Slider 2 Settings
if(isset($smof_data['slider2_animationloop'])) { $jozoor_slider2_animationloop = $smof_data['slider2_animationloop']; }
if(isset($smof_data['slider2_slideshow'])) { $jozoor_slider2_slideshow = $smof_data['slider2_slideshow']; }
if(isset($smof_data['slider2_slideshowSpeed'])) { $jozoor_slider2_slideshowSpeed = $smof_data['slider2_slideshowSpeed']; }
if(isset($smof_data['slider2_animationSpeed'])) { $jozoor_slider2_animationSpeed = $smof_data['slider2_animationSpeed']; }
if(isset($smof_data['slider2_pauseOnHover'])) { $jozoor_slider2_pauseOnHover = $smof_data['slider2_pauseOnHover']; }
if(isset($smof_data['slider2_pauseOnAction'])) { $jozoor_slider2_pauseOnAction = $smof_data['slider2_pauseOnAction']; }
if(isset($smof_data['slider2_directionNav'])) { $jozoor_slider2_directionNav = $smof_data['slider2_directionNav']; }


#=============================================================#
# Home Options
#=============================================================#



#=============================================================#
# Portfolio Options
#=============================================================#
global $jozoor_custom_projects_per_page, $jozoor_portfolio4_projects_per_page, $jozoor_portfolio3_projects_per_page, $jozoor_portfolio2_projects_per_page, $jozoor_single_project_current_page,
$jozoor_single_details_project, $jozoor_single_share_buttons_project, $jozoor_single_related_projects_project, $jozoor_single_comments_project, $jozoor_projects_category_current_page,
$jozoor_projects_category_layout_style ;

if(isset($smof_data['custom_projects_per_page'])) { $jozoor_custom_projects_per_page = $smof_data['custom_projects_per_page']; }
if(isset($smof_data['portfolio4_projects_per_page'])) { $jozoor_portfolio4_projects_per_page = $smof_data['portfolio4_projects_per_page']; }
if(isset($smof_data['portfolio3_projects_per_page'])) { $jozoor_portfolio3_projects_per_page = $smof_data['portfolio3_projects_per_page']; }
if(isset($smof_data['portfolio2_projects_per_page'])) { $jozoor_portfolio2_projects_per_page = $smof_data['portfolio2_projects_per_page']; }
if(isset($smof_data['single_project_current_page'])) { $jozoor_single_project_current_page = $smof_data['single_project_current_page']; }
if(isset($smof_data['single_details_project'])) { $jozoor_single_details_project = $smof_data['single_details_project']; }
if(isset($smof_data['single_share_buttons_project'])) { $jozoor_single_share_buttons_project = $smof_data['single_share_buttons_project']; }
if(isset($smof_data['single_related_projects_project'])) { $jozoor_single_related_projects_project = $smof_data['single_related_projects_project']; }
if(isset($smof_data['single_comments_project'])) { $jozoor_single_comments_project = $smof_data['single_comments_project']; }
if(isset($smof_data['projects_category_current_page'])) { $jozoor_projects_category_current_page = $smof_data['projects_category_current_page']; }
if(isset($smof_data['projects_category_layout_style'])) { $jozoor_projects_category_layout_style = $smof_data['projects_category_layout_style']; }


#=============================================================#
# Blog Options
#=============================================================#
global $jozoor_single_post_tags, $jozoor_single_prev_next_links, $jozoor_single_about_author, $jozoor_single_related_posts, $jozoor_single_related_posts_num, $jozoor_single_custom_entry, $jozoor_custom_content_top_entry, $jozoor_custom_content_down_entry, $jozoor_single_post_current_page, $jozoor_single_layout_page_style, $jozoor_single_sidebar_type, $jozoor_other_blog_pages_current_page, $jozoor_other_blog_pages_layout_style, $jozoor_other_blog_pages_sidebar_type, $jozoor_other_blog_pages_blog_style, $jozoor_custom_posts_per_page,
$jozoor_blog1_posts_per_page, $jozoor_blog2_posts_per_page, $jozoor_blog3_posts_per_page  ;

if(isset($smof_data['single_post_tags'])) { $jozoor_single_post_tags = $smof_data['single_post_tags']; }
if(isset($smof_data['single_prev_next_links'])) { $jozoor_single_prev_next_links = $smof_data['single_prev_next_links']; }
if(isset($smof_data['single_about_author'])) { $jozoor_single_about_author = $smof_data['single_about_author']; }
if(isset($smof_data['single_related_posts'])) { $jozoor_single_related_posts = $smof_data['single_related_posts']; }
if(isset($smof_data['single_related_posts_num'])) { $jozoor_single_related_posts_num = $smof_data['single_related_posts_num']; }
if(isset($smof_data['single_custom_entry'])) { $jozoor_single_custom_entry = $smof_data['single_custom_entry']; }
if(isset($smof_data['custom_content_top_entry'])) { $jozoor_custom_content_top_entry = $smof_data['custom_content_top_entry']; }
if(isset($smof_data['custom_content_down_entry'])) { $jozoor_custom_content_down_entry = $smof_data['custom_content_down_entry']; }
if(isset($smof_data['single_post_current_page'])) { $jozoor_single_post_current_page = $smof_data['single_post_current_page']; }
if(isset($smof_data['single_layout_page_style'])) { $jozoor_single_layout_page_style = $smof_data['single_layout_page_style']; }
if(isset($smof_data['single_sidebar_type'])) { $jozoor_single_sidebar_type = $smof_data['single_sidebar_type']; }
if(isset($smof_data['other_blog_pages_current_page'])) { $jozoor_other_blog_pages_current_page = $smof_data['other_blog_pages_current_page']; }
if(isset($smof_data['other_blog_pages_layout_style'])) { $jozoor_other_blog_pages_layout_style = $smof_data['other_blog_pages_layout_style']; }
if(isset($smof_data['other_blog_pages_sidebar_type'])) { $jozoor_other_blog_pages_sidebar_type = $smof_data['other_blog_pages_sidebar_type']; }
if(isset($smof_data['other_blog_pages_blog_style'])) { $jozoor_other_blog_pages_blog_style = $smof_data['other_blog_pages_blog_style']; }
if(isset($smof_data['custom_posts_per_page'])) { $jozoor_custom_posts_per_page = $smof_data['custom_posts_per_page']; }
if(isset($smof_data['blog1_posts_per_page'])) { $jozoor_blog1_posts_per_page = $smof_data['blog1_posts_per_page']; }
if(isset($smof_data['blog2_posts_per_page'])) { $jozoor_blog2_posts_per_page = $smof_data['blog2_posts_per_page']; }
if(isset($smof_data['blog3_posts_per_page'])) { $jozoor_blog3_posts_per_page = $smof_data['blog3_posts_per_page']; }


#=============================================================#
# Services Options
#=============================================================#
global $jozoor_single_service_post_current_page, $jozoor_single_service_layout_page_style, $jozoor_single_service_sidebar_type, $jozoor_single_comments_service ;

if(isset($smof_data['single_service_post_current_page'])) { $jozoor_single_service_post_current_page = $smof_data['single_service_post_current_page']; }
if(isset($smof_data['single_service_layout_page_style'])) { $jozoor_single_service_layout_page_style = $smof_data['single_service_layout_page_style']; }
if(isset($smof_data['single_service_sidebar_type'])) { $jozoor_single_service_sidebar_type = $smof_data['single_service_sidebar_type']; }
if(isset($smof_data['single_comments_service'])) { $jozoor_single_comments_service = $smof_data['single_comments_service']; }


#=============================================================#
# Galleries Options
#=============================================================#
global $jozoor_custom_galleries_per_page, $jozoor_col4_galleries_per_page, $jozoor_col3_galleries_per_page, $jozoor_col2_galleries_per_page,
$jozoor_gallery_page_current_page, $jozoor_gallery_page_comments ;

if(isset($smof_data['custom_galleries_per_page'])) { $jozoor_custom_galleries_per_page = $smof_data['custom_galleries_per_page']; }
if(isset($smof_data['col4_galleries_per_page'])) { $jozoor_col4_galleries_per_page = $smof_data['col4_galleries_per_page']; }
if(isset($smof_data['col3_galleries_per_page'])) { $jozoor_col3_galleries_per_page = $smof_data['col3_galleries_per_page']; }
if(isset($smof_data['col2_galleries_per_page'])) { $jozoor_col2_galleries_per_page = $smof_data['col2_galleries_per_page']; }
if(isset($smof_data['gallery_page_current_page'])) { $jozoor_gallery_page_current_page = $smof_data['gallery_page_current_page']; }
if(isset($smof_data['gallery_page_comments'])) { $jozoor_gallery_page_comments = $smof_data['gallery_page_comments']; }


?>