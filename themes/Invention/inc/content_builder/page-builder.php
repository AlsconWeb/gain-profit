<?php

$prefix = '_jozoor_'; // Prefix for all fields

return array(
    
        
        array(
          'type' => 'notebox',
          'name' => $prefix . 'page_builder_info_text',
          'label' => __( 'Create unlimited sections, sort, delete or do what want', 'jozoorthemes' ),
          'description' => '',
          'status' => 'info',
        ),    
        array(
            'type'      => 'group',
            'repeating' => true,
            'sortable'  => true,
            'name'      => $prefix . 'page_section',
            'title'     => __('Section', 'jozoorthemes'),
            'fields'    => array(
                
                array(
                 'type' => 'textbox',
                 'name' => 'section_name',
                 'label' => __( 'Section Name', 'jozoorthemes' ),
                 'description' => __( 'notes : <br> 1- this name does\'t appear in the content <br> 2- if you choose [ onepage ] type This name should be the same in onepage menu link text,
so if you named section example : ( our work ) you should add it also when you create menu link item in section [ Link Text ]', 'jozoorthemes' ),
                 'default' => '',
                 'validation' => '',
                 ),
                array(
            'type' => 'radiobutton',
            'name' => 'section_bg_type',
            'label' => __( 'Section Background Type', 'jozoorthemes' ),
            'description' => __( 'choose type : this option required', 'jozoorthemes' ),
            'items' => array(
               array(
                'value' => 'color',
                'label' => __( 'Color', 'jozoorthemes' ),
               ),
               array(
                'value' => 'image',
                'label' => __( 'Image', 'jozoorthemes' ),
               ),
             ),
            'default' => array(
            'color',
              ),
             'validation' => '',
             ),
              array(
            'type' => 'select',
            'name' => 'section_bg_type_color',
            'label' => '<a>&#8627; '.__( 'Select Color', 'jozoorthemes' ).'</a>',
            'description' => __( 'default : body color', 'jozoorthemes' ),
            'items' => array(
               array(
                'value' => 'bodycolor',
                'label' => __( 'Body Color', 'jozoorthemes' ),
               ),
               array(
                'value' => 'defaultcolor',
                'label' => __( 'Default Color', 'jozoorthemes' ),
               ),
               array(
                'value' => 'themeskincolor',
                'label' => __( 'Theme Skin Color', 'jozoorthemes' ),
               ),
               array(
                'value' => 'customcolor',
                'label' => __( 'Custom Color', 'jozoorthemes' ),
               ),
             ),
            'default' => array(
            'bodycolor',
           ),
            'dependency' => array(
						'field'    => 'section_bg_type',
						'function' => 'vp_dep_is_color',
					),
             ),
            array(
            'type' => 'upload',
            'name' => 'section_bg_type_image',
            'label' => '<a>&#8627; '.__( 'Upload Image', 'jozoorthemes' ).'</a>',
            'description' => __( 'upload background image for section', 'jozoorthemes' ),
            'default' => '',
                'dependency' => array(
						'field'    => 'section_bg_type',
						'function' => 'vp_dep_is_image',
					),
            ),
            array(
            'type' => 'select',
            'name' => 'section_bg_type_image_repeat',
            'label' => '<a>&#8627;&#8627; '.__( 'Image Repeat', 'jozoorthemes' ).'</a>',
            'description' => __( 'default : no-repeat', 'jozoorthemes' ),
            'items' => array(
               array(
                'value' => 'no-repeat',
                'label' => __( 'no-repeat', 'jozoorthemes' ),
               ),
               array(
                'value' => 'repeat',
                'label' => __( 'repeat', 'jozoorthemes' ),
               ),
             ),
            'default' => array(
            'no-repeat',
             ),
             'dependency' => array(
						'field'    => 'section_bg_type',
						'function' => 'vp_dep_is_image',
					),
           ),
            array(
        'type' => 'toggle',
        'name' => 'section_bg_type_image_parallax',
        'label' => '<a>&#8627;&#8627; '.__('Parallax Background', 'jozoorthemes').'</a>',
        'description' => __( 'check this option to parallax background image', 'jozoorthemes' ),
        'default' => '0',
                'dependency' => array(
						'field'    => 'section_bg_type',
						'function' => 'vp_dep_is_image',
					),
           ),
                
            array(
           'type' => 'color',
           'name' => 'section_bg_type_color_custom',
           'label' => '<a>&#8627;&#8627; '.__( 'Choose Custom color', 'jozoorthemes' ).'</a>',
           'description' => __( 'pick custom color for section', 'jozoorthemes' ),
           'default' => '',
           'format' => 'rgba',
                 'dependency' => array(
						'field'    => 'section_bg_type_color',
						'function' => 'vp_dep_is_customcolor',
					),
            ),
                
            array(
           'type' => 'color',
           'name' => 'section_content_text_color',
           'label' => __( 'Content Text Color', 'jozoorthemes' ),
           'description' => __( 'pick custom color for content text and links, don\'t pick any color to make it default', 'jozoorthemes' ),
           'default' => '',
           'format' => 'HEX',
            ),
            
            array(
        'type' => 'toggle',
        'name' => 'section_remove_padding_top',
        'label' => __('Remove Top Padding', 'jozoorthemes'),
        'description' => __( 'check this option to make padding top = 0', 'jozoorthemes' ),
        'default' => '0',
           ), 
            array(
        'type' => 'toggle',
        'name' => 'section_remove_padding_bottom',
        'label' => __('Remove Bottom Padding', 'jozoorthemes'),
        'description' => __( 'check this option to make padding bottom = 0', 'jozoorthemes' ),
        'default' => '0',
           ),
            array(
          'type' => 'notebox',
          'name' => 'columns_info_text',
          'label' => __( 'Create section columns', 'jozoorthemes' ),
          'description' => '',
          'status' => 'info',
        ), 
                array(
                    'type'      => 'group',
                    'repeating' => true,
                    'sortable'  => true,
                    'name'      => 'section_column',
                    'title'     => __('Column', 'jozoorthemes'),
                    'fields'    => array(
                        
                        array(
                        'type' => 'select',
                        'name' => 'section_column_width',
                        'label' => __('Column Width', 'jozoorthemes'),
                        'description' => __( 'default : full width', 'jozoorthemes' ),
                        'items' => array(
                           array( 'value' => 'sixteen columns', 'label' => __( 'Full Width', 'jozoorthemes' ), ),
                           array( 'value' => 'eight columns', 'label' => __( '1/2 Column', 'jozoorthemes' ), ),
                           array( 'value' => 'one-third column', 'label' => __( '1/3 Column', 'jozoorthemes' ), ),
                           array( 'value' => 'two-thirds column', 'label' => __( '2/3 Column', 'jozoorthemes' ), ),
                           array( 'value' => 'four columns', 'label' => __( '1/4 Column', 'jozoorthemes' ), ),          
                           array( 'value' => 'eleven columns', 'label' => __( '11 Column', 'jozoorthemes' ), ),
                           array( 'value' => 'five columns', 'label' => __( '5 Column', 'jozoorthemes' ), ),
                           array( 'value' => 'twelve columns', 'label' => __( '12 Column', 'jozoorthemes' ), ),
                         ),
                         'default' => array(
                         'sixteen columns',
                         ),
                        ),
                        
                        array(
                        'type' => 'wpeditor',
                        'name' => 'section_column_content',
                        'label' => __( 'Content', 'jozoorthemes' ),
                        'description' => __( 'you can add normal content or shortcodes just click on shortcode icon and select what you want , after that insert to editor', 'jozoorthemes' ),
                        'use_external_plugins' => '1',
                        'disabled_externals_plugins' => '',
                        'disabled_internals_plugins' => '',
                        ),
                        
                        
                    ),
                ),
                
            ),
        ),
    
);

?>