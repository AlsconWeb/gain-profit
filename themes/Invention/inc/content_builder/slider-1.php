<?php 

$prefix = '_jozoor_'; // Prefix for all fields

return array(
    
    array(
        'type' => 'notebox',
        'name' => $prefix . 'sliders_info_text',
        'label' => __( 'Create unlimited slides, sort, delete or do what want - you can see any slider before define it to any page, just click on view slider under title', 'jozoorthemes' ),
        'description' => '',
        'status' => 'info',
    ),
    
    array(
    'type'      => 'group',
    'repeating' => true,
    'name'      => $prefix . 'slider_one_slides',
    'title'     => __('Slide', 'jozoorthemes'),
    'sortable' => true,
    'fields'    => array(
        
        array(
            'type' => 'upload',
            'name' => 'slide_bg_image',
            'label' => __( 'Background Image', 'jozoorthemes' ),
            'description' => __( 'upload background image for slide', 'jozoorthemes' ),
            'default' => '',
        ),
        array(
            'type' => 'select',
            'name' => 'slide_bg_image_repeat',
            'label' => '<a>&#8627; '.__( 'Background Repeat', 'jozoorthemes' ).'</a>',
            'description' => __( 'default no-repeat', 'jozoorthemes' ),
            'items' => array(
               array(
                'value' => 'no-repeat',
                'label' => __( 'no-repeat', 'jozoorthemes' ),
               ),
               array(
                'value' => 'repeat',
                'label' => __( 'repeat', 'jozoorthemes' ),
               ),
             ),
            'default' => array(
            'no-repeat',
           ),
        ),
        array(
           'type' => 'color',
           'name' => 'slide_bg_color',
           'label' => '<a>&#8627; '.__( 'Background color', 'jozoorthemes' ).'</a>',
           'description' => __( 'this color will used when you don\'t upload image<br> don\'t pick any color to get default theme skin color', 'jozoorthemes' ),
           'default' => '',
           'format' => 'rgba',
        ),
       array(
        'type' => 'textbox',
        'name' => 'slide_title',
        'label' => __( 'Slide Title', 'jozoorthemes' ),
        'description' => __( 'if you want to hidden title, let field empty', 'jozoorthemes' ),
        'default' => '',
        'validation' => '',
       ),
       array(
        'type' => 'slider',
        'name' => 'slide_title_top',
        'label' => '<a>&#8627; '.__( 'Title Top Position', 'jozoorthemes' ).'</a>',
        'description' => __( 'this number will define title position from the top, default is : 20', 'jozoorthemes' ),
        'min' => '0',
        'max' => '100',
        'step' => '1',
        'default' => '20',
      ),
      array(
        'type' => 'textarea',
        'name' => 'slide_desc',
        'label' => __( 'Slide Description', 'jozoorthemes' ),
        'description' => __( 'if you want to hidden description, let field empty', 'jozoorthemes' ),
        'default' => '',
        'validation' => '',
      ),
      array(
        'type' => 'slider',
        'name' => 'slide_desc_bottom',
        'label' => '<a>&#8627; '.__( 'Description Bottom Position', 'jozoorthemes' ).'</a>',
        'description' => __( 'this number will define description position from the bottom, default is : 39', 'jozoorthemes' ),
        'min' => '0',
        'max' => '100',
        'step' => '1',
        'default' => '39',
      ),
      array(
           'type' => 'color',
           'name' => 'slide_title_desc_color',
           'label' => '<a>&#8627; '.__( 'Title / Description Color', 'jozoorthemes' ).'</a>',
           'description' => __( 'default is white, don\'t pick any color if you need white', 'jozoorthemes' ),
           'default' => '',
           'format' => 'HEX',
        ),
      array(
            'type' => 'wpeditor',
            'name' => 'slide_buttons',
            'label' => __( 'Slide Buttons', 'jozoorthemes' ),
            'description' => __( 'just click on shortcode icon and select button to create your buttons, after that insert to editor <br> if you want to hidden buttons, let editor empty', 'jozoorthemes' ),
            'use_external_plugins' => '1',
            'disabled_externals_plugins' => '',
            'disabled_internals_plugins' => '',
        ),
        array(
        'type' => 'slider',
        'name' => 'slide_buttons_bottom',
        'label' => '<a>&#8627; '.__( 'Buttons Bottom Position', 'jozoorthemes' ).'</a>',
        'description' => __( 'this number will define buttons position from the bottom, default is : 20', 'jozoorthemes' ),
        'min' => '0',
        'max' => '100',
        'step' => '1',
        'default' => '20',
      ),
      array(
            'type' => 'upload',
            'name' => 'slide_image',
            'label' => __( 'Slide Image', 'jozoorthemes' ),
            'description' => __( 'upload slide image', 'jozoorthemes' ),
            'default' => '',
        ),
       array(
        'type' => 'slider',
        'name' => 'slide_image_top',
        'label' => '<a>&#8627; '.__( 'Image Top Position', 'jozoorthemes' ).'</a>',
        'description' => __( 'this number will define image position from the top, default is : 16', 'jozoorthemes' ),
        'min' => '0',
        'max' => '100',
        'step' => '1',
        'default' => '16',
      ),
      array(
        'type' => 'textbox',
        'name' => 'slide_global_link',
        'label' => __( 'Add Link to title/description/image', 'jozoorthemes' ),
        'description' => __( 'this link will be active on this sections, let field empty if you don\'t need link', 'jozoorthemes' ),
        'default' => '',
        'validation' => '',
       ),
        
        
    ),
),
    
    
    
);

?>