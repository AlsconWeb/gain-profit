<?php
/* Custom Post Type ( Sliders ) */

// register post type
add_action('init', 'jozoor_post_type_sliders');

function jozoor_post_type_sliders() {

  $labels = array(
  'name' => _x('Sliders', 'post type general name', 'jozoorthemes'),
  'singular_name' => _x('Slider', 'post type singular name', 'jozoorthemes'),
  'add_new' => _x('Add New', 'sliders', 'jozoorthemes'),
  'add_new_item' => __('Add New', 'jozoorthemes'),
  'edit_item' => __('Edit Slider', 'jozoorthemes'),
  'new_item' => __('New Slider', 'jozoorthemes'),
  'all_items' => __('All Sliders', 'jozoorthemes'),
  'view_item' => __('View Slider', 'jozoorthemes'),
  'search_items' => __('Search Slider', 'jozoorthemes'),
  'not_found' =>  __('No Sliders found', 'jozoorthemes'),
  'not_found_in_trash' => __('No Sliders found in Trash', 'jozoorthemes'), 
  'parent_item_colon' => '',
  'menu_name' => __('Sliders', 'jozoorthemes')

  );

  $sliders_args = array(
  'labels' => $labels,
  'public' => true,
  'publicly_queryable' => true,
  'show_ui' => true, 
  'show_in_menu' => true, 
  'query_var' => true,
  'capability_type' => 'post',
  'exclude_from_search ' => true,
  'hierarchical' => false,
  'rewrite' => array('slug' => 'slider-item', 'with_front' => false),
  'has_archive' => false, 
  'menu_position' => 101,
  'menu_icon' => get_template_directory_uri().'/images/admin-icons/sliders.png',
  'supports' => array( 'title' ),
  );
    
  register_post_type('sliders',$sliders_args);
    
}


// Customizing the messages
function jozoor_sliders_updated_messages( $messages ) {
    
  global $post, $post_ID;

  $messages['sliders'] = array(
  0 => '', 
  1 => sprintf( __('Slider updated.', 'jozoorthemes'), esc_url( get_permalink($post_ID) ) ),
  2 => __('Custom field updated.', 'jozoorthemes'),
  3 => __('Custom field deleted.', 'jozoorthemes'),
  4 => __('Slider updated.', 'jozoorthemes'),
  5 => isset($_GET['revision']) ? sprintf( __('Slider restored to revision from %s', 'jozoorthemes'), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
  6 => sprintf( __('Slider published.', 'jozoorthemes'), esc_url( get_permalink($post_ID) ) ),
  7 => __('Slider saved.', 'jozoorthemes'),
  8 => sprintf( __('Slider submitted.', 'jozoorthemes'), esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
  9 => sprintf( __('Slider scheduled for: <strong>%1$s</strong>.', 'jozoorthemes'),
  date_i18n( __( 'M j, Y @ G:i', 'jozoorthemes' ), strtotime( $post->post_date ) ), esc_url( get_permalink($post_ID) ) ),
  10 => sprintf( __('Slider draft updated.', 'jozoorthemes'), esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
  );

  return $messages;
    
}
add_filter( 'post_updated_messages', 'jozoor_sliders_updated_messages' ); 

// slider edit column
add_filter("manage_edit-sliders_columns", "jozoor_slider_edit_columns");

function jozoor_slider_edit_columns($columns) {
        $columns = array(
            "cb" => "<input type=\"checkbox\" />",
            "title" => __("Slider", "jozoorthemes"),
            "date" => __("Date", "jozoorthemes"),
        );
        return $columns;
}

// ======= sliders metaboxs ======= //


function jozoor_metaboxs_options_sliders( $meta_boxes ) {
    
$prefix = '_jozoor_'; // Prefix for all fields
    
    $meta_boxes['jozoor-metabox-sliders-type'] = array(
        'id' => 'jozoor-metabox-sliders-type',
        'title' => __('Slider Type', 'jozoorthemes'),
        'pages' => array('sliders'), // post type
        'context' => 'normal',
        'priority' => 'high',
        'show_names' => true, // Show field names on the left
        'fields' => array(
            
            array(
				'name' => __( 'Choose Type', 'jozoorthemes' ),
				'desc' => '',
				'id'   => $prefix . 'slider_type',
				'type'    => 'radio_inline',
                'default' => 'slider1',
				'options' => array(
					'slider1' => __( 'Slider 1', 'jozoorthemes' ),
					'slider2' => __( 'Slider 2', 'jozoorthemes' ),
                ),
            ),
            array(
				'name' => __( 'Slider Height', 'jozoorthemes' ),
				'desc' => __( 'this number will define slider height, default is : 478', 'jozoorthemes' ),
				'id'   => $prefix . 'slider_min_height',
				'type' => 'text_small',
                'attributes' => array(
                'placeholder' => 'EX: 478',
                ),
            ),
            
        ),
    );
    
return $meta_boxes;
}
add_filter( 'cmb_meta_boxes', 'jozoor_metaboxs_options_sliders' );


?>