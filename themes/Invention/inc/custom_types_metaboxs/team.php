<?php
/* Custom Post Type ( Team ) */

// register post type
add_action('init', 'jozoor_post_type_team');

function jozoor_post_type_team() {

  $labels = array(
  'name' => _x('Team', 'post type general name', 'jozoorthemes'),
  'singular_name' => _x('Member', 'post type singular name', 'jozoorthemes'),
  'add_new' => _x('Add New', 'team', 'jozoorthemes'),
  'add_new_item' => __('Add New', 'jozoorthemes'),
  'edit_item' => __('Edit Member', 'jozoorthemes'),
  'new_item' => __('New Member', 'jozoorthemes'),
  'all_items' => __('All Team', 'jozoorthemes'),
  'view_item' => __('View Member', 'jozoorthemes'),
  'search_items' => __('Search Member', 'jozoorthemes'),
  'not_found' =>  __('No Members found', 'jozoorthemes'),
  'not_found_in_trash' => __('No Member found in Trash', 'jozoorthemes'), 
  'parent_item_colon' => '',
  'menu_name' => __('Team', 'jozoorthemes')

  );

  $team_args = array(
  'labels' => $labels,
  'public' => false,
  'publicly_queryable' => true,
  'show_ui' => true, 
  'show_in_menu' => true, 
  'query_var' => true,
  'capability_type' => 'post',
  'exclude_from_search ' => true,
  'hierarchical' => false,
  'rewrite' => array('slug' => 'team-item', 'with_front' => false),
  'has_archive' => false, 
  'menu_position' => 101,
  'menu_icon' => get_template_directory_uri().'/images/admin-icons/team.png',
  'supports' => array( 'title', 'editor', 'thumbnail' ),
  );
    
  register_post_type('team',$team_args);
    
}


// Customizing the messages
function jozoor_team_updated_messages( $messages ) {
    
  global $post, $post_ID;

  $messages['team'] = array(
  0 => '', 
  1 => sprintf( __('Member updated.', 'jozoorthemes'), esc_url( get_permalink($post_ID) ) ),
  2 => __('Custom field updated.', 'jozoorthemes'),
  3 => __('Custom field deleted.', 'jozoorthemes'),
  4 => __('Member updated.', 'jozoorthemes'),
  5 => isset($_GET['revision']) ? sprintf( __('Member restored to revision from %s', 'jozoorthemes'), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
  6 => sprintf( __('Member published.', 'jozoorthemes'), esc_url( get_permalink($post_ID) ) ),
  7 => __('Member saved.', 'jozoorthemes'),
  8 => sprintf( __('Member submitted.', 'jozoorthemes'), esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
  9 => sprintf( __('Member scheduled for: <strong>%1$s</strong>.', 'jozoorthemes'),
  date_i18n( __( 'M j, Y @ G:i', 'jozoorthemes' ), strtotime( $post->post_date ) ), esc_url( get_permalink($post_ID) ) ),
  10 => sprintf( __('Member draft updated.', 'jozoorthemes'), esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
  );

  return $messages;
    
}
add_filter( 'post_updated_messages', 'jozoor_team_updated_messages' ); 

// team edit column
add_filter("manage_edit-team_columns", "jozoor_team_edit_columns");

function jozoor_team_edit_columns($columns) {
        $columns = array(
            "cb" => "<input type=\"checkbox\" />",
            "title" => __("Member", "jozoorthemes"),
            "typeteam" => __("Category", "jozoorthemes"),
            "featured" => __("Featured Image", "jozoorthemes"),
            "date" => __("Date", "jozoorthemes"),
        );
        return $columns;
}

// custom project column
add_action("manage_posts_custom_column",  "jozoor_team_custom_columns");

function jozoor_team_custom_columns($column) {
    
    global $post_ID;
    switch ($column) {
    case "typeteam":
    $terms3 =  get_the_term_list($post_ID, 'team-category', '', ', ','');
    if ( is_string( $terms3 ) ) {
       echo strip_tags($terms3);
    } else {
       echo __("No Categories Found", "jozoorthemes");
    }
    break;
        
    }
    
}


// register new taxonomy
register_taxonomy(
    "team-category", array("team"), array("hierarchical" => true, "label" => __("Team Categories", "jozoorthemes"), "singular_label" => __("Category", "jozoorthemes"), "rewrite" => true, "show_in_nav_menus" => false)
); 

// ======= team metaboxs ======= //

function jozoor_metaboxs_options_team( $meta_boxes ) {
    
      // social icons list
      $jozoor_shortcode_social_icons_list = array(
          'twitter' => 'twitter', 'facebook' => 'facebook', 'behance' => 'behance', 'flickr' => 'flickr', 'gplus' => 'gplus', 'linkedin' => 'linkedin', 'instagram' => 'instagram', 
          'dribbble' => 'dribbble', 'tumblr' => 'tumblr', 'youtube' => 'youtube', 'vimeo' => 'vimeo', 'deviantart' => 'deviantart', 'digg' => 'digg', 'gmail' => 'gmail', 
          'delicious' => 'delicious', 'skype' => 'skype', 'forrst' => 'forrst', 'reddit' => 'reddit', 'pinterest' => 'pinterest', 'foursquare' => 'foursquare', 'soundcloud' => 'soundcloud',
          'github' => 'github', 'github-circled' => 'github-circled', 'stackoverflow' => 'stackoverflow', 'wordpress' => 'wordpress', 'blogger' => 'blogger',  'google' => 'google',
          'yahoo' => 'yahoo', 'dropbox' => 'dropbox', 'paypal' => 'paypal',  'android' => 'android', 'rss' => 'rss', 'scribd' => 'scribd', 'disqus' => 'disqus',
          'duckduckgo' => 'duckduckgo', 'aim' => 'aim', 'flattr' => 'flattr', 'eventful' => 'eventful', 'smashmag' => 'smashmag', 'wikipedia' => 'wikipedia', 'lanyrd' => 'lanyrd', 
          'stumbleupon' => 'stumbleupon', 'fivehundredpx' => 'fivehundredpx', 'bitcoin' => 'bitcoin', 'w3c' => 'w3c', 'html5' => 'html5', 'ie' => 'ie', 'grooveshark' => 'grooveshark',                             'ninetyninedesigns' => 'ninetyninedesigns', 'spotify' => 'spotify', 'gowalla' => 'gowalla', 'appstore' => 'appstore', 'cc' => 'cc', 'evernote' => 'evernote', 
          'viadeo' => 'viadeo', 'instapaper' => 'instapaper', 'weibo' => 'weibo', 'klout' => 'klout',  'meetup' => 'meetup', 'vk' => 'vk', 'plancast' => 'plancast',  'windows' => 'windows', 
          'xing' => 'xing', 'chrome' => 'chrome', 'myspace' => 'myspace','podcast' => 'podcast', 'amazon' => 'amazon', 'steam' => 'steam', 
          'cloudapp' => 'cloudapp', 'ebay' => 'ebay', 'googleplay' => 'googleplay', 'itunes' => 'itunes', 'plurk' => 'plurk', 'songkick' => 'songkick', 'lastfm' => 'lastfm',  
          'openid' => 'openid', 'quora' => 'quora', 'eventasaurus' => 'eventasaurus', 'yelp' => 'yelp', 'intensedebate' => 'intensedebate', 'eventbrite' => 'eventbrite', 
          'posterous' => 'posterous', 'stripe' => 'stripe', 'opentable' => 'opentable', 'angellist' => 'angellist', 'dwolla' => 'dwolla', 'appnet' => 'appnet', 'statusnet' => 'statusnet', 
          'drupal' => 'drupal', 'buffer' => 'buffer', 'pocket' => 'pocket',  'bitbucket' => 'bitbucket', 'lego' => 'lego', 'hackernews' => 'hackernews', 'lkdto' => 'lkdto'
      );
  
      
    
$prefix = '_jozoor_'; // Prefix for all fields
    
    $meta_boxes['jozoor-metabox-team'] = array(
        'id' => 'jozoor-metabox-team',
        'title' => __('Member Options', 'jozoorthemes'),
        'pages' => array('team'), // post type
        'context' => 'normal',
        'priority' => 'high',
        'show_names' => true, // Show field names on the left
        'fields' => array(
            
            array(
				'id'          => $prefix . 'member_social_icon_url',
				'type'        => 'group',
				'description' => __( 'Add social network name and url for team member', 'jozoorthemes' ),
				'options'     => array(
					'add_button'    => __( 'Add New Network', 'jozoorthemes' ),
					'remove_button' => __( 'Remove Network', 'jozoorthemes' ),
					'sortable'      => true, // beta
				),
				
				'fields'      => array(
                    array(
				        'name'    => __( 'Select Social Network', 'jozoorthemes' ),
				        'id'      => 'member_social_icon_name',
				        'type'    => 'select',
				        'options' => $jozoor_shortcode_social_icons_list
			        ),
					array(
						'name' => __( 'Social Network Link', 'jozoorthemes' ),
						'id'   => 'member_social_url',
                        'desc' => '',
						'type' => 'text_url',
					),
					
				),
			),
            
        ),
    );
    
    
return $meta_boxes;
}
add_filter( 'cmb_meta_boxes', 'jozoor_metaboxs_options_team' );

?>