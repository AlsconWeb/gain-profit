<?php
/* Custom Post Type ( Clients ) */

// register post type
add_action('init', 'jozoor_post_type_clients');

function jozoor_post_type_clients() {

  $labels = array(
  'name' => _x('Clients', 'post type general name', 'jozoorthemes'),
  'singular_name' => _x('Client', 'post type singular name', 'jozoorthemes'),
  'add_new' => _x('Add New', 'clients', 'jozoorthemes'),
  'add_new_item' => __('Add New', 'jozoorthemes'),
  'edit_item' => __('Edit Client', 'jozoorthemes'),
  'new_item' => __('New Client', 'jozoorthemes'),
  'all_items' => __('All Clients', 'jozoorthemes'),
  'view_item' => __('View Client', 'jozoorthemes'),
  'search_items' => __('Search Client', 'jozoorthemes'),
  'not_found' =>  __('No Clients found', 'jozoorthemes'),
  'not_found_in_trash' => __('No Clients found in Trash', 'jozoorthemes'), 
  'parent_item_colon' => '',
  'menu_name' => __('Clients', 'jozoorthemes')

  );

  $clients_args = array(
  'labels' => $labels,
  'public' => false,
  'publicly_queryable' => true,
  'show_ui' => true, 
  'show_in_menu' => true, 
  'query_var' => true,
  'capability_type' => 'post',
  'exclude_from_search ' => true,
  'hierarchical' => false,
  'rewrite' => array('slug' => 'clients-item', 'with_front' => false),
  'has_archive' => false, 
  'menu_position' => 101,
  'menu_icon' => get_template_directory_uri().'/images/admin-icons/clients.png',
  'supports' => array( 'title', 'thumbnail' ),
  );
    
  register_post_type('clients',$clients_args);
    
}


// Customizing the messages
function jozoor_clients_updated_messages( $messages ) {
    
  global $post, $post_ID;

  $messages['clients'] = array(
  0 => '', 
  1 => sprintf( __('Client updated.', 'jozoorthemes'), esc_url( get_permalink($post_ID) ) ),
  2 => __('Custom field updated.', 'jozoorthemes'),
  3 => __('Custom field deleted.', 'jozoorthemes'),
  4 => __('Client updated.', 'jozoorthemes'),
  5 => isset($_GET['revision']) ? sprintf( __('Client restored to revision from %s', 'jozoorthemes'), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
  6 => sprintf( __('Client published.', 'jozoorthemes'), esc_url( get_permalink($post_ID) ) ),
  7 => __('Client saved.', 'jozoorthemes'),
  8 => sprintf( __('Client submitted.', 'jozoorthemes'), esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
  9 => sprintf( __('Client scheduled for: <strong>%1$s</strong>.', 'jozoorthemes'),
  date_i18n( __( 'M j, Y @ G:i', 'jozoorthemes' ), strtotime( $post->post_date ) ), esc_url( get_permalink($post_ID) ) ),
  10 => sprintf( __('Client draft updated.', 'jozoorthemes'), esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
  );

  return $messages;
    
}
add_filter( 'post_updated_messages', 'jozoor_clients_updated_messages' ); 

// client edit column
add_filter("manage_edit-clients_columns", "jozoor_client_edit_columns");

function jozoor_client_edit_columns($columns) {
        $columns = array(
            "cb" => "<input type=\"checkbox\" />",
            "title" => __("Client", "jozoorthemes"),
            "featured" => __("Featured Image", "jozoorthemes"),
            "date" => __("Date", "jozoorthemes"),
        );
        return $columns;
}

// ======= client metaboxs ======= //

function jozoor_metaboxs_options_clients( $meta_boxes ) {
    
$prefix = '_jozoor_'; // Prefix for all fields
    
    $meta_boxes['jozoor-metabox-clients'] = array(
        'id' => 'jozoor-metabox-clients',
        'title' => __('Client Options', 'jozoorthemes'),
        'pages' => array('clients'), // post type
        'context' => 'normal',
        'priority' => 'high',
        'show_names' => true, // Show field names on the left
        'fields' => array(
            
            array(
                'name' => __('Client Online URL', 'jozoorthemes'),
                'desc' => __( 'if you want client without link, let field empty', 'jozoorthemes' ),
                'id' => $prefix . 'client_online_url',
                'type' => 'text_url'
            ),
            
        ),
    );
    
    
return $meta_boxes;
}
add_filter( 'cmb_meta_boxes', 'jozoor_metaboxs_options_clients' );

?>