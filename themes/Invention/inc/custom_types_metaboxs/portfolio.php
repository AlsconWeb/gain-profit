<?php
/* Custom Post Type ( Portfolio ) */

// register post type
add_action('init', 'jozoor_post_type_portfolio');

function jozoor_post_type_portfolio() {

  $labels = array(
  'name' => _x('Portfolio', 'post type general name', 'jozoorthemes'),
  'singular_name' => _x('Portfolio', 'post type singular name', 'jozoorthemes'),
  'add_new' => _x('Add New', 'portfolio', 'jozoorthemes'),
  'add_new_item' => __('Add New', 'jozoorthemes'),
  'edit_item' => __('Edit Project', 'jozoorthemes'),
  'new_item' => __('New Project', 'jozoorthemes'),
  'all_items' => __('All Projects', 'jozoorthemes'),
  'view_item' => __('View Project', 'jozoorthemes'),
  'search_items' => __('Search Portfolio', 'jozoorthemes'),
  'not_found' =>  __('No Projects found', 'jozoorthemes'),
  'not_found_in_trash' => __('No Projects found in Trash', 'jozoorthemes'), 
  'parent_item_colon' => '',
  'menu_name' => __('Portfolio', 'jozoorthemes')

  );

  $portfolio_args = array(
  'labels' => $labels,
  'public' => true,
  'publicly_queryable' => true,
  'show_ui' => true, 
  'show_in_menu' => true, 
  'query_var' => true,
  'capability_type' => 'post',
  'hierarchical' => false,
  'rewrite' => array('slug' => 'portfolio-item', 'with_front' => false),
  'has_archive' => true, 
  'menu_position' => 101,
  'menu_icon' => get_template_directory_uri().'/images/admin-icons/portfolio.png',
  'supports' => array( 'title', 'editor', 'thumbnail', 'comments' ),
  );
    
  register_post_type('portfolio',$portfolio_args);
    
}


// Customizing the messages
function jozoor_portfolio_updated_messages( $messages ) {
    
  global $post, $post_ID;

  $messages['portfolio'] = array(
  0 => '', 
  1 => sprintf( __('Project updated. <a href="%s">View Project</a>', 'jozoorthemes'), esc_url( get_permalink($post_ID) ) ),
  2 => __('Custom field updated.', 'jozoorthemes'),
  3 => __('Custom field deleted.', 'jozoorthemes'),
  4 => __('Project updated.', 'jozoorthemes'),
  5 => isset($_GET['revision']) ? sprintf( __('Project restored to revision from %s', 'jozoorthemes'), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
  6 => sprintf( __('Project published. <a href="%s">View Project</a>', 'jozoorthemes'), esc_url( get_permalink($post_ID) ) ),
  7 => __('Project saved.', 'jozoorthemes'),
  8 => sprintf( __('Project submitted. <a target="_blank" href="%s">Preview Portfolio</a>', 'jozoorthemes'), esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
  9 => sprintf( __('Project scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview Project</a>', 'jozoorthemes'),
  date_i18n( __( 'M j, Y @ G:i', 'jozoorthemes' ), strtotime( $post->post_date ) ), esc_url( get_permalink($post_ID) ) ),
  10 => sprintf( __('Project draft updated. <a target="_blank" href="%s">Preview Project</a>', 'jozoorthemes'), esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
  );

  return $messages;
    
}
add_filter( 'post_updated_messages', 'jozoor_portfolio_updated_messages' );


// project edit column
add_filter("manage_edit-portfolio_columns", "jozoor_project_edit_columns");

function jozoor_project_edit_columns($columns) {
        $columns = array(
            "cb" => "<input type=\"checkbox\" />",
            "title" => __("Project", "jozoorthemes"),
            "type" => __("Category", "jozoorthemes"),
            "featured" => __("Featured Image", "jozoorthemes"),
            "date" => __("Date", "jozoorthemes"),
        );
        return $columns;
}


// custom project column
add_action("manage_posts_custom_column",  "jozoor_project_custom_columns");

function jozoor_project_custom_columns($column) {
    
    global $post_ID;
    switch ($column) {
    case "featured":
    $post_featured_image = jozoor_get_featured_image_dash($post_ID);
    if ($post_featured_image) {
       // HAS A FEATURED IMAGE
       echo '<img src="' . $post_featured_image . '" width="110"/>';
    } 
    break;
    case "type":
    $terms2 =  get_the_term_list($post_ID, 'portfolio-category', '', ', ','');
    if ( is_string( $terms2 ) ) {
       echo $terms2;
    } else {
       echo __("No Categories Found", "jozoorthemes");
    }
    break;
        
    }
    
}


// register new taxonomy
register_taxonomy(
    "portfolio-category", array("portfolio"), array("hierarchical" => true, "label" => __("Portfolio Categories", "jozoorthemes"), "singular_label" => __("Category", "jozoorthemes"), "rewrite" => true)
); 


// ======= project metaboxs ======= //

function jozoor_metaboxs_options_portfolio( $meta_boxes ) {
    
$prefix = '_jozoor_'; // Prefix for all fields
    
$meta_boxes['jozoor-metabox-portfolio-format'] = array(
        'id' => 'jozoor-metabox-portfolio-format',
        'title' => __('Project Format', 'jozoorthemes'),
        'pages' => array('portfolio'), // post type
        'context' => 'normal',
        'priority' => 'high',
        'show_names' => true, // Show field names on the left
        'fields' => array(
            
            array(
				'name' => __( 'Choose Format', 'jozoorthemes' ),
				'desc' => '',
				'id'   => $prefix . 'project_format',
				'type'    => 'radio_inline',
                'default' => 'image',
				'options' => array(
					'image' => __( 'Image', 'jozoorthemes' ),
					'video' => __( 'Video', 'jozoorthemes' ),
					'audio' => __( 'Audio', 'jozoorthemes' ),
                ),
            ),
            
        ),
            
    );    
    
#=============================================================#
# Image Options 
#=============================================================#
    $meta_boxes['jozoor-metabox-portfolio-image'] = array(
        'id' => 'jozoor-metabox-portfolio-image',
        'title' => __('Image Options', 'jozoorthemes'),
        'pages' => array('portfolio'), // post type
        'context' => 'normal',
        'priority' => 'high',
        'show_names' => true, // Show field names on the left
        'fields' => array(
            
            array(
				'name' => __( 'Project Images', 'jozoorthemes' ),
				'desc' => __( 'upload any numbers of images', 'jozoorthemes' ),
				'id'   => $prefix . 'project_images',
				'type' => 'file_list',
                'preview_size' => array( 150, 150 ),
			),
            array(
				'name' => __( 'Show Images without slider', 'jozoorthemes' ),
				'desc' => __( 'default show in slider, check here if you want show without slider', 'jozoorthemes' ),
				'id'   => $prefix . 'project_images_without_slider',
				'type' => 'checkbox',
			),
            array(
				'name' => __( 'Using LightBox', 'jozoorthemes' ),
				'desc' => __( 'activate lightbox link on project images to show images in large', 'jozoorthemes' ),
				'id'   => $prefix . 'project_images_using_lightbox',
				'type' => 'checkbox',
			),
            
        ),
    );
    
#=============================================================#
# Video Options 
#=============================================================#
    $meta_boxes['jozoor-metabox-portfolio-video'] = array(
        'id' => 'jozoor-metabox-portfolio-video',
        'title' => __('Video Options', 'jozoorthemes'),
        'pages' => array('portfolio'), // post type
        'context' => 'normal',
        'priority' => 'high',
        'show_names' => true, // Show field names on the left
        'fields' => array(
            
            array(
				'name' => __( 'Video URL', 'jozoorthemes' ),
				'desc' => __( 'Enter your video url dicrect which supported : <br> [ youtube, vimeo, wordpress.tv, dailymotion, flickr videos, revision3, Hulu, funnyordie, slideshare, scribd ], <br> so just copy url of video or item and add it', 'jozoorthemes' ),
				'id'   => $prefix . 'project_video_embed_content',
				'type' => 'oembed',
			),
            
        ),
    );
    
#=============================================================#
# Audio Options 
#=============================================================#
    $meta_boxes['jozoor-metabox-portfolio-audio'] = array(
        'id' => 'jozoor-metabox-portfolio-audio',
        'title' => __('Audio Options', 'jozoorthemes'),
        'pages' => array('portfolio'), // post type
        'context' => 'normal',
        'priority' => 'high',
        'show_names' => true, // Show field names on the left
        'fields' => array(
            
            array(
                'name' => __('MP3 File URL', 'jozoorthemes'),
                'desc' => 'EX : http://www.website.com/audio/your_file.mp3',
                'id' => $prefix . 'project_audio_mp3_url',
                'type' => 'text'
            ),
            array(
				'name' => __( 'Audio Background', 'jozoorthemes' ),
				'desc' => '',
				'id'   => $prefix . 'project_audio_mp3_bg',
				'type' => 'file',
                'attributes' => array(
                      'placeholder' => __( 'add url direct or upload image', 'jozoorthemes' ),
                ),
			),
            array(
				'name' => __( 'Using SoundCloud', 'jozoorthemes' ),
				'desc' => __( 'embed soundcloud audio instead of mp3 file', 'jozoorthemes' ),
				'id'   => $prefix . 'project_using_soundcloud',
				'type' => 'checkbox',
			),
            array(
				'name' => __( 'SoundCloud URL', 'jozoorthemes' ),
				'desc' => __( 'add your soundcloud url', 'jozoorthemes' ),
				'id'   => $prefix . 'project_soundcloud_embed',
				'type' => 'oembed',
			),
            
        ),
    );
    
#=============================================================#
# General Options 
#=============================================================#
    $meta_boxes['jozoor-metabox-portfolio-general-options'] = array(
        'id' => 'jozoor-metabox-portfolio-general-options',
        'title' => __('General Options', 'jozoorthemes'),
        'pages' => array('portfolio'), // post type
        'context' => 'normal',
        'priority' => 'high',
        'show_names' => true, // Show field names on the left
        'fields' => array(
            
            array(
				'name' => __( 'Project Page Layout', 'jozoorthemes' ),
				'desc' => '',
				'id'   => $prefix . 'project_page_layout',
				'type'    => 'radio_inline',
                'default' => 'default',
				'options' => array(
					'default' => __( 'Default', 'jozoorthemes' ),
					'full' => __( 'Full Width', 'jozoorthemes' ),
                ),
            ),
            array(
                'name' => __('Project Skills', 'jozoorthemes'),
                'desc' => 'EX : HTML/CSS, Logo Design <br>'.__( 'if you want to hidden this section, let field empty', 'jozoorthemes' ),
                'id' => $prefix . 'project_skills',
                'type' => 'text'
            ),
            array(
                'name' => __('Project Online URL', 'jozoorthemes'),
                'desc' => __( 'if you want to hidden this section, let field empty', 'jozoorthemes' ),
                'id' => $prefix . 'project_online_url',
                'type' => 'text_url'
            ),
            
        ),
    );
    
    
return $meta_boxes;
}
add_filter( 'cmb_meta_boxes', 'jozoor_metaboxs_options_portfolio' );

?>