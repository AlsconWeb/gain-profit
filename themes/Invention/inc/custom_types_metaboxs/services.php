<?php
/* Custom Post Type ( Services ) */

// register post type
add_action('init', 'jozoor_post_type_services');

function jozoor_post_type_services() {

  $labels = array(
  'name' => _x('Services', 'post type general name', 'jozoorthemes'),
  'singular_name' => _x('Service', 'post type singular name', 'jozoorthemes'),
  'add_new' => _x('Add New', 'services', 'jozoorthemes'),
  'add_new_item' => __('Add New', 'jozoorthemes'),
  'edit_item' => __('Edit Service', 'jozoorthemes'),
  'new_item' => __('New Service', 'jozoorthemes'),
  'all_items' => __('All Services', 'jozoorthemes'),
  'view_item' => __('View Service', 'jozoorthemes'),
  'search_items' => __('Search Service', 'jozoorthemes'),
  'not_found' =>  __('No Services found', 'jozoorthemes'),
  'not_found_in_trash' => __('No Services found in Trash', 'jozoorthemes'), 
  'parent_item_colon' => '',
  'menu_name' => __('Services', 'jozoorthemes')

  );

  $services_args = array(
  'labels' => $labels,
  'public' => true,
  'publicly_queryable' => true,
  'show_ui' => true, 
  'show_in_menu' => true, 
  'query_var' => true,
  'capability_type' => 'post',
  'hierarchical' => false,
  'rewrite' => array('slug' => 'services-item', 'with_front' => false),
  'has_archive' => true, 
  'menu_position' => 101,
  'menu_icon' => get_template_directory_uri().'/images/admin-icons/services.png',
  'supports' => array( 'title', 'editor', 'thumbnail', 'comments' ),
  );
    
  register_post_type('services',$services_args);
    
}


// Customizing the messages
function jozoor_services_updated_messages( $messages ) {
    
  global $post, $post_ID;

  $messages['services'] = array(
  0 => '', 
  1 => sprintf( __('Service updated. <a href="%s">View Service</a>', 'jozoorthemes'), esc_url( get_permalink($post_ID) ) ),
  2 => __('Custom field updated.', 'jozoorthemes'),
  3 => __('Custom field deleted.', 'jozoorthemes'),
  4 => __('Service updated.', 'jozoorthemes'),
  5 => isset($_GET['revision']) ? sprintf( __('Service restored to revision from %s', 'jozoorthemes'), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
  6 => sprintf( __('Service published. <a href="%s">View Service</a>', 'jozoorthemes'), esc_url( get_permalink($post_ID) ) ),
  7 => __('Service saved.', 'jozoorthemes'),
  8 => sprintf( __('Service submitted. <a target="_blank" href="%s">Preview Service</a>', 'jozoorthemes'), esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
  9 => sprintf( __('Service scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview Service</a>', 'jozoorthemes'),
  date_i18n( __( 'M j, Y @ G:i', 'jozoorthemes' ), strtotime( $post->post_date ) ), esc_url( get_permalink($post_ID) ) ),
  10 => sprintf( __('Service draft updated. <a target="_blank" href="%s">Preview Service</a>', 'jozoorthemes'), esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
  );

  return $messages;
    
}
add_filter( 'post_updated_messages', 'jozoor_services_updated_messages' ); 

// service edit column
add_filter("manage_edit-services_columns", "jozoor_service_edit_columns");

function jozoor_service_edit_columns($columns) {
        $columns = array(
            "cb" => "<input type=\"checkbox\" />",
            "title" => __("Service", "jozoorthemes"),
            "featured" => __("Featured Image", "jozoorthemes"),
            "date" => __("Date", "jozoorthemes"),
        );
        return $columns;
}


// ======= service metaboxs ======= //

function jozoor_metaboxs_options_services( $meta_boxes ) {
    
      // icons list
      $jozoor_shortcode_icons_list = array(
          'glass', 'music', 'search', 'envelope', 'heart', 'star', 'star-empty', 'user', 'film', 'th-large', 'th', 'th-list', 'ok', 'zoom-in', 'zoom-out', 'off', 'signal', 'cog', 'trash', 'home',
          'file', 'time', 'road', 'download-alt', 'download', 'upload', 'inbox', 'play-circle', 'repeat', 'refresh', 'list-alt', 'lock', 'flag', 'headphones', 'volume-off', 'volume-down', 'volume-up',           'qrcode', 'barcode', 'tag', 'tags', 'book', 'bookmark', 'print', 'camera', 'font', 'bold', 'italic', 'text-height', 'text-width', 'align-left', 'align-center', 'align-right', 'align-justify',           'list', 'indent-left', 'indent-right', 'facetime-video', 'pencil', 'map-marker', 'adjust', 'tint', 'edit', 'share', 'check', 'move', 'step-backward', 'fast-backward', 'backward', 'play',               'pause', 'stop', 'forward', 'fast-forward', 'step-forward', 'eject', 'chevron-left', 'chevron-right', 'plus-sign', 'minus-sign', 'remove-sign', 'ok-sign', 'question-sign',
          'info-sign', 'screenshot', 'remove-circle', 'ok-circle', 'ban-circle', 'arrow-left', 'arrow-right', 'arrow-up', 'arrow-down', 'share-alt', 'resize-full', 'resize-small', 'plus', 'minus',               'asterisk', 'exclamation-sign', 'gift', 'leaf', 'fire', 'eye-open', 'eye-close',
          'warning-sign', 'plane', 'calendar', 'random', 'comment', 'magnet', 'chevron-up', 'chevron-down', 'retweet', 'shopping-cart', 'folder-close', 'folder-open', 'resize-vertical', 
          'resize-horizontal', 'bar-chart', 'twitter-sign', 'facebook-sign', 'camera-retro', 'key', 'cogs', 'comments',
          'thumbs-up', 'thumbs-down', 'star-half', 'heart-empty', 'signout', 'linkedin-sign', 'pushpin', 'external-link', 'signin', 'trophy', 'github-sign', 'upload-alt', 'lemon', 'phone', 
          'check-empty', 'bookmark-empty', 'phone-sign', 'twitter', 'facebook', 'github', 'unlock',
          'credit', 'rss', 'hdd', 'bullhorn', 'bell', 'certificate', 'hand-right', 'hand-left', 'hand-up', 'hand-down', 'circle-arrow-left', 'circle-arrow-right', 'circle-arrow-up', 
          'circle-arrow-down', 'globe', 'wrench', 'tasks', 'filter', 'briefcase', 'fullscreen', 'group',
          'link', 'cloud', 'beaker', 'cut', 'copy', 'paper-clip', 'save', 'sign-blank', 'reorder', 'list-ul', 'list-ol', 'strikethrough', 'underline', 'table', 'magic', 'truck', 'pinterest',                     'pinterest-sign', 'google-plus-sign', 'google-plus', 'money',
          'caret-down', 'caret-up', 'caret-left', 'caret-right', 'columns', 'sort', 'sort-down', 'sort-up', 'envelope-alt', 'linkedin', 'undo', 'legal', 'dashboard', 'comment-alt', 'comments-alt',               'bolt', 'sitemap', 'umbrella', 'paste', 'lightbulb', 'exchange',
          'cloud-download', 'cloud-upload', 'user-md', 'stethoscope', 'suitcase', 'bell-alt', 'coffee', 'food', 'file-alt', 'building', 'hospital', 'ambulance', 'medkit', 'fighter-jet', 'beer', 
          'h-sign', 'plus-sign2', 'double-angle-left', 'double-angle-right', 'double-angle-up', 'double-angle-down',
          'angle-left', 'angle-right', 'angle-up', 'angle-down', 'desktop', 'laptop', 'tablet', 'mobile', 'circle-blank', 'quote-left', 'quote-right', 'spinner', 'circle', 'reply', 'github-alt',                 'folder-close-alt', 'folder-open-alt', 'expand-alt', 'collapse-alt', 'smile', 'frown',
          'meh', 'gamepad', 'keyboard', 'flag-alt', 'flag-checkered', 'terminal', 'code', 'reply-all', 'star-half-full', 'location-arrow', 'crop', 'code-fork', 'unlink', 'question', 'info',                       'exclamation', 'superscript', 'subscript', 'eraser', 'puzzle', 'microphone',
          'microphone-off', 'shield', 'calendar-empty', 'fire-extinguisher', 'rocket', 'maxcdn', 'chevron-sign-left', 'chevron-sign-right', 'chevron-sign-up', 'chevron-sign-down', 'html5', 'css3',               'anchor', 'unlock-alt', 'bullseye', 'ellipsis-horizontal', 'ellipsis-vertical', 'rss-sign', 'play-sign', 'ticket', 'minus-sign-alt',
          'check-minus', 'level-up', 'file-text', 'sort-by-alphabet', 'sort-by-alphabet-alt', 'tumblr', 'tumblr-sign', 'long-arrow-down', 'vk', 'weibo', 'renren', 'level-down', 'check-sign', 
          'edit-sign', 'external-link-sign', 'share-sign', 'compass', 'collapse', 'collapse-top', 'expand', 'euro', 'gbp', 'dollar', 'rupee', 'yen', 'renminbi', 'won', 'bitcoin', 'file2', 
          'sort-by-attributes', 'sort-by-attributes-alt', 'sort-by-order', 'sort-by-order-alt', 'thumbs-up2', 'thumbs-down2', 'youtube-sign', 'youtube', 'xing', 'xing-sign', 'youtube-play', 'dropbox', 
          'stackexchange', 'instagram', 'flickr', 'adn', 'bitbucket', 'bitbucket-sign', 'long-arrow-up', 'long-arrow-left', 'long-arrow-right', 'apple', 'windows', 'android', 'linux', 'dribbble',                 'skype', 'foursquare', 'trello', 'female', 'male', 'gittip', 'sun', 'moon', 'archive', 'bug', 'picture', 'remove'
      );
    
      $list_of_icons = '';
    
      foreach ($jozoor_shortcode_icons_list as $icon_name) {
        $list_of_icons .= '<li><i class="icon-'.$icon_name.'"></i></li>';  
      }

    
$prefix = '_jozoor_'; // Prefix for all fields
    
    
    $meta_boxes['jozoor-metabox-service-icon'] = array(
        'id' => 'jozoor-metabox-service-icon',
        'title' => __('Service Icon Type', 'jozoorthemes'),
        'pages' => array('services'), // post type
        'context' => 'normal',
        'priority' => 'high',
        'show_names' => true, // Show field names on the left
        'fields' => array(
            
            array(
				'name' => __( 'Choose Type', 'jozoorthemes' ),
				'desc' => '',
				'id'   => $prefix . 'service_icon_type',
				'type'    => 'radio_inline',
                'default' => 'iconfont',
				'options' => array(
					'iconfont' => __( 'Icon Font', 'jozoorthemes' ),
					'iconimage' => __( 'Icon Image', 'jozoorthemes' ),
                ),
            ),
            
        ),
            
    );    
    
#=============================================================#
# Icon Font Options 
#=============================================================#
    $meta_boxes['jozoor-metabox-service-icon-font'] = array(
        'id' => 'jozoor-metabox-service-icon-font',
        'title' => __('Icon Font', 'jozoorthemes'),
        'pages' => array('services'), // post type
        'context' => 'normal',
        'priority' => 'high',
        'show_names' => true, // Show field names on the left
        'fields' => array(
            
            array(
                'name' => __('Choose Icon', 'jozoorthemes'),
                'desc' => __( '<div id="j_services_icons_preview" class="j_icons_preview"><ul>'.$list_of_icons.'</ul></div>', 'jozoorthemes' ),
                'id' => $prefix . 'service_icon_font_name',
                'type' => 'text'
            ),
            array(
				'name' => __( 'Spin Icon', 'jozoorthemes' ),
				'desc' => __( 'yes make icon spin', 'jozoorthemes' ),
				'id'   => $prefix . 'service_icon_font_spin',
				'type' => 'checkbox',
			),
            array(
				'name' => __( 'Flip Icon', 'jozoorthemes' ),
				'desc' => '',
				'id'   => $prefix . 'service_icon_font_flip',
				'type'    => 'radio_inline',
                'default' => 'no',
				'options' => array(
					'no' => __( 'No', 'jozoorthemes' ),
					'horizontal' => __( 'Horizontal', 'jozoorthemes' ),
                    'vertical' => __( 'Vertical', 'jozoorthemes' ),
                ),
            ),
            array(
				'name' => __( 'Rotate Icon', 'jozoorthemes' ),
				'desc' => '',
				'id'   => $prefix . 'service_icon_font_rotate',
				'type'    => 'radio_inline',
                'default' => 'no',
				'options' => array(
					'no' => __( 'No', 'jozoorthemes' ),
					'ro90' => __( '90', 'jozoorthemes' ),
                    'ro180' => __( '180', 'jozoorthemes' ),
                    'ro270' => __( '270', 'jozoorthemes' ),
                ),
            ),
            
        ),
    );
    
#=============================================================#
# Icon Image Options 
#=============================================================#
    $meta_boxes['jozoor-metabox-service-icon-image'] = array(
        'id' => 'jozoor-metabox-service-icon-image',
        'title' => __('Icon Image', 'jozoorthemes'),
        'pages' => array('services'), // post type
        'context' => 'normal',
        'priority' => 'high',
        'show_names' => true, // Show field names on the left
        'fields' => array(
            
            array(
				'name' => __( 'Icon Image', 'jozoorthemes' ),
				'desc' => '',
				'id'   => $prefix . 'service_icon_image_name',
				'type' => 'file',
                'attributes' => array(
                      'placeholder' => __( 'add url direct or upload image', 'jozoorthemes' ),
                ),
			),
            
        ),
    );
    
    
return $meta_boxes;
}
add_filter( 'cmb_meta_boxes', 'jozoor_metaboxs_options_services' );

?>