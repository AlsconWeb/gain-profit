<?php
/* Custom Post Type ( Gallery ) */

// register post type
add_action('init', 'jozoor_post_type_gallery');

function jozoor_post_type_gallery() {

  $labels = array(
  'name' => _x('Gallery', 'post type general name', 'jozoorthemes'),
  'singular_name' => _x('Gallery', 'post type singular name', 'jozoorthemes'),
  'add_new' => _x('Add New', 'gallery', 'jozoorthemes'),
  'add_new_item' => __('Add New', 'jozoorthemes'),
  'edit_item' => __('Edit Gallery', 'jozoorthemes'),
  'new_item' => __('New Gallery', 'jozoorthemes'),
  'all_items' => __('All Galleries', 'jozoorthemes'),
  'view_item' => __('View Gallery', 'jozoorthemes'),
  'search_items' => __('Search Gallery', 'jozoorthemes'),
  'not_found' =>  __('No Galleries found', 'jozoorthemes'),
  'not_found_in_trash' => __('No Galleries found in Trash', 'jozoorthemes'), 
  'parent_item_colon' => '',
  'menu_name' => __('Galleries', 'jozoorthemes')

  );

  $gallery_args = array(
  'labels' => $labels,
  'public' => true,
  'publicly_queryable' => true,
  'show_ui' => true, 
  'show_in_menu' => true, 
  'query_var' => true,
  'capability_type' => 'post',
  'hierarchical' => false,
  'rewrite' => array('slug' => 'galleries', 'with_front' => false),
  'has_archive' => false, 
  'menu_position' => 101,
  'menu_icon' => get_template_directory_uri().'/images/admin-icons/gallery.png',
  'supports' => array( 'title', 'editor', 'thumbnail', 'comments' ),
  );
    
  register_post_type('gallery',$gallery_args);
    
}


// Customizing the messages
function jozoor_gallery_updated_messages( $messages ) {
    
  global $post, $post_ID;

  $messages['gallery'] = array(
  0 => '', 
  1 => sprintf( __('Gallery updated. <a href="%s">View Gallery</a>', 'jozoorthemes'), esc_url( get_permalink($post_ID) ) ),
  2 => __('Custom field updated.', 'jozoorthemes'),
  3 => __('Custom field deleted.', 'jozoorthemes'),
  4 => __('Gallery updated.', 'jozoorthemes'),
  5 => isset($_GET['revision']) ? sprintf( __('Gallery restored to revision from %s', 'jozoorthemes'), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
  6 => sprintf( __('Gallery published. <a href="%s">View Gallery</a>', 'jozoorthemes'), esc_url( get_permalink($post_ID) ) ),
  7 => __('Gallery saved.', 'jozoorthemes'),
  8 => sprintf( __('Gallery submitted. <a target="_blank" href="%s">Preview Gallery</a>', 'jozoorthemes'), esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
  9 => sprintf( __('Gallery scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview Gallery</a>', 'jozoorthemes'),
  date_i18n( __( 'M j, Y @ G:i', 'jozoorthemes' ), strtotime( $post->post_date ) ), esc_url( get_permalink($post_ID) ) ),
  10 => sprintf( __('Gallery draft updated. <a target="_blank" href="%s">Preview Gallery</a>', 'jozoorthemes'), esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
  );

  return $messages;
    
}
add_filter( 'post_updated_messages', 'jozoor_gallery_updated_messages' ); 

// client edit column
add_filter("manage_edit-gallery_columns", "jozoor_gallery_edit_columns");

function jozoor_gallery_edit_columns($columns) {
        $columns = array(
            "cb" => "<input type=\"checkbox\" />",
            "title" => __("Gallery", "jozoorthemes"),
            "featured" => __("Featured Image", "jozoorthemes"),
            "date" => __("Date", "jozoorthemes"),
        );
        return $columns;
}

// ======= gallery metaboxs ======= //

function jozoor_metaboxs_options_gallery( $meta_boxes ) {
    
$prefix = '_jozoor_'; // Prefix for all fields
    
    $meta_boxes['jozoor-metabox-gallery-options'] = array(
        'id' => 'jozoor-metabox-gallery-options',
        'title' => __('Gallery Options', 'jozoorthemes'),
        'pages' => array('gallery'), // post type
        'context' => 'normal',
        'priority' => 'high',
        'show_names' => true, // Show field names on the left
        'fields' => array(
            
            array(
				'name' => __( 'Gallery Layout', 'jozoorthemes' ),
				'desc' => '',
				'id'   => $prefix . 'gallery_layout',
				'type'    => 'radio_inline',
                'default' => 'col3',
				'options' => array(
					'col4' => __( '4 Columns', 'jozoorthemes' ),
					'col3' => __( '3 Columns', 'jozoorthemes' ),
					'col2' => __( '2 Columns', 'jozoorthemes' ),
                    'col1' => __( '1 Column', 'jozoorthemes' ),
                ),
            ),
            array(
				'name' => __( 'Show in Masonry', 'jozoorthemes' ),
				'desc' => __( 'yes active masonry style for images', 'jozoorthemes' ),
				'id'   => $prefix . 'gallery_layout_masonry',
				'type' => 'checkbox',
			),
            
        ),
    );
    
    $meta_boxes['jozoor-metabox-gallery'] = array(
        'id' => 'jozoor-metabox-gallery',
        'title' => __('Gallery Images', 'jozoorthemes'),
        'pages' => array('gallery'), // post type
        'context' => 'normal',
        'priority' => 'high',
        'show_names' => true, // Show field names on the left
        'fields' => array(
            
            array(
				'name' => __( 'Upload Your Images', 'jozoorthemes' ),
				'desc' => __( 'upload any numbers of images', 'jozoorthemes' ),
				'id'   => $prefix . 'gallery_images',
				'type' => 'file_list',
                'preview_size' => array( 150, 150 ),
			),
            
        ),
    );
    
    
return $meta_boxes;
}
add_filter( 'cmb_meta_boxes', 'jozoor_metaboxs_options_gallery' );

?>