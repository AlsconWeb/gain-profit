<?php

function jozoor_metaboxs_options_post( $meta_boxes ) {
    
$prefix = '_jozoor_'; // Prefix for all fields
    
#=============================================================#
# Quote Options 
#=============================================================#
    $meta_boxes['jozoor-metabox-post-quote'] = array(
        'id' => 'jozoor-metabox-post-quote',
        'title' => __('Quote Options', 'jozoorthemes'),
        'pages' => array('post'), // post type
        'context' => 'normal',
        'priority' => 'high',
        'show_names' => true, // Show field names on the left
        'fields' => array(
            
            array(
				'name' => __( 'Quote Content', 'jozoorthemes' ),
				'desc' => '',
				'id'   => $prefix . 'quote_content',
				'type' => 'textarea',
			),
            array(
                'name' => __('Quote Author', 'jozoorthemes'),
                'desc' => '',
                'id' => $prefix . 'quote_author',
                'type' => 'text'
            ),
            
        ),
    );
    
#=============================================================#
# Link Options 
#=============================================================#
    $meta_boxes['jozoor-metabox-post-link'] = array(
        'id' => 'jozoor-metabox-post-link',
        'title' => __('Link Options', 'jozoorthemes'),
        'pages' => array('post'), // post type
        'context' => 'normal',
        'priority' => 'high',
        'show_names' => true, // Show field names on the left
        'fields' => array(
            
            array(
                'name' => __('Link URL', 'jozoorthemes'),
                'desc' => '',
                'id' => $prefix . 'link_url',
                'type' => 'text_url'
            ),
            
        ),
    );
    
#=============================================================#
# Status Options 
#=============================================================#
    $meta_boxes['jozoor-metabox-post-status'] = array(
        'id' => 'jozoor-metabox-post-status',
        'title' => __('Status Options', 'jozoorthemes'),
        'pages' => array('post'), // post type
        'context' => 'normal',
        'priority' => 'high',
        'show_names' => true, // Show field names on the left
        'fields' => array(
            
            array(
				'name' => __( 'Status Content', 'jozoorthemes' ),
				'desc' => __( 'you can embed twitter or facebook status or other social embed', 'jozoorthemes' ),
				'id'   => $prefix . 'status_content',
				'type' => 'textarea_code',
			),
            
        ),
    );
    
#=============================================================#
# Image Options 
#=============================================================#
    $meta_boxes['jozoor-metabox-post-image'] = array(
        'id' => 'jozoor-metabox-post-image',
        'title' => __('Image Options', 'jozoorthemes'),
        'pages' => array('post'), // post type
        'context' => 'normal',
        'priority' => 'high',
        'show_names' => true, // Show field names on the left
        'fields' => array(
            
            array(
				'name' => __( 'Using LightBox', 'jozoorthemes' ),
				'desc' => __( 'activate lightbox link on featured image or uncheck to let default post link', 'jozoorthemes' ),
				'id'   => $prefix . 'using_lightbox',
				'type' => 'checkbox',
			),
            
        ),
    );
    
#=============================================================#
# Gallery Options 
#=============================================================#
    $meta_boxes['jozoor-metabox-post-gallery'] = array(
        'id' => 'jozoor-metabox-post-gallery',
        'title' => __('Gallery Options', 'jozoorthemes'),
        'pages' => array('post'), // post type
        'context' => 'normal',
        'priority' => 'high',
        'show_names' => true, // Show field names on the left
        'fields' => array(
            
            array(
				'name' => __( 'Gallery Images', 'jozoorthemes' ),
				'desc' => __( 'upload any numbers of images', 'jozoorthemes' ),
				'id'   => $prefix . 'gallery_images',
				'type' => 'file_list',
                'preview_size' => array( 150, 150 ),
			),
            array(
				'name' => __( 'Show Images in 3 columns', 'jozoorthemes' ),
				'desc' => __( 'default 2 columns, check here if you want 3 columns', 'jozoorthemes' ),
				'id'   => $prefix . 'gallery_images_col3',
				'type' => 'checkbox',
			),
            array(
				'name' => __( 'Show Images in Slider', 'jozoorthemes' ),
				'desc' => __( 'show images in slider instead of normal gallery', 'jozoorthemes' ),
				'id'   => $prefix . 'gallery_images_slider',
				'type' => 'checkbox',
			),
            
        ),
    );
    
#=============================================================#
# Audio Options 
#=============================================================#
    $meta_boxes['jozoor-metabox-post-audio'] = array(
        'id' => 'jozoor-metabox-post-audio',
        'title' => __('Audio Options', 'jozoorthemes'),
        'pages' => array('post'), // post type
        'context' => 'normal',
        'priority' => 'high',
        'show_names' => true, // Show field names on the left
        'fields' => array(
            
            array(
                'name' => __('MP3 File URL', 'jozoorthemes'),
                'desc' => 'EX : http://www.website.com/audio/your_file.mp3',
                'id' => $prefix . 'audio_mp3_url',
                'type' => 'text'
            ),
            array(
				'name' => __( 'Using SoundCloud', 'jozoorthemes' ),
				'desc' => __( 'embed soundcloud audio instead of mp3 file', 'jozoorthemes' ),
				'id'   => $prefix . 'using_soundcloud',
				'type' => 'checkbox',
			),
            array(
				'name' => __( 'SoundCloud URL', 'jozoorthemes' ),
				'desc' => __( 'add your soundcloud url', 'jozoorthemes' ),
				'id'   => $prefix . 'soundcloud_embed',
				'type' => 'oembed',
			),
            
        ),
    );
    
#=============================================================#
# Video Options 
#=============================================================#
    $meta_boxes['jozoor-metabox-post-video'] = array(
        'id' => 'jozoor-metabox-post-video',
        'title' => __('Video Options', 'jozoorthemes'),
        'pages' => array('post'), // post type
        'context' => 'normal',
        'priority' => 'high',
        'show_names' => true, // Show field names on the left
        'fields' => array(
            
            array(
				'name' => __( 'Video URL', 'jozoorthemes' ),
				'desc' => __( 'Enter your video url dicrect which supported : <br> [ youtube, vimeo, wordpress.tv, dailymotion, flickr videos, revision3, Hulu, funnyordie, slideshare, scribd ], <br> so just copy url of video or item and add it', 'jozoorthemes' ),
				'id'   => $prefix . 'video_embed_content',
				'type' => 'oembed',
			),
            
        ),
    );
    
    

    return $meta_boxes;
}
add_filter( 'cmb_meta_boxes', 'jozoor_metaboxs_options_post' );

?>