<?php
/* Custom Post Type ( Testimonials ) */

// register post type
add_action('init', 'jozoor_post_type_testimonials');

function jozoor_post_type_testimonials() {

  $labels = array(
  'name' => _x('Testimonials', 'post type general name', 'jozoorthemes'),
  'singular_name' => _x('Testimonial', 'post type singular name', 'jozoorthemes'),
  'add_new' => _x('Add New', 'testimonials', 'jozoorthemes'),
  'add_new_item' => __('Add New', 'jozoorthemes'),
  'edit_item' => __('Edit Testimonial', 'jozoorthemes'),
  'new_item' => __('New Testimonial', 'jozoorthemes'),
  'all_items' => __('All Testimonials', 'jozoorthemes'),
  'view_item' => __('View Testimonial', 'jozoorthemes'),
  'search_items' => __('Search Testimonial', 'jozoorthemes'),
  'not_found' =>  __('No Testimonials found', 'jozoorthemes'),
  'not_found_in_trash' => __('No Testimonials found in Trash', 'jozoorthemes'), 
  'parent_item_colon' => '',
  'menu_name' => __('Testimonials', 'jozoorthemes')

  );

  $testimonials_args = array(
  'labels' => $labels,
  'public' => false,
  'publicly_queryable' => true,
  'show_ui' => true, 
  'show_in_menu' => true, 
  'query_var' => true,
  'capability_type' => 'post',
  'exclude_from_search ' => true,
  'hierarchical' => false,
  'rewrite' => array('slug' => 'testimonial-item', 'with_front' => false),
  'has_archive' => false, 
  'menu_position' => 101,
  'menu_icon' => get_template_directory_uri().'/images/admin-icons/testimonials.png',
  'supports' => array( 'title', 'editor' ),
  );
    
  register_post_type('testimonials',$testimonials_args);
    
}


// Customizing the messages
function jozoor_testimonials_updated_messages( $messages ) {
    
  global $post, $post_ID;

  $messages['testimonials'] = array(
  0 => '', 
  1 => sprintf( __('Testimonial updated.', 'jozoorthemes'), esc_url( get_permalink($post_ID) ) ),
  2 => __('Custom field updated.', 'jozoorthemes'),
  3 => __('Custom field deleted.', 'jozoorthemes'),
  4 => __('Testimonial updated.', 'jozoorthemes'),
  5 => isset($_GET['revision']) ? sprintf( __('Testimonial restored to revision from %s', 'jozoorthemes'), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
  6 => sprintf( __('Testimonial published.', 'jozoorthemes'), esc_url( get_permalink($post_ID) ) ),
  7 => __('Testimonial saved.', 'jozoorthemes'),
  8 => sprintf( __('Testimonial submitted.', 'jozoorthemes'), esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
  9 => sprintf( __('Testimonial scheduled for: <strong>%1$s</strong>.', 'jozoorthemes'),
  date_i18n( __( 'M j, Y @ G:i', 'jozoorthemes' ), strtotime( $post->post_date ) ), esc_url( get_permalink($post_ID) ) ),
  10 => sprintf( __('Testimonial draft updated.', 'jozoorthemes'), esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
  );

  return $messages;
    
}
add_filter( 'post_updated_messages', 'jozoor_testimonials_updated_messages' ); 

// testimonials edit column
add_filter("manage_edit-testimonials_columns", "jozoor_testimonial_edit_columns");

function jozoor_testimonial_edit_columns($columns) {
        $columns = array(
            "cb" => "<input type=\"checkbox\" />",
            "title" => __("Testimonial", "jozoorthemes"),
            "date" => __("Date", "jozoorthemes"),
        );
        return $columns;
}

// ======= testimonials metaboxs ======= //

function jozoor_metaboxs_options_testimonials( $meta_boxes ) {
    
$prefix = '_jozoor_'; // Prefix for all fields
    
    $meta_boxes['jozoor-metabox-testimonials'] = array(
        'id' => 'jozoor-metabox-testimonials',
        'title' => __('Testimonial Options', 'jozoorthemes'),
        'pages' => array('testimonials'), // post type
        'context' => 'normal',
        'priority' => 'high',
        'show_names' => true, // Show field names on the left
        'fields' => array(
            
            array(
                'name' => __('Company or Client Online URL', 'jozoorthemes'),
                'desc' => __( 'if you want client without link, let field empty', 'jozoorthemes' ),
                'id' => $prefix . 'testimonial_client_online_url',
                'type' => 'text_url'
            ),
            array(
                'name' => __('Company Name or Client job', 'jozoorthemes'),
                'desc' => __( 'add company or client job like [ jozoor, e-marketing, developer...and what you want ], if you want to hidden this section, let field empty', 'jozoorthemes' ),
                'id' => $prefix . 'testimonial_client_job',
                'type' => 'text'
            ),
            
        ),
    );
    
    
return $meta_boxes;
}
add_filter( 'cmb_meta_boxes', 'jozoor_metaboxs_options_testimonials' );

?>