<?php

function jozoor_metaboxs_options_page( $meta_boxes ) {
    
$prefix = '_jozoor_'; // Prefix for all fields
    

#=============================================================#
# Blog Options 
#=============================================================#
    $meta_boxes['jozoor-metabox-page-blog'] = array(
        'id' => 'jozoor-metabox-page-blog',
        'title' => __('Blog Options', 'jozoorthemes'),
        'pages' => array('page'), // post type
        'context' => 'normal',
        'priority' => 'high',
        'show_names' => true, // Show field names on the left
        'fields' => array(
            
            array(
				'name' => __( 'Blog Style', 'jozoorthemes' ),
				'desc' => '',
				'id'   => $prefix . 'blog_style',
				'type'    => 'radio_inline',
                'default' => 'blog1',
				'options' => array(
					'blog1' => __( 'Blog 1', 'jozoorthemes' ),
					'blog2' => __( 'Blog 2', 'jozoorthemes' ),
					'blog3' => __( 'Blog 3', 'jozoorthemes' ),
                ),
            ),
            array(
				'name' => __( 'Using Categories Filter', 'jozoorthemes' ),
				'desc' => __( 'show categories filter', 'jozoorthemes' ),
				'id'   => $prefix . 'using_cats_filter',
				'type' => 'checkbox',
			),
            
        ),
            
    );
    
#=============================================================#
# Portfolio Options 
#=============================================================#
    $meta_boxes['jozoor-metabox-page-portfolio'] = array(
        'id' => 'jozoor-metabox-page-portfolio',
        'title' => __('Portfolio Options', 'jozoorthemes'),
        'pages' => array('page'), // post type
        'context' => 'normal',
        'priority' => 'high',
        'show_names' => true, // Show field names on the left
        'fields' => array(
            
            array(
				'name' => __( 'Portfolio Layout', 'jozoorthemes' ),
				'desc' => '',
				'id'   => $prefix . 'portfolio_layout',
				'type'    => 'radio_inline',
                'default' => 'col4',
				'options' => array(
					'col4' => __( '4 Columns', 'jozoorthemes' ),
					'col3' => __( '3 Columns', 'jozoorthemes' ),
					'col2' => __( '2 Columns', 'jozoorthemes' ),
                ),
            ),
            array(
				'name' => __( 'Show Categories Filter', 'jozoorthemes' ),
				'desc' => '',
				'id'   => $prefix . 'portfolio_show_cats_filter',
				'type'    => 'select',
                'default' => 'yes',
				'options' => array(
					'yes' => __( 'Yes', 'jozoorthemes' ),
					'no' => __( 'No', 'jozoorthemes' ),
                ),
			),
            
        ),
            
    );
    

#=============================================================#
# Galleries Options 
#=============================================================#
    $meta_boxes['jozoor-metabox-page-galleries'] = array(
        'id' => 'jozoor-metabox-page-galleries',
        'title' => __('Galleries Options', 'jozoorthemes'),
        'pages' => array('page'), // post type
        'context' => 'normal',
        'priority' => 'high',
        'show_names' => true, // Show field names on the left
        'fields' => array(
            
            array(
				'name' => __( 'Galleries Layout', 'jozoorthemes' ),
				'desc' => '',
				'id'   => $prefix . 'galleries_layout',
				'type'    => 'radio_inline',
                'default' => 'col3',
				'options' => array(
					'col4' => __( '4 Columns', 'jozoorthemes' ),
					'col3' => __( '3 Columns', 'jozoorthemes' ),
					'col2' => __( '2 Columns', 'jozoorthemes' ),
                ),
            ),
            
        ),
            
    );
    
    
#=============================================================#
# Home Options 
#=============================================================#
    $meta_boxes['jozoor-metabox-page-home'] = array(
        'id' => 'jozoor-metabox-page-home',
        'title' => __('Home Options', 'jozoorthemes'),
        'pages' => array('page'), // post type
        'context' => 'normal',
        'priority' => 'high',
        'show_names' => true, // Show field names on the left
        'fields' => array(
            
            array(
				'name' => __( 'Home Layout', 'jozoorthemes' ),
				'desc' => '',
				'id'   => $prefix . 'home_layout',
				'type'    => 'radio_inline',
                'default' => 'home1',
				'options' => array(
					'home1' => __( 'Home 1', 'jozoorthemes' ),
					'home2' => __( 'Home 2', 'jozoorthemes' ),
					'home3' => __( 'Home 3', 'jozoorthemes' ),
                    'home4' => __( 'Home 4', 'jozoorthemes' ),
                ),
            ),
            
            
        ),
            
    );
    
    
    #=============================================================#
    # Home 1 Sections 
    #=============================================================#
    $meta_boxes['jozoor-metabox-page-home1-sections'] = array(
        'id' => 'jozoor-metabox-page-home1-sections',
        'title' => __('Home 1 Sections', 'jozoorthemes'),
        'pages' => array('page'), // post type
        'context' => 'normal',
        'priority' => 'high',
        'show_names' => true, // Show field names on the left
        'fields' => array(
            
              array(
				'name' => __( 'Show - Services', 'jozoorthemes' ),
				'desc' => '',
				'id'   => $prefix . 'home_1_show_services_section',
				'type'    => 'radio_inline',
                'default' => 'yes',
				'options' => array(
					'yes' => __( 'Yes', 'jozoorthemes' ),
					'no' => __( 'No', 'jozoorthemes' ),
                ),
             ),
                   array(
				    'name'    => '<a>&#8627; '.__( 'Add Another Content', 'jozoorthemes' ).'</a>',
				    'desc'    => __( 'you can add normal or shortcodes content, if you do not need adding content, let textarea empty', 'jozoorthemes' ),
				    'id'      => $prefix . 'home_1_services_section_instead_content',
				    'type'    => 'wysiwyg',
				   'options' => array( 'textarea_rows' => 5, 'wpautop' => true ),
			       ),
            
            array(
				'name' => __( 'Show - Recent Work', 'jozoorthemes' ),
				'desc' => '',
				'id'   => $prefix . 'home_1_show_recentwork_section',
				'type'    => 'radio_inline',
                'default' => 'yes',
				'options' => array(
					'yes' => __( 'Yes', 'jozoorthemes' ),
					'no' => __( 'No', 'jozoorthemes' ),
                ),
                     
            ),
                    array(
				    'name'    => '<a>&#8627; '.__( 'Add Another Content', 'jozoorthemes' ).'</a>',
				    'desc'    => __( 'you can add normal or shortcodes content, if you do not need adding content, let textarea empty', 'jozoorthemes' ),
				    'id'      => $prefix . 'home_1_recentwork_section_instead_content',
				    'type'    => 'wysiwyg',
				   'options' => array( 'textarea_rows' => 5, 'wpautop' => true ),
			       ),
            
            array(
				'name' => __( 'Show - Latest News', 'jozoorthemes' ),
				'desc' => '',
				'id'   => $prefix . 'home_1_show_latestnews_section',
				'type'    => 'radio_inline',
                'default' => 'yes',
				'options' => array(
					'yes' => __( 'Yes', 'jozoorthemes' ),
					'no' => __( 'No', 'jozoorthemes' ),
                ),
            ),
                     array(
				    'name'    => '<a>&#8627; '.__( 'Add Another Content', 'jozoorthemes' ).'</a>',
				    'desc'    => __( 'you can add normal or shortcodes content, if you do not need adding content, let textarea empty', 'jozoorthemes' ),
				    'id'      => $prefix . 'home_1_show_latestnews_section_instead_content',
				    'type'    => 'wysiwyg',
				   'options' => array( 'textarea_rows' => 5, 'wpautop' => true ),
			       ),      
            
            array(
				'name' => __( 'Show - Featured Clients', 'jozoorthemes' ),
				'desc' => '',
				'id'   => $prefix . 'home_1_show_clients_section',
				'type'    => 'radio_inline',
                'default' => 'yes',
				'options' => array(
					'yes' => __( 'Yes', 'jozoorthemes' ),
					'no' => __( 'No', 'jozoorthemes' ),
                ),
            ),
                     array(
				    'name'    => '<a>&#8627; '.__( 'Add Another Content', 'jozoorthemes' ).'</a>',
				    'desc'    => __( 'you can add normal or shortcodes content, if you do not need adding content, let textarea empty', 'jozoorthemes' ),
				    'id'      => $prefix . 'home_1_clients_section_instead_content',
				    'type'    => 'wysiwyg',
				   'options' => array( 'textarea_rows' => 5, 'wpautop' => true ),
			       ),
            
        ),
            
    );
    
    
    #=============================================================#
    # Home 2 Sections 
    #=============================================================#
    $meta_boxes['jozoor-metabox-page-home2-sections'] = array(
        'id' => 'jozoor-metabox-page-home2-sections',
        'title' => __('Home 2 Sections', 'jozoorthemes'),
        'pages' => array('page'), // post type
        'context' => 'normal',
        'priority' => 'high',
        'show_names' => true, // Show field names on the left
        'fields' => array(
            
              array(
				'name' => __( 'Show - Services', 'jozoorthemes' ),
				'desc' => '',
				'id'   => $prefix . 'home_2_show_services_section',
				'type'    => 'radio_inline',
                'default' => 'yes',
				'options' => array(
					'yes' => __( 'Yes', 'jozoorthemes' ),
					'no' => __( 'No', 'jozoorthemes' ),
                ),
             ),
                   array(
				    'name'    => '<a>&#8627; '.__( 'Add Another Content', 'jozoorthemes' ).'</a>',
				    'desc'    => __( 'you can add normal or shortcodes content, if you do not need adding content, let textarea empty', 'jozoorthemes' ),
				    'id'      => $prefix . 'home_2_services_section_instead_content',
				    'type'    => 'wysiwyg',
				   'options' => array( 'textarea_rows' => 5, 'wpautop' => true ),
			       ),
            
            array(
				'name' => __( 'Show - Recent Work', 'jozoorthemes' ),
				'desc' => '',
				'id'   => $prefix . 'home_2_show_recentwork_section',
				'type'    => 'radio_inline',
                'default' => 'yes',
				'options' => array(
					'yes' => __( 'Yes', 'jozoorthemes' ),
					'no' => __( 'No', 'jozoorthemes' ),
                ),
                     
            ),
                    array(
				    'name'    => '<a>&#8627; '.__( 'Add Another Content', 'jozoorthemes' ).'</a>',
				    'desc'    => __( 'you can add normal or shortcodes content, if you do not need adding content, let textarea empty', 'jozoorthemes' ),
				    'id'      => $prefix . 'home_2_recentwork_section_instead_content',
				    'type'    => 'wysiwyg',
				   'options' => array( 'textarea_rows' => 5, 'wpautop' => true ),
			       ),
            
            array(
				'name' => __( 'Show - Latest News', 'jozoorthemes' ),
				'desc' => '',
				'id'   => $prefix . 'home_2_show_latestnews_section',
				'type'    => 'radio_inline',
                'default' => 'yes',
				'options' => array(
					'yes' => __( 'Yes', 'jozoorthemes' ),
					'no' => __( 'No', 'jozoorthemes' ),
                ),
            ),
                     array(
				    'name'    => '<a>&#8627; '.__( 'Add Another Content', 'jozoorthemes' ).'</a>',
				    'desc'    => __( 'you can add normal or shortcodes content, if you do not need adding content, let textarea empty', 'jozoorthemes' ),
				    'id'      => $prefix . 'home_2_show_latestnews_section_instead_content',
				    'type'    => 'wysiwyg',
				   'options' => array( 'textarea_rows' => 5, 'wpautop' => true ),
			       ),    
            
            array(
				'name' => __( 'Show - Testimonials', 'jozoorthemes' ),
				'desc' => '',
				'id'   => $prefix . 'home_2_show_testimonials_section',
				'type'    => 'radio_inline',
                'default' => 'yes',
				'options' => array(
					'yes' => __( 'Yes', 'jozoorthemes' ),
					'no' => __( 'No', 'jozoorthemes' ),
                ),
            ),
                     array(
				    'name'    => '<a>&#8627; '.__( 'Add Another Content', 'jozoorthemes' ).'</a>',
				    'desc'    => __( 'you can add normal or shortcodes content, if you do not need adding content, let textarea empty', 'jozoorthemes' ),
				    'id'      => $prefix . 'home_2_show_testimonials_section_instead_content',
				    'type'    => 'wysiwyg',
				   'options' => array( 'textarea_rows' => 5, 'wpautop' => true ),
			       ),      
            
            array(
				'name' => __( 'Show - Featured Clients', 'jozoorthemes' ),
				'desc' => '',
				'id'   => $prefix . 'home_2_show_clients_section',
				'type'    => 'radio_inline',
                'default' => 'yes',
				'options' => array(
					'yes' => __( 'Yes', 'jozoorthemes' ),
					'no' => __( 'No', 'jozoorthemes' ),
                ),
            ),
                     array(
				    'name'    => '<a>&#8627; '.__( 'Add Another Content', 'jozoorthemes' ).'</a>',
				    'desc'    => __( 'you can add normal or shortcodes content, if you do not need adding content, let textarea empty', 'jozoorthemes' ),
				    'id'      => $prefix . 'home_2_clients_section_instead_content',
				    'type'    => 'wysiwyg',
				   'options' => array( 'textarea_rows' => 5, 'wpautop' => true ),
			       ),
            
        ),
            
    );
    
    
    #=============================================================#
    # Home 3 Sections 
    #=============================================================#
    $meta_boxes['jozoor-metabox-page-home3-sections'] = array(
        'id' => 'jozoor-metabox-page-home3-sections',
        'title' => __('Home 3 Sections', 'jozoorthemes'),
        'pages' => array('page'), // post type
        'context' => 'normal',
        'priority' => 'high',
        'show_names' => true, // Show field names on the left
        'fields' => array(
            
              array(
				'name' => __( 'Show - Services', 'jozoorthemes' ),
				'desc' => '',
				'id'   => $prefix . 'home_3_show_services_section',
				'type'    => 'radio_inline',
                'default' => 'yes',
				'options' => array(
					'yes' => __( 'Yes', 'jozoorthemes' ),
					'no' => __( 'No', 'jozoorthemes' ),
                ),
             ),
                   array(
				    'name'    => '<a>&#8627; '.__( 'Add Another Content', 'jozoorthemes' ).'</a>',
				    'desc'    => __( 'you can add normal or shortcodes content, if you do not need adding content, let textarea empty', 'jozoorthemes' ),
				    'id'      => $prefix . 'home_3_services_section_instead_content',
				    'type'    => 'wysiwyg',
				   'options' => array( 'textarea_rows' => 5, 'wpautop' => true ),
			       ),
            
            array(
				'name' => __( 'Show - Recent Work', 'jozoorthemes' ),
				'desc' => '',
				'id'   => $prefix . 'home_3_show_recentwork_section',
				'type'    => 'radio_inline',
                'default' => 'yes',
				'options' => array(
					'yes' => __( 'Yes', 'jozoorthemes' ),
					'no' => __( 'No', 'jozoorthemes' ),
                ),
                     
            ),
                    array(
				    'name'    => '<a>&#8627; '.__( 'Add Another Content', 'jozoorthemes' ).'</a>',
				    'desc'    => __( 'you can add normal or shortcodes content, if you do not need adding content, let textarea empty', 'jozoorthemes' ),
				    'id'      => $prefix . 'home_3_recentwork_section_instead_content',
				    'type'    => 'wysiwyg',
				   'options' => array( 'textarea_rows' => 5, 'wpautop' => true ),
			       ),
            
            array(
				'name' => __( 'Show - Latest News', 'jozoorthemes' ),
				'desc' => '',
				'id'   => $prefix . 'home_3_show_latestnews_section',
				'type'    => 'radio_inline',
                'default' => 'yes',
				'options' => array(
					'yes' => __( 'Yes', 'jozoorthemes' ),
					'no' => __( 'No', 'jozoorthemes' ),
                ),
            ),
                     array(
				    'name'    => '<a>&#8627; '.__( 'Add Another Content', 'jozoorthemes' ).'</a>',
				    'desc'    => __( 'you can add normal or shortcodes content, if you do not need adding content, let textarea empty', 'jozoorthemes' ),
				    'id'      => $prefix . 'home_3_show_latestnews_section_instead_content',
				    'type'    => 'wysiwyg',
				   'options' => array( 'textarea_rows' => 5, 'wpautop' => true ),
			       ),      
            
            array(
				'name' => __( 'Show - Featured Clients', 'jozoorthemes' ),
				'desc' => '',
				'id'   => $prefix . 'home_3_show_clients_section',
				'type'    => 'radio_inline',
                'default' => 'yes',
				'options' => array(
					'yes' => __( 'Yes', 'jozoorthemes' ),
					'no' => __( 'No', 'jozoorthemes' ),
                ),
            ),
                     array(
				    'name'    => '<a>&#8627; '.__( 'Add Another Content', 'jozoorthemes' ).'</a>',
				    'desc'    => __( 'you can add normal or shortcodes content, if you do not need adding content, let textarea empty', 'jozoorthemes' ),
				    'id'      => $prefix . 'home_3_clients_section_instead_content',
				    'type'    => 'wysiwyg',
				   'options' => array( 'textarea_rows' => 5, 'wpautop' => true ),
			       ),
            
        ),
            
    );
    
    #=============================================================#
    # Home 4 Sections 
    #=============================================================#
    $meta_boxes['jozoor-metabox-page-home4-sections'] = array(
        'id' => 'jozoor-metabox-page-home4-sections',
        'title' => __('Home 4 Sections', 'jozoorthemes'),
        'pages' => array('page'), // post type
        'context' => 'normal',
        'priority' => 'high',
        'show_names' => true, // Show field names on the left
        'fields' => array(
            
              array(
				'name' => __( 'Show - Welcome Box', 'jozoorthemes' ),
				'desc' => '',
				'id'   => $prefix . 'home_4_show_welcome_section',
				'type'    => 'radio_inline',
                'default' => 'yes',
				'options' => array(
					'yes' => __( 'Yes', 'jozoorthemes' ),
					'no' => __( 'No', 'jozoorthemes' ),
                ),
             ),
            
                   array(
                   'name' => '<a>&#8627; '.__( 'Welcome Text', 'jozoorthemes' ).'</a>',
                   'desc'    => __( 'you can add normal or shortcodes content', 'jozoorthemes' ),
                   'default' => '',
                   'id' => $prefix . 'home_4_show_welcome_text',
                   'type' => 'wysiwyg',
                    'options' => array( 'textarea_rows' => 2, 'wpautop' => true ),
                   ),
                   array(
				    'name'    => '<a>&#8627; '.__( 'Add Another Content', 'jozoorthemes' ).'</a>',
				    'desc'    => __( 'you can add normal or shortcodes content, if you do not need adding content, let textarea empty', 'jozoorthemes' ),
				    'id'      => $prefix . 'home_4_welcome_section_instead_content',
				    'type'    => 'wysiwyg',
				   'options' => array( 'textarea_rows' => 5, 'wpautop' => true ),
			       ),
            
            array(
				'name' => __( 'Show - Recent Work', 'jozoorthemes' ),
				'desc' => '',
				'id'   => $prefix . 'home_4_show_recentwork_section',
				'type'    => 'radio_inline',
                'default' => 'yes',
				'options' => array(
					'yes' => __( 'Yes', 'jozoorthemes' ),
					'no' => __( 'No', 'jozoorthemes' ),
                ),
                     
            ),
                    array(
				    'name'    => '<a>&#8627; '.__( 'Add Another Content', 'jozoorthemes' ).'</a>',
				    'desc'    => __( 'you can add normal or shortcodes content, if you do not need adding content, let textarea empty', 'jozoorthemes' ),
				    'id'      => $prefix . 'home_4_recentwork_section_instead_content',
				    'type'    => 'wysiwyg',
				    'options' => array( 'textarea_rows' => 5, 'wpautop' => true ),
			       ),
            
            array(
				'name' => __( 'Show - Latest News', 'jozoorthemes' ),
				'desc' => '',
				'id'   => $prefix . 'home_4_show_latestnews_section',
				'type'    => 'radio_inline',
                'default' => 'yes',
				'options' => array(
					'yes' => __( 'Yes', 'jozoorthemes' ),
					'no' => __( 'No', 'jozoorthemes' ),
                ),
            ),
                     array(
				    'name'    => '<a>&#8627; '.__( 'Add Another Content', 'jozoorthemes' ).'</a>',
				    'desc'    => __( 'you can add normal or shortcodes content, if you do not need adding content, let textarea empty', 'jozoorthemes' ),
				    'id'      => $prefix . 'home_4_show_latestnews_section_instead_content',
				    'type'    => 'wysiwyg',
				   'options' => array( 'textarea_rows' => 5, 'wpautop' => true ),
			       ),      
            
            array(
				'name' => __( 'Show - Featured Clients', 'jozoorthemes' ),
				'desc' => '',
				'id'   => $prefix . 'home_4_show_clients_section',
				'type'    => 'radio_inline',
                'default' => 'yes',
				'options' => array(
					'yes' => __( 'Yes', 'jozoorthemes' ),
					'no' => __( 'No', 'jozoorthemes' ),
                ),
            ),
                     array(
				    'name'    => '<a>&#8627; '.__( 'Add Another Content', 'jozoorthemes' ).'</a>',
				    'desc'    => __( 'you can add normal or shortcodes content, if you do not need adding content, let textarea empty', 'jozoorthemes' ),
				    'id'      => $prefix . 'home_4_clients_section_instead_content',
				    'type'    => 'wysiwyg',
				   'options' => array( 'textarea_rows' => 5, 'wpautop' => true ),
			       ),
            
        ),
            
    );
    
    
    
    #=============================================================#
    # Page Puilder
    #=============================================================#
    $meta_boxes['jozoor-metabox-page-builder-options'] = array(
        'id' => 'jozoor-metabox-page-builder-options',
        'title' => __('Page Builder', 'jozoorthemes'),
        'pages' => array('page'), // post type
        'context' => 'normal',
        'priority' => 'high',
        'show_names' => true, // Show field names on the left
        'fields' => array(
            
             array(
				'name' => __( 'Using Page Builder', 'jozoorthemes' ),
				'desc' => __( 'yes use it', 'jozoorthemes' ),
				'id'   => $prefix . 'using_page_builder',
				'type' => 'checkbox',
			),
              array(
				'name' => __( 'Page Type', 'jozoorthemes' ),
				'desc' => '',
				'id'   => $prefix . 'page_builder_page_type',
				'type'    => 'radio_inline',
                'default' => 'normal',
				'options' => array(
					'normal' => __( 'Normal Page', 'jozoorthemes' ),
					'onepage' => __( 'OnePage', 'jozoorthemes' ),
                ),
            ),
            array(
             'name' => __( 'Select Onepage Menu', 'jozoorthemes' ),
             'desc' => __( 'here you can select your onepage menu name which created from : Appearance > Menus', 'jozoorthemes' ),
             'id' => $prefix . 'page_builder_page_one_menu',
             'taxonomy' => 'nav_menu',
             'type' => 'taxonomy_select',    
            ),
            
        ),
            
    );
    
    
#=============================================================#
# Page Options 
#=============================================================#
    $meta_boxes['jozoor-metabox-page'] = array(
        'id' => 'jozoor-metabox-page',
        'title' => __('Page Options', 'jozoorthemes'),
        'pages' => array('page'), // post type
        'context' => 'normal',
        'priority' => 'core',
        'show_names' => true, // Show field names on the left
        'fields' => array(
            
            array(
				'name' => __( 'Page Layout', 'jozoorthemes' ),
				'desc' => '',
				'id'   => $prefix . 'page_layout',
				'type'    => 'radio_inline',
                'default' => 'full',
				'options' => array(
					'full' => __( 'Full Width', 'jozoorthemes' ), 
					'right' => __( 'Right Sidebar', 'jozoorthemes' ), 
					'left' => __( 'Left Sidebar', 'jozoorthemes' ), 
                ),
            ),
            array(
				'name' => '<a>&#8627; '.__( 'Select Sidebar', 'jozoorthemes' ).'</a>',
				'desc' => '',
				'id'   => $prefix . 'page_sidebar',
				'type'    => 'select',
                'default' => 'default',
				'options' => array(
					'default' => __( 'Default Sidebar', 'jozoorthemes' ),
					'Sidebar2' => __( 'Sidebar2', 'jozoorthemes' ),
					'Sidebar3' => __( 'Sidebar3', 'jozoorthemes' ), 
                    'Sidebar4' => __( 'Sidebar4', 'jozoorthemes' ),
                    'Sidebar5' => __( 'Sidebar5', 'jozoorthemes' ),
                ),
            ),
            array(
				'name' => __( 'Show Slider', 'jozoorthemes' ),
				'desc' => '',
				'id'   => $prefix . 'show_page_slider',
				'type'    => 'select',
                'default' => 'no',
				'options' => array(
					'no' => __( 'No', 'jozoorthemes' ),
                    'yes' => __( 'Yes', 'jozoorthemes' ),
                ),
			),
            array(
				'name' => '<a>&#8627; '.__( 'Select Slider', 'jozoorthemes' ).'</a>',
				'desc' => __( 'this all sliders which you created from [ sliders ] page', 'jozoorthemes' ),
				'id'   => $prefix . 'page_slider_name',
				'type' => 'select',
				'options' => jozoor_cmb_get_post_options( array( 'post_type' => 'sliders', 'numberposts' => -1 ) ),
			),
            array(
				'name' => __( 'Show Page Title', 'jozoorthemes' ),
				'desc' => '',
				'id'   => $prefix . 'show_page_title',
				'type'    => 'select',
                'default' => 'yes',
				'options' => array(
					'yes' => __( 'Yes', 'jozoorthemes' ),
					'no' => __( 'No', 'jozoorthemes' ),
                ),
			),
            array(
				'name' => '<a>&#8627; '.__( 'Background for Page Title', 'jozoorthemes' ).'</a>',
				'desc' => '',
				'id'   => $prefix . 'page_title_bg',
				'type' => 'file',
                'attributes' => array(
                      'placeholder' => __( 'add url direct or upload image', 'jozoorthemes' ),
                ),
			),
            array(
				'name' => '<a>&#8627; '.__( 'Background Image Height', 'jozoorthemes' ).'</a>',
				'desc' => 'EX : 200',
				'id'   => $prefix . 'page_title_bg_height',
				'type' => 'text_small',
			),
            array(
				'name' => '<a>&#8627; '.__( 'Margin Top for Title', 'jozoorthemes' ).'</a>',
				'desc' => 'EX : 50',
				'id'   => $prefix . 'page_title_margin_top',
				'type' => 'text_small',
			),
            array(
				'name' => __( 'Custom Options', 'jozoorthemes' ),
				'desc' => __( 'yes i need to make custom changes', 'jozoorthemes' ),
				'id'   => $prefix . 'page_custom_options',
				'type' => 'checkbox',
			),
            array(
				'name' => '<a>&#8627; '.__( 'Layout Style', 'jozoorthemes' ).'</a>',
				'desc' => '',
				'id'   => $prefix . 'page_layout_style',
				'type'    => 'select',
                'default' => 'none',
				'options' => array(
                    'none' => __( 'Let Theme options layout', 'jozoorthemes' ),
					'wide' => __( 'Wide', 'jozoorthemes' ),
					'boxed' => __( 'Boxed', 'jozoorthemes' ),
                ),
			),
            array(
				'name' => '<a>&#8627;&#8627; '.__( 'Body Background image', 'jozoorthemes' ).'</a>',
				'desc' => '',
				'id'   => $prefix . 'page_body_bg',
				'type' => 'file',
                'attributes' => array(
                      'placeholder' => __( 'add url direct or upload image', 'jozoorthemes' ),
                ),
			),
            array(
				'name' => '<a>&#8627;&#8627; '.__( 'Body Background repeat', 'jozoorthemes' ).'</a>',
				'desc' => '',
				'id'   => $prefix . 'page_body_bg_repeat',
				'type'    => 'select',
                'default' => 'repeat',
				'options' => array(
					'repeat' => __( 'repeat', 'jozoorthemes' ),
					'repeat-x' => __( 'repeat-x', 'jozoorthemes' ),
                    'repeat-y' => __( 'repeat-y', 'jozoorthemes' ),
                    'fixed' => __( 'fixed', 'jozoorthemes' ),
                    'no-repeat' => __( 'no-repeat', 'jozoorthemes' ),
                ),
			),
            array(
               'name' => '<a>&#8627;&#8627; '.__( 'Body Background color', 'jozoorthemes' ).'</a>',
               'id'   => $prefix . 'page_body_bg_color',
               'type' => 'colorpicker',
               'default'  => '',
            ),
            array(
				'name' => '<a>&#8627; '.__( 'Header Layout', 'jozoorthemes' ).'</a>',
				'desc' => '',
				'id'   => $prefix . 'page_header_layout',
				'type'    => 'select',
                'default' => 'none',
				'options' => array(
					'none' => __( 'Let Theme options layout', 'jozoorthemes' ),
					'style-default' => __( 'Default Style', 'jozoorthemes' ),
                    'style-3' => __( 'Style 2', 'jozoorthemes' ),
                    'style-4' => __( 'Style 3', 'jozoorthemes' ),
                    'style-3 style-5' => __( 'Style 4', 'jozoorthemes' ),
                ),
			),
            array(
				'name' => '<a>&#8627; '.__( 'Fixed header', 'jozoorthemes' ).'</a>',
				'desc' => __( 'yes i need header fixed', 'jozoorthemes' ),
				'id'   => $prefix . 'page_fixed_header',
				'type' => 'checkbox',
			),
            array(
				'name' => '<a>&#8627; '.__( 'Remove Menu Item bg & top border for hover and current page', 'jozoorthemes' ).'</a>',
				'desc' => __( 'if you don\'t like bg and border in menu links, just check here to remove it.', 'jozoorthemes' ),
				'id'   => $prefix . 'page_menu_item_bg_border_none',
				'type' => 'checkbox',
			),
            array(
				'name' => '<a>&#8627;&#8627; '.__( 'Header Background image', 'jozoorthemes' ).'</a>',
				'desc' => '',
				'id'   => $prefix . 'page_header_bg',
				'type' => 'file',
                'attributes' => array(
                      'placeholder' => __( 'add url direct or upload image', 'jozoorthemes' ),
                ),
			),
            array(
				'name' => '<a>&#8627;&#8627; '.__( 'Header Background repeat', 'jozoorthemes' ).'</a>',
				'desc' => '',
				'id'   => $prefix . 'page_header_bg_repeat',
				'type'    => 'select',
                'default' => 'repeat',
				'options' => array(
					'repeat' => __( 'repeat', 'jozoorthemes' ),
					'repeat-x' => __( 'repeat-x', 'jozoorthemes' ),
                    'repeat-y' => __( 'repeat-y', 'jozoorthemes' ),
                    'no-repeat' => __( 'no-repeat', 'jozoorthemes' ),
                ),
			),
            array(
				'name' => '<a>&#8627; '.__( 'Hide Top Bar in header', 'jozoorthemes' ).'</a>',
				'desc' => __( 'yes hide it', 'jozoorthemes' ),
				'id'   => $prefix . 'page_hide_top_bar_header',
				'type' => 'checkbox',
			),
            array(
				'name' => '<a>&#8627; '.__( 'Hide Footer Widgets', 'jozoorthemes' ).'</a>',
				'desc' => __( 'yes hide it', 'jozoorthemes' ),
				'id'   => $prefix . 'page_hide_footer_widgets',
				'type' => 'checkbox',
			),
            array(
				'name' => '<a>&#8627; '.__( 'Show Search Icon in Nav Menu', 'jozoorthemes' ).'</a>',
				'desc' => __( 'yes show it', 'jozoorthemes' ),
				'id'   => $prefix . 'page_show_search_icon',
				'type' => 'checkbox',
			),
            array(
				'name' => '<a>&#8627; '.__( 'Show Top Header Menu instead of social links', 'jozoorthemes' ).'</a>',
				'desc' => __( 'yes show it', 'jozoorthemes' ),
				'id'   => $prefix . 'page_show_top_header_menu',
				'type' => 'checkbox',
			),
            array(
				'name' => '<a>&#8627; '.__( 'Show Down Footer Menu instead of social links', 'jozoorthemes' ).'</a>',
				'desc' => __( 'yes show it', 'jozoorthemes' ),
				'id'   => $prefix . 'page_show_down_footer_menu',
				'type' => 'checkbox',
			),
            
        ),
    );
    

    return $meta_boxes;
}
add_filter( 'cmb_meta_boxes', 'jozoor_metaboxs_options_page' );

?>