<?php

function jozoor_metaboxs_options_user( $meta_boxes ) {
    
$prefix = '_jozoor_'; // Prefix for all fields
    

    $meta_boxes['jozoor-metabox-user'] = array(
        'id' => 'jozoor-metabox-user',
        'title' => __('User Profile Options', 'jozoorthemes'),
        'pages' => array('user'), // users
        'show_names' => true, // Show field names on the left
        'fields' => array(
            
            array(
				'name' => __( 'Twitter URL', 'jozoorthemes' ),
				'desc' => '',
				'id'   => $prefix . 'user_twitter_url',
				'type' => 'text_url',
			),
            array(
				'name' => __( 'Facebook URL', 'jozoorthemes' ),
				'desc' => '',
				'id'   => $prefix . 'user_facebook_url',
				'type' => 'text_url',
			),
            array(
				'name' => __( 'Googl+ URL', 'jozoorthemes' ),
				'desc' => '',
				'id'   => $prefix . 'user_googleplus_url',
				'type' => 'text_url',
			),
            array(
				'name' => __( 'Linkedin URL', 'jozoorthemes' ),
				'desc' => '',
				'id'   => $prefix . 'user_linkedin_url',
				'type' => 'text_url',
			),
            array(
				'name' => __( 'Dribbble URL', 'jozoorthemes' ),
				'desc' => '',
				'id'   => $prefix . 'user_dribbble_url',
				'type' => 'text_url',
			),
            array(
				'name' => __( 'Behance URL', 'jozoorthemes' ),
				'desc' => '',
				'id'   => $prefix . 'user_behance_url',
				'type' => 'text_url',
			),
            array(
				'name' => __( 'Instagram URL', 'jozoorthemes' ),
				'desc' => '',
				'id'   => $prefix . 'user_instagram_url',
				'type' => 'text_url',
			),
            array(
				'name' => __( 'Youtube URL', 'jozoorthemes' ),
				'desc' => '',
				'id'   => $prefix . 'user_youtube_url',
				'type' => 'text_url',
			),
            array(
				'name' => __( 'Pinterest URL', 'jozoorthemes' ),
				'desc' => '',
				'id'   => $prefix . 'user_pinterest_url',
				'type' => 'text_url',
			),
            array(
				'name' => __( 'Flickr URL', 'jozoorthemes' ),
				'desc' => '',
				'id'   => $prefix . 'user_flickr_url',
				'type' => 'text_url',
			),
            array(
				'name' => __( 'Vimeo URL', 'jozoorthemes' ),
				'desc' => '',
				'id'   => $prefix . 'user_vimeo_url',
				'type' => 'text_url',
			),
            array(
				'name' => __( 'Tumblr URL', 'jozoorthemes' ),
				'desc' => '',
				'id'   => $prefix . 'user_tumblr_url',
				'type' => 'text_url',
			),
            
        ),
    );
    

    return $meta_boxes;
}
add_filter( 'cmb_meta_boxes', 'jozoor_metaboxs_options_user' );

?>