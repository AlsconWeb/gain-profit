<script>

jQuery(document).ready(function($) {

<?php if($jozoor_totop_icon == 1) { ?>
// UItoTop plugin 	
$().UItoTop({ easingType: 'easeOutQuart' });
<?php } ?>
    
<?php if($j_page_custom_options == 'on' && $j_page_show_search_icon == 'on') { ?>
    
// appened search icon & form to nav menu
$('#menu > ul').append($('<li class="nav-search"></li>').html('<a><i class="icon-search"></i></a><div class="nav-search-from widget search"><form action="<?php echo home_url(); ?>" id="searchform" method="get"><input type="text" id="s" name="s" class="text-search" value="" placeholder="<?php _e("Search...", "jozoorthemes"); ?>" /><input type="submit" value="" id="searchsubmit" class="submit-search" /></form></div>'));
    
// show search form
$(".nav-search > a").click(function (e) {
  if($('body').hasClass('rtl')) {
  $('.nav-search .nav-search-from').fadeToggle("fast").css({"left":0});    
  } else {
  $('.nav-search .nav-search-from').fadeToggle("fast").css({"right":0});
  }
});
$(document).mouseup(function(event){
    var target = $(".nav-search .nav-search-from, .nav-search .nav-search-from form input");
    if(!target.is(event.target) && !$(".nav-search > a").is(event.target) && target.is(":visible")){
       if($('body').hasClass('rtl')) {
  $('.nav-search .nav-search-from').fadeToggle("fast").css({"left":0});    
  } else {
  $('.nav-search .nav-search-from').fadeToggle("fast").css({"right":0});
  }
    }else{
      return false;
    }
});
    
<?php } else { ?>
    
<?php if($jozoor_show_search_icon == 1) { ?>
// appened search icon & form to nav menu
$('#menu > ul').append($('<li class="nav-search"></li>').html('<a><i class="icon-search"></i></a><div class="nav-search-from widget search"><form action="<?php echo home_url(); ?>" id="searchform" method="get"><input type="text" id="s" name="s" class="text-search" value="" placeholder="<?php _e("Search...", "jozoorthemes"); ?>" /><input type="submit" value="" id="searchsubmit" class="submit-search" /></form></div>'));
    
// show search form
$(".nav-search > a").click(function (e) {
  if($('body').hasClass('rtl')) {
  $('.nav-search .nav-search-from').fadeToggle("fast").css({"left":0});    
  } else {
  $('.nav-search .nav-search-from').fadeToggle("fast").css({"right":0});
  }
});
$(document).mouseup(function(event){
    var target = $(".nav-search .nav-search-from, .nav-search .nav-search-from form input");
    if(!target.is(event.target) && !$(".nav-search > a").is(event.target) && target.is(":visible")){
       if($('body').hasClass('rtl')) {
  $('.nav-search .nav-search-from').fadeToggle("fast").css({"left":0});    
  } else {
  $('.nav-search .nav-search-from').fadeToggle("fast").css({"right":0});
  }
    }else{
      return false;
    }
});

<?php } ?>
    
<?php } ?>
    
<?php if($j_page_custom_options == 'on' && $j_page_fixed_header == 'on' && $j_page_header_layout != 'none') { ?>
 
<?php if($j_page_header_layout == 'style-3' || $j_page_header_layout == 'style-3 style-5') { ?>
/* this for header 3 and 5*/
$("header.fixed .down-header").sticky({ topSpacing: 0 });
<?php } else { ?>
/* header fixed with sticky plugin */
$("header.fixed .main-header").sticky({ topSpacing: 0 });
<?php } ?>
    
<?php } else { ?>
    
<?php if($jozoor_header_layout == 'style-3' || $jozoor_header_layout == 'style-3 style-5' || $j_page_header_layout == 'style-3' || $j_page_header_layout == 'style-3 style-5') { ?>
/* this for header 3 and 5*/
$("header.fixed .down-header").sticky({ topSpacing: 0 });
<?php } else { ?>
/* header fixed with sticky plugin */
$("header.fixed .main-header").sticky({ topSpacing: 0 });
<?php } ?>
    
<?php } ?>
$('.sticky-wrapper').removeAttr('style');
    

// Flex Slider
$(window).ready(function() {
    $('.flex-caption').css('display', 'block');
    $('.flex-container').css('display', 'block');	
    $('.slider-1 .flexslider ul.slides li:first, .slider-2 .flexslider4 ul.slides li:first').addClass('flex-active-slide');

   $('.flexslider').flexslider({
	animation: 'fade',
	animationLoop: <?php if($jozoor_slider1_animationloop == 0) { echo 'false'; } else { echo 'true'; } ?>,           
	slideshow: <?php if($jozoor_slider1_slideshow == 0) { echo 'false'; } else { echo 'true'; } ?>,               
	slideshowSpeed: <?php if(!empty($jozoor_slider1_slideshowSpeed)) { echo $jozoor_slider1_slideshowSpeed; } else { echo '6000'; } ?>,          
	animationSpeed: <?php if(!empty($jozoor_slider1_animationSpeed)) { echo $jozoor_slider1_animationSpeed; } else { echo '800'; } ?>,           
	pauseOnHover: <?php if($jozoor_slider1_pauseOnHover == 0) { echo 'false'; } else { echo 'true'; } ?>, 
	pauseOnAction: <?php if($jozoor_slider1_pauseOnAction == 0) { echo 'false'; } else { echo 'true'; } ?>,
	controlNav: false,
    video: true,
    useCSS:false,
	directionNav: <?php if($jozoor_slider1_directionNav == 0) { echo 'false'; } else { echo 'true'; } ?>,
	controlsContainer: '.flex-container',
	start: function(slider) {
		 var toptitle = $('.slider-1 .flex-active-slide h2').data('toptitle');
		 var topimage = $('.slider-1 .flex-active-slide .item').data('topimage');
		 var bottomtext = $('.slider-1 .flex-active-slide .slide-desc').data('bottomtext');
		 var bottomlinks = $('.slider-1 .flex-active-slide .links').data('bottomlinks');
		 
		 $('.slider-1 .flex-active-slide').find('.item').css({ top: topimage});
         $('.slider-1 .flex-active-slide').find('.item').animate({ right: '0', opacity: '1'}, 1500);
		 $('.slider-1 .flex-active-slide').find('h2').animate({ left: '0', top: toptitle, opacity: '1'}, 1800);
		 $('.slider-1 .flex-active-slide').find('.slide-desc').animate({ left: '0', bottom: bottomtext, opacity: '1'}, 2000);
		 $('.slider-1 .flex-active-slide').find('.links').animate({ left: '0', bottom: bottomlinks, opacity: '1'}, 2300);
		 
		 // remove class loading after start
		 slider.removeClass('loading');
      },
	before: function(slider) {
         $('.slider-1 .flex-active-slide').find('.item').animate({ right: '-100%', opacity: '0'}, 1500);
		 $('.slider-1 .flex-active-slide').find('h2').animate({ left: '0', top: '-100%', opacity: '0'}, 1800);
		 $('.slider-1 .flex-active-slide').find('.slide-desc').animate({ left: '0', bottom: '-50%', opacity: '0'}, 2000);
		 $('.slider-1 .flex-active-slide').find('.links').animate({ left: '0', bottom: '-100%', opacity: '0'}, 2300);
      },
	after: function(slider) {
		 var toptitle = $('.slider-1 .flex-active-slide h2').data('toptitle');
		 var topimage = $('.slider-1 .flex-active-slide .item').data('topimage');
		 var bottomtext = $('.slider-1 .flex-active-slide .slide-desc').data('bottomtext');
		 var bottomlinks = $('.slider-1 .flex-active-slide .links').data('bottomlinks');
		 
		 $('.slider-1 .flex-active-slide').find('.item').css({ top: topimage});
         $('.slider-1 .flex-active-slide').find('.item').animate({ right: '0', opacity: '1'}, 1500);
		 $('.slider-1 .flex-active-slide').find('h2').animate({ left: '0', top: toptitle, opacity: '1'}, 1800);
		 $('.slider-1 .flex-active-slide').find('.slide-desc').animate({ left: '0', bottom: bottomtext, opacity: '1'}, 2000);
		 $('.slider-1 .flex-active-slide').find('.links').animate({ left: '0', bottom: bottomlinks, opacity: '1'}, 2300);
      }
		});
  
  $('.flexslider4').flexslider({
	animation: 'fade',
	animationLoop: <?php if($jozoor_slider2_animationloop == 0) { echo 'false'; } else { echo 'true'; } ?>,           
	slideshow: <?php if($jozoor_slider2_slideshow == 0) { echo 'false'; } else { echo 'true'; } ?>,               
	slideshowSpeed: <?php if(!empty($jozoor_slider2_slideshowSpeed)) { echo $jozoor_slider2_slideshowSpeed; } else { echo '6000'; } ?>,          
	animationSpeed: <?php if(!empty($jozoor_slider2_animationSpeed)) { echo $jozoor_slider2_animationSpeed; } else { echo '800'; } ?>,           
	pauseOnHover: <?php if($jozoor_slider2_pauseOnHover == 0) { echo 'false'; } else { echo 'true'; } ?>, 
	pauseOnAction: <?php if($jozoor_slider2_pauseOnAction == 0) { echo 'false'; } else { echo 'true'; } ?>,
	controlNav: false,
	directionNav: <?php if($jozoor_slider2_directionNav == 0) { echo 'false'; } else { echo 'true'; } ?>,
	controlsContainer: '.flex-container',
	start: function(slider) {
		 var bottomtitle = $('.slider-2 .flex-active-slide h2').data('bottomtitle');
		 var bottomtext = $('.slider-2 .flex-active-slide .slide-desc').data('bottomtext');
		 var bottomlinks = $('.slider-2 .flex-active-slide .links').data('bottomlinks');
         var bottomimage = $('.slider-2 .flex-active-slide .item').data('bottomimage');
		 
		 $('.slider-2 .flex-active-slide').find('h2').animate({ bottom: bottomtitle, opacity: '1'}, 1500);
		 $('.slider-2 .flex-active-slide').find('.slide-desc').animate({ bottom: bottomtext, opacity: '1'}, 2000);
		 $('.slider-2 .flex-active-slide').find('.links').animate({ bottom: bottomlinks, opacity: '1'}, 2200);
         $('.slider-2 .flex-active-slide').find('.item').animate({ bottom: bottomimage, opacity: '1'}, 2000);
		 
		 // remove class loading after start
		 slider.removeClass('loading');
      },
	before: function(slider) {
		 $('.slider-2 .flex-active-slide').find('h2').animate({ bottom: '-20%', opacity: '0'}, 1500);
		 $('.slider-2 .flex-active-slide').find('.slide-desc').animate({ bottom: '-50%', opacity: '0'}, 2000);
		 $('.slider-2 .flex-active-slide').find('.links').animate({ bottom: '-60%', opacity: '0'}, 2200);
         $('.slider-2 .flex-active-slide').find('.item').animate({ bottom: '-80%', opacity: '0'}, 2000);
      },
	after: function(slider) {
		  var bottomtitle = $('.slider-2 .flex-active-slide h2').data('bottomtitle');
		 var bottomtext = $('.slider-2 .flex-active-slide .slide-desc').data('bottomtext');
		 var bottomlinks = $('.slider-2 .flex-active-slide .links').data('bottomlinks');
         var bottomimage = $('.slider-2 .flex-active-slide .item').data('bottomimage');
		 
		 $('.slider-2 .flex-active-slide').find('h2').animate({ bottom: bottomtitle, opacity: '1'}, 1500);
		 $('.slider-2 .flex-active-slide').find('.slide-desc').animate({  bottom: bottomtext, opacity: '1'}, 2000);
		 $('.slider-2 .flex-active-slide').find('.links').animate({  bottom: bottomlinks, opacity: '1'}, 2200);
         $('.slider-2 .flex-active-slide').find('.item').animate({ bottom: bottomimage, opacity: '1'}, 2000);
      }
		}); 

});

});
    
</script>