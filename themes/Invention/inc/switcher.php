<?php

if($jozoor_style_switcher == 1 && $jozoor_custom_design_colors == 0 && $jozoor_custom_body_bg_image == 0){
    
    $path = explode('/', $jozoor_body_bg_image);
    $filename = $path[count($path)-1];
    
    $class_name = str_replace('.jpg', '', $filename); 

?>  
<!-- Style Switcher Box -->
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/switcher/style.css">

<script>

/* Style Switcher */

window.console = window.console || (function(){
	var c = {}; c.log = c.warn = c.debug = c.info = c.error = c.time = c.dir = c.profile = c.clear = c.exception = c.trace = c.assert = function(){};
	return c;
})();

jQuery(document).ready(function($) {
						   
var styleswitcherstr = ' \
	<h2><?php _e("Style Switcher ", "jozoorthemes"); ?> <a href="#"><i class="icon-cog s-26"></i></a></h2> \
    <div class="content2"> \
    <h3><?php _e("Layout Style", "jozoorthemes"); ?></h3> \
	<div class="layout-switcher"> \
    <a id="wide" class="layout current"><?php _e("Wide", "jozoorthemes"); ?></a> \
    <a id="boxed" class="layout"><?php _e("boxed", "jozoorthemes"); ?></a> \
    </div> \
    <div class="clear"></div> \
    <h3><?php _e("Color Style", "jozoorthemes"); ?></h3> \
    <div class="switcher-box"> \
    <a id="red" class="styleswitch color current"></a> \
    <a id="orange" class="styleswitch color"></a> \
	<a id="turquoise" class="styleswitch color"></a> \
    <a id="green" class="styleswitch color"></a> \
    <a id="blue" class="styleswitch color"></a> \
    </div><!-- End switcher-box --> \
    <div class="clear"></div>  \
    <div class="bg hidden">  \
    <h3><?php _e("BG Pattern", "jozoorthemes"); ?></h3>  \
    <a id="wood" class="pattern current"></a> \
    <a id="crossed" class="pattern"></a> \
    <a id="fabric" class="pattern"></a> \
    <a id="linen" class="pattern"></a> \
    <a id="diagmonds" class="pattern"></a> \
    <a id="triangles" class="pattern"></a> \
    <a id="black_thread" class="pattern"></a> \
    <a id="checkered_pattern" class="pattern"></a> \
    <a id="black_mamba" class="pattern"></a> \
    <a id="back_pattern" class="pattern"></a> \
    <a id="vichy" class="pattern"></a> \
    <a id="diamond_upholstery" class="pattern"></a> \
    <a id="lyonnette" class="pattern"></a> \
    <a id="graphy" class="pattern"></a> \
    <a id="subtlenet2" class="pattern"></a> \
    </div> \
    <div class="clear"></div> \
    <br> \
	<a id="reset" class="dark-style"><?php _e("Reset Styles", "jozoorthemes"); ?></a> \
    </div><!-- End content --> \
	';
	
$(".switcher").prepend( styleswitcherstr );

});

/* boxed & wide syle */
jQuery(document).ready(function($) {

var cookieName = '<?php echo $jozoor_layout_style; ?>';

function changeLayout(layout) {
$.cookie(cookieName, layout);
$('head link[id=layout-style-css]').attr('href', '<?php echo get_template_directory_uri(); ?>/css/layout/' + layout + '.css');
}

if( $.cookie(cookieName)) {
changeLayout($.cookie(cookieName));
}

$("#wide").click( function(){ $
changeLayout('wide');
$("#boxed").removeClass('current');
$(this).addClass('current');
});

$("#boxed").click( function(){ $
changeLayout('boxed');
$("#wide").removeClass('current');
$(this).addClass('current');
});

/* reset */
$("#reset").click( function(){ $
changeLayout('<?php echo $jozoor_layout_style; ?>');
<?php if ($jozoor_layout_style == 'wide') { ?>
$("#boxed").removeClass('current');
$("#wide").addClass('current');
<?php } else {  ?>
$("#wide").removeClass('current');
$("#boxed").addClass('current');
<?php } ?>
});

});


/* background images */
jQuery(document).ready(function($) {
  
  var startClass = $.cookie('mycookie');
  $("body").addClass("<?php echo $class_name; ?>");

/* crossed */
$("#crossed").click( function(){ 
  $("body").removeClass('fabric');
  $("body").removeClass('linen');
  $("body").removeClass('wood');
  $("body").removeClass('diagmonds');
  $("body").removeClass('triangles');
  $("body").removeClass('black_mamba');
  $("body").removeClass('vichy');
  $("body").removeClass('back_pattern');
  $("body").removeClass('checkered_pattern');
  $("body").removeClass('diamond_upholstery');
  $("body").removeClass('lyonnette');
  $("body").removeClass('graphy');
  $("body").removeClass('black_thread');
  $("body").removeClass('subtlenet2');
  $("body").addClass('crossed');
  $.cookie('mycookie','crossed');
  $(".pattern").removeClass('current');
  $(this).addClass('current');
});

/* fabric */
$("#fabric").click( function(){ 
  $("body").removeClass('crossed');
  $("body").removeClass('linen');
  $("body").removeClass('wood');
  $("body").removeClass('diagmonds');
  $("body").removeClass('triangles');
  $("body").removeClass('black_mamba');
  $("body").removeClass('vichy');
  $("body").removeClass('back_pattern');
  $("body").removeClass('checkered_pattern');
  $("body").removeClass('diamond_upholstery');
  $("body").removeClass('lyonnette');
  $("body").removeClass('graphy');
  $("body").removeClass('black_thread');
  $("body").removeClass('subtlenet2');
  $("body").addClass('fabric');
  $.cookie('mycookie','fabric');
  $(".pattern").removeClass('current');
  $(this).addClass('current');
});

/* linen */
$("#linen").click( function(){ 
  $("body").removeClass('crossed');
  $("body").removeClass('fabric');
  $("body").removeClass('wood');
  $("body").removeClass('diagmonds');
  $("body").removeClass('triangles');
  $("body").removeClass('black_mamba');
  $("body").removeClass('vichy');
  $("body").removeClass('back_pattern');
  $("body").removeClass('checkered_pattern');
  $("body").removeClass('diamond_upholstery');
  $("body").removeClass('lyonnette');
  $("body").removeClass('graphy');
  $("body").removeClass('black_thread');
  $("body").removeClass('subtlenet2');
  $("body").addClass('linen');
  $.cookie('mycookie','linen');
  $(".pattern").removeClass('current');
  $(this).addClass('current');
});

/* wood */
$("#wood").click( function(){ 
  $("body").removeClass('crossed');
  $("body").removeClass('fabric');
  $("body").removeClass('linen');
  $("body").removeClass('diagmonds');
  $("body").removeClass('triangles');
  $("body").removeClass('black_mamba');
  $("body").removeClass('vichy');
  $("body").removeClass('back_pattern');
  $("body").removeClass('checkered_pattern');
  $("body").removeClass('diamond_upholstery');
  $("body").removeClass('lyonnette');
  $("body").removeClass('graphy');
  $("body").removeClass('black_thread');
  $("body").removeClass('subtlenet2');
  $("body").addClass('wood');
  $.cookie('mycookie','wood');
  $(".pattern").removeClass('current');
  $(this).addClass('current');
});

/* diagmonds */
$("#diagmonds").click( function(){ 
  $("body").removeClass('crossed');
  $("body").removeClass('fabric');
  $("body").removeClass('linen');
  $("body").removeClass('wood');
  $("body").removeClass('triangles');
  $("body").removeClass('black_mamba');
  $("body").removeClass('vichy');
  $("body").removeClass('back_pattern');
  $("body").removeClass('checkered_pattern');
  $("body").removeClass('diamond_upholstery');
  $("body").removeClass('lyonnette');
  $("body").removeClass('graphy');
  $("body").removeClass('black_thread');
  $("body").removeClass('subtlenet2');
  $("body").addClass('diagmonds');
  $.cookie('mycookie','diagmonds');
  $(".pattern").removeClass('current');
  $(this).addClass('current');
});

/* triangles */
$("#triangles").click( function(){ 
  $("body").removeClass('crossed');
  $("body").removeClass('fabric');
  $("body").removeClass('linen');
  $("body").removeClass('wood');
  $("body").removeClass('diagmonds');
  $("body").removeClass('black_mamba');
  $("body").removeClass('vichy');
  $("body").removeClass('back_pattern');
  $("body").removeClass('checkered_pattern');
  $("body").removeClass('diamond_upholstery');
  $("body").removeClass('lyonnette');
  $("body").removeClass('graphy');
  $("body").removeClass('black_thread');
  $("body").removeClass('subtlenet2');
  $("body").addClass('triangles');
  $.cookie('mycookie','triangles');
  $(".pattern").removeClass('current');
  $(this).addClass('current');
});

/* triangles */
$("#black_mamba").click( function(){ 
  $("body").removeClass('crossed');
  $("body").removeClass('fabric');
  $("body").removeClass('linen');
  $("body").removeClass('wood');
  $("body").removeClass('diagmonds');
  $("body").removeClass('triangles');
  $("body").removeClass('vichy');
  $("body").removeClass('back_pattern');
  $("body").removeClass('checkered_pattern');
  $("body").removeClass('diamond_upholstery');
  $("body").removeClass('lyonnette');
  $("body").removeClass('graphy');
  $("body").removeClass('black_thread');
  $("body").removeClass('subtlenet2');
  $("body").addClass('black_mamba');
  $.cookie('mycookie','black_mamba');
  $(".pattern").removeClass('current');
  $(this).addClass('current');
});

/* vichy */
$("#vichy").click( function(){ 
  $("body").removeClass('crossed');
  $("body").removeClass('fabric');
  $("body").removeClass('linen');
  $("body").removeClass('wood');
  $("body").removeClass('diagmonds');
  $("body").removeClass('triangles');
  $("body").removeClass('black_mamba');
  $("body").removeClass('back_pattern');
  $("body").removeClass('checkered_pattern');
  $("body").removeClass('diamond_upholstery');
  $("body").removeClass('lyonnette');
  $("body").removeClass('graphy');
  $("body").removeClass('black_thread');
  $("body").removeClass('subtlenet2');
  $("body").addClass('vichy');
  $.cookie('mycookie','vichy');
  $(".pattern").removeClass('current');
  $(this).addClass('current');
});

/* back_pattern */
$("#back_pattern").click( function(){ 
  $("body").removeClass('crossed');
  $("body").removeClass('fabric');
  $("body").removeClass('linen');
  $("body").removeClass('wood');
  $("body").removeClass('diagmonds');
  $("body").removeClass('triangles');
  $("body").removeClass('black_mamba');
  $("body").removeClass('vichy');
  $("body").removeClass('checkered_pattern');
  $("body").removeClass('diamond_upholstery');
  $("body").removeClass('lyonnette');
  $("body").removeClass('graphy');
  $("body").removeClass('black_thread');
  $("body").removeClass('subtlenet2');
  $("body").addClass('back_pattern');
  $.cookie('mycookie','back_pattern');
  $(".pattern").removeClass('current');
  $(this).addClass('current');
});

/* checkered_pattern */
$("#checkered_pattern").click( function(){ 
  $("body").removeClass('crossed');
  $("body").removeClass('fabric');
  $("body").removeClass('linen');
  $("body").removeClass('wood');
  $("body").removeClass('diagmonds');
  $("body").removeClass('triangles');
  $("body").removeClass('black_mamba');
  $("body").removeClass('vichy');
  $("body").removeClass('back_pattern');
  $("body").removeClass('diamond_upholstery');
  $("body").removeClass('lyonnette');
  $("body").removeClass('graphy');
  $("body").removeClass('black_thread');
  $("body").removeClass('subtlenet2');
  $("body").addClass('checkered_pattern');
  $.cookie('mycookie','checkered_pattern');
  $(".pattern").removeClass('current');
  $(this).addClass('current');
});

/* diamond_upholstery */
$("#diamond_upholstery").click( function(){ 
  $("body").removeClass('crossed');
  $("body").removeClass('fabric');
  $("body").removeClass('linen');
  $("body").removeClass('wood');
  $("body").removeClass('diagmonds');
  $("body").removeClass('triangles');
  $("body").removeClass('black_mamba');
  $("body").removeClass('vichy');
  $("body").removeClass('back_pattern');
  $("body").removeClass('checkered_pattern');
  $("body").removeClass('lyonnette');
  $("body").removeClass('graphy');
  $("body").removeClass('black_thread');
  $("body").removeClass('subtlenet2');
  $("body").addClass('diamond_upholstery');
  $.cookie('mycookie','diamond_upholstery');
  $(".pattern").removeClass('current');
  $(this).addClass('current');
});

/* lyonnette */
$("#lyonnette").click( function(){ 
  $("body").removeClass('crossed');
  $("body").removeClass('fabric');
  $("body").removeClass('linen');
  $("body").removeClass('wood');
  $("body").removeClass('diagmonds');
  $("body").removeClass('triangles');
  $("body").removeClass('black_mamba');
  $("body").removeClass('vichy');
  $("body").removeClass('back_pattern');
  $("body").removeClass('checkered_pattern');
  $("body").removeClass('diamond_upholstery');
  $("body").removeClass('graphy');
  $("body").removeClass('black_thread');
  $("body").removeClass('subtlenet2');
  $("body").addClass('lyonnette');
  $.cookie('mycookie','lyonnette');
  $(".pattern").removeClass('current');
  $(this).addClass('current');
});

/* graphy */
$("#graphy").click( function(){ 
  $("body").removeClass('crossed');
  $("body").removeClass('fabric');
  $("body").removeClass('linen');
  $("body").removeClass('wood');
  $("body").removeClass('diagmonds');
  $("body").removeClass('triangles');
  $("body").removeClass('black_mamba');
  $("body").removeClass('vichy');
  $("body").removeClass('back_pattern');
  $("body").removeClass('checkered_pattern');
  $("body").removeClass('diamond_upholstery');
  $("body").removeClass('lyonnette');
  $("body").removeClass('black_thread');
  $("body").removeClass('subtlenet2');
  $("body").addClass('graphy');
  $.cookie('mycookie','graphy');
  $(".pattern").removeClass('current');
  $(this).addClass('current');
});

/* black_thread */
$("#black_thread").click( function(){ 
  $("body").removeClass('crossed');
  $("body").removeClass('fabric');
  $("body").removeClass('linen');
  $("body").removeClass('wood');
  $("body").removeClass('diagmonds');
  $("body").removeClass('triangles');
  $("body").removeClass('black_mamba');
  $("body").removeClass('vichy');
  $("body").removeClass('back_pattern');
  $("body").removeClass('checkered_pattern');
  $("body").removeClass('diamond_upholstery');
  $("body").removeClass('lyonnette');
  $("body").removeClass('graphy');
  $("body").removeClass('subtlenet2');
  $("body").addClass('black_thread');
  $.cookie('mycookie','black_thread');
  $(".pattern").removeClass('current');
  $(this).addClass('current');
});

/* subtlenet2 */
$("#subtlenet2").click( function(){ 
  $("body").removeClass('crossed');
  $("body").removeClass('fabric');
  $("body").removeClass('linen');
  $("body").removeClass('wood');
  $("body").removeClass('diagmonds');
  $("body").removeClass('triangles');
  $("body").removeClass('black_mamba');
  $("body").removeClass('vichy');
  $("body").removeClass('back_pattern');
  $("body").removeClass('checkered_pattern');
  $("body").removeClass('diamond_upholstery');
  $("body").removeClass('lyonnette');
  $("body").removeClass('graphy');
  $("body").removeClass('black_thread');
  $("body").addClass('subtlenet2');
  $.cookie('mycookie','subtlenet2');
  $(".pattern").removeClass('current');
  $(this).addClass('current');
});

/* reset */
$("#reset").click( function(){ 
  $("body").removeClass('crossed');
  $("body").removeClass('fabric');
  $("body").removeClass('linen');
  $("body").removeClass('diagmonds');
  $("body").removeClass('triangles');
  $("body").removeClass('black_mamba');
  $("body").removeClass('vichy');
  $("body").removeClass('back_pattern');
  $("body").removeClass('checkered_pattern');
  $("body").removeClass('diamond_upholstery');
  $("body").removeClass('lyonnette');
  $("body").removeClass('graphy');
  $("body").removeClass('black_thread');
  $("body").removeClass('subtlenet2');
  $("body").addClass('<?php echo $class_name; ?>');
  $.cookie('mycookie','<?php echo $class_name; ?>');
  $(".pattern").removeClass('current');
  $("#<?php echo $class_name; ?>").addClass('current');
});



if ($.cookie('mycookie')) {
  $('body').addClass($.cookie('mycookie'));
}

});

/* Skins Style */
jQuery(document).ready(function($) {

var cookieName = '<?php echo $jozoor_default_color_scheme; ?>';

function changeLayout(layout) {
$.cookie(cookieName, layout);
$('head link[id=color-style-css]').attr('href', '<?php echo get_template_directory_uri(); ?>/css/skins/colors/' + layout + '.css');
}

if( $.cookie(cookieName)) {
changeLayout($.cookie(cookieName));
}

$("#red").click( function(){ $
changeLayout('red');
$(".styleswitch").removeClass('current');
$(this).addClass('current');
});

$("#orange").click( function(){ $
changeLayout('orange');
$(".styleswitch").removeClass('current');
$(this).addClass('current');
});

$("#green").click( function(){ $
changeLayout('green');
$(".styleswitch").removeClass('current');
$(this).addClass('current');
});

$("#blue").click( function(){ $
changeLayout('blue');
$(".styleswitch").removeClass('current');
$(this).addClass('current');
});

$("#turquoise").click( function(){ $
changeLayout('turquoise');
$(".styleswitch").removeClass('current');
$(this).addClass('current');
});

/* reset */
$("#reset").click( function(){ $
changeLayout('<?php echo $jozoor_default_color_scheme; ?>');
$(".styleswitch").removeClass('current');
$("#<?php echo $jozoor_default_color_scheme; ?>").addClass('current');
});


});


/* Reset Switcher */
jQuery(document).ready(function($) {
    
// show after page loading
$(window).load(function() {
$('.switcher').show();
});

// Style Switcher	
$('.switcher').animate({
left: '-160px'
});

$('.switcher h2 a').click(function(e){
e.preventDefault();
var div = $('.switcher');
console.log(div.css('left'));
if (div.css('left') === '-160px') {
$('.switcher').animate({
  left: '0px'
}); 
} else {
$('.switcher').animate({
  left: '-160px'
});
}
})

		 
});						   

</script>
  
<?php } else { 
    echo '
    <style>
    .switcher {
        display:none;
    }
    </style>
    ';
} ?>

      