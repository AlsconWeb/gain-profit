<?php

$cons_timer = explode(" | ", $jozoor_under_cons_timer);
 // $cons_timer[0]; // year
 // $cons_timer[1]; // month
 // $cons_timer[2]; // day
 // $cons_timer[3]; // hour
 // $cons_timer[4]; // minute
 // $cons_timer[5]; // time zone

?>

<script>

jQuery(document).ready(function($) {
    
    $(window).load(function() {
	$(".loader").fadeOut("fast");
    });

	// Adding a colortip
	$('[data]').colorTip({color:'black'});
	// animate elements
	$('.logo img').animate({ opacity: '1'}, 1000);
	$('.content h2.title').animate({ opacity: '1', marginTop: '0'}, 1500);
	$('.content p.text').animate({ opacity: '1'}, 1500);
	$('form.subscribe').animate({ opacity: '1'}, 1500);
	$('#clock-ticker').animate({ opacity: '1'}, 1500);


});

var current="Message After Time End!";        
var year=<?php echo $cons_timer[0]; ?>;       
var month=<?php echo $cons_timer[1]; ?>;     
var day=<?php echo $cons_timer[2]; ?>;       
var hour=<?php echo $cons_timer[3]; ?>;         
var minute=<?php echo $cons_timer[4]; ?>;       
var tz=<?php echo $cons_timer[5]; ?>;          

var montharray=new Array("Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec");

function countdown(yr,m,d,hr,min){
theyear=yr;themonth=m;theday=d;thehour=hr;theminute=min;
var today=new Date();
var todayy=today.getYear();
if (todayy < 1000) { todayy+=1900; }
var todaym=today.getMonth();
var todayd=today.getDate();
var todayh=today.getHours();
var todaymin=today.getMinutes();
var todaysec=today.getSeconds();
var todaystring1=montharray[todaym]+" "+todayd+", "+todayy+" "+todayh+":"+todaymin+":"+todaysec;
var todaystring=Date.parse(todaystring1)+(tz*1000*60*60);
var futurestring1=(montharray[m-1]+" "+d+", "+yr+" "+hr+":"+min);
var futurestring=Date.parse(futurestring1)-(today.getTimezoneOffset()*(1000*60));
var dd=futurestring-todaystring;
var dday=Math.floor(dd/(60*60*1000*24)*1);
var dhour=Math.floor((dd%(60*60*1000*24))/(60*60*1000)*1);
var dmin=Math.floor(((dd%(60*60*1000*24))%(60*60*1000))/(60*1000)*1);
var dsec=Math.floor((((dd%(60*60*1000*24))%(60*60*1000))%(60*1000))/1000*1);
if(dday<=0&&dhour<=0&&dmin<=0&&dsec<=0){
document.getElementById('count2').innerHTML=current;
document.getElementById('count2').style.display="block";
document.getElementById('dday').style.display="none";
document.getElementById('dhour').style.display="none";
document.getElementById('dmin').style.display="none";
document.getElementById('dsec').style.display="none";
document.getElementById('days').style.display="none";
document.getElementById('hours').style.display="none";
document.getElementById('minutes').style.display="none";
document.getElementById('seconds').style.display="none";
document.getElementById('spacer1').style.display="none";
/*document.getElementById('spacer2').style.display="none";*/
return;
}
else {
document.getElementById('count2').style.display="none";
document.getElementById('dday').innerHTML=dday;
document.getElementById('dhour').innerHTML=dhour;
document.getElementById('dmin').innerHTML=dmin;
document.getElementById('dsec').innerHTML=dsec;
setTimeout("countdown(theyear,themonth,theday,thehour,theminute)",1000);
}
}

countdown(year,month,day,hour,minute);

</script>