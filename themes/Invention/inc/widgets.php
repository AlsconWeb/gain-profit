<?php

// Register Widgets
if (function_exists('register_sidebar')) {

// Default Sidebar Widgets
register_sidebar(array(
  'name' => 'Default Sidebar',
  'id'   => 'default-sidebar',
  'description'   => __("Here default sidebar widgets.", "jozoorthemes"),
  'before_widget' => '<div id="%1$s" class="widget %2$s">',
  'after_widget'  => '</div>',
  'before_title'  => '<h3 class="title bottom-1">',
  'after_title'   => '</h3>'
));

// Sidebar2 Widgets
register_sidebar(array(
  'name' => 'Sidebar2',
  'id'   => 'sidebar2',
  'description'   => __("here sidebar2 widgets.", "jozoorthemes"),
  'before_widget' => '<div id="%1$s" class="widget %2$s">',
  'after_widget'  => '</div>',
  'before_title'  => '<h3 class="title bottom-1">',
  'after_title'   => '</h3>'
));

// Sidebar3 Widgets
register_sidebar(array(
  'name' => 'Sidebar3',
  'id'   => 'sidebar3',
  'description'   => __("here sidebar3 widgets.", "jozoorthemes"),
  'before_widget' => '<div id="%1$s" class="widget %2$s">',
  'after_widget'  => '</div>',
  'before_title'  => '<h3 class="title bottom-1">',
  'after_title'   => '</h3>'
));

// Sidebar4 Widgets
register_sidebar(array(
  'name' => 'Sidebar4',
  'id'   => 'sidebar4',
  'description'   => __("here sidebar4 widgets.", "jozoorthemes"),
  'before_widget' => '<div id="%1$s" class="widget %2$s">',
  'after_widget'  => '</div>',
  'before_title'  => '<h3 class="title bottom-1">',
  'after_title'   => '</h3>'
));

// Sidebar5 Widgets
register_sidebar(array(
  'name' => 'Sidebar5',
  'id'   => 'sidebar5',
  'description'   => __("here sidebar5 widgets.", "jozoorthemes"),
  'before_widget' => '<div id="%1$s" class="widget %2$s">',
  'after_widget'  => '</div>',
  'before_title'  => '<h3 class="title bottom-1">',
  'after_title'   => '</h3>'
));

// Footer1 Widget
register_sidebar(array(
  'name' => 'Footer1',
  'id'   => 'footer1',
  'description'   => __("Here footer1 widgets", "jozoorthemes"),
  'before_widget' => '<div id="%1$s" class="widget %2$s">',
  'after_widget'  => '</div>',
  'before_title'  => '<h3 class="title">',
  'after_title'   => '<span class="line"></span></h3>'
));

// Footer2 Widget
register_sidebar(array(
  'name' => 'Footer2',
  'id'   => 'footer2',
  'description'   => __("Here footer2 widgets", "jozoorthemes"),
  'before_widget' => '<div id="%1$s" class="widget %2$s">',
  'after_widget'  => '</div>',
  'before_title'  => '<h3 class="title">',
  'after_title'   => '<span class="line"></span></h3>'
));

// Footer3 Widget
register_sidebar(array(
  'name' => 'Footer3',
  'id'   => 'footer3',
  'description'   => __("Here footer3 widgets", "jozoorthemes"),
  'before_widget' => '<div id="%1$s" class="widget %2$s">',
  'after_widget'  => '</div>',
  'before_title'  => '<h3 class="title">',
  'after_title'   => '<span class="line"></span></h3>'
));

// Footer4 Widget
register_sidebar(array(
  'name' => 'Footer4',
  'id'   => 'footer4',
  'description'   => __("Here footer4 widgets", "jozoorthemes"),
  'before_widget' => '<div id="%1$s" class="widget %2$s">',
  'after_widget'  => '</div>',
  'before_title'  => '<h3 class="title">',
  'after_title'   => '<span class="line"></span></h3>'
));

}

?>