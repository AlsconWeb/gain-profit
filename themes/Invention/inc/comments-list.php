<?php

function jozoor_theme_comment_list($comment, $args, $depth) {
    
    $GLOBALS['comment'] = $comment;
    extract($args, EXTR_SKIP);

    if ( 'div' == $args['style'] ) {
        $tag = 'div';
        $add_below = 'comment';
    } else {
        $tag = 'li';
        $add_below = 'div-comment';
    }
?>
		<li <?php comment_class('comment-box') ?> id="comment-<?php comment_ID() ?>">
        <div class="comment-author vcard avatar">
            <?php if ($args['avatar_size'] != 0) echo get_avatar( $comment, $size='60',$default='' ); ?>
        </div>
		<div id="div-comment-<?php comment_ID() ?>" class="comment-body comment">
		<div class="comment-author vcard">
		<h5><?php printf('%s', get_comment_author_link()) ?></h5>
		</div>
<?php if ($comment->comment_approved == '0') : ?>
		<p class="comment-awaiting-moderation"><?php _e('Your comment is awaiting moderation.', 'jozoorthemes') ?></p>
		<br />
<?php endif; ?>

		<div class="comment-meta commentmetadata date-replay">
            <a href="<?php echo htmlspecialchars( get_comment_link( $comment->comment_ID ) ) ?>">
			<?php
				printf( __('%1$s - %2$s', 'jozoorthemes'), get_comment_date('M d , Y'),  get_comment_time()) ?>
            </a>
			
             <?php comment_reply_link(array_merge( $args, array('before' => '&nbsp; / &nbsp;', 'add_below' => $add_below, 'depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
		</div>

		<?php comment_text() ?>
            
        <br>
            
        <?php edit_comment_link(__('Edit', 'jozoorthemes'),'  ','' ); ?>

		</div>

<?php } ?>