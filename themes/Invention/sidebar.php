<!-- Start Sidebar Widgets -->
<?php 
  $queried_object = get_queried_object();
  $term_id = $queried_object->term_id;

  if(is_category()) {
    $sidebar = get_term_meta($term_id, 'sidebar', true);
  } else {
    $sidebar = get_post_meta($post->ID, "sidebar", true);
  }
  
  if(empty($sidebar)) {
    $sidebar = 'Default Sidebar';
  }
?>
<aside class="five columns sidebar bottom-3">
   
<?php 
if (function_exists('dynamic_sidebar') && dynamic_sidebar($sidebar)) : else : 
    
// All this stuff in here only shows up if you DON'T have any widgets active in this zone 

?>

     <!-- Search Widget -->
     <div class="widget">
     <?php get_search_form(); ?>
     </div>
     <!-- End -->
        
     <!-- Categories Widget -->
     <div class="widget categories">
       <h3 class="title bottom-1"><?php _e("Categories", "jozoorthemes"); ?></h3>
       <ul class="square-list">
         <?php wp_list_categories('title_li='); ?>
       </ul><!-- End -->
     </div>
     <!-- End -->
        
    <!-- Pages Widget -->
    <div class="widget pages">
       <h3 class="title bottom-1"><?php _e("Pages", "jozoorthemes"); ?></h3>
       <ul class="square-list">
         <?php wp_list_pages('title_li='); ?>
       </ul><!-- End -->
     </div>
    
    <!-- Archives Widget -->    
    <div class="widget pages">
       <h3 class="title bottom-1"><?php _e("Archives", "jozoorthemes"); ?></h3>
       <ul class="square-list">
         <?php wp_get_archives('type=monthly'); ?>
       </ul><!-- End -->
     </div>
        
	
<?php endif; ?>
   
</aside><!-- End Sidebar Widgets -->
   
