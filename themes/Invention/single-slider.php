<?php get_header(); ?>

   <?php 

   while ( have_posts() ) : the_post();

   
   // get slider metaboxs
    $j_slider_type = get_post_meta( $post->ID, '_jozoor_slider_type', true ); // slider type
    $j_slider_height = get_post_meta( $post->ID, '_jozoor_slider_min_height', true ); // slider height
    // slider 1 slides
    $j_slider_one_slides_data = vp_metabox('jozoor_metabox_slider_one._jozoor_slider_one_slides');
    // slider 2 slides
    $j_slider_two_slides_data = vp_metabox('jozoor_metabox_slider_two._jozoor_slider_two_slides');
        
        
    // check slider height
    if(!empty($j_slider_height)) {
      $j_slider_height_style = ' style="min-height:'.$j_slider_height.'px"';  
    } else {
      $j_slider_height_style = '';
    }
        
    
    if($j_slider_type == 'slider2') {
        
    echo '<div class="slider-2 clearfix"><div class="flex-container"><div class="flexslider4 loading">
          <ul class="slides">';
            
   foreach ( $j_slider_two_slides_data as $key => $entry ) {
       
      $j_two_slide_bg_image = vp_metabox('jozoor_metabox_slider_two._jozoor_slider_two_slides.'.$key.'.slide_bg_image'); // bg image
      $j_two_slide_bg_image_repeat = vp_metabox('jozoor_metabox_slider_two._jozoor_slider_two_slides.'.$key.'.slide_bg_image_repeat'); // bg repeat
      $j_two_slide_bg_color = vp_metabox('jozoor_metabox_slider_two._jozoor_slider_two_slides.'.$key.'.slide_bg_color'); // bg color
      $j_two_slide_title = vp_metabox('jozoor_metabox_slider_two._jozoor_slider_two_slides.'.$key.'.slide_title'); // slide title
      $j_two_slide_title_bottom = vp_metabox('jozoor_metabox_slider_two._jozoor_slider_two_slides.'.$key.'.slide_title_bottom'); // title bottom 
      $j_two_slide_title_color = vp_metabox('jozoor_metabox_slider_two._jozoor_slider_two_slides.'.$key.'.slide_title_color'); // title color
      $j_two_slide_title_bg_color = vp_metabox('jozoor_metabox_slider_two._jozoor_slider_two_slides.'.$key.'.slide_title_bg_color'); // title bg color
      $j_two_slide_desc = vp_metabox('jozoor_metabox_slider_two._jozoor_slider_two_slides.'.$key.'.slide_desc');  // slide description
      $j_two_slide_desc_bottom = vp_metabox('jozoor_metabox_slider_two._jozoor_slider_two_slides.'.$key.'.slide_desc_bottom'); // description bottom
      $j_two_slide_desc_color = vp_metabox('jozoor_metabox_slider_two._jozoor_slider_two_slides.'.$key.'.slide_desc_color'); // description color
      $j_two_slide_buttons = vp_metabox('jozoor_metabox_slider_two._jozoor_slider_two_slides.'.$key.'.slide_buttons'); // slide buttons   
      $j_two_slide_buttons_bottom = vp_metabox('jozoor_metabox_slider_two._jozoor_slider_two_slides.'.$key.'.slide_buttons_bottom'); // buttons bottom
      $j_two_slide_image = vp_metabox('jozoor_metabox_slider_two._jozoor_slider_two_slides.'.$key.'.slide_image');  // slide image
      $j_two_slide_video = vp_metabox('jozoor_metabox_slider_two._jozoor_slider_two_slides.'.$key.'.slide_video'); // slide video
      $j_two_slide_image_video_bottom = vp_metabox('jozoor_metabox_slider_two._jozoor_slider_two_slides.'.$key.'.slide_image_video_bottom'); // image/video bottom
      $j_two_slide_global_link = vp_metabox('jozoor_metabox_slider_two._jozoor_slider_two_slides.'.$key.'.slide_global_link'); // global link
        
      // check bg image
      if(!empty($j_two_slide_bg_image)) {
          
        if(!empty($j_two_slide_bg_image_repeat)) { $j_bg_image_repeat = $j_two_slide_bg_image_repeat; } else { $j_bg_image_repeat = 'no-repeat'; }
        if($j_bg_image_repeat == 'repeat') {
        $j_two_slide_bg_image_content = 'background:url('.$j_two_slide_bg_image.') '.$j_bg_image_repeat.';';
        } else {
        $j_two_slide_bg_image_content = 'background:url('.$j_two_slide_bg_image.') '.$j_bg_image_repeat.';background-position:center;background-size: cover;-moz-background-size: cover;
        -webkit-background-size: cover;-o-background-size: cover;';  
        }
          
      } else { $j_two_slide_bg_image_content = ''; }
        
      // check bg color
      if(!empty($j_two_slide_bg_color)) {
          
        $j_two_slide_bg_color_content = 'background-color:'.$j_two_slide_bg_color.';';  $j_two_slide_bg_color_class = '';
          
      } else { $j_two_slide_bg_color_content = ''; $j_two_slide_bg_color_class = 'class="slide-bg-color"'; }
        
      // check title
      if(!empty($j_two_slide_title)) {
          
        if(!empty($j_two_slide_title_bottom)) { $j_title_bottom = $j_two_slide_title_bottom; } else { $j_title_bottom = '45'; }
        if(!empty($j_two_slide_title_color)) { $j_title_color = ' style="color:'.$j_two_slide_title_color.'"'; } else { $j_title_color = ''; }
        if(!empty($j_two_slide_global_link)) { 
            $j_global_link_start = '<a href="'.$j_two_slide_global_link.'"'.$j_title_color.'>'; 
            $j_global_link_end = '</a>'; 
        } else { $j_global_link_start = ''; $j_global_link_end = ''; }
        $j_two_slide_title_content = '<h2 data-bottomtitle="'.$j_title_bottom.'%"><span'.$j_title_color.'>'.$j_global_link_start.''.$j_two_slide_title.''.$j_global_link_end.'</span></h2>';
          
      } else { $j_two_slide_title_content = ''; } 
       
      // check desc
      if(!empty($j_two_slide_desc)) {
          
        if(!empty($j_two_slide_desc_bottom)) { $j_desc_bottom = $j_two_slide_desc_bottom; } else { $j_desc_bottom = '33'; }
        if(!empty($j_two_slide_desc_color)) { $j_desc_color = ' style="color:'.$j_two_slide_desc_color.'"'; } else { $j_desc_color = ''; }
        if(!empty($j_two_slide_global_link)) { 
            $j_global_link_start = '<a href="'.$j_two_slide_global_link.'"'.$j_desc_color.'>'; 
            $j_global_link_end = '</a>'; 
        } else { $j_global_link_start = ''; $j_global_link_end = ''; }
        $j_two_slide_desc_content = '<div class="slide-desc" data-bottomtext="'.$j_desc_bottom.'%"><span'.$j_desc_color.'>'.$j_global_link_start.''.$j_two_slide_desc.''.$j_global_link_end.'</span></div>';
          
      } else { $j_two_slide_desc_content = ''; }
       
      // check buttons textarea
      if(!empty($j_two_slide_buttons)) {
          
        if(!empty($j_two_slide_buttons_bottom)) { $j_buttons_bottom = $j_two_slide_buttons_bottom; } else { $j_buttons_bottom = '18'; }
        $j_two_slide_buttons_content = '<div class="links" data-bottomlinks="'.$j_buttons_bottom.'%"><span>'.do_shortcode(wpautop($j_two_slide_buttons)).'</span></div>';
          
      } else { $j_two_slide_buttons_content = ''; }
       
      // check image/video
      if(!empty($j_two_slide_video)) {
        if(!empty($j_two_slide_image_video_bottom)) { $j_image_video_bottom = $j_two_slide_image_video_bottom; } else { $j_image_video_bottom = '0'; }
        $j_two_slide_image_video_content = '<div class="item" data-bottomimage="'.$j_image_video_bottom.'%"><div class="video-wrap"><center>'.wp_oembed_get($j_two_slide_video).'</center></div></div>';
      } 
      elseif(!empty($j_two_slide_image)) {
        if(!empty($j_two_slide_image_video_bottom)) { $j_image_video_bottom = $j_two_slide_image_video_bottom; } else { $j_image_video_bottom = '0'; }
        if(!empty($j_two_slide_global_link)) { 
            $j_global_link_start = '<a href="'.$j_two_slide_global_link.'">'; 
            $j_global_link_end = '</a>'; 
        } else { $j_global_link_start = ''; $j_global_link_end = ''; }
        $j_two_slide_image_video_content = '<div class="item" data-bottomimage="'.$j_image_video_bottom.'%"><center>'.$j_global_link_start.'<img src="'.$j_two_slide_image.'" alt="'.$j_two_slide_title.'">'.$j_global_link_end.'</center></div>';  
      }
      else { $j_two_slide_image_video_content = ''; }
       
      // check if all fields empty
      if( empty($j_two_slide_title) && empty($j_two_slide_desc) && empty($j_two_slide_buttons) ) {
         $j_item_class = ' image-video-item';   
      } else {
          $j_item_class = '';
      }
       
    ?>

    <li <?php echo $j_two_slide_bg_color_class; ?> style="<?php echo $j_two_slide_bg_color_content.$j_two_slide_bg_image_content; ?>">
          
     <div class="container">
     <div class="sixteen columns contain<?php echo $j_item_class; ?>"<?php echo $j_slider_height_style; ?>>
          
     <?php
     echo $j_two_slide_title_content.$j_two_slide_desc_content.$j_two_slide_buttons_content.$j_two_slide_image_video_content;
     ?>
           
     </div>
     </div>
          
   </li>

    <?php
    } // end foreach 
        
    echo '</ul>
          </div></div></div><!-- End slider -->';
        
    } // end slider 2
        
    else {
        
    echo '<div class="slider-1 clearfix"><div class="flex-container"><div class="flexslider loading">
          <ul class="slides">';
    
   
    foreach ( $j_slider_one_slides_data as $key => $entry ) {
       
      $j_one_slide_bg_image = vp_metabox('jozoor_metabox_slider_one._jozoor_slider_one_slides.'.$key.'.slide_bg_image'); // bg image
      $j_one_slide_bg_image_repeat = vp_metabox('jozoor_metabox_slider_one._jozoor_slider_one_slides.'.$key.'.slide_bg_image_repeat'); // bg repeat
      $j_one_slide_bg_color = vp_metabox('jozoor_metabox_slider_one._jozoor_slider_one_slides.'.$key.'.slide_bg_color'); // bg color
      $j_one_slide_title = vp_metabox('jozoor_metabox_slider_one._jozoor_slider_one_slides.'.$key.'.slide_title'); // slide title
      $j_one_slide_title_top = vp_metabox('jozoor_metabox_slider_one._jozoor_slider_one_slides.'.$key.'.slide_title_top'); // title top 
      $j_one_slide_desc = vp_metabox('jozoor_metabox_slider_one._jozoor_slider_one_slides.'.$key.'.slide_desc');  // slide description
      $j_one_slide_desc_bottom = vp_metabox('jozoor_metabox_slider_one._jozoor_slider_one_slides.'.$key.'.slide_desc_bottom'); // description bottom
      $j_one_slide_title_desc_color = vp_metabox('jozoor_metabox_slider_one._jozoor_slider_one_slides.'.$key.'.slide_title_desc_color'); // title/desc color
      $j_one_slide_buttons = vp_metabox('jozoor_metabox_slider_one._jozoor_slider_one_slides.'.$key.'.slide_buttons'); // slide buttons   
      $j_one_slide_buttons_bottom = vp_metabox('jozoor_metabox_slider_one._jozoor_slider_one_slides.'.$key.'.slide_buttons_bottom'); // buttons bottom
      $j_one_slide_image = vp_metabox('jozoor_metabox_slider_one._jozoor_slider_one_slides.'.$key.'.slide_image');  // slide image
      $j_one_slide_video = vp_metabox('jozoor_metabox_slider_one._jozoor_slider_one_slides.'.$key.'.slide_video'); // slide video
      $j_one_slide_image_video_top = vp_metabox('jozoor_metabox_slider_one._jozoor_slider_one_slides.'.$key.'.slide_image_video_top'); // image/video top
      $j_one_slide_global_link = vp_metabox('jozoor_metabox_slider_one._jozoor_slider_one_slides.'.$key.'.slide_global_link'); // global link
        
      // check bg image
      if(!empty($j_one_slide_bg_image)) {
          
        if(!empty($j_one_slide_bg_image_repeat)) { $j_bg_image_repeat = $j_one_slide_bg_image_repeat; } else { $j_bg_image_repeat = 'no-repeat'; }
        if($j_bg_image_repeat == 'repeat') {
        $j_one_slide_bg_image_content = 'background:url('.$j_one_slide_bg_image.') '.$j_bg_image_repeat.';';
        } else {
        $j_one_slide_bg_image_content = 'background:url('.$j_one_slide_bg_image.') '.$j_bg_image_repeat.';background-position:center;background-size: cover;-moz-background-size: cover;
        -webkit-background-size: cover;-o-background-size: cover;';  
        }
          
      } else { $j_one_slide_bg_image_content = ''; }
        
      // check bg color
      if(!empty($j_one_slide_bg_color)) {
          
        $j_one_slide_bg_color_content = 'background-color:'.$j_one_slide_bg_color.';';  $j_one_slide_bg_color_class = '';
          
      } else { $j_one_slide_bg_color_content = ''; $j_one_slide_bg_color_class = 'class="slide-bg-color"'; }
        
      // check title
      if(!empty($j_one_slide_title)) {
          
        if(!empty($j_one_slide_title_top)) { $j_title_top = $j_one_slide_title_top; } else { $j_title_top = '20'; }
        if(!empty($j_one_slide_title_desc_color)) { $j_title_desc_color = ' style="color:'.$j_one_slide_title_desc_color.'"'; } else { $j_title_desc_color = ''; }
        if(!empty($j_one_slide_global_link)) { 
            $j_global_link_start = '<a href="'.$j_one_slide_global_link.'"'.$j_title_desc_color.'>'; 
            $j_global_link_end = '</a>'; 
        } else { $j_global_link_start = ''; $j_global_link_end = ''; }
        $j_one_slide_title_content = '<h2 data-toptitle="'.$j_title_top.'%"'.$j_title_desc_color.'>'.$j_global_link_start.''.$j_one_slide_title.''.$j_global_link_end.'</h2>';
          
      } else { $j_one_slide_title_content = ''; }
        
      // check desc
      if(!empty($j_one_slide_desc)) {
          
        if(!empty($j_one_slide_desc_bottom)) { $j_desc_bottom = $j_one_slide_desc_bottom; } else { $j_desc_bottom = '39'; }
        if(!empty($j_one_slide_title_desc_color)) { $j_title_desc_color = ' style="color:'.$j_one_slide_title_desc_color.'"'; } else { $j_title_desc_color = ''; }
        if(!empty($j_one_slide_global_link)) { 
            $j_global_link_start = '<a href="'.$j_one_slide_global_link.'"'.$j_title_desc_color.'>'; 
            $j_global_link_end = '</a>'; 
        } else { $j_global_link_start = ''; $j_global_link_end = ''; }
        $j_one_slide_desc_content = '<div class="slide-desc" data-bottomtext="'.$j_desc_bottom.'%"'.$j_title_desc_color.'>'.$j_global_link_start.''.$j_one_slide_desc.''.$j_global_link_end.'</div>';
          
      } else { $j_one_slide_desc_content = ''; }
        
      // check buttons textarea
      if(!empty($j_one_slide_buttons)) {
          
        if(!empty($j_one_slide_buttons_bottom)) { $j_buttons_bottom = $j_one_slide_buttons_bottom; } else { $j_buttons_bottom = '20'; }
        $j_one_slide_buttons_content = '<div class="links" data-bottomlinks="'.$j_buttons_bottom.'%">'.do_shortcode(wpautop($j_one_slide_buttons)).'</div>';
          
      } else { $j_one_slide_buttons_content = ''; }
        
      // check image/video
      if(!empty($j_one_slide_video)) {
        if(!empty($j_one_slide_image_video_top)) { $j_image_video_top = $j_one_slide_image_video_top; } else { $j_image_video_top = '16'; }
        $j_one_slide_image_video_content = '<div class="item" data-topimage="'.$j_image_video_top.'%"><div class="video-wrap">'.wp_oembed_get($j_one_slide_video).'</div></div>';
      } 
      elseif(!empty($j_one_slide_image)) {
        if(!empty($j_one_slide_image_video_top)) { $j_image_video_top = $j_one_slide_image_video_top; } else { $j_image_video_top = '16'; }
        if(!empty($j_one_slide_global_link)) { 
            $j_global_link_start = '<a href="'.$j_one_slide_global_link.'">'; 
            $j_global_link_end = '</a>'; 
        } else { $j_global_link_start = ''; $j_global_link_end = ''; }
        $j_one_slide_image_video_content = '<div class="item" data-topimage="'.$j_image_video_top.'%">'.$j_global_link_start.'<img src="'.$j_one_slide_image.'" alt="'.$j_one_slide_title.'">'.$j_global_link_end.'</div>';  
      }
      else { $j_one_slide_image_video_content = ''; }
       
    ?>

    <li <?php echo $j_one_slide_bg_color_class; ?> style="<?php echo $j_one_slide_bg_color_content.$j_one_slide_bg_image_content; ?>">
          
     <div class="container">
     <div class="sixteen columns contain"<?php echo $j_slider_height_style; ?>>
          
     <?php
     echo $j_one_slide_title_content.$j_one_slide_desc_content.$j_one_slide_buttons_content.$j_one_slide_image_video_content;
     ?>
           
     </div>
     </div>
          
   </li>

    <?php
    } // end foreach
    

    echo '</ul>
          </div></div></div><!-- End slider -->';

    } // end slider 1
    


   endwhile; 

   ?>

   <!-- Start main content -->
   <div class="container main-content clearfix">
       
   <div class="sixteen columns full-width top-3">
       
      <div class="entry-content post-content bottom-3">
         <div class="alert info"><i class="icon-info-sign"></i>
          <p><?php _e("This page is just for slider preview before define it to any page", "jozoorthemes"); ?>.</p>
         </div> 
      </div>
       
   </div>
       
<?php get_footer(); ?>