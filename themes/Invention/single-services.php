<?php get_header(); ?>

   <?php
   // get menu item id
   global $wpdb;
   $post_id = $wpdb->get_var( 'SELECT ID FROM '.$wpdb->posts.' WHERE post_name="'.$jozoor_single_service_post_current_page.'" AND post_type="page" AND post_status="publish"' );
   $post_title = $wpdb->get_var( 'SELECT post_id FROM '.$wpdb->postmeta.' WHERE meta_key="_menu_item_object_id" AND meta_value="'.$post_id.'"' );
   
   // add active class for nav item
   if (!is_admin() && $jozoor_single_service_post_current_page != 'Select Page Name') { ?>
   <script>
    jQuery(document).ready(function($) {
     $("#menu-item-<?php echo $post_title;  ?>").addClass("current-menu-item");
    });
    </script>
   <?php } ?>

   <!-- Start main content -->
   <div class="container main-content clearfix">
       
   <?php if( $jozoor_single_service_layout_page_style == 'Left Sidebar' ) { 
   if( $jozoor_single_service_sidebar_type == 'Default Sidebar' ) { get_sidebar(); } else {
   echo '<!-- Start Sidebar Widgets --><aside class="five columns sidebar bottom-3">';
   if (function_exists('dynamic_sidebar') && dynamic_sidebar($jozoor_single_service_sidebar_type)) : endif;
   echo '</aside><!-- End Sidebar Widgets -->';
   }  
   } ?>
       
   <!-- Start Posts -->
   <?php if( $jozoor_single_service_layout_page_style == 'Right Sidebar' || $jozoor_single_service_layout_page_style == 'Left Sidebar' ) { ?>
   <div class="eleven columns bottom-3 single-post">
   <?php } else { ?>
   <div class="sixteen columns full-width bottom-3 single-post">    
   <?php } ?> 
       
       
   <?php 

   while ( have_posts() ) : the_post();

   // check post protected 
   if ( !post_password_required() ) { 
      if ( has_post_thumbnail() ) {
        echo '<div class="image-post">';
        echo get_the_post_thumbnail($post->ID, 'full', array('title' => ''));
        echo '</div><!-- End image-post -->';
      } 
   }

   echo '<div class="entry-content post-content bottom-3">';
     the_content();
   echo '</div><!-- End page-content -->';

   // comments 
   if( $jozoor_single_comments_service == 1 ) {
   if ( comments_open() || get_comments_number() ) {
       comments_template();
    }
   }

   endwhile; 

   ?>
       
   </div><!-- End service -->
       
<?php if( $jozoor_single_service_layout_page_style == 'Right Sidebar' ) { 
   if( $jozoor_single_service_sidebar_type == 'Default Sidebar' ) { get_sidebar(); } else {
   echo '<!-- Start Sidebar Widgets --><aside class="five columns sidebar bottom-3">';
   if (function_exists('dynamic_sidebar') && dynamic_sidebar($jozoor_single_service_sidebar_type)) : endif;
   echo '</aside><!-- End Sidebar Widgets -->';
   }  
} ?>
       
<?php get_footer(); ?>