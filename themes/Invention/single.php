<?php get_header(); ?>

   <?php
   // get menu item id
   global $wpdb;
   $post_id = $wpdb->get_var( 'SELECT ID FROM '.$wpdb->posts.' WHERE post_name="'.$jozoor_single_post_current_page.'" AND post_type="page" AND post_status="publish"' );
   $post_title = $wpdb->get_var( 'SELECT post_id FROM '.$wpdb->postmeta.' WHERE meta_key="_menu_item_object_id" AND meta_value="'.$post_id.'"' );

   // add active class for nav item
   if (!is_admin() && $jozoor_single_post_current_page != 'Select Page Name') { ?>
   <script>
    jQuery(document).ready(function($) {
     $("#menu-item-<?php echo $post_title;  ?>").addClass("current-menu-item");
    });
    </script>
   <?php } ?>

   <!-- Start main content -->
   <div class="container main-content clearfix">

   <?php if( $jozoor_single_layout_page_style == 'Left Sidebar' ) {
   if( $jozoor_single_sidebar_type == 'Default Sidebar' ) { get_sidebar(); } else {
   echo '<!-- Start Sidebar Widgets --><aside class="five columns sidebar bottom-3">';
   if (function_exists('dynamic_sidebar') && dynamic_sidebar($jozoor_single_sidebar_type)) : endif;
   echo '</aside><!-- End Sidebar Widgets -->';
   }
   } ?>

   <!-- Start Posts -->
   <?php if( $jozoor_single_layout_page_style == 'Right Sidebar' || $jozoor_single_layout_page_style == 'Left Sidebar' ) { ?>
   <div class="eleven columns bottom-3 single-post">
   <?php } else { ?>
   <div class="sixteen columns full-width bottom-3 single-post">
   <?php } ?>

   <?php

   while ( have_posts() ) : the_post();

   // set post views
   jozoor_setPostViews(get_the_ID());

   // top post area
   if($jozoor_single_custom_entry == 1 && !empty($jozoor_custom_content_top_entry)) {
      echo '<div class="custom-area top">'.$jozoor_custom_content_top_entry.'</div>';
   }

   // get post format
   get_template_part( 'inc/post_formats/content', get_post_format() );

   // tags
   if($jozoor_single_post_tags == 1 || $jozoor_single_post_tags != 0) {
   the_tags( '<div class="post-tags bottom-2"><i class="icon-tags"></i> : &nbsp; ', ', ', '</div><!-- End Tags -->' );
   }

   // down post area
   if($jozoor_single_custom_entry == 1 && !empty($jozoor_custom_content_down_entry)) {
      echo '<div class="custom-area down">'.$jozoor_custom_content_down_entry.'</div>';
   }

   // previous & next posts
   if($jozoor_single_prev_next_links == 1 || $jozoor_single_prev_next_links != 0) {
   $jozoor_prev_post = get_previous_post();
   $jozoor_next_post = get_next_post();

   if( !empty($jozoor_prev_post) || !empty($jozoor_next_post) ) { echo '<div class="next-prev-posts bottom-3">'; }
   previous_post_link('<span class="prev-post"><i class="icon-double-angle-left"></i> %link</span>', ''.__("Previous Post", "jozoorthemes").'' , 'no');
   next_post_link('<span class="next-post">%link <i class="icon-double-angle-right"></i></span>', ''.__("Next Post", "jozoorthemes").'' , 'no');
   if( !empty($jozoor_prev_post) || !empty($jozoor_next_post) ) { echo '</div><!-- End next-prev post -->'; }
   }

   // about author
   if($jozoor_single_about_author == 1 || $jozoor_single_about_author != 0) {
   ?>
   <div class="about-author bottom-3">
      <h3 class="title bottom-1"><?php _e("About The Author", "jozoorthemes"); ?></h3><!-- Title Post -->
      <div class="content">
        <div class="avatar"><?php if (function_exists('get_avatar')) { echo get_avatar( get_the_author_meta('email'), '80' ); }?></div>
        <div class="data">
          <h5><?php the_author_posts_link(); ?> </h5>
          <p><?php the_author_meta('description'); ?></p>

          <?php
          $j_user_website = get_the_author_meta('user_url');
          // get custom fields for user
          $j_user_twitter_url = get_the_author_meta('_jozoor_user_twitter_url');        $j_user_facebook_url = get_the_author_meta('_jozoor_user_facebook_url');
          $j_user_googleplus_url = get_the_author_meta('_jozoor_user_googleplus_url');  $j_user_linkedin_url = get_the_author_meta('_jozoor_user_linkedin_url');
          $j_user_dribbble_url = get_the_author_meta('_jozoor_user_dribbble_url');      $j_user_behance_url = get_the_author_meta('_jozoor_user_behance_url');
          $j_user_instagram_url = get_the_author_meta('_jozoor_user_instagram_url');    $j_user_youtube_url = get_the_author_meta('_jozoor_user_youtube_url');
          $j_user_pinterest_url = get_the_author_meta('_jozoor_user_pinterest_url');    $j_user_flickr_url = get_the_author_meta('_jozoor_user_flickr_url');
          $j_user_vimeo_url = get_the_author_meta('_jozoor_user_vimeo_url');            $j_user_tumblr_url = get_the_author_meta('_jozoor_user_tumblr_url');
          ?>

          <div class="member-social author-links">
            <?php if( !empty($j_user_website) ) { ?><a href="<?php echo $j_user_website; ?>" target="_blank" data="Website"><i class="icon-home s-18"></i></a><?php } ?>
            <?php if( !empty($j_user_twitter_url) ) { ?><a href="<?php echo $j_user_twitter_url; ?>" target="_blank" data="Twitter"><i class="social_icon-twitter s-15"></i></a><?php } ?>
            <?php if( !empty($j_user_facebook_url) ) { ?><a href="<?php echo $j_user_facebook_url; ?>" target="_blank" data="Facebook"><i class="social_icon-facebook s-16"></i></a><?php } ?>
            <?php if( !empty($j_user_googleplus_url) ) { ?><a href="<?php echo $j_user_googleplus_url; ?>" target="_blank" data="Google+"><i class="social_icon-gplus s-16"></i></a><?php } ?>
            <?php if( !empty($j_user_linkedin_url) ) { ?><a href="<?php echo $j_user_linkedin_url; ?>" target="_blank" data="Linkedin"><i class="social_icon-linkedin s-15"></i></a><?php } ?>
            <?php if( !empty($j_user_dribbble_url) ) { ?><a href="<?php echo $j_user_dribbble_url; ?>" target="_blank" data="Dribbble"><i class="social_icon-dribbble s-16"></i></a><?php } ?>
            <?php if( !empty($j_user_behance_url) ) { ?><a href="<?php echo $j_user_behance_url; ?>" target="_blank" data="Behance"><i class="social_icon-behance s-16"></i></a><?php } ?>
            <?php if( !empty($j_user_instagram_url) ) { ?><a href="<?php echo $j_user_instagram_url; ?>" target="_blank" data="Instagram"><i class="social_icon-instagram s-16"></i></a><?php } ?>
            <?php if( !empty($j_user_youtube_url) ) { ?><a href="<?php echo $j_user_youtube_url; ?>" target="_blank" data="Youtube"><i class="social_icon-youtube s-16"></i></a><?php } ?>
            <?php if( !empty($j_user_pinterest_url) ) { ?><a href="<?php echo $j_user_pinterest_url; ?>" target="_blank" data="Pinterest"><i class="social_icon-pinterest s-16"></i></a><?php } ?>
            <?php if( !empty($j_user_flickr_url) ) { ?><a href="<?php echo $j_user_flickr_url; ?>" target="_blank" data="Flickr"><i class="social_icon-flickr s-16"></i></a><?php } ?>
            <?php if( !empty($j_user_vimeo_url) ) { ?><a href="<?php echo $j_user_vimeo_url; ?>" target="_blank" data="Vimeo"><i class="social_icon-vimeo s-15"></i></a><?php } ?>
            <?php if( !empty($j_user_tumblr_url) ) { ?><a href="<?php echo $j_user_tumblr_url; ?>" target="_blank" data="Tumblr"><i class="social_icon-tumblr s-16"></i></a><?php } ?>
          </div><!-- /author-links -->

        </div><!-- End data -->
      </div><!-- End content -->
   </div><!-- End about author -->

   <?php
   }

   /*// related posts
   if($jozoor_single_related_posts == 1 || $jozoor_single_related_posts != 0) {
    $orig_post = $post;
	$tags = wp_get_post_tags($post->ID);

	if ($tags) {
	$tag_ids = array();
	foreach($tags as $individual_tag) $tag_ids[] = $individual_tag->term_id;
	$args=array(
	'tag__in' => $tag_ids,
	'post__not_in' => array($post->ID),
    'post_type' => 'post',
	'posts_per_page'=> $jozoor_single_related_posts_num, // Number of related posts
    'orderby' => 'rand',
	'ignore_sticky_posts'=>1
	);

	$my_query = new wp_query( $args );
    if( $my_query->have_posts() ) {

    echo '<div class="related-posts bottom-3">
      <h3 class="title bottom-1">'.__("Related Posts", "jozoorthemes").'</h3>';

	while( $my_query->have_posts() ) { $my_query->the_post();
     if ( !post_password_required($post->ID) ) {
    ?>
          <div class="item">
             <?php if (has_post_thumbnail( $post->ID ) ) {?>
              <a href="<?php echo get_permalink(); ?>"><?php echo get_the_post_thumbnail($post->ID, 'related-posts-thumb', array('title' => '')); ?></a>
              <?php } ?>
              <h5><a href="<?php echo get_permalink(); ?>"><?php the_title(); ?></a></h5>
              <div class="meta"><i class="icon-time"></i> <?php the_time('d M, Y'); ?> </div><!-- Date -->
          </div>

    <?php  } }

    echo '</div><!-- End related-posts -->';

	}

    }
	$post = $orig_post;
	wp_reset_query();
   }*/

   // comments
   if ( comments_open() || get_comments_number() ) {
       comments_template();
    }

   endwhile;

   ?>

   </div><!-- End Posts -->

<?php if( $jozoor_single_layout_page_style == 'Right Sidebar' ) {
   if( $jozoor_single_sidebar_type == 'Default Sidebar' ) {
      $sidebar = get_post_meta($post->ID, "sidebar", true);
      get_sidebar($sidebar);
   } else {
      echo '<!-- Start Sidebar Widgets --><aside class="five columns sidebar bottom-3">';
   if (function_exists('dynamic_sidebar') && dynamic_sidebar($jozoor_single_sidebar_type)) : endif;
      echo '</aside><!-- End Sidebar Widgets -->';
   }
} ?>

<?php get_footer(); ?>