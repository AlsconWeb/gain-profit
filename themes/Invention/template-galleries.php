<?php
/*
Template Name: Galleries
*/
?>

<?php get_header(); ?>

   <?php // get metaboxes
   global $post;
   $j_galleries_layout = get_post_meta( $post->ID, '_jozoor_galleries_layout', true ); 

   // check column
   if( $j_galleries_layout == 'col2' ) { $j_galleries_layout_class = 'eight columns col2'; }
   elseif ( $j_galleries_layout == 'col4' ) { $j_galleries_layout_class = 'four columns col4'; }
   else { $j_galleries_layout_class = 'one-third column col3'; }

   ?>

   <!-- Start main content -->
   <div class="container main-content clearfix">
       
   <!-- Start Page Content -->       
   <div class="sixteen columns full-width"> 
   <?php 

   // page loop
   while ( have_posts() ) : the_post();

   // check post protected 
   if ( !post_password_required() ) { 
      
      if ( has_post_thumbnail() ) {
        echo '<div class="image-post">';
        echo get_the_post_thumbnail($post->ID, 'full', array('title' => ''));
        echo '</div><!-- End image-post -->';
      } 
   }
   
   if( !empty( $post->post_content) ) {
   echo '<div class="entry-content post-content bottom-3">';
     the_content();
   echo '</div><!-- End page-content -->';
   }

   endwhile; 

   // Reset Query
   wp_reset_query();

   ?>  
   </div><!-- End -->
       
   <div class="clearfix"></div> 
       
   <div class="portfolio galleries bottom-3">
   <?php
   $query_string;
   parse_str( $query_string, $my_query_array );
   $paged = ( isset( $my_query_array['paged'] ) && !empty( $my_query_array['paged'] ) ) ? $my_query_array['paged'] : 1;

   // get projects per page
   if($jozoor_custom_galleries_per_page == 1) {
     if($j_galleries_layout == 'col2') { 
       $j_galleries_per_page = $jozoor_col2_galleries_per_page;
     } elseif ($j_galleries_layout == 'col3') { 
       $j_galleries_per_page = $jozoor_col3_galleries_per_page;
     } elseif ($j_galleries_layout == 'col4') { 
       $j_galleries_per_page = $jozoor_col4_galleries_per_page;
     } else {
       $j_galleries_per_page = '';
     }
   } else {
       $j_galleries_per_page = '';
   }

   ?>
       
   <div id="contain" class="clearfix"> 
       
   <?php
   // projects loop
   query_posts('post_type=gallery&paged='.$paged.'&posts_per_page='.$j_galleries_per_page.'');
   if(have_posts()) : while(have_posts()) : the_post(); 

   // get number of images
   $j_gallery_images = get_post_meta( $post->ID, '_jozoor_gallery_images', true );
   if(!empty($j_gallery_images)) {
   $j_gallery_images_num = ' &nbsp;&nbsp; <i class="icon-picture"></i> ' . count($j_gallery_images) . ' '.__("Photos", "jozoorthemes");
   } else {
   $j_gallery_images_num = '';
   }
   ?>
       
   <div id="post-<?php the_ID(); ?>" class="<?php $allClasses = get_post_class(); foreach ($allClasses as $class) { echo $class . ' '; } ?><?php echo $j_galleries_layout_class; ?> item">
    <a href="<?php echo get_permalink(); ?>">
    <?php echo get_the_post_thumbnail($post->ID, 'portfolio-thumb', array('title' => '')); ?>
    <div class="img-caption">
    <div class="desc">
      <h3><?php the_title(); ?></h3>
      <p><i class="icon-time"></i> <?php the_time('d M, Y'); ?><?php echo $j_gallery_images_num; ?></p>
      <span>+</span></div>
    </div><!-- hover effect -->
    </a>
   </div>
   
   <?php
   endwhile; 

   echo '</div><!-- End contain -->';

   echo '<div class="sixteen columns">';
   jozoor_pagination(); // Pagination
   echo '</div>';

   endif;

   // Reset Query
   wp_reset_query();
   ?>
   </div><!-- End Galleries -->
       
<?php get_footer(); ?>