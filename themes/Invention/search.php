<?php get_header(); ?>

   <?php
   // get menu item id
   global $wpdb;
   $post_id = $wpdb->get_var( 'SELECT ID FROM '.$wpdb->posts.' WHERE post_name="'.$jozoor_search_page_current_page.'" AND post_type="page" AND post_status="publish"' );
   $post_title = $wpdb->get_var( 'SELECT post_id FROM '.$wpdb->postmeta.' WHERE meta_key="_menu_item_object_id" AND meta_value="'.$post_id.'"' );
   
   // add active class for nav item
   if (!is_admin() && $jozoor_search_page_current_page != 'Select Page Name') { ?>
   <script>
    jQuery(document).ready(function($) {
     $("#menu-item-<?php echo $post_title;  ?>").addClass("current-menu-item");
    });
    </script>
   <?php } ?>

   <?php
   if ( !have_posts() ) { 
   echo '<div class="page-title"><div class="container clearfix"><div class="sixteen columns">'; ?>
   <h1><?php echo __("Results for", "jozoorthemes"); ?> : <?php the_search_query(); ?></h1>
   <?php
   echo '</div></div><!-- End Container --></div><!-- End Page title -->';  
   }
   ?>

   <!-- Start main content -->
   <div class="container main-content clearfix">
       
   <?php if( $jozoor_search_page_layout_style == 'Left Sidebar' ) { 
   if( $jozoor_search_page_sidebar_type == 'Default Sidebar' ) { get_sidebar(); } else {
   echo '<!-- Start Sidebar Widgets --><aside class="five columns sidebar bottom-3">';
   if (function_exists('dynamic_sidebar') && dynamic_sidebar($jozoor_search_page_sidebar_type)) : endif;
   echo '</aside><!-- End Sidebar Widgets -->';
   }  
   } ?>
   
   <!-- Start Posts -->
   <?php if( $jozoor_search_page_layout_style == 'Right Sidebar' || $jozoor_search_page_layout_style == 'Left Sidebar' ) { ?>
   <div class="eleven columns bottom-3">
   <?php } else { ?>
   <div class="sixteen columns full-width bottom-3">    
   <?php } ?>
       
   <?php 
   if(have_posts()) : while(have_posts()) : the_post(); 
   ?>
   
   <article id="post-<?php the_ID(); ?>" <?php post_class('post style-1 bottom-2'); ?>>
   <div class="post-wrap">
    
    <?php

	the_title( '<h3 class="entry-title title bottom-1"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h3><!-- Title Post -->' );

    if( !empty( $post->post_content) ) {
    echo '<div class="entry-content post-content">';
    the_excerpt(); 
    echo '</div><!-- End post-content -->';
    }

    echo '</div></article><!-- End Post -->';

	endwhile; 
    
    jozoor_pagination(); // Pagination

    else: 

    echo '<p class="nothing-found">'.__("Sorry, but nothing matched your search terms. Please try again with some different keywords.", "jozoorthemes").'</p>';

    endif; 
			  
    ?>
       
    </div><!-- End Posts -->
       
<?php if( $jozoor_search_page_layout_style == 'Right Sidebar' ) { 
   if( $jozoor_search_page_sidebar_type == 'Default Sidebar' ) { get_sidebar(); } else {
   echo '<!-- Start Sidebar Widgets --><aside class="five columns sidebar bottom-3">';
   if (function_exists('dynamic_sidebar') && dynamic_sidebar($jozoor_search_page_sidebar_type)) : endif;
   echo '</aside><!-- End Sidebar Widgets -->';
   }  
} ?>
    
<?php get_footer(); ?>   