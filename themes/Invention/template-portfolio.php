<?php
/*
Template Name: Portfolio
*/
?>

<?php get_header(); ?>

   <?php // get metaboxes
   global $post;
   $j_portfolio_layout = get_post_meta( $post->ID, '_jozoor_portfolio_layout', true ); 
   $j_portfolio_show_cats_filter = get_post_meta( $post->ID, '_jozoor_portfolio_show_cats_filter', true );

   // check column
   if( $j_portfolio_layout == 'col2' ) { $j_portfolio_layout_class = 'eight columns col2'; }
   elseif ( $j_portfolio_layout == 'col3' ) { $j_portfolio_layout_class = 'one-third column col3'; }
   else { $j_portfolio_layout_class = 'four columns col4'; }
   ?>

   <!-- Start main content -->
   <div class="container main-content clearfix">
       
   <!-- Start Page Content -->       
   <div class="sixteen columns full-width"> 
   <?php 

   // page loop
   while ( have_posts() ) : the_post();

   // check post protected 
   if ( !post_password_required() ) { 
      
      if ( has_post_thumbnail() ) {
        echo '<div class="image-post">';
        echo get_the_post_thumbnail($post->ID, 'full', array('title' => ''));
        echo '</div><!-- End image-post -->';
      } 
   }
   
   if( !empty( $post->post_content) ) {
   echo '<div class="entry-content post-content bottom-3">';
     the_content();
   echo '</div><!-- End page-content -->';
   }

   endwhile; 

   // Reset Query
   wp_reset_query();

   ?>  
   </div><!-- End -->
       
   <div class="clearfix"></div> 
       
   <div class="portfolio bottom-3">
   <?php
   $query_string;
   parse_str( $query_string, $my_query_array );
   $paged = ( isset( $my_query_array['paged'] ) && !empty( $my_query_array['paged'] ) ) ? $my_query_array['paged'] : 1;

   // get projects per page
   if($jozoor_custom_projects_per_page == 1) {
     if($j_portfolio_layout == 'col2') { 
       $j_projects_per_page = $jozoor_portfolio2_projects_per_page;
     } elseif ($j_portfolio_layout == 'col3') { 
       $j_projects_per_page = $jozoor_portfolio3_projects_per_page;
     } elseif ($j_portfolio_layout == 'col4') { 
       $j_projects_per_page = $jozoor_portfolio4_projects_per_page;
     } else {
       $j_projects_per_page = '';
     }
   } else {
       $j_projects_per_page = '';
   }

   if( $j_portfolio_show_cats_filter == 'yes' ) { 
   // get taxonomy
   $j_terms = get_terms("portfolio-category");
   $j_count = count($j_terms);
   if ( $j_count > 0 ) {
   ?>
   <div class="sixteen columns">  
      <div class="title clearfix" id="options">
        <ul id="filters" class="option-set clearfix" data-option-key="filter">
        <li><a href="#*" data-option-value="*" class="selected"><?php _e("All", "jozoorthemes"); ?></a></li>
        <?php
        foreach ( $j_terms as $term ) {
        echo '<li><a href="#.'.jozoor_clean_string($term->name).'" data-option-value=".'.jozoor_clean_string($term->name).'">' . $term->name . '</a></li>'; 
        }
        ?>
        </ul>
      </div>
   </div> <!-- End options -->
       
   <div class="clearfix"></div>
    
   <?php } 
       
   } ?>
       
   <div id="contain" class="clearfix"> 
       
   <?php
   // projects loop
   query_posts('post_type=portfolio&paged='.$paged.'&posts_per_page='.$j_projects_per_page.'');
   if(have_posts()) : while(have_posts()) : the_post(); 
   ?>
       
   <div id="post-<?php the_ID(); ?>" class="<?php $allClasses = get_post_class(); foreach ($allClasses as $class) { echo $class . ' '; } ?><?php echo $j_portfolio_layout_class; ?> item <?php
   $j_project_terms = wp_get_object_terms($post->ID, 'portfolio-category');
   if(!empty($j_project_terms)){
    if(!is_wp_error( $j_project_terms )){
      foreach($j_project_terms as $term){ echo jozoor_clean_string($term->name).' '; }
    }
   }
   ?>">
    <a href="<?php echo get_permalink(); ?>">
    <?php echo get_the_post_thumbnail($post->ID, 'portfolio-thumb', array('title' => '')); ?>
    <div class="img-caption">
    <div class="desc">
      <h3><?php the_title(); ?></h3>
      <p><?php
      $j_terms = get_the_terms( $post->ID, 'portfolio-category' );						
      if ( $j_terms && ! is_wp_error( $j_terms ) ) : 
      $j_draught_links = array();
      foreach ( $j_terms as $term ) {
      $j_draught_links[] = $term->name;
      }		
      echo $j_on_draught = join( ", ", $j_draught_links );
      endif;
      ?></p>
      <span>+</span></div>
    </div><!-- hover effect -->
    </a>
   </div>
   
   <?php
   endwhile; 

   echo '</div><!-- End contain -->';

   echo '<div class="sixteen columns">';
   jozoor_pagination(); // Pagination
   echo '</div>';

   endif;

   // Reset Query
   wp_reset_query();
   ?>
   </div><!-- End portfolio -->
       
<?php get_footer(); ?>