<?php get_header();

?>

   <!-- Start main content -->
   <div class="container main-content clearfix">
   
   <!-- Start Posts -->
   <div class="eleven columns bottom-3">
       
   <?php 

   if(have_posts()) : while(have_posts()) : the_post(); 
				
	get_template_part( 'inc/post_formats/content', get_post_format() ); 

	endwhile; 
    
    jozoor_pagination(); // Pagination

    endif; 
			  
    ?>
       
    </div><!-- End Posts -->
       
<?php get_sidebar(); ?>
    
<?php get_footer(); ?>   