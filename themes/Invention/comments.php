<?php
/**
 * The template for displaying Comments.
 *
 * The area of the page that contains both current comments
 * and the comment form.
 *
 * @package _s
 */

/*
 * If the current post is protected by a password and
 * the visitor has not yet entered the password we will
 * return early without loading the comments.
 */
if ( post_password_required() ) {
	return;
}
?>

<div id="comments" class="comments-area comments-box">

	<?php // You can start editing here -- including this comment! ?>

	<?php if ( have_comments() ) : ?>
		<h3 class="comments-title title bottom-1">
			<?php
				printf( _nx( 'One Comment', '(%1$s) Comments', get_comments_number(), 'comments title', 'jozoorthemes' ),
					number_format_i18n( get_comments_number() ), '<span>' . get_the_title() . '</span>' );
			?>
		</h3>
        <hr class="bottom-2">

		<?php if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) : // are there comments to navigate through ?>
		<nav id="comment-nav-above" class="comment-navigation" role="navigation">
			<h1 class="screen-reader-text"><?php _e( 'Comment navigation', 'jozoorthemes' ); ?></h1>
			<div class="nav-previous"><?php previous_comments_link( __( '&larr; Older Comments', 'jozoorthemes' ) ); ?></div>
			<div class="nav-next"><?php next_comments_link( __( 'Newer Comments &rarr;', 'jozoorthemes' ) ); ?></div>
		</nav><!-- #comment-nav-above -->
		<?php endif; // check for comment navigation ?>

		<ul class="comment-list comments">
			<?php wp_list_comments('callback=jozoor_theme_comment_list'); ?>
		</ul><!-- .comment-list -->

		<?php if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) : // are there comments to navigate through ?>
		<nav id="comment-nav-below" class="comment-navigation" role="navigation">
			<h1 class="screen-reader-text"><?php _e( 'Comment navigation', 'jozoorthemes' ); ?></h1>
			<div class="nav-previous"><?php previous_comments_link( __( '&larr; Older Comments', 'jozoorthemes' ) ); ?></div>
			<div class="nav-next"><?php next_comments_link( __( 'Newer Comments &rarr;', 'jozoorthemes' ) ); ?></div>
		</nav><!-- #comment-nav-below -->
		<?php endif; // check for comment navigation ?>

	<?php endif; // have_comments() ?>

	<?php
		// If comments are closed and there are comments, let's leave a little note, shall we?
		if ( ! comments_open() && '0' != get_comments_number() && post_type_supports( get_post_type(), 'comments' ) ) :
	?>
    <p class="no-comments"><?php _e( 'Comments are closed.', 'jozoorthemes' ); ?></p>
	<?php endif; ?>
    
    <div class="comment-form top-4">
        
	<?php // start comment form
    $req      = get_option( 'require_name_email' );
	$aria_req = ( $req ? " aria-required='true'" : '' );
    $required_text = sprintf( ' <small class="required">' . __('Required fields are marked %s', 'jozoorthemes'), '<span class="required">*</span></small>' );
    $comment_form_args = array (
    'id_form'           => 'commentform',
    'id_submit'         => 'submit',
    'title_reply'       => '<hr class="bottom-2"><h3 class="title bottom-2">'.__( 'Leave a Reply', 'jozoorthemes' ).'</h3>',
    'title_reply_to'    => __( 'Leave a Reply to %s', 'jozoorthemes' ),
    'cancel_reply_link' => __( 'Cancel Reply', 'jozoorthemes' ),
    'label_submit'      => __( 'Submit Comment', 'jozoorthemes' ),

    'comment_field' =>  '<div class="form-box big"><label for="comment">' . __('Comment', 'jozoorthemes') .
    '</label><textarea id="comment" name="comment" cols="45" rows="8" aria-required="true">' .
    '</textarea></div>',

  'must_log_in' => '<p class="must-log-in">' .
    sprintf(
      __( 'You must be <a href="%s">logged in</a> to post a comment.', 'jozoorthemes' ),
      wp_login_url( apply_filters( 'the_permalink', get_permalink() ) )
    ) . '</p><br>',

  'logged_in_as' => '<p class="logged-in-as">' .
    sprintf(
    __( 'Logged in as <a href="%1$s">%2$s</a>. <a href="%3$s" title="Log out of this account">Log out?</a>', 'jozoorthemes' ),
      admin_url( 'profile.php' ),
      $user_identity,
      wp_logout_url( apply_filters( 'the_permalink', get_permalink( ) ) )
    ) . '</p><br>',

  'comment_notes_before' => '<p class="comment-notes">' .
    __( 'Your email address will not be published.', 'jozoorthemes' ) . ( $req ? $required_text : '' ) .
    '</p><br>',

  'comment_notes_after' => '<div class="clearfix"></div><p class="form-allowed-tags">' .
    sprintf(
      __( 'You may use these <abbr title="HyperText Markup Language">HTML</abbr> tags and attributes: %s', 'jozoorthemes' ),
      ' <code>' . allowed_tags() . '</code>'
    ) . '</p>',

  'fields' => apply_filters( 'comment_form_default_fields', array(

    'author' =>
      '<div class="form-box"><label for="author">' . __( 'Name', 'jozoorthemes' ) . ( $req ? ' <small class="required">*</small>' : '' ) . 
      '</label><input id="author" name="author" type="text" value="' . esc_attr( $commenter['comment_author'] ) .
      '" size="30"' . $aria_req . ' class="text" /></div>',

    'email' =>
      '<div class="form-box"><label for="email">' . __( 'Email', 'jozoorthemes' ) . ( $req ? ' <small class="required">*</small>' : '' ) .
      '</label><input id="email" name="email" type="text" value="' . esc_attr(  $commenter['comment_author_email'] ) .
      '" size="30"' . $aria_req . ' class="text" /></div>',

    'url' =>
      '<div class="form-box last"><label for="url">' .
      __( 'Website', 'jozoorthemes' ) . '<small class="required"></small></label>' .
      '<input id="url" name="url" type="text" value="' . esc_attr( $commenter['comment_author_url'] ) .
      '" size="30" class="text" /></div>'
      )
    ), 
        
    );

    comment_form($comment_form_args); ?>
        
    </div><!-- End Comment form -->

</div><!-- #comments -->