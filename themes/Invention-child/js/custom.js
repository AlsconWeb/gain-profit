$(document).ready(function(){
	$('.js-calc-refback-button').on('click', function (e) {
		e.preventDefault();
		$('.calc-salary-modal').modal('show');
	})
	$('#calcSalary').on('keyup', function(){
		var percent = parseFloat($('.js-ref-percent').val()),
			amount = parseFloat($('.js-ref-amount').val()),
			salary = 0;
		if(percent > 0 && percent <= 100 && amount > 0){
			salary = (amount/100) * percent;
			$('.js-ref-salary').val(salary);
		}
	})
	$('.js-add-to-form').on('click', function (e) {
		e.preventDefault();
		$('#user_curr').val($('.js-ref-salary').val());
		$('.calc-salary-modal').modal('hide');
	})
	$('a[rel*="album"]').fancybox({
		scrolling : 'no',
		touch: false,
		helpers: {
			overlay: {
				locked: false
			}
		}
	});

	/**
	 * FORM REGISTRATION
	 *
	 */
	var formReg = $('#c_registerform');
	formReg.submit(function(e){
		e.preventDefault();
		// console.log('asd');
		var userName = $('#c_user_login').val();
		var userEmail = $('#user_email').val();
		var userPsw = $('#user_psw').val();
		var refUserId = $('#userid').val();
		var refEventId = $('#refid').val();
		var blogEvent = $('#event_bl:checked', formReg).val();
		var blogBonus = $('#bonus_bl:checked', formReg).val();

		// console.log( blogEvent, blogBonus );

		var errorName = $('.error-login');
		var errorEmail = $('.error-email');
		var errorPsw = $('.error-psw');
		var errorBl = $('.error-bl');
		var succReg = $('.reg-suc');

		errorPsw.html( '' );
		errorEmail.html( '' );
		errorName.html( '' );
		errorBl.html( '' );
		succReg.html( '' );

		console.log( userName, userEmail, userPsw, refUserId );

		$.ajax({
			type: "POST",
			url: ajaxurl,
			data: {
				action: 'new_user_registration',   // name action function
				user_name: userName,
				user_email: userEmail,
				user_psv: userPsw,
				user_ref_u_id: refUserId,
				user_ref_e_id: refEventId,
				event_bl: blogEvent,
				bonus_bl: blogBonus,
			},
			success: function(data){
				// console.log( data );
				if( data == "login-no-val" ){
					errorName.html( "Введите верное Имя" );
					console.log('login no ok');
				} else if( data == "login-dupl" ){
					errorName.html( "Имя существует" );
					console.log('login dupl');
				} else if( data == "login-empty" ){
					errorName.html( "Заполните поле" );
					console.log('login dupl');
				} else if( data == "email-no-val" ){
					errorEmail.html( "Введите верный email" );
					console.log('mail dupl');
				} else if( data == "email-dupl" ){
					errorEmail.html( "email уже существует" );
				} else if( data == "pass-length" ){
					errorPsw.html( "Введите пароль от 5 до 15 символов" );
				} else if( data == "pass-empty" ){
					errorPsw.html( "Заполните поле" );
				} else if( data == "blog-empty" ){
					errorBl.html( "Выберете поле" );
				} else if( data == "subscriber" ){
					succReg.html( 'Вы зарегестрированы, проверьте свой Email' );
					// setTimeout( function() {$.fancybox.close(); },3000);
					// window.location.href=window.location.origin;
					window.location.reload();
				} else {
					errorPsw.html( data );
					errorEmail.html( '' );
					errorName.html( '' );
					console.log('all ok');
					succReg.html( 'Вы зарегестрированы, проверьте свой Email' );
					// setTimeout( function() {$.fancybox.close(); },3000);
				}
				//console.log('ok');
			},
		}); // END AJAX
	}); // END FORM SUBMIT


	/**
	 * FORM DEBIT PERSONAL ACCOUNT
	 *
	 */
	var formDebs = $('#debit');
	formDebs.submit(function(e){
		e.preventDefault();
		var userNic = $('#user_nic').val();
		var userId = $('#user_id').val();
		var userDeb = $('#user_deb').val();
		var userMail = $('#user_mail').val();
		var userText = $('#user_text').val();
		var userPay = $('input[name=pay_type]:checked', formDeb).val();

		var errorNic = t.find('.error-nic');
		var errorCur = t.find('.error-curr');
		var errorMail = t.find('.error-mail');
		var succDeb = t.find('.deb-suc');

		errorNic.html( '' );
		errorCur.html( '' );
		errorMail.html( '' );
		succDeb.html( '' );

		$.ajax({
			type: "POST",
			url: ajaxurl,
			data: {
				action: 'user_debit',   // name action function
				user_nic: userNic,
				user_id: userId,
				user_deb: userDeb,
				user_mail: userMail,
				user_text: userText,
				pay_type: userPay,
			},
			success: function(data){
				if( data == "nic-no-val" ){
					errorNic.html( 'Введите свой логин' );
				} else if( data == "nic-no" ){
					errorNic.html( 'Логин не существует' );
				} else if( data == "mail-no-val" ){
					errorMail.html( 'Введите свой Email' );
				} else if( data == "mail-no" ){
					errorMail.html( 'Email не существует' );
				} else if( data == "deb-error" ){
					errorCur.html( 'Введите корректное число' );
				} else {
					errorNic.html( '' );
					errorCur.html( '' );
					errorMail.html( '' );
					console.log( data );
					succDeb.html( 'Форма отправлена администратору' );
				}
			},
		}); // END AJAX
	}); // END FORM SUBMIT

	// debit призовые
	var formDeb = $('#form_debit');
	formDeb.submit(function(e){
		e.preventDefault();
		var t = $(this),
			typeForm = $('#d_debit_type').val(),
			userId = $('#d_user_id').val(),
			// userNic = $('#d_user_nic').val(),
			userMail = $('#d_user_mail').val(),
			userProj = $('#d_user_proj').val(),
			userProjLogin = $('#d_proj_login').val(),
			userWall = $('#d_user_wall').val(),
			// userDeb = $('#d_user_deb').val(),
			userCurr = $('#d_user_curr').val(),
			// userDebData = $('#d_user_deb_dat').val(),
			// userDebPlan = $('#d_user_deb_plan').val(),
			userText = $('#d_user_text').val(),
			userPayType = $("input[name='d_pay_type']:radio:checked").val(),
			errorNic = t.find('.error-nic'),
			errorCur = t.find('.error-curr'),
			errorMail = t.find('.error-mail'),
			succDeb = t.find('.deb-suc');

		errorNic.html( '' );
		errorCur.html( '' );
		errorMail.html( '' );
		succDeb.html( '' );

		$.ajax({
			type: "POST",
			url: ajaxurl,
			data: {
				action: 'user_bonus',   // name action function
				type_form: typeForm,
				user_id: userId,
				// user_nic: userNic,
				user_mail: userMail,
				user_proj: userProj,
				proj_login: userProjLogin,
				user_wall: userWall,
				// user_deb: userDeb,
				user_curr: userCurr,
				// user_deb_dat: userDebData,
				// user_deb_plan: userDebPlan,
				user_text: userText,
				user_pay_type: userPayType,
			},
			success: function(data){
				if( data == "nic-no-val" ){
					errorNic.html( 'Введите свой логин' );
				} else if( data == "nic-no" ){
					errorNic.html( 'Логин не существует' );
				} else if( data == "mail-no-val" ){
					errorMail.html( 'Введите свой Email' );
				} else if( data == "mail-no" ){
					errorMail.html( 'Email не существует' );
				} else if( data == "deb-error" ){
					errorCur.html( 'Введите корректное число' );
				} else {
					errorNic.html( '' );
					errorCur.html( '' );
					errorMail.html( '' );
					console.log( data );
					succDeb.html( 'Форма отправлена администратору' );
				}
			},
		}); // END AJAX
	}); // END FORM SUBMIT


	// bonus рефбек
	$('#user_deb_dat').mask("99.99.9999");
	var formBon = $('#form_bonus');
	formBon.submit(function(e){
		e.preventDefault();
		var t = $(this);
		var typeForm = $('#bonus_type').val();
		var userId = $('#user_id').val();
		var userNic = $('#user_nic').val();
		var userMail = $('#user_mail').val();
		var userProj = $('#user_proj').val();
		var userProjLogin = $('#proj_login').val();
		var userProjMail = $('#proj_mail').val();

		var userWall = $('#user_wall').val();
		var userDeb = $('#user_deb').val();
		var userCurr = $('#user_curr').val();
		var userDebData = $('#user_deb_dat').val();
		var userDebPlan = $('#user_deb_plan').val();
		var userText = $('#user_text').val();
		var userPayType = $("input[name='pay_type']:radio:checked").val();

		var errorNic = t.find('.error-nic');
		var errorCur = t.find('.error-curr');
		var errorMail = t.find('.error-mail');
		var succDeb = t.find('.deb-suc');

		console.log( userPayType );

		errorNic.html( '' );
		errorCur.html( '' );
		errorMail.html( '' );
		succDeb.html( '' );

		$.ajax({
			type: "POST",
			url: ajaxurl,
			data: {
				action: 'user_debit',   // name action function
				type_form: typeForm,
				user_id: userId,
				user_nic: userNic,
				user_mail: userMail,
				user_proj: userProj,
				proj_login: userProjLogin,
				proj_mail: userProjMail,
				user_wall: userWall,
				user_deb: userDeb,
				user_curr: userCurr,
				user_deb_dat: userDebData,
				user_deb_plan: userDebPlan,
				user_text: userText,
				user_pay_type: userPayType,
			},
			success: function(data){
				if( data == "nic-no-val" ){
					errorNic.html( 'Введите свой логин' );
				} else if( data == "nic-no" ){
					errorNic.html( 'Логин не существует' );
				} else if( data == "mail-no-val" ){
					errorMail.html( 'Введите свой Email' );
				} else if( data == "mail-no" ){
					errorMail.html( 'Email не существует' );
				} else if( data == "deb-error" ){
					errorCur.html( 'Введите корректное число' );
				} else {
					errorNic.html( '' );
					errorCur.html( '' );
					errorMail.html( '' );
					console.log( data );
					succDeb.html( 'Форма отправлена администратору' );
				}
			},
		}); // END AJAX
	}); // END FORM SUBMIT bonus рефбек

	// страховка
	var formInsurance = $('#form_insurance');
	formInsurance.submit(function(e){
		e.preventDefault();
		var t = $(this),
			formData = new FormData($(t)[0]),
			err_ins_mail = $('.i-error-mail'),
			err_ins_proj = $('.i-error-proj'),
			err_ins_wall = $('.i-error-wall'),
			succ_form = t.find('.i-suc');

		err_ins_mail.html( '' );
		err_ins_proj.html( '' );
		err_ins_wall.html( '' );
		succ_form.html( '' );

		$.ajax({
			type: "POST",
			url: ajaxurl,
			data: formData,
			processData: false,
			contentType: false,
			success: function(data){
				if( data == "nic-no-val" ){
					err_ins_mail.html( 'Введите свой логин' );
				} else {
					err_ins_mail.html( '' );
					err_ins_proj.html( '' );
					err_ins_wall.html( '' );
					succ_form.html( 'Форма отправлена администратору' );
				}
			},
		}); // END AJAX
	});// END FORM SUBMIT страховка

	// forgot password
	var formPass = $('#forgot_pass');
	formPass.submit(function(e){
		e.preventDefault();

		var userMail = $('.forgot_pass-mail').val(),
			errorM = $('.forgot_pass-err'),
			succeM = $('.forgot_pass-suc');

		errorM.html( '' );
		succeM.html( '' );

		$.ajax({
			type: "POST",
			url: ajaxurl,
			data: {
				action: 'fog_pass',
				user_mail: userMail,
			},
			success: function(data){
				if( data == "no-mail" ){
					errorM.html( 'Введите свой email' );
				} else {
					errorM.html( '' );
					succeM.html( '' );
					succeM.html( 'Пароль отправлен на email' );
					setTimeout(function(){
						$('.forgot-password-modal').modal('hide');
						$('.entrance').modal('show');
					},1000)
				}
			},
		});
	});

	$('.commentator-dropdown-toggle').click(function(e){
		e.preventDefault();
		console.log('qqq');
	});

	$('.commentator-pull-right li:nth-of-type(3)').attr('class', 'button-to-come-in');
	$('.commentator-pull-right li:nth-of-type(5)').attr('class', 'button-check-in');
	$('body').on('click','.button-check-in', function(e){
		e.preventDefault();
		$('.check-in').modal('show');
	})
	$('body').on('click','.button-to-come-in', function(e){
		e.preventDefault();
		$('.entrance').modal('show');
	})

	$('body').on('click','.forgot-password', function(e){
		e.preventDefault();
		$('.entrance').modal('hide');
		$('.forgot-password-modal').modal('show');
	})

});