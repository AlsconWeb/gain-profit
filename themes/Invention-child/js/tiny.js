(function() {
	tinymce.PluginManager.add('form_button', function( editor, url ) {
		editor.addButton( 'form_button', {
			text: 'Форма участия',
			icon: false,
			onclick: function() {
				editor.insertContent('[contest_form]');
			}
		});
	});
})();