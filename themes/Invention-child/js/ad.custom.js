$(document).ready(function(){
	var delPost = $('.action-del');
	delPost.click(function(e){
		e.preventDefault();
		var t = $(this);
		var dataPost = t.data('post');
		
		$.ajax({
			type: "POST",
			url: ajaxurl,
			data: {
				action: 'del_post',
				post_del: dataPost,
			},
			success: function(data){
				alert( data );
				window.location.reload();
			},
		});
	});

	/* оплатить */
	var adminList = $('#wpbody-content');
	var btnPay = $('.js-action-pay');
	adminList.on('click', '.js-action-pay-disable', function(e){
		e.preventDefault();
	})
	var payResult = $('.js-pay-res');
	var payWrapResult = $('.js-pay-res-wrap');
	var closePayResult = $('.js-pay-res-wrap span');

	closePayResult.click(function(){
		payWrapResult.hide();
	});
	payWrapResult.hide();

	adminList.on('click', '.js-action-pay', function(e){
		e.preventDefault();
		var t = $(this),
			payPost = t.data('post'),
			payUser = t.data('user'),
			payWall = t.data('wall'),
			payAmou = t.data('amount'),
			payType = t.data('type'),
			newPayList = $('.js-pay-list');

		payWrapResult.hide();

		$.ajax({
			type: "POST",
			url: ajaxurl,
			data: {
				action: 'pay_action',
				pay_post: payPost,
				pay_user: payUser,
				pay_wall: payWall,
				pay_amount: payAmou,
				pay_type: payType,
			},
			success: function(data){
				// console.log( data );
				if( data.status == 'pay-send' ){
					t.addClass('js-action-pay-disable');
					t.removeClass('js-action-pay');
					newPayList.html( data.html );
					payResult.html( data.alert );
					payWrapResult.show();
					// console.log( data.alert );
				} else {
					payResult.html( data.alert );
					payWrapResult.show();
					// console.log( data.alert );
				}
				// console.log( data.alert );
			},
		});

		// console.log( payPost, payUser, payWall, payAmou, payType );
	})
});