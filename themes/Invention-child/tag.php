<?php get_header(); ?>

<?php
   // get menu item id
   global $wpdb;
   $post_id = $wpdb->get_var( 'SELECT ID FROM '.$wpdb->posts.' WHERE post_name="'.$jozoor_other_blog_pages_current_page.'" AND post_type="page" AND post_status="publish"' );
   $post_title = $wpdb->get_var( 'SELECT post_id FROM '.$wpdb->postmeta.' WHERE meta_key="_menu_item_object_id" AND meta_value="'.$post_id.'"' );
   
   // add active class for nav item
   if (!is_admin() && $jozoor_other_blog_pages_current_page != 'Select Page Name') { ?>
<script>
jQuery(document).ready(function($) {
    $("#menu-item-<?php echo $post_title;  ?>").addClass("current-menu-item");
});
</script>
<?php } ?>

<!-- Start main content -->
<div class="container main-content clearfix">

    <?php if($jozoor_other_blog_pages_blog_style == 'Blog3') { ?>
    <div class="blog-3 bottom-2 top-0">
        <?php } else { ?>

        <?php if( $jozoor_other_blog_pages_layout_style == 'Left Sidebar' ) { 
   if( $jozoor_other_blog_pages_sidebar_type == 'Default Sidebar' ) { get_sidebar(); } else {
   echo '<!-- Start Sidebar Widgets --><aside class="five columns sidebar bottom-3">';
   if (function_exists('dynamic_sidebar') && dynamic_sidebar($jozoor_other_blog_pages_sidebar_type)) : endif;
   echo '</aside><!-- End Sidebar Widgets -->';
   }  
   } ?>

        <!-- Start Posts -->
        <?php if( $jozoor_other_blog_pages_layout_style == 'Right Sidebar' || $jozoor_other_blog_pages_layout_style == 'Left Sidebar' ) { ?>
        <div class="eleven columns bottom-3">
            <?php } else { ?>
            <div class="sixteen columns full-width bottom-3">
                <?php } ?>

                <?php } ?>

                <?php 

   // check blog style
   if($jozoor_other_blog_pages_blog_style == 'Blog3') { 
       $j_folder_template_part_other = 'blog-3/';
   } elseif ($jozoor_other_blog_pages_blog_style == 'Blog2') { 
       $j_folder_template_part_other = 'blog-2/'; 
   } else { 
       $j_folder_template_part_other = '';
   }

   if($jozoor_other_blog_pages_blog_style == 'Blog3') { 
   echo '<div id="contain" class="clearfix">';       
   }

   if(have_posts()) : while(have_posts()) : the_post(); 
				
	get_template_part( 'inc/post_formats/'.$j_folder_template_part_other.'content', get_post_format() );  
    
	endwhile; 

    if($jozoor_other_blog_pages_blog_style == 'Blog3') { 
    echo '</div><!-- End contain -->';       
    }
    
    if($jozoor_other_blog_pages_blog_style == 'Blog3') { 
    echo '<div class="pagination-contain top-2">';
    }

    jozoor_pagination(); // Pagination

    if($jozoor_other_blog_pages_blog_style == 'Blog3') {  
    echo '</div>';
    }

    endif; 
    $id = $post->ID;
    $id_tag_trm = get_the_tags( $id );
    $mane_sitebare = get_term_meta($id_tag_trm[0]->term_id);
    ?>

            </div><!-- End Posts -->

            <?php 
    
        if (!$mane_sitebare)
        {
            echo '<!-- Start Sidebar Widgets  --><aside class="five columns sidebar bottom-3">';
            dynamic_sidebar('Default Sidebar');
            echo '</aside><!-- End Sidebar Widgets -->';
        }
        else{
            
            echo '<!-- Start Sidebar Widgets  --><aside class="five columns sidebar bottom-3">';
                dynamic_sidebar($mane_sitebare['sidebar'][0]);
            echo '</aside><!-- End Sidebar Widgets -->';
    
                    }
       
 ?>

            <?php get_footer(); ?>