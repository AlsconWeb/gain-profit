<?php header("Content-type: text/css; charset=utf-8"); 

// Setup location of WordPress
$absolute_path = __FILE__;
$path_to_file = explode( 'wp-content', $absolute_path );
$path_to_wp = $path_to_file[0];

// Access WordPress
require_once( $path_to_wp . '/wp-load.php' );

// get options
global $smof_data;

// Dark Folder Src
if($smof_data['theme_skin'] == 'dark') { 
    $jozoor_images_src_dark = 'dark/'; 
} else{
    $jozoor_images_src_dark = '';
}

echo "/*   
Theme Name: Invention
Theme URI: http://themes.jozoor.com/?theme=Invention-WP
Description: Invention Theme corporate, portfolio and business websites, responsive and clean layout, more than color skins, Fully Customizable, Comprehensive Theme Options and more..
Author: Jozoor Team
Author URI: http://www.jozoor.com
Version: 1.1
*/

/*
= = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =   
=     00   00 00 00   00 00 00   00 00 00   00 00 00   00 00  =
=     00   00    00        00    00    00   00    00   00     =
=     00   00    00      00      00    00   00    00   00     =  
=     00   00    00    00        00    00   00    00   00     =
=  00 00   00 00 00   00 00 00   00 00 00   00 00 00   00     =
= = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
= = = = = = Thanks for watching our source code :)  = = = = = =
*/

";

#=============================================================#
# Custom Styles Options
#=============================================================#
?>
/* Start Custom Styles Options */
.single-gallery .comments-title {
    margin-top: 35px;
}

.animate-element p:last-child {
    margin-bottom:0;
}

.page-section img[class*=wp-image-] {
    display: block;
    margin: 0 auto;
}

.page-section .button {
   line-height: 22px;
}

.container .column.shortcode-column.no-margin-bottom, .container .columns.shortcode-column.no-margin-bottom, .container .column.shortcode-column p:last-child, .container .columns.shortcode-column p:last-child {
    margin-bottom: 0;
}

.switcher {
    display:none;
}

.gmaps {
    color: #5A5C5D;
}

.woocommerce .woocommerce-message, .woocommerce .woocommerce-error, .woocommerce .woocommerce-info, .woocommerce-page .woocommerce-message, .woocommerce-page .woocommerce-error, .woocommerce-page .woocommerce-info, .woocommerce #payment div.payment_box, .woocommerce-page #payment div.payment_box {
    color: inherit;
}

.woocommerce .cart .quantity input.qty { background: inherit; }

.cart-tab span.contents { overflow: hidden; }

body.rtl .button.small.no-text {
    width: 40px;
    height: 22px;
}

body.rtl .post-content [class*=" icon-"], body.rtl .post-content [class*=" social_icon-"], body.rtl .post-content [class^=icon-], body.rtl .post-content [class^=social_icon-], body.rtl .slider-1 [class*=" icon-"], body.rtl .slider-1 [class*=" social_icon-"], body.rtl .slider-1 [class^=icon-], body.rtl .slider-1 [class^=social_icon-], body.rtl .slider-2 [class*=" icon-"], body.rtl .slider-2 [class*=" social_icon-"], body.rtl .slider-2 [class^=icon-], body.rtl .slider-2 [class^=social_icon-] {
    margin-left: 4px;
    margin-right:0;
}

body.rtl .button.no-text i {
    margin-left: 0;
    margin-right:0;
}

pre { direction: ltr; }

body.rtl .slider-shortcode {
    overflow: hidden;
}

body.rtl .top-bar {
    padding: 13px 0 7px;
}

body.rtl header .top-bar.none {
    min-height: 4px;
    padding: 0;
}

.type-post.sticky {
   padding: 20px;
}

<?php if($smof_data['logo_text_instead_image'] == 1) { ?>

/* logo text style */
h1.site-title-logo {
   font:<?php echo $smof_data['logo_name_font_sizes_styles']['style'].' '.$smof_data['logo_name_font_sizes_styles']['size'].' "'.$smof_data['logo_name_font_type'].'", Arial, sans-serif;'; ?>
}

h1.site-title-logo a, h1.site-title-logo a:hover {
   color: <?php echo $smof_data['logo_name_custom_color']; ?>;
}

<?php } ?>
/* logo img style */
.logo img {
   width: <?php echo $smof_data['logo_width']; ?>px;
   height: <?php echo $smof_data['logo_height']; ?>px;
}

.logo {
    margin: <?php echo $smof_data['logo_top_margin']; ?>px 0 <?php echo $smof_data['logo_bottom_margin']; ?>px 0;
}

<?php 
// header bg
if($smof_data['add_bg_image_main_header'] == 1) { ?>

header .main-header {
    background:<?php echo 'url('.$smof_data['bg_image_main_header'].') '.$smof_data['bg_image_repeat_main_header']; ?>;
}

<?php } ?>

<?php 
// body bg
if($smof_data['custom_body_bg_image'] == 1) { 

if($smof_data['body_bg_repeat'] == 'fixed') { ?>



<?php } else { ?>

body {
    background:<?php echo 'url('.$smof_data['custom_body_background'].') '.$smof_data['body_bg_repeat']; ?> !important;
}

<?php } ?>

<?php } ?>

.navigation > ul > li > a {
    padding: <?php echo $smof_data['menu_links_top_padding_one']; ?>px <?php echo $smof_data['menu_links_leftright_padding_one']; ?>px <?php echo $smof_data['menu_links_bottom_padding_one']; ?>px;
}

body.rtl .navigation > ul > li > a {
    padding: <?php echo $smof_data['menu_links_top_padding_one'] - 1; ?>px <?php echo $smof_data['menu_links_leftright_padding_one']; ?>px <?php echo $smof_data['menu_links_bottom_padding_one']; ?>px;
}

.navigation ul li ul {
    top: <?php echo $smof_data['submenu_links_top_position_one']; ?>px !important;
}

header.style-3 .navigation > ul > li > a {
    padding: <?php echo $smof_data['menu_links_top_padding_two']; ?>px <?php echo $smof_data['menu_links_leftright_padding_two']; ?>px <?php echo $smof_data['menu_links_bottom_padding_two']; ?>px;
}

body.rtl header.style-3 .navigation > ul > li > a {
    padding: <?php echo $smof_data['menu_links_top_padding_two'] - 1; ?>px <?php echo $smof_data['menu_links_leftright_padding_two']; ?>px <?php echo $smof_data['menu_links_bottom_padding_two']; ?>px;
}

header.style-3 .navigation ul li ul {
    top: <?php echo $smof_data['submenu_links_top_position_two']; ?>px !important;
}

header.style-3 .header-custom-content {
    margin: <?php echo $smof_data['header_layout3_right_custom_content_margin']; ?>px 0;
}

.top-header-custom-content, .bottom-header-custom-content {
    padding:10px 0;
}

.tagcloud a {
    font-size:14px !important;
}

.widget .submit-search, .woocommerce.widget_product_search input[type="submit"] {
	background:url(../images/icons/<?php echo $jozoor_images_src_dark; ?>search-icon.png) no-repeat;
} 

header.bg-border-none .navigation ul li a:hover, header.bg-border-none .navigation ul li a.selected, header.bg-border-none .navigation ul li.active a, header.bg-border-none .navigation ul li.current-menu-item a, header.bg-border-none .navigation ul li.current-menu-parent a, header.bg-border-none .navigation ul li.current-menu-ancestor a {
    border-top: 4px solid transparent;
    background:transparent;
}

header.style-3.bg-border-none .navigation ul li a:hover, header.style-3.bg-border-none .navigation ul li a.selected, header.style-3.bg-border-none .navigation ul li.active a, header.style-3.bg-border-none .navigation ul li.current-menu-item a, header.style-3.bg-border-none .navigation ul li.current-menu-parent a, header.style-3.bg-border-none .navigation ul li.current-menu-ancestor a {
    border-top:1px solid transparent;
    background:transparent;
}

<?php if($smof_data['responsive_design'] != 1) { ?>
#wrap {
	min-width:1000px;
}
<?php } ?>

<?php if($smof_data['custom_design_colors'] == 1) { ?>

<?php 
// Body Text Color
if(!empty($smof_data['body_text_color'])) { ?>
/* Body Text Color */
body, .services.style-1 .item h3, .services.style-1 .item p, .services.style-1 .item a, .slidecontrols li a.carousel1-next.carousel1-disabled, .slidecontrols li a.carousel1-prev.carousel1-disabled, .slidecontrols li a.carousel2-next.carousel2-disabled, .slidecontrols li a.carousel2-prev.carousel2-disabled, .slidecontrols li a.carousel3-next.carousel3-disabled, .slidecontrols li a.carousel3-prev.carousel3-disabled, .slidecontrols li a.carousel4-next.carousel4-disabled, .slidecontrols li a.carousel4-prev.carousel4-disabled, #toTop, .down-button, .accordion p, .progress-bar h5, .button.normal, #toggle-view li h4.normal, #toggle-view li span.normal, .services.style-2 .item h4, .services.style-3 .item h4, .portfolio-control a, .sidebar .widget .text-search, footer .widget .text-search, header .widget .text-search, .widget.most-posts ul.posts li a, .post-tags a:hover, .format-quote .entry-quote a:hover, .project-details li.cats a:hover, .dropcaps.color, .page-section .button.normal, .page-section .button.normal i, .latest-blog .item p, .latest-blog .item .meta, .blog-3 .item .post-meta, .blog-3 .item .post-meta i, .related-posts .item .meta,
/* new wp code */
.carousel-shortcode .slidecontrols li [class*="-disabled"], .carousel-shortcode .slidecontrols li [class*=" -disabled"], .woocommerce-ordering select, .woocommerce-ordering select, .woocommerce ul.products li.product a h3, .woocommerce-page ul.products li.product a h3 {
	 color:<?php echo $smof_data['body_text_color']; ?>;
}
<?php } ?>

<?php 
// Footer Text Color
if(!empty($smof_data['footer_text_color'])) { ?>
/* Footer Text Color */
footer, .tweet.footer, footer p {
	 color:<?php echo $smof_data['footer_text_color']; ?>;
}
<?php } ?>

<?php 
// Top Bar Header Text Color
if(!empty($smof_data['topbar_header_text_color'])) { ?>
/* Top Bar Header Text Color */
.top-bar, .top-bar a, header .social a {
	 color:<?php echo $smof_data['topbar_header_text_color']; ?>;
}
<?php } ?>

<?php 
// Down Bar Footer Text Color
if(!empty($smof_data['downbar_footer_text_color'])) { ?>
/* Down Bar Footer Text Color */
span.copyright, footer .social a {
	 color:<?php echo $smof_data['downbar_footer_text_color']; ?>;
}
<?php } ?>

<?php 
// Body Links Color
if(!empty($smof_data['body_links_color'])) { ?>
/* Body Links Color */
a, .page-sections .page-section a, .latest-blog .item h3 a, .accordion.style2 h4 a, .latest-blog .item .meta a, .blog-3 .item .post-meta a  {
	 color:<?php echo $smof_data['body_links_color']; ?>;
}
<?php } ?>

<?php 
// Body Links ( hover ) Color
if(!empty($smof_data['body_links_hover_color'])) { ?>
/* Body Links ( hover ) Color */
a:hover, .page-sections .page-section a:hover, .latest-blog .item h3 a:hover, .accordion.style2 h4 a:hover, .latest-blog .item .meta a:hover, .blog-3 .item .post-meta a:hover  {
	 color:<?php echo $smof_data['body_links_hover_color']; ?>;
}
<?php } ?>

<?php 
// Footer Links Color
if(!empty($smof_data['footer_links_color'])) { ?>
/* Footer Links Color */
footer a {
	 color:<?php echo $smof_data['footer_links_color']; ?> !important;
}
<?php } ?>

<?php 
// Footer Links ( hover ) Color
if(!empty($smof_data['footer_links_hover_color'])) { ?>
/* Footer Links ( hover ) Color */
footer a:hover {
	 color:<?php echo $smof_data['footer_links_hover_color']; ?> !important;
}
<?php } ?>

<?php 
// Heading Title Color in ( body )
if(!empty($smof_data['heading_title_color_body'])) { ?>
/* Heading Title Color in ( body ) */
h3.title  {
	 color:<?php echo $smof_data['heading_title_color_body']; ?>;
}
<?php } ?>

<?php 
// Heading Title Color in ( footer )
if(!empty($smof_data['heading_title_color_footer'])) { ?>
/* Heading Title Color in ( footer ) */
footer h3.title  {
	 color:<?php echo $smof_data['heading_title_color_footer']; ?>;
}
<?php } ?>

<?php 
// Page Title Color
if(!empty($smof_data['page_title_color'])) { ?>
/* Page Title Color */
.page-title h1, .single-gallery-meta  {
	 color:<?php echo $smof_data['page_title_color']; ?>;
}
<?php } ?>

<?php 
// Custom Color Scheme        
if(!empty($smof_data['custom_color_scheme'])) { 

// Convert Hex Color to RGB
$j_hex_color = str_replace("#", "", $smof_data['custom_color_scheme']);
if(strlen($j_hex_color) == 3) {
      $r = hexdec(substr($j_hex_color,0,1).substr($j_hex_color,0,1));
      $g = hexdec(substr($j_hex_color,1,1).substr($j_hex_color,1,1));
      $b = hexdec(substr($j_hex_color,2,1).substr($j_hex_color,2,1));
} else {
      $r = hexdec(substr($j_hex_color,0,2));
      $g = hexdec(substr($j_hex_color,2,2));
      $b = hexdec(substr($j_hex_color,4,2));
}
$j_rgb_color = array($r, $g, $b);                                                     

?>

/* Custom Color Scheme */
/* Backgrounds
================================================== */
::-moz-selection {
    background:<?php echo $smof_data['custom_color_scheme']; ?>;
}

::selection {
    background:<?php echo $smof_data['custom_color_scheme']; ?>;
}

.services.style-1 .item.active .circle, .services.style-1 .item:hover .circle, .more:hover, .subscribe input.submit, #menu > a, .page-title, .accordion h4.ui-state-active, .accordion.style2 h4.ui-state-active .icon-plus, .accordion.style2 h4.ui-state-active .icon-minus, .meter > span:after, .animate > span > span, .button.color, .button.black:hover, #horizontal-tabs.style2 ul.tabs li.current, #vertical-tabs.style2 ul.tabs li.current, #horizontal-tabs.style2 ul.tabs li:hover, #vertical-tabs.style2 ul.tabs li:hover, #toggle-view li h4.color, .highlight-color, .dropcaps.color:first-letter, .pagination a.next:hover, .pagination a.prev:hover, .pagination a:hover, .pagination span.current, table.style.color th, .meter .meter-content, .team .item .member-social a:hover, .services.style-2 .item .circle, .services.style-2 .item.active:hover .circle, .services.style-3 .item.active .circle, .services.style-3 .item:hover .circle, .tables-column .footer h4 a:hover, .tables-column.color .header h3 , .tables-column.color .footer h4 a, .tables-column .header h3 span.pop, .share-social a:hover, .popular-tags a:hover, .widget .followers li a:hover, body.under-construction #wrap.boxed, body.under-construction .content input.subscribe-button, .flex-control-nav li a.flex-active, .flex-control-nav li a:hover, header.style-4 .top-bar, html.ie.ie8 .img-caption, html.ie.ie8 .fancybox-title-float-wrap .child, .slide-bg-color,
/* wp classes */
.tagcloud a:hover, .bg-color, .member-social.author-links a:hover, .page-section.theme-skin-color, .wpcf7-submit, .woocommerce span.onsale, .woocommerce-page span.onsale, .woocommerce a.button, .woocommerce-page a.button, .woocommerce a.button:hover, .woocommerce-page a.button:hover,
.woocommerce nav.woocommerce-pagination ul li span.current, .woocommerce-page nav.woocommerce-pagination ul li span.current, .woocommerce nav.woocommerce-pagination ul li a:hover, .woocommerce-page nav.woocommerce-pagination ul li a:hover, .woocommerce .widget_price_filter .ui-slider .ui-slider-range, .woocommerce-page .widget_price_filter .ui-slider .ui-slider-range, .woocommerce .widget_layered_nav_filters ul li a, .woocommerce-page .widget_layered_nav_filters ul li a, .woocommerce .widget_layered_nav ul li.chosen a, .woocommerce-page .widget_layered_nav ul li.chosen a, .cart-tab a.cart-parent span.contents, .woocommerce div.product form.cart .button, .woocommerce-page div.product form.cart .button, .woocommerce div.product .woocommerce-tabs ul.tabs li.active, .woocommerce #content div.product .woocommerce-tabs ul.tabs li.active, .woocommerce-page div.product .woocommerce-tabs ul.tabs li.active, .woocommerce-page #content div.product .woocommerce-tabs ul.tabs li.active, .woocommerce div.product .woocommerce-tabs ul.tabs li:hover, .woocommerce #content div.product .woocommerce-tabs ul.tabs li:hover, .woocommerce-page div.product .woocommerce-tabs ul.tabs li:hover, .woocommerce-page #content div.product .woocommerce-tabs ul.tabs li:hover, .woocommerce #review_form #respond .form-submit input, .woocommerce-page #review_form #respond .form-submit input, .woocommerce .cart .checkout-button.button, .woocommerce .cart input.checkout-button.button, .woocommerce-page .cart .checkout-button.button, .woocommerce-page .cart input.checkout-button.button,
.woocommerce .cart .button:hover, .woocommerce .cart input.button:hover, .woocommerce-page .cart .button:hover, .woocommerce-page .cart input.button:hover, .woocommerce .cart-collaterals .shipping_calculator .button, .woocommerce-page .cart-collaterals .shipping_calculator .button, .woocommerce a.button.alt, .woocommerce button.button.alt, .woocommerce input.button.alt, .woocommerce #respond input#submit.alt, .woocommerce #content input.button.alt, .woocommerce-page a.button.alt, .woocommerce-page button.button.alt, .woocommerce-page input.button.alt, .woocommerce-page #respond input#submit.alt, .woocommerce-page #content input.button.alt, .woocommerce a.button:hover, .woocommerce button.button:hover, .woocommerce input.button:hover, .woocommerce #respond input#submit:hover, .woocommerce #content input.button:hover, .woocommerce-page a.button:hover, .woocommerce-page button.button:hover, .woocommerce-page input.button:hover, .woocommerce-page #respond input#submit:hover, .woocommerce-page #content input.button:hover
{
	 background:<?php echo $smof_data['custom_color_scheme']; ?>;
}

.img-caption, .fancybox-title-float-wrap .child {
	background:rgba(<?php echo implode(",", $j_rgb_color); ?>,.88);
}

/* video & audio player */
.mejs-controls .mejs-time-rail .mejs-time-current, .mejs-controls .mejs-horizontal-volume-slider .mejs-horizontal-volume-current, .mejs-controls .mejs-volume-button .mejs-volume-slider .mejs-volume-current {
    background:<?php echo $smof_data['custom_color_scheme']; ?> !important;
}

/* Colors 
================================================== */
p a, a:hover, .latest-blog .item h3 a:hover, .tweet_list li i.icon-twitter, .tweet.footer .tweet_list li a:hover, footer a:hover, .button.normal:hover, .breadcrumbs a, span.color, .team .item span, .check-list.why li:before, .widget.most-posts ul.posts li a:hover, .blog-3 .item .post-meta a:hover, .post-tags a, .form-box label small, i.color, header.style-4 .navigation > ul > li > a:hover, header.style-4 .navigation > ul > li > a.selected, header.style-4 .navigation > ul > li.active > a, 
/* wp classes */
header.style-4 .navigation > ul > li.current-menu-item > a, header.style-4 .navigation > ul > li.current-menu-parent > a, header.style-4 .navigation > ul > li.current-menu-ancestor > a, .format-aside .post-meta .meta a:hover, .comment-reply-link, .comment-edit-link, .comment-awaiting-moderation, #cancel-comment-reply-link, .no-comments, .project-details li.cats a,
.check-list.list-color li:before, .plus-list.list-color li:before, .arrow-list.list-color li:before, .cross-list.list-color li:before, .star-list.list-color li:before, .minus-list.list-color li:before, .heart-list.list-color li:before, .circle-list.list-color li:before, .asterisk-list.list-color li:before, .square-list.list-color li:before, .post-content .latest-blog .item p a,
.sticky-wrapper.is-sticky .navigation > ul > li.current-menu-item > a, .sticky-wrapper.is-sticky .navigation > ul > li.current-menu-parent > a, .sticky-wrapper.is-sticky .navigation > ul > li.current-menu-ancestor > a, .sticky-wrapper.is-sticky .navigation > ul > li > a:hover,
header.bg-border-none .navigation > ul > li > a:hover, header.bg-border-none .navigation > ul > li > a.selected, header.bg-border-none .navigation > ul > li.active > a, header.bg-border-none .navigation > ul > li.current-menu-item > a, header.bg-border-none .navigation > ul > li.current-menu-parent > a, header.bg-border-none .navigation > ul > li.current-menu-ancestor > a, header.style-3.bg-border-none .navigation > ul > li > a:hover, header.style-3.bg-border-none .navigation > ul > li > a.selected, header.style-3.bg-border-none .navigation > ul > li.active > a, header.style-3.bg-border-none .navigation > ul > li.current-menu-item > a, header.style-3.bg-border-none .navigation > ul > li.current-menu-parent > a, header.style-3.bg-border-none .navigation > ul > li.current-menu-ancestor > a, table#wp-calendar td a, .woocommerce ul.products li.product .price, .woocommerce-page ul.products li.product .price, .woocommerce ul.products li.product a h3:hover, .woocommerce-page ul.products li.product a h3:hover, .woocommerce ul.cart_list li a, .woocommerce ul.product_list_widget li a, .woocommerce-page ul.cart_list li a, .woocommerce-page ul.product_list_widget li a, .woocommerce button.button:hover, .woocommerce-page button.button:hover, .woocommerce div.product p.price, .woocommerce-page div.product p.price, .woocommerce .product_meta a, .woocommerce div.product span.price, .woocommerce-page div.product span.price, .woocommerce .woocommerce-message a.button, .woocommerce-page .woocommerce-message a.button, .woocommerce .woocommerce-message a.button:hover, .woocommerce-page .woocommerce-message a.button:hover, .woocommerce .cart-collaterals .cart_totals table .order-total td .amount, .woocommerce-page .cart-collaterals .cart_totals table .order-total td .amount
{
	 color:<?php echo $smof_data['custom_color_scheme']; ?>;
}

.page-section .button.normal:hover, .page-section .button.normal:hover i {
   color:<?php echo $smof_data['custom_color_scheme']; ?> !important; 
}

/* borders
================================================== */
.navigation ul li a:hover, .navigation ul li a.selected, .navigation ul li.active a, 
/* wp classes */
.navigation ul li.current-menu-item a, .navigation ul li.current-menu-parent a, .navigation ul li.current-menu-ancestor a
{
	border-top:4px solid <?php echo $smof_data['custom_color_scheme']; ?>;
}

#horizontal-tabs ul.tabs li.current {
	border-top:2px solid <?php echo $smof_data['custom_color_scheme']; ?>;
}

#vertical-tabs ul.tabs li.current {
	border-left:2px solid <?php echo $smof_data['custom_color_scheme']; ?>;
}

body.rtl #vertical-tabs ul.tabs li.current {
	border-right:2px solid <?php echo $smof_data['custom_color_scheme']; ?>;
}

header.style-3 .navigation ul li a:hover, header.style-3 .navigation ul li a.selected, header.style-3 .navigation ul li.active a, 
/* wp classes */
header.style-3 .navigation ul li.current-menu-item a, header.style-3 .navigation ul li.current-menu-parent a, header.style-3 .navigation ul li.current-menu-ancestor a
{
	border-top:1px solid <?php echo $smof_data['custom_color_scheme']; ?>;
}

blockquote {
	border-left:4px solid <?php echo $smof_data['custom_color_scheme']; ?>;
}

body.rtl blockquote {
	border-right:4px solid <?php echo $smof_data['custom_color_scheme']; ?>;
}

<?php } ?>

<?php 
// Body Background Color     
if(!empty($smof_data['body_background_color'])) { ?>
/* Body Background Color */
body {
    background:<?php echo $smof_data['body_background_color']; ?> !important;
}
<?php } ?>

<?php 
// Header Main content Background Color 
if(!empty($smof_data['header_background_color'])) { ?>
/* Header Main content Background Color */
header .main-header, header .down-header {
    background:<?php echo $smof_data['header_background_color']; ?> !important;
}
<?php } ?>

<?php 
// Header Top Bar Background Color
if(!empty($smof_data['top_bar_header_background_color'])) { ?>
/* Header Top Bar Background Color */
.top-bar {
    background:<?php echo $smof_data['top_bar_header_background_color']; ?> !important;
}
<?php } ?>

<?php 
// Wrap Background Color
if(!empty($smof_data['wrap_background_color'])) { ?>
/* Wrap Background Color */
#wrap.boxed {
    background:<?php echo $smof_data['wrap_background_color']; ?>;
}
<?php } ?>

<?php 
// Separate Line Color in Body
if(!empty($smof_data['separate_line_color_in_body'])) { ?>
/* Separate Line Color in Body */
hr.line {
    border: solid <?php echo $smof_data['separate_line_color_in_body']; ?>;
    border-width: 1px 0 0;
    clear: both;
    height: 0;
}
<?php } ?>

<?php 
// Footer Background Color
if(!empty($smof_data['footer_background_color'])) { ?>
/* Footer Background Color */
.footer-top {
    background:<?php echo $smof_data['footer_background_color']; ?>;
}
<?php } ?>

<?php 
// Footer Down Bar Background Color
if(!empty($smof_data['footer_downbar_background_color'])) { ?>
/* Footer Down Bar Background Color */
.footer-down {
    background:<?php echo $smof_data['footer_downbar_background_color']; ?>;
}
<?php } ?>

<?php 
// Header Menu Links ( active & hover ) Background Color
if(!empty($smof_data['menu_links_header_background_color'])) { ?>
/* Header Menu Links ( active & hover ) Background Color */
.navigation ul li a:hover, .navigation ul li a.selected, .navigation ul li.active a, .navigation ul li.current-menu-item a, .navigation ul li.current-menu-parent a, .navigation ul li.current-menu-ancestor a {
    background:<?php echo $smof_data['menu_links_header_background_color']; ?>;
}
<?php } ?>

<?php 
// Header Menu style ( #2 & #4 ) Background Color
if(!empty($smof_data['menu_style35_header_background_color'])) { ?>
/* Header Menu style ( #2 & #4 ) Background Color */
header.style-3 .navigation ul li a:hover, header.style-3 .navigation ul li a.selected, header.style-3 .navigation ul li.active a, header.style-3 .navigation ul li.current-menu-item a, header.style-3 .navigation ul li.current-menu-parent a, header.style-3 .navigation ul li.current-menu-ancestor a {
    background:<?php echo $smof_data['menu_style35_header_background_color']; ?>;
}
<?php } ?>

<?php 
// Top Border for Header Menu style ( #2 & #4 ) Color
if(!empty($smof_data['topborder_menu_style35_header_color'])) { ?>
/* Top Border for Header Menu style ( #2 & #4 ) Color */
header.style-3 .down-header {
    border-top: 1px solid <?php echo $smof_data['topborder_menu_style35_header_color']; ?>;
}
<?php } ?>

<?php 
// Sublevels Menu links Background Color
if(!empty($smof_data['sublevels_menu_links_background_color'])) { ?>
/* Sublevels Menu links Background Color */
.navigation ul li ul li a, .responsive ul li a, .responsive > ul {
    background:<?php echo $smof_data['sublevels_menu_links_background_color']; ?> !important;
}
<?php } ?>

<?php 
// Sublevels Menu links ( active & hover ) Background Color
if(!empty($smof_data['sublevels_menu_links_hover_background_color'])) { ?>
/* Sublevels Menu links ( active & hover ) Background Color */
.navigation ul li ul li a:hover, .navigation ul > li > ul > li.active > a, .navigation ul > li > ul > li.current-menu-item > a, .navigation ul > li > ul > li.current-menu-parent > a, .navigation ul > li > ul > li.current-menu-ancestor > a, .responsive > ul > li.active > a, .responsive > ul > li > a:hover, 
.responsive > ul > li.active > ul > li.active > a, .responsive > ul > li > ul > li > a:hover, 
.responsive > ul > li.active > ul > li.active > ul > li.active > a, .responsive > ul > li > ul > li > ul > li > a:hover,
.responsive > ul > li.active > ul > li.active > ul > li.active > ul > li.active > a, .responsive > ul > li > ul > li > ul > li > ul > li > a:hover, .responsive > ul > li.active > ul > li.active > ul > li.active > ul > li.active > ul > li.active a, .responsive > ul > li > ul > li > ul > li > ul > li > ul > li > a:hover,
/* wp classes */
.responsive > ul > li.current-menu-item > a, .responsive > ul > li.current-menu-parent > a, .responsive > ul > li.current-menu-ancestor > a,
.responsive ul > li > ul > li.current-menu-item > a, .responsive ul > li > ul > li.current-menu-parent > a, .responsive ul > li > ul > li.current-menu-ancestor > a ,
.responsive ul > li > ul > li > ul > li.current-menu-item > a, .responsive ul > li > ul > li > ul > li.current-menu-parent > a, .responsive ul > li > ul > li > ul > li.current-menu-ancestor > a ,
.responsive ul > li > ul > li > ul > li > ul > li.current-menu-item > a, .responsive ul > li > ul > li > ul > li > ul > li.current-menu-parent > a, .responsive ul > li > ul > li > ul > li > ul > li.current-menu-ancestor > a,
.responsive ul > li > ul > li > ul > li > ul > li > ul > li.current-menu-item > a, .responsive ul > li > ul > li > ul > li > ul > li > ul > li.current-menu-parent > a, .responsive ul > li > ul > li > ul > li > ul > li > ul > li.current-menu-ancestor > a {
    background:<?php echo $smof_data['sublevels_menu_links_hover_background_color']; ?> !important;
}
<?php } ?>

<?php 
// Sublevels Menu links Background Color
if(!empty($smof_data['separate_sublevels_menu_color'])) { ?>
/* Sublevels Menu links Background Color */
.navigation ul li ul li a, .responsive ul li a, .navigation ul li ul li a:hover, .navigation ul li ul li.active a,
.navigation ul li ul li.current-menu-item a, .navigation ul li ul li.current-menu-parent a, .navigation ul li ul li.current-menu-ancestor a
{
	border-bottom:1px solid <?php echo $smof_data['separate_sublevels_menu_color']; ?>;
}
<?php } ?>

<?php 
// Menu Links Font Color
if(!empty($smof_data['menu_links_header_font_color'])) { ?>
/* Menu Links Font Color */
.navigation ul li a {
	 color:<?php echo $smof_data['menu_links_header_font_color']; ?>;
}
<?php } ?>

<?php 
// Menu Links ( active & hover ) Font Color
if(!empty($smof_data['menu_links_active_header_font_color'])) { ?>
/* Menu Links ( active & hover ) Font Color */
.navigation > ul > li > a:hover, .navigation > ul > li > a.selected, .navigation > ul > li.active > a, .navigation > ul > li.current-menu-item > a, .navigation > ul > li.current-menu-parent > a, .navigation > ul > li.current-menu-ancestor > a {
	 color:<?php echo $smof_data['menu_links_active_header_font_color']; ?>;
}
<?php } ?>

<?php 
// Sublevels Menu Links Font Color
if(!empty($smof_data['sublevels_menu_links_header_font_color'])) { ?>
/* Sublevels Menu Links Font Color */
.navigation ul li ul li a, .responsive ul a {
	 color:<?php echo $smof_data['sublevels_menu_links_header_font_color']; ?>;
}
<?php } ?>

<?php 
// Sublevels Menu Links Font Color
if(!empty($smof_data['sublevels_menu_links_active_header_font_color'])) { ?>
/* Sublevels Menu Links Font Color */
.navigation ul li ul li a:hover, .responsive ul a:hover, .navigation ul li ul li a:hover, .navigation ul > li > ul > li.active > a, .navigation ul > li > ul > li.current-menu-item > a, .navigation ul > li > ul > li.current-menu-parent > a, .navigation ul > li > ul > li.current-menu-ancestor > a, .responsive > ul > li.active > a, .responsive > ul > li > a:hover, 
.responsive > ul > li.active > ul > li.active > a, .responsive > ul > li > ul > li > a:hover, 
.responsive > ul > li.active > ul > li.active > ul > li.active > a, .responsive > ul > li > ul > li > ul > li > a:hover,
.responsive > ul > li.active > ul > li.active > ul > li.active > ul > li.active > a, .responsive > ul > li > ul > li > ul > li > ul > li > a:hover, .responsive > ul > li.active > ul > li.active > ul > li.active > ul > li.active > ul > li.active a, .responsive > ul > li > ul > li > ul > li > ul > li > ul > li > a:hover,
/* wp classes */
.responsive > ul > li.current-menu-item > a, .responsive > ul > li.current-menu-parent > a, .responsive > ul > li.current-menu-ancestor > a,
.responsive ul > li > ul > li.current-menu-item > a, .responsive ul > li > ul > li.current-menu-parent > a, .responsive ul > li > ul > li.current-menu-ancestor > a ,
.responsive ul > li > ul > li > ul > li.current-menu-item > a, .responsive ul > li > ul > li > ul > li.current-menu-parent > a, .responsive ul > li > ul > li > ul > li.current-menu-ancestor > a ,
.responsive ul > li > ul > li > ul > li > ul > li.current-menu-item > a, .responsive ul > li > ul > li > ul > li > ul > li.current-menu-parent > a, .responsive ul > li > ul > li > ul > li > ul > li.current-menu-ancestor > a,
.responsive ul > li > ul > li > ul > li > ul > li > ul > li.current-menu-item > a, .responsive ul > li > ul > li > ul > li > ul > li > ul > li.current-menu-parent > a, .responsive ul > li > ul > li > ul > li > ul > li > ul > li.current-menu-ancestor > a {
	 color:<?php echo $smof_data['sublevels_menu_links_active_header_font_color']; ?>;
}
<?php } ?>


<?php } // end check custom_design_colors ?>


<?php if($smof_data['using_standard_fonts'] == 1) { ?>

<?php 
// Body Font [Type]
if($smof_data['body_font_type_standard'] != 'none') { ?>
/* Body Font [Type] */
body, .form-elements input, .form-elements textarea, .form-elements select, .qoute p, .tables-column .header h3 span.pop, .widget .followers li span, .one-page.welcome .welcome-text, .post-content .latest-blog .item h3, .team-shortcode .item span, 
.latest-blog .item p, footer, p, .services.style-1 .item h3, .progress-bar h5, .button, #horizontal-tabs ul.tabs li, #vertical-tabs ul.tabs li, .alert p, .pagination, .qoute, .welcome, .progress-bar .meter, .testimonial-item .author span, .testimonial-item .author a, .team .item h4, .services.style-2 .item h4, .services.style-3 .item h4, .page-404 .oops, .tables-column .header h3, .tables-column .header .price, .tables-column .footer h4, .square-list li, .check-list li, .plus-list li, .arrow-list li, .cross-list li, .star-list li, .minus-list li, .heart-list li, .circle-list li, .asterisk-list li, .widget .followers h4, .sidebar .widget, .blog-3 .item a.title, .form-box label, .address-info li, .phone-info li, .email-info li, body.under-construction .content h2.title, body.under-construction .content .block .label, .post-content, .project-details li, 
.subscribe input.submit, body.under-construction .content .block .flip-top, body.under-construction .content input.subscribe-button, 
.img-caption .desc span, .more,
.subscribe input.mail, blockquote, blockquote p, .about-author h3, .related-posts h3, .comments-box h3, .comment-form h3, .form-box input.text, .form-box textarea, body.under-construction .content p.text, body.under-construction .content input.subscribe-text, .notification_error, .notification_ok, .fancybox-title-inside-wrap, .fancybox-title-outside-wrap, .single-gallery .item.gallery-item .img-caption-gallery .desc, 
.page-title h1, .one-page p.description, /* wp classes */ .post-content .wp-caption p, .page-section .post-content > p, .page-section .post-content .shortcode-column > p,
.pricing-tables .tables-column, .single-gallery-meta, .wpcf7-submit, .woocommerce-ordering select {
	 font-family:<?php echo $smof_data['body_font_type_standard']; ?> !important;
}
<?php } ?>

<?php 
// Menu Font [Type]
if($smof_data['menu_font_type_standard'] != 'none') { ?>
/* Menu Font [Type] */
.navigation ul li, .responsive ul li {
	 font-family:<?php echo $smof_data['menu_font_type_standard']; ?> !important;
}
<?php } ?>

<?php 
// Top Bar header Font [Type]
if($smof_data['topbar_font_type_standard'] != 'none') { ?>
/* Top Bar header Font [Type] */
.top-bar {
	 font-family:<?php echo $smof_data['topbar_font_type_standard']; ?> !important;
}
<?php } ?>

<?php 
// Heading Font [Type]
if($smof_data['heading_font_type_standard'] != 'none') { ?>
/* Heading Font [Type] */
h3.title, .about-author .content .data h5, .comments .comment-box h5, .one-page h2.title, .one-page h1.title, .page-section h1, .page-section h2, .page-section h3 {
	 font-family:<?php echo $smof_data['heading_font_type_standard']; ?> !important;
}
<?php } ?>

<?php 
// Heading Footer Font [Type]
if($smof_data['heading_footer_font_type_standard'] != 'none') { ?>
/* Heading Footer Font [Type] */
footer h3.title {
	 font-family:<?php echo $smof_data['heading_footer_font_type_standard']; ?> !important;
}
<?php } ?>

<?php 
// Down Bar Font [Type]
if($smof_data['downbar_font_type_standard'] != 'none') { ?>
/* Down Bar Font [Type] */
footer .footer-down {
	 font-family:<?php echo $smof_data['downbar_font_type_standard']; ?> !important;
}
<?php } ?>

<?php 
// Sliders Title Font [Type]
if($smof_data['title_sliders_font_type_standard'] != 'none') { ?>
/* Sliders Title Font [Type] */
.slider-1 .slides li h2, .slider-2 .slides li h2 {
	 font-family:<?php echo $smof_data['title_sliders_font_type_standard']; ?> !important;
}
<?php } ?>

<?php 
// Sliders Description Font [Type]
if($smof_data['desc_sliders_font_type_standard'] != 'none') { ?>
/* Sliders Description Font [Type] */
.slider-1 .slides li .slide-desc, .slider-2 .slides li .slide-desc {
	 font-family:<?php echo $smof_data['desc_sliders_font_type_standard']; ?> !important;
}
<?php } ?>


<?php } // end check using_standard_fonts ?>


<?php if($smof_data['using_google_fonts'] == 1) { ?>

<?php 
// Body Font [Type]
if($smof_data['body_font_type'] != 'none' && $smof_data['body_font_type'] != 'ar') { ?>
/* Body Font [Type] */
body, .form-elements input, .form-elements textarea, .form-elements select, .qoute p, .tables-column .header h3 span.pop, .widget .followers li span, .one-page.welcome .welcome-text, .post-content .latest-blog .item h3, .team-shortcode .item span, 
.latest-blog .item p, footer, p, .services.style-1 .item h3, .progress-bar h5, .button, #horizontal-tabs ul.tabs li, #vertical-tabs ul.tabs li, .alert p, .pagination, .qoute, .welcome, .progress-bar .meter, .testimonial-item .author span, .testimonial-item .author a, .team .item h4, .services.style-2 .item h4, .services.style-3 .item h4, .page-404 .oops, .tables-column .header h3, .tables-column .header .price, .tables-column .footer h4, .square-list li, .check-list li, .plus-list li, .arrow-list li, .cross-list li, .star-list li, .minus-list li, .heart-list li, .circle-list li, .asterisk-list li, .widget .followers h4, .sidebar .widget, .blog-3 .item a.title, .form-box label, .address-info li, .phone-info li, .email-info li, body.under-construction .content h2.title, body.under-construction .content .block .label, .post-content, .project-details li, 
.subscribe input.submit, body.under-construction .content .block .flip-top, body.under-construction .content input.subscribe-button, 
.img-caption .desc span, .more,
.subscribe input.mail, blockquote, blockquote p, .about-author h3, .related-posts h3, .comments-box h3, .comment-form h3, .form-box input.text, .form-box textarea, body.under-construction .content p.text, body.under-construction .content input.subscribe-text, .notification_error, .notification_ok, .fancybox-title-inside-wrap, .fancybox-title-outside-wrap, .single-gallery .item.gallery-item .img-caption-gallery .desc, 
.page-title h1, .one-page p.description, /* wp classes */ .post-content .wp-caption p, .page-section .post-content > p, .page-section .post-content .shortcode-column > p,
.pricing-tables .tables-column, .single-gallery-meta, .wpcf7-submit, .woocommerce-ordering select {
	 font-family:'<?php echo $smof_data['body_font_type']; ?>', Arial, sans-serif !important;
}
<?php } ?>

<?php 
// Menu Font [Type]
if($smof_data['menu_font_type'] != 'none' && $smof_data['menu_font_type'] != 'ar') { ?>
/* Menu Font [Type] */
.navigation ul li, .responsive ul li {
	 font-family:'<?php echo $smof_data['menu_font_type']; ?>', Arial, sans-serif !important;
}
<?php } ?>

<?php 
// Top Bar header Font [Type]
if($smof_data['topbar_font_type'] != 'none' && $smof_data['topbar_font_type'] != 'ar') { ?>
/* Top Bar header Font [Type] */
.top-bar {
	 font-family:'<?php echo $smof_data['topbar_font_type']; ?>', Arial, sans-serif !important;
}
<?php } ?>

<?php 
// Heading Font [Type]
if($smof_data['heading_font_type'] != 'none' && $smof_data['heading_font_type'] != 'ar') { ?>
/* Heading Font [Type] */
h3.title, .about-author .content .data h5, .comments .comment-box h5, .one-page h2.title, .one-page h1.title, .page-section h1, .page-section h2, .page-section h3 {
	 font-family:'<?php echo $smof_data['heading_font_type']; ?>', Arial, sans-serif !important;
}
<?php } ?>

<?php 
// Heading Footer Font [Type]
if($smof_data['heading_footer_font_type'] != 'none' && $smof_data['heading_footer_font_type'] != 'ar') { ?>
/* Heading Footer Font [Type] */
footer h3.title {
	 font-family:'<?php echo $smof_data['heading_footer_font_type']; ?>', Arial, sans-serif !important;
}
<?php } ?>

<?php 
// Down Bar Font [Type]
if($smof_data['downbar_font_type'] != 'none' && $smof_data['downbar_font_type'] != 'ar') { ?>
/* Down Bar Font [Type] */
footer .footer-down {
	 font-family:'<?php echo $smof_data['downbar_font_type']; ?>', Arial, sans-serif !important;
}
<?php } ?>

<?php 
// Sliders Title Font [Type]
if($smof_data['title_sliders_font_type'] != 'none' && $smof_data['title_sliders_font_type'] != 'ar') { ?>
/* Sliders Title Font [Type] */
.slider-1 .slides li h2, .slider-2 .slides li h2 {
	 font-family:'<?php echo $smof_data['title_sliders_font_type']; ?>', Arial, sans-serif !important;
}
<?php } ?>

<?php 
// Sliders Description Font [Type]
if($smof_data['desc_sliders_font_type'] != 'none' && $smof_data['desc_sliders_font_type'] != 'ar') { ?>
/* Sliders Description Font [Type] */
.slider-1 .slides li .slide-desc, .slider-2 .slides li .slide-desc {
	 font-family:'<?php echo $smof_data['desc_sliders_font_type']; ?>', Arial, sans-serif !important;
}
<?php } ?>


<?php } // end check using_google_fonts ?>


<?php if($smof_data['update_font_sizes_styles'] == 1) { ?>

/* H1 Heading Font */
h1 { font-size: <?php echo $smof_data['h1_font_sizes_styles']['size']; ?> !important; font-weight: <?php echo $smof_data['h1_font_sizes_styles']['style']; ?>; }
/* H2 Heading Font */
h2 { font-size: <?php echo $smof_data['h2_font_sizes_styles']['size']; ?> !important; font-weight: <?php echo $smof_data['h2_font_sizes_styles']['style']; ?>; }
/* H3 Heading Font */
h3 { font-size: <?php echo $smof_data['h3_font_sizes_styles']['size']; ?> !important; font-weight: <?php echo $smof_data['h3_font_sizes_styles']['style']; ?>; }
/* H4 Heading Font */
h4 { font-size: <?php echo $smof_data['h4_font_sizes_styles']['size']; ?> !important; font-weight: <?php echo $smof_data['h4_font_sizes_styles']['style']; ?>; }
/* H5 Heading Font */
h5 { font-size: <?php echo $smof_data['h5_font_sizes_styles']['size']; ?> !important; font-weight: <?php echo $smof_data['h5_font_sizes_styles']['style']; ?>; }
/* H6 Heading Font */
h6 { font-size: <?php echo $smof_data['h6_font_sizes_styles']['size']; ?> !important; font-weight: <?php echo $smof_data['h6_font_sizes_styles']['style']; ?>; }

/* Body Font */
body, .team .item span, p {
	font-size: <?php echo $smof_data['body_font_sizes_styles']['size']; ?> !important;
    font-weight: <?php echo $smof_data['body_font_sizes_styles']['style']; ?>;
}

/* Header Menu Font */
.navigation ul li, .responsive ul li {
	font-size: <?php echo $smof_data['menu_font_sizes_styles']['size']; ?> !important;
    font-weight: <?php echo $smof_data['menu_font_sizes_styles']['style']; ?>;
}

/* Sublevels Menu Font */
.navigation ul li ul li, .responsive ul li ul li {
	font-size: <?php echo $smof_data['sublevels_menu_font_sizes_styles']['size']; ?> !important;
    font-weight: <?php echo $smof_data['sublevels_menu_font_sizes_styles']['style']; ?>;
}

/* Top Bar Header Font */
.top-bar {
	font-size: <?php echo $smof_data['topbar_header_font_sizes_styles']['size']; ?> !important;
    font-weight: <?php echo $smof_data['topbar_header_font_sizes_styles']['style']; ?>;
}

/* Slider [1] Font */
.slider-1 .slides li h2 {
	font-size: <?php echo $smof_data['slider1_font_sizes_styles']['size']; ?> !important;
    font-weight: <?php echo $smof_data['slider1_font_sizes_styles']['style']; ?>;
}

/* Slider [2] Font */
.slider-2 .slides li h2 span {
	font-size: <?php echo $smof_data['slider2_font_sizes_styles']['size']; ?> !important;
    font-weight: <?php echo $smof_data['slider2_font_sizes_styles']['style']; ?>;
}

/* Sliders Description Font */
.slider-1 .slides li .slide-desc, .slider-2 .slides li .slide-desc {
	font-size: <?php echo $smof_data['desc_sliders_font_sizes_styles']['size']; ?> !important;
    font-weight: <?php echo $smof_data['desc_sliders_font_sizes_styles']['style']; ?>;
}

/* Page Title Font */
.page-title h1 {
	font-size: <?php echo $smof_data['page_title_font_sizes_styles']['size']; ?> !important;
    font-weight: <?php echo $smof_data['page_title_font_sizes_styles']['style']; ?>;
}

/* Heading Body Font */
h3.title, .one-page h2.title, .one-page h1.title, .page-section h1, .page-section h2, .page-section h3 {
	font-size: <?php echo $smof_data['heading_body_font_sizes_styles']['size']; ?> !important;
    font-weight: <?php echo $smof_data['heading_body_font_sizes_styles']['style']; ?>;
}

/* Footer Font */
footer, .tweet.footer, footer p {
	font-size: <?php echo $smof_data['footer_font_sizes_styles']['size']; ?> !important;
    font-weight: <?php echo $smof_data['footer_font_sizes_styles']['style']; ?>;
}

/* Heading Footer Font */
footer h3.title {
	font-size: <?php echo $smof_data['heading_footer_font_sizes_styles']['size']; ?> !important;
    font-weight: <?php echo $smof_data['heading_footer_font_sizes_styles']['style']; ?>;
}

/* Down Bar footer Font */
footer .footer-down, span.copyright, footer .footer-down p {
	font-size: <?php echo $smof_data['downbar_footer_font_sizes_styles']['size']; ?> !important;
    font-weight: <?php echo $smof_data['downbar_footer_font_sizes_styles']['style']; ?>;
}


<?php } // end check update_font_sizes_styles ?>


<?php if($smof_data['update_font_lineheights'] == 1) { ?>

/* Body Font [ Line Height ] */
p, blockquote, .post-content {
	line-height: <?php echo $smof_data['body_font_lineheights']; ?> !important;
}

/* H1 - Heading Font [ Line Height ] */
h1 { line-height: <?php echo $smof_data['h1_font_lineheights']; ?> !important; }
/* H2 - Heading Font [ Line Height ] */
h2 { line-height: <?php echo $smof_data['h2_font_lineheights']; ?> !important; }
/* H3 - Heading Font [ Line Height ] */
h3 { line-height: <?php echo $smof_data['h3_font_lineheights']; ?> !important; }
/* H4 - Heading Font [ Line Height ] */
h4 { line-height: <?php echo $smof_data['h4_font_lineheights']; ?> !important; }
/* H5 - Heading Font [ Line Height ] */
h5 { line-height: <?php echo $smof_data['h5_font_lineheights']; ?> !important; }
/* H6 - Heading Font [ Line Height ] */
h6 { line-height: <?php echo $smof_data['h6_font_lineheights']; ?> !important; }


<?php } // end check update_font_lineheights ?>


<?php 
#=============================================================#
# Custom CSS Codes
#=============================================================#

if($smof_data['custom_css'] == 1) { 
    
    // Normal Desktop
    if(!empty($smof_data['custom_css_code'])) { 
        echo "/* Start Custom CSS Codes */\n". $smof_data['custom_css_code'] ."\n \n"; 
    } 
       
    // Tablets Landscape [ 1024x768 ]
    if(!empty($smof_data['tablets_landscape_custom_css_code'])) { 
        echo "/* Smaller than standard 1024 (devices and browsers) */\n@media only screen and (max-width: 1024px) {\n\n". $smof_data['tablets_landscape_custom_css_code'] . "\n\n}\n\n";
    }
       
    // Tablets Portrait [ 768x1024 ]
    if(!empty($smof_data['tablets_portrait_custom_css_code'])) { 
        echo "/* Tablet Portrait size to standard 960 (devices and browsers) */\n@media only screen and (min-width: 768px) and (max-width: 959px) {\n\n". $smof_data['tablets_portrait_custom_css_code'] . "\n\n}\n\n"; 
    } 
       
    // Mobiles Landscape [ 480x320 ]
    if(!empty($smof_data['mobiles_landscape_custom_css_code'])) { 
        echo "/* Mobile Landscape Size to Tablet Portrait (devices and browsers) */\n@media only screen and (min-width: 480px) and (max-width: 767px) {\n\n".                $smof_data['mobiles_landscape_custom_css_code'] . "\n\n}\n\n";
    } 
       
    // Mobiles Portrait [ 320x480 ]
    if(!empty($smof_data['mobiles_portrait_custom_css_code'])) { 
        echo "/* Mobile Portrait Size to Mobile Landscape Size (devices and browsers) */\n@media only screen and (max-width: 479px) {\n\n". $smof_data['mobiles_portrait_custom_css_code'] . "\n\n}"; 
    } 

}

// new codes for header style 3
if($smof_data['responsive_design'] == 1) { ?>

/* new codes */
@media only screen and (min-width: 959px) {
    
header.style-3 .main-header .container .eleven.columns , header.style-3 .main-header .container .five.columns,
header .main-header .container .one-third.column, header .main-header .container .two-thirds.column,
header .top-bar .container .eight.columns, footer .footer-down .container .eight.columns {
    width:auto;
}

header.style-3 .main-header .container .eleven.columns, header .main-header .container .one-third.column,
header .top-bar .container .eight.columns.left, footer .footer-down .container .eight.columns.left {
    float:left;
}

header.style-3 .main-header .container .five.columns, header .main-header .container .two-thirds.column,
header .top-bar .container .eight.columns.right, footer .footer-down .container .eight.columns.right {
    float:right;
}

/* RTL */
body.rtl header.style-3 .main-header .container .eleven.columns, body.rtl header .main-header .container .one-third.column,
body.rtl header .top-bar .container .eight.columns.left, body.rtl footer .footer-down .container .eight.columns.left {
    float:right;
}

body.rtl header.style-3 .main-header .container .five.columns, body.rtl header .main-header .container .two-thirds.column,
body.rtl header .top-bar .container .eight.columns.right, body.rtl footer .footer-down .container .eight.columns.right {
    float:left;
}

header .search form {
    min-width: 280px;
}

header .widget .text-search {
    width: 83%;
}
    
}

@media only screen and (min-width: 768px) and (max-width: 959px) {
    
header.style-3 .main-header .container .eleven.columns , header.style-3 .main-header .container .five.columns,
header .top-bar .container .eight.columns, footer .footer-down .container .eight.columns {
    width:auto;
}

header.style-3 .main-header .container .eleven.columns,
header .top-bar .container .eight.columns.left, footer .footer-down .container .eight.columns.left {
    float:left;
}

header.style-3 .main-header .container .five.columns,
header .top-bar .container .eight.columns.right, footer .footer-down .container .eight.columns.right {
    float:right;
}

/* RTL */
body.rtl header.style-3 .main-header .container .eleven.columns,
body.rtl header .top-bar .container .eight.columns.left, body.rtl footer .footer-down .container .eight.columns.left {
    float:right;
}

body.rtl header.style-3 .main-header .container .five.columns,
body.rtl header .top-bar .container .eight.columns.right, body.rtl footer .footer-down .container .eight.columns.right {
    float:left;
}

header .search form {
    min-width: 220px;
}

header .widget .text-search {
    width: 81%;
}
    
}
    
<?php } else { ?>

header.style-3 .main-header .container .eleven.columns , header.style-3 .main-header .container .five.columns,
header .main-header .container .one-third.column, header .main-header .container .two-thirds.column,
header .top-bar .container .eight.columns, footer .footer-down .container .eight.columns {
    width:auto;
}

header.style-3 .main-header .container .eleven.columns, header .main-header .container .one-third.column,
header .top-bar .container .eight.columns.left, footer .footer-down .container .eight.columns.left {
    float:left;
}

header.style-3 .main-header .container .five.columns, header .main-header .container .two-thirds.column,
header .top-bar .container .eight.columns.right, footer .footer-down .container .eight.columns.right {
    float:right;
}

/* RTL */
body.rtl header.style-3 .main-header .container .eleven.columns, body.rtl header .main-header .container .one-third.column,
body.rtl header .top-bar .container .eight.columns.left, body.rtl footer .footer-down .container .eight.columns.left {
    float:right;
}

body.rtl header.style-3 .main-header .container .five.columns, body.rtl header .main-header .container .two-thirds.column,
body.rtl header .top-bar .container .eight.columns.right, body.rtl footer .footer-down .container .eight.columns.right {
    float:left;
}

header .search form {
    min-width: 280px;
}

header .widget .text-search {
    width: 83%;
}

<?php }

?>