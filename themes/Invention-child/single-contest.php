<?php 
global $post, $user_ID;
get_header(); ?>
<!-- Start main content -->
<div class="container main-content clearfix">

	<div id="post-<?php echo $post->ID; ?>" <?php post_class('single-project'); ?>>

		<?php echo $post->ID; ?>

		<?php echo $user_ID; ?>

		<?php while ( have_posts() ) : the_post(); ?>
			<?php the_content(); ?>
		<?php endwhile; ?>

		<?php if( $user_ID ): ?>

			<!-- <form id="contest" method="post" action="">
				<label>
					<input type="checkbox" id="contest-check">
					<span>
						Участвовать в конкурсе
					</span>
					<br>
					<input type="hidden" id="contest-info" data-contest="<?php // echo $post->ID; ?>" data-member="<?php // echo $user_ID; ?>">
					<input type="submit" value="Отправить заявку">
				</label>
			</form> -->

		<?php endif; ?>

	</div>
</div><!-- END main content -->
<?php
get_footer();