<?php 
	$usr_id = $_GET['userid'];
	$ref_id = $_GET['refid'];
?>
<h3>Регистрацияя</h3>
<form id="c_registerform" action="<?php echo get_template_directory_uri() . '/handler/reg-handler.php'; ?>" method="post">
	<p>
		<label for="user_login">
			Имя пользователя<br>
			<input type="text" name="user_login" id="c_user_login" class="input" size="20" style="">
		</label>
		<p class="error-login" style="color:coral;"></p>
	</p>
	<p>
		<label for="user_email">
			E-mail<br>
			<input type="text" name="user_email" id="user_email" class="input" size="25">
		</label>
		<p class="error-email" style="color:coral;"></p>
	</p>
	<p>
		<label for="user_psw">
			Пароль<br>
			<input type="password" name="user_psw" id="user_psw" class="input" size="25">
		</label>
		<p class="error-psw" style="color:coral;"></p>
	</p>

	<p id="reg_passmail">Подтверждение регистрации будет отправлено на ваш e-mail.</p>
	<p class="reg-suc" style="color:forestgreen;"></p>

	<br class="clear">
	<input type="hidden" name="redirect_to" value="<?php echo get_permalink( 193 ); ?>">
	<input type="hidden" name="userid" id="userid" value="<?php echo $usr_id ;?>">
	<input type="hidden" name="refid" id="refid" value="<?php echo $ref_id ;?>">

	<p class="submit"><input type="submit" name="c_wp-submit" id="c_wp-submit" class="button button-primary button-large" value="Регистрация"></p>
</form>