<?php /*Template Name: Cabinet Page*/
global $user_ID;
if(!$user_ID){
	header('location:' . site_url());
	exit;
} else {
	// данные текущего пользователя
	$user_data = get_user_by( 'id', $user_ID );
	$user_mail = $user_data->user_email;
	$user_name = $user_data->display_name;
	$user_nicename = $user_data->user_nicename;
	$user_name_dis = $user_data->display_name;
	$user_curr = get_user_meta( $user_ID, 'user_curr', true ); // личный счет

	// значение рефера
	$referrer = get_user_meta( $user_ID, 'user_refer_master', true );
	$referrer_data = get_user_by( 'id', $referrer );

	// массив рефералов
	$ref_array = get_user_meta( $user_ID, 'user_refer', true );

	// массив событий на которые подписан
	$user_conts_array = get_user_meta( $user_ID, 'user_conts', true );
}

$role = $user_data->roles[0]; // administrator OR subscriber
get_header(); 

?>

<!-- Start main content -->
<div class="container main-content clearfix">
	<!-- Start Page Content -->
	<div class="sixteen columns full-width">

		<div class="personal-area">

			<div class="left-block">
				<ul>
					<li><b>Ник:</b> <?php echo $user_nicename; ?></li>
					<li><b>Ваша реферальная ссылка:</b> <?php echo home_url(); ?>/?userid=<?php echo $user_ID; ?>&refid=21<?php echo $user_ID; ?></li>
					<li><b>Ваши рефералы:</b> 
					<?php 
						foreach ( $ref_array as $val ):
							$ref_user_data = get_user_by( 'id', $val );
							if( $ref_user_data ):
					?>
							<span><?php echo $ref_user_data->user_nicename; ?> </span>
					<?php
							endif;
						endforeach;
					?>
					</li>
					<li><b>Ваш рефер:</b> <?php echo $referrer_data->user_nicename; ?></li>
					<li><b>Ваш счет:</b> <?php echo $user_curr; ?></li>
				</ul>
			</div>

			<div class="right-block ref-form">
				<ul class="nav nav-tabs">
					<li class="active">
						<a href="#order-refback" data-toggle="tab">Заказать рефбек</a>
					</li>
					<li>
						<a href="#order-insurance" data-toggle="tab">Заказ страховки</a>
					</li>
				</ul>
				<div class="tab-content">
					<div class="tab-pane" id="order-prize-money">
						<form id="form_debit" method="post" action="">
							<input class="type_form" id="d_debit_type" type="hidden" name="debit_type" value="debit">
							<div class="input">
								<label>Название проекта, в который был сделан вклад<span style="color:coral;">*</span></label>
								<input type="text" id="d_user_proj" value="" required>
								<p class="error-proj" style="color:coral;"></p>
							</div>

							<div class="input">
								<label>Ваш логин в проекте<span style="color:coral;">*</span></label>
								<input type="text" id="d_proj_login" value="" required>
								<p class="error-proj_login" style="color:coral;"></p>
							</div>

							<div class="input">
								<label>Ваш E-mail в проекте<span style="color:coral;">*</span></label>
								<input type="text" id="d_user_mail" value="" required>
								<p class="error-mail" style="color:coral;"></p>
							</div>

							<div class="input" style="display:none;">
								<label>Ваш ник<span style="color:coral;">*</span></label>
								<input type="text" id="d_user_nic" value="<?php echo $user_nicename; ?>">
								<input type="hidden" id="d_user_id" value="<?php echo $user_ID; ?>">
								<p class="error-nic" style="color:coral;"></p>
							</div>

							<div class="input check-input check-input_radio">
								<label>Выберете платежную систему<span style="color:coral;">*</span></label>
								<input type="radio" id="d_pay_type1" name="d_pay_type" value="perfect_money" checked>
									<label for="d_pay_type1">Perfect Money</label>
								<input type="radio" id="d_pay_type2" name="d_pay_type" value="payeer">
									<label for="d_pay_type2">Payeer</label>
								<input type="radio" id="d_pay_type3" name="d_pay_type" value="advcash">
									<label for="d_pay_type3">AdvCash</label>

								<p class="error-user_pay_type" style="color:coral;"></p>
							</div>

							<div class="input">
								<label>Укажите реквизиты электронного кошелька для выплаты призовых<span style="color:coral;">*</span></label>
								<span style="color:coral;font-size:13px;text-align:left;display:inline-block;">для AdvCash введите кошелек в формате U123456789123</span>
								<input type="text" id="d_user_wall" value="" required>
								<p class="error-user_wall" style="color:coral;"></p>
							</div>

							<div class="input" style="display:none;">
								<label>Укажите сумму вклада (USD)<span style="color:coral;">*</span></label>
								<input type="number" id="d_user_deb" min="0" name="debit">
								<p class="error-user_deb" style="color:coral;"></p>
							</div>

							<div class="input">
								<label>Укажите сумму выплаты (сумму рефбека) (USD)  <span style="color:coral;">*</span></label>
								<span style="color:coral;font-size:13px;text-align:left;display:inline-block;width:100%;">разделитель запятая, пример 21,5</span>
								<input type="number" id="d_user_curr" min="0" step="0.01" name="currency" required>
								<p class="error-user_curr" style="color:coral;"></p>
							</div>

							<div class="input" style="display:none;">
								<label>Укажите дату, когда был внесён депозит<span style="color:coral;">*</span></label>
								<input type="text" id="d_user_deb_dat" min="0" name="deb_dat">
								<p class="error-user_deb_dat" style="color:coral;"></p>
							</div>

							<div class="input" style="display:none;">
								<label>Укажите выбранный инвестиционный план</label>
								<input type="text" id="d_user_deb_plan" name="deb_plan">
								<p class="error-deb_plan" style="color:coral;"></p>
							</div>

							<div class="input">
								<label>Комментарий</label>
								<textarea name="user_text" id="d_user_text" cols="40" rows="5" style="resize:none;max-width:100%;"></textarea>
								<p class="error-text" style="color:coral;"></p>
							</div>
							<p class="deb-suc" style="color:forestgreen;"></p>
							<input type="submit" value="Отправить">
						</form>
					</div>

					<div class="tab-pane active" id="order-refback">
						<form id="form_bonus" method="post" action="">
							<input class="type_form" id="bonus_type" type="hidden" name="bonus_type" value="bonus">
							<div class="input" style="display:none;">
								<label>Ваш ник<span style="color:coral;">*</span></label>
								<input type="text" id="user_nic" value="<?php echo $user_nicename; ?>">
								<input type="hidden" id="user_id" value="<?php echo $user_ID; ?>">
								<p class="error-nic" style="color:coral;"></p>
							</div>
							<div class="input" style="display:none;">
								<label>Ваш E-mail</label>
								<input type="text" id="user_mail" value="<?php echo $user_mail; ?>">
								<p class="error-mail" style="color:coral;"></p>
							</div>
							<div class="input">
								<label>Название проекта, в который был сделан вклад<span style="color:coral;">*</span></label>
								<input type="text" id="user_proj" value="" required>
								<p class="error-proj" style="color:coral;"></p>
							</div>

							<div class="input">
								<label>Ваш логин в проекте<span style="color:coral;">*</span></label>
								<input type="text" id="proj_login" value="" required>
								<p class="error-proj_login" style="color:coral;"></p>
							</div>

							<div class="input">
								<label>Ваш E-mail в проекте<span style="color:coral;">*</span></label>
								<input type="text" id="proj_mail" value="" required>
								<p class="error-proj_mail" style="color:coral;"></p>
							</div>

							<div class="input check-input check-input_radio">
								<label>Выберете платежную систему<span style="color:coral;">*</span></label>

								<input type="radio" id="pay_type1" name="pay_type" value="perfect_money" checked>
									<label for="pay_type1">Perfect Money</label>
								<input type="radio" id="pay_type2" name="pay_type" value="payeer">
									<label for="pay_type2">Payeer</label>
								<input type="radio" id="pay_type3" name="pay_type" value="advcash">
									<label for="pay_type3">AdvCash</label>

								<p class="error-user_pay_type" style="color:coral;"></p>
							</div>

							<div class="input">
								<label>Укажите реквизиты электронного кошелька для выплаты рефбека<span style="color:coral;">*</span></label>
								<span style="color:coral;font-size:13px;text-align:left;display:inline-block;">для AdvCash введите кошелек в формате U123456789123</span>
								<input type="text" id="user_wall" value="" required>
								<p class="error-user_wall" style="color:coral;"></p>
							</div>

							<div class="input">
								<label>Укажите сумму вклада (USD)<span style="color:coral;">*</span></label>
								<input type="number" id="user_deb" min="0" name="debit" required>
								<p class="error-user_deb" style="color:coral;"></p>
							</div>

							<div class="input">
                                <button class="js-calc-refback-button">Калькулятор</button>
                                <label>Укажите сумму выплаты (USD)<span style="color:coral;">*</span></label>
								<span style="color:coral;font-size:13px;text-align:left;display:inline-block;width:100%;">разделитель запятая, пример 21,5</span>
								<input type="number" id="user_curr" min="0" step="0.01" name="currency" required>
								<p class="error-user_curr" style="color:coral;"></p>
							</div>

							<div class="input">
								<label>Укажите дату, когда был внесён депозит<span style="color:coral;">*</span></label>
								<input type="text" id="user_deb_dat" min="0" name="deb_dat" required>
								<p class="error-user_deb_dat" style="color:coral;"></p>
							</div>

							<div class="input">
								<label>Укажите выбранный инвестиционный план</label>
								<input type="text" id="user_deb_plan" min="0" name="deb_plan">
								<p class="error-deb_plan" style="color:coral;"></p>
							</div>

							<div class="input">
								<label>Комментарий</label>
								<textarea name="user_text" id="user_text" cols="40" rows="5" style="resize:none;max-width:100%;"></textarea>
								<p class="error-text" style="color:coral;"></p>
							</div>
							<p class="deb-suc" style="color:forestgreen;"></p>
							<input type="submit" value="Отправить">
						</form>
					</div>

					<div class="tab-pane" id="order-insurance">
						<form id="form_insurance" method="post" action="" enctype="multipart/form-data">
							<div class="input">
								<label>Укажите E-mail, который был указан при заказе рефбека<span style="color:coral;">*</span></label>
								<input type="text" id="i_user_mail" name="ins_mail" value="" required>
								<p class="i-error-mail" style="color:coral;"></p>
							</div>

							<div class="input">
								<label>Укажите название проекта<span style="color:coral;">*</span></label>
								<input type="text" id="i_user_proj" name="ins_proj" value="" required>
								<p class="i-error-proj" style="color:coral;"></p>
							</div>

                            <div class="input">
                                <label>Ваш логин в проекте<span style="color:coral;"></span></label>
                                <input type="text" id="i_user_proj_login" name="ins_proj_login" value="" required>
                                <p class="error-user_proj_login" style="color:coral;"></p>
                            </div>

                            <div class="input">
                                <label>Ссылка на Ваш комментарий<span style="color:coral;"></span></label>
                                <input type="text" id="i_user_proj_comment_link" name="ins_proj_comment_link" value="" required>
                                <p class="error-user_proj_comment_link" style="color:coral;"></p>
                            </div>

							<div class="input">
								<label>Укажите кошелёк, который был указан при заказе рефбека<span style="color:coral;">*</span></label>
								<input type="text" id="i_user_wall" value="" name="ins_wall" required>
								<p class="i-error-wall" style="color:coral;"></p>
							</div>


							<div class="input">
								<label>Укажите сумму потери (USD)</label>
								<input type="number" id="i_user_loss" min="0" step="0.01" name="ins_loss">
								<p class="i-error-loss" style="color:coral;"></p>
							</div>

							<div class="input">
								<label>Комментарий</label>
								<textarea id="i_user_text" cols="40" rows="5" name="ins_text" style="resize:none;max-width:100%;"></textarea>
								<p class="i-error-text" style="color:coral;"></p>
							</div>

                            <div class="input">
                                <label>Изображения</label>
                                <input type="file" name="attachmentFiles[]" class="fileInput" accept="image/*" multiple>
                            </div>
							<p class="i-suc" style="color:forestgreen;"></p>
                            <input type="hidden" name="action" value="user_insur">
							<input type="submit" value="Отправить">
						</form>
					</div>
				</div>
				
			</div>
<?php // var_dump( $ref_array ); ?>
			<h2>рефералы участующие в конкурсе</h2>
			<ul class="competitions">
				<?php if( $ref_array ): ?>
				<?php foreach( $ref_array as $user_items ): ?>
					<?php 
						$ref_user_items = get_user_by( 'ID', $user_items );
						if( !is_bool($ref_user_items)  ){

							$user_blog_bonus = get_user_meta( $user_items, 'user_event_blog', true );
							if( $user_blog_bonus == 'event_bl' ){
								?>
								<li data-text="Id реферала"><b><?php echo $user_items; ?></b></li>
								<li data-text="Реферальная ссылка"><b><?php echo home_url(); ?>/?userid=<?php echo $user_ID; ?>&refid=21<?php echo $user_ID; ?></b></li>
								<li data-text="Ник"><b><?php echo $ref_user_items->user_nicename; ?></b></li>
								<li data-text="Почта реферала"><b><?php echo $ref_user_items->user_email; ?></b></li>
								<?php
							}
						}
					 ?>
				<?php endforeach; ?>
				<?php else: ?>
					<li data-text="Id реферала"><b>Нет</b></li>
					<li data-text="Реферальная ссылка"><b>Нет</b></li>
					<li data-text="Ник"><b>Нет</b></li>
					<li data-text="Почта реферала"><b>Нет</b></li>
				<?php endif; ?>
			</ul>
			<h2>рефералы c регистрацией на бонус от блога</h2>
			<ul class="competitions">
				<?php if( $ref_array ): ?>
				<?php foreach( $ref_array as $user_items ): ?>
					<?php 
						$ref_user_items = get_user_by( 'ID', $user_items );
						if( !is_bool($ref_user_items)  ){

							$user_blog_event = get_user_meta( $user_items, 'user_bonus_blog', true );
							if( $user_blog_event == 'bonus_bl' ){
								?>
								<li data-text="Id реферала"><b><?php echo $user_items; ?></b></li>
								<li data-text="Реферальная ссылка"><b><?php echo home_url(); ?>/?userid=<?php echo $user_ID; ?>&refid=21<?php echo $user_ID; ?></b></li>
								<li data-text="Ник"><b><?php echo $ref_user_items->user_nicename; ?></b></li>
								<li data-text="Почта реферала"><b><?php echo $ref_user_items->user_email; ?></b></li>
								<?php
							}
						}
					 ?>
				<?php endforeach; ?>
				<?php else: ?>
					<li data-text="Id реферала"><b>Нет</b></li>
					<li data-text="Реферальная ссылка"><b>Нет</b></li>
					<li data-text="Ник"><b>Нет</b></li>
					<li data-text="Почта реферала"><b>Нет</b></li>
				<?php endif; ?>
			</ul>
		</div>
	</div><!-- END Page Content -->
</div><!-- END main content -->


<?php get_footer();