<?php

function ab_wp_admin_scripts () {
	wp_enqueue_style( 'admin-custom-css', get_stylesheet_directory_uri() . '/css/dist/css/admin-css.css', [], '',
		'all' );
	wp_enqueue_script( 'admin-custom-script', get_stylesheet_directory_uri() . '/js/ad.custom.js', [ 'jquery' ],
		'1.0', true );
}

add_action( 'admin_enqueue_scripts', 'ab_wp_admin_scripts' );

add_action( 'wp_enqueue_scripts', 'enqueue_parent_styles' );
function enqueue_parent_styles () {
	
	wp_enqueue_script( 'jquery-ui-datepicker' );
	
	wp_enqueue_style( 'parent-layout-boxed', get_template_directory_uri() . '/css/layout/boxed.css' );
	wp_enqueue_style( 'parent-animate-css', get_template_directory_uri() . '/css/animate.min.css' );
	wp_enqueue_style( 'parent-custom-styles', get_template_directory_uri() . '/css/custom.css.php' );
	wp_enqueue_style( 'dist-styles', get_stylesheet_directory_uri() . '/css/dist/css/main.css' );
	
	wp_enqueue_script( 'dist-script', get_stylesheet_directory_uri() . '/css/dist/js/bootstrap.min.js', [ 'jquery' ],
		'1.0', true );
	wp_enqueue_script( 'jquery-maskedinput', get_stylesheet_directory_uri() . '/js/jquery.maskedinput-1.2.2.js',
		[ 'jquery' ], '1.0', true );
	wp_enqueue_script( 'custom-script', get_stylesheet_directory_uri() . '/js/custom.js', [ 'jquery' ], '1.0', true );
	
}

/* const pay system*/
const CAT_NAME       = 'type_bid';
const PAY_PERFECT    = 'perfect_money';
const PAY_PAYEER     = 'payeer';
const PAY_ADVCASH    = 'advcash';
const MAIL_ADMIN     = 'info@gain-profit.com';
const MAIL_ADMIN_SEC = 'fin@gain-profit.com';
// 'info@gain-profit.com'

/**
 *
 * Registration ajax
 *
 */
add_action( 'wp_ajax_new_user_registration', 'new_user_registration' );
add_action( 'wp_ajax_nopriv_new_user_registration', 'new_user_registration' );
function new_user_registration () {
	// $username = sanitize_user( $_POST['user_login'] );
	$username = $_POST['user_name'];
	
	$useremail       = sanitize_email( $_POST['user_email'] );
	$userpass        = esc_attr( $_POST['user_psv'] );
	$user_blog_bonus = esc_attr( $_POST['bonus_bl'] );  // bonus_bl
	$user_blog_event = esc_attr( $_POST['event_bl'] );  // event_bl
	
	// $user_ref_id = ( isset( $_POST['user_ref_u_id'] ) ) ? $_POST['user_ref_u_id'] : '';
	$user_ref_id = ( isset( $_COOKIE['referal'] ) ) ? $_COOKIE['referal'] : '';
	// $ref_user = get_user_by( 'id', $user_ref_id );
	$form_ref_id = ( isset( $_POST['user_ref_e_id'] ) ) ? $_POST['user_ref_e_id'] : '';
	
	if ( ! validate_username( $username ) ) {
		echo "login-no-val";
		wp_die();
	} else if ( username_exists( $username ) ) {
		echo "login-dupl";
		wp_die();
	}
	
	if ( ! is_email( $useremail ) ) {
		echo "email-no-val";
		wp_die();
	} else if ( email_exists( $useremail ) ) {
		echo "email-dupl";
		wp_die();
	}
	
	if ( iconv_strlen( $userpass ) < 5 || iconv_strlen( $userpass ) > 15 ) {
		echo "pass-length";
		wp_die();
	} else if ( empty( $userpass ) ) {
		echo "pass-empty";
		wp_die();
	}
	
	if ( empty( $user_blog_bonus ) && empty( $user_blog_event ) ) {
		echo "blog-empty";
		wp_die();
	}
	
	$userdata     = [
		'user_pass'  => $userpass,
		'user_login' => $username,
		'user_email' => $useremail,
		'role'       => 'subscriber',
	];
	$user_curr_id = wp_insert_user( $userdata );
	
	// var_dump( $user_curr_id );
	
	/* ADD REFERAL */
	if ( $user_ref_id > 0 ) {
		$in_meta = get_user_meta( $user_ref_id, 'user_refer', true );
		if ( ! is_array( $in_meta ) ) {
			$in_meta = [];
		}
		$in_meta[] = $user_curr_id;
		update_user_meta( $user_curr_id, 'user_refer_master', $user_ref_id );
		update_user_meta( $user_ref_id, 'user_refer', $in_meta );
	}
	/* END ADD REFERAL */
	/* ADD USER CHECKBOX INFO */
	if ( ! empty( $user_blog_bonus ) && ! empty( $user_blog_event ) ) {
		update_user_meta( $user_curr_id, 'user_bonus_blog', $user_blog_bonus );
		update_user_meta( $user_curr_id, 'user_event_blog', $user_blog_event );
	} else if ( ! empty( $user_blog_bonus ) && empty( $user_blog_event ) ) {
		update_user_meta( $user_curr_id, 'user_bonus_blog', $user_blog_bonus );
	} else if ( empty( $user_blog_bonus ) && ! empty( $user_blog_event ) ) {
		update_user_meta( $user_curr_id, 'user_event_blog', $user_blog_event );
	}
	/* END USER CHECKBOX INFO */
	
	/* SEND MAIL */
	$subject = "Регистрация на сайте " . get_bloginfo( 'name' );
	$message = 'Вы успешно зарегестрировались на сайте: ' . get_bloginfo( 'name' ) . '. Ваш логин: ' . $username . '. Ваш пароль: ' . $userpass;
	wp_mail( $useremail, $subject, $message );
	/* END SEND MAIL */
	
	/* Login */
	$user_signon = wp_signon( [ 'user_login' => $username, 'user_password' => $userpass ], false );
	if ( is_wp_error( $user_signon ) ) {
		echo 'error';
		wp_die();
	} else {
		echo "subscriber";
		wp_die();
	}
	/* END Login */
	
	wp_die();
}

/*   *   *   *   END Registration ajax   *   *   *   *   */

/**
 *
 * AJAX FORM DEBIT
 *
 */
add_action( 'wp_ajax_user_bonus', 'user_bonus' );
add_action( 'wp_ajax_nopriv_user_bonus', 'user_bonus' );
function user_bonus () {
	$type_form = $_POST['type_form'];
	$user_id   = $_POST['user_id'];
	// $user_nic = $_POST['user_nic'];
	$user_mail       = $_POST['user_mail'];
	$user_proj       = $_POST['user_proj'];
	$user_proj_login = $_POST['proj_login'];
	$user_wall       = $_POST['user_wall'];
	// $user_deb = $_POST['user_deb'];
	$user_curr = $_POST['user_curr'];
	// $user_deb_data = $_POST['user_deb_dat'];
	// $user_deb_plan = $_POST['user_deb_plan'];
	$user_text     = $_POST['user_text'];
	$user_pay_type = $_POST['user_pay_type'];
	
	// $user_pay = $_POST['pay_type'];
	$user_total = get_user_meta( $user_id, 'user_curr', true );
	$site_url   = home_url();
	$admin_mail = get_option( 'admin_email' );
	
	//ADD POST
	$cat_name = CAT_NAME;
	$idObj    = get_term_by( 'slug', $user_pay_type, CAT_NAME );
	$id       = $idObj->term_id;
	
	$message_body = '<div><table>
	<tr><td>Название проекта, в который был сделан вклад: </td><td>' . $user_proj . '</td></tr>
	<tr><td>Логин в проекте: </td><td>' . $user_proj_login . '</td></tr>
	<tr><td>Email в проекте: </td><td>' . $user_mail . '</td></tr>
	<tr><td>Платежная система: </td><td>' . $user_pay_type . '</td></tr>
	<tr><td>Реквизиты электронного кошелька: </td><td>' . $user_wall . '</td></tr>
	<tr><td>Сумма выплаты: </td><td>' . $user_curr . '</td></tr>
	<tr><td>Комментарий: </td><td>' . $user_text . '</td></tr>
	</table></div>';
	
	$post_data = [
		'post_title'   => wp_strip_all_tags( $user_proj ),
		'post_content' => "<h2>Заказ призовых из личного кабинета</h2>" . $message_body,
		'post_status'  => 'publish',
		'post_author'  => $user_id,
		'post_type'    => 'contest',
	];
	// Вставляем запись в базу данных
	$post_id = wp_insert_post( $post_data );
	wp_set_object_terms( $post_id, $id, $cat_name );
	
	// добавляем к записи поля
	update_post_meta( $post_id, 'pm_user_login', $user_proj_login );
	update_post_meta( $post_id, 'pm_user_mail', $user_mail );
	update_post_meta( $post_id, 'pm_user_wall', $user_wall );
	update_post_meta( $post_id, 'pm_user_amount', $user_curr );
	update_post_meta( $post_id, 'pm_user_pay_type', $user_pay_type );
	update_post_meta( $post_id, 'pm_user_pay_status', false );
	update_post_meta( $post_id, 'pm_user_send_type', $type_form );
	
	/* SEND MAIL */
	$subject = "Заказ призовых из личного кабинета " . get_bloginfo( 'name' );
	
	$headers = [ 'Content-Type: text/html; charset=UTF-8' ];
	wp_mail( MAIL_ADMIN, $subject, $message_body, $headers );
	wp_mail( MAIL_ADMIN_SEC, $subject, $message_body, $headers );
	
	/* END SEND MAIL */
	wp_die();
}

add_action( 'wp_ajax_user_debit', 'user_debit' );
add_action( 'wp_ajax_nopriv_user_debit', 'user_debit' );
function user_debit () {
	$type_form       = $_POST['type_form'];
	$user_id         = $_POST['user_id'];
	$user_nic        = $_POST['user_nic'];
	$user_mail       = $_POST['user_mail'];
	$user_proj       = $_POST['user_proj'];
	$user_proj_login = $_POST['proj_login'];
	$user_proj_mail  = $_POST['proj_mail'];
	$user_wall       = $_POST['user_wall'];
	$user_deb        = $_POST['user_deb'];
	$user_curr       = $_POST['user_curr'];
	$user_deb_data   = $_POST['user_deb_dat'];
	$user_deb_plan   = $_POST['user_deb_plan'];
	$user_text       = $_POST['user_text'];
	$user_pay_type   = $_POST['user_pay_type'];
	
	// $user_pay = $_POST['pay_type'];
	$user_total = get_user_meta( $user_id, 'user_curr', true );
	$site_url   = home_url();
	$admin_mail = get_option( 'admin_email' );
	
	
	//ADD POST
	$cat_name = CAT_NAME;
	$idObj    = get_term_by( 'slug', $user_pay_type, CAT_NAME );
	$id       = $idObj->term_id;
	
	$message_body = '<div><table>
	<tr><td>Название проекта, в который был сделан вклад: </td><td>' . $user_proj . '</td></tr>
	<tr><td>Логин в проекте: </td><td>' . $user_proj_login . '</td></tr>
	<tr><td>Email в проекте: </td><td>' . $user_proj_mail . '</td></tr>
	<tr><td>Платежная система: </td><td>' . $user_pay_type . '</td></tr>
	<tr><td>Реквизиты электронного кошелька: </td><td>' . $user_wall . '</td></tr>
	<tr><td>Сумма вклада: </td><td>' . $user_deb . '</td></tr>
	<tr><td>Сумма выплаты: </td><td>' . $user_curr . '</td></tr>
	<tr><td>Дату, когда был внесён депозит: </td><td>' . $user_deb_data . '</td></tr>
	<tr><td>Выбранный инвестиционный план: </td><td>' . $user_deb_plan . '</td></tr>
	<tr><td>Комментарий: </td><td>' . $user_text . '</td></tr>
	</table></div>';
	
	$post_data = [
		'post_title'   => wp_strip_all_tags( $user_proj ),
		'post_content' => "<h2>Заказ рефбека через личный кабинет</h2>" . $message_body,
		'post_status'  => 'publish',
		'post_author'  => $user_id,
		'post_type'    => 'contest',
	];
	// Вставляем запись в базу данных
	$post_id = wp_insert_post( $post_data );
	wp_set_object_terms( $post_id, $id, $cat_name );
	
	// добавляем к записи поля
	update_post_meta( $post_id, 'pm_user_login', $user_proj_login );
	update_post_meta( $post_id, 'pm_user_mail', $user_proj_mail );
	update_post_meta( $post_id, 'pm_user_wall', $user_wall );
	update_post_meta( $post_id, 'pm_user_amount', $user_curr );
	update_post_meta( $post_id, 'pm_user_pay_type', $user_pay_type );
	update_post_meta( $post_id, 'pm_user_pay_status', false );
	update_post_meta( $post_id, 'pm_user_send_type', $type_form );
	
	/* SEND MAIL */
	$subject = "Заказ рефбека через личный кабинет " . get_bloginfo( 'name' );
	
	$headers = [ 'Content-Type: text/html; charset=UTF-8' ];
	wp_mail( MAIL_ADMIN, $subject, $message_body, $headers );
	wp_mail( MAIL_ADMIN_SEC, $subject, $message_body, $headers );
	/* END SEND MAIL */
	wp_die();
}

/*   *   *   *   END AJAX FORM DEBIT   *   *   *   *   */


add_action( 'wp_ajax_user_insur', 'user_insurance' );
add_action( 'wp_ajax_nopriv_user_insur', 'user_insurance' );
function user_insurance () {
	$ins_mail              = $_POST['ins_mail'];
	$ins_proj              = $_POST['ins_proj'];
	$ins_wall              = $_POST['ins_wall'];
	$ins_loss              = $_POST['ins_loss'];
	$ins_text              = $_POST['ins_text'];
	$ins_proj_login        = $_POST['ins_proj_login'];
	$ins_proj_comment_link = $_POST['ins_proj_comment_link'];
	
	/* SEND MAIL */
	$subject      = "Заявка на страховку из личного кабинета " . get_bloginfo( 'name' );
	$message_body = '<div><table>
	<tr><td>E-mail, который был указан при заказе рефбека: </td><td>' . $ins_mail . '</td></tr>
	<tr><td>Название проекта: </td><td>' . $ins_proj . '</td></tr>
	<tr><td>Логин в проекте: </td><td>' . $ins_proj_login . '</td></tr>
	<tr><td>Ссылка на комментарий: </td><td><a href="' . $ins_proj_comment_link . '"> ' . $ins_proj_comment_link . '</a></td></tr>
	<tr><td>Кошелёк, который был указан при заказе рефбека: </td><td>' . $ins_wall . '</td></tr>
	<tr><td>Сумма потери: </td><td>' . $ins_loss . '</td></tr>
	<tr><td>Комментарий: </td><td>' . $ins_text . '</td></tr>
	</table></div>';
	
	$headers    = [ 'Content-Type: text/html; charset=UTF-8' ];
	$attachment = [];
	if ( ! empty( $_FILES['attachmentFiles'] ) ) {
		$files = $_FILES['attachmentFiles'];
		foreach ( $files['name'] as $key => $value ) {
			if ( $files['name'][ $key ] && getimagesize( $files['tmp_name'][ $key ] ) ) {
				$file         = [
					'name'     => $files['name'][ $key ],
					'type'     => $files['type'][ $key ],
					'tmp_name' => $files['tmp_name'][ $key ],
					'error'    => $files['error'][ $key ],
					'size'     => $files['size'][ $key ],
				];
				$attachment[] = wp_handle_upload( $file, [ 'test_form' => false ] )['file'];
			}
			
		}
	}
	
	wp_mail( MAIL_ADMIN, $subject, $message_body, $headers, $attachment );
	wp_mail( MAIL_ADMIN_SEC, $subject, $message_body, $headers, $attachment );
	/* END SEND MAIL */
	
	wp_die();
}

/*   *   *   *   END AJAX FORM INSURANCE   *   *   *   *   */


/**
 *
 * AJAX FORM PASSWORD FORGOT
 *
 */
add_action( 'wp_ajax_fog_pass', 'fog_pass' );
add_action( 'wp_ajax_nopriv_fog_pass', 'fog_pass' );
function fog_pass () {
	
	$user_mail = $_POST['user_mail'];
	
	if ( ! is_email( $user_mail ) || ! email_exists( $user_mail ) ) {
		echo "no-mail";
		wp_die();
	} else if ( email_exists( $user_mail ) ) {
		$user     = get_user_by( 'email', $user_mail );
		$new_pass = wp_generate_password( 10, false );
		
		wp_set_password( $new_pass, $user->ID );
		
		$subject      = "Новый пароль на сайте " . get_bloginfo( 'name' );
		$message_body = '<div><table>
		<tr><td>Новый пароль: </td><td>' . $new_pass . '</td></tr>
		</table></div>';
		$headers      = [ 'Content-Type: text/html; charset=UTF-8' ];
		wp_mail( $user_mail, $subject, $message_body, $headers );
		wp_die();
	}
	
	wp_die();
}

/*   *   *   *   END AJAX FORM PASSWORD FORGOT   *   *   *   *   */

/**
 *
 * AJAX FORM DEBIT
 *
 */
add_action( 'wp_ajax_del_post', 'delete_custom_post' );
add_action( 'wp_ajax_nopriv_del_post', 'delete_custom_post' );
function delete_custom_post () {
	$post_id = $_POST['post_del'];
	
	// wp_trash_post( $post_id );
	wp_delete_post( $post_id );
	
	echo "Удален";
	wp_die();
}


/**
 * payment systems show message
 */
function new_pay_list () {
	$pay_permon = '0';
	$pay_adcash = '0';
	$pay_payeer = '0';
	
	$pay_args = [
		'post_type'      => 'contest',
		'posts_per_page' => - 1,
	];
	
	/*  PAY_PERFECT PAY_PAYEER PAY_ADVCASH  */
	
	$pay_query = new WP_Query( $pay_args );
	if ( $pay_query->have_posts() ):
		while ( $pay_query->have_posts() ):
			$pay_query->the_post();
			$pay_item_id = get_the_ID();
			
			$post_pay_type   = get_post_meta( $pay_item_id, 'pm_user_pay_type', true );
			$post_pay_amount = get_post_meta( $pay_item_id, 'pm_user_amount', true );
			$post_pay_status = get_post_meta( $pay_item_id, 'pm_user_pay_status', true );
			
			if ( $post_pay_type == PAY_PERFECT ) {
				if ( ! $post_pay_status ) {
					$pay_permon += $post_pay_amount;
				}
			} else if ( $post_pay_type == PAY_PAYEER ) {
				if ( ! $post_pay_status ) {
					$pay_payeer += $post_pay_amount;
				}
			} else if ( $post_pay_type == PAY_ADVCASH ) {
				if ( ! $post_pay_status ) {
					$pay_adcash += $post_pay_amount;
				}
			}
		endwhile;
	endif;
	wp_reset_postdata();
	$res_func = "<div class='js-pay-list' style='float:left;line-height:30px;'>
		Сумма по запросам для выплат - PerfectMoney:&nbsp;&nbsp;<span style='color:coral;'>$pay_permon</span>;&nbsp;&nbsp;
		Payeer:&nbsp;&nbsp;<span style='color:coral;'>$pay_payeer</span>;&nbsp;&nbsp;
		AdvCash:&nbsp;&nbsp;<span style='color:coral;'>$pay_adcash</span>;</div>";
	
	return $res_func;
}

// add_action( 'all_admin_notices', 'show_pay_total' );
add_action( 'manage_posts_extra_tablenav', 'show_pay_total' );
function show_pay_total () {
	if ( $_GET['post_type'] === 'contest' ) {
		$res_func = new_pay_list();
		echo $res_func;
	}
}

add_action( 'manage_posts_extra_tablenav', 'show_pay_status' );
function show_pay_status () {
	if ( $_GET['post_type'] === 'contest' ) {
		$res_func = "<div class='js-pay-res-wrap'><span>x</span><div class='js-pay-res'></div></div>";
		echo $res_func;
	}
}

function ab_clean_user_int ( $val ) {
	$val = trim( $val );
	$val = strip_tags( $val );
	$val = htmlspecialchars( $val );
	$val = (int) $val;
	
	return $val;
}

function ab_clean_user_float ( $val ) {
	$val = trim( $val );
	$val = strip_tags( $val );
	$val = htmlspecialchars( $val );
	$val = preg_replace( '/[^0-9.,]*/', '', $val );
	$val = (float) $val;
	$val = round( $val, 2 );
	
	return $val;
}


/**
 * PERFECT MONEY  оплата
 * $pay_amount сумма
 * $pay_wall кошелек
 */
function ab_pay_perfect_money ( $pay_amount, $pay_wall, $post_id ) {
	$my_account = '3588239';
	$my_passwor = 'NAuiWfsoS9mY6V2';
	$my_wall    = 'U16719267';
	
	$pay_amount = $pay_amount;
	$pay_wall   = $pay_wall;
	
	$pm_out = '';
	
	$param = [
		'AccountID'     => "$my_account",
		'PassPhrase'    => "$my_passwor",
		'Payer_Account' => "$my_wall",
		'Payee_Account' => "$pay_wall",
		'Amount'        => "$pay_amount",
		'PAY_IN'        => "1",
		'PAYMENT_ID'    => "$post_id",
	];
	$param = http_build_query( $param );
	
	$pm_api = fopen( "https://perfectmoney.is/acct/confirm.asp?$param", 'rb' );
	if ( $pm_api === false ) {
		$pm_out = 'Ошибка URL';
		
		return $pm_out;
		wp_die();
	}
	$pm_out = [];
	$pm_out = "";
	while ( ! feof( $pm_api ) ) {
		$pm_out .= fgets( $pm_api );
	}
	fclose( $pm_api );
	
	return $pm_out;
}

/**
 * PAYEER  оплата
 * $pay_amount сумма
 * $pay_wall кошелек
 */
// include_once( dirname(__DIR__) . '/classes/cpayeer.php' );
require_once 'classes/cpayeer.php';
function ab_balance_payeer () {
	// $accountNumber = 'P93901479';
	// $apiId = '536595722';
	// $apiKey = 'KlRj51ARnudJ5y0g';
	$accountNumber = 'P93901479';
	$apiId         = '536595722';
	$apiKey        = 'KlRj51ARnudJ5y0g';
	$payeer        = new CPayeer( $accountNumber, $apiId, $apiKey );
	$status        = "";
	if ( $payeer->isAuth() ) {
		$arBalance = $payeer->getBalance();
		$status    = '<pre>' . print_r( $arBalance, true ) . '</pre>';
	} else {
		$status = '<pre>' . print_r( $payeer->getErrors(), true ) . '</pre>';
	}
	
	return $status;
}

function ab_pay_payeer ( $pay_amount, $pay_wall, $post_id ) {
	// require_once( dirname(__DIR__) . '/classes/cpayeer.php' );
	$accountNumber = 'P93901479';
	$apiId         = '541716558';
	$apiKey        = '4G7SpHgoA9Qm6rPm';
	$payeer        = new CPayeer( $accountNumber, $apiId, $apiKey );
	$status        = "";
	
	if ( $payeer->isAuth() ) {
		$arTransfer = $payeer->transfer( [
			'curIn'  => 'USD',
			'sum'    => $pay_amount,
			'curOut' => 'USD',
			'to'     => $pay_wall,
		] );
		if ( empty( $arTransfer['errors'] ) ) {
			$status = $arTransfer['historyId'] . ": Перевод средств успешно выполнен";
		} else {
			$status = $arTransfer["errors"];
		}
	} else {
		$status = $payeer->getErrors();
	}
	
	return $status;
}

function ab_error_payeer ( $error ) {
	$error_args = [
		'balanceError'               => 'недостаточно средств для перевода',
		'balanceError000'            => 'недостаточно средств для перевода с учетом ограничений по аккаунту',
		'transferError'              => 'ошибка перевода, необходимо повторить перевод через некоторое время',
		'sumNotNull'                 => 'сумма отправления и получения не должна быть нулевой',
		'sumInNotMinus'              => 'сумма отправления не должна быть быть отрицательной',
		'toError'                    => 'получатель указан неверно',
		'outputHold'                 => 'У нас возникли вопросы к Вашей деятельности. Пожалуйста, обратитесь в Службу поддержки',
		'transferToForbiddenCountry' => 'Перевод клиентам в некоторые страны запрещен',
	];
	$out_error  = 'Ошибка';
	if ( is_array( $error ) ) {
		foreach ( $error_args as $key => $value ):
			if ( in_array( $key, $error ) ) {
				$out_error = $value;
			}
		endforeach;
	} else {
		foreach ( $error_args as $key => $value ):
			if ( $error == $key ) {
				$out_error = $value;
			}
		endforeach;
	}
	
	return $out_error;
}

/**
 * advcash  оплата
 */
require_once 'classes/MerchantWebService.php';
function ab_pay_advcash ( $pay_amount, $pay_wall, $post_id ) {
	$merchantWebService = new MerchantWebService();
	$arg0               = new authDTO();
	// $arg0->apiName = "API-gaint";
	$arg0->apiName             = "api";
	$arg0->accountEmail        = "info@gain-profit.com";
	$arg0->authenticationToken = $merchantWebService->getAuthenticationToken( "C1!0nW6bZa" );
	$arg1                      = new sendMoneyRequest();
	$arg1->amount              = $pay_amount;
	$arg1->currency            = "USD";
	// $arg1->email = $pay_mail;
	$arg1->walletId            = $pay_wall;
	$arg1->note                = "note";
	$arg1->savePaymentTemplate = false;
	
	$validationSendMoney       = new validationSendMoney();
	$validationSendMoney->arg0 = $arg0;
	$validationSendMoney->arg1 = $arg1;
	$sendMoney                 = new sendMoney();
	$sendMoney->arg0           = $arg0;
	$sendMoney->arg1           = $arg1;
	$resBalance                = '';
	try {
		$merchantWebService->validationSendMoney( $validationSendMoney );
		$sendMoneyResponse = $merchantWebService->sendMoney( $sendMoney );
		
		$resBalance .= print_r( $sendMoneyResponse, true ) . "<br/><br/>";
		$resBalance .= $sendMoneyResponse->return . "<br/><br/>";
	} catch ( Exception $e ) {
		$resBalance .= "ERROR MESSAGE : " . $e->getMessage() . "<br/>";
		// $resBalance .= $e->getTraceAsString();
	}
	
	return $resBalance;
}

function ab_balance_advcash () {
	$merchantWebService        = new MerchantWebService();
	$arg0                      = new authDTO();
	$arg0->apiName             = "API-gaint";
	$arg0->accountEmail        = "info@gain-profit.com";
	$arg0->authenticationToken = $merchantWebService->getAuthenticationToken( "G26Ap5pRoF54" );
	$getBalances               = new getBalances();
	$getBalances->arg0         = $arg0;
	$resBalance                = '';
	try {
		$getBalancesResponse = $merchantWebService->getBalances( $getBalances );
		$resBalance          .= print_r( $getBalancesResponse, true ) . "<br/><br/>";
		$resBalance          .= print_r( $getBalancesResponse->return, true ) . "<br/><br/>";
	} catch ( Exception $e ) {
		$resBalance .= "ERROR MESSAGE => " . $e->getMessage() . "<br/>";
		$resBalance .= $e->getTraceAsString();
	}
	
	return $resBalance;
}


/**
 * получить реферера по ID пользователя
 *
 */
function ad_get_user_refer ( $user_id ) {
	$user_field = get_user_meta( $user_id, 'user_refer_master', true );
	if ( ! empty( $user_field ) ) {
		return $user_field;
	}
}

/**
 * получить 20%
 *
 */
function ad_get_refer_percent ( $in_amount ) {
	$out_amount = ( $in_amount / 100 ) * 20;
	$out_amount = round( $out_amount, 2 );
	
	return $out_amount;
}


/**
 * AJAX PAY BUTTON
 * обработка кнопки оплатить
 *
 *
 */
add_action( 'wp_ajax_pay_action', 'pay_custom_post' );
add_action( 'wp_ajax_nopriv_pay_action', 'pay_custom_post' );
function pay_custom_post () {
	$pay_post_id     = $_POST['pay_post'];        // id поста
	$pay_user_id     = $_POST['pay_user'];        // id пользователя
	$pay_wall        = $_POST['pay_wall'];        // кошелек пользователя
	$pay_amount      = $_POST['pay_amount'];      // сумма для выплаты
	$pay_type        = $_POST['pay_type'];        // тип платежной системы
	$pay_post_status = get_post_meta( $pay_post_id, 'pm_user_pay_status', true );
	$pay_user_mail   = get_post_meta( $pay_post_id, 'pm_user_mail', true );
	
	$pay_amount = ab_clean_user_float( $pay_amount ); // очистка сумма от пользователя
	$refer      = ad_get_user_refer( $pay_user_id );  // id рефера пользователя
	// print_r( $_POST );
	// wp_die();
	
	$response = [];
	
	if ( $pay_type == PAY_PERFECT ) {
		if ( ! $pay_post_status ) {
			// $pay_amount, $pay_wall, $post_id  -  сумма, кошелек, id поста
			$pattern_error     = "/error/i";
			$pattern_error_url = "/Ошибка/i";
			$return_pay_info   = ab_pay_perfect_money( $pay_amount, $pay_wall, $pay_post_id );
			$res_pattern       = preg_match( $pattern_error, $return_pay_info );
			$res_pattern_url   = preg_match( $pattern_error_url, $return_pay_info );
			
			if ( ! $res_pattern && ! $res_pattern_url ) {
				
				if ( ! empty( $refer ) ) {
					if ( ! empty( $pay_amount ) && is_numeric( $pay_amount ) || is_float( $pay_amount ) ) {
						$refer_percent    = ad_get_refer_percent( $pay_amount );
						$old_refer_amount = get_user_meta( $refer, 'user_curr', true );
						$new_refer_amount = $old_refer_amount + $refer_percent;
						update_user_meta( $refer, 'user_curr', $new_refer_amount );
					}
				}
				
				// ставим поле true, если пост оплачен
				update_post_meta( $pay_post_id, 'pm_user_pay_status', true );
				// обновляем сумму по запросам
				$new_pay_list       = new_pay_list();
				$response['alert']  = $return_pay_info;
				$response['html']   = $new_pay_list;
				$response['status'] = 'pay-send';
			} else {
				$response['alert']  = $return_pay_info;
				$response['status'] = 'no-pay';
			}
			
		}
	} else if ( $pay_type == PAY_PAYEER ) {
		if ( ! $pay_post_status ) {
			// $pay_amount, $pay_wall, $post_id  -  сумма, кошелек, id поста
			$return_pay_info = ab_pay_payeer( $pay_amount, $pay_wall, $pay_post_id );
			
			$response['errors'] = $return_pay_info["errors"];
			
			if ( ! empty( $response['errors'] ) ) {
				
				if ( ! empty( $refer ) ) {
					if ( ! empty( $pay_amount ) && is_numeric( $pay_amount ) || is_float( $pay_amount ) ) {
						$refer_percent    = ad_get_refer_percent( $pay_amount );
						$old_refer_amount = get_user_meta( $refer, 'user_curr', true );
						$new_refer_amount = $old_refer_amount + $refer_percent;
						update_user_meta( $refer, 'user_curr', $new_refer_amount );
					}
				}
				
				// ставим поле true, если пост оплачен
				update_post_meta( $pay_post_id, 'pm_user_pay_status', true );
				// обновляем сумму по запросам
				$new_pay_list       = new_pay_list();
				$response['alert']  = $return_pay_info;
				$response['html']   = $new_pay_list;
				$response['status'] = 'pay-send';
				
			} else {
				$payeer_error       = ab_error_payeer( $return_pay_info );
				$response['alert']  = $payeer_error;
				$response['status'] = 'no-pay';
			}
			
		}
	} else if ( $pay_type == PAY_ADVCASH ) {
		if ( ! $pay_post_status ) {
			
			$pattern_error = "/ERROR MESSAGE/i";
			// $pay_amount, $pay_mail, $post_id  -  сумма, почта, id поста
			$return_pay_info = ab_pay_advcash( $pay_amount, $pay_wall, $pay_post_id );
			$res_pattern     = preg_match( $pattern_error, $return_pay_info );
			
			if ( ! $res_pattern ) {
				
				if ( ! empty( $refer ) ) {
					if ( ! empty( $pay_amount ) && is_numeric( $pay_amount ) || is_float( $pay_amount ) ) {
						$refer_percent    = ad_get_refer_percent( $pay_amount );
						$old_refer_amount = get_user_meta( $refer, 'user_curr', true );
						$new_refer_amount = $old_refer_amount + $refer_percent;
						update_user_meta( $refer, 'user_curr', $new_refer_amount );
					}
				}
				
				// ставим поле true, если пост оплачен
				update_post_meta( $pay_post_id, 'pm_user_pay_status', true );
				// обновляем сумму по запросам
				$new_pay_list      = new_pay_list();
				$response['alert'] = $return_pay_info;
				// $response['alert'] = 'Перевод осуществлен';
				$response['html']   = $new_pay_list;
				$response['status'] = 'pay-send';
				
			} else {
				$response['alert']  = $return_pay_info;
				$response['status'] = 'no-pay';
			}
			
		}
	} else {
		$response['alert']  = 'empty';
		$response['status'] = 'no-pay';
	}
	
	wp_send_json( $response );
	// echo $new_pay_list;
	// wp_die();
}


/**
 *  USER currency
 *
 */
add_action( 'show_user_profile', 'userShowCu' );
add_action( 'edit_user_profile', 'userShowCu' );
// add_action('personal_options_update', 'UserFBAddress_update');
function userShowCu ( $user ) {
	?>
	<h3>Внутренний счет пользователя</h3>
	<table class="form-table">
		<tr>
			<th><label for="currency">Счет</label></th>
			<?php if ( current_user_can( 'administrator' ) ): ?>
				<td>
					<input type="number" min="0" name="currency" id="currency"
					       value="<?php echo esc_attr( get_the_author_meta( 'user_curr', $user->ID ) ); ?>"
					       class="regular-text"/><br/>
				</td>
			<?php else: ?>
				<td>
					<p>-<?php echo esc_attr( get_the_author_meta( 'user_curr', $user->ID ) ); ?>-</p><br/>
				</td>
			<?php endif; ?>
		</tr>
	</table>
	<?php
}

add_action( 'personal_options_update', 'userSaveCu' );
add_action( 'edit_user_profile_update', 'userSaveCu' );
function userSaveCu ( $user_id ) {
	if ( current_user_can( 'edit_users', $user_id ) ) {
		update_user_meta( $user_id, 'user_curr', $_POST['currency'] );
		// if( !empty( $_POST['bonus'] ) ){
		// 	$add_user_curr = $_POST['bonus'];
		// 	$now_user_curr = $_POST['currency'];
		// 	$total_user_curr = $add_user_curr + $now_user_curr;
		// 	update_user_meta( $user_id, 'user_curr', $total_user_curr );
		// } else {
		// 	update_user_meta( $user_id, 'user_curr', $_POST['currency'] );
		// }
	}
}


/*
 * ORDER POSTS COLUMN
 * post type заявка на оплату
 * сортировка полей в админке, добавление и удаление полей
 */
function order_post_columns ( $posts_columns ) {
	if ( $_GET['post_status'] === 'trash' ) {
		$posts_columns = [
			'cb'                => '',
			'title'             => 'Название конкурса',
			'login'             => 'Логин в проекте',
			'mail'              => 'E-mail',
			'wall'              => 'Реквизиты',
			'amount'            => 'Сумма выплаты',
			'taxonomy-type_bid' => 'Тип системы',
		];
	} else {
		$posts_columns = [
			'cb'                => '',
			'title'             => 'Название конкурса',
			'login'             => 'Логин в проекте',
			'mail'              => 'E-mail в проекте',
			'wall'              => 'Реквизиты',
			'amount'            => 'Сумма выплаты',
			'taxonomy-type_bid' => 'Тип системы',
			'action'            => 'Действие',
			'form'              => 'Форма',
		];
	}
	
	return $posts_columns;
}

add_filter( 'manage_contest_posts_columns', 'order_post_columns' );


/**
 * ADD VALUE IN COLUMN
 * post type заявка на оплату
 * Добляем кнопки оплатить/удалить
 * добавляем значения, ID поста и пользователя который подал завявку
 *
 */
function new_value_btn_column ( $column, $post_id ) {
	$post_user     = get_post( $post_id );
	$user_info     = get_user_by( 'id', $post_user->post_author );
	$proj_login    = get_post_meta( $post_id, 'pm_user_login', true );
	$proj_mail     = get_post_meta( $post_id, 'pm_user_mail', true );
	$proj_wall     = get_post_meta( $post_id, 'pm_user_wall', true );
	$proj_amount   = get_post_meta( $post_id, 'pm_user_amount', true );
	$proj_pay_type = get_post_meta( $post_id, 'pm_user_pay_type', true );
	$proj_status   = get_post_meta( $post_id, 'pm_user_pay_status', true );
	$proj_form     = get_post_meta( $post_id, 'pm_user_send_type', true );
	
	if ( empty( $proj_login ) ) {
		$proj_login = 'empty';
	}
	if ( empty( $proj_mail ) ) {
		$proj_mail = 'empty';
	}
	if ( empty( $proj_wall ) ) {
		$proj_wall = '—';
	}
	if ( empty( $proj_amount ) ) {
		$proj_amount = 'empty';
	}
	if ( empty( $proj_pay_type ) ) {
		$proj_pay_type = 'empty';
	}
	if ( empty( $proj_status ) || $proj_status === false ) {
		$proj_status = 'empty';
	} else {
		$proj_status = 'pay-ok';
	}
	
	if ( ! empty( $proj_form ) && $proj_form === 'bonus' ) {
		$proj_form_status = 'Рефбек';
	} else if ( ! empty( $proj_form ) && $proj_form === 'debit' ) {
		$proj_form_status = 'Призовые';
	}
	
	if ( $column == 'action' ) {
		if ( $proj_status === 'pay-ok' ) {
			echo '<a href="#" class="action-pay js-action-pay-disable"
				data-type=' . $proj_pay_type . ' 
				data-amount=' . $proj_amount . ' 
				data-wall=' . $proj_wall . ' 
				data-post=' . $post_id . ' 
				data-user=' . $post_user->post_author . ' 
				style="color:forestgreen;">Оплатить</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="#" class="action-del" data-post=' . $post_id . ' style="color:coral;">Удалить</a>';
		} else {
			echo '<a href="#" class="action-pay js-action-pay"
				data-type=' . $proj_pay_type . ' 
				data-amount=' . $proj_amount . ' 
				data-wall=' . $proj_wall . ' 
				data-post=' . $post_id . ' 
				data-user=' . $post_user->post_author . ' 
				style="color:forestgreen;">Оплатить</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="#" class="action-del" data-post=' . $post_id . ' style="color:coral;">Удалить</a>';
		}
	}
	if ( $column == 'login' ) {
		echo $proj_login;
	}
	if ( $column == 'mail' ) {
		// echo $user_info->user_email;
		echo $proj_mail;
	}
	if ( $column == 'wall' ) {
		echo $proj_wall;
	}
	if ( $column == 'amount' ) {
		echo $proj_amount;
	}
	if ( $column == 'form' ) {
		echo $proj_form_status;
	}
}

add_filter( 'manage_contest_posts_custom_column', 'new_value_btn_column', 5, 3 );


/**
 * USER CUSTOM COLUMNS
 *
 */
/* CREATE COLUMN */
function new_user_column ( $columns ) {
	$columns['referal'] = 'Referal';
	unset( $columns['posts'] );
	
	return $columns;
}

add_filter( 'manage_users_columns', 'new_user_column', 4 );

/* ADD VALUE IN COLUMN */
function new_value_user_column ( $val, $column_name, $user_id ) {
	switch ( $column_name ) {
		case 'referal' :
			if ( get_the_author_meta( 'user_refer_master', $user_id ) ) {
				return 'is referral';
			}
			break;
		default:
	}
	
	return $val;
}

add_filter( 'manage_users_custom_column', 'new_value_user_column', 5, 3 );

/* ADD SORT COLUMN */
function new_sortable_user_column ( $columns ) {
	$columns['referal'] = 'referal';
	
	return $columns;
}

add_filter( 'manage_users_sortable_columns', 'new_sortable_user_column' );

/* ADD ORDER COLUMN */
function new_orderby_user_column ( $query ) {
	global $wpdb, $current_screen;
	
	if ( 'users' != $current_screen->id ) {
		return;
	}
	
	if ( 'referal' == $query->get( 'orderby' ) ) {
		$query->set( 'orderby', 'meta_value' );
		$query->set( 'meta_key', 'user_refer_master' );
	}
}

add_action( 'pre_get_users', 'new_orderby_user_column' );

/**
 *
 *  ADD CUSTOM POST TYPE
 */
add_action( 'init', 'post_type_contest' );
add_action( 'init', 'create_contest_taxonomies' );
function post_type_contest () {
	$labels = [
		'name'               => _x( 'Заявки на оплату', 'post type general name', 'jozoorthemes' ),
		'singular_name'      => _x( 'Заявка на оплату', 'post type singular name', 'jozoorthemes' ),
		'add_new'            => _x( 'Add New', 'заявку', 'jozoorthemes' ),
		'add_new_item'       => __( 'Добавить новую', 'jozoorthemes' ),
		'edit_item'          => __( 'Редактировать заявку', 'jozoorthemes' ),
		'new_item'           => __( 'Новая заявка', 'jozoorthemes' ),
		'all_items'          => __( 'Все заявки', 'jozoorthemes' ),
		'view_item'          => __( 'Просмотр заявок', 'jozoorthemes' ),
		'search_items'       => __( 'Поиск заявок', 'jozoorthemes' ),
		'not_found'          => __( 'Заявок не найдено', 'jozoorthemes' ),
		'not_found_in_trash' => __( 'Нет заявок для удаления', 'jozoorthemes' ),
		'parent_item_colon'  => '',
		'menu_name'          => __( 'Заявки на оплату', 'jozoorthemes' ),
	];
	
	$args = [
		'labels'              => $labels,
		'public'              => true,
		'publicly_queryable'  => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'exclude_from_search' => true,
		'query_var'           => true,
		'rewrite'             => [ 'slug' => 'contest' ],
		'capability_type'     => 'post',
		'has_archive'         => true,
		'hierarchical'        => false,
		'menu_icon'           => 'dashicons-list-view',
		'menu_position'       => 95,
		'supports'            => [ 'title', 'editor', 'custom-fields', 'thumbnail' ],
		'taxonomies'          => [ 'type' ],
	];
	register_post_type( 'contest', $args );
}

function create_contest_taxonomies () {
	$labels = [
		'name'              => _x( 'Тип заявки', 'taxonomy general name' ),
		'singular_name'     => _x( 'Тип заявки', 'taxonomy singular name' ),
		'search_items'      => __( 'Поиск' ),
		'all_items'         => __( 'Все' ),
		'parent_item'       => __( 'Родительская заявка' ),
		'parent_item_colon' => __( 'Родительская заявка:' ),
		'edit_item'         => __( 'Редактировать' ),
		'update_item'       => __( 'Обновить' ),
		'add_new_item'      => __( 'Добавить' ),
		'new_item_name'     => __( 'Новая' ),
		'menu_name'         => __( 'Тип заявки' ),
	];
	register_taxonomy( 'type_bid', [ 'contest' ], [
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => [ 'slug' => 'type_bid' ],
	] );
}

/**
 *
 * ADD CUSTOM META BOX contest single post type
 */
//add_action( 'add_meta_boxes', 'my_meta_sel_box' );
function my_meta_sel_box () {
	add_meta_box( 'select_meta_box', 'Активность конкурса', 'render_my_meta_sel_box', 'post', 'side', 'high' );
}

function render_my_meta_sel_box ( $post ) {
	// Используем nonce для верификации
	wp_nonce_field( 'name_of_my_action', 'name_of_nonce_field' );
	
	$post_meta = get_post_meta( $post->ID, 'post_select_contest', true );
	//echo $post_meta;
	
	if ( $post_meta == 'on' || ! $post_meta ) {
		echo '<label>';
		echo '<input type="radio" checked id= "select_contest" value="on" name="select_contest" size="25" />';
		echo __( "Конкурс Открыт", 'lang' ) . '</label> ';
		echo '<br>';
		echo '<label>';
		echo '<input type="radio" id= "select_contest" value="off" name="select_contest" size="25" />';
		echo __( "Конкурс закрыт", 'lang' ) . '</label> ';
	} else if ( $post_meta == 'off' ) {
		echo '<label>';
		echo '<input type="radio" id= "select_contest" value="on" name="select_contest" size="25" />';
		echo __( "Конкурс Открыт", 'lang' ) . '</label> ';
		echo '<br>';
		echo '<label>';
		echo '<input type="radio" checked id= "select_contest" value="off" name="select_contest" size="25" />';
		echo __( "Конкурс закрыт", 'lang' ) . '</label> ';
	}
}

//add_action( 'save_post', 'save_my_meta_sel_box' );
function save_my_meta_sel_box ( $post_id ) {
	// Убедимся что поле установлено.
	if ( ! isset( $_POST['select_contest'] ) ) {
		return;
	}
	
	// проверяем nonce нашей страницы, потому что save_post может быть вызван с другого места.
	if ( ! wp_verify_nonce( $_POST['name_of_nonce_field'], 'name_of_my_action' ) ) {
		return;
	}
	
	// если это автосохранение ничего не делаем
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
		return;
	}
	
	// проверяем права юзера
	if ( ! current_user_can( 'edit_post', $post_id ) ) {
		return;
	}
	
	// Все ОК. Теперь, нужно найти и сохранить данные
	// Очищаем значение поля input.
	$my_data = sanitize_text_field( $_POST['select_contest'] );
	
	// Обновляем данные в базе данных.
	update_post_meta( $post_id, 'post_select_contest', $my_data );
}


/*********************************************************************************/


$taxname             = 'category';

// Поля при добавлении элемента таксономии
add_action( "{$taxname}_add_form_fields", 'add_new_custom_fields' );
// Поля при редактировании элемента таксономии
add_action( "{$taxname}_edit_form_fields", 'edit_new_custom_fields' );

// Сохранение при добавлении элемента таксономии
add_action( "create_{$taxname}", 'save_custom_taxonomy_meta' );
// Сохранение при редактировании элемента таксономии
add_action( "edited_{$taxname}", 'save_custom_taxonomy_meta' );

function edit_new_custom_fields ( $term ) { ?>
	<h2><?php _e( 'Выбрать сайтбар:' ); ?></h2>
	<p><select name="extra[sidebar]">
			<?php foreach ( $GLOBALS['wp_registered_sidebars'] as $sidebar ) { ?>
				<option value="<?php echo ucwords( $sidebar['id'] ); ?>"
					<?php selected( esc_attr( get_term_meta( $term->term_id, 'sidebar', 1 ) ), $sidebar['name'] ) ?>>
					<?php echo ucwords( $sidebar['name'] ); ?>
				</option>
			<?php } ?>
		</select>
	</p>
	
	<input type="hidden" name="extra_fields_nonce" value="<?php echo wp_create_nonce( __FILE__ ); ?>"/>
	<?php
}

function add_new_custom_fields ( $taxonomy_slug ) { ?>
	<h2><?php _e( 'Выбрать сайтбар:' ); ?></h2>
	<p><select name="extra[sidebar]" id="tag-sidebar">
			<?php foreach ( $GLOBALS['wp_registered_sidebars'] as $sidebar ) {
				$sel_v = get_post_meta( $post->ID, 'sidebar', 1 );
				print_r( $sel_v ); ?>
				<option value="<?php echo ucwords( $sidebar['id'] ); ?>" <?php selected( $sel_v, $sidebar['name'] ) ?>>
					<?php echo ucwords( $sidebar['name'] ); ?>
				</option>
			<?php } ?>
		</select>
	</p>
	
	<input type="hidden" name="extra_fields_nonce" value="<?php echo wp_create_nonce( __FILE__ ); ?>"/>
	<?php
}

function save_custom_taxonomy_meta ( $term_id ) {
	// die;
	if ( ! isset( $_POST['extra'] ) ) {
		return;
	}
	if ( ! current_user_can( 'edit_term', $term_id ) ) {
		return;
	}
	if (
		! wp_verify_nonce( $_POST['_wpnonce'], "update-tag_$term_id" ) && // wp_nonce_field( 'update-tag_' . $tag_ID );
		! wp_verify_nonce( $_POST['_wpnonce_add-tag'], "add-tag" ) // wp_nonce_field('add-tag', '_wpnonce_add-tag');
	) {
		return;
	}
	
	// Все ОК! Теперь, нужно сохранить/удалить данные
	$extra = wp_unslash( $_POST['extra'] );
	
	
	foreach ( $extra as $key => $val ) {
		// проверка ключа
		$_key = sanitize_key( $key );
		if ( $_key !== $key ) {
			wp_die( 'bad key' . esc_html( $key ) );
		}
		
		// очистка
		if ( $_key === 'tag_posts_shortcode_links' ) {
			$val = sanitize_textarea_field( strip_tags( $val ) );
		} else {
			$val = sanitize_text_field( $val );
		}
		// var_dump($key, $val);
		
		// сохранение
		if ( ! $val ) {
			delete_term_meta( $term_id, $_key );
		} else {
			update_term_meta( $term_id, $_key, $val );
		}
		
	}
	
	return $term_id;
}


// подключаем функцию активации мета блока (my_extra_fields)
add_action( 'add_meta_boxes', 'my_extra_fields', 1 );
function my_extra_fields () {
	add_meta_box( 'extra_fields', 'Выбор сайтбара:', 'extra_fields_box_func', [ 'post', 'category' ], 'side',
		'high' );
}

// код блока
function extra_fields_box_func ( $post ) { ?>
	<p><select name="extra[sidebar]">
			<?php foreach ( $GLOBALS['wp_registered_sidebars'] as $sidebar ) {
				$sel_v = get_post_meta( $post->ID, 'sidebar', 1 ); ?>
				<option value="<?php echo ucwords( $sidebar['id'] ); ?>" <?php selected( $sel_v, $sidebar['name'] ) ?>>
					<?php echo ucwords( $sidebar['name'] ); ?>
				</option>
			<?php } ?>
		</select>
	</p>
	
	<input type="hidden" name="extra_fields_nonce" value="<?php echo wp_create_nonce( __FILE__ ); ?>"/>
	<?php
}

// включаем обновление полей при сохранении
add_action( 'save_post', 'my_extra_fields_update', 0 );

## Сохраняем данные, при сохранении поста
function my_extra_fields_update ( $post_id ) {
	// базовая проверка
	if (
		empty( $_POST['extra'] )
		|| ! wp_verify_nonce( $_POST['extra_fields_nonce'], __FILE__ )
		|| wp_is_post_autosave( $post_id )
		|| wp_is_post_revision( $post_id )
	) {
		return false;
	}
	
	// Все ОК! Теперь, нужно сохранить/удалить данные
	$_POST['extra'] = array_map( 'sanitize_text_field', $_POST['extra'] ); // чистим все данные от пробелов по краям
	foreach ( $_POST['extra'] as $key => $value ) {
		if ( empty( $value ) ) {
			delete_post_meta( $post_id, $key ); // удаляем поле если значение пустое
			continue;
		}
		
		update_post_meta( $post_id, $key, $value ); // add_post_meta() работает автоматически
		
	}
	
	return $post_id;
}


// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


// Поля при добавлении элемента таксономии
add_action( "add_tag_form_fields", 'add_new_custom_fields_tag' );
add_action( "post_tag_edit_form_fields", 'edit_new_custom_fields_tag' );
add_action( "edit_post_tag", 'save_custom_taxonomy_meta' );
add_action( "create_post_tag", 'save_custom_taxonomy_meta' );


function add_new_custom_fields_tag ( $taxonomy ) { ?>
	<h2><?php _e( 'Выбрать сайтбар:' ); ?></h2>
	<p><select name="extra[sidebar]" id="tag-sidebar">
			<?php foreach ( $GLOBALS['wp_registered_sidebars'] as $sidebar ) {
				$sel_v = get_post_meta( $post->ID, 'sidebar', 1 );
				print_r( $sel_v ); ?>
				<option value="<?php echo ucwords( $sidebar['id'] ); ?>" <?php selected( $sel_v, $sidebar['name'] ) ?>>
					<?php echo ucwords( $sidebar['name'] ); ?>
				</option>
			<?php } ?>
		</select>
	</p>
	
	<input type="hidden" name="extra_fields_nonce" value="<?php echo wp_create_nonce( __FILE__ ); ?>"/>
	<?php
}

function edit_new_custom_fields_tag ( $term ) { ?>
	<h2><?php _e( 'Выбрать сайтбар:' ); ?></h2>
	<p><select name="extra[sidebar]">
			<?php foreach ( $GLOBALS['wp_registered_sidebars'] as $sidebar ) { ?>
				<option value="<?php echo ucwords( $sidebar['id'] ); ?>"
					<?php selected( esc_attr( get_term_meta( $term->term_id, 'sidebar', 1 ) ), $sidebar['name'] ) ?>>
					<?php echo ucwords( $sidebar['name'] ); ?>
				</option>
			<?php } ?>
		</select>
	</p>
	
	<input type="hidden" name="extra_fields_nonce" value="<?php echo wp_create_nonce( __FILE__ ); ?>"/>
	<?php
}

if ( function_exists( 'acf_add_options_page' ) ) {
	
	acf_add_options_page();
	
}

function fragment_cache ( $key, $ttl, $function ) {
	if ( is_user_logged_in() ) {
		call_user_func( $function );
		
		return;
	}
	$key    = apply_filters( 'fragment_cache_prefix', 'fragment_cache_' ) . $key;
	$output = get_transient( $key );
	if ( empty( $output ) ) {
		ob_start();
		call_user_func( $function );
		$output = ob_get_clean();
		set_transient( $key, $output, $ttl );
	}
	echo $output;
}